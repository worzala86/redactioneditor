angular.module('ngModal', [])

    .factory('dialog', ($rootScope, $compile, http, $timeout) => {
        let zIndex = 1000
        return (options) => {
            return {
                show: () => {
                    angular.element(document.querySelector('#element-menu-popup')).remove()
                    http.get(options.templateUrl, (response) => {
                        let scope = $rootScope.$new(true);
                        let element = $compile(response.data)(scope)
                        angular.forEach(options.scope, (value, key) => {
                            scope[key] = value
                        })
                        scope.buttons = options.buttons
                        angular.forEach(scope.buttons, (value) => {
                            value.click = () => {
                                if (value.callback) {
                                    value.callback(scope)
                                } else {
                                    scope.close()
                                }
                            }
                        })
                        scope.close = () => {
                            element.css('top', -window.innerHeight+'px')
                            $timeout(()=>{
                                element.remove()
                            }, 330)
                            angular.element(wrapper).css('filter', 'none')
                        }
                        element.css('top', -window.innerHeight+'px')
                        element.addClass('animate')
                        element.css('z-index', zIndex++)
                        const wrapper = document.querySelector('wrapper')
                        angular.element(wrapper).css('filter', 'blur(2px) grayscale(50%)')
                        $timeout(()=>{
                            element.css('top', '0px')
                        }, 1)
                        angular.element(document.body).append(element)
                    })
                }
            }
        }
    })

    .factory('http', ($rootScope, $http) => {
        let buffor = []
        const loadFromBuffor = (url) => {
            if (!buffor[url]) {
                return null
            }
            return buffor[url]
        }
        const addToBuffor = (url, data) => {
            if (!buffor[url]) {
                buffor[url] = data
            }
        }
        return {
            get: (url, callback) => {
                const data = loadFromBuffor(url)
                if (data) {
                    callback({data: data})
                } else {
                    $http.get(url).then((response) => {
                        addToBuffor(url, response.data)
                        callback(response)
                    })
                }
            }
        }
    })
;