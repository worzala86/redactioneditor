angular.module('ngPopup', [])

    .factory('popup', ($rootScope, $compile, http, $window) => {
        let zIndex = 1000
        return (options) => {
            return {
                show: () => {
                    http.get(options.templateUrl, (response) => {
                        let scope = $rootScope.$new(true);
                        let element = $compile(response.data)(scope)
                        angular.forEach(options.scope, (value, key) => {
                            scope[key] = value
                        })
                        scope.buttons = options.buttons
                        angular.forEach(scope.buttons, (value) => {
                            value.click = () => {
                                if (value.callback) {
                                    value.callback(scope)
                                } else {
                                    scope.close()
                                }
                            }
                        })
                        scope.close = () => {
                            element.remove()
                        }
                        angular.element(document.body).append(element)
                        let left = $rootScope.cursorX
                        if (($rootScope.cursorX + element[0].offsetWidth) > document.body.clientWidth) {
                            left = document.body.clientWidth - element[0].offsetWidth - 25
                        }
                        let posY = $rootScope.cursorY - $window.pageYOffset
                        if (options.posX) {
                            left = options.posX
                        }
                        if (options.posY) {
                            posY = options.posY
                        }
                        element.css('z-index', zIndex++)
                        element.css('left', left + 'px')
                        element.css('top', posY + 'px')
                    })
                }
            }
        }
    })

    .factory('confirmPopup', (popup) => {
        return (callback, options=null) => {
            popup({
                templateUrl: templateBase + 'DeleteConfirmPopup.html',
                title: (options&&options.title)?options.title:null,
                buttons: [
                    {
                        title: (options&&options.confirmButtonText)?options.confirmButtonText:'Usuń',
                        class: 'red',
                        callback: (scope) => {
                            callback()
                            scope.close()
                        }
                    },
                    {
                        title: 'Anuluj',
                        callback: (scope) => {
                            scope.close()
                        }
                    }
                ]
            }).show()
        }
    })

    .factory('http', ($rootScope, $http) => {
        let buffor = []
        const loadFromBuffor = (url) => {
            if (!buffor[url]) {
                return null
            }
            return buffor[url]
        }
        const addToBuffor = (url, data) => {
            if (!buffor[url]) {
                buffor[url] = data
            }
        }
        return {
            get: (url, callback) => {
                const data = loadFromBuffor(url)
                if (data) {
                    callback({data: data})
                } else {
                    $http.get(url).then((response) => {
                        addToBuffor(url, response.data)
                        callback(response)
                    })
                }
            }
        }
    })
;