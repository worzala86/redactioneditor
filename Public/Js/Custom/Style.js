angular.module('ngStyle', ['color.picker'])

    .controller('stylesEditController', function ($scope, $http, $compile, dialog, $timeout, $rootScope, $location,
                                                  $window, $routeParams, loader, Upload) {
        $rootScope.cssStylePointer = {}
        $rootScope.customCssStylePointer = {}
        $scope.sentences = []
        $scope.paragraphs = []
        $scope.words = []
        $scope.floats = []
        for (let i = 0; i < 10; i++) {
            $scope.sentences.push(loremIpsum.sentences(1))
            $scope.paragraphs.push(loremIpsum.sentences(loremIpsum.random(6, 10)))
            $scope.words.push(loremIpsum.words(loremIpsum.random(1, 2)))
            $scope.floats.push((Math.random() * 100).toFixed(2))
        }
        $rootScope.changeCss = () => {
            let css = ''
            angular.forEach($rootScope.cssStyle, (value, key) => {
                angular.forEach(value, (kindValue, kindKey) => {
                    if (kindKey === 'default') {
                        kindKey = ''
                    }
                    let tagName = (key.altId ? ('#id' + key.altId) : key).split(':')
                    let suffix = ''
                    let tagsArray = []
                    tagsArray.push(tagName[0])
                    const distinct = (array) => {
                        angular.forEach(array, (value3, key3) => {
                            let exists = false
                            angular.forEach(tagsArray, (value2, key2) => {
                                if (value3 === value2) {
                                    exists = true
                                }
                            })
                            if (!exists && (value3 !== '')) {
                                tagsArray.push(value3)
                            }
                        })
                    }
                    distinct(tagName)
                    distinct(kindKey.split(':'))
                    let style = tagsArray.join(':') + '{'
                    angular.forEach(kindValue, (value2, key2) => {
                        if (!(typeof key2 == 'string')) {
                            return
                        }
                        key2 = key2.replace(/[A-Z]/, (match) => {
                            return '-' + match.toLowerCase()
                        })
                        if ((value2 !== undefined) && (value2 !== null) && (value2 !== ''))
                            style += key2 + ': ' + value2 + ';'
                    })
                    style += '}'
                    css += style
                })
            })
            if ($rootScope.cssStylePointer) {
                $rootScope.cssStylePointer.innerHTML = css
            } else {
                $timeout($rootScope.changeCss, 100)
            }
            if ($scope.scroll) {
                $scope.scroll.update()
            }
        }
        const setCustomStyleInViewFrame = (css) => {
            const frm = frames['viewFrame']
            if (!frm || !frm.document) {
                $timeout(setCustomStyleInViewFrame, 200)
                return
            }
            const document = frm.document
            let otherhead = document.getElementsByTagName("head")[0]
            if (document.querySelector('#custom-style')) {
                angular.element(document.querySelector('#custom-style')).remove()
            }
            customStyleElement = document.createElement("style")
            angular.element(customStyleElement).attr('id', 'custom-style')
            customStyleElement.innerHTML = css
            otherhead.appendChild(customStyleElement)
        }
        const setCustomStyleInViewLook = (css) => {
            const frm = frames['viewLook']
            if (!frm || !frm.document) {
                $timeout(setCustomStyleInViewLook, 200)
                return
            }
            const document = frm.document
            let otherhead = document.getElementsByTagName("head")[0]
            if (document.querySelector('#custom-style')) {
                angular.element(document.querySelector('#custom-style')).remove()
            }
            customStyleElement = document.createElement("style")
            angular.element(customStyleElement).attr('id', 'custom-style')
            customStyleElement.innerHTML = css
            otherhead.appendChild(customStyleElement)
        }
        $rootScope.setUpCustomStyleCss = (css) => {
            setCustomStyleInViewFrame(css)
            setCustomStyleInViewLook(css)
        }
        $rootScope.changeCustomCss = () => {
            let css = ''
            angular.forEach($rootScope.customCssStyle, (value, key) => {
                angular.forEach(value, (kindValue, kindKey) => {
                    if (kindKey === 'default') {
                        kindKey = ''
                    }
                    let tagName = key.split(':')
                    let suffix = ''
                    let tagsArray = []
                    tagsArray.push(tagName[0])
                    const distinct = (array) => {
                        angular.forEach(array, (value3, key3) => {
                            let exists = false
                            angular.forEach(tagsArray, (value2, key2) => {
                                if (value3 === value2) {
                                    exists = true
                                }
                            })
                            if (!exists && (value3 !== '')) {
                                tagsArray.push(value3)
                            }
                        })
                    }
                    distinct(tagName)
                    distinct(kindKey.split(':'))
                    let style = tagsArray.join(':') + '{'
                    angular.forEach(kindValue, (value2, key2) => {
                        if (!(typeof key2 == 'string')) {
                            return
                        }
                        key2 = key2.replace(/[A-Z]/, (match) => {
                            return '-' + match.toLowerCase()
                        })
                        if ((value2 !== undefined) && (value2 !== null) && (value2 !== ''))
                            style += key2 + ': ' + value2 + ';'
                    })
                    style += '}'
                    css += style
                })
            })
            $rootScope.setUpCustomStyleCss(css)
        }
        $scope.prepareStyleContanier = () => {
            let frameDocument = frames['previewStyleFrame'] ? frames['previewStyleFrame'].document : null
            if (!frameDocument) {
                frameDocument = frames['viewFrame'].document
            }
            const otherhead = frameDocument.getElementsByTagName("head")[0]
            if (!otherhead) {
                $timeout($scope.prepareStyleContanier, 100)
                return
            }
            if (!frameDocument.querySelector('#new-style')) {
                $rootScope.cssStylePointer = frameDocument.createElement("style")
                angular.element($rootScope.cssStylePointer).attr('id', 'new-style')
                otherhead.appendChild($rootScope.cssStylePointer)
            }
            const otherbody = frameDocument.getElementsByTagName("body")[0]
            if (!otherbody || (otherbody.innerHTML == '')) {
                $timeout($scope.prepareStyleContanier, 100)
                return
            }
            const buffor = otherbody.innerHTML
            otherbody.innerHTML = ''
            const compiled = $compile(buffor)($scope)
            angular.element(otherbody).append(angular.element(compiled))
            $rootScope.changeCss()
            $rootScope.changeCustomCss()
        }
        $scope.prepareStyleContanier()
        $rootScope.selectedCssElement = 'body'
        $scope.fonts = []
        $rootScope.kindSuffix = 'default'
        const makeSuffix = () => {
            let suffix = '';
            suffix += $rootScope.hover ? 'hover' : ''
            suffix += $rootScope.active ? 'active' : ''
            suffix += $rootScope.focus ? 'focus' : ''
            suffix += $rootScope.firstChild ? ((suffix !== '' ? ':' : '') + 'first-child') : ''
            suffix += $rootScope.lastChild ? ((suffix !== '' ? ':' : '') + 'last-child') : ''
            if (suffix == '') {
                suffix = 'default'
            }
            $rootScope.kindSuffix = suffix
        }
        $scope.checkboxClick = (name, value) => {
            makeSuffix()
            $rootScope.changeCss()
        }
        $scope.checkboxChildClick = (name, value) => {
            makeSuffix()
            $rootScope.changeCss()
        }
        const repairHeight = () => {
            const frame = document.querySelector('[name=previewStyleFrame]')
            if (!frame) {
                $timeout(repairHeight, 100)
            }
            angular.element(frame).css('height', ($window.innerHeight - 75 - 25) + 'px')
        }
        repairHeight()
        angular.element(window).bind('resize', () => {
            repairHeight()
        })
        $scope.pageFonts = []
        $scope.showSelectFont = (param) => {
            $http.get(apiBase + '/fonts').then((response) => {
                $scope.fonts = {
                    list: [],
                    totalCount: Math.ceil(response.data.fonts.length / 20),
                    page: 1,
                }
                let buffor = null
                let scope = {
                    fonts: $scope.fonts,
                    sentences: [],
                    paragraphs: [],
                    words: [],
                    selectFont: (font, scp, parent = true) => {
                        $rootScope.selectedFontActiveElement = font;
                        const frm = frames['previewFrame'].document
                        let otherhead = frm.getElementsByTagName("head")[0]
                        otherhead.innerHTML = ''
                        let link = frm.createElement("link")
                        angular.element(link).attr('rel', 'stylesheet')
                        angular.element(link).attr('href', font.link)
                        let style = frm.createElement("style")
                        style.innerHTML = 'body{font-family: \'' + font.name + '\', ' + font.category + ';}'
                        otherhead.appendChild(link)
                        otherhead.appendChild(style)
                        if (!buffor) {
                            let otherbody = frm.getElementsByTagName("body")[0]
                            buffor = otherbody.innerHTML
                            otherbody.innerHTML = ''
                            const compile = () => {
                                if (buffor == '') {
                                    $timeout(compile, 100)
                                    return
                                }
                                const compiled = $compile(buffor)($scope)
                                angular.element(otherbody).append(angular.element(compiled))
                            }
                            compile()
                        }
                    },
                    ready: (scp) => {
                        for (let i = 0; i < 10; i++) {
                            scope.sentences.push(loremIpsum.sentences(1))
                            scope.paragraphs.push(loremIpsum.sentences(loremIpsum.random(4, 7)))
                            scope.words.push(loremIpsum.words(loremIpsum.random(1, 2)))
                        }
                        scope.selectFont(scope.fonts.list[0], scp, false)
                    },
                    selectFontChooseClick: (font, scope) => {
                        let exists = false
                        angular.forEach($scope.pageFonts, (value, key) => {
                            if (value.name == font.name) {
                                exists = true
                            }
                        })
                        if (!exists) {
                            $scope.pageFonts.push(font)
                        }
                        let selectedElement = $rootScope.selectedCssElement
                        if ($rootScope.selectedCssElement.altId) {
                            selectedElement = '#id' + $rootScope.selectedCssElement.altId
                        }
                        if (!$rootScope.cssStyle) {
                            $rootScope.cssStyle = {}
                        }
                        if (!$rootScope.cssStyle[selectedElement]) {
                            $rootScope.cssStyle[selectedElement] = {}
                        }
                        if (!$rootScope.cssStyle[selectedElement][$rootScope.kindSuffix]) {
                            $rootScope.cssStyle[selectedElement][$rootScope.kindSuffix] = {}
                        }
                        if (!$rootScope.cssStyle[selectedElement][$rootScope.kindSuffix]['fontFamily']) {
                            $rootScope.cssStyle[selectedElement][$rootScope.kindSuffix]['fontFamily'] = ''
                        }
                        $rootScope.cssStyle[selectedElement][$scope.kindSuffix]['fontFamily'] = '\'' + font.name + '\', ' + font.category
                        if (!$rootScope.style) {
                            $rootScope.style = {}
                        }
                        $rootScope.style['fontFamily'] = '\'' + font.name + '\', ' + font.category
                        $scope.pageFontsChange()
                        scope.close()
                    },
                    prev: () => {
                        --$scope.fonts.page
                        scope.reloadPage()
                    },
                    next: () => {
                        ++$scope.fonts.page
                        scope.reloadPage()
                    },
                    reloadPage: () => {
                        scope.totalCount = response.data.fonts.length;
                        $scope.fonts.list = []
                        for (let i = (($scope.fonts.page - 1) * 20); (i < $scope.fonts.page * 20) && (($scope.fonts.page * 20) < response.data.fonts.length); i++) {
                            $scope.fonts.list.push(response.data.fonts[i])
                        }
                    }
                }
                scope.reloadPage(scope)
                dialog({
                    templateUrl: templateRoot + 'Editor/Modals/SelectFontModal.html',
                    scope: scope,
                }).show()
            })
        }
        $scope.pageFontsChange = () => {
            let frm = null
            if (frames['previewStyleFrame']) {
                frm = frames['previewStyleFrame'].document
            }
            if (!frm) {
                frm = frames['viewFrame'].document
            }
            const otherhead = frm.getElementsByTagName("head")[0]
            const oldLinks = otherhead.querySelectorAll('#font-link')
            angular.element(oldLinks).remove()
            const fontLink = otherhead.querySelectorAll('#font-link')
            angular.element(fontLink).remove()
            const fontStyle = otherhead.querySelectorAll('#font-style')
            angular.element(fontStyle).remove()
            angular.forEach($scope.pageFonts, (value, key) => {
                let link = frm.createElement("link")
                angular.element(link).attr('id', 'font-link')
                angular.element(link).attr('rel', 'stylesheet')
                angular.element(link).attr('href', value.link)
                otherhead.appendChild(link)
            })
            $scope.changeCss()
        }
        $scope.cssParams = [
            {
                name: 'Marginesy',
                items: [
                    {title: 'Padding', param: 'padding'},
                    {title: 'Padding top', param: 'paddingTop'},
                    {title: 'Padding bottom', param: 'paddingBottom'},
                    {title: 'Padding left', param: 'paddingLeft'},
                    {title: 'Padding right', param: 'paddingRight'},
                    {title: 'Margin', param: 'margin'},
                    {title: 'Margin top', param: 'marginTop'},
                    {title: 'Margin bottom', param: 'marginBottom'},
                    {title: 'Margin left', param: 'marginLeft'},
                    {title: 'Margin right', param: 'marginRight'},
                ]
            },
            {
                name: 'Rozmiary',
                items: [
                    {title: 'Width', param: 'width'},
                    {title: 'Height', param: 'height'},
                    {title: 'Min width', param: 'minWidth'},
                    {title: 'Min height', param: 'minHeight'},
                    {title: 'Max width', param: 'maxWidth'},
                    {title: 'Max height', param: 'maxHeight'},
                ]
            },
            {
                name: 'Tło',
                items: [
                    {title: 'Background', param: 'background', color: true},
                    {title: 'B. image', param: 'backgroundImage', image: true},
                    {title: 'B. size', param: 'backgroundSize'},
                    {title: 'B. position', param: 'backgroundPosition'},
                ]
            },
            {
                name: 'Czcionki',
                items: [
                    {title: 'Color', param: 'color', color: true},
                    {title: 'Font size', param: 'fontSize'},
                    {title: 'Font', param: 'fontFamily', font: true},
                    {title: 'Font weight', param: 'fontWeight'},
                    {title: 'Text align', param: 'textAlign'},
                    {title: 'Text transform', param: 'textTransform'},
                    {title: 'Text decoration', param: 'textDecoration'},
                ]
            },
            {
                name: 'Ramki',
                items: [
                    {title: 'Border', param: 'border'},
                    {title: 'Border top', param: 'borderTop'},
                    {title: 'Border bottom', param: 'borderBottom'},
                    {title: 'Border left', param: 'borderLeft'},
                    {title: 'Border right', param: 'borderRight'},
                    {title: 'Border radius', param: 'borderRadius'},
                    {title: 'Box shadow', param: 'boxShadow'},
                ]
            },
            {
                name: 'Pozycje',
                items: [
                    {title: 'Position', param: 'position'},
                    {title: 'Float', param: 'float'},
                    {title: 'Top', param: 'top'},
                    {title: 'Bottom', param: 'bottom'},
                    {title: 'Left', param: 'left'},
                    {title: 'Right', param: 'right'},
                ]
            },
            {
                name: 'Inne',
                items: [
                    {title: 'Display', param: 'display'},
                    {title: 'Box sizing', param: 'boxSizing'},
                    {title: 'Cursor', param: 'cursor'},
                    {title: 'Outline', param: 'outline'},
                    {title: 'Z index', param: 'zIndex'},
                    {title: 'Overflow', param: 'overflow'},
                    {title: 'Opacity', param: 'opacity'},
                    {title: 'Vertical align', param: 'verticalAlign'},
                    {title: 'Filter', param: 'filter'},
                    {title: 'Flex', param: 'flex'},
                ]
            },
        ]
        $scope.elements = [
            {title: 'Body', name: 'body'},
            {title: 'H1', name: 'h1'},
            {title: 'H2', name: 'h2'},
            {title: 'H3', name: 'h3'},
            {title: 'H4', name: 'h4'},
            {title: 'H5', name: 'h5'},
            {title: 'H6', name: 'h6'},
            {title: 'P', name: 'p'},
            {title: 'Button', name: 'button'},
            {title: 'Button SM', name: 'button.sm'},
            {title: 'Button XL', name: 'button.xl'},
            {title: 'A', name: 'a'},
            {title: 'Label', name: 'label'},
            {title: 'Input', name: 'input'},
            {title: 'Select', name: 'select'},
            {title: 'Textarea', name: 'textarea'},
            {title: 'View', name: 'view'},
            {title: 'View header', name: 'view-header'},
            {title: 'View header a', name: 'view-header a'},
            {title: 'View body', name: 'view-body'},
            {title: 'View body element', name: 'view-body .element'},
            {title: 'View body element lookup', name: 'view-body .element.lookup'},
            {title: 'View body center', name: 'view-body center'},
            {title: 'View body input', name: 'view-body .input'},
            {title: 'View body right sidebar', name: 'view-body.right-sidebar'},
            {title: 'View body left sidebar', name: 'view-body.left-sidebar'},
            {title: 'View body left sidebar & right sidebar', name: 'view-body.left-sidebar.right-sidebar'},
            {title: 'View footer', name: 'view-footer'},
            {title: 'Pagination', name: 'pagination'},
            {title: 'Pagination link', name: 'pagination .link'},
            {title: 'Pagination link - not important', name: 'pagination .link.not-important'},
            {title: 'Pagination link active', name: 'pagination .link.active'},
            {title: 'Pagination link unactive', name: 'pagination .link.unactive'},
            {title: 'Wrapper', name: 'wrapper'},
            {title: 'Left sidebar', name: 'left-sidebar'},
            {title: 'Right sidebar', name: 'right-sidebar'},
            {title: 'Container', name: '.container'},
            {title: 'Image responsive', name: '.image.responsive'},
            {title: 'View container', name: '.view-container'},
            {title: 'Article', name: 'article'},
            {title: 'Checkbox', name: 'input[type=checkbox]'},
            {title: 'Label checkbox', name: 'label.checkbox'},
            {title: 'Form group', name: '.form-group'},
            {title: 'Group', name: '.group'},
            {title: 'Group button', name: '.group button'},
            {title: 'Button primary', name: 'button.primary'},
            {title: 'Button secondary', name: 'button.secondary'},
            {title: 'Button alert', name: 'button.alert'},
            {title: 'Image responsive', name: 'img.responsive'},
            {title: 'Card', name: '.card'},
            {title: 'Card header', name: '.card .header'},
            {title: 'Card body', name: '.card .body'},
            {title: 'Card footer', name: '.card .footer'},
            {title: 'Card header with image', name: '.card .header.image'},
            {title: 'Card header image', name: '.card .header img'},
            {title: 'Table', name: '.table'},
            {title: 'Table header', name: '.table .header'},
            {title: 'Table body', name: '.table .body'},
            {title: 'Table body row', name: '.table .body .row'},
            {title: 'Table col', name: '.table .col'},
            {title: 'Table body row img', name: '.table .body .row img'},
            {title: 'Right', name: '.right'},
            {title: 'Panel', name: '.panel'},
            {title: 'Panel header', name: '.panel .header'},
            {title: 'Panel body', name: '.panel .body'},
            {title: 'Panel footer', name: '.panel .footer'},
            {title: 'Tabs', name: '.tabs'},
            {title: 'Tabs tab', name: '.tabs .tab'},
            {title: 'Tabs tab.active', name: '.tabs .tab.active'},
            {title: 'Menu', name: '.menu'},
            {title: 'Menu a', name: '.menu a'},
            {title: 'Menu vertical', name: '.menu.vertical'},
            {title: 'Menu vertical a', name: '.menu.vertical a'},
            {title: 'Menu vertical title', name: '.menu.vertical .title'},
            {title: 'Hide', name: '.hide'},
        ]
        $scope.colorPickerOptions = {
            required: false,
            format: 'hex',
        }
        $scope.pickerApi = {}
        $scope.activeColorParam = null
        $scope.openColor = (param) => {
            $scope.activeColorParam = param
            let selectedElement = null
            let css = 'cssStyle'
            if ($rootScope.selectedElement) {
                selectedElement = '#id' + $rootScope.selectedElement.altId
                css = 'customCssStyle'
            } else {
                selectedElement = $rootScope.selectedCssElement
            }
            if (!$rootScope[css]) {
                $rootScope[css] = {}
            }
            if (!$rootScope[css][selectedElement]) {
                $rootScope[css][selectedElement] = {}
            }
            if (!$rootScope[css][selectedElement][$rootScope.kindSuffix]) {
                $rootScope[css][selectedElement][$rootScope.kindSuffix] = ''
            }
            if (!$rootScope[css][selectedElement][$rootScope.kindSuffix][$scope.activeColorParam]) {
                $rootScope[css][selectedElement][$rootScope.kindSuffix][$scope.activeColorParam] = {}
            }
            if (typeof $rootScope[css][selectedElement][$rootScope.kindSuffix][$scope.activeColorParam] == 'string') {
                $scope.color = $rootScope[css][selectedElement][$rootScope.kindSuffix][$scope.activeColorParam].substring(1)
            } else {
                $scope.color = '888';
            }
            $scope.pickerApi[param].open()
        }
        $scope.pickerEventApi = {}
        $scope.pickerEventApi.onChange = (api, color, $event) => {
            let selectedElement = null
            let css = 'cssStyle'
            if ($rootScope.selectedElement) {
                selectedElement = '#id' + $rootScope.selectedElement.altId
                css = 'customCssStyle'
            } else {
                selectedElement = $rootScope.selectedCssElement
            }
            if (!$rootScope[css][selectedElement]) {
                $rootScope[css][selectedElement] = {}
            }
            if (!$rootScope[css][selectedElement][$rootScope.kindSuffix]) {
                $rootScope[css][selectedElement][$rootScope.kindSuffix] = {}
            }
            if (!$rootScope[css][selectedElement][$rootScope.kindSuffix][$scope.activeColorParam]) {
                $rootScope[css][selectedElement][$rootScope.kindSuffix][$scope.activeColorParam] = ''
            }
            $rootScope[css][selectedElement][$rootScope.kindSuffix][$scope.activeColorParam] = '#' + color
            if (css == 'cssStyle') {
                $rootScope.changeCss()
            } else {
                $rootScope.changeCustomCss()
            }
        }
        $scope.saveStyleWithImage = (callback) => {
            loader.show()
            angular.element(document.querySelector("view")).css('min-width', '1000px')
            const body = frames['previewStyleFrame'] ? frames['previewStyleFrame'].document.body : null
            angular.element(body.querySelector('url')).css({display: 'none'})
            const saveImage = () => {
                html2canvas(body).then(canvas => {
                    const upload = function (file) {
                        let dataUrl = file
                        const name = 'screen-' + $routeParams.styleId + '.png'
                        const type = 'image/png'
                        const base64Data = dataUrl.substr(dataUrl.indexOf('base64,') + 'base64,'.length)
                        Upload.upload({
                            url: apiBase + '/files',
                            data: {
                                files: [{
                                    name: name,
                                    type: type,
                                    file: base64Data,
                                }],
                                target: 'Styles',
                            }
                        }).success(function (resp) {
                            $scope.saveStyle(resp.files[0].id, callback)
                        }).error(function (resp) {
                        }).progress(function (evt) {
                        }).xhr(function (e, xhr) {
                            $scope.xhr = xhr
                        })
                    }
                    const data = canvas.toDataURL("image/png")
                    upload(data)
                })
            }
            $rootScope.resizeView()
            saveImage()
        }
        $scope.saveStyle = (imageId, callback) => {
            let css = []
            let fonts = []
            angular.forEach($rootScope.cssStyle, (value, key) => {
                angular.forEach(value, (kindValue, kindKey) => {
                    let kindKeyString = (kindKey !== 'default' ? kindKey : '').split(':')
                    let keyArray = key.split(':')
                    let newTag = []
                    const distinct = (array) => {
                        angular.forEach(array, (value, key) => {
                            let find = false
                            angular.forEach(newTag, (value2, key2) => {
                                if (value2 == value) {
                                    find = true
                                }
                            })
                            if (!find && value && (value !== '')) {
                                newTag.push(value)
                            }
                        })
                    }
                    distinct(keyArray)
                    distinct(kindKeyString)
                    let tagRaw = newTag.join(':')
                    let style = {
                        tag: tagRaw,
                        css: [],
                    }
                    angular.forEach(kindValue, (value2, key2) => {
                        key2 = key2.replace(/[A-Z]/, (match) => {
                            return '-' + match.toLowerCase()
                        })
                        style.css.push({
                            parameter: key2,
                            value: value2
                        })
                        if (key2 == 'font-family') {
                            let name = value2.split('\'')
                            name = name[1]
                            fonts.push(name)
                        }
                    })
                    css.push(style)
                })
            })
            let fontsToSave = []
            angular.forEach($scope.pageFonts, (value, key) => {
                angular.forEach(fonts, (value2, key2) => {
                    if (value.name == value2) {
                        let exists = false
                        angular.forEach(fontsToSave, (value3, key3) => {
                            if (value3.name == value.name) {
                                exists = true
                            }
                        })
                        if (!exists) {
                            fontsToSave.push({
                                name: value.name,
                                category: value.category,
                                link: value.link,
                            })
                        }
                    }
                })
            })
            $http.post(apiBase + '/styles/' + $routeParams.styleId + '/css', {
                css: css,
                fonts: fontsToSave,
                imageId: imageId
            }).then((response) => {
                if (callback) {
                    callback()
                }
                loader.hide()
            })
        }
        if ($routeParams.styleId) {
            $http.get(apiBase + '/styles/' + $routeParams.styleId + '/css').then((response) => {
                const css = response.data.css
                $scope.pageFonts = response.data.fonts
                angular.forEach(css, (value, key) => {
                    const tagName = value.tag
                    let suffix = 'default'
                    const tag = tagName.split(':')
                    if (tag[1]) {
                        suffix = tag[1]
                    }
                    if (tag[2]) {
                        suffix += ':' + tag[2]
                    }
                    angular.forEach(value.css, (value2, key2) => {
                        let parameterName = value2.parameter.replace(/-[a-z]/, (match) => {
                            return match.substring(1).toUpperCase()
                        })
                        if (!$rootScope.cssStyle) {
                            $rootScope.cssStyle = {}
                        }
                        if (!$rootScope.cssStyle[tagName]) {
                            $rootScope.cssStyle[tagName] = {}
                        }
                        if (!$rootScope.cssStyle[tagName][suffix]) {
                            $rootScope.cssStyle[tagName][suffix] = {}
                        }
                        if (!$rootScope.cssStyle[tagName][suffix][parameterName]) {
                            $rootScope.cssStyle[tagName][suffix][parameterName] = ''
                        }
                        $rootScope.cssStyle[tagName][suffix][parameterName] = value2.value
                    })
                })
                loader.hide()
            })
        } else {
            loader.hide()
        }
        /*const timeoutCallback = () => {
            if (!frames || !frames['viewFrame'] || !frames['viewFrame'].document ||
                !frames['viewFrame'].document.body) {
                $timeout(timeoutCallback, 250)
                return
            }
            $scope.pageFontsChange()
            $rootScope.changeCss()
        }
        $timeout(timeoutCallback, 250)*/
        $scope.select = (event, elementName) => {
            event.stopPropagation()
            $rootScope.selectedCssElement = elementName
        }
        $scope.closeStyle = () => {
            $scope.saveStyleWithImage(() => {
                $location.path(base + '/style')
            })
        }
        const createScroll = () => {
            const scroll = document.querySelector('.scroll-area')
            if (!scroll) {
                $timeout(createScroll, 100)
                return
            }
            $scope.scroll = new PerfectScrollbar(scroll);
            $scope.scroll.update()
        }
        createScroll()
        $rootScope.selectedElement = null
        let customImages = []
        $scope.files = {}
        let imageTarget = null
        $scope.selectImageSource = (css, a, b, c) => {
            imageTarget = {
                css: css,
                a: a,
                b: b,
                c: c,
            }
            $http.get(apiBase + '/images/custom?limit=20&page=1').then((customResponse) => {
                //$http.get(apiBase + '/images/system?limit=20&page=1').then((systemResponse) => {
                customImages = customResponse.data.images
                if (!customImages) {
                    customImages = []
                }
                //$rootScope.systemImages = systemResponse.data.images
                //$rootScope.systemImagesTotalCount = systemResponse.data.totalCount
                $rootScope.customImages = customImages
                $rootScope.customImagesTotalCount = customResponse.data.totalCount
                dialog({
                    templateUrl: templateRoot + 'Modals/SelectImageModal.html',
                    scope: {
                        drop: $scope.drop,
                        select: $scope.select,
                        submit: $scope.submit,
                        files: $scope.files,
                        upload: $scope.upload,
                        uploading: false,
                        selectImage: $scope.selectImage,
                        deleteImage: $scope.deleteImage,
                        searchImages: $scope.searchImages,
                        //systemImagesNextPage: $scope.systemImagesNextPage,
                        //systemImagesPrevPage: $scope.systemImagesPrevPage,
                        customImagesNextPage: $scope.customImagesNextPage,
                        customImagesPrevPage: $scope.customImagesPrevPage,
                        searchImagesNextPage: $scope.searchImagesNextPage,
                        selectSearchedImage: $scope.selectSearchedImage,
                        ceil: (number) => {
                            return Math.ceil(number)
                        }
                    }
                }).show()
                //})
            })
        }
        $scope.searchImages = (searchKey) => {
            $http.post(apiBase + '/images/search', {key: searchKey, page: 1}).then((response) => {
                $rootScope.searchedImages = response.data.images
                $rootScope.searchedImagesPage = 1
            })
        }
        /*$rootScope.systemImagesPage = 1
        $scope.systemImagesPrevPage = () => {
            $rootScope.systemImagesPage--
            $http.get(apiBase + '/images/system?limit=20&page=' + $rootScope.systemImagesPage).then((response) => {
                $rootScope.systemImages = response.data.images
            })
        }
        $scope.systemImagesNextPage = () => {
            $rootScope.systemImagesPage++
            $http.get(apiBase + '/images/system?limit=20&page=' + $rootScope.systemImagesPage).then((response) => {
                $rootScope.systemImages = response.data.images
            })
        }*/
        $rootScope.customImagesPage = 1
        $scope.customImagesPrevPage = () => {
            $rootScope.customImagesPage--
            $http.get(apiBase + '/images/custom?limit=20&page=' + $rootScope.customImagesPage).then((response) => {
                $rootScope.customImages = response.data.images
            })
        }
        $scope.customImagesNextPage = () => {
            $rootScope.customImagesPage++
            $http.get(apiBase + '/images/custom?limit=20&page=' + $rootScope.customImagesPage).then((response) => {
                $rootScope.customImages = response.data.images
            })
        }
        $scope.searchImagesNextPage = (searchKey) => {
            $rootScope.searchedImagesPage++
            $http.post(apiBase + '/images/search', {
                key: searchKey,
                page: $rootScope.searchedImagesPage
            }).then((response) => {
                $rootScope.searchedImages = response.data.images
            })
        }
        $scope.selectSearchedImage = (element, id) => {
            if (!element) {
                element = $scope.selectedElement
            }
            loader.show()
            $http.get(apiBase + '/images/' + id + '/download').then((response) => {
                if (response.data.url) {
                    if (element.src) {
                        $rootScope.dataSet[element.elementPositionId][element.src] = response.data.url
                    } else {
                        const tag = '#id-' + element.elementPositionId + '-' + (element.altId ? element.altId : element.id)
                        if (!$rootScope.customCssStyle[tag]) {
                            $rootScope.customCssStyle[tag] = {}
                        }
                        if (!$rootScope.customCssStyle[tag]['default']) {
                            $rootScope.customCssStyle[tag]['default'] = {}
                        }
                        $rootScope.customCssStyle[tag]['default']['backgroundImage'] = 'url(/pliki/' + response.data.id + '/' + response.data.name + ')'
                    }
                    $rootScope.changeCustomCss()
                    loader.hide()
                }
            })
        }
        $scope.selectImage = (element, image, target) => {
            if (!$rootScope[imageTarget.css]) {
                $rootScope[imageTarget.css] = {}
            }
            if (!$rootScope[imageTarget.css][imageTarget.a]) {
                $rootScope[imageTarget.css][imageTarget.a] = {}
            }
            if (!$rootScope[imageTarget.css][imageTarget.a][imageTarget.b]) {
                $rootScope[imageTarget.css][imageTarget.a][imageTarget.b] = {}
            }
            if (!$rootScope[imageTarget.css][imageTarget.a][imageTarget.b][imageTarget.c]) {
                $rootScope[imageTarget.css][imageTarget.a][imageTarget.b][imageTarget.c] = ''
            }
            $rootScope[imageTarget.css][imageTarget.a][imageTarget.b][imageTarget.c] = 'url(/pliki/' + image.id + '/' + image.name + ')'
            $rootScope.changeCustomCss()
        }
        $scope.deleteImage = (element, images, image) => {
            $http.delete(apiBase + '/images/' + image.id).then((response) => {
                let index = null
                angular.forEach(images, (value, key) => {
                    if (value.id == image.id) {
                        index = key
                    }
                })
                if (index != null) {
                    images.splice(index, 1)
                }
            })
        }
        $scope.abort = function (event, files) {
            files.uploading = false
            if ($scope.xhr) {
                $scope.xhr.abort()
            }
            event ? event.stopPropagation() : {}
        }
        $scope.drop = function () {
            $scope.submit()
        }
        $scope.selectImg = function (event, files) {
            $scope.submit(files)
            event.stopPropagation()
        }
        $scope.submit = function (files) {
            angular.forEach(files, (value, key) => {
                $scope.upload(files, value)
            })
        }
        $scope.upload = function (filesCnt, file) {
            var files = [];
            files.push(file)
            var fileReader = new FileReader()
            fileReader.readAsDataURL(file)
            fileReader.onload = function (e) {
                var dataUrl = e.target.result
                var name = file.name
                var type = file.type
                var base64Data = dataUrl.substr(dataUrl.indexOf('base64,') + 'base64,'.length)
                filesCnt.uploading = true
                Upload.upload({
                    url: apiBase + '/files',
                    data: {
                        files: [{
                            name: name,
                            type: type,
                            file: base64Data,
                        }],
                        target: 'Images',
                    }
                }).success(function (resp) {
                    $http.put(apiBase + '/images/' + resp.files[0].id).then((response) => {
                        customImages.unshift(resp.files[0])
                    })
                    $scope.abort(null, filesCnt)
                }).error(function (resp) {
                }).progress(function (evt) {
                }).xhr(function (e, xhr) {
                    $scope.xhr = xhr
                })
            }
        }
    })