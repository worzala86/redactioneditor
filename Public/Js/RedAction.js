var base = '';
var apiBase = '/api';
var templateBase = '/Public/Templates/';

angular.module('RedAction', ['ngRoute', 'ngSanitize'])

    .config(function ($routeProvider, $locationProvider, $compileProvider, $httpProvider) {
        $routeProvider
            .when(base + '/', {
                templateUrl: templateBase + 'LandingPage.html',
                controller: 'landingPageController',
            })
            .when(base + '/logowanie', {
                templateUrl: templateBase + 'User/Login.html',
                controller: 'loginController',
            })
            .when(base + '/rejestracja', {
                templateUrl: templateBase + 'User/Register.html',
                controller: 'registerController',
            })
            .when(base + '/potwierdzenie-rejestracji', {
                templateUrl: templateBase + 'User/AfterRegister.html',
                controller: 'afterRegisterController',
            })
            .when(base + '/aktywacja/:code', {
                templateUrl: templateBase + 'User/Activate.html',
                controller: 'activateController',
            })
            .when(base + '/reset-hasla', {
                templateUrl: templateBase + 'User/Reset.html',
                controller: 'resetController',
            })
            .when(base + '/reset-hasla-wyslano', {
                templateUrl: templateBase + 'User/AfterReset.html',
                controller: 'afterResetController',
            })
            .when(base + '/nowe-haslo/:code', {
                templateUrl: templateBase + 'User/NewPassword.html',
                controller: 'newPasswordController',
            })
            .when(base + '/zmieniono-haslo', {
                templateUrl: templateBase + 'User/PasswordChanged.html',
                controller: 'passwordChangedController',
            })
            .when(base + '/:url', {
                templateUrl: templateBase + 'CrmPage.html',
                controller: 'crmPageController',
            })
        ;

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $compileProvider.debugInfoEnabled(true)
        delete $httpProvider.defaults.headers.common['X-Requested-With']
        $httpProvider.defaults.useXDomain = true
        $httpProvider.defaults.withCredentials = true
    })

    .run(function ($rootScope, $http, $location, $route, $compile) {
        $rootScope.baseUrl = base
        $rootScope.user = {
            logged: false,
        }
        let path = null
        $rootScope.logout = function () {
            $http.get(apiBase + '/logout').then(function (response) {
                if (response.data.success) {
                    $rootScope.user.logged = false
                    window.location = '/'
                }
            });
        }
        $rootScope.loadUserStatus = () => {
            $http.get(apiBase + '/user-status').then(function (response) {
                $rootScope.user = {
                    logged: response.data.logged,
                    admin: response.data.isAdmin,
                    projectsCount: response.data.projectsCount,
                }
                if (!$rootScope.user.logged && (window.location.pathname != '/logowanie')) {
                }
            })
        }
        $rootScope.loadUserStatus()
        $rootScope.$on('$routeChangeStart', function ($event, next, current) {
        });
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
        })
        $rootScope.cursorX;
        $rootScope.cursorY;
        document.onmousemove = function (e) {
            $rootScope.cursorX = e.pageX;
            $rootScope.cursorY = e.pageY;
        }
        const calculateGrid = () => {
            $rootScope.$apply(() => {
                const width = document.body.clientWidth
                let gridStyle = 'xs'
                if (width >= 500) gridStyle = 'sm'
                if (width >= 700) gridStyle = 'md'
                if (width >= 900) gridStyle = 'lg'
                if (width >= 1100) gridStyle = 'xl'
                $rootScope.gridStyle = gridStyle
            })
        }
        angular.element(window).bind('resize', () => {
            calculateGrid()
        })
        calculateGrid()
    })

    .controller('crmPageController', function ($scope, $location, $http, $rootScope, $routeParams, $sanitize) {
        $http.post(apiBase + '/crm-page', {url: '/' + $routeParams.url}).then((response) => {
            response.data.content = $sanitize(response.data.content)
            $scope.page = response.data
        })
    })

    .controller('loginController', function ($scope, $location, $http, $rootScope) {
        $scope.send = function () {
            $http.post(apiBase + '/login', {
                mail: $scope.mail,
                password: sha512($scope.password),
            }).then(function (response) {
                if (response.data.redirect) {
                    $rootScope.user.logged = true
                    $scope.message = ''
                    $rootScope.loadUserStatus()
                    window.location = response.data.redirect
                } else {
                    $scope.message = 'Błędny adres mailowy lub hasło'
                }
            });
        }
    })

    .controller('registerController', function ($scope, $location, $http) {
        $scope.send = function () {
            if ($scope.terms !== true) {
                $scope.message = 'Musisz zaakceptować regulamin'
                return
            } else {
                $scope.message = ''
            }
            if ($scope.personalData !== true) {
                $scope.message = 'Musisz wyrazić zgodę na przetwarzanie danych osobowych'
                return
            } else {
                $scope.message = ''
            }
            if (!$scope.mail) {
                $scope.message = 'Wprowadź adres mailowy'
                return
            } else if (!$scope.mail.match(/[a-zA-Z0-9.]+@[a-zA-Z0-9.]+.[a-zA-Z0-9.]+/)) {
                $scope.message = 'Wprowadź prawidłowy adres mailowy'
            } else {
                $scope.message = ''
            }
            if (!$scope.firstName) {
                $scope.message = 'Musisz wprowadzić imię'
                return
            } else {
                $scope.message = ''
            }
            if (!$scope.lastName) {
                $scope.message = 'Musisz wprowadzić nazwisko'
                return
            } else {
                $scope.message = ''
            }
            if (!$scope.password) {
                $scope.message = 'Musisz wprowadzić hasło'
                return
            } else {
                $scope.message = ''
            }
            if (!$scope.repeatedPassword) {
                $scope.message = 'Musisz powtórzyć hasło'
                return
            } else {
                $scope.message = ''
            }
            if ($scope.password !== $scope.repeatedPassword) {
                $scope.message = 'Musisz wprowadzić takie samo hasło'
                return
            } else {
                $scope.message = ''
            }
            $http.post(apiBase + '/register', {
                mail: $scope.mail,
                password: sha512($scope.password),
                firstName: $scope.firstName,
                lastName: $scope.lastName,
                newsletter: $scope.newsletter ? true : false,
            }).then(function (response) {
                if (response.data.error) {
                    $scope.message = response.data.error.message
                }
                if (response.data.success) {
                    $scope.message = ''
                    $location.path('/potwierdzenie-rejestracji');
                }
            });
        }
    })

    .controller('afterRegisterController', function ($scope) {
    })

    .controller('activateController', function ($scope, $http, $routeParams) {
        $scope.success = false
        $scope.accountIsActive = false
        $http.get(apiBase + '/activate-account/' + $routeParams.code).then((response) => {
            $scope.success = response.data.success
            $scope.accountIsActive = response.data.accountIsActive
        })
    })

    .controller('landingPageController', function ($scope, $location, $http, $rootScope) {
        $http.get(apiBase + '/projects/public/main').then((response) => {
            $scope.projects = response.data.projects
        })
    })

    .controller('resetController', function ($scope, $location, $http, $rootScope) {
        $scope.send = () => {
            if ($scope.mail) {
                $http.post(apiBase + '/reset-password', {mail: $scope.mail}).then((response) => {
                    if (response.data.success) {
                        $scope.message = ''
                        $location.path(base + '/reset-hasla-wyslano')
                    } else {
                        $scope.message = 'Wprowadź prawidłowy email'
                    }
                })
            } else {
                $scope.message = 'Wprowadź adres email'
            }
        }
    })

    .controller('afterResetController', function ($scope) {
    })

    .controller('passwordChangedController', function ($scope) {
    })

    .controller('newPasswordController', function ($scope, $http, $routeParams, $location) {
        $scope.send = () => {
            if (!$scope.password) {
                $scope.message = 'Wprowadz hasło do serwisu';
                return
            } else {
                $scope.message = '';
            }
            if (!$scope.repeatedPassword) {
                $scope.message = 'Powtórz hasło';
                return
            } else {
                $scope.message = '';
            }
            if ($scope.password != $scope.repeatedPassword) {
                $scope.message = 'Hasło musi się powtarzać';
                return
            } else {
                $scope.message = '';
            }
            $http.post(apiBase + '/new-password', {
                password: sha512($scope.password),
                code: $routeParams.code
            }).then((response) => {
                if (response.data.success) {
                    $scope.message = ''
                    $location.path(base + '/zmieniono-haslo')
                } else {
                    $scope.message = 'Nie zapisano hasła. Sprawdz czy masz poprawny link'
                }
            })
        }
    })

    .directive('imageScroll', () => {
        return {
            link: function (scope, element, attrs) {
                let offsetY = 0
                element.css('position', 'relative')
                element.css('overflow', 'hidden')
                element.find('img').css('position', 'absolute')
                element.find('img').css('top', '0px')
                element.bind('mousewheel', function (e) {
                    let prevent = false
                    if (e.deltaY < 0) {
                        offsetY += 30
                    } else {
                        offsetY -= 30
                    }
                    if (offsetY > 0) {
                        offsetY = 0
                        prevent = true
                    }
                    if (offsetY < -(element.find('img')[0].offsetHeight - element[0].offsetHeight)) {
                        offsetY = -(element.find('img')[0].offsetHeight - element[0].offsetHeight)
                        prevent = true
                    }
                    if (element.find('img')[0].offsetHeight < element[0].offsetHeight) {
                        offsetY = 0
                        prevent = true
                    }
                    angular.element(element).find('img').css('top', offsetY + 'px')
                    if (!prevent) {
                        e.stopPropagation()
                        e.preventDefault()
                    }
                })
            }
        }
    })
;