angular.module('ngElementMenuPopup', ['ngModal'])

    .controller('elementMenuPopupController', function ($scope, $rootScope, $timeout, dialog, Upload, loader, $http,
                                                        confirmPopup) {
        const frame = document.querySelector('#viewFrame')
        const parentScope = angular.element(frame).scope()
        $scope.selectStyleElement = (event, rebuildScrolls = true, element) => {
            $rootScope.leftPanel = 'styles'
            $timeout(() => {
                parentScope.change(event, element)
            }, 1000)
            $rootScope.rebuildPanelLeftScrollbar()
        }
        const searchParentElement = (element, elements) => {
            let parentElement = null
            if (!elements || !elements.childrens) {
                return
            }
            angular.forEach(elements.childrens, (value, key) => {
                let result = null
                if (value.id == element.id) {
                    parentElement = elements
                }
                if (result && !parentElement) {
                    parentElement = elements
                }
                result = searchParentElement(element, value)
                if (result && !parentElement) {
                    parentElement = result
                }
            })
            return parentElement
        }
        $scope.selectParent = (event) => {
            const element = $rootScope.selectedElement
            const setSelectedElement = (element, elements) => {
                if ((parentElement = searchParentElement(element, elements)) && parentElement
                    && (parentElement !== parentScope.viewBody)) {
                    $rootScope.selectedElement = parentElement
                    $rootScope.elementToAddNewElement = $rootScope.selectedElement
                    $rootScope.style = $rootScope.selectedElement.style
                    return true
                }
                return false
            }
            if (!(setSelectedElement(element, parentScope.viewBody))) {
                $rootScope.selectedElement = null
                $rootScope.elementToAddNewElement = null
                angular.element(document.querySelector('#element-menu-popup')).remove()
            }
        }
        const searchElements = (node, elem) => {
            let elements = null
            let index = null
            const searchElementsAndIndex = (n, element) => {
                angular.forEach(n.childrens, (value, key) => {
                    if (elem.id == value.id) {
                        elements = n.childrens
                        index = key
                    }
                    if (value.childrens) {
                        searchElementsAndIndex(value, element)
                    }
                })
                return elements
            }
            searchElementsAndIndex(node, elem)
            return {
                elements: elements,
                index: index,
            }
        }
        const moveInArray = (elements, element, index, direction = 'up') => {
            if (((direction == 'up') && (elements && index > 0)) || ((direction == 'down') && (elements && index < elements.length))) {
                elements.splice(index, 1)
                elements.splice(((direction == 'up') ? (index - 1) : (index + 1)), 0, element)
                $timeout(() => {
                    $rootScope.selectedElement = element
                }, 1)
            }
        }
        $scope.moveUp = (event) => {
            const element = $rootScope.selectedElement
            let result = searchElements(parentScope.viewBody, element)
            moveInArray(result.elements, element, result.index, 'up')
            $timeout(() => {
                parentScope.showElementMenuPopup(event, element)
            }, 1)
        }
        $scope.moveDown = (event) => {
            const element = $rootScope.selectedElement
            let result = searchElements(parentScope.viewBody, element)
            moveInArray(result.elements, element, result.index, 'down')
            $timeout(() => {
                parentScope.showElementMenuPopup(event, element)
            }, 1)
        }
        const elementExistsInElement = (find, element) => {
            let result = false
            if (element.childrens) {
                angular.forEach(element.childrens, (value, key) => {
                    result = result || elementExistsInElement(find, value)
                })
            }
            result = result || (find && (element.id == find.id))
            return result
        }
        $scope.setDataSetted = (element)=>{
            element.dataSetActive = !element.dataSetActive
        }
        $scope.setBackground = (element)=>{
            element.backgroundActive = !element.backgroundActive
        }
    })