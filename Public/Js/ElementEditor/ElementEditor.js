angular.module('ngRedActionElementEditor', ['ngElementMenuPopup'])

    .controller('elementEditorController', function ($scope, dialog, $rootScope, $http, $sanitize, $routeParams, $timeout,
                                                     Upload, popup, confirmPopup, $location, $compile, loader, $sce) {
        $rootScope.tags = ''
        $rootScope.dataSet = []
        loader.show()
        $rootScope.pageId = 1
        $rootScope.paginationLimit = 10;
        const getId = (length = 8) => {
            var text = "";
            var possible = "abcdef0123456789";
            for (var i = 0; i < length; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }
        $scope.viewBody = {id: getId(), type: 'container'}
        const getParentElementPositionId = (element) => {
            let elementPositionId = null
            if (element.elementPositionId) {
                elementPositionId = element.elementPositionId
            }
            angular.forEach(element.childrens, (value, key) => {
                const sel = getParentElementPositionId(value)
                if (sel) {
                    elementPositionId = sel
                }
            })
            return elementPositionId
        }
        const addNewElement = (element, type, index) => {
            if (!element) {
                return
            }
            if (!element.childrens) {
                element.childrens = []
            }
            let scp = {}
            scp.type = type
            scp.show = true
            scp.id = getId()
            scp.altId = getId()
            scp.field = getId()
            scp.elementPositionId = getParentElementPositionId(element)
            let defaultText = loremIpsum.sentences(1)
            if ((type == 'paragraph') || (type == 'article')) {
                defaultText = loremIpsum.sentences(loremIpsum.random(6, 10))
            } else if (type == 'image') {
                defaultText = undefined
            } else if (type == 'pagination') {
                defaultText = undefined
            } else if (type == 'container') {
                defaultText = undefined
            } else if (type == 'link') {
                defaultText = loremIpsum.words(1)
            } else if (type == 'button') {
                defaultText = loremIpsum.words(1)
            } else if (type == 'input') {
                defaultText = loremIpsum.words(loremIpsum.random(1, 3))
            }

            if (type == 'container') {
                scp.style = {}
                scp.style.padding = '20px'
                scp.style.display = 'block'
                scp.style.position = 'relative'
                scp.style.boxSizing = 'border-box'
                scp.style.float = 'left'
                scp.style.border = '1px dashed #aaa'
                scp.class = 'col col-xs-12'
                delete (scp.altId)
            } else if (type == 'link') {
                scp.style = {}
                scp.style.display = 'inline-block'
                scp.style.margin = '0px 5px'
                scp.style.cursor = 'pointer'
                scp.style.outline = 'none'
                scp.class = 'link'
            } else if (type == 'pagination') {
                scp.style = {}
                scp.style.display = 'block'
                scp.style.textAlign = 'center'
            } else if (type == 'image') {
                scp.style = {}
                scp.src = getId()
                scp.picTitle = getId()
                scp.alt = getId()
                scp.style.minWidth = '25px'
                scp.style.minHeight = '25px'
                scp.style.background = '#ccc'
                scp.class = 'image responsive'
                $rootScope.dataSet[scp.src] = '/Files/default.jpg'
            } else if (type == 'input') {
                scp.class = 'input'
                scp.style = {}
                scp.style.padding = '3px 8px'
                scp.style.border = '1px solid #ccc'
                scp.placeholder = getId()
                $rootScope.dataSet[scp.placeholder] = defaultText
                defaultText = null
            } else if (type == 'label') {
                scp.class = 'label'
                scp.style = {}
            } else if (type == 'textarea') {
                scp.class = 'input'
                scp.style = {}
            } else if (type == 'html') {
                scp.html = ''
                scp.style = {}
                scp.style.padding = '20px'
                scp.style.border = '1px dashed #aaa'
            }

            if (index) {
                element.childrens.splice(index, 0, scp)
            } else {
                element.childrens.push(scp)
            }

            if (defaultText) {
                if (!$rootScope.dataSet) {
                    $rootScope.dataSet = {}
                }
                if (!$rootScope.dataSet[scp.elementPositionId]) {
                    $rootScope.dataSet[scp.elementPositionId] = {}
                }
                if (!$rootScope.dataSet[scp.elementPositionId][scp.field]) {
                    $rootScope.dataSet[scp.elementPositionId][scp.field] = {}
                }
                $rootScope.dataSet[scp.elementPositionId][scp.field] = defaultText
            }
        }
        $scope.copyFrom = (scope) => {
            let newObject = {
                type: scope.type,
                id: scope.id,
                selector: scope.selector,
                field: scope.field,
                src: scope.src,
                altId: scope.altId,
                alt: scope.alt,
                picTitle: scope.picTitle,
                href: scope.href,
                target: scope.target,
                class: scope.class,
                placeholder: scope.placeholder,
                html: scope.html,
                elementId: scope.elementId,
                elementPositionId: scope.elementPositionId,
                show: scope.show,
                dataSetActive: scope.dataSetActive,
                backgroundActive: scope.backgroundActive,
            }
            if (scope.childrens) {
                newObject.childrens = []
                angular.forEach(scope.childrens, (value, key) => {
                    newObject.childrens[key] = $scope.copyFrom(value)
                })
            }
            return newObject
        }
        const saveImage = (callback) => {
            const body = frames['viewFrame'] ? frames['viewFrame'].document.body : null
            html2canvas(body).then(canvas => {
                const upload = function (file) {
                    let dataUrl = file
                    const name = 'screen-' + $scope.elementId + '.png'
                    const type = 'image/png'
                    const base64Data = dataUrl.substr(dataUrl.indexOf('base64,') + 'base64,'.length)
                    Upload.upload({
                        url: apiBase + '/files',
                        data: {
                            files: [{
                                name: name,
                                type: type,
                                file: base64Data,
                            }],
                            target: 'Elements',
                        }
                    }).success(function (resp) {
                        if (resp.files) {
                            $http.post(apiBase + '/elements/' + $scope.elementId + '/images/' + resp.files[0].id).then((res) => {
                                callback()
                            })
                        }
                    }).error(function (resp) {
                    }).progress(function (evt) {
                    }).xhr(function (e, xhr) {
                    })
                }
                const data = canvas.toDataURL("image/png")
                upload(data)
            })
        }
        const saveElement = (callback) => {
            loader.show()
            let content = $scope.copyFrom($scope.viewBody.childrens[0])
            let dataSet = {}
            angular.forEach(Object.assign({}, $rootScope.dataSet), (value, key) => {
                angular.forEach(Object.assign({}, value), (value2, key2) => {
                    if(!dataSet[key]){
                        dataSet[key] = {}
                    }
                    dataSet[key][key2] = value2
                })
            })
            content.show = true
            const data = {
                content: content,
                rootStyleId: $rootScope.styleId,
                dataSet: dataSet,
            }
            $http.put(apiBase + '/elements/' + $scope.elementId, data).then((response) => {
                $http.post(apiBase + '/elements/' + $scope.elementId + '/tags', {tags: $rootScope.tags}).then((response) => {
                })
                saveImage(callback)
            })
        }
        $rootScope.deleteElement = (event, element) => {
            confirmPopup(() => {
                const deleteInElements = (elem) => {
                    let index = null
                    angular.forEach(elem.childrens, (value, key) => {
                        if (value == element) {
                            index = key
                        }
                        deleteInElements(value)
                    })
                    if (index != null) {
                        delete ($rootScope.dataSet[element.id])
                        elem.childrens.splice(index, 1)
                    }
                }
                deleteInElements($scope.viewBody)
                $rootScope.selectedElement = null
                $rootScope.elementToAddNewElement = null
            })
        }
        const loadAllComponents = () => {
            $http.get(apiBase + '/components').then((response) => {
                $scope.components = response.data.components
            })
        }
        loadAllComponents()
        $scope.addNewComponentToSelectedElement = (event, component) => {
            event.stopPropagation()
            addNewElement($rootScope.selectedElement, component.key, $rootScope.selectedElementIndex)
        }
        $scope.addNewElementToSelectedElement = (event, element) => {
            event.stopPropagation()
            $scope.loadViewElement({
                id: element ? element.id : $scope.sourceElement.id,
                element: $rootScope.selectedElement,
                index: $rootScope.selectedElementIndex,
            })
        }
        const setStyleInViewFrame = (css) => {
            const frm = frames['viewFrame']
            if (!frm || !frm.document) {
                $timeout(setStyleInViewFrame, 200)
                return
            }
            const document = frm.document
            let otherhead = document.getElementsByTagName("head")[0]
            if (document.querySelector('#new-style')) {
                angular.element(document.querySelector('#new-style')).remove()
            }
            styleElement = document.createElement("style")
            angular.element(styleElement).attr('id', 'new-style')
            styleElement.innerHTML = css
            otherhead.appendChild(styleElement)
        }
        const setStyleInViewLook = (css) => {
            const frm = frames['viewLook']
            if (!frm || !frm.document) {
                $timeout(setStyleInViewLook, 200)
                return
            }
            const document = frm.document
            let otherhead = document.getElementsByTagName("head")[0]
            if (document.querySelector('#new-style')) {
                angular.element(document.querySelector('#new-style')).remove()
            }
            styleElement = document.createElement("style")
            angular.element(styleElement).attr('id', 'new-style')
            styleElement.innerHTML = css
            otherhead.appendChild(styleElement)
        }
        const setCustomStyleInViewFrame = (css) => {
            const frm = frames['viewFrame']
            if (!frm || !frm.document) {
                $timeout(setCustomStyleInViewFrame, 200)
                return
            }
            const document = frm.document
            let otherhead = document.getElementsByTagName("head")[0]
            if (document.querySelector('#custom-style')) {
                angular.element(document.querySelector('#custom-style')).remove()
            }
            customStyleElement = document.createElement("style")
            angular.element(customStyleElement).attr('id', 'custom-style')
            customStyleElement.innerHTML = css
            otherhead.appendChild(customStyleElement)
        }
        const setCustomStyleInViewLook = (css) => {
            const frm = frames['viewLook']
            if (!frm || !frm.document) {
                $timeout(setCustomStyleInViewLook, 200)
                return
            }
            const document = frm.document
            let otherhead = document.getElementsByTagName("head")[0]
            if (document.querySelector('#custom-style')) {
                angular.element(document.querySelector('#custom-style')).remove()
            }
            customStyleElement = document.createElement("style")
            angular.element(customStyleElement).attr('id', 'custom-style')
            customStyleElement.innerHTML = css
            otherhead.appendChild(customStyleElement)
        }
        const setUpStyleCss = (css) => {
            setStyleInViewFrame(css)
            setStyleInViewLook(css)
        }
        $rootScope.setUpCustomStyleCss = (css) => {
            setCustomStyleInViewFrame(css)
            setCustomStyleInViewLook(css)
        }
        $scope.styleChange = () => {
            let css = ''
            angular.forEach($scope.newCss, (value, key) => {
                angular.forEach(value, (kindValue, kindKey) => {
                    let kindKeyString = (kindKey !== 'default' ? ':' + kindKey : '')
                    let style = key + kindKeyString + '{'
                    angular.forEach(kindValue, (value2, key2) => {
                        key2 = key2.replace(/[A-Z]/, (match) => {
                            return '-' + match.toLowerCase()
                        })
                        style += key2 + ': ' + value2 + ';'
                    })
                    style += '}'
                    css += style
                })
            })
            let customCss = ''
            angular.forEach($rootScope.customCssStyle, (value, key) => {
                angular.forEach(value, (kindValue, kindKey) => {
                    let kindKeyString = (kindKey !== 'default' ? ':' + kindKey : '')
                    let style = key + kindKeyString + '{'
                    angular.forEach(kindValue, (value2, key2) => {
                        key2 = key2.replace(/[A-Z]/, (match) => {
                            return '-' + match.toLowerCase()
                        })
                        style += key2 + ': ' + value2 + ';'
                    })
                    style += '}'
                    customCss += style
                })
            })
            setUpStyleCss(css)
            $rootScope.setUpCustomStyleCss(customCss)
        }
        $scope.pageFontsChange = () => {
            let frm = frames['viewFrame'].document
            let otherhead = frm.getElementsByTagName("head")[0]
            let oldLinks = otherhead.querySelectorAll('#font-link')
            angular.element(oldLinks).remove()
            let fontLink = otherhead.querySelector('#font-link')
            if (fontLink) fontLink.remove()
            let fontStyle = otherhead.querySelector('#font-style')
            if (fontStyle) fontStyle.remove()
            angular.forEach($scope.pageFonts, (value, key) => {
                let link = frm.createElement("link")
                angular.element(link).attr('id', 'font-link')
                angular.element(link).attr('rel', 'stylesheet')
                angular.element(link).attr('href', value.link)
                otherhead.appendChild(link)
            })
            $scope.styleChange()
        }
        $scope.styleId = null
        $scope.newCss = {}
        $rootScope.loadStyle = (cssId) => {
            $scope.styleId = cssId
            if (!$scope.styleId) {
                return
            }
            $scope.newCss = {}
            $http.get(apiBase + '/styles/' + $scope.styleId + '/css').then((response) => {
                const css = response.data.css
                $scope.pageFonts = response.data.fonts
                angular.forEach(css, (value, key) => {
                    const tagName = value.tag
                    let suffix = 'default'
                    const tag = value.tag.split(':')
                    if (tag[1]) {
                        suffix = tag[1]
                    }
                    angular.forEach(value.css, (value2, key2) => {
                        let parameterName = value2.parameter.replace(/-[a-z]/, (match) => {
                            return match.substring(1).toUpperCase()
                        })
                        if (!$scope.newCss[tagName]) {
                            $scope.newCss[tagName] = {}
                        }
                        if (!$scope.newCss[tagName][suffix]) {
                            $scope.newCss[tagName][suffix] = {}
                        }
                        if (!$scope.newCss[tagName][suffix][parameterName]) {
                            $scope.newCss[tagName][suffix][parameterName] = ''
                        }
                        $scope.newCss[tagName][suffix][parameterName] = value2.value
                    })
                })
                const timeoutCallback = () => {
                    if (!frames || !frames['viewFrame'] || !frames['viewFrame'].document ||
                        !frames['viewFrame'].document.body) {
                        $timeout(timeoutCallback, 250)
                    } else {
                        $scope.pageFontsChange()
                        $scope.styleChange()
                    }
                }
                $timeout(timeoutCallback, 250)
            })
        }
        $rootScope.customCssStyle = {}
        $rootScope.loadCustomStyle = (cssId) => {
            if (!cssId) {
                return
            }
            $http.get(apiBase + '/styles/' + cssId + '/css').then((response) => {
                const css = response.data.css
                $scope.pageFonts = response.data.fonts
                angular.forEach(css, (value, key) => {
                    const tagName = value.tag
                    let suffix = 'default'
                    const tag = value.tag.split(':')
                    if (tag[1]) {
                        suffix = tag[1]
                    }
                    angular.forEach(value.css, (value2, key2) => {
                        let parameterName = value2.parameter.replace(/-[a-z]/, (match) => {
                            return match.substring(1).toUpperCase()
                        })
                        if (!$rootScope.customCssStyle[tagName]) {
                            $rootScope.customCssStyle[tagName] = {}
                        }
                        if (!$rootScope.customCssStyle[tagName][suffix]) {
                            $rootScope.customCssStyle[tagName][suffix] = {}
                        }
                        if (!$rootScope.customCssStyle[tagName][suffix][parameterName]) {
                            $rootScope.customCssStyle[tagName][suffix][parameterName] = ''
                        }
                        $rootScope.customCssStyle[tagName][suffix][parameterName] = value2.value
                    })
                })
                const timeoutCallback = () => {
                    if (!frames || !frames['viewFrame'] || !frames['viewFrame'].document ||
                        !frames['viewFrame'].document.body) {
                        $timeout(timeoutCallback, 250)
                    } else {
                        $scope.pageFontsChange()
                        $scope.styleChange()
                    }
                }
                $timeout(timeoutCallback, 250)
            })
        }
        const loadData = (data, element, index = null, modal = false, clear = false) => {
            if (!data) {
                return
            }
            if (!element) {
                element = $scope.viewBody
            }
            if (clear || !element.childrens) {
                element.childrens = []
            }
            element.childrens.push(data.childrens)
            element.elementPositionId = data.elementPositionId

            const setScopeDataSet = (node, dat) => {
                const set = (set) => {
                    //if (node[set] && dat.dataSet && dat.dataSet[node[set]]) {
                        $rootScope.dataSet[node.elementPositionId] = dat.dataSet[node.elementPositionId]//dat.dataSet[node[set]]
                    //}
                }
                set('field')
                set('src')
                set('alt')
                set('altId')
                set('altTitle')
                set('placeholder')
                set('html')
                if (node.childrens) {
                    angular.forEach(node.childrens, (value, key) => {
                        setScopeDataSet(value, dat)
                    })
                }
            }
            setScopeDataSet(element, data)

            const loadTrustHtml = (node) => {
                if (node.html) {
                    node.trustedHtml = $sce.trustAsHtml(node.html)
                }
                if (node.childrens) {
                    angular.forEach(node.childrens, (value, key) => {
                        loadTrustHtml(value)
                    })
                }
            }
            loadTrustHtml(element)
        }
        $scope.loadViewElement = (loadOptions) => {
            $scope.elementId = loadOptions.id
            if (loadOptions.element) {
                $http.get(apiBase + '/elements/' + loadOptions.id).then((response) => {
                    /*let elementPositionId = getParentElementPositionId(loadOptions.element)
                    const setElementPositionId = (node)=>{
                        node.elementPositionId = elementPositionId
                        angular.forEach(node.childrens, (value, key)=>{
                            setElementPositionId(value)
                        })
                    }
                    setElementPositionId(response.data.content)*/
                    let data = {
                        childrens: response.data.content,
                        dataSet: response.data.dataSet,
                        type:'container',
                        elementPositionId: response.data.content.elementPositionId
                    }
                    loadData(data, loadOptions.element, loadOptions.index, loadOptions.modal)
                    $scope.customStyleId = response.data.styleId
                    $rootScope.loadCustomStyle(response.data.styleId)
                    $rootScope.styleId = response.data.rootStyleId
                    $rootScope.loadStyle(response.data.rootStyleId)
                    loader.hide()
                })
            }
        }
        $scope.rootAddContainer = () => {
            addNewElement($scope.viewBody, 'container')
        }
        $scope.pasteToRoot = () => {
            $rootScope.pasteElement($scope.viewBody)
        }
        $rootScope.pasteElement = (element) => {
            if (!element.childrens) {
                element.childrens = []
            }
            const changeIds = (elem) => {
                if (elem.childrens) {
                    angular.forEach(elem.childrens, (value, key) => {
                        elem.childrens[key] = changeIds(value)
                    })
                }
                if (elem.id) {
                    const oldId = '-' + elem.elementPositionId + '-' + elem.id
                    elem.id = getId()
                    if ($rootScope.customCssStyle['#id' + oldId]) {
                        $rootScope.customCssStyle['#id' + elem.id] = $rootScope.customCssStyle['#id' + oldId]
                    }
                }
                if (elem.altId) {
                    const oldAltId = '-' + (elem.elementPositionId?elem.elementPositionId:('-' + elem.altId))
                    elem.altId = getId()
                    if ($rootScope.customCssStyle['#id' + oldAltId]) {
                        $rootScope.customCssStyle['#id' + elem.altId] = $rootScope.customCssStyle['#id' + oldAltId]
                    }
                }
                if (elem.alt) {
                    const oldAltId = '-' + (elem.elementPositionId?elem.elementPositionId:('-' + elem.alt))
                    elem.alt = getId()
                    if ($rootScope.customCssStyle['#id' + oldAltId]) {
                        $rootScope.customCssStyle['#id' + elem.alt] = $rootScope.customCssStyle['#id' + oldAltId]
                    }
                }
                $rootScope.dataSet[elem.elementPositionId][elem.id] = $rootScope.dataSet[elem.field]
                const oldSrc = elem.src
                elem.src = getId()
                $rootScope.dataSet[elem.elementPositionId][elem.src] = $rootScope.dataSet[oldSrc]
                elem.field = elem.id
                return elem
            }
            let newElement = $scope.copyFrom($rootScope.cuttedElement)
            changeIds(newElement)
            element.childrens.push(newElement)
            $scope.styleChange()
        }
        $scope.showElementMenuPopup = (event, element) => {
            if (!element) {
                return
            }
            angular.element(document.querySelector('#element-menu-popup')).remove()
            const frame = frames['viewFrame']
            const elem = frame.document.querySelector('#id-' + element.elementPositionId + '-' + (element.altId ? element.altId : element.id))
            if (!elem) {
                return
            }
            const rect = elem.getBoundingClientRect()
            const frameOffsetTop = document.querySelector('#viewFrame').offsetTop
            const frameOffsetLeft = document.querySelector('#viewFrame').offsetLeft
            popup({
                templateUrl: templateRoot + 'ElementEditor/Popups/ElementMenuPopup.html',
                posX: rect.left + frameOffsetLeft,
                posY: rect.top + rect.height + frameOffsetTop,
                scope: {
                    close: () => {
                        angular.element(document.querySelector('#element-menu-popup')).remove()
                    },
                }
            }).show()
        }
        $http.get(apiBase + '/elements/tags').then((response) => {
            $scope.elementsTags = response.data.tags
        })
        $scope.filterElements = (element) => {
            if ($scope.elementsTag) {
                let find = false
                angular.forEach(element.tags, (value, key) => {
                    if (value == $scope.elementsTag) {
                        find = true
                    }
                })
                return find
            } else {
                return true
            }
        }
        //$scope.contentEditableId = null
        //$scope.lastContentEditableElement = {}
        $scope.change = (event, element) => {
            if (!element) {
                return
            }
            $rootScope.selectedElement = element
            $timeout(() => {
                $rootScope.selectedElementIndex = null
            }, 1)
            //if (element) {
            //$scope.lastContentEditableElement.editable = false
            //if ($scope.contentEditableId == element.altId) {
            //   element.editable = true
            //}
            //$scope.contentEditableId = element.altId
            //$scope.lastContentEditableElement = element
            //}
            if (event) {
                $scope.showElementMenuPopup(event, $rootScope.selectedElement)
                event.stopPropagation()
            }
        }
        $rootScope.change = $scope.change
        $scope.changeHtml = () => {
            $rootScope.selectedElement.trustedHtml = $sce.trustAsHtml($rootScope.selectedElement.html)
        }
        const loadElement = () => {
            const body = frames['viewFrame'] ? frames['viewFrame'].document.body : null
            const view = body ? body.querySelector('view') : null
            if (!view && $routeParams.id) {
                $timeout(loadElement, 100)
                return
            } else if ($routeParams.id) {
                $rootScope.resizeView()
                $http.get(apiBase + '/elements/' + $routeParams.id + '/tags').then((response) => {
                    $rootScope.tags = response.data.tags
                })
                $scope.loadViewElement({
                    id: $routeParams.id,
                    element: $scope.viewBody,
                })
            }
        }
        loadElement()
        $scope.closePage = () => {
            $scope.savePage(true)
        }
        $scope.savePage = (close = false) => {
            $rootScope.selectedElement = null
            loader.show()
            $scope.saveStyle()
            saveElement(() => {
                if (close) {
                    $location.path(base + '/elementy')
                } else {
                    loader.hide()
                }
            })
        }
        $rootScope.selectElement = (event, element) => {
            $scope.change(event, element)
        }
        $scope.setRandomValue = (element, kind) => {
            if (kind == 'word') {
                $rootScope.dataSet[element.elementPositionId][element.field] = loremIpsum.words(loremIpsum.random(1, 2))
            } else if (kind == 'sentence') {
                $rootScope.dataSet[element.elementPositionId][element.field] = loremIpsum.sentences(1)
            } else if (kind == 'paragraph') {
                $rootScope.dataSet[element.elementPositionId][element.field] = loremIpsum.sentences(loremIpsum.random(6, 10))
            }
        }
        $scope.colorChange = {
            onChange: () => {
                console.log('change')
            }
        }
        $http.get(apiBase + '/styles/public').then((response) => {
            $scope.styles = response.data.styles
        })
        $scope.selectStyleChooseClick = (style, scope) => {
            $rootScope.styleId = style.id
            $rootScope.loadStyle(style.id)
        }
        $scope.selectStyle = () => {
            $rootScope.leftPanel = 'selectStyle'
            $rootScope.rebuildPanelLeftScrollbar()
        }
        $scope.showElements = () => {
            $rootScope.leftPanel = 'structure'
            $rootScope.rebuildPanelLeftScrollbar()
        }
        $scope.showTags = () => {
            $rootScope.leftPanel = 'tags'
            $rootScope.rebuildPanelLeftScrollbar()
        }
        angular.element(frames['viewFrame']).on('load', function () {
            const frm = frames['viewFrame'].document
            let otherbody = frm.getElementsByTagName("body")[0]
            if (!otherbody) {
                $timeout(loading, 200)
                return
            }
            $compile(otherbody)($scope)
        })
        $rootScope.rebuildPanelLeftScrollbar = () => {
            const left = document.querySelector('.my-scroll-area-left')
            if (left) {
                if (!$scope.psl) {
                    $scope.psl = new PerfectScrollbar(left);
                }
                $scope.psl.update()
            }
        }
        $rootScope.rebuildPanelLeftScrollbar()
        $rootScope.selectAddElement = (event, rebuildScrolls = true, element) => {
            if (!$rootScope.elementsSection) {
                $rootScope.elementsSection = 'welcome'
            }
            if (!$rootScope.elementsKind) {
                $rootScope.elementsKind = 'body'
            }
            $rootScope.leftPanel = 'elements'
        }
        $scope.saveStyle = () => {
            let css = []
            let fonts = []
            angular.forEach($rootScope.customCssStyle, (value, key) => {
                angular.forEach(value, (kindValue, kindKey) => {
                    let kindKeyString = (kindKey !== 'default' ? kindKey : '').split(':')
                    let keyArray = key.split(':')
                    let newTag = []
                    const distinct = (array) => {
                        angular.forEach(array, (value, key) => {
                            let find = false
                            angular.forEach(newTag, (value2, key2) => {
                                if (value2 == value) {
                                    find = true
                                }
                            })
                            if (!find && value && (value !== '')) {
                                newTag.push(value)
                            }
                        })
                    }
                    distinct(keyArray)
                    distinct(kindKeyString)
                    let tagRaw = newTag.join(':')
                    let style = {
                        tag: tagRaw,
                        css: [],
                    }
                    angular.forEach(kindValue, (value2, key2) => {
                        key2 = key2.replace(/[A-Z]/, (match) => {
                            return '-' + match.toLowerCase()
                        })
                        style.css.push({
                            parameter: key2,
                            value: value2
                        })
                        if (key2 == 'font-family') {
                            let name = value2.split('\'')
                            name = name[1]
                            fonts.push(name)
                        }
                    })
                    css.push(style)
                })
            })
            $http.post(apiBase + '/styles/' + $scope.customStyleId + '/css', {
                css: css,
            }).then((response) => {
            })
        }
        $rootScope.copyElement = (element, callback) => {
            element = element ? element : $rootScope.selectedElement
            $rootScope.cuttedElement = angular.copy(element)
            if (callback) {
                callback()
            }
        }
        const deleteInElements = (elem, element) => {
            let index = null
            angular.forEach(elem.childrens, (value, key) => {
                if (value == element) {
                    index = key
                }
                deleteInElements(value, element)
            })
            if (index != null) {
                elem.childrens.splice(index, 1)
            }
        }
        $rootScope.cutElement = (callback, element) => {
            element = element ? element : $rootScope.selectedElement
            deleteInElements($scope.viewBody, element)
            $rootScope.cuttedElement = element
            $rootScope.selectedElement = null
            $rootScope.elementToAddNewElement = null
            $rootScope.resizeView()
            if (callback) {
                callback()
            }
        }
    })