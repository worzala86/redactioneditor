var base = '/admin';
var apiBase = '/api';
var templateBase = '/Public/Templates/Admin/';
var templateRoot = '/Public/Templates/';

angular.module('RedActionAdmin', ['ngRoute', 'ngSanitize', 'chart.js', 'ngFileUpload',
    'ngModal', 'ngPopup',
    'ngStyle', 'ngRedActionElementEditor'])

    .config(function ($routeProvider, $locationProvider, $compileProvider, $httpProvider, $rootScopeProvider) {
        $routeProvider
            .when(base + '/', {
                templateUrl: templateBase + 'Dashboard.html',
                controller: 'dashboardController',
                logged: true,
            })
            .when(base + '/uzytkownicy', {
                templateUrl: templateBase + 'Users.html',
                controller: 'usersController',
                logged: true,
            })
            .when(base + '/projekty', {
                templateUrl: templateBase + 'Projects.html',
                controller: 'projectsController',
                logged: true,
            })
            .when(base + '/elementy', {
                templateUrl: templateBase + 'Elements.html',
                controller: 'elementsController',
                logged: true,
            })
            .when(base + '/zdjecia', {
                templateUrl: templateBase + 'Images.html',
                controller: 'imagesController',
                logged: true,
            })
            .when(base + '/strony', {
                templateUrl: templateBase + 'Pages.html',
                controller: 'pagesController',
                logged: true,
            })
            .when(base + '/strona/nowa', {
                templateUrl: templateBase + 'PagesEdit.html',
                controller: 'pagesEditController',
                logged: true,
            })
            .when(base + '/strona/:id', {
                templateUrl: templateBase + 'PagesEdit.html',
                controller: 'pagesEditController',
                logged: true,
            })
            .when(base + '/komponenty', {
                templateUrl: templateBase + 'Components.html',
                controller: 'componentsController',
                logged: true,
            })
            .when(base + '/style', {
                templateUrl: templateBase + 'Styles.html',
                controller: 'stylesController',
                logged: true,
            })
            .when(base + '/dokumentacja', {
                templateUrl: templateBase + 'Documentation.html',
                controller: 'documentationController'
            })
            .when(base + '/konfiguracja', {
                templateUrl: templateBase + 'Config.html',
                controller: 'configurationController'
            })
            .when(base + '/statystyki', {
                templateUrl: templateBase + 'Statistics.html',
                controller: 'statisticsController'
            })
            .when(base + '/projekt/nowy', {
                templateUrl: templateBase + 'Projects/Edit.html',
                controller: 'projectsEditController',
                logged: true,
            })
            .when(base + '/projekt/:id/edycja', {
                templateUrl: templateBase + 'Editor/RedActionEditor.html',
                controller: '3ViewEditorController',
                logged: true,
            })
            .when(base + '/styl/:styleId/edycja', {
                templateUrl: templateRoot + 'Styles/Edit.html',
                controller: 'stylesEditController',
                logged: true,
            })
            .when(base + '/element/:id/edycja', {
                templateUrl: templateRoot + 'ElementEditor/RedActionElementEditor.html',
                controller: 'elementEditorController',
                logged: true,
            })
            .when(base + '/tagi', {
                templateUrl: templateBase + 'Tags.html',
                controller: 'tagsController',
                logged: true,
            })
        ;

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $compileProvider.debugInfoEnabled(true)
        delete $httpProvider.defaults.headers.common['X-Requested-With']
        $httpProvider.defaults.useXDomain = true
        $httpProvider.defaults.withCredentials = true

        $rootScopeProvider.digestTtl(100)
    })

    .run(function ($rootScope, $http, $location, $route, $compile, $timeout, $window) {
        $rootScope.baseUrl = base
        $rootScope.logout = function () {
            $http.get(apiBase + '/logout').then(function (response) {
                if (response.data.success) {
                    $rootScope.user.logged = false
                    window.location = '/'
                }
            });
        }
        $rootScope.loadUserStatus = () => {
            $http.get(apiBase + '/user-status').then(function (response) {
                $rootScope.user = {
                    logged: response.data.logged,
                    admin: response.data.isAdmin,
                };
                if (!$rootScope.user.admin || !$rootScope.user.logged) {
                    if (!$rootScope.user.logged) {
                        window.location = '/logowanie'
                    } else {
                        window.location = '/'
                    }
                }
            })
        }
        $rootScope.loadUserStatus()
        $rootScope.$on('$routeChangeStart', function ($event, next, current) {
            if (next && (next.logged == true) && $rootScope.user && !$rootScope.user.admin && !$rootScope.user.logged) {
                if (!$rootScope.user.logged) {
                    window.location = '/logowanie'
                } else {
                    window.location = '/'
                }
            }
        });
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
        })
        $rootScope.cursorX;
        $rootScope.cursorY;
        document.onmousemove = function (e) {
            $rootScope.cursorX = e.pageX;
            $rootScope.cursorY = e.pageY;
        }
        const calculateGrid = () => {
            //$rootScope.$apply(() => {
            const width = document.body.clientWidth
            let gridStyle = 'xs'
            if (width >= 500) gridStyle = 'sm'
            if (width >= 700) gridStyle = 'md'
            if (width >= 900) gridStyle = 'lg'
            if (width >= 1100) gridStyle = 'xl'
            $rootScope.gridStyle = gridStyle
            //})
        }
        $rootScope.resizeView = (extraHeight = 0, screenWidth = null) => {
            if (screenWidth) {
                $rootScope.screenWidth = screenWidth
            }
            const element = document.querySelector('iframe#viewFrame')
            if (!element) {
                //$timeout($rootScope.resizeView, 100)
                return
            }
            if (!element.contentDocument) {
                //$timeout($rootScope.resizeView, 100)
                return
            }
            const editor = element.contentDocument.querySelector('#editor')
            if (!editor) {
                //$timeout($rootScope.resizeView, 100)
                return
            }
            let width = $rootScope.screenWidth
            let left = 0
            let right = 0
            let leftPanelWidth = 0
            let rightPanelWidth = 0

            if ($rootScope.leftPanel) {
                leftPanelWidth = 300
                if ((($rootScope.leftPanel == 'elements') && ($rootScope.selectedElementsShow == 'elements'))
                    || ($rootScope.leftPanel == 'configure')
                    || ($rootScope.leftPanel == 'structure')) {
                    leftPanelWidth = 400
                }
            }
            if ($rootScope.rightPanel) {
                rightPanelWidth = 300
            }

            width = width - (leftPanelWidth + rightPanelWidth) + (document.documentElement.clientWidth - width)
            if (width > 1280) {
                width = 1280
            }
            if (leftPanelWidth) {
                left = leftPanelWidth + 15
                width -= 15
            } else if (rightPanelWidth) {
                left = 15
                width -= 15
            }
            if (rightPanelWidth) {
                right = rightPanelWidth + 15
                width -= 15
            } else if (leftPanelWidth) {
                width -= 15
            }
            if ((left == 0) && (right == 0)) {
                left = (document.documentElement.clientWidth - width) / 2
            }
            if ($rootScope.screenWidth < 1280) {
                if (screenWidth < width) {
                    let difference = width - $rootScope.screenWidth
                    width = screenWidth
                    left += difference / 2
                }
            }

            const style = window.getComputedStyle(editor)
            let height = style.height
            angular.element(element).css('width', width + 'px')
            angular.element(element).css('left', left + 'px')
            angular.element(element).css('right', right + 'px')
            /*angular.element(document.querySelector('url')).css('width', width + 'px')
            angular.element(document.querySelector('url')).css('left', left + 'px')
            angular.element(document.querySelector('url')).css('right', right + 'px')*/
            angular.element(document.querySelector('resize')).css('padding-left', left + 'px')
            angular.element(document.querySelector('resize')).css('width', width + 'px')
            angular.element(element).css('height', (parseInt(height) + extraHeight + 1) + 'px')
            angular.element(element.contentDocument.body).css('height', (parseInt(height) + extraHeight) + 'px')
            /*if (document.querySelector('#viewBuffor')) {
                angular.element(document.querySelector('#viewBuffor')).css('width', width + 'px')
                angular.element(document.querySelector('#viewBuffor')).css('left', left + 'px')
                angular.element(document.querySelector('#viewBuffor')).css('right', right + 'px')
            }*/
        }
        angular.element(window).bind('resize', () => {
            calculateGrid()
            $rootScope.resizeView()
        })
        calculateGrid()
        //$rootScope.resizeView(null, 1280)
        $rootScope.screenWidth = 1280
        const autoResize = () => {
            $rootScope.resizeView()
            $timeout(autoResize, 1000)
        }
        autoResize()

        $rootScope.showUserMenu = () => {
            const element = angular.element(document.querySelector('#user-menu-popup'))
            if (element.length) {
                element.remove()
                return
            }
            popup({
                templateUrl: templateBase + 'UserMenuPopup.html',
                posY: 46,
                scope: {
                    close: () => {
                        angular.element(document.querySelector('#user-menu-popup')).remove()
                    },
                }
            }).show()
        }

        $rootScope.scrollPanels = () => {
            /*const panelLeft = angular.element(document.querySelector('editor-left'))
            const panelRight = angular.element(document.querySelector('editor'))
            const offsetY = window.pageYOffset
            let top = 56 - offsetY
            if (top < 0) {
                top = 0
            }
            panelLeft.css('top', top + 'px')
            panelRight.css('top', top + 'px')*/
        }

        $rootScope.formatSizeUnits = (bytes) => {
            const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
            if (bytes === 0) return 'n/a'
            const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
            if (i === 0) return `${bytes} ${sizes[i]})`
            return `${(bytes / (1024 ** i)).toFixed(1)} ${sizes[i]}`
        }

        $rootScope.timeConverter = (timestamp) => {
            if (!timestamp) {
                return ''
            }
            var a = new Date(timestamp * 1000)
            var months = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Pażdziernik', 'Listopad', 'Grudzień']
            var year = a.getFullYear()
            var month = months[a.getMonth()]
            var date = a.getDate()
            var hour = a.getHours()
            if (hour < 10) {
                hour = '0' + hour
            }
            var min = a.getMinutes()
            if (min < 10) {
                min = '0' + min
            }
            var sec = a.getSeconds()
            if (sec < 10) {
                sec = '0' + sec
            }
            var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec
            return time
        }
    })

    .controller('projectsEditController', function ($scope, $http, $location, $routeParams, $rootScope) {
        if ($routeParams.projectId) {
            $http.get(apiBase + '/projects/' + $routeParams.projectId).then((response) => {
                $scope.project = response.data
            })
        }
        $scope.require = {
            name: true
        }
        $scope.save = () => {
            if (!$scope.project.name) {
                return
            }
            if ($routeParams.projectId) {
                $http.put(apiBase + '/projects/' + $routeParams.projectId, $scope.project).then((response) => {
                    if (response.data.success) {
                        $rootScope.projectsCount = true
                        $location.path(base + '/projekty')
                    }
                })
            } else {
                $http.post(apiBase + '/projects', $scope.project).then((response) => {
                    if (response.data.id) {
                        $rootScope.projectsCount = true
                        $location.path(base + '/projekt/' + response.data.id + '/edycja')
                    }
                })
            }
        }
    })

    .controller('documentationController', function ($scope, $http) {
        $http.get(apiBase + '/documentation').then((response) => {
            $scope.modules = response.data.modules;
            $scope.types = response.data.types;
        })
    })

    .controller('configurationController', function ($scope, $http) {
        $http.get(apiBase + '/update-info').then((response) => {
            $scope.version = response.data.version
            $scope.newVersion = response.data.newVersion
        })
        $scope.upgrade = () => {
            $http.get(apiBase + '/upgrade').then((response) => {
                if (response.data.success) {
                    $scope.version = response.data.version
                }
            })
        }
    })

    .controller('statisticsController', function ($scope, $http) {
        $scope.newPagesMonthLabels = [];
        $scope.newPagesMonthSeries = []
        $scope.newPagesMonthData = [];
        $http.get(apiBase + '/statistics').then((response) => {
            angular.forEach(response.data.newPagesMonth, (value, key) => {
                $scope.newPagesMonthLabels.push(key)
                $scope.newPagesMonthData.push(value)
            })
        })
        $scope.newPagesMonthDatasetOverride = [{yAxisID: 'y-axis-1'}, {yAxisID: 'y-axis-2'}];
        $scope.newPagesMonthOptions = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        }


        $scope.newPagesLabels = [];
        $scope.newPagesSeries = []
        $scope.newPagesData = [];
        $http.get(apiBase + '/statistics').then((response) => {
            angular.forEach(response.data.newPages, (value, key) => {
                $scope.newPagesLabels.push(key)
                $scope.newPagesData.push(value)
            })
        })
        $scope.newPagesDatasetOverride = [{yAxisID: 'y-axis-1'}, {yAxisID: 'y-axis-2'}];
        $scope.newPagesOptions = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        }
    })

    .controller('dashboardController', function ($scope, $location, $http, $rootScope) {
        $scope.newPagesLabels = [];
        $scope.newPagesSeries = []
        $scope.newPagesData = [];
        $scope.newPagesDatasetOverride = [{yAxisID: 'y-axis-1'}, {yAxisID: 'y-axis-2'}];
        $scope.newPagesOptions = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        }
        $http.get(apiBase + '/admin/dashboard').then((response) => {
            $scope.styleKindPopularity = response.data.htmlStyleKindsPopularity
            $scope.pagesCount = response.data.pagesCount
            angular.forEach(response.data.newPages, (value, key) => {
                $scope.newPagesLabels.push(key)
                $scope.newPagesData.push(value)
            })
            $scope.fileSystemSize = $rootScope.formatSizeUnits(response.data.fileSystemSize)
            $scope.databaseSize = $rootScope.formatSizeUnits(response.data.databaseSize)
            $scope.databaseVersion = response.data.databaseVersion
        });
    })

    .controller('usersController', function ($scope, $location, $http, $httpParamSerializerJQLike) {
        const reloadUsers = () => {
            if (!$scope.filter) {
                $scope.filter = {}
            }
            let filters = {
                filters: []
            }
            if ($scope.filter.mail) {
                filters.filters.push({
                    name: 'mail',
                    kind: '%',
                    value: $scope.filter.mail,
                })
            }
            if ($scope.filter.firstName) {
                filters.filters.push({
                    name: 'firstName',
                    kind: '%',
                    value: $scope.filter.firstName,
                })
            }
            if ($scope.filter.lastName) {
                filters.filters.push({
                    name: 'lastName',
                    kind: '%',
                    value: $scope.filter.lastName,
                })
            }
            let params = $httpParamSerializerJQLike(filters)
            if (params) {
                params = '?' + params;
            }
            $http.get(apiBase + '/users' + params).then((response) => {
                $scope.users = response.data.users
            })
        }
        reloadUsers()
        $scope.change = () => {
            reloadUsers()
        }
    })

    .controller('projectsController', function ($scope, $location, $http, $httpParamSerializerJQLike, $window, loader, confirmPopup) {
        loader.hide()
        $scope.filter = {
            status: 'private',
        }
        $scope.page = 1
        $scope.limit = 10
        const reloadProjects = () => {
            if (!$scope.filter) {
                $scope.filter = {}
            }
            let filters = {
                filters: []
            }
            if ($scope.filter.name) {
                filters.filters.push({
                    name: 'name',
                    kind: '%',
                    value: $scope.filter.name,
                })
            }
            let params = $httpParamSerializerJQLike(filters)
            if (params) {
                params = '?' + params;
            }
            if (params) {
                params = params + '&limit=' + $scope.limit + '&page=' + $scope.page
            } else {
                params = '?limit=' + $scope.limit + '&page=' + $scope.page
            }
            $http.get(apiBase + '/admin-projects' + params).then((response) => {
                $scope.projects = response.data.projects
                $scope.totalCount = response.data.totalCount
                $window.scrollTo(0, 0);
            })
        }
        reloadProjects()
        $scope.change = () => {
            reloadProjects()
        }
        $scope.prev = () => {
            $scope.page--
            reloadProjects()
        }
        $scope.next = () => {
            $scope.page++
            reloadProjects()
        }
        $scope.ceil = (number) => {
            return Math.ceil(number)
        }
        $scope.changeTag = (project) => {
            $http.post(apiBase + '/projects/' + project.id + '/tags', {tags: project.tags}).then((response) => {
            })
        }
        /*$scope.goToEditor = (project) => {
            angular.element(document.querySelector('#element-menu-popup')).remove()
            loader.show()
            $location.path(base + '/projekt/' + project.id + '/edycja')
        }*/
        /*$scope.delete = (projects, project) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/projects/' + project.id).then((response) => {
                    angular.element(document.querySelector('#menu-popup')).remove()
                    let index = null
                    angular.forEach(projects, (value, key) => {
                        if (value.id == project.id) {
                            index = key
                        }
                    })
                    if (index != null) {
                        projects.splice(index, 1)
                    }
                })
            })
        }*/
        /*$scope.makeFork = (project) => {
            loader.show()
            $http.get(apiBase + '/projects/' + project.id + '/fork').then((response) => {
                if (response.data.id) {
                    $location.path(base + '/projekt/' + response.data.id + '/edycja');
                }
            })
        }*/
    })

    .controller('componentsController', function ($scope, $location, $http, dialog, confirmPopup) {
        $scope.components = []
        $http.get(apiBase + '/admin-components').then((response) => {
            $scope.components = response.data.components
        })
        $scope.editComponent = (component) => {
            dialog({
                templateUrl: templateBase + 'Modals/EditComponentModal.html',
                scope: {
                    component: component ? angular.copy(component) : {},
                },
                buttons: [
                    {
                        title: 'Zapisz',
                        callback: (scope) => {
                            if (component) {
                                $http.put(apiBase + '/components/' + scope.component.id, scope.component).then((response) => {
                                    if (response.data.success) {
                                        angular.forEach(scope.component, (value, key) => {
                                            component[key] = value
                                        })
                                        scope.close()
                                    }
                                })
                            } else {
                                $http.post(apiBase + '/components', scope.component).then((response) => {
                                    if (response.data.id) {
                                        scope.component.id = response.data.id
                                        $scope.components.unshift(scope.component)
                                        scope.close()
                                    }
                                })
                            }
                        }
                    },
                    {
                        title: 'Anuluj',
                        callback: (scope) => {
                            scope.close()
                        }
                    }
                ]
            }).show()
        }
        $scope.delete = (components, component) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/components/' + component.id).then((response) => {
                    if (response.data.success) {
                        let index = null
                        angular.forEach(components, (value, key) => {
                            if (value.id == component.id) {
                                index = key
                            }
                        })
                        if (index != null) {
                            components.splice(index, 1)
                        }
                    }
                })
            })
        }
    })

    .controller('tagsController', function ($scope, $location, $http, dialog, confirmPopup) {
        $scope.tags = []
        $http.get(apiBase + '/tags').then((response) => {
            $scope.tags = response.data.tags
        })
        $scope.editTag = (tag) => {
            dialog({
                templateUrl: templateBase + 'Modals/EditTagModal.html',
                scope: {
                    tag: tag ? angular.copy(tag) : {},
                },
                buttons: [
                    {
                        title: 'Zapisz',
                        callback: (scope) => {
                            if (tag) {
                                $http.put(apiBase + '/tags/' + scope.tag.id, scope.tag).then((response) => {
                                    if (response.data.success) {
                                        angular.forEach(scope.tag, (value, key) => {
                                            tag[key] = value
                                        })
                                        scope.close()
                                    }
                                })
                            } else {
                                $http.post(apiBase + '/tags', scope.tag).then((response) => {
                                    if (response.data.id) {
                                        scope.tag.id = response.data.id
                                        $scope.tags.unshift(scope.tag)
                                        scope.close()
                                    }
                                })
                            }
                        }
                    },
                    {
                        title: 'Anuluj',
                        callback: (scope) => {
                            scope.close()
                        }
                    }
                ]
            }).show()
        }
        $scope.delete = (tags, tag) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/tags/' + tag.id).then((response) => {
                    if (response.data.success) {
                        let index = null
                        angular.forEach(tags, (value, key) => {
                            if (value.id == tag.id) {
                                index = key
                            }
                        })
                        if (index != null) {
                            tags.splice(index, 1)
                        }
                    }
                })
            })
        }
    })

    .controller('stylesController', function ($scope, $location, $http, dialog, confirmPopup) {
        $scope.styles = []
        $http.get(apiBase + '/admin-styles').then((response) => {
            $scope.styles = response.data.styles ? response.data.styles : []
        })
        $scope.editStyle = (style) => {
            dialog({
                templateUrl: templateBase + 'Modals/EditStyleModal.html',
                scope: {
                    style: style ? angular.copy(style) : {},
                },
                buttons: [
                    {
                        title: 'Zapisz',
                        callback: (scope) => {
                            if (style) {
                                $http.put(apiBase + '/styles/' + scope.style.id, scope.style).then((response) => {
                                    if (response.data.success) {
                                        angular.forEach(scope.style, (value, key) => {
                                            style[key] = value
                                        })
                                        scope.close()
                                    }
                                })
                            } else {
                                $http.post(apiBase + '/styles', scope.style).then((response) => {
                                    if (response.data.id) {
                                        scope.style.id = response.data.id
                                        $scope.styles.unshift(scope.style)
                                        scope.close()
                                    }
                                })
                            }
                        }
                    },
                    {
                        title: 'Anuluj',
                        callback: (scope) => {
                            scope.close()
                        }
                    }
                ]
            }).show()
        }
        $scope.delete = (styles, style) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/styles/' + style.id).then((response) => {
                    if (response.data.success) {
                        let index = null
                        angular.forEach(styles, (value, key) => {
                            if (value.id == style.id) {
                                index = key
                            }
                        })
                        if (index != null) {
                            styles.splice(index, 1)
                        }
                    }
                })
            })
        }
        $scope.setPublic = (style, publicBool) => {
            $http.post(apiBase + '/admin-styles/' + style.id + '/public', {'public': publicBool}).then((response) => {
                style.public = publicBool
            })
        }
        $scope.fork = (style) => {
            $http.post(apiBase + '/styles/' + style.id + '/fork').then((response) => {
                if (response.data.id) {
                    $location.path(base + '/styl/' + response.data.id + '/edycja')
                }
            })
        }
    })

    .controller('imagesController', function ($scope, $location, $http, $httpParamSerializerJQLike, $window) {
        $scope.page = 1
        $scope.limit = 10
        const reloadImages = () => {
            if (!$scope.filter) {
                $scope.filter = {}
            }
            let filters = {
                filters: []
            }
            if ($scope.filter.tagsExists) {
                let value = null;
                if ($scope.filter.tagsExists == 'true') {
                    value = 1
                } else if ($scope.filter.tagsExists == 'false') {
                    value = 0
                }
                if (value === 0) {
                    filters.filters.push({
                        name: 'tagsCount',
                        kind: '=',
                        value: 0,
                    })
                } else if (value === 1) {
                    filters.filters.push({
                        name: 'tagsCount',
                        kind: '>',
                        value: 0,
                    })
                }
            }
            let params = $httpParamSerializerJQLike(filters)
            if (params) {
                params = '?' + params;
            }
            if (params) {
                params = params + '&limit=' + $scope.limit + '&page=' + $scope.page
            } else {
                params = '?limit=' + $scope.limit + '&page=' + $scope.page
            }
            $http.get(apiBase + '/admin-images' + params).then((response) => {
                $scope.images = response.data.images
                $scope.totalCount = response.data.totalCount
                $window.scrollTo(0, 0)
            })
        }
        reloadImages()
        $scope.change = () => {
            reloadImages()
        }
        $scope.prev = () => {
            $scope.page--
            reloadImages()
        }
        $scope.next = () => {
            $scope.page++
            reloadImages()
        }
        $scope.ceil = (number) => {
            return Math.ceil(number)
        }
        $scope.changeTag = (image) => {
            $http.post(apiBase + '/images/' + image.id + '/tags', {tags: image.tags}).then((response) => {
            })
        }
    })

    .controller('pagesController', function ($scope, $location, $http, $rootScope, confirmPopup) {
        $http.get(apiBase + '/crm/pages').then((response) => {
            $scope.pages = response.data.pages
        })
        $scope.delete = (pages, page) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/crm/pages/' + page.id).then((response) => {
                    if (response.data.success) {
                        let index = null
                        angular.forEach(pages, (value, key) => {
                            if (value.id == page.id) {
                                index = key
                            }
                        })
                        if (index != null) {
                            pages.splice(index, 1)
                        }
                    }
                })
            })
        }
    })

    .controller('pagesEditController', function ($scope, $location, $http, $rootScope, $routeParams) {
        if ($routeParams.id) {
            $http.get(apiBase + '/crm/pages/' + $routeParams.id).then((response) => {
                $scope.page = response.data
            })
        }
        $scope.save = () => {
            if ($routeParams.id) {
                $http.put(apiBase + '/crm/pages/' + $routeParams.id, $scope.page).then((response) => {
                    if (response.data.success) {
                        $location.path(base + '/strony')
                    }
                })
            } else {
                $http.post(apiBase + '/crm/pages', $scope.page).then((response) => {
                    if (response.data.id) {
                        $location.path(base + '/strony')
                    }
                })
            }
        }
    })

    .controller('elementsController', function ($scope, $location, $http, $httpParamSerializerJQLike, $window,
                                                loader, confirmPopup, dialog) {
        $scope.filter = {}
        loader.hide()
        $scope.limit = 10
        $scope.page = 1
        const reloadElements = () => {
            if (!$scope.filter) {
                $scope.filter = {}
            }
            let filters = {
                filters: []
            }
            if ($scope.filter.kind) {
                filters.filters.push({
                    name: 'kind',
                    kind: '=',
                    value: $scope.filter.kind,
                })
            }
            let params = $httpParamSerializerJQLike(filters)
            if (params) {
                params = '?' + params;
            }

            if (params) {
                params = params + '&limit=' + $scope.limit + '&page=' + $scope.page
            } else {
                params = '?limit=' + $scope.limit + '&page=' + $scope.page
            }
            $http.get(apiBase + '/admin-elements' + params).then((response) => {
                $scope.elements = response.data.elements
                $scope.totalCount = response.data.totalCount
                $window.scrollTo(0, 0)
            })
        }
        reloadElements()
        $scope.change = () => {
            reloadElements()
        }
        $scope.prev = () => {
            $scope.page--
            reloadElements()
        }
        $scope.next = () => {
            $scope.page++
            reloadElements()
        }
        $scope.ceil = (number) => {
            return Math.ceil(number)
        }
        $scope.delete = (elements, element) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/elements/' + element.id).then((response) => {
                    angular.element(document.querySelector('#menu-popup')).remove()
                    let index = null
                    angular.forEach(elements, (value, key) => {
                        if (value.id == element.id) {
                            index = key
                        }
                    })
                    if (index != null) {
                        elements.splice(index, 1)
                    }
                })
            })
        }
        $scope.createElement = () => {
            dialog({
                templateUrl: templateBase + '/Modals/CreateElementModal.html',
                buttons: [
                    {
                        title: 'Zapisz',
                        callback: (scope) => {
                            $http.post(apiBase + '/elements/create', {kind: scope.newElement.kind}).then((response) => {
                                if (response.data.id) {
                                    scope.close()
                                    $location.path(base + '/element/' + response.data.id + '/edycja')
                                }
                            })
                        }
                    },
                    {
                        title: 'Anuluj',
                        callback: (scope) => {
                            scope.close()
                        }
                    }
                ]
            }).show()
        }
    })

    .directive("contenteditable", () => {
        return {
            restrict: "A",
            require: "ngModel",
            link: (scope, element, attrs, ngModel) => {
                ngModel.$render = () => {
                    element.html(ngModel.$viewValue || "kuku")
                }
                element.bind("keyup", () => {
                    ngModel.$setViewValue(element.html())
                })
            }
        }
    })

    .directive('imageScroll', () => {
        return {
            link: function (scope, element, attrs) {
                let offsetY = 0
                element.css('position', 'relative')
                element.css('overflow', 'hidden')
                element.find('img').css('position', 'absolute')
                element.find('img').css('top', '0px')
                element.bind('mousewheel', function (e) {
                    let prevent = false
                    if (e.deltaY < 0) {
                        offsetY += 30
                    } else {
                        offsetY -= 30
                    }
                    if (offsetY > 0) {
                        offsetY = 0
                        prevent = true
                    }
                    if (offsetY < -(element.find('img')[0].offsetHeight - element[0].offsetHeight)) {
                        offsetY = -(element.find('img')[0].offsetHeight - element[0].offsetHeight)
                        prevent = true
                    }
                    if (element.find('img')[0].offsetHeight < element[0].offsetHeight) {
                        offsetY = 0
                        prevent = true
                    }
                    angular.element(element).find('img').css('top', offsetY + 'px')
                    if (!prevent) {
                        e.stopPropagation()
                        e.preventDefault()
                    }
                })
            }
        }
    })

    .factory('loader', () => {
        const loader = angular.element(document.querySelector('#preloader'))
        return {
            show: () => {
                loader.css({'display': 'block'})
            },
            hide: () => {
                loader.css({'display': 'none'})
            }
        }
    })
;