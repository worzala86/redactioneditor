angular.module('ngRedActionEditor', ['ngMenuPopup'])

    .controller('3ViewEditorController', function ($scope, dialog, $rootScope, $http, $sanitize, $routeParams, $timeout,
                                                   Upload, popup, confirmPopup, $location, $compile, loader, $sce) {
        $rootScope.dataSet = []
        loader.show()
        $rootScope.pageId = 1
        $rootScope.paginationLimit = 10;
        const getId = (length = 8) => {
            var text = "";
            var possible = "abcdef0123456789";
            for (var i = 0; i < length; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }
        $scope.viewBody = {id: getId()}
        $scope.viewHeader = {id: getId()}
        $scope.viewFooter = {id: getId()}
        $scope.viewSidebarRight = {id: getId()}
        $scope.viewSidebarLeft = {id: getId()}
        $scope.viewModal = {id: getId()}
        $scope.getParentsSelector = (id, node) => {
            let selector = null
            if (!node) {
                selector = $scope.getParentsSelector(id, $scope.viewBody)
                selector = selector ? selector : $scope.getParentsSelector(id, $scope.viewHeader)
                selector = selector ? selector : $scope.getParentsSelector(id, $scope.viewFooter)
                selector = selector ? selector : $scope.getParentsSelector(id, $scope.viewSidebarRight)
                selector = selector ? selector : $scope.getParentsSelector(id, $scope.viewSidebarLeft)
                return selector
            }
            if (node.selector) {
                selector = node.selector
            }
            if (id == node.id) {
                return selector
            }
            angular.forEach(node.childrens, (value, key) => {
                let res = $scope.getParentsSelector(id, value)
                if (res) {
                    selector = res
                }
            })
            return selector
        }
        const addNewElement = (element, type, index) => {
            if (!element) {
                return
            }
            if (!element.childrens) {
                element.childrens = []
            }
            let scp = {}
            scp.type = type
            scp.show = true
            scp.id = getId()
            scp.altId = getId()
            scp.field = getId()
            let defaultText = loremIpsum.sentences(1)
            if (type == 'paragraph') {
                defaultText = loremIpsum.sentences(loremIpsum.random(6, 10))
            } else if (type == 'image') {
                defaultText = undefined
            } else if (type == 'pagination') {
                defaultText = undefined
            } else if (type == 'container') {
                defaultText = undefined
            } else if (type == 'link') {
                defaultText = loremIpsum.words(1)
            } else if (type == 'button') {
                defaultText = loremIpsum.words(1)
            } else if (type == 'input') {
                defaultText = loremIpsum.words(loremIpsum.random(1, 3))
            }

            if (type == 'container') {
                scp.style = {}
                scp.style.padding = '20px'
                scp.style.display = 'block'
                scp.style.position = 'relative'
                scp.style.boxSizing = 'border-box'
                scp.style.float = 'left'
                scp.style.border = '1px dashed #aaa'
                scp.class = 'col col-xs-12'
                delete (scp.altId)
            } else if (type == 'link') {
                scp.style = {}
                scp.style.display = 'inline-block'
                scp.style.margin = '0px 5px'
                scp.style.cursor = 'pointer'
                scp.style.outline = 'none'
                scp.class = 'link'
            } else if (type == 'pagination') {
                scp.style = {}
                scp.style.display = 'block'
                scp.style.textAlign = 'center'
            } else if (type == 'image') {
                scp.style = {}
                scp.src = getId()
                scp.picTitle = getId()
                scp.alt = getId()
                scp.style.minWidth = '25px'
                scp.style.minHeight = '25px'
                scp.style.background = '#ccc'
                scp.class = 'image responsive'
                $rootScope.dataSet[scp.src] = '/Files/default.jpg'
            } else if (type == 'input') {
                scp.class = 'input'
                scp.style = {}
                scp.style.padding = '3px 8px'
                scp.style.border = '1px solid #ccc'
                scp.placeholder = getId()
                $rootScope.dataSet[scp.placeholder] = defaultText
                defaultText = null
            } else if (type == 'label') {
                scp.class = 'label'
                scp.style = {}
            } else if (type == 'textarea') {
                scp.class = 'input'
                scp.style = {}
            } else if (type == 'html') {
                scp.html = ''
                scp.style = {}
                scp.style.padding = '20px'
                scp.style.border = '1px dashed #aaa'
            }

            if (index) {
                element.childrens.splice(index, 0, scp)
            } else {
                element.childrens.push(scp)
            }

            if (defaultText) {
                $rootScope.dataSet[scp.field] = defaultText
            }

            const getParantsSelector = (elementContainer) => {
                let selector = null
                const getParentSelector = (element) => {
                    if (element.selector) {
                        selector = element.selector
                    }
                    angular.forEach(element.childrens, (value, key) => {
                        if (value.id == scp.id) {
                            return selector
                        }
                        const sel = getParentSelector(value)
                        if (sel) {
                            selector = sel
                        }
                    })
                    return selector
                }
                getParentSelector(elementContainer)
                return selector
            }
            let selector = getParantsSelector($scope.viewBody)
            selector = !selector ? getParantsSelector($scope.viewHeader) : selector
            selector = !selector ? getParantsSelector($scope.viewSidebarRight) : selector
            selector = !selector ? getParantsSelector($scope.viewSidebarLeft) : selector
            selector = !selector ? getParantsSelector($scope.viewFooter) : selector
            if (selector) {
                angular.forEach($rootScope.dataSet[selector], (value, key) => {
                    $rootScope.dataSet[selector][key][scp.field] = defaultText
                    if (scp.src) {
                        $rootScope.dataSet[selector][key][scp.src] = '/Files/default.jpg'
                    }
                })
            }
            /*$timeout(() => {
                $rootScope.resizeView()
            }, 1)*/
        }
        $scope.copyFrom = (scope) => {
            return {
                type: scope.type,
                id: scope.id,
                selector: scope.selector,
                field: scope.field,
                src: scope.src,
                altId: scope.altId,
                alt: scope.alt,
                picTitle: scope.picTitle,
                href: scope.href,
                target: scope.target,
                class: scope.class,
                placeholder: scope.placeholder,
                html: scope.html,
                elementId: scope.elementId,
                show: scope.show,
                dataSetActive: scope.dataSetActive,
                backgroundActive: scope.backgroundActive,
            }
        }
        $scope.getElementsToSave = (element) => {
            const getElements = (scope, childrens) => {
                let data = []
                if (childrens) {
                    data = $scope.copyFrom(scope)
                }
                angular.forEach(scope.childrens, (value, key) => {
                    if (childrens) {
                        if (!data.childrens) {
                            data.childrens = []
                        }
                        data.childrens.push(getElements(value, true))
                    } else {
                        data.push(getElements(value, true))
                    }
                })
                return data
            }
            return Object.assign({}, getElements(element))
        }
        $scope.getDataSet = (element) => {
            let dataSet = []
            const getDataSetColsByField = (node) => {
                if (node.childrens) {
                    angular.forEach(node.childrens, (value, key) => {
                        getDataSetColsByField(value)
                    })
                }
                if (node.field && (($rootScope.dataSet[node.field] !== undefined))
                    && (($rootScope.dataSet[node.field] !== ''))
                    && (!dataSet[node.field])) {
                    dataSet[node.field] = $rootScope.dataSet[node.field]
                }
                if (node.src && (($rootScope.dataSet[node.src] !== undefined))
                    && (($rootScope.dataSet[node.src] !== ''))
                    && (!dataSet[node.src])) {
                    dataSet[node.src] = $rootScope.dataSet[node.src]
                }
                if (node.alt && (($rootScope.dataSet[node.alt] !== undefined))
                    && (($rootScope.dataSet[node.alt] !== ''))
                    && (!dataSet[node.alt])) {
                    dataSet[node.alt] = $rootScope.dataSet[node.alt]
                }
                if (node.altTitle && (($rootScope.dataSet[node.altTitle] !== undefined))
                    && (($rootScope.dataSet[node.altTitle] !== ''))
                    && (!dataSet[node.altTitle])) {
                    dataSet[node.altTitle] = $rootScope.dataSet[node.altTitle]
                }
                if (node.placeholder && (($rootScope.dataSet[node.placeholder] !== undefined))
                    && (($rootScope.dataSet[node.placeholder] !== ''))
                    && (!dataSet[node.placeholder])) {
                    dataSet[node.placeholder] = $rootScope.dataSet[node.placeholder]
                }
                if (node.html && (($rootScope.dataSet[node.html] !== undefined))
                    && (($rootScope.dataSet[node.html] !== ''))
                    && (!dataSet[node.html])) {
                    dataSet[node.html] = $rootScope.dataSet[node.html]
                }
            }
            const unsetDataSetsEndpoints = (node) => {
                if (node.childrens) {
                    angular.forEach(node.childrens, (value, key) => {
                        unsetDataSetsEndpoints(value)
                    })
                }
                delete (dataSet[node.selector])
            }
            getDataSetColsByField(element)
            unsetDataSetsEndpoints(element)
            return Object.assign({}, dataSet)
        }
        $scope.getContent = (element) => {
            const data = {
                id: element.id,
                type: element.type ? element.type : 'container',
                class: element.class,
                childrens: element.childrens,
                dataSet: $scope.getDataSet(element),
                show: true,
            }
            return data
        }
        const saveElement = (saveOptions) => {
            loader.show()
            $scope.saveCustomStyle()
            const data = {
                content: $scope.getContent(saveOptions.elements.body),
                header: $scope.getContent(saveOptions.elements.header),
                footer: $scope.getContent(saveOptions.elements.footer),
                sidebarLeft: $scope.getContent(saveOptions.elements.sidebarLeft),
                sidebarRight: $scope.getContent(saveOptions.elements.sidebarRight),
                config: $scope.config,
                showLeftSidebar: $rootScope.showLeftSidebar,
                showRightSidebar: $rootScope.showRightSidebar,
                styleId: $rootScope.styleId,
            }
            $http.put(apiBase + '/admin/projects/' + $routeParams.id + '/pages/' + $scope.config.defaultPageId, data).then((response) => {
                if (saveOptions.callback) {
                    saveOptions.callback(response)
                }
            })
        }
        $scope.addToRootElement = (root)=>{
            addNewElement(root, 'container')
        }
        $scope.pasteToRoot = (root)=>{
            $rootScope.pasteElement(root)
        }
        $scope.deleteElementFromDatabase = (event, elements, element) => {
            event.stopPropagation()
            confirmPopup(() => {
                $http.delete(apiBase + '/elements/' + element.id).then((response) => {
                    let index = null
                    angular.forEach(elements, (value, key) => {
                        if (value.id == element.id) {
                            index = key
                        }
                    })
                    if (index !== null) {
                        elements.splice(index, 1)
                    }
                })
            })
        }
        const loadAllElements = () => {
            $http.get(apiBase + '/elements/all').then((response) => {
                $scope.elements = response.data.elements
                $scope.sections = response.data.sections
                angular.forEach($scope.elements, (value, key) => {
                    value.type = 'element'
                })
            })
        }
        loadAllElements()
        const loadAllComponents = () => {
            $http.get(apiBase + '/components').then((response) => {
                $scope.components = response.data.components
            })
        }
        loadAllComponents()
        $scope.addNewComponentToSelectedElement = (event, component) => {
            event.stopPropagation()
            addNewElement($rootScope.selectedElement, component.key, $rootScope.selectedElementIndex)
        }
        $scope.addNewElementToSelectedElement = (event, element) => {
            event.stopPropagation()
            $scope.loadViewElement({
                id: element ? element.id : $scope.sourceElement.id,
                element: $rootScope.selectedElement,
                index: $rootScope.selectedElementIndex,
            })
        }
        $rootScope.showElement = (event, element) => {
            event.stopPropagation()
            $scope.sourceElement = element
            $scope.loadViewElement({
                id: element.id,
                element: $scope.viewModal,
                modal: true,
            })
        }
        $scope.cssData = {}
        const loadDataSet = (value, data) => {
            if (data && data.dataSet && data.dataSet[value.field]) {
                $rootScope.dataSet[value.altId] = data.dataSet[value.field]
                value.field = value.altId
            }
            if (value.src && data && data.dataSet && data.dataSet[value.src]) {
                $rootScope.dataSet[value.altId] = data.dataSet[value.src]
                value.src = value.altId
            }
        }
        const setDataSet = (element, data) => {
            angular.forEach(element.childrens, (value, key) => {
                if (value.childrens) {
                    setDataSet(value.childrens, data)
                }
            })
            loadDataSet(element, data)
        }
        const loadData = (data, element, index = null, modal = false, clear = false) => {
            if (!data) {
                return
            }
            if (!element) {
                element = $scope.viewBody
            }
            if (clear || !element.childrens) {
                element.childrens = []
            }
            element.elementId = data.elementId
            if (data.childrens) {
                angular.forEach(data.childrens, (value, key) => {
                    if (!element.childrens) {
                        element.childrens = []
                    }
                    if (index) {
                        element.childrens.splice(index, 0, value)
                    } else {
                        element.childrens.push(value)
                    }
                })
            }
            //element.childrens.push(data)

            if (!$rootScope.dataSet) {
                $rootScope.dataSet = []
            }
            const forEachCildrensGetDataToScopeDataset = (node, dat) => {
                if (node.field && dat.dataSet && dat.dataSet[node.field]) {
                    $rootScope.dataSet[node.field] = dat.dataSet[node.field]
                }
                if (node.src && dat.dataSet && dat.dataSet[node.src]) {
                    $rootScope.dataSet[node.src] = dat.dataSet[node.src]
                }
                if (node.alt && dat.dataSet && dat.dataSet[node.alt]) {
                    $rootScope.dataSet[node.alt] = dat.dataSet[node.alt]
                }
                if (node.altTitle && dat.dataSet && dat.dataSet[node.altTitle]) {
                    $rootScope.dataSet[node.altTitle] = dat.dataSet[node.altTitle]
                }
                if (node.placeholder && dat.dataSet && dat.dataSet[node.placeholder]) {
                    $rootScope.dataSet[node.placeholder] = dat.dataSet[node.placeholder]
                }
                if (node.html && dat.dataSet && dat.dataSet[node.html]) {
                    $rootScope.dataSet[node.html] = dat.dataSet[node.html]
                }
                if (node.childrens) {
                    angular.forEach(node.childrens, (value, key) => {
                        forEachCildrensGetDataToScopeDataset(value, dat)
                    })
                }
            }
            forEachCildrensGetDataToScopeDataset(element, data)

            const forEachCildrensLoadDatasetAndHtml = (node) => {
                if (node.selector) {
                    $scope.loadDataSetBySelector(node.selector)
                }
                if (node.html) {
                    node.trustedHtml = $sce.trustAsHtml(node.html)
                }
                if (node.childrens) {
                    angular.forEach(node.childrens, (value, key) => {
                        forEachCildrensLoadDatasetAndHtml(value)
                    })
                }
            }
            forEachCildrensLoadDatasetAndHtml(element)
        }
        const changeDataIds = (dataSet, element) => {
            let map = {}
            const changeIds = (node) => {
                if (!node) {
                    return
                }
                let newId = getId()
                map[node.id] = newId
                node.id = newId
                let newAltId = getId()
                map[node.altId] = newAltId
                node.altId = newAltId
                if (node.type == 'container') {
                    delete (node.altId)
                }
                const changeIdInField = (name) => {
                    const newId = getId()
                    if (dataSet && dataSet[node[name]]) {
                        dataSet[newId] = dataSet[node[name]]
                        delete (dataSet[node[name]])
                    }
                    node[name] = newId
                }
                changeIdInField('field')
                changeIdInField('src')
                changeIdInField('alt')
                changeIdInField('altTitle')
                changeIdInField('placeholder')
                changeIdInField('html')
                if (node.childrens) {
                    angular.forEach(node.childrens, (value, key) => {
                        node.childrens[key] = changeIds(value)
                    })
                }
                return node
            }
            changeIds(element)
            return {map: map, element: element, dataSet: dataSet}
        }
        $rootScope.viewElement = (element) => {
            $scope.viewModal = {id: getId()}

            loadData(element, $scope.viewModal, null, true)

            $timeout(() => {
                const frameBody = frames['viewLook'].document.body
                const lookBody = frameBody.querySelector('element-buffor')
                const style = window.getComputedStyle(lookBody)
                const height = style.height
                angular.element(document.querySelector('#viewLook')).css({height: height})
            }, 1)
            //$timeout(() => {
            //    $rootScope.resizeView()
            //}, 250)
        }
        $scope.loadViewElement = (loadOptions) => {
            if (loadOptions.element) {
                $http.get(apiBase + '/elements/' + loadOptions.id).then((response) => {
                    let data = {}
                    let childrens = {}
                    const res = changeDataIds(response.data.content.dataSet, response.data.content.childrens[0])
                    const map = res.map
                    data.childrens = [res.element]
                    data.type = response.data.content.type
                    map[response.data.content.id] = getId()
                    data.id = map[response.data.content.id]
                    data.style = response.data.content.style
                    data.dataSet = res.dataSet
                    data.childrens[0].elementId = loadOptions.id
                    loadData(data, loadOptions.element, loadOptions.index, loadOptions.modal)
                    $rootScope.loadCustomStyle(response.data.styleId)
                    $timeout(() => {
                        const frameBody = frames['viewLook'].document.body
                        const lookBody = frameBody.querySelector('element-buffor')
                        const style = window.getComputedStyle(lookBody)
                        const height = style.height
                        angular.element(document.querySelector('#viewLook')).css({height: height})
                    }, 1)
                    //$timeout(() => {
                    //    $rootScope.resizeView()
                    //}, 250)
                })
            } else {
                $http.get(apiBase + '/projects/' + $routeParams.id + '/pages/' + (loadOptions.id ? loadOptions.id : 'default')).then((response) => {
                    const data = response.data
                    $rootScope.paginationUrl = (data.paginationUrl) ? data.paginationUrl : ''
                    $rootScope.paginationUrlParam = (data.paginationUrlParam) ? data.paginationUrlParam : ''
                    $rootScope.paginationLimit = (data.paginationLimit) ? data.paginationLimit : $rootScope.paginationLimit
                    $scope.config = data.config
                    $scope.pageHash = data.id
                    $scope.domain = data.domain
                    $rootScope.showLeftSidebar = data.showLeftSidebar
                    $rootScope.showRightSidebar = data.showRightSidebar

                    loadData(data.body, $scope.viewBody, false, false, true)

                    $rootScope.loadStyle(data.styleId)
                    $scope.customStyleId = data.customStyleId
                    $rootScope.loadCustomStyle(data.customStyleId)
                    if (!loadOptions.onlyBodyLoad) {
                        loadData(data.header, $scope.viewHeader, true, false, true)
                        loadData(data.footer, $scope.viewFooter, true, false, true)
                        loadData(data.sidebarLeft, $scope.viewSidebarLeft, true, false, true)
                        loadData(data.sidebarRight, $scope.viewSidebarRight, true, false, true)
                    }
                    loader.hide()
                })
            }
        }
        $scope.showElementMenuPopup = (event, element) => {
            if (!element) {
                return
            }
            angular.element(document.querySelector('#element-menu-popup')).remove()
            const frame = frames['viewFrame']
            const elem = frame.document.querySelector('#id' + (element.altId ? element.altId : element.id))
            const rect = elem.getBoundingClientRect()
            const frameOffsetTop = document.querySelector('#viewFrame').offsetTop
            const frameOffsetLeft = document.querySelector('#viewFrame').offsetLeft
            popup({
                templateUrl: templateRoot + 'Editor/Popups/ElementMenuPopup.html',
                posX: rect.left + frameOffsetLeft,
                posY: rect.top + rect.height + frameOffsetTop + 20,
                scope: {
                    close: () => {
                        angular.element(document.querySelector('#element-menu-popup')).remove()
                    },
                }
            }).show()
        }
        $http.get(apiBase + '/elements/tags').then((response) => {
            $scope.elementsTags = response.data.tags
        })
        $scope.filterElements = (element) => {
            if ($scope.elementsTag) {
                let find = false
                angular.forEach(element.tags, (value, key) => {
                    if (value == $scope.elementsTag) {
                        find = true
                    }
                })
                return find
            } else {
                return true
            }
        }
        $scope.contentEditableId = null
        $scope.lastContentEditableElement = {}
        $scope.change = (event, element) => {
            if (!element) {
                return
            }
            if ($rootScope.selectedElement == element) {
                return
            }
            if (element) {
                $rootScope.selectedElement = element
            }
            $timeout(() => {
                $rootScope.selectedElementIndex = null
            }, 1)
            if (element) {
                $scope.lastContentEditableElement.editable = false
                if ($scope.contentEditableId == element.altId) {
                    element.editable = true
                }
                $scope.contentEditableId = element.altId
                $scope.lastContentEditableElement = element
            }
            if (event) {
                $scope.showElementMenuPopup(event, $rootScope.selectedElement)
                event.stopPropagation()
            }
        }
        $rootScope.change = $scope.change
        $scope.addDataSet = (newDataSet, datasets) => {
            $http.post(apiBase + '/projects/' + $routeParams.id + '/datasets', {name: newDataSet.name}).then((response) => {
                if (response.data.id) {
                    if (!datasets) {
                        datasets = []
                    }
                    datasets.unshift({
                        name: newDataSet.name,
                        id: response.data.id
                    })
                }
            })
        }
        $scope.deleteDataSet = (datasets, dataset) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/projects/' + $routeParams.id + '/datasets/' + dataset.id).then((response) => {
                    if (response.data.success) {
                        let index = null
                        angular.forEach(datasets, (value, key) => {
                            if (value.id == dataset.id) {
                                index = key
                            }
                        })
                        if (index !== null) {
                            datasets.splice(index, 1)
                        }
                    }
                })
            })
        }
        $scope.deleteDataSetField = (dataset, fields, field) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/projects/' + $routeParams.id + '/datasets/' + dataset.id + '/fields/' + field.id).then((response) => {
                    if (response.data.success) {
                        let index = null
                        angular.forEach(fields, (value, key) => {
                            if (value.id == field.id) {
                                index = key
                            }
                        })
                        if (index !== null) {
                            fields.splice(index, 1)
                        }
                    }
                })
            })
        }
        $scope.addDataSetField = (message, newDataSetField, dataset) => {
            if (!newDataSetField) {
                message.text = 'Wprowadź dane nowej pozycji!'
                return
            } else {
                message.text = null
            }
            if (!dataset.fields) {
                dataset.fields = []
            }
            if (!newDataSetField || !newDataSetField.name) {
                message.text = 'Wpisz nazwę pola!'
                return
            } else {
                message.text = null
            }
            if (!newDataSetField || !newDataSetField.type) {
                message.text = 'Wybierz typ pola!'
                return
            } else {
                message.text = null
            }
            $http.post(apiBase + '/projects/' + $routeParams.id + '/datasets/' + dataset.id + '/fields', {
                name: newDataSetField.name,
                type: newDataSetField.type
            }).then((response) => {
                if (response.data.id) {
                    dataset.fields.push({
                        name: newDataSetField.name,
                        type: newDataSetField.type,
                        id: response.data.id,
                    })
                    newDataSetField.name = null
                    newDataSetField.type = null
                }
            })
        }
        $scope.demoDataSet = (dataset) => {
            $http.put(apiBase + '/projects/' + $routeParams.id + '/datasets/' + dataset.id + '/demo').then((response) => {
            })
        }
        $scope.demoClearDataSet = (dataset) => {
            $http.delete(apiBase + '/projects/' + $routeParams.id + '/datasets/' + dataset.id + '/demo').then((response) => {
            })
        }
        $scope.showDataSets = () => {
            $http.get(apiBase + '/projects/' + $routeParams.id + '/datasets').then((response) => {
                dialog({
                    templateUrl: templateRoot + 'Editor/Modals/EditDataSetsModal.html',
                    scope: {
                        newDataSet: {},
                        message: {
                            text: null,
                        },
                        addDataSet: $scope.addDataSet,
                        addDataSetField: $scope.addDataSetField,
                        deleteDataSet: $scope.deleteDataSet,
                        deleteDataSetField: $scope.deleteDataSetField,
                        demoDataSet: $scope.demoDataSet,
                        demoClearDataSet: $scope.demoClearDataSet,
                        dataSets: response.data.datasets ? response.data.datasets : [],
                    },
                    buttons: [
                        {
                            title: 'Wczytaj',
                            callback: (scope) => {
                                scope.close()
                            }
                        },
                        {
                            title: 'Anuluj',
                            callback: (scope) => {
                                scope.close()
                            }
                        }
                    ]
                }).show()
            })
        }
        const loadDataSets = () => {
            $http.get(apiBase + '/projects/' + $routeParams.id + '/datasets').then((response) => {
                $rootScope.dataSets = response.data.datasets
            })
        }
        loadDataSets()
        const setNewDataSet = (selector) => {
            const setDataSetForAllChildrens = (element) => {
                angular.forEach($rootScope.dataSet[selector], (value, key) => {
                    if ($rootScope.dataSet[element.field]) {
                        $rootScope.dataSet[selector][key][element.field] = $rootScope.dataSet[element.field]
                    }
                    if ($rootScope.dataSet[element.src]) {
                        $rootScope.dataSet[selector][key][element.src] = $rootScope.dataSet[element.src]
                    }
                    if ($rootScope.dataSet[element.alt]) {
                        $rootScope.dataSet[selector][key][element.alt] = $rootScope.dataSet[element.alt]
                    }
                    if ($rootScope.dataSet[element.placeholder]) {
                        $rootScope.dataSet[selector][key][element.placeholder] = $rootScope.dataSet[element.placeholder]
                    }
                    if ($rootScope.dataSet[element.altTitle]) {
                        $rootScope.dataSet[selector][key][element.altTitle] = $rootScope.dataSet[element.altTitle]
                    }
                })
                angular.forEach(element.childrens, (value, key) => {
                    setDataSetForAllChildrens(value)
                })
            }
            setDataSetForAllChildrens($scope.viewBody)
            setDataSetForAllChildrens($scope.viewHeader)
            setDataSetForAllChildrens($scope.viewSidebarLeft)
            setDataSetForAllChildrens($scope.viewSidebarRight)
            setDataSetForAllChildrens($scope.viewFooter)
        }
        $scope.selectDataSet = (dataset, element) => {
            $http.get(apiBase + '/projects/' + $routeParams.id + '/datasets/' + dataset.id + '?limit=' + $rootScope.paginationLimit + '&page=1').then((response) => {
                element.selector = response.data.name
                $rootScope.dataSet[element.selector] = response.data.dataset
                setNewDataSet(element.selector)
            })
        }
        $scope.loadDataSetBySelector = (selector) => {
            $http.get(apiBase + '/projects/' + $routeParams.id + '/datasets/selector/' + selector + '?limit=' + $rootScope.paginationLimit + '&page=1').then((response) => {
                $rootScope.dataSet[selector] = response.data.dataset
                setNewDataSet(selector)
            })
        }
        $scope.changeHtml = () => {
            $rootScope.selectedElement.trustedHtml = $sce.trustAsHtml($rootScope.selectedElement.html)
        }
        $scope.selectDataSetField = (field) => {
            if ($rootScope.selectedElement.type == 'image') {
                $rootScope.selectedElement.src = field.name
            } else {
                $rootScope.selectedElement.field = field.name
            }
        }
        $scope.closeStyle = () => {
            $rootScope.selectedElement = null
        }
        $scope.changePage = () => {
            angular.element(document.querySelector('#select-page')).remove()
            $http.get(apiBase + '/projects/' + $routeParams.id + '/pages').then((response) => {
                dialog({
                    templateUrl: templateRoot + 'Editor/Modals/SelectPageModal.html',
                    scope: {
                        page: {},
                        pages: response.data.pages,
                        addPage: $scope.addPage,
                        editPage: $scope.editPage,
                        changePageData: $scope.changePageData,
                        deletePage: $scope.deletePage,
                    },
                    buttons: [
                        {
                            title: 'Wczytaj',
                            callback: (scope) => {
                                scope.close()
                            }
                        },
                        {
                            title: 'Anuluj',
                            callback: (scope) => {
                                scope.close()
                            }
                        }
                    ]
                }).show()
            })
        }
        $scope.deletePage = (pages, page) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/projects/' + $routeParams.id + '/pages/' + page.id).then((response) => {
                    let index = null
                    angular.forEach(pages, (value, key) => {
                        if (value.id == page.id) {
                            index = key
                        }
                    })
                    if (index != null) {
                        pages.splice(index, 1)
                    }
                })
            })
        }
        $scope.changePageData = (page, pageSelected) => {
            page.url = pageSelected.url
            page.id = pageSelected.id
        }
        $scope.addPage = (pages, page) => {
            let url = page.url
            let id = page.id
            if (url[0] != '/') {
                url = '/' + url
            }
            page.url = null
            page.id = null
            if (id) {
                $http.put(apiBase + '/projects/' + $routeParams.id + '/pages/' + id + '/url', {url: url}).then((response) => {
                    angular.forEach(pages, (value, key) => {
                        if (value.id == id) {
                            value.url = url
                        }
                    })
                })
            } else {
                $http.post(apiBase + '/projects/' + $routeParams.id + '/pages', {url: url}).then((response) => {
                    if (!pages) {
                        pages = []
                    }
                    pages.push({
                        url: url,
                        id: response.data.id
                    })
                })
            }
        }
        $scope.editPage = (url) => {
            $scope.viewBody.childrens = []
            $scope.loadViewElement({id: url.id, onlyBodyLoad: true})
        }
        const loadPage = () => {
            $scope.loadViewElement({})
            $scope.pageFontsChange()
            $rootScope.changeCss()
            $rootScope.changeCustomCss()
        }
        $scope.closePage = () => {
            $scope.savePage(true)
        }
        $scope.savePage = (close = false) => {
            $rootScope.selectedElement = null
            loader.show()
            $scope.saveCustomStyle()
            const frame = frames['viewFrame'].document
            const view = frame.getElementsByTagName("view")
            let scope = angular.element(view).scope()
            saveElement({
                elements: {
                    body: scope.viewBody,
                    header: scope.viewHeader,
                    footer: scope.viewFooter,
                    sidebarLeft: scope.viewSidebarLeft,
                    sidebarRight: scope.viewSidebarRight
                },
                page: true,
            })
            if(close) {
                $location.path(base + '/projekty');
            }
        }
        $rootScope.showLeftSidebar = false
        $rootScope.showRightSidebar = false
        $scope.closeLeftPanel = (event) => {
            $rootScope.leftPanel = false
            $rootScope.resizeView()
            /*const element = $rootScope.selectedElement
            if (!element) {
                return
            }
            $timeout(() => {
                $scope.change(event, element)
            }, 300)
            $timeout(() => {
                $scope.change(event, element)
            }, 250)
            $timeout(() => {
                $scope.change(event, element)
            }, 200)
            $timeout(() => {
                $scope.change(event, element)
            }, 150)
            $timeout(() => {
                $scope.change(event, element)
            }, 100)
            $timeout(() => {
                $scope.change(event, element)
            }, 50)*/
        }
        /*$scope.closeRightPanel = (event) => {
            $rootScope.rightPanel = false
            $rootScope.resizeView()
            const element = $rootScope.selectedElement
            if (!element) {
                return
            }
            $timeout(() => {
                $scope.change(event, element)
            }, 500)
            $timeout(() => {
                $scope.change(event, element)
            }, 450)
            $timeout(() => {
                $scope.change(event, element)
            }, 400)
            $timeout(() => {
                $scope.change(event, element)
            }, 350)
            $timeout(() => {
                $scope.change(event, element)
            }, 300)
            $timeout(() => {
                $scope.change(event, element)
            }, 250)
            $timeout(() => {
                $scope.change(event, element)
            }, 200)
            $timeout(() => {
                $scope.change(event, element)
            }, 150)
            $timeout(() => {
                $scope.change(event, element)
            }, 100)
            $timeout(() => {
                $scope.change(event, element)
            }, 50)
        }*/
        $scope.setPaginationPage = () => {
            $http.post(apiBase + '/projects/' + $routeParams.id + '/pages/' + $scope.pageHash + '/pagination', {
                paginationUrl: $rootScope.paginationUrl,
                paginationUrlParam: $rootScope.paginationUrlParam,
                paginationLimit: $rootScope.paginationLimit,
            }).then((response) => {
            })
        }
        $rootScope.copyElement = (element, callback) => {
            element = element ? element : $rootScope.selectedElement
            $rootScope.cuttedElement = angular.copy(element)
            if (callback) {
                callback()
            }
        }
        $rootScope.pasteElement = (element) => {
            if (!element.childrens) {
                element.childrens = []
            }
            const changeIds = (elem) => {
                if (elem.childrens) {
                    angular.forEach(elem.childrens, (value, key) => {
                        elem.childrens[key] = changeIds(value)
                    })
                }
                if (elem.id) {
                    const oldId = elem.id
                    elem.id = getId()
                    if($rootScope.customCssStyle['#id'+oldId]) {
                        $rootScope.customCssStyle['#id' + elem.id] = $rootScope.customCssStyle['#id' + oldId]
                    }
                }
                if (elem.altId) {
                    const oldAltId = elem.altId
                    elem.altId = getId()
                    if($rootScope.customCssStyle['#id'+oldAltId]) {
                        $rootScope.customCssStyle['#id' + elem.altId] = $rootScope.customCssStyle['#id' + oldAltId]
                    }
                }
                $rootScope.dataSet[elem.id] = $rootScope.dataSet[elem.field]
                const oldSrc = elem.src
                elem.src = getId()
                $rootScope.dataSet[elem.src] = $rootScope.dataSet[oldSrc]
                elem.field = elem.id
                return elem
            }
            let newElement = $scope.copyFrom($rootScope.cuttedElement)
            changeIds(newElement)
            element.childrens.push(newElement)
            $scope.styleChange()
        }
        const deleteInElements = (elem, element) => {
            let index = null
            angular.forEach(elem.childrens, (value, key) => {
                if (value == element) {
                    index = key
                }
                deleteInElements(value, element)
            })
            if (index != null) {
                elem.childrens.splice(index, 1)
            }
        }
        $rootScope.cutElement = (callback, element) => {
            element = element?element:$rootScope.selectedElement
            deleteInElements($scope.viewBody, element)
            $rootScope.cuttedElement = element
            $rootScope.selectedElement = null
            $rootScope.elementToAddNewElement = null
            $rootScope.resizeView()
            if (callback) {
                callback()
            }
        }
        $rootScope.deleteElement = (callback, element) => {
            element = element?element:$rootScope.selectedElement
            confirmPopup(() => {
                const deleteInElements = (elem) => {
                    if(!elem){
                        return
                    }
                    let index = null
                    angular.forEach(elem.childrens, (value, key) => {
                        if (value == element) {
                            index = key
                        }
                        deleteInElements(value)
                    })
                    if (index != null) {
                        delete ($rootScope.dataSet[element.id])
                        elem.childrens.splice(index, 1)
                    }
                }
                deleteInElements($scope.viewHeader)
                deleteInElements($scope.viewSidebarLeft)
                deleteInElements($scope.viewBody)
                deleteInElements($scope.viewSidebarRight)
                deleteInElements($scope.viewFooter)
                $rootScope.selectedElement = null
                $rootScope.elementToAddNewElement = null
                $rootScope.resizeView()
                if (callback) {
                    callback()
                }
            })
        }
        $scope.selectElement = (event, element) => {
            $scope.change(event, element)
        }
        $scope.showElements = () => {
            $rootScope.leftPanel = 'structure'
            $scope.rebuildPanelLeftScrollbar()
        }
        $scope.setRandomValue = (element, kind) => {
            if (kind == 'word') {
                $rootScope.dataSet[element.field] = loremIpsum.words(loremIpsum.random(1, 2))
            } else if (kind == 'sentence') {
                $rootScope.dataSet[element.field] = loremIpsum.sentences(1)
            } else if (kind == 'paragraph') {
                $rootScope.dataSet[element.field] = loremIpsum.sentences(loremIpsum.random(6, 10))
            }
        }
        let customImages = []
        $scope.files = {}
        $scope.selectImageSource = (element, target = 'image') => {
            $http.get(apiBase + '/images/custom?limit=20&page=1').then((customResponse) => {
                $http.get(apiBase + '/images/system?limit=20&page=1').then((systemResponse) => {
                    customImages = customResponse.data.images
                    if (!customImages) {
                        customImages = []
                    }
                    $rootScope.systemImages = systemResponse.data.images
                    $rootScope.systemImagesTotalCount = systemResponse.data.totalCount
                    $rootScope.customImages = customImages
                    $rootScope.customImagesTotalCount = customResponse.data.totalCount
                    dialog({
                        templateUrl: templateRoot + 'Modals/SelectImageModal.html',
                        scope: {
                            target: target,
                            drop: $scope.drop,
                            select: $scope.select,
                            submit: $scope.submit,
                            files: $scope.files,
                            upload: $scope.upload,
                            uploading: false,
                            selectImage: $scope.selectImage,
                            deleteImage: $scope.deleteImage,
                            element: element,
                            searchImages: $scope.searchImages,
                            systemImagesNextPage: $scope.systemImagesNextPage,
                            systemImagesPrevPage: $scope.systemImagesPrevPage,
                            customImagesNextPage: $scope.customImagesNextPage,
                            customImagesPrevPage: $scope.customImagesPrevPage,
                            searchImagesNextPage: $scope.searchImagesNextPage,
                            selectSearchedImage: $scope.selectSearchedImage,
                            ceil: (number) => {
                                return Math.ceil(number)
                            }
                        }
                    }).show()
                })
            })
        }
        $scope.searchImages = (searchKey) => {
            $http.post(apiBase + '/images/search', {key: searchKey, page: 1}).then((response) => {
                $rootScope.searchedImages = response.data.images
                $rootScope.searchedImagesPage = 1
            })
        }
        $rootScope.systemImagesPage = 1
        $scope.systemImagesPrevPage = () => {
            $rootScope.systemImagesPage--
            $http.get(apiBase + '/images/system?limit=20&page=' + $rootScope.systemImagesPage).then((response) => {
                $rootScope.systemImages = response.data.images
            })
        }
        $scope.systemImagesNextPage = () => {
            $rootScope.systemImagesPage++
            $http.get(apiBase + '/images/system?limit=20&page=' + $rootScope.systemImagesPage).then((response) => {
                $rootScope.systemImages = response.data.images
            })
        }
        $rootScope.customImagesPage = 1
        $scope.customImagesPrevPage = () => {
            $rootScope.customImagesPage--
            $http.get(apiBase + '/images/custom?limit=20&page=' + $rootScope.customImagesPage).then((response) => {
                $rootScope.customImages = response.data.images
            })
        }
        $scope.customImagesNextPage = () => {
            $rootScope.customImagesPage++
            $http.get(apiBase + '/images/custom?limit=20&page=' + $rootScope.customImagesPage).then((response) => {
                $rootScope.customImages = response.data.images
            })
        }
        $scope.searchImagesNextPage = (searchKey) => {
            $rootScope.searchedImagesPage++
            $http.post(apiBase + '/images/search', {
                key: searchKey,
                page: $rootScope.searchedImagesPage
            }).then((response) => {
                $rootScope.searchedImages = response.data.images
            })
        }
        $scope.selectSearchedImage = (element, id) => {
            loader.show()
            $http.get(apiBase + '/images/' + id + '/download').then((response) => {
                if (response.data.url) {
                    $rootScope.dataSet[element.src] = response.data.url
                    loader.hide()
                }
            })
        }
        $scope.selectImage = (element, image, target) => {
            if (target == 'background') {
                $rootScope.selectedElement.style.background = 'url(\'' + image.url + '\')'
                $scope.makeStyle($rootScope.selectedElement, $rootScope.selectedElement.style)
            } else {
                $rootScope.dataSet[element.src] = image.url
            }
        }
        $scope.deleteImage = (element, images, image) => {
            $http.delete(apiBase + '/images/' + image.id).then((response) => {
                let index = null
                angular.forEach(images, (value, key) => {
                    if (value.id == image.id) {
                        index = key
                    }
                })
                if (index != null) {
                    images.splice(index, 1)
                }
            })
        }
        $scope.abort = function (event, files) {
            files.uploading = false
            if ($scope.xhr) {
                $scope.xhr.abort()
            }
            event ? event.stopPropagation() : {}
        }
        $scope.drop = function () {
            $scope.submit()
        }
        $scope.select = function (event, files) {
            $scope.submit(files)
            event.stopPropagation()
        }
        $scope.submit = function (files) {
            angular.forEach(files, (value, key) => {
                $scope.upload(files, value)
            })
        }
        $scope.upload = function (filesCnt, file) {
            var files = [];
            files.push(file)
            var fileReader = new FileReader()
            fileReader.readAsDataURL(file)
            fileReader.onload = function (e) {
                var dataUrl = e.target.result
                var name = file.name
                var type = file.type
                var base64Data = dataUrl.substr(dataUrl.indexOf('base64,') + 'base64,'.length)
                filesCnt.uploading = true
                Upload.upload({
                    url: apiBase + '/files',
                    data: {
                        files: [{
                            name: name,
                            type: type,
                            file: base64Data,
                        }],
                        target: 'Images',
                    }
                }).success(function (resp) {
                    $http.put(apiBase + '/images/' + resp.files[0].id).then((response) => {
                        customImages.unshift(resp.files[0])
                    })
                    $scope.abort(null, filesCnt)
                }).error(function (resp) {
                }).progress(function (evt) {
                }).xhr(function (e, xhr) {
                    $scope.xhr = xhr
                })
            }
        }
        $rootScope.selectDataSetModal = (element) => {
            dialog({
                templateUrl: templateRoot + 'Editor/Modals/SelectDataSetModal.html',
                scope: {
                    datasets: $scope.datasets,
                    selectDataSet: $scope.selectDataSet,
                    selectDataSetField: $scope.selectDataSetField,
                    getParentsSelector: $scope.getParentsSelector,
                    selectedElement: $rootScope.selectedElement,
                }
            }).show()
        }
        $scope.colorChange = {
            onChange: () => {
                console.log('change')
            }
        }
        $scope.showProjectMenu = () => {
            const element = angular.element(document.querySelector('#project-menu-popup'))
            if (element.length) {
                element.remove()
                return
            }
            popup({
                templateUrl: templateRoot + '/Editor/Popups/ProjectMenuPopup.html',
                posY: 46,
                scope: {
                    close: () => {
                        angular.element(document.querySelector('#project-menu-popup')).remove()
                    },
                    showSettings: $scope.showSettings,
                    showSelectStyle: $scope.showSelectStyle,
                    showSelectFont: $scope.showSelectFont,
                }
            }).show()
        }
        $http.get(apiBase + '/styles/public').then((response) => {
            $scope.styles = response.data.styles
        })
        $scope.selectStyleChooseClick = (style, scope) => {
            $rootScope.styleId = style.id
            $rootScope.loadStyle(style.id)
        }
        $scope.selectStyle = () => {
            $rootScope.leftPanel = 'selectStyle'
            $scope.rebuildPanelLeftScrollbar()
        }
        angular.element(frames['viewFrame']).on('load', function(){
            const frm = frames['viewFrame'].document
            let otherbody = frm.getElementsByTagName("body")[0]
            $compile(otherbody)($scope)
            loadPage()
        })
        angular.element(frames['viewLook']).on('load', function(){
            const frm = frames['viewLook'].document
            let otherbody = frm.getElementsByTagName("body")[0]
            $compile(otherbody)($scope)
        })
        $scope.rebuildPanelLeftScrollbar = () => {
            //const refresh = () => {
                const left = document.querySelector('.my-scroll-area-left')
                if (!$scope.psl) {
                    //if (!$scope.psl) {
                        $scope.psl = new PerfectScrollbar(left);
                    }
                    $scope.psl.update()
                //}
            //}
            //$timeout(refresh, 100)
        }
        $scope.rebuildPanelLeftScrollbar()
        $rootScope.selectAddElement = (event, rebuildScrolls = true, element) => {
            $rootScope.leftPanel = 'elements'
            //$scope.editorViewLookSetup()
            $rootScope.resizeView()
            /*$timeout(() => {
                $scope.change(event, element)
            }, 300)
            $timeout(() => {
                $scope.change(event, element)
            }, 250)
            $timeout(() => {
                $scope.change(event, element)
            }, 200)
            $timeout(() => {
                $scope.change(event, element)
            }, 150)
            $timeout(() => {
                $scope.change(event, element)
            }, 100)
            $timeout(() => {
                $scope.change(event, element)
            }, 50)*/
            $timeout(() => {
                if (rebuildScrolls) {
                    const left = document.querySelector('.my-scroll-area-left')
                    if (left) {
                        $scope.psl = new PerfectScrollbar(left);
                        $scope.psl.update()
                    }
                    const right = document.querySelector('.my-scroll-area-right')
                    if (right) {
                        $scope.psr = new PerfectScrollbar(right);
                        $scope.psr.update()
                    }
                } else {
                    if ($scope.psl) {
                        $scope.psl.update()
                    }
                    if ($scope.psr) {
                        $scope.psr.update()
                    }
                }
            }, 100)
            $rootScope.scrollPanels()
        }
        $scope.tips = {}
        $scope.setupTips = () => {
            if (!$scope.viewHeader.childrens) {
                $scope.tips.headerCreate = true
                $scope.viewHeader.childrens = []
            }
            $scope.tips.headerCreateFunction = (event) => {
                $rootScope.leftPanel = 'elements'
                $rootScope.selectedElementsShow = 'elements'
                $scope.change(null, $scope.viewHeader)
                $timeout(() => {
                    $rootScope.selectAddElement(event, false, $scope.viewHeader)
                }, 100)
            }
            if (!$scope.viewBody.childrens) {
                $scope.tips.bodyCreate = true
                $scope.viewBody.childrens = []
            }
            $scope.tips.bodyCreateFunction = (event) => {
                $rootScope.leftPanel = 'elements'
                $rootScope.selectedElementsShow = 'elements'
                $scope.change(null, $scope.viewBody)
                $timeout(() => {
                    $rootScope.selectAddElement(event, false, $scope.viewBody)
                }, 100)
            }
            if (!$scope.viewFooter.childrens) {
                $scope.tips.footerCreate = true
                $scope.viewFooter.childrens = []
            }
            $scope.tips.footerCreateFunction = (event) => {
                $rootScope.leftPanel = 'elements'
                $rootScope.selectedElementsShow = 'elements'
                $scope.change(null, $scope.viewFooter)
                $timeout(() => {
                    $rootScope.selectAddElement(event, false, $scope.viewFooter)
                }, 100)
            }
            if (!$scope.viewSidebarLeft.childrens) {
                $scope.tips.sidebarLeftCreate = true
                $scope.viewSidebarLeft.childrens = []
            }
            $scope.tips.sidebarLeftCreateFunction = (event) => {
                $rootScope.showLeftSidebar = true
                $rootScope.leftPanel = 'elements'
                $rootScope.selectedElementsShow = 'elements'
                $scope.change(null, $scope.viewSidebarLeft)
                $timeout(() => {
                    $rootScope.selectAddElement(event, false, $scope.viewSidebarLeft)
                }, 100)
            }
            if (!$scope.viewSidebarRight.childrens) {
                $scope.tips.sidebarRightCreate = true
                $scope.viewSidebarLeft.childrens = []
            }
            $scope.tips.sidebarRightCreateFunction = (event) => {
                $rootScope.showRightSidebar = true
                $rootScope.leftPanel = 'elements'
                $rootScope.selectedElementsShow = 'elements'
                $scope.change(null, $scope.viewSidebarRight)
                $timeout(() => {
                    $rootScope.selectAddElement(event, false, $scope.viewSidebarRight)
                }, 100)
            }
        }
        $scope.setupTips()
        const setStyleInViewFrame = (css) => {
            const frm = frames['viewFrame']
            if (!frm || !frm.document) {
                $timeout(setStyleInViewFrame, 200)
                return
            }
            const document = frm.document
            let otherhead = document.getElementsByTagName("head")[0]
            if (document.querySelector('#new-style')) {
                angular.element(document.querySelector('#new-style')).remove()
            }
            styleElement = document.createElement("style")
            angular.element(styleElement).attr('id', 'new-style')
            styleElement.innerHTML = css
            otherhead.appendChild(styleElement)
        }
        const setStyleInViewLook = (css) => {
            const frm = frames['viewLook']
            if (!frm || !frm.document) {
                $timeout(setStyleInViewLook, 200)
                return
            }
            const document = frm.document
            let otherhead = document.getElementsByTagName("head")[0]
            if (document.querySelector('#new-style')) {
                angular.element(document.querySelector('#new-style')).remove()
            }
            styleElement = document.createElement("style")
            angular.element(styleElement).attr('id', 'new-style')
            styleElement.innerHTML = css
            otherhead.appendChild(styleElement)
        }
        const setCustomStyleInViewFrame = (css) => {
            const frm = frames['viewFrame']
            if (!frm || !frm.document) {
                $timeout(setCustomStyleInViewFrame, 200)
                return
            }
            const document = frm.document
            let otherhead = document.getElementsByTagName("head")[0]
            if (document.querySelector('#custom-style')) {
                angular.element(document.querySelector('#custom-style')).remove()
            }
            customStyleElement = document.createElement("style")
            angular.element(customStyleElement).attr('id', 'custom-style')
            customStyleElement.innerHTML = css
            otherhead.appendChild(customStyleElement)
        }
        const setCustomStyleInViewLook = (css) => {
            const frm = frames['viewLook']
            if (!frm || !frm.document) {
                $timeout(setCustomStyleInViewLook, 200)
                return
            }
            const document = frm.document
            let otherhead = document.getElementsByTagName("head")[0]
            if (document.querySelector('#custom-style')) {
                angular.element(document.querySelector('#custom-style')).remove()
            }
            customStyleElement = document.createElement("style")
            angular.element(customStyleElement).attr('id', 'custom-style')
            customStyleElement.innerHTML = css
            otherhead.appendChild(customStyleElement)
        }
        $scope.pageFontsChange = () => {
            let frm = frames['viewFrame'].document
            let otherhead = frm.getElementsByTagName("head")[0]
            let oldLinks = otherhead.querySelectorAll('#font-link')
            angular.element(oldLinks).remove()
            let fontLink = otherhead.querySelector('#font-link')
            if (fontLink) fontLink.remove()
            let fontStyle = otherhead.querySelector('#font-style')
            if (fontStyle) fontStyle.remove()
            angular.forEach($scope.pageFonts, (value, key) => {
                let link = frm.createElement("link")
                angular.element(link).attr('id', 'font-link')
                angular.element(link).attr('rel', 'stylesheet')
                angular.element(link).attr('href', value.link)
                otherhead.appendChild(link)
            })
            $scope.styleChange()
        }
        const setUpStyleCss = (css) => {
            setStyleInViewFrame(css)
            setStyleInViewLook(css)
        }
        $rootScope.setUpCustomStyleCss = (css) => {
            setCustomStyleInViewFrame(css)
            setCustomStyleInViewLook(css)
        }
        $scope.styleChange = () => {
            let css = ''
            angular.forEach($scope.newCss, (value, key) => {
                angular.forEach(value, (kindValue, kindKey) => {
                    let kindKeyString = (kindKey !== 'default' ? ':' + kindKey : '')
                    let style = key + kindKeyString + '{'
                    angular.forEach(kindValue, (value2, key2) => {
                        key2 = key2.replace(/[A-Z]/, (match) => {
                            return '-' + match.toLowerCase()
                        })
                        style += key2 + ': ' + value2 + ';'
                    })
                    style += '}'
                    css += style
                })
            })
            let customCss = ''
            angular.forEach($rootScope.customCssStyle, (value, key) => {
                angular.forEach(value, (kindValue, kindKey) => {
                    let kindKeyString = (kindKey !== 'default' ? ':' + kindKey : '')
                    let style = key + kindKeyString + '{'
                    angular.forEach(kindValue, (value2, key2) => {
                        key2 = key2.replace(/[A-Z]/, (match) => {
                            return '-' + match.toLowerCase()
                        })
                        style += key2 + ': ' + value2 + ';'
                    })
                    style += '}'
                    customCss += style
                })
            })
            setUpStyleCss(css)
            $rootScope.setUpCustomStyleCss(customCss)
        }
        $rootScope.styleId = null
        $scope.newCss = {}
        $rootScope.loadStyle = (cssId) => {
            $rootScope.styleId = cssId
            if (!$rootScope.styleId) {
                return
            }
            $scope.newCss = {}
            $http.get(apiBase + '/styles/' + $rootScope.styleId + '/css').then((response) => {
                const css = response.data.css
                $scope.pageFonts = response.data.fonts
                angular.forEach(css, (value, key) => {
                    const tagName = value.tag
                    let suffix = 'default'
                    const tag = value.tag.split(':')
                    if (tag[1]) {
                        suffix = tag[1]
                    }
                    angular.forEach(value.css, (value2, key2) => {
                        let parameterName = value2.parameter.replace(/-[a-z]/, (match) => {
                            return match.substring(1).toUpperCase()
                        })
                        if (!$scope.newCss[tagName]) {
                            $scope.newCss[tagName] = {}
                        }
                        if (!$scope.newCss[tagName][suffix]) {
                            $scope.newCss[tagName][suffix] = {}
                        }
                        if (!$scope.newCss[tagName][suffix][parameterName]) {
                            $scope.newCss[tagName][suffix][parameterName] = ''
                        }
                        $scope.newCss[tagName][suffix][parameterName] = value2.value
                    })
                })
                /*const timeoutCallback = () => {
                    if (!frames || !frames['viewFrame'] || !frames['viewFrame'].document ||
                        !frames['viewFrame'].document.body) {
                        $timeout(timeoutCallback, 250)
                    } else {
                        $scope.pageFontsChange()
                        $scope.styleChange()
                    }
                }
                $timeout(timeoutCallback, 250)*/
                $scope.pageFontsChange()
                $scope.styleChange()
            })
        }
        $rootScope.customCssStyle = {}
        $rootScope.loadCustomStyle = (cssId) => {
            if (!cssId) {
                return
            }
            $http.get(apiBase + '/styles/' + cssId + '/css').then((response) => {
                const css = response.data.css
                $scope.pageFonts = response.data.fonts
                angular.forEach(css, (value, key) => {
                    const tagName = value.tag
                    let suffix = 'default'
                    const tag = value.tag.split(':')
                    if (tag[1]) {
                        suffix = tag[1]
                    }
                    angular.forEach(value.css, (value2, key2) => {
                        let parameterName = value2.parameter.replace(/-[a-z]/, (match) => {
                            return match.substring(1).toUpperCase()
                        })
                        if (!$rootScope.customCssStyle[tagName]) {
                            $rootScope.customCssStyle[tagName] = {}
                        }
                        if (!$rootScope.customCssStyle[tagName][suffix]) {
                            $rootScope.customCssStyle[tagName][suffix] = {}
                        }
                        if (!$rootScope.customCssStyle[tagName][suffix][parameterName]) {
                            $rootScope.customCssStyle[tagName][suffix][parameterName] = ''
                        }
                        $rootScope.customCssStyle[tagName][suffix][parameterName] = value2.value
                    })
                })
                /*const timeoutCallback = () => {
                    if (!frames || !frames['viewFrame'] || !frames['viewFrame'].document ||
                        !frames['viewFrame'].document.body) {
                        $timeout(timeoutCallback, 250)
                    } else {
                        $scope.pageFontsChange()
                        $scope.styleChange()
                    }
                }
                $timeout(timeoutCallback, 250)*/
                $scope.pageFontsChange()
                $scope.styleChange()
            })
        }
        $scope.saveCustomStyle = () => {
            let css = []
            let fonts = []
            angular.forEach($rootScope.customCssStyle, (value, key) => {
                angular.forEach(value, (kindValue, kindKey) => {
                    let kindKeyString = (kindKey !== 'default' ? kindKey : '').split(':')
                    let keyArray = key.split(':')
                    let newTag = []
                    const distinct = (array) => {
                        angular.forEach(array, (value, key) => {
                            let find = false
                            angular.forEach(newTag, (value2, key2) => {
                                if (value2 == value) {
                                    find = true
                                }
                            })
                            if (!find && value && (value !== '')) {
                                newTag.push(value)
                            }
                        })
                    }
                    distinct(keyArray)
                    distinct(kindKeyString)
                    let tagRaw = newTag.join(':')
                    let style = {
                        tag: tagRaw,
                        css: [],
                    }
                    angular.forEach(kindValue, (value2, key2) => {
                        key2 = key2.replace(/[A-Z]/, (match) => {
                            return '-' + match.toLowerCase()
                        })
                        style.css.push({
                            parameter: key2,
                            value: value2
                        })
                        if (key2 == 'font-family') {
                            let name = value2.split('\'')
                            name = name[1]
                            fonts.push(name)
                        }
                    })
                    css.push(style)
                })
            })
            $http.post(apiBase + '/styles/' + $scope.customStyleId + '/css', {
                css: css,
            }).then((response) => {
                loader.hide()
            })
        }
    })