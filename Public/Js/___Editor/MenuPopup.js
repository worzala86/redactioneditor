angular.module('ngMenuPopup', ['ngModal'])

    .controller('menuPopupController', function ($scope, $rootScope, $timeout, dialog, Upload, loader, $http,
                                                 confirmPopup) {
        const frame = document.querySelector('#viewFrame')
        const parentScope = angular.element(frame).scope()
        $scope.selectStyleElement = (event, element) => {
            $rootScope.leftPanel = 'editStyle'
            //$timeout(() => {
                const left = document.querySelector('.my-scroll-area-left')
                if (!parentScope.psl) {
                    parentScope.psl = new PerfectScrollbar(left);
                }
                parentScope.psl.update()
                //$rootScope.scrollPanels()
            //}, 100)
        }
        const searchParentElement = (element, elements) => {
            let parentElement = null
            if (!elements || !elements.childrens) {
                return
            }
            angular.forEach(elements.childrens, (value, key) => {
                let result = null
                if (value.id == element.id) {
                    parentElement = elements
                }
                if (result && !parentElement) {
                    parentElement = elements
                }
                result = searchParentElement(element, value)
                if (result && !parentElement) {
                    parentElement = result
                }
            })
            return parentElement
        }
        $scope.selectParent = (event) => {
            const element = $rootScope.selectedElement
            const setSelectedElement = (element, elements) => {
                if ((parentElement = searchParentElement(element, elements)) && parentElement
                    && (parentElement !== parentScope.viewBody)
                    && (parentElement !== parentScope.viewHeader)
                    && (parentElement !== parentScope.viewFooter)
                    && (parentElement !== parentScope.viewSidebarLeft)
                    && (parentElement !== parentScope.viewSidebarRight)) {
                    $rootScope.selectedElement = parentElement
                    $rootScope.elementToAddNewElement = $rootScope.selectedElement
                    $rootScope.style = $rootScope.selectedElement.style
                    return true
                }
                return false
            }
            if (!(setSelectedElement(element, parentScope.viewBody) ||
                setSelectedElement(element, parentScope.viewHeader) ||
                setSelectedElement(element, parentScope.viewSidebarRight) ||
                setSelectedElement(element, parentScope.viewSidebarLeft) ||
                setSelectedElement(element, parentScope.viewFooter))) {
                $rootScope.selectedElement = null
                $rootScope.elementToAddNewElement = null
                angular.element(document.querySelector('#element-menu-popup')).remove()
            }
        }
        const searchElements = (node, elem) => {
            let elements = null
            let index = null
            const searchElementsAndIndex = (n, element) => {
                angular.forEach(n.childrens, (value, key) => {
                    if (elem.id == value.id) {
                        elements = n.childrens
                        index = key
                    }
                    if (value.childrens) {
                        searchElementsAndIndex(value, element)
                    }
                })
                return elements
            }
            searchElementsAndIndex(node, elem)
            return {
                elements: elements,
                index: index,
            }
        }
        const moveInArray = (elements, element, index, direction = 'up') => {
            if (((direction == 'up') && (elements && index > 0)) || ((direction == 'down') && (elements && index < elements.length))) {
                elements.splice(index, 1)
                elements.splice(((direction == 'up') ? (index - 1) : (index + 1)), 0, element)
                $timeout(() => {
                    $rootScope.selectedElement = element
                }, 1)
            }
        }
        $scope.moveUp = (event) => {
            const element = $rootScope.selectedElement
            let result = searchElements(parentScope.viewHeader, element)
            if (result.index == null) {
                result = searchElements(parentScope.viewSidebarLeft, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewBody, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewSidebarRight, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewFooter, element)
            }
            moveInArray(result.elements, element, result.index, 'up')
            $timeout(() => {
                parentScope.showElementMenuPopup(event, element)
            }, 1)
        }
        $scope.moveDown = (event) => {
            const element = $rootScope.selectedElement
            let result = searchElements(parentScope.viewHeader, element)
            if (result.index == null) {
                result = searchElements(parentScope.viewSidebarLeft, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewBody, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewSidebarRight, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewFooter, element)
            }
            moveInArray(result.elements, element, result.index, 'down')
            $timeout(() => {
                parentScope.showElementMenuPopup(event, element)
            }, 1)
        }
        const getId = (length = 8) => {
            var text = "";
            var possible = "abcdef0123456789";
            for (var i = 0; i < length; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }
        const elementExistsInElement = (find, element) => {
            let result = false
            if (element.childrens) {
                angular.forEach(element.childrens, (value, key) => {
                    result = result || elementExistsInElement(find, value)
                })
            }
            result = result || (find && (element.id == find.id))
            return result
        }
        $scope.saveElementStyle = (element, callback) => {
            let css = []
            let fonts = []
            angular.forEach($rootScope.customCssStyle, (value, key) => {
                let exists = false
                const idExists = (node) => {
                    const id = node.altId ? node.altId : node.id
                    if (('#id' + id) == key) {
                        exists = true
                    }
                    angular.forEach(node.childrens, (val, k) => {
                        idExists(val)
                    })
                }
                idExists(element)
                if (!exists) {
                    return
                }
                angular.forEach(value, (kindValue, kindKey) => {
                    let kindKeyString = (kindKey !== 'default' ? kindKey : '').split(':')
                    let keyArray = key.split(':')
                    let newTag = []
                    const distinct = (array) => {
                        angular.forEach(array, (value, key) => {
                            let find = false
                            angular.forEach(newTag, (value2, key2) => {
                                if (value2 == value) {
                                    find = true
                                }
                            })
                            if (!find && value && (value !== '')) {
                                newTag.push(value)
                            }
                        })
                    }
                    distinct(keyArray)
                    distinct(kindKeyString)
                    let tagRaw = newTag.join(':')
                    let style = {
                        tag: tagRaw,
                        css: [],
                    }
                    angular.forEach(kindValue, (value2, key2) => {
                        key2 = key2.replace(/[A-Z]/, (match) => {
                            return '-' + match.toLowerCase()
                        })
                        style.css.push({
                            parameter: key2,
                            value: value2
                        })
                        if (key2 == 'font-family') {
                            let name = value2.split('\'')
                            name = name[1]
                            fonts.push(name)
                        }
                    })
                    css.push(style)
                })
            })
            $http.post(apiBase + '/styles/css', {
                css: css,
            }).then((response) => {
                if (callback) {
                    callback(response.data.id)
                }
                loader.hide()
            })
        }
        const saveElement = (saveOptions) => {
            loader.show()
            $timeout(() => {
                const callback = (styleId) => {
                    saveOptions.data.styleId = styleId
                    saveOptions.data.rootStyleId = $rootScope.styleId
                    const id = $rootScope.selectedElement.altId ? $rootScope.selectedElement.altId : $rootScope.selectedElement.id
                    const element = frames['viewFrame'].document.body.querySelector('#id' + id)
                    $rootScope.selectedElement = null
                    $http.post(apiBase + '/elements', saveOptions.data).then((response) => {
                    })
                    if (saveOptions.callback) {
                        saveOptions.callback()
                    }
                }
                $scope.saveElementStyle($rootScope.selectedElement, callback)
            }, 100)
        }
        $scope.saveElementModal = (element) => {
            dialog({
                templateUrl: templateRoot + 'Editor/Modals/SaveElementModal.html',
                scope: {
                    sections: parentScope.sections,
                    newElement: {
                        kind: 'body'
                    }
                },
                buttons: [
                    {
                        title: 'Zapisz',
                        callback: (scope) => {
                            saveElement({
                                data: {
                                    content: parentScope.getContent(element),
                                    kind: scope.newElement.kind,
                                },
                                callback: (result) => {
                                    scope.close()
                                }
                            })
                        }
                    },
                    {
                        title: 'Anuluj',
                        callback: (scope) => {
                            scope.close()
                        }
                    }
                ]
            }).show()
        }
    })