angular.module('RedActionLookup', ['ngRoute', 'ngSanitize'])

    .config(function ($routeProvider, $locationProvider, $compileProvider, $httpProvider) {
        angular.forEach(routes, (value, key)=>{
            $routeProvider
                .when(value.url, {
                    templateUrl: value.template,
                    controller: '3ViewController',
                })
        })

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $compileProvider.debugInfoEnabled(true)
        delete $httpProvider.defaults.headers.common['X-Requested-With']
        $httpProvider.defaults.useXDomain = true
        $httpProvider.defaults.withCredentials = true
    })

    .run(function ($rootScope, $http, $location, $route, $compile, $timeout) {
        $rootScope.baseUrl = base
        $rootScope.$on('$routeChangeStart', function ($event, next, current) {
            angular.element(document.querySelector('#preloader')).css({'display': 'block'})
        });
        angular.element(document).ready(function () {
            $timeout(() => {
                angular.element(document.querySelector('#preloader')).css({'display': 'none'})
            }, 1000)
        });
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
        })
    })

    .controller('3ViewController', function ($scope, $rootScope, $http, $sanitize, $routeParams, $timeout) {
        $scope.projectId = projectId
        $scope.paginationLimit = 10
        $scope.viewBody = {}
        $scope.viewHeader = {}
        $scope.viewFooter = {}
        $scope.viewSidebarRight = {}
        $scope.viewSidebarLeft = {}
        const setDataSets = (element) => {
            $scope.loadDataSet(element)
            angular.forEach(element.childrens, (value, key) => {
                setDataSets(value)
            })
        }
        const loadData = (data, element) => {
            if (!element.childrens) {
                element.childrens = []
            }
            if (data && data.childrens) {
                element.childrens = data.childrens
            }
            if (!$rootScope.dataSet) {
                $rootScope.dataSet = {}
            }
            const loadDataSet = (dataSet, value) => {
                if (!value) {
                    return
                }
                if (dataSet && dataSet[value.field]) {
                    $rootScope.dataSet[value.field] = dataSet[value.field]
                }
                if (dataSet && dataSet[value.src]) {
                    $rootScope.dataSet[value.src] = dataSet[value.src]
                }
                if (dataSet && dataSet[value.alt]) {
                    $rootScope.dataSet[value.alt] = dataSet[value.alt]
                }
                if (dataSet && dataSet[value.altTitle]) {
                    $rootScope.dataSet[value.altTitle] = dataSet[value.altTitle]
                }
                if (dataSet && dataSet[value.placeholder]) {
                    $rootScope.dataSet[value.placeholder] = dataSet[value.placeholder]
                }
                if (dataSet && dataSet[value.html]) {
                    $rootScope.dataSet[value.html] = dataSet[value.html]
                }
            }
            const setDataSet = (dataset, element) => {
                loadDataSet(dataset, element)
                if (element.childrens) {
                    angular.forEach(element.childrens, (value, key) => {
                        setDataSet(dataset, value)
                    })
                }
            }
            setDataSet(data.dataSet, element)
            setDataSets(element)
        }
        const loadDataSet = (dataSet) => {
            //if (!value) {
            //    return
            //}
            //if (dataSet && dataSet[value.field]) {
                $rootScope.dataSet = dataSet
            /*}
            if (dataSet && dataSet[value.src]) {
                $rootScope.dataSet[value.src] = dataSet[value.src]
            }
            if (dataSet && dataSet[value.alt]) {
                $rootScope.dataSet[value.alt] = dataSet[value.alt]
            }
            if (dataSet && dataSet[value.altTitle]) {
                $rootScope.dataSet[value.altTitle] = dataSet[value.altTitle]
            }
            if (dataSet && dataSet[value.placeholder]) {
                $rootScope.dataSet[value.placeholder] = dataSet[value.placeholder]
            }
            if (dataSet && dataSet[value.html]) {
                $rootScope.dataSet[value.html] = dataSet[value.html]
            }*/
        }
        $scope.loadViewElement = (url) => {
            $http.post(apiBase + '/projects/' + $scope.projectId + '/pages/url', {url: url}).then((response) => {
                $scope.paginationUrl = response.data.paginationUrl ? response.data.paginationUrl : ''
                $scope.paginationUrlParam = response.data.paginationUrlParam ? response.data.paginationUrlParam : ''
                $scope.paginationLimit = response.data.paginationLimit ? response.data.paginationLimit : $scope.paginationLimit
                $scope.config = response.data.config
                $rootScope.showLeftSidebar = response.data.showLeftSidebar
                $rootScope.showRightSidebar = response.data.showRightSidebar

                loadDataSet(response.data.dataSet)

                loadData(response.data.body, $scope.viewBody)
                loadData(response.data.header, $scope.viewHeader)
                loadData(response.data.footer, $scope.viewFooter)
                loadData(response.data.sidebarLeft, $scope.viewSidebarLeft)
                loadData(response.data.sidebarRight, $scope.viewSidebarRight)
            })
        }
        const setNewDataSet = (selector)=>{
            const setDataSetForAllChildrens = (element) => {
                angular.forEach($rootScope.dataSet[selector], (value, key) => {
                    if ($rootScope.dataSet[element.elementPositionId][element.field]) {
                        $rootScope.dataSet[element.elementPositionId][element.field] = $rootScope.dataSet[element.elementPositionId][element.field]
                    }
                    if ($rootScope.dataSet[element.elementPositionId][element.src]) {
                        $rootScope.dataSet[element.elementPositionId][element.src] = $rootScope.dataSet[element.elementPositionId][element.src]
                    }
                    if ($rootScope.dataSet[element.elementPositionId][element.alt]) {
                        $rootScope.dataSet[element.elementPositionId][element.alt] = $rootScope.dataSet[element.elementPositionId][element.alt]
                    }
                    if ($rootScope.dataSet[element.elementPositionId][element.placeholder]) {
                        $rootScope.dataSet[element.elementPositionId][element.placeholder] = $rootScope.dataSet[element.elementPositionId][element.placeholder]
                    }
                    if ($rootScope.dataSet[element.elementPositionId][element.altTitle]) {
                        $rootScope.dataSet[element.elementPositionId][element.altTitle] = $rootScope.dataSet[element.elementPositionId][element.altTitle]
                    }
                })
                angular.forEach(element.childrens, (value, key) => {
                    setDataSetForAllChildrens(value)
                })
            }
            setDataSetForAllChildrens($scope.viewBody)
            setDataSetForAllChildrens($scope.viewHeader)
            setDataSetForAllChildrens($scope.viewSidebarLeft)
            setDataSetForAllChildrens($scope.viewSidebarRight)
            setDataSetForAllChildrens($scope.viewFooter)
        }
        $scope.loadDataSet = (element) => {
            if (!element) {
                return
            }
            if (!$rootScope.dataSet) {
                $rootScope.dataSet = []
            }
            if (element.selector && $rootScope.dataSet && (!$rootScope.dataSet[element.elementPositionId])
                    && (!$rootScope.dataSet[element.elementPositionId][element.selector])) {
                $rootScope.dataSet[element.elementPositionId] = {}
                $rootScope.dataSet[element.elementPositionId][element.selector] = true
                $scope.pageId = 1
                if ($scope.paginationUrlParam) {
                    if ($routeParams['url' + $scope.paginationUrlParam]) {
                        $scope.pageId = parseInt($routeParams['url' + $scope.paginationUrlParam])
                    } else {
                        $scope.pageId = 1
                    }
                }
                $http.get(apiBase + '/projects/' + $scope.projectId + '/datasets/selector/' + element.selector +
                    '?limit=' + $scope.paginationLimit + '&page=' + $scope.pageId).then((response) => {
                    $rootScope.dataSet[element.elementPositionId][element.selector] = response.data.dataset
                    setNewDataSet(element.selector)
                    $scope.pageTotalCount = response.data.totalCount
                    $scope.pagesTotal = Math.ceil($scope.pageTotalCount / $scope.paginationLimit)
                })
            }
        }
        $scope.loadPage = () => {
            let url = '/'
            if ($routeParams.url1) {
                url = '/' + $routeParams.url1
            }
            if ($routeParams.url2) {
                url = url + '/' + $routeParams.url2
            }
            if ($routeParams.url3) {
                url = url + '/' + $routeParams.url3
            }
            if ($routeParams.url4) {
                url = url + '/' + $routeParams.url4
            }
            if ($routeParams.url5) {
                url = url + '/' + $routeParams.url5
            }
            $scope.loadViewElement(url)
        }
        $scope.loadPage()
        $scope.pageId = 1
        $timeout(() => {
            angular.element(document.querySelector('#preloader')).css({'display': 'none'})
        }, 1000)
    })
;