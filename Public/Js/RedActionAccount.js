var base = '/konto';
var apiBase = '/api';
var templateBase = '/Public/Templates/';

angular.module('RedActionAccount', ['ngRoute', 'ngSanitize', 'ngFileUpload', 'color.picker', 'chart.js',
    'ngModal', 'ngPopup',
    'ngSimpleRedActionEditor'])

    .config(function ($routeProvider, $locationProvider, $compileProvider, $httpProvider, $rootScopeProvider) {
        $routeProvider
            .when(base + '/', {
                templateUrl: templateBase + 'Dashboard.html',
                controller: 'dashboardController',
            })
            .when(base + '/start', {
                templateUrl: templateBase + 'Dashboard.html',
                controller: 'dashboardController',
            })
            .when(base + '/projekty', {
                templateUrl: templateBase + 'Projects/List.html',
                controller: 'projectsController',
                logged: true,
            })
            .when(base + '/projekt/nowy', {
                templateUrl: templateBase + 'Projects/Edit.html',
                controller: 'projectsEditController',
                logged: true,
            })
            .when(base + '/projekt/:projectId', {
                templateUrl: templateBase + 'Projects/Edit.html',
                controller: 'projectsEditController',
                logged: true,
            })
            .when(base + '/projekt/:id/edycja', {
                templateUrl: templateBase + 'SimpleEditor/RedActionEditor.html',
                controller: 'Simple3ViewEditorController',
                logged: true,
            })
            .when(base + '/projekt/:id/datasety', {
                templateUrl: templateBase + 'SimpleEditor/DataSets.html',
                controller: 'datasetsController',
                logged: true,
            })
            .when(base + '/projekt/:id/style', {
                templateUrl: templateBase + 'Styles/SelectStyle.html',
                controller: 'selectStylesController',
                logged: true,
            })
            /*.when(base + '/projekt/:id/rodzaj', {
                templateUrl: templateBase + 'SimpleEditor/SelectKind.html',
                controller: 'selectKindController',
                logged: true,
            })
            .when(base + '/projekt/:id/:kind/styl', {
                templateUrl: templateBase + 'SimpleEditor/SelectStyle.html',
                controller: 'selectStyleController',
                logged: true,
            })
            .when(base + '/projekt/:id/:kind/szablon', {
                templateUrl: templateBase + 'SimpleEditor/SelectTemplate.html',
                controller: 'selectTemplateController',
                logged: true,
            })*/
            .when(base + '/profil', {
                templateUrl: templateBase + 'Profile.html',
                controller: 'profileEditController',
                logged: true,
            })
            .when(base + '/projekt', {
                templateUrl: templateBase + 'Project.html',
                controller: 'projectController',
                logged: true,
            })
        ;

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $compileProvider.debugInfoEnabled(true)
        delete $httpProvider.defaults.headers.common['X-Requested-With']
        $httpProvider.defaults.useXDomain = true
        $httpProvider.defaults.withCredentials = true

        $rootScopeProvider.digestTtl(100)
    })

    .run(function ($rootScope, $http, $location, $route, $compile, popup, $window, $timeout) {
        $rootScope.baseUrl = base
        $rootScope.user = null
        let path = null
        $rootScope.logout = function () {
            angular.element(document.querySelector('#user-menu-popup')).remove()
            $http.get(apiBase + '/logout').then(function (response) {
                if (response.data.success) {
                    $rootScope.user.logged = false
                    window.location = '/'
                }
            });
        }
        $rootScope.loadUserStatus = () => {
            $http.get(apiBase + '/user-status').then(function (response) {
                $rootScope.user = {
                    logged: response.data.logged,
                    admin: response.data.isAdmin,
                    projectsCount: response.data.projectsCount,
                    firstName: response.data.firstName,
                    lastName: response.data.lastName,
                }
                if (!$rootScope.user.logged && (window.location.pathname != '/logowanie')) {
                    window.location = '/logowanie'
                }
            })
        }
        $rootScope.loadUserStatus()
        $rootScope.$on('$routeChangeStart', function ($event, next, current) {
            if (next && (next.logged == true) && $rootScope.user && !$rootScope.user.logged) {
                if (!$rootScope.user.logged) {
                    window.location = '/logowanie'
                } else {
                    window.location = '/'
                }
            }
        });
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
        })
        $rootScope.cursorX;
        $rootScope.cursorY;
        document.onmousemove = function (e) {
            $rootScope.cursorX = e.pageX;
            $rootScope.cursorY = e.pageY;
        }
        const calculateGrid = () => {
            //$rootScope.$apply(() => {
            const width = document.body.clientWidth
            let gridStyle = 'xs'
            if (width >= 500) gridStyle = 'sm'
            if (width >= 700) gridStyle = 'md'
            if (width >= 900) gridStyle = 'lg'
            if (width >= 1100) gridStyle = 'xl'
            $rootScope.gridStyle = gridStyle
            //})
        }
        $rootScope.resizeView = (extraHeight = 0, screenWidth = null) => {
            if (screenWidth) {
                $rootScope.screenWidth = screenWidth
            }
            const element = document.querySelector('iframe#viewFrame')
            if (!element) {
                $timeout($rootScope.resizeView, 100)
                return
            }
            if (!element.contentDocument) {
                $timeout($rootScope.resizeView, 100)
                return
            }
            const editor = element.contentDocument.querySelector('#editor')
            if (!editor) {
                $timeout($rootScope.resizeView, 100)
                return
            }
            let width = $rootScope.screenWidth
            let left = 0
            let right = 0
            let leftPanelWidth = 0
            let rightPanelWidth = 0
            let marginRight = 65

            if ($rootScope.leftPanel) {
                leftPanelWidth = 300
                if (($rootScope.selectedElementsShow == 'elements')
                    || ($rootScope.leftPanel == 'configure')) {
                    leftPanelWidth = 400
                }
            }
            if ($rootScope.rightPanel) {
                rightPanelWidth = 300
            }

            width = width - (leftPanelWidth + rightPanelWidth) + (document.documentElement.clientWidth - width)
            if (width > 1280) {
                width = 1280
            }
            if ($rootScope.selectedElement) {
                width -= marginRight
            }
            if (leftPanelWidth) {
                left = leftPanelWidth + 15
                width -= 15
            } else if (rightPanelWidth) {
                left = 15
                width -= 15
            }
            if (rightPanelWidth) {
                right = rightPanelWidth + 15
                width -= 15
            } else if (leftPanelWidth) {
                width -= 15
            }
            if ((left == 0) && (right == 0)) {
                left = (document.documentElement.clientWidth - width) / 2
            }
            if ($rootScope.screenWidth < 1280) {
                if (screenWidth < width) {
                    let difference = width - $rootScope.screenWidth
                    width = screenWidth
                    left += difference / 2
                }
            }

            const style = window.getComputedStyle(editor)
            let height = style.height
            angular.element(element).css('width', width + 'px')
            angular.element(element).css('left', left + 'px')
            angular.element(element).css('right', right + 'px')
            angular.element(document.querySelector('resize')).css('padding-left', left + 'px')
            angular.element(document.querySelector('resize')).css('width', width + 'px')
            angular.element(document.querySelector('#add-links')).css('padding-left', left + 'px')
            angular.element(document.querySelector('#add-links')).css('width', width + 'px')
            angular.element(element).css('height', (parseInt(height) + extraHeight + 1) + 'px')
            angular.element(element.contentDocument.body).css('height', (parseInt(height) + extraHeight) + 'px')
            if (document.querySelector('#viewBuffor')) {
                angular.element(document.querySelector('#viewBuffor')).css('width', width + 'px')
                angular.element(document.querySelector('#viewBuffor')).css('left', left + 'px')
                angular.element(document.querySelector('#viewBuffor')).css('right', right + 'px')
            }
        }
        angular.element(window).bind('resize', () => {
            calculateGrid()
            $rootScope.resizeView()
        })
        calculateGrid()
        $rootScope.resizeView(null, 1280)
        const autoResize = () => {
            $rootScope.resizeView()
            $timeout(autoResize, 1000)
        }
        autoResize()

        $rootScope.showUserMenu = () => {
            const element = angular.element(document.querySelector('#user-menu-popup'))
            if (element.length) {
                element.remove()
                return
            }
            popup({
                templateUrl: templateBase + 'UserMenuPopup.html',
                posY: 46,
                scope: {
                    close: () => {
                        angular.element(document.querySelector('#user-menu-popup')).remove()
                    },
                }
            }).show()
        }

        $rootScope.scrollPanels = () => {
            /*const panelLeft = angular.element(document.querySelector('editor-left'))
            const panelRight = angular.element(document.querySelector('editor'))
            const offsetY = window.pageYOffset
            let top = 56 - offsetY
            if (top < 0) {
                top = 0
            }
            panelLeft.css('top', top + 'px')
            panelRight.css('top', top + 'px')*/
        }
    })

    .controller('datasetsController', function ($scope, $http, $location, $routeParams, $rootScope, confirmPopup) {
        $scope.datasets = []
        $scope.addDataSet = (newDataSet, datasets) => {
            $http.post(apiBase + '/projects/' + $routeParams.id + '/datasets', {name: newDataSet.name}).then((response) => {
                if (response.data.id) {
                    datasets.unshift({
                        name: response.data.name,
                        id: response.data.id
                    })
                }
            })
        }
        $scope.deleteDataSet = (datasets, dataset) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/projects/' + $routeParams.id + '/datasets/' + dataset.id).then((response) => {
                    if (response.data.success) {
                        let index = null
                        angular.forEach(datasets, (value, key) => {
                            if (value.id == dataset.id) {
                                index = key
                            }
                        })
                        if (index !== null) {
                            datasets.splice(index, 1)
                        }
                    }
                })
            })
        }
        $scope.deleteDataSetField = (dataset, fields, field) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/projects/' + $routeParams.id + '/datasets/' + dataset.id + '/fields/' + field.id).then((response) => {
                    if (response.data.success) {
                        let index = null
                        angular.forEach(fields, (value, key) => {
                            if (value.id == field.id) {
                                index = key
                            }
                        })
                        if (index !== null) {
                            fields.splice(index, 1)
                        }
                    }
                })
            })
        }
        $scope.message = {}
        $scope.addDataSetField = (message, newDataSetField, dataset) => {
            if (!newDataSetField) {
                message.text = 'Wprowadź dane nowej pozycji!'
                return
            } else {
                message.text = null
            }
            if (!dataset.fields) {
                dataset.fields = []
            }
            if (!newDataSetField || !newDataSetField.name) {
                message.text = 'Wpisz nazwę pola!'
                return
            } else {
                message.text = null
            }
            if (!newDataSetField || !newDataSetField.type) {
                message.text = 'Wybierz typ pola!'
                return
            } else {
                message.text = null
            }
            $http.post(apiBase + '/projects/' + $routeParams.id + '/datasets/' + dataset.id + '/fields', {
                name: newDataSetField.name,
                type: newDataSetField.type
            }).then((response) => {
                if (response.data.id) {
                    dataset.fields.push({
                        name: newDataSetField.name,
                        type: newDataSetField.type,
                        id: response.data.id,
                    })
                    newDataSetField.name = null
                    newDataSetField.type = null
                }
            })
        }
        $scope.demoDataSet = (dataset) => {
            $http.put(apiBase + '/projects/' + $routeParams.id + '/datasets/' + dataset.id + '/demo').then((response) => {
            })
        }
        $scope.demoClearDataSet = (dataset) => {
            $http.delete(apiBase + '/projects/' + $routeParams.id + '/datasets/' + dataset.id + '/demo').then((response) => {
            })
        }
        const loadDataSets = () => {
            $http.get(apiBase + '/projects/' + $routeParams.id + '/datasets').then((response) => {
                $rootScope.dataSets = response.data.datasets
            })
        }
        loadDataSets()
    })

    .controller('projectController', function ($scope, $http, $location, $routeParams, $rootScope, confirmPopup) {
        $rootScope.datasetLabels = [];
        $rootScope.datasetData = [];
        $rootScope.datasetSeries = [];
        $http.get(apiBase + '/projects/active').then((response) => {
            $scope.project = response.data
            let prevValue = 100 * Math.random()
            angular.forEach(Object.assign({}, $scope.project.dataset), (value, key) => {
                $rootScope.datasetLabels.push(key + '')
                value = 100 * Math.random()
                prevValue = value
                value = value + ((4 * Math.random()) - 2)
                $rootScope.datasetData.push(40 + parseInt(value))
            })
        })
        $rootScope.datasetDatasetOverride = [{yAxisID: 'y-axis-1'}, {yAxisID: 'y-axis-2'}];
        $rootScope.datasetOptions = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        }
        $scope.delete = () => {
            confirmPopup(() => {
                $http.delete(apiBase + '/projects/active').then((response) => {
                    if (response.data.success) {
                        $rootScope.user.projectsCount--
                        if ($rootScope.user.projectsCount == 0) {
                            $location.path(base + '/start')
                        } else {
                            $location.path(base + '/projekty')
                        }
                    }
                })
            }, {
                title: 'Czy na pewno chcesz usunąć ten projekt',
                confirmButtonText: 'Usuń',
            })
        }
    })

    .controller('selectStylesController', function ($scope, $http, $location, $routeParams, $rootScope, confirmPopup) {
        $http.get(apiBase + '/styles/public?page=' + $scope.pagePublic + '&limit=' + $scope.limitPublic).then((response) => {
            $scope.publicStyles = response.data.styles
        })
        $scope.selectStyle = (style) => {
            $http.put(apiBase + '/projects/' + $routeParams.id + '/styles/' + style.id, {}).then((response) => {
                if (response.data.success) {
                    $location.path(base + '/projekt')
                }
            })
        }
    })

    .controller('profileEditController', function ($scope, $http, $location, $routeParams, $rootScope) {
        $http.get(apiBase + '/profil').then((response) => {
            $scope.firstName = response.data.firstName;
            $scope.lastName = response.data.lastName;
        })
        $scope.save = () => {
            $http.post(apiBase + '/profil', {
                firstName: $scope.firstName,
                lastName: $scope.lastName
            }).then((response) => {
            })
        }
        $scope.savePassword = () => {
            $http.post(apiBase + '/password', {
                oldPassword: sha512($scope.oldPassword),
                newPassword: sha512($scope.newPassword)
            }).then((response) => {
            })
        }
    })

    /*.controller('selectTemplateController', function ($scope, $http, $location, $routeParams, $rootScope, loader) {
        $scope.projectId = $routeParams.id
        $http.post(apiBase + '/projects/' + $routeParams.id + '/find-by-tag', {tag: $routeParams.kind}).then((response) => {
            $scope.projects = response.data.projects
        })
        $scope.selectProject = (project) => {
            loader.show()
            $http.post(apiBase + '/projects/' + project.id + '/fork', {
                soft: true,
                newId: $routeParams.id
            }).then((response) => {
                if (response.data.id) {
                    $location.path(base + '/projekt/' + project.id);
                }
            })
        }
    })*/

    /*.controller('selectKindController', function ($scope, $http, $location, $routeParams, $rootScope) {
        $scope.select = (kind) => {
            $location.path(base + '/projekt/' + $routeParams.id + '/' + kind + '/styl')
        }
    })*/

    /*.controller('selectStyleController', function ($scope, $http, $location, $routeParams, $rootScope) {
        $scope.pagePublic = 1
        $scope.limitPublic = 10
        $scope.loadNextPublic = () => {
            $scope.page++
            $http.get(apiBase + '/styles/public?page=' + $scope.page + '&limit=' + $scope.limit).then((response) => {
                $scope.page++
                $scope.totalCount = response.data.totalCount
                $scope.pages = Math.ceil($scope.totalCount / $scope.limit)
                angular.forEach(response.data.styles, (value, key) => {
                    $scope.styles.push(value)
                })
            })
        }
        $http.get(apiBase + '/styles/public?page=' + $scope.pagePublic + '&limit=' + $scope.limitPublic).then((response) => {
            $scope.publicStyles = response.data.styles
        })
        $scope.selectStyle = (style) => {
            $http.put(apiBase + '/projects/' + $routeParams.id + '/styles/' + style.id, {}).then((response) => {
                if (response.data.success) {
                    $location.path(base + '/projekt/' + $routeParams.id + '/' + $routeParams.kind + '/szablon')
                }
            })
        }
    })*/

    .controller('projectsEditController', function ($scope, $http, $location, $routeParams, $rootScope) {
        if ($routeParams.projectId) {
            $http.get(apiBase + '/projects/' + $routeParams.projectId).then((response) => {
                $scope.project = response.data
            })
        }
        $scope.require = {
            name: true
        }
        $scope.save = () => {
            if (!$scope.project.name) {
                return
            }
            if ($routeParams.projectId) {
                $http.put(apiBase + '/projects/' + $routeParams.projectId, $scope.project).then((response) => {
                    if (response.data.success) {
                        $rootScope.projectsCount = true
                        $location.path(base + '/projekt')
                    }
                })
            } else {
                $http.post(apiBase + '/projects', $scope.project).then((response) => {
                    if (response.data.id) {
                        $rootScope.projectsCount = true
                        $location.path(base + '/projekt/' + response.data.id + '/rodzaj')
                    }
                })
            }
        }
    })

    .controller('publicStylesController', function ($scope, $http, loader, $location) {
        $scope.styles = []
        $scope.page = 1
        $scope.limit = 10
        $scope.totalCount = 0
        const loadNext = () => {
            $http.get(apiBase + '/styles/public?page=' + $scope.page + '&limit=' + $scope.limit).then((response) => {
                $scope.page++
                $scope.totalCount = response.data.totalCount
                $scope.pages = Math.ceil($scope.totalCount / $scope.limit)
                angular.forEach(response.data.styles, (value, key) => {
                    $scope.styles.push(value)
                })
            })
        }
        loadNext()
        $scope.fork = (style) => {
            $http.post(apiBase + '/styles/' + style.id + '/fork').then((response) => {
                if (response.data.id) {
                    $location.path(base + '/moj-styl/' + response.data.id + '/edycja')
                }
            })
        }
        $scope.loadNext = () => {
            loadNext()
        }
    })

    .controller('projectsController', function ($scope, $http, confirmPopup, $rootScope, $location, popup, loader) {
        $scope.projects = []
        $scope.page = 1
        $scope.limit = 10
        $scope.totalCount = 0
        const loadNext = () => {
            $http.get(apiBase + '/projects?page=' + $scope.page + '&limit=' + $scope.limit).then((response) => {
                $scope.page++
                $scope.totalCount = response.data.totalCount
                $scope.pages = Math.ceil($scope.totalCount / $scope.limit)
                angular.forEach(response.data.projects, (value, key) => {
                    $scope.projects.push(value)
                })
                loader.hide()
            })
        }
        loadNext()
        $scope.delete = (projects, project) => {
            confirmPopup(() => {
                $http.delete(apiBase + '/projects/' + project.id).then((response) => {
                    angular.element(document.querySelector('#menu-popup')).remove()
                    let index = null
                    angular.forEach(projects, (value, key) => {
                        if (value.id == project.id) {
                            index = key
                        }
                    })
                    if (index != null) {
                        projects.splice(index, 1)
                    }
                    $rootScope.user.projectsCount = projects.length
                    if (projects.length == 0) {
                        $location.path('/')
                    }
                })
            })
        }
        $scope.loadNext = () => {
            loadNext()
        }
        $scope.setActiveProject = (id) => {
            $http.put(apiBase + '/projects/' + id + '/active').then((response) => {
                if (response.data.success) {
                    $location.path(base + '/projekt')
                }
            })
        }
    })

    .controller('dashboardController', function ($scope, $location, $http, $rootScope) {
    })

    .directive('imageScroll', () => {
        return {
            link: function (scope, element, attrs) {
                let offsetY = 0
                element.css('position', 'relative')
                element.css('overflow', 'hidden')
                element.find('img').css('position', 'absolute')
                element.find('img').css('top', '0px')
                element.bind('mousewheel', function (e) {
                    let prevent = false
                    if (e.deltaY < 0) {
                        offsetY += 30
                    } else {
                        offsetY -= 30
                    }
                    if (offsetY > 0) {
                        offsetY = 0
                        prevent = true
                    }
                    if (offsetY < -(element.find('img')[0].offsetHeight - element[0].offsetHeight)) {
                        offsetY = -(element.find('img')[0].offsetHeight - element[0].offsetHeight)
                        prevent = true
                    }
                    if (element.find('img')[0].offsetHeight < element[0].offsetHeight) {
                        offsetY = 0
                        prevent = true
                    }
                    angular.element(element).find('img').css('top', offsetY + 'px')
                    if (!prevent) {
                        e.stopPropagation()
                        e.preventDefault()
                    }
                })
            }
        }
    })

    .directive("contenteditable", () => {
        return {
            restrict: "A",
            require: "ngModel",
            link: (scope, element, attrs, ngModel) => {
                ngModel.$render = () => {
                    element.html(ngModel.$viewValue || "kuku")
                }
                element.bind("keyup", () => {
                    ngModel.$setViewValue(element.html())
                })
            }
        };
    })

    .directive('elemReady', ($parse) => {
        return {
            restrict: 'A',
            link: ($scope, elem, attrs) => {
                elem.ready(() => {
                    $scope.$apply(() => {
                        let func = $parse(attrs.elemReady)
                        func($scope)
                    })
                })
            }
        }
    })

    .directive('scroll', ($parse) => {
        return {
            restrict: "A",
            link: (scope, element, attrs) => {
                element.ready(() => {
                    scope.$apply(() => {
                        angular.element(document).bind("scroll", function () {
                            let func = $parse(attrs.scroll)
                            func(scope)
                        })
                    })
                })
            }
        }
    })

    .factory('loader', () => {
        const loader = angular.element(document.querySelector('#preloader'))
        return {
            show: () => {
                loader.css({'display': 'block'})
            },
            hide: () => {
                loader.css({'display': 'none'})
            }
        }
    })
;