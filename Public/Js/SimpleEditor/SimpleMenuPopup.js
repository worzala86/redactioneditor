angular.module('ngSimpleElementMenuPopup', ['ngModal'])

    .controller('simpleMenuPopupController', function ($scope, $rootScope, $timeout, dialog, Upload, loader, $http,
                                                        confirmPopup) {
        const frame = document.querySelector('#viewFrame')
        const parentScope = angular.element(frame).scope()
        const searchElements = (node, elem) => {
            let elements = null
            let index = null
            const searchElementsAndIndex = (n, element) => {
                angular.forEach(n.childrens, (value, key) => {
                    if (elem.id == value.id) {
                        elements = n.childrens
                        index = key
                    }
                    if (value.childrens) {
                        searchElementsAndIndex(value, element)
                    }
                })
                return elements
            }
            searchElementsAndIndex(node, elem)
            return {
                elements: elements,
                index: index,
            }
        }
        const moveInArray = (elements, element, index, direction = 'up') => {
            if (((direction == 'up') && (elements && index > 0)) || ((direction == 'down') && (elements && index < elements.length))) {
                elements.splice(index, 1)
                elements.splice(((direction == 'up') ? (index - 1) : (index + 1)), 0, element)
                $timeout(() => {
                    $rootScope.selectedElement = element
                }, 1)
            }
        }
        $scope.moveUp = (event) => {
            const element = $rootScope.selectedElement
            let result = searchElements(parentScope.viewHeader, element)
            if (result.index == null) {
                result = searchElements(parentScope.viewSidebarLeft, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewBody, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewSidebarRight, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewFooter, element)
            }
            moveInArray(result.elements, element, result.index, 'up')
            $timeout(() => {
                $rootScope.showElementMenuPopup(event, element)
            }, 1)
        }
        $scope.moveDown = (event) => {
            const element = $rootScope.selectedElement
            let result = searchElements(parentScope.viewHeader, element)
            if (result.index == null) {
                result = searchElements(parentScope.viewSidebarLeft, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewBody, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewSidebarRight, element)
            }
            if (result.index == null) {
                result = searchElements(parentScope.viewFooter, element)
            }
            moveInArray(result.elements, element, result.index, 'down')
            $timeout(() => {
                $rootScope.showElementMenuPopup(event, element)
            }, 1)
        }
        $scope.deleteElement = (callback) => {
            const element = $rootScope.selectedElement
            confirmPopup(() => {
                const deleteInElements = (elem) => {
                    let index = null
                    angular.forEach(elem.childrens, (value, key) => {
                        if (value == element) {
                            index = key
                        }
                        deleteInElements(value)
                    })
                    if (index != null) {
                        delete (parentScope.dataSet[element.id])
                        elem.childrens.splice(index, 1)
                    }
                }
                /*const repairEmptyContainer = (elem) => {
                    if (!elem.childrens) {
                        elem.childrens = []
                    }
                    if (elem.childrens.length === 0) {
                        addNewElement(elem, 'container')
                    }

                }*/
                deleteInElements(parentScope.viewHeader)
                deleteInElements(parentScope.viewSidebarLeft)
                deleteInElements(parentScope.viewBody)
                deleteInElements(parentScope.viewSidebarRight)
                deleteInElements(parentScope.viewFooter)
                /*repairEmptyContainer(parentScope.viewHeader)
                repairEmptyContainer(parentScope.viewSidebarLeft)
                repairEmptyContainer(parentScope.viewBody)
                repairEmptyContainer(parentScope.viewSidebarRight)
                repairEmptyContainer(parentScope.viewFooter)*/
                $rootScope.selectedElement = null
                $rootScope.elementToAddNewElement = null
                $rootScope.resizeView()
                if (callback) {
                    callback()
                }
            })
        }
    })