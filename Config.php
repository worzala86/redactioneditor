<?php

define('DEBUG', true);

if (DEBUG) {
    error_reporting(E_ALL);
    ini_set('display_errors', true);
}

define('SESSION_TIMEOUT', 60 * 60);//godzina

define('ROOT_DIRECTORY', __DIR__);
define('FILES_DIRECTORY', __DIR__.'/Files');

define('DEFAULT_DATABASE_CONNECTION', 'REDACTION_MASTER');
define('DATABASE_CONNECTION', [
    'REDACTION_MASTER' => [
        'HOST' => 'localhost',
        'DATABASE' => 'redaction',
        'USER' => 'root',
        'PASSWORD' => 'root',
    ],
    'REDACTION_DATASETS' => [
        'HOST' => 'localhost',
        'DATABASE' => 'redaction_datasets',
        'USER' => 'root',
        'PASSWORD' => 'root',
    ],
]);

define('DOMAIN', 'http://redaction.localhost');

define('MAIL_AUTHORIZATION', [
    'HOST' => 'smtp.gmail.com',
    'SMTPAUTH' => true,
    'USER' => 'megazin.automat@gmail.com',
    'PASSWORD' => 'M2fgjzXDFmykaNg',
    'SMTPSECURE' => 'tsl',
    'PORT' => 587,
    'SENT_FROM' => 'megazin.automat@gmail.com',
    'REPLY_TO' => 'megazin.automat@gmail.com',
    'YOUR_NAME' => 'Megazin',
]);

define('DEFAULT_STYLE_ID', 6);