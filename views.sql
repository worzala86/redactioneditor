create or replace view html_style as select styles_id, styles_params.name as kind, `value` from styles_style
     left join styles_params on param_id=styles_params.id
     left join styles_values on value_id=styles_values.id;

create or replace view components_view as select id, uuid, `key`, description, name, file, deleted,
     (select count(*) from html where html.component_id=components.id) as `count`,
     kind, simple_view from components;

create or replace view images_view as select id, uuid, user_id, file_id, pixabay_id, deleted,
  (select count(*) from tags_images where image_id=images.id) as `tags_count` from images;

create or replace view styles_css_view as
SELECT styles_style.id as id, styles_id, styles_params.name as param, styles_values.`value` as `value`, styles_tags.name as tag
FROM styles_style
       left join styles_tags on styles_tags.id=styles_style.tag_id
       left join styles_params on styles_params.id=styles_style.param_id
       left join styles_values on styles_values.id=styles_style.value_id;

create or replace view pages_day_project_count_view as select project_id, count(*) as `count`, DATE_FORMAT(from_unixtime(added), '%Y-%m-%d') as date from pages group by project_id;
create or replace view pages_day_count_view as select sum(`count`) as `count`, date from pages_day_project_count_view as p group by date;

create or replace view pages_month_project_count_view as select project_id, count(*) as `count`, DATE_FORMAT(from_unixtime(added), '%Y-%m') as date from pages group by project_id;
create or replace view pages_month_count_view as select sum(`count`) as `count`, date from pages_month_project_count_view as p group by date;

create or replace view pages_datasets_view as
select pages_datasets.page_id, `key`, `value`, pages_elements.uuid as element_position_id from pages_datasets
    left join pages_elements on pages_elements.id=pages_datasets.element_position_id;

create or replace view projects_datasets_view as
select projects_datasets.project_id, `key`, `value`, projects_elements.uuid as element_position_id from projects_datasets
    left join projects_elements on projects_elements.id=projects_datasets.element_position_id;

create or replace view elements_datasets_view as
SELECT element_id, `key`, `value`, elements.uuid as element_position_id FROM `elements_datasets`
    left join elements on elements.id=elements_datasets.element_position_id