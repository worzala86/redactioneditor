<?php

namespace App\Types;

use App\Exceptions\WrongPageKindException;
use App\Type;

class PageKind extends Type
{
    public function __construct($data = null)
    {
        if(!in_array($data, ['header', 'sidebar-left', 'sidebar-right', 'footer'])){
            throw new WrongPageKindException;
        }
        parent::__construct($data);
    }
}