<?php

namespace App\Types;

use App\Exceptions\WrongElementKindTypeException;
use App\Type;

class ImagesKind extends Type
{
    static $pattern = '(system|custom)';

    public function __construct($data = null)
    {
        if(!in_array($data, ['system', 'custom'])){
            throw new WrongElementKindTypeException;
        }
        parent::__construct($data);
    }
}