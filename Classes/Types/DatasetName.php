<?php

namespace App\Types;

use App\Exceptions\IncorrectDatasetNameException;
use App\Type;

class DatasetName extends Type
{
    static $pattern = '([a-zA-Z_]+[a-zA-Z_0-9]+)';

    public function __construct($data = null)
    {
        preg_match('/^'.self::$pattern.'$/', $data, $matches);
        if(count($matches)==0){
            throw new IncorrectDatasetNameException;
        }
        parent::__construct($data);
    }
}