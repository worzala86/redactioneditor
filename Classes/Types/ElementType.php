<?php

namespace App\Types;

use App\Exceptions\WrongKindTypeException;
use App\Type;

class ElementType extends Type
{
    static $pattern = '(custom|system)';

    public function __construct($data = null)
    {
        if(!in_array($data, ['custom', 'system'])){
            throw new WrongKindTypeException;
        }
        parent::__construct($data);
    }
}