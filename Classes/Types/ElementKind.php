<?php

namespace App\Types;

use App\Exceptions\WrongElementKindTypeException;
use App\Type;

class ElementKind extends Type
{
    static $pattern = '(header|body|sidebar|footer)';

    public function __construct($data = null)
    {
        if(!in_array($data, ['header', 'sidebar', 'body', 'footer'])){
            throw new WrongElementKindTypeException;
        }
        parent::__construct($data);
    }
}