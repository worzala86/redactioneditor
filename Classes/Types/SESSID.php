<?php

namespace App\Types;

use App\Exceptions\WrongSessionCodeException;
use App\Type;

class SESSID extends Type
{
    static $pattern = '([a-f0-9]{32})';

    public function __construct($data = null)
    {
        preg_match('/^'.self::$pattern.'$/', $data, $matches);
        if(empty($matches)){
            throw new WrongSessionCodeException;
        }
        parent::__construct($data);
    }

    public static function fake(){
        $string = '';
        $charset = "0123456789abcdf";
        for ($i = 0; $i < 32; $i++) {
            $string .= $charset[rand(0, strlen($charset) - 1)];
        }
        return new SESSID($string);
    }
}