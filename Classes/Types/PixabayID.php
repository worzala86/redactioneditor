<?php

namespace App\Types;

use App\Exceptions\IncorrectIdException;
use App\Type;

class PixabayID extends Type
{
    static $pattern = '([0-9]+)';

    public function __construct(string $data = null)
    {
        if(!preg_match('/^'.self::$pattern.'$/', $data, $matches)){
           throw new IncorrectIdException;
        }
        parent::__construct($data);
    }
}