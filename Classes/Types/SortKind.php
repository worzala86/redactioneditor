<?php

namespace App\Types;

use App\Exceptions\WrongSortingTypeException;
use App\Type;

class SortKind extends Type
{
    public function __construct($data = null)
    {
        if(!in_array($data, ['desc', 'asc', 'rand'])){
            throw new WrongSortingTypeException;
        }
        parent::__construct($data);
    }
}