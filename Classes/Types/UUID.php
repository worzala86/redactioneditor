<?php

namespace App\Types;

use App\Exceptions\IncorrectIdException;
use App\Type;

class UUID extends Type
{
    static $pattern = '([a-f0-9]{16})';

    public function __construct(string $data = null)
    {
        if(strlen($data) === 8){
            $data = bin2hex($data);
        }
        if(!preg_match('/^'.self::$pattern.'$/', $data, $matches)){
           throw new IncorrectIdException;
        }
        parent::__construct($data);
    }

    public static function fake()
    {
        $string = '';
        $charset = "0123456789abcdf";
        for ($i = 0; $i < 16; $i++) {
            $string .= $charset[rand(0, strlen($charset) - 1)];
        }
        return new UUID($string);
    }
}