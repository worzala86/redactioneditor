<?php

namespace App\Types;

use App\Exceptions\WrongPageKindException;
use App\Type;

class StyleKind extends Type
{
    static $pattern = '(public|private)';

    public function __construct($data = null)
    {
        if(!in_array($data, ['public', 'private'])){
            throw new WrongPageKindException;
        }
        parent::__construct($data);
    }
}