<?php

namespace App\Types;

use App\Exceptions\IncorrectComponentNameException;
use App\Type;

class ComponentKey extends Type
{
    static $pattern = '([a-zA-Z-]+[a-zA-Z-0-9]+)';

    public function __construct($data = null)
    {
        preg_match('/^'.self::$pattern.'$/', $data, $matches);
        if(count($matches)==0){
            throw new IncorrectComponentNameException;
        }
        parent::__construct($data);
    }
}