<?php

namespace App\Types;

use App\Exceptions\WrongDatasetFieldTypeException;
use App\Type;

class DatasetFieldKind extends Type
{
    public function __construct($data = null)
    {
        if(!in_array($data, ['text', 'textarea', 'image', 'date'])){
            throw new WrongDatasetFieldTypeException;
        }
        parent::__construct($data);
    }
}