<?php

namespace App\Types;

use App\Exceptions\IncorrectIdException;
use App\Type;

class ShortUUID extends Type
{
    static $pattern = '([a-f0-9]{8})';

    public function __construct($data = null)
    {
        if (strlen($data) === 4) {
            $data = bin2hex($data);
        }
        preg_match('/^' . self::$pattern . '$/', $data, $matches);
        if (empty($matches)) {
            print_r([$data, is_string($data), bin2hex($data), strlen($data)]);
            throw new IncorrectIdException;
        }
        parent::__construct($data);
    }

    public static function fake()
    {
        $string = '';
        $charset = "0123456789abcdf";
        for ($i = 0; $i < 8; $i++) {
            $string .= $charset[rand(0, strlen($charset) - 1)];
        }
        return new ShortUUID($string);
    }
}