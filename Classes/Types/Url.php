<?php

namespace App\Types;

use App\Exceptions\IncorrectUrlAddressException;
use App\Type;

class Url extends Type
{
    static $pattern = '\/(.*)';

    public function __construct($data = null)
    {
        preg_match('/^'.self::$pattern.'$/', $data, $matches);
        if(!($matches)){
            throw new IncorrectUrlAddressException;
        }
        parent::__construct($data);
    }
}