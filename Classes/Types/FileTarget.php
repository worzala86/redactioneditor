<?php

namespace App\Types;

use App\Exceptions\WrongUploadFileTargetException;
use App\Type;

class FileTarget extends Type
{
    public function __construct($data = null)
    {
        if(!in_array($data, ['Projects', 'Templates', 'Images', 'Elements', 'Styles'])){
            throw new WrongUploadFileTargetException;
        }
        parent::__construct($data);
    }
}