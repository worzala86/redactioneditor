<?php

namespace App\Types;

use App\Exceptions\IncorrectDatasetFieldNameException;
use App\Type;

class DatasetFieldName extends Type
{
    static $pattern = '([a-zA-Z_]+[a-zA-Z_0-9]+)';

    public function __construct($data = null)
    {
        preg_match('/^'.self::$pattern.'$/', $data, $matches);
        if(count($matches)==0){
            throw new IncorrectDatasetFieldNameException;
        }
        parent::__construct($data);
    }
}