<?php

namespace App\Types;

use App\Exceptions\IncorrectDomainException;
use App\Type;

class Domain extends Type
{
    static $pattern = '(((^www\.)?|^(http:\/\/)?)+[a-z0-9_\-]+\.[a-z0-9_\-\.]+)';

    public function __construct($data = null)
    {
        preg_match('/^'.self::$pattern.'$/', $data, $matches);
        if(!($matches)&&$data){
            throw new IncorrectDomainException;
        }
        parent::__construct($data);
    }
}