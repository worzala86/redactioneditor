<?php

namespace App\Types;

use App\Exceptions\PasswordMustBeInSHA512Exception;
use App\Type;

class Password extends Type
{
    static $pattern = '([a-z0-9]{128})';

    public function __construct($data = null)
    {
        preg_match('/^'.self::$pattern.'$/', $data, $matches);
        if(empty($matches)){
            throw new PasswordMustBeInSHA512Exception;
        }
        parent::__construct($data);
    }
}