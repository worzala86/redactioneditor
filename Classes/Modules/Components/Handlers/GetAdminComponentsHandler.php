<?php

namespace App\Modules\Components\Handlers;

use App\Containers\ComponentContainer;
use App\Containers\ComponentsContainer;
use App\Handler;
use App\Modules\Components\Collections\ComponentsCollection;
use App\Modules\Components\Collections\ComponentsViewCollection;
use App\Modules\Components\Requests\GetAdminComponentsRequest;
use App\Modules\Components\Requests\GetComponentsRequest;
use App\Modules\Components\Responses\GetAdminComponentsResponse;
use App\Modules\Components\Responses\GetComponentsResponse;

/**
 * Class GetAdminComponentsHandler
 * @package App\Modules\Components\Handlers
 * @description Metoda służy do pobrania elementów składowych strony - dla admina
 */
class GetAdminComponentsHandler extends Handler
{
    public function __invoke(GetAdminComponentsRequest $request): GetAdminComponentsResponse
    {
        $componentsCollection = (new ComponentsViewCollection)
            ->loadAll();

        $components = (new ComponentsContainer);
        foreach ($componentsCollection as $componentsModel) {
            $components->add(
                (new ComponentContainer)
                    ->setId($componentsModel->getUuid())
                    ->setKey($componentsModel->getKey())
                    ->setName($componentsModel->getName())
                    ->setDescription($componentsModel->getDescription())
                    ->setFile($componentsModel->getFile())
                    ->setCount($componentsModel->getCount())
                    ->setKind($componentsModel->getKind())
                    ->setSimpleView($componentsModel->getSimpleView())
            );
        }

        return (new GetAdminComponentsResponse)
            ->setComponents($components);
    }
}