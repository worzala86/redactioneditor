<?php

namespace App\Modules\Components\Handlers;

use App\Handler;
use App\Modules\Components\Models\ComponentsModel;
use App\Modules\Components\Requests\CreateComponentRequest;
use App\Modules\Components\Responses\CreateComponentResponse;
use App\Types\UUID;

/**
 * Class CreateComponentHandler
 * @package App\Modules\Components\Handlers
 * @description Metoda służy do dodania nowego komponentu
 */
class CreateComponentHandler extends Handler
{
    public function __invoke(CreateComponentRequest $request): CreateComponentResponse
    {
        $uuid = UUID::fake();
        (new ComponentsModel)
            ->setUuid($uuid)
            ->setKey($request->getKey())
            ->setName($request->getName())
            ->setDescription($request->getDescription())
            ->setFile($request->getFile())
            ->setKind($request->getKind())
            ->setSimpleView($request->getSimpleView())
            ->insert();

        return (new CreateComponentResponse)
            ->setId($uuid);
    }
}