<?php

namespace App\Modules\Components\Handlers;

use App\Containers\ComponentContainer;
use App\Containers\ComponentsContainer;
use App\Handler;
use App\Modules\Components\Collections\ComponentsCollection;
use App\Modules\Components\Requests\GetComponentsRequest;
use App\Modules\Components\Responses\GetComponentsResponse;

/**
 * Class GetComponentsHandler
 * @package App\Modules\Components\Handlers
 * @description Metoda służy do pobrania elementów składowych strony
 */
class GetComponentsHandler extends Handler
{
    public function __invoke(GetComponentsRequest $request): GetComponentsResponse
    {
        $componentsCollection = (new ComponentsCollection)
            ->loadAll();

        $components = (new ComponentsContainer);
        foreach ($componentsCollection as $componentsModel) {
            $components->add(
                (new ComponentContainer)
                    ->setId($componentsModel->getUuid())
                    ->setKey($componentsModel->getKey())
                    ->setName($componentsModel->getName())
                    ->setDescription($componentsModel->getDescription())
                    ->setFile($componentsModel->getFile())
                    ->setKind($componentsModel->getKind())
            );
        }

        return (new GetComponentsResponse)
            ->setComponents($components);
    }
}