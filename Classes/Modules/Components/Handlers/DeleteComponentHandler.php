<?php

namespace App\Modules\Components\Handlers;

use App\Handler;
use App\Modules\Components\Models\ComponentsModel;
use App\Modules\Components\Requests\DeleteComponentRequest;
use App\Modules\Components\Responses\DeleteComponentResponse;

/**
 * Class DeleteComponentHandler
 * @package App\Modules\Components\Handlers
 * @description Metoda służy do usuwania komponentu
 */
class DeleteComponentHandler extends Handler
{
    public function __invoke(DeleteComponentRequest $request): DeleteComponentResponse
    {
        $deleted = (new ComponentsModel)
            ->delete($request->getId(), true);

        return (new DeleteComponentResponse)
            ->setSuccess($deleted);
    }
}