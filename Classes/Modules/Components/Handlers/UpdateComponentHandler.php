<?php

namespace App\Modules\Components\Handlers;

use App\Handler;
use App\Modules\Components\Exceptions\ComponentNotFoundException;
use App\Modules\Components\Models\ComponentsModel;
use App\Modules\Components\Requests\UpdateComponentRequest;
use App\Modules\Components\Responses\UpdateComponentResponse;

/**
 * Class UpdateComponentHandler
 * @package App\Modules\Components\Handlers
 * @description Metoda służy do aktualizacji komponentu
 */
class UpdateComponentHandler extends Handler
{
    public function __invoke(UpdateComponentRequest $request): UpdateComponentResponse
    {
        $componentsModel = (new ComponentsModel)
            ->load($request->getId(), true);

        if(!$componentsModel->isLoaded()){
            throw new ComponentNotFoundException;
        }

        $updated = $componentsModel
            ->setKey($request->getKey())
            ->setName($request->getName())
            ->setDescription($request->getDescription())
            ->setFile($request->getFile())
            ->setKind($request->getKind())
            ->setSimpleView($request->getSimpleView())
            ->update();

        return (new UpdateComponentResponse)
            ->setSuccess($updated);
    }
}