<?php

namespace App\Modules\Components\Collections;

use App\CollectionTrait;
use App\Modules\Components\Models\ComponentsViewModel;

class ComponentsViewCollection extends ComponentsViewModel implements \Iterator
{
    use CollectionTrait;
}