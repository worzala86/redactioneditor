<?php

namespace App\Modules\Components\Collections;

use App\CollectionTrait;
use App\Modules\Components\Models\ComponentsModel;

class ComponentsCollection extends ComponentsModel implements \Iterator
{
    use CollectionTrait;
}