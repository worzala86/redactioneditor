<?php

namespace App\Modules\Components\Models;

use App\Model;
use App\Types\ComponentKey;
use App\Types\ComponentKind;
use App\Types\UUID;

class ComponentsViewModel extends Model
{
    private $id;
    private $uuid;
    private $key;
    private $description;
    private $name;
    private $file;
    private $count;
    private $kind;
    private $simpleView;

    public function setSimpleView(bool $simpleView)
    {
        $this->set('simple_view', $simpleView);
        $this->simpleView = $simpleView;
        return $this;
    }

    public function getSimpleView(): bool
    {
        return $this->simpleView;
    }

    public function setKind(ComponentKind $kind)
    {
        $this->set('kind', $kind);
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): ComponentKind
    {
        return $this->kind;
    }

    public function setFile(string $file)
    {
        $this->set('file', $file);
        $this->file = $file;
        return $this;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    public function setName(string $name)
    {
        $this->set('name', $name);
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setDescription(string $description = null)
    {
        $this->set('description', $description);
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setKey(ComponentKey $key)
    {
        $this->set('key', $key);
        $this->key = $key;
        return $this;
    }

    public function getKey(): ComponentKey
    {
        return $this->key;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setCount(int $count)
    {
        $this->set('count', $count);
        $this->count = $count;
        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}