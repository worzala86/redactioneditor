<?php

namespace App\Modules\Components\Exceptions;

use App\NamedException;

class ComponentNotFoundException extends \Exception implements NamedException
{
}