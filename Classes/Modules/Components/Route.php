<?php

namespace App;

use App\Modules\Components\Handlers;
use App\Types\UUID;

return [
    ['method'=>'post', 'url'=>'api/components', 'handler'=>Handlers\CreateComponentHandler::class],
    ['method'=>'get', 'url'=>'api/components', 'handler'=>Handlers\GetComponentsHandler::class],
    ['method'=>'get', 'url'=>'api/components/simple-view', 'handler'=>Handlers\GetSimpleViewComponentsHandler::class],
    ['method'=>'get', 'url'=>'api/admin-components', 'handler'=>Handlers\GetAdminComponentsHandler::class],
    ['method'=>'put', 'url'=>'api/components/:id', 'handler'=>Handlers\UpdateComponentHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'delete', 'url'=>'api/components/:id', 'handler'=>Handlers\DeleteComponentHandler::class, 'regex'=>['id'=>UUID::$pattern]],
];