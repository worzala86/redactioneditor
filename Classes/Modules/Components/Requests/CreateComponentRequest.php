<?php

namespace App\Modules\Components\Requests;

use App\AdminRequest;
use App\Types\ComponentKey;
use App\Types\ComponentKind;

class CreateComponentRequest extends AdminRequest
{
    private $key;
    private $name;
    private $description;
    private $file;
    private $kind;
    private $simpleView;

    /**
     * @param ComponentKey $key
     * @description Klucz komponentu wykorzystywany wewnętrznie
     * @return $this
     */
    public function setKey(ComponentKey $key)
    {
        $this->key = $key;
        return $this;
    }

    public function getKey(): ComponentKey
    {
        return $this->key;
    }

    /**
     * @param string $name
     * @description Nazwa komponentu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $description
     * @description Opis komponentu
     * @return $this
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $file
     * @description Nazwa pliku z kodem html komponentu
     * @return $this
     */
    public function setFile(string $file)
    {
        $this->file = $file;
        return $this;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @param ComponentKind $kind
     * @description Rodzaj komponentu wykorzystywany w SimpleEdit jako pole wprowadzania danych
     * @return $this
     */
    public function setKind(ComponentKind $kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): ComponentKind
    {
        return $this->kind;
    }

    /**
     * @param bool $simpleView
     * @description Jeżeli wartość jest true to komeponent jest edytowalny w SimpleView w lewym panelu
     * @return $this
     */
    public function setSimpleView(bool $simpleView)
    {
        $this->simpleView = $simpleView;
        return $this;
    }

    public function getSimpleView(): bool
    {
        return $this->simpleView;
    }
}