<?php

namespace App\Modules\Components\Requests;

use App\AdminRequest;
use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;

class GetAdminComponentsRequest extends AdminRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;
}