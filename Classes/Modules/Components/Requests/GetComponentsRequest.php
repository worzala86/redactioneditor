<?php

namespace App\Modules\Components\Requests;

use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;
use App\UserRequest;

class GetComponentsRequest extends UserRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;
}