<?php

namespace App\Modules\Components\Requests;

use App\AdminRequest;
use App\Types\UUID;

class DeleteComponentRequest extends AdminRequest
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator usuwanego komponentu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}