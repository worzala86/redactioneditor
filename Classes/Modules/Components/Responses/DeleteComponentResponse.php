<?php

namespace App\Modules\Components\Responses;

use App\Response;

class DeleteComponentResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Pole zwraca czy pomyślnie usunięto komponent
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }
}