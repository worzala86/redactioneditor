<?php

namespace App\Modules\Components\Responses;

use App\Response;
use App\Types\UUID;

class CreateComponentResponse extends Response
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator nowo dodanego komponentu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}