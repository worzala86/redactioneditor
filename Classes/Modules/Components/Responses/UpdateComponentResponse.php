<?php

namespace App\Modules\Components\Responses;

use App\Response;

class UpdateComponentResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Pole zwraca czy pomyślnie zapisano komponent
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }
}