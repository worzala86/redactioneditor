<?php

namespace App\Modules\Components\Responses;

use App\Containers\ComponentsContainer;
use App\Response;

class GetSimpleViewComponentsResponse extends Response
{
    private $components;

    /**
     * @param ComponentsContainer $components
     * @description Pole zawiera listę komponentów
     * @return $this
     */
    public function setComponents(ComponentsContainer $components)
    {
        $this->components = $components;
        return $this;
    }

    public function getComponents(): ComponentsContainer
    {
        return $this->components;
    }
}