<?php

namespace App\Modules\Projects\Handlers;

use App\Handler;
use App\Modules\Projects\Exceptions\ProjectNotFoundException;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Requests\AttachTagsToProjectRequest;
use App\Modules\Projects\Responses\AttachTagsToProjectResponse;
use App\Workers\TagsWorker;

/**
 * Class AttachTagsToProjectHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do przypisywania tagów do projektu
 */
class AttachTagsToProjectHandler extends Handler
{
    public function __invoke(AttachTagsToProjectRequest $request): AttachTagsToProjectResponse
    {
        $projectsModel = (new ProjectsModel)
            ->load($request->getId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $tagsWorker = new TagsWorker;
        $tagsWorker->setProjectId($projectsModel->getId());
        $tagsWorker->saveTagsToDatabase($request->getTags());

        return (new AttachTagsToProjectResponse)
            ->setSuccess(true);
    }
}