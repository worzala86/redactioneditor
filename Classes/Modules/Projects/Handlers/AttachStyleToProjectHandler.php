<?php

namespace App\Modules\Projects\Handlers;

use App\Handler;
use App\Modules\Projects\Exceptions\ProjectNotFoundException;
use App\Modules\Projects\Exceptions\StyleNotFoundException;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Requests\AttachStyleToProjectRequest;
use App\Modules\Projects\Responses\AttachStyleToProjectResponse;
use App\Modules\Styles\Models\StylesModel;

/**
 * Class AttachStyleToProjectHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do przypisywania stylu do projektu
 */
class AttachStyleToProjectHandler extends Handler
{
    public function __invoke(AttachStyleToProjectRequest $request): AttachStyleToProjectResponse
    {
        $projectsModel = (new ProjectsModel)
            ->load($request->getId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $stylesModel = (new StylesModel)
            ->load($request->getStyleId(), true);

        if (!$stylesModel->isLoaded()) {
            throw new StyleNotFoundException;
        }

        (new ProjectsModel)
            ->setStyleId($stylesModel->getId())
            ->update($projectsModel->getId());

        return (new AttachStyleToProjectResponse)
            ->setSuccess(true);
    }
}