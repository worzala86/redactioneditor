<?php

namespace App\Modules\Projects\Handlers;

use App\File;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Projects\Exceptions\ProjectNotFoundException;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Requests\DeleteProjectRequest;
use App\Modules\Projects\Responses\DeleteProjectResponse;
use App\Modules\User\Models\UsersModel;
use App\Types\FileTarget;

/**
 * Class DeleteProjectHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do usunięcia projektu na podstawie ID
 */
class DeleteProjectHandler extends Handler
{
    public function __invoke(DeleteProjectRequest $request): DeleteProjectResponse
    {
        $projectModel = (new ProjectsModel)
            ->where('`uuid`=?', $request->getId())
            ->load();

        if (!$projectModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        if ($projectModel->getImageId()) {
            $oldFilesModel = (new FilesModel)
                ->where('user_id=?', $request->getCurrentUserId())
                ->load($projectModel->getImageId(), true);
            if ($oldFilesModel->isLoaded()) {
                (new File(new FileTarget('Projects')))
                    ->load($oldFilesModel->getId())
                    ->delete();
            }
        }

        (new UsersModel)->startTransaction();

        $userModel = (new UsersModel)
            ->where('`active_project_id`=?', $projectModel->getId());
        if ($userModel->isLoaded()) {
            $userModel
                ->setActiveProjectId(null)
                ->update();
        }

        $deleted = $projectModel->delete();

        (new UsersModel)->commit();

        return (new DeleteProjectResponse)
            ->setSuccess($deleted);
    }
}