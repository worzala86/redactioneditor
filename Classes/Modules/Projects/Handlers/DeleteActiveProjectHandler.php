<?php

namespace App\Modules\Projects\Handlers;

use App\File;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Projects\Exceptions\ProjectNotFoundException;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Requests\DeleteActiveProjectRequest;
use App\Modules\Projects\Responses\DeleteActiveProjectResponse;
use App\Modules\User\Models\UsersModel;
use App\Types\FileTarget;

/**
 * Class DeleteActiveProjectHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do usunięcia aktywnego projektu
 */
class DeleteActiveProjectHandler extends Handler
{
    public function __invoke(DeleteActiveProjectRequest $request): DeleteActiveProjectResponse
    {
        $userModel = (new UsersModel)
            ->load($request->getCurrentUserId());

        $projectModel = (new ProjectsModel)
            ->load($userModel->getActiveProjectId());

        if (!$projectModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        (new UsersModel)->startTransaction();

        $userModel = (new UsersModel)
            ->where('`active_project_id`=?', $projectModel->getId());
        if ($userModel->isLoaded()) {
            $userModel
                ->setActiveProjectId(null)
                ->update();
        }

        $deleted = $projectModel->delete();

        (new UsersModel)->commit();

        return (new DeleteActiveProjectResponse)
            ->setSuccess($deleted);
    }
}