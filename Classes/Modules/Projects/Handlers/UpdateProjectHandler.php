<?php

namespace App\Modules\Projects\Handlers;

use App\Handler;
use App\Modules\Projects\Exceptions\ProjectNotFoundException;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Requests\UpdateProjectRequest;
use App\Modules\Projects\Responses\UpdateProjectResponse;

/**
 * Class UpdateProjectHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do edycji projektu na podstawie ID
 */
class UpdateProjectHandler extends Handler
{
    public function __invoke(UpdateProjectRequest $request): UpdateProjectResponse
    {
        $projectModel = (new ProjectsModel)
            ->where('`uuid`=?', $request->getId())
            ->where('`user_id`=?', $request->getCurrentUserId())
            ->load($request->getId(), true);

        if (!$projectModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $updated = (new ProjectsModel)
            ->setUuid($request->getId())
            ->setName($request->getName())
            ->setDomain($request->getDomain())
            ->setDescription($request->getDescription())
            ->update($projectModel->getId());

        return (new UpdateProjectResponse)
            ->setSuccess($updated);
    }
}