<?php

namespace App\Modules\Projects\Handlers;

use App\Containers\ImageContainer;
use App\Containers\ImageThumbContainer;
use App\Containers\ProjectContainer;
use App\Containers\ProjectsContainer;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Projects\Collections\ProjectsCollection;
use App\Modules\Projects\Requests\GetProjectsByTagRequest;
use App\Modules\Projects\Requests\GetProjectsRequest;
use App\Modules\Projects\Responses\GetProjectsByTagResponse;
use App\Modules\Projects\Responses\GetProjectsResponse;

/**
 * Class GetProjectsByTagHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania listy projektów według tagu
 */

class GetProjectsByTagHandler extends Handler
{
    public function __invoke(GetProjectsByTagRequest $request): GetProjectsByTagResponse
    {
        $projectsCollection = (new ProjectsCollection)
            ->setPage($request->getPage())
            ->setLimit($request->getLimit())
            ->where('exists(select 1 from tags where name=? and 
                exists(select 1 from tags_projects where project_id=projects.id and tag_id=tags.id))', $request->getTag())
            ->loadAll();

        $projects = new ProjectsContainer;
        foreach($projectsCollection as $projectModel){
            $file = (new FilesModel)
                ->load($projectModel->getImageId());
            $image = new ImageContainer;
            if($file->isLoaded()){
                $thumb = new ImageThumbContainer;
                $fileThumb = (new FilesModel)
                    ->load($file->getThumbId());
                if($fileThumb->isLoaded()){
                    $thumb->setUrl($fileThumb->getUrl());
                }
                $image->setUrl($file->getUrl())
                    ->setThumb($thumb);
            }
            $projects->add(
                (new ProjectContainer)
                    ->setId($projectModel->getUuid())
                    ->setName($projectModel->getName())
                    ->setImage($image)
            );
        }

        return (new GetProjectsByTagResponse)
            ->setProjects($projects)
            ->setTotalCount($projectsCollection->getTotalCount());
    }
}