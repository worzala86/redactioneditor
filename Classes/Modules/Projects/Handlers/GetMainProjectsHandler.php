<?php

namespace App\Modules\Projects\Handlers;

use App\Containers\ImageContainer;
use App\Containers\ImageThumbContainer;
use App\Containers\ProjectContainer;
use App\Containers\ProjectsContainer;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Projects\Collections\ProjectsCollection;
use App\Modules\Projects\Requests\GetMainProjectsRequest;
use App\Modules\Projects\Responses\GetMainProjectsResponse;
use App\Types\SortKind;

/**
 * Class GetMainProjectsHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania publicznej listy projektów
 */
class GetMainProjectsHandler extends Handler
{
    public function __invoke(GetMainProjectsRequest $request): GetMainProjectsResponse
    {
        $projectsCollection = (new ProjectsCollection)
            ->where('main=?', true)
            ->setSortName(null)
            ->setSortKind(new SortKind('rand'))
            ->setLimit(8)
            ->loadAll();

        $projects = new ProjectsContainer;
        foreach ($projectsCollection as $projectModel) {
            $file = (new FilesModel)
                ->load($projectModel->getImageId());
            $image = new ImageContainer;
            if($file->isLoaded()){
                $thumb = new ImageThumbContainer;
                $fileThumb = (new FilesModel)
                    ->load($file->getThumbId());
                if($fileThumb->isLoaded()){
                    $thumb->setUrl($fileThumb->getUrl());
                }
                $image->setUrl($file->getUrl())
                    ->setThumb($thumb);
            }
            $projects->add(
                (new ProjectContainer)
                    ->setId($projectModel->getUuid())
                    ->setImage($image)
            );
        }

        return (new GetMainProjectsResponse)
            ->setProjects($projects);
    }
}