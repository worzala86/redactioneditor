<?php

namespace App\Modules\Projects\Handlers;

use App\Handler;
use App\Modules\Projects\Exceptions\ProjectNotAddedException;
use App\Modules\Projects\Exceptions\ProjectNotFoundException;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Requests\GetProjectRequest;
use App\Modules\Projects\Responses\GetProjectResponse;

/**
 * Class GetProjectHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania szczegułów projektu
 */

class GetProjectHandler extends Handler
{
    public function __invoke(GetProjectRequest $request): GetProjectResponse
    {
        $projectModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where(' `uuid`=? ', $request->getId())
            ->load();

        if(!$projectModel->isLoaded()){
            throw new ProjectNotFoundException;
        }

        return (new GetProjectResponse)
            ->setId($projectModel->getUuid())
            ->setName($projectModel->getName())
            ->setDomain($projectModel->getDomain())
            ->setDescription($projectModel->getDescription());
    }
}