<?php

namespace App\Modules\Projects\Handlers;

use App\Handler;
use App\Modules\Projects\Exceptions\ProjectNotAddedException;
use App\Modules\Projects\Models\PagesModel;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Requests\CreateProjectRequest;
use App\Modules\Projects\Responses\CreateProjectResponse;
use App\Modules\Styles\Models\StylesModel;
use App\Modules\User\Models\UsersModel;
use App\Types\Url;
use App\Types\UUID;

/**
 * Class CreateProjectHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do utworzenia nowego projektu
 */

class CreateProjectHandler extends Handler
{
    public function __invoke(CreateProjectRequest $request): CreateProjectResponse
    {
        (new ProjectsModel)->startTransaction();

        $customStylesModel = (new StylesModel)
            ->setUuid(UUID::fake())
            ->setUserId($request->getCurrentUserId());
        $customStylesId = $customStylesModel->insert();

        $uuid = UUID::fake();
        $projectId = (new ProjectsModel)
            ->setUuid($uuid)
            ->setName($request->getName())
            ->setUserId($request->getCurrentUserId())
            ->setDomain($request->getDomain())
            ->setDescription($request->getDescription())
            ->setStyleId(DEFAULT_STYLE_ID)
            ->setCustomStyleId($customStylesId)
            ->insert();

        if(!$projectId){
            throw new ProjectNotAddedException;
        }

        $pageUuid = UUID::fake();
        (new PagesModel)
            ->setUserId($request->getCurrentUserId())
            ->setUuid($pageUuid)
            ->setProjectId($projectId)
            ->setUrl(new Url('/'))
            ->insert();

        (new ProjectsModel)
            ->setConfig(json_encode(['defaultPageId' => (string)$pageUuid, 'defaultPageUrl' => '/']))
            ->update($projectId);

        (new UsersModel)
            ->setActiveProjectId($projectId)
            ->update($request->getCurrentUserId());

        (new ProjectsModel)->commit();

        return (new CreateProjectResponse)
            ->setId(new UUID($uuid));
    }
}