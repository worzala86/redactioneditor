<?php

namespace App\Modules\Projects\Handlers;

use App\Database\DB;
use App\File;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Projects\Exceptions\ProjectNotFoundException;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Requests\DeleteProjectRequest;
use App\Modules\Projects\Requests\SetActiveProjectRequest;
use App\Modules\Projects\Responses\DeleteProjectResponse;
use App\Modules\Projects\Responses\SetActiveProjectResponse;
use App\Modules\User\Models\UsersModel;
use App\Types\FileTarget;

/**
 * Class SetActiveProjectHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do ustawienia głównego projektu
 */
class SetActiveProjectHandler extends Handler
{
    public function __invoke(SetActiveProjectRequest $request): SetActiveProjectResponse
    {
        $projectModel = (new ProjectsModel)
            ->where('`uuid`=?', $request->getId())
            ->load();

        if (!$projectModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        (new UsersModel)->startTransaction();

        $updated = (new UsersModel)
            ->setActiveProjectId($projectModel->getId())
            ->update();

        (new UsersModel)->commit();

        return (new SetActiveProjectResponse)
            ->setSuccess($updated);
    }
}