<?php

namespace App\Modules\Projects\Handlers;

use App\Containers\ImageContainer;
use App\Containers\ImageThumbContainer;
use App\Containers\ProjectContainer;
use App\Containers\ProjectsContainer;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Projects\Collections\ProjectsCollection;
use App\Modules\Projects\Requests\GetPublicProjectsRequest;
use App\Modules\Projects\Responses\GetPublicProjectsResponse;

/**
 * Class GetPublicProjectsHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania publicznej listy projektów
 */
class GetPublicProjectsHandler extends Handler
{
    public function __invoke(GetPublicProjectsRequest $request): GetPublicProjectsResponse
    {
        /*$categoryModel = (new CategoriesModel)
            ->where('`key`=?', $request->getCategoryKey())
            ->load();

        $categoryName = null;
        if($categoryModel->isLoaded()) {
            $categoryName = $categoryModel->getName();
        }*/

        $projectsCollection = (new ProjectsCollection)
            ->where('public=?', true);
        if($request->getTag()) {
            $projectsCollection->where(' exists(select 1 from tags_projects where project_id=projects.id and tag_id=
            (select id from tags where name=?)) ', $request->getTag());
        }
        $projectsCollection->loadAll();

        $projects = new ProjectsContainer;
        foreach ($projectsCollection as $projectModel) {
            $file = (new FilesModel)
                ->load($projectModel->getImageId());
            $image = new ImageContainer;
            if($file->isLoaded()){
                $thumb = new ImageThumbContainer;
                $fileThumb = (new FilesModel)
                    ->load($file->getThumbId());
                if($fileThumb->isLoaded()){
                    $thumb->setUrl($fileThumb->getUrl())
                        ->setId($fileThumb->getUuid());
                }
                $image->setUrl($file->getUrl())
                    ->setId($file->getUuid())
                    ->setThumb($thumb);
            }
            $projects->add(
                (new ProjectContainer)
                    ->setId($projectModel->getUuid())
                    ->setImage($image)
            );
        }

        return (new GetPublicProjectsResponse)
            ->setProjects($projects)
            ;//->setCategoryName($categoryName);
    }
}