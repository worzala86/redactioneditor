<?php

namespace App\Modules\Projects\Handlers;

use App\Handler;
use App\Modules\Datasets\Workers\CreateDataSetsShemaWorker;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Requests\ForkProjectRequest;
use App\Modules\Projects\Responses\ForkProjectResponse;
use App\Database\DB;
use App\Modules\Projects\Workers\PrepereLookupFilesWorker;
use App\Types\Time;
use App\Types\UUID;
use App\Workers\HtmlWorker;

/**
 * Class ForkProjectHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do klonowania projektu
 */
class ForkProjectHandler extends Handler
{
    public function __invoke(ForkProjectRequest $request): ForkProjectResponse
    {
        (new ProjectsModel)->startTransaction();

        $projectsUuid = UUID::fake();
        $projects = DB::get()->getRow('select custom_style_id, id, show_left_sidebar, show_right_sidebar, header_id, footer_id, sidebar_left_id, sidebar_right_id from projects where uuid=?', hex2bin($request->getId()));
        $htmlWorker = new HtmlWorker;
        $headerId = $htmlWorker->saveHtmlElements($htmlWorker->loadHtmlElements($projects['header_id']));
        $footerId = $htmlWorker->saveHtmlElements($htmlWorker->loadHtmlElements($projects['footer_id']));
        $sidebarLeftId = $htmlWorker->saveHtmlElements($htmlWorker->loadHtmlElements($projects['sidebar_left_id']));
        $sidebarRightId = $htmlWorker->saveHtmlElements($htmlWorker->loadHtmlElements($projects['sidebar_right_id']));

        $oldStyleId = $projects['custom_style_id'];
        $styles = DB::get()->getAll('select * from styles_style where styles_id=?', $oldStyleId);
        DB::get()->execute('insert into styles set uuid=?, added=?, added_by=?, added_ip_id=?, 
            name=?, user_id=?',
            [UUID::fake(), Time::fake(), $request->getCurrentUserId(), $request->getIpId(),
                UUID::fake(), $request->getCurrentUserId()]);
        $newStyleId = DB::get()->insertId();
        foreach ($styles as $style) {
            DB::get()->execute('insert into styles_style set styles_id=?, param_id=?, value_id=?, tag_id=?',
                [$newStyleId, $style['param_id'], $style['value_id'], $style['tag_id']]);
        }

        $oldProjectId = $projects['id'];
        $newProjectsId = DB::get()->getOne('select id from projects where uuid=?', hex2bin($request->getNewId()));
        if ($request->getSoft() == 1) {
            DB::get()->execute('update projects set 
                  header_id=:header_id, footer_id=:footer_id, 
                  sidebar_left_id=:sidebar_left_id, sidebar_right_id=:sidebar_right_id, 
                  show_left_sidebar=:show_left_sidebar, show_right_sidebar=:show_right_sidebar,
                  forked_id=:proid, custom_style_id=:custom_style_id where id=:id', [
                'id' => $newProjectsId,
                'header_id' => $headerId,
                'footer_id' => $footerId,
                'sidebar_left_id' => $sidebarLeftId,
                'sidebar_right_id' => $sidebarRightId,
                'show_left_sidebar' => $projects['show_left_sidebar'],
                'show_right_sidebar' => $projects['show_right_sidebar'],
                'proid' => $projects['id'],
                'custom_style_id' => $newStyleId,
            ]);
        } else {
            DB::get()->execute('insert into projects 
                  (name, user_id, header_id, footer_id, uuid, sidebar_left_id, sidebar_right_id, 
                  added, added_by, added_ip_id, config, show_left_sidebar, show_right_sidebar,
                  forked_id, style_id, custom_style_id)
                  select 
                  concat(name, " ", :hex), :user_id, :header_id, :footer_id, :new_uuid, :sidebar_left_id, :sidebar_right_id, 
                  :added, :added_by, :added_ip_id, config, show_left_sidebar, show_right_sidebar,
                  pro.id, :style_id, :custom_style_id
                  from
                  projects as pro
                  where pro.uuid=:uuid', [
                'user_id' => $request->getCurrentUserId(),
                'uuid' => hex2bin($request->getId()),
                'new_uuid' => hex2bin($projectsUuid),
                'added' => time(),
                'added_by' => $request->getCurrentUserId(),
                'added_ip_id' => $request->getCurrentIpId(),
                'hex' => substr($projectsUuid, 0, 6),
                'header_id' => $headerId,
                'footer_id' => $footerId,
                'sidebar_left_id' => $sidebarLeftId,
                'sidebar_right_id' => $sidebarRightId,
                'style_id' => DEFAULT_STYLE_ID,
                'custom_style_id' => $newStyleId,
            ]);
        }
        $projectId = DB::get()->insertId();

        /*DB::get()->execute('insert into projects_configs
                  (project_id, config, uuid, user_id, page_style, show_left_sidebar, show_right_sidebar, 
                  added, added_by, added_ip_id)
                  select 
                  :new_project_id, config, :new_uuid, :user_id, page_style, show_left_sidebar, 
                  show_right_sidebar, :added, :added_by, :added_ip_id
                  from
                  projects_configs as pro
                  where pro.project_id=:old_project_id', [
            'user_id' => $request->getCurrentUserId(),
            'old_project_id' => $oldProjectId,
            'new_project_id' => $projectId,
            'new_uuid' => hex2bin(UUID::fake()),
            'added' => time(),
            'added_by' => $request->getCurrentUserId(),
            'added_ip_id' => $request->getCurrentIpId()
        ]);
        $projectsConfigsId = DB::get()->insertId();*/

        if ($request->getSoft() == 1) {
            DB::get()->execute('update pages set deleted=?, deleted_by=?, deleted_ip_id=? where project_id=?',
                [time(), $request->getCurrentUserId(), $request->getIpId(), $newProjectsId]);
        }
        $pages = DB::get()->getAll('select id, content_id from pages where project_id=?
            and deleted=0', ($request->getSoft() == 1) ? $projects['id'] : $oldProjectId);
        foreach ($pages as $page) {
            $contentId = $htmlWorker->saveHtmlElements($htmlWorker->loadHtmlElements($page['content_id']));
            DB::get()->execute('insert into pages 
                  (name, content_id, project_id, url, uuid, pagination_url_param, 
                  pagination_limit, user_id, added, added_by, added_ip_id, pagination_url, content_id)
                  select 
                  name, :content_id, :new_project_id, url, unhex(substring(MD5(RAND()) FROM 1 FOR 16)), pagination_url_param, 
                  pagination_limit, :user_id, :added, :added_by, :added_ip_id, pagination_url,
                  content_id=:content
                  from
                  pages as pro
                  where deleted=0 and pro.id=:id and pro.contentId=true', [
                'user_id' => $request->getCurrentUserId(),
                'new_project_id' => ($request->getSoft() == 1) ? $newProjectsId : $contentId,
                'added' => time(),
                'added_by' => $request->getCurrentUserId(),
                'added_ip_id' => $request->getCurrentIpId(),
                'id' => $page['id'],
                'content' => ($request->getSoft() == 1) ? $newProjectsId : $contentId,
            ]);
        }

        $pageUuid = DB::get()->getOne('select uuid from pages where project_id=? and url=\'\/\' and deleted=0',
            ($request->getSoft() == 1) ? $newProjectsId : $projectId);
        $config = DB::get()->getOne('select config from projects where id=? and deleted=0', ($request->getSoft() == 1) ? $projects['id'] : $projectId);
        $config = json_decode($config);
        $config->defaultPageId = (string)bin2hex($pageUuid);
        $config = json_encode($config);
        DB::get()->execute('update projects set config=? where id=?',
            [$config, ($request->getSoft() == 1) ? $newProjectsId : $projectId]);

        DB::get()->execute('insert into datasets 
                  (name, project_id, uuid, user_id, added, added_by, added_ip_id)
                  select 
                  name, :new_project_id, unhex(substring(MD5(RAND()) FROM 1 FOR 16)), :user_id, :added, :added_by, :added_ip_id
                  from
                  datasets as pro
                  where pro.project_id=:old_project_id and deleted=0 and pro.id=:id and pro.contentId=true', [
            'user_id' => $request->getCurrentUserId(),
            'old_project_id' => $oldProjectId,
            'new_project_id' => ($request->getSoft() == 1) ? $newProjectsId : $projectId,
            'added' => time(),
            'added_by' => $request->getCurrentUserId(),
            'added_ip_id' => $request->getCurrentIpId(),
            'content_id' => ($request->getSoft() == 1) ? $newProjectsId : $contentId,
        ]);
        $datasetsId = DB::get()->insertId();

        $datasets = DB::get()->getAll('select name, id from datasets where project_id=? and deleted=0', $oldProjectId);
        if ($datasets) {
            foreach ($datasets as $dataset) {
                $newDatasetsId = DB::get()->getOne('select id from datasets where project_id=? and name=? and deleted=0',
                    [$projectId, $dataset['name']]);
                DB::get()->execute('insert into datasets_fields 
                  (name, uuid, added, added_by, added_ip_id, datasets_id, type)
                  select 
                  name, unhex(substring(MD5(RAND()) FROM 1 FOR 16)), :added, :added_by, :added_ip_id, :new_datasets_id, type
                  from
                  datasets_fields as pro
                  where pro.datasets_id=:old_datasets_id and deleted=0', [
                    'added' => time(),
                    'added_by' => $request->getCurrentUserId(),
                    'added_ip_id' => $request->getCurrentIpId(),
                    'old_datasets_id' => $dataset['id'],
                    'new_datasets_id' => $newDatasetsId,
                ]);
            }
        }

        $createDataSetsShemaWorker = new CreateDataSetsShemaWorker;
        $createDataSetsShemaWorker->create($request->getCurrentUserId(), $projects['id']);

        $tableUsersName = 'datasets_' . $request->getCurrentUserId() . '_' . $projects['id'] . '_users';
        $tableIpName = 'datasets_' . $request->getCurrentUserId() . '_' . $projects['id'] . '_ip';

        $datasets = DB::get()->getAll('select name, id from datasets where project_id=? and deleted=0', $projectId);
        if ($datasets) {
            foreach ($datasets as $dataset) {
                $tableName = 'datasets_' . $request->getCurrentUserId() . '_' . ($request->getSoft() == 1) ? $projects['id'] : $projectId . '_' . $dataset['name'];
                DB::get('REDACTION_DATASETS')->execute('create table `' . $tableName . '`(
                id int primary key auto_increment,
                uuid binary(8),
                unique(uuid),
                added int unsigned default null,
                added_by int unsigned default null,
                added_ip_id int unsigned default null,
                updated int unsigned default null,
                updated_by int unsigned default null,
                updated_ip_id int unsigned default null,
                deleted int unsigned not null default 0,
                deleted_by int unsigned default null,
                deleted_ip_id int unsigned default null,
                CONSTRAINT `fk_exemple_table_added_by` FOREIGN KEY (added_by) REFERENCES ' . $tableUsersName . '(id) ON DELETE NO ACTION ON UPDATE CASCADE,
                CONSTRAINT `fk_exemple_table_deleted_by` FOREIGN KEY (deleted_by) REFERENCES ' . $tableUsersName . '(id) ON DELETE NO ACTION ON UPDATE CASCADE,
                CONSTRAINT `fk_exemple_table_updated_by` FOREIGN KEY (updated_by) REFERENCES ' . $tableUsersName . '(id) ON DELETE NO ACTION ON UPDATE CASCADE,
                CONSTRAINT `fk_exemple_table_added_ip_id` FOREIGN KEY (added_ip_id) REFERENCES ' . $tableIpName . '(id) ON DELETE NO ACTION ON UPDATE CASCADE,
                CONSTRAINT `fk_exemple_table_deleted_ip_id` FOREIGN KEY (deleted_ip_id) REFERENCES ' . $tableIpName . '(id) ON DELETE NO ACTION ON UPDATE CASCADE,
                CONSTRAINT `fk_exemple_table_updated_ip_id` FOREIGN KEY (updated_ip_id) REFERENCES ' . $tableIpName . '(id) ON DELETE NO ACTION ON UPDATE CASCADE
                );');
                foreach (DB::get()->getAll('select type, name from datasets_fields where datasets_id=?', $dataset['id']) as $field) {
                    $typeName = $field['type'];
                    $name = $field['name'];
                    $type = ' varchar(250) ';
                    if ($typeName == 'text') {
                        $type = ' varchar(250) ';
                    } else if ($typeName == 'textarea') {
                        $type = ' text ';
                    } else if ($typeName == 'image') {
                        $type = ' varchar(250) ';
                    } else if ($typeName == 'date') {
                        $type = ' varchar(250) ';
                    }
                    DB::get('REDACTION_DATASETS')->execute('alter table `' . $tableName . '` add `' . $name . '` ' . $type);
                }
            }
        }

        $oldDatasets = DB::get()->getAll('select name, id, user_id from datasets where project_id=? and deleted=0', $oldProjectId);
        if ($oldDatasets) {
            foreach ($oldDatasets as $oldDataset) {
                $newDataset = DB::get()->getRow('select name, id, user_id from datasets where project_id=? and name=?',
                    [$projectId, $oldDataset['name']]);
                $oldTableName = 'datasets_' . $oldDataset['user_id'] . '_' . $oldProjectId . '_' . $oldDataset['name'];
                $newTableName = 'datasets_' . $request->getCurrentUserId() . '_' . ($request->getSoft() == 1) ? $projects['id'] : $projectId . '_' . $newDataset['name'];
                $rows = DB::get('REDACTION_DATASETS')->execute('insert into `' . $newTableName . '` 
                    select * from `' . $oldTableName . '`');
            }
        }

        (new ProjectsModel)->commit();

        $lookupFilesWorker = new PrepereLookupFilesWorker();
        $lookupFilesWorker->prepare(($request->getSoft() == 1) ? $projects['id'] : $projectId);

        return (new ForkProjectResponse)
            ->setId($projectsUuid);
    }
}