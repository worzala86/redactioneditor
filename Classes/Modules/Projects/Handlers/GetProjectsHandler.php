<?php

namespace App\Modules\Projects\Handlers;

use App\Containers\ImageContainer;
use App\Containers\ImageThumbContainer;
use App\Containers\ProjectContainer;
use App\Containers\ProjectsContainer;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Projects\Collections\ProjectsCollection;
use App\Modules\Projects\Requests\GetProjectsRequest;
use App\Modules\Projects\Responses\GetProjectsResponse;

/**
 * Class GetProjectsHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania listy projektów
 */

class GetProjectsHandler extends Handler
{
    public function __invoke(GetProjectsRequest $request): GetProjectsResponse
    {
        $projectsCollection = (new ProjectsCollection)
            ->setPage($request->getPage())
            ->setLimit($request->getLimit())
            ->where('user_id=?', $request->getCurrentUserId())
            ->loadAll();

        $projects = new ProjectsContainer;
        foreach($projectsCollection as $projectModel){
            $projects->add(
                (new ProjectContainer)
                    ->setId($projectModel->getUuid())
                    ->setName($projectModel->getName())
            );
        }

        return (new GetProjectsResponse)
            ->setProjects($projects)
            ->setTotalCount($projectsCollection->getTotalCount());
    }
}