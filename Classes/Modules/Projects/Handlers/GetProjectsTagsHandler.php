<?php

namespace App\Modules\Projects\Handlers;

use App\Containers\ImageContainer;
use App\Containers\ImageThumbContainer;
use App\Containers\ProjectContainer;
use App\Containers\ProjectsContainer;
use App\Containers\TagContainer;
use App\Containers\TagsContainer;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Projects\Collections\ProjectsCollection;
use App\Modules\Projects\Requests\GetProjectsByTagRequest;
use App\Modules\Projects\Requests\GetProjectsRequest;
use App\Modules\Projects\Requests\GetProjectsTagsRequest;
use App\Modules\Projects\Responses\GetProjectsByTagResponse;
use App\Modules\Projects\Responses\GetProjectsResponse;
use App\Modules\Projects\Responses\GetProjectsTagsResponse;
use App\Workers\TagsWorker;

/**
 * Class GetProjectsTagsHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania listy tagów wszystkich projektów
 */

class GetProjectsTagsHandler extends Handler
{
    public function __invoke(GetProjectsTagsRequest $request): GetProjectsTagsResponse
    {
        $tagsWorker = new TagsWorker;
        $tags = $tagsWorker->getProjectsTags();

        $tagsResult = new TagsContainer;
        foreach ($tags as $tag){
            $tagsResult->add(
                (new TagContainer)
                    ->setName($tag[0])
                    ->setCaption($tag[1])
            );
        }

        return (new GetProjectsTagsResponse)
            ->setTags($tagsResult);
    }
}