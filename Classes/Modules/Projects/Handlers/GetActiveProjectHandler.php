<?php

namespace App\Modules\Projects\Handlers;

use App\Containers\ImageContainer;
use App\Database\DB;
use App\File;
use App\Handler;
use App\Modules\Projects\Exceptions\ActiveProjectNotSettedException;
use App\Modules\Projects\Exceptions\ProjectNotFoundException;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Requests\GetActiveProjectRequest;
use App\Modules\Projects\Responses\GetActiveProjectResponse;
use App\Modules\Styles\Models\StylesModel;
use App\Modules\User\Models\UsersModel;

/**
 * Class GetActiveProjectHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania szczegułów głównego projektu użytkownika
 */
class GetActiveProjectHandler extends Handler
{
    public function __invoke(GetActiveProjectRequest $request): GetActiveProjectResponse
    {
        $userModel = (new UsersModel)
            ->load($request->getCurrentUserId());
        if (!$userModel->isLoaded()) {
            throw new ActiveProjectNotSettedException;
        }

        $projectModel = (new ProjectsModel)
            ->load($userModel->getActiveProjectId());

        if (!$projectModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $styleId = $projectModel->getStyleId();
        $image = new ImageContainer;
        $styleModel = null;
        if ($styleId) {
            $styleModel = (new StylesModel)->load($styleId);
            $image = (new File)->load($styleModel->getImageId());
        }

        $pagesCount = DB::get()->getOne('select count(*) from pages where project_id=?', $projectModel->getId());
        $elementsCount = DB::get()->getOne('select count(*) from elements
            left join pages on pages.project_id=?
            left join pages_elements on elements.id=pages_elements.element_id and pages.id=pages_elements.page_id', $projectModel->getId())
        + DB::get()->getOne('select count(*) from elements
            left join projects_elements on projects_elements.project_id=?
            and projects_elements.element_id=elements.id', $projectModel->getId());

        $datasetsCount = DB::get()->getOne('select count(*) from datasets where project_id=?', $projectModel->getId());
        $datasetsFieldsCount = DB::get()->getOne('select count(*) from datasets
            left join datasets_fields on datasets.id=datasets_fields.datasets_id
            left join projects on projects.id=datasets.project_id
            where projects.id=?', $projectModel->getId());

        $dataset = [];
        for($pastDays=59;$pastDays>=0;$pastDays--) {
            $date = mktime(0, 0, 0, date("m"), date("d") - $pastDays, date("Y"));
            $date = date('Y-m-d', $date);
            $count = DB::get()->getOne('select coalesce((select `count` from pages_day_count_view where `date`=?), 0) as x', [$date]);
            $dataset[$date] = $count;
        }

        return (new GetActiveProjectResponse)
            ->setId($projectModel->getUuid())
            ->setName($projectModel->getName())
            ->setDomain($projectModel->getDomain())
            ->setDescription($projectModel->getDescription())
            ->setStyleName($styleModel ? $styleModel->getName() : null)
            ->setStyleImage($image)
            ->setPagesCount($pagesCount)
            ->setElementsCount($elementsCount)
            ->setDatasetsCount($datasetsCount)
            ->setDatasetsFieldsCount($datasetsFieldsCount)
            ->setDataset($dataset);
    }
}