<?php

namespace App\Modules\Projects\Handlers;

use App\Containers\ImageContainer;
use App\Containers\ImageThumbContainer;
use App\Containers\ProjectContainer;
use App\Containers\ProjectsContainer;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Projects\Collections\ProjectsCollection;
use App\Modules\Projects\Models\CategoriesModel;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Requests\GetAdminProjectsRequest;
use App\Modules\Projects\Responses\GetAdminProjectsResponse;
use App\Modules\User\Collections\UsersCollection;
use App\Modules\User\Models\UsersModel;
use App\Workers\TagsWorker;

/**
 * Class GetAdminProjectsHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania listy projektów w widoku admina
 */
class GetAdminProjectsHandler extends Handler
{
    public function __invoke(GetAdminProjectsRequest $request): GetAdminProjectsResponse
    {
        $usersCollection = (new UsersCollection)
            ->where('editor=?', 1)
            ->loadAll();
        $usersSql = [];
        $usersIds = [];
        foreach ($usersCollection as $usersModel) {
            $usersIds[] = $usersModel->getId();
            $usersSql[] = '?';
        }

        $projectsCollection = (new ProjectsCollection)
            ->setPage($request->getPage())
            ->setLimit($request->getLimit())
            ->setSortName($request->getSortName())
            ->setSortKind($request->getSortKind())
            ->setFilters($request->getFilters())
            ->where('user_id in (' . join(', ', $usersSql) . ')', $usersIds)
            ->loadAll();

        $projects = new ProjectsContainer;
        foreach ($projectsCollection as $projectModel) {
            $forkedId = $projectModel->getForkedId();
            $forkedUuid = null;
            if ($forkedId) {
                $forkedFrom = (new ProjectsModel)
                    ->load($forkedId);
                if ($forkedFrom->isLoaded()) {
                    $forkedUuid = $forkedFrom->getUuid();
                }
            }
            $usersModel = (new UsersModel)
                ->load($projectModel->getUserId());
            $projects->add(
                (new ProjectContainer)
                    ->setId($projectModel->getUuid())
                    ->setName($projectModel->getName())
                    ->setForkedId($forkedUuid)
                    ->setUserName($usersModel->getFirstName() . ' ' . $usersModel->getLastName())
                    ->setAdded($projectModel->getAdded())
                    ->setUpdated($projectModel->getUpdated())
                    ->setDescription($projectModel->getDescription())
            );
        }

        return (new GetAdminProjectsResponse)
            ->setProjects($projects)
            ->setTotalCount($projectsCollection->getTotalCount());
    }
}