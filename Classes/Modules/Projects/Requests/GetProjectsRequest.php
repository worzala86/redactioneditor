<?php

namespace App\Modules\Projects\Requests;

use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;
use App\UserRequest;

class GetProjectsRequest extends UserRequest
{
    use PaginationTrait;
}