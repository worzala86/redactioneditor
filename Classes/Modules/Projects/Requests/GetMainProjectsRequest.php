<?php

namespace App\Modules\Projects\Requests;

use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;
use App\Request;

class GetMainProjectsRequest extends Request
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;
}