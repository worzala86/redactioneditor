<?php

namespace App\Modules\Projects\Requests;

use App\PaginationTrait;
use App\UserRequest;

class GetProjectsByTagRequest extends UserRequest
{
    use PaginationTrait;

    private $tag;

    /**
     * @param string $id
     * @description Tag projektu na podstawie którego będzie utworzona lista projektów
     * @return $this
     */
    public function setTag(string $tag)
    {
        $this->tag = $tag;
        return $this;
    }

    public function getTag(): string
    {
        return $this->tag;
    }
}