<?php

namespace App\Modules\Projects\Requests;

use App\Types\UUID;
use App\UserRequest;

class ForkProjectRequest extends UserRequest
{
    private $id;
    private $soft;
    private $newId;

    /**
     * @param UUID $id
     * @description Identyfikator klonowanego projektu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param bool $soft
     * @description Jeżeli jest true to kopiuje bez styli głównych itd. dla kreatora strony
     * @return $this
     */
    public function setSoft(bool $soft)
    {
        $this->soft = $soft;
        return $this;
    }

    public function getSoft(): ?bool
    {
        return $this->soft;
    }

    /**
     * @param UUID $newId
     * @description Identyfikator projektu który ma zostać skopiowany
     * @return $this
     */
    public function setNewId(UUID $newId)
    {
        $this->newId = $newId;
        return $this;
    }

    public function getNewId(): ?UUID
    {
        return $this->newId;
    }
}