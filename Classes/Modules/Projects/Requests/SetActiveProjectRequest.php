<?php

namespace App\Modules\Projects\Requests;

use App\Types\UUID;
use App\UserRequest;

class SetActiveProjectRequest extends UserRequest
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator projektu który zostanie oznaczony jako główny
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}