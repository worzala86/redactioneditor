<?php

namespace App\Modules\Projects\Requests;

use App\AdminRequest;
use App\Types\UUID;

class AttachTagsToProjectRequest extends AdminRequest
{
    private $id;
    private $tags;

    /**
     * @param UUID $id
     * @description Identyfikator prodjektu do którego zostaną przypisane tagi
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $tags
     * @description Tagi które mają być przypisane do projektu
     * @return $this
     */
    public function setTags(string $tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getTags(): string
    {
        return $this->tags;
    }
}