<?php

namespace App\Modules\Projects\Requests;

use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;
use App\UserRequest;

class GetPublicProjectsRequest extends UserRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;

    private $tag;

    /**
     * @param string $tag
     * @description Tag dla którego będą pobrane projekty
     * @return $this
     */
    public function setTag(string $tag)
    {
        $this->tag = $tag;
        return $this;
    }

    public function getTag(): string
    {
        return $this->tag;
    }
}