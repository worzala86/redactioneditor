<?php

namespace App\Modules\Projects\Requests;

use App\Types\Domain;
use App\Types\UUID;
use App\UserRequest;

class UpdateProjectRequest extends UserRequest
{
    private $id;
    private $name;
    private $domain;
    private $description;

    /**
     * @param UUID $id
     * @description Identyfikator edytowanego projektu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa projektu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Domain $domain
     * @description Pliki ze adresem pod ktorym będzie udostępniona strona
     * @return $this
     */
    public function setDomain(Domain $domain = null)
    {
        $this->domain = $domain;
        return $this;
    }

    public function getDomain(): ?Domain
    {
        return $this->domain;
    }

    /**
     * @param string $description
     * @description Opis projektu
     * @return $this
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}