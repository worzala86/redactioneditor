<?php

namespace App\Modules\Projects\Requests;

use App\Types\Domain;
use App\UserRequest;

class CreateProjectRequest extends UserRequest
{
    private $name;
    private $domain;
    private $description;

    /**
     * @param string $name
     * @description Nazwa projektu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param Domain $domain
     * @description Pliki ze adresem pod ktorym będzie udostępniona strona
     * @return $this
     */
    public function setDomain(Domain $domain = null)
    {
        $this->domain = $domain;
        return $this;
    }

    public function getDomain(): ?Domain
    {
        return $this->domain;
    }

    /**
     * @param string $description
     * @description Opis projektu
     * @return $this
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}