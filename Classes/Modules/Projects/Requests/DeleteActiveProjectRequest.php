<?php

namespace App\Modules\Projects\Requests;

use App\Types\UUID;
use App\UserRequest;

class DeleteActiveProjectRequest extends UserRequest
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator usuwanego projektu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}