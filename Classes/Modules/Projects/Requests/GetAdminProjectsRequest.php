<?php

namespace App\Modules\Projects\Requests;

use App\AdminRequest;
use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;

class GetAdminProjectsRequest extends AdminRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;
}