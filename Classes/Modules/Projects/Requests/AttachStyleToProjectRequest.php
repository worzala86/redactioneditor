<?php

namespace App\Modules\Projects\Requests;

use App\AdminRequest;
use App\Types\UUID;

class AttachStyleToProjectRequest extends AdminRequest
{
    private $id;
    private $styleId;

    /**
     * @param UUID $id
     * @description Identyfikator prodjektu do którego zostaną przypisane tagi
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param UUID $styleId
     * @description Identyfikator stylu przypisywanego do projektu
     * @return $this
     */
    public function setStyleId(UUID $styleId)
    {
        $this->styleId = $styleId;
        return $this;
    }

    public function getStyleId(): UUID
    {
        return $this->styleId;
    }
}