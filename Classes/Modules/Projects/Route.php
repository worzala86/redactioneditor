<?php

namespace App;

use App\Modules\Projects\Handlers;
use App\Types\UUID;

return [
    ['method'=>'get', 'url'=>'api/projects', 'handler'=>Handlers\GetProjectsHandler::class],
    ['method'=>'get', 'url'=>'api/projects/public/main', 'handler'=>Handlers\GetMainProjectsHandler::class],
    ['method'=>'get', 'url'=>'api/projects/public/:tag', 'handler'=>Handlers\GetPublicProjectsHandler::class, 'regex'=>['tag'=>'(.*)']],
    ['method'=>'get', 'url'=>'api/admin-projects', 'handler'=>Handlers\GetAdminProjectsHandler::class],
    ['method'=>'post', 'url'=>'api/projects', 'handler'=>Handlers\CreateProjectHandler::class],
    ['method'=>'get', 'url'=>'api/projects/:id', 'handler'=>Handlers\GetProjectHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'put', 'url'=>'api/projects/:id', 'handler'=>Handlers\UpdateProjectHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'delete', 'url'=>'api/projects/:id', 'handler'=>Handlers\DeleteProjectHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'get', 'url'=>'api/projects/:id/fork', 'handler'=>Handlers\ForkProjectHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/projects/:id/fork', 'handler'=>Handlers\ForkProjectHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/projects/:id/tags', 'handler'=>Handlers\AttachTagsToProjectHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/projects/:id/find-by-tag', 'handler'=>Handlers\GetProjectsByTagHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'put', 'url'=>'api/projects/:id/styles/:styleId', 'handler'=>Handlers\AttachStyleToProjectHandler::class, 'regex'=>['id'=>UUID::$pattern, 'styleId'=>UUID::$pattern]],
    ['method'=>'get', 'url'=>'api/projects/tags', 'handler'=>Handlers\GetProjectsTagsHandler::class],
    ['method'=>'get', 'url'=>'api/projects/active', 'handler'=>Handlers\GetActiveProjectHandler::class],
    ['method'=>'delete', 'url'=>'api/projects/active', 'handler'=>Handlers\DeleteActiveProjectHandler::class],
    ['method'=>'put', 'url'=>'api/projects/:id/active', 'handler'=>Handlers\SetActiveProjectHandler::class, 'regex'=>['id'=>UUID::$pattern]],
];