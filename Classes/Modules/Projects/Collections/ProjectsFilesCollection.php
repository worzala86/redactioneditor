<?php

namespace App\Modules\Projects\Collections;

use App\CollectionTrait;
use App\Modules\Projects\Models\ProjectsFilesModel;
use App\Modules\Projects\Models\ProjectsModel;

class ProjectsFilesCollection extends ProjectsFilesModel implements \Iterator
{
    use CollectionTrait;
}