<?php

namespace App\Modules\Projects\Collections;

use App\CollectionTrait;
use App\Modules\Projects\Models\ProjectsModel;
use App\PaginationTrait;
use App\SortTrait;

class ProjectsCollection extends ProjectsModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}