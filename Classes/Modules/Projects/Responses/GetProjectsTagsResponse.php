<?php

namespace App\Modules\Projects\Responses;

use App\Containers\TagsContainer;
use App\Response;

class GetProjectsTagsResponse extends Response
{
    private $tags;

    /**
     * @param TagsContainer $products
     * @description Pele zawiera listę tagów projektów
     * @return $this
     */
    public function setTags(TagsContainer $tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getTags(): TagsContainer
    {
        return $this->tags;
    }
}