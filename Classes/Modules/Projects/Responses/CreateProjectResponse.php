<?php

namespace App\Modules\Projects\Responses;

use App\Response;
use App\Types\UUID;

class CreateProjectResponse extends Response
{
    private $id;

    /**
     * @param bool $id
     * @description Pole id zawiera numer utworzonego projektu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}