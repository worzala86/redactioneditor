<?php

namespace App\Modules\Projects\Responses;

use App\Containers\ProjectsContainer;
use App\Response;

class GetPublicProjectsResponse extends Response
{
    private $projects;

    /**
     * @param ProjectsContainer $products
     * @description Pole zawiera listę projektów
     * @return $this
     */
    public function setProjects(ProjectsContainer $projects)
    {
        $this->projects = $projects;
        return $this;
    }

    public function getProjects(): ProjectsContainer
    {
        return $this->projects;
    }
}