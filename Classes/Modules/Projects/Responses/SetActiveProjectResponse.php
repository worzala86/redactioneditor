<?php

namespace App\Modules\Projects\Responses;

use App\Modules\Statistics\Responses\GetStatisticsResponse;
use App\Response;

class SetActiveProjectResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy pomyślnie ustawiono projekt na główny
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}