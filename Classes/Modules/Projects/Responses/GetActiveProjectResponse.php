<?php

namespace App\Modules\Projects\Responses;

use App\Containers\ImageContainer;
use App\Response;
use App\Types\Domain;
use App\Types\UUID;

class GetActiveProjectResponse extends Response
{
    private $id;
    private $name;
    private $domain;
    private $description;
    private $styleName;
    private $styleImage;
    private $pagesCount;
    private $elementsCount;
    private $datasetsCount;
    private $datasetsFieldsCount;
    private $dataset;

    /**
     * @param int $id
     * @description Identyfikator projektu - do wykorzystywania np. przy edycji i usuwaniu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa projektu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Domain $domain
     * @description Zawiera adres url strony projektu
     * @return $this
     */
    public function setDomain(Domain $domain = null)
    {
        $this->domain = $domain;
        return $this;
    }

    public function getDomain(): ?Domain
    {
        return $this->domain;
    }

    /**
     * @param string $description
     * @description Opis projektu
     * @return $this
     */
    public function setDescription(string $description = null)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $styleName
     * @description Opis projektu
     * @return $this
     */
    public function setStyleName(string $styleName = null)
    {
        $this->styleName = $styleName;
        return $this;
    }

    public function getStyleName(): ?string
    {
        return $this->styleName;
    }

    /**
     * @param ImageContainer $styleImage
     * @description Opis projektu
     * @return $this
     */
    public function setStyleImage(ImageContainer $styleImage = null)
    {
        $this->styleImage = $styleImage;
        return $this;
    }

    public function getStyleImage(): ?ImageContainer
    {
        return $this->styleImage;
    }

    /**
     * @param int $pagesCount
     * @description Liczba stron projektu
     * @return $this
     */
    public function setPagesCount(int $pagesCount)
    {
        $this->pagesCount = $pagesCount;
        return $this;
    }

    public function getPagesCount(): int
    {
        return $this->pagesCount;
    }

    /**
     * @param int $elementsCount
     * @description Liczba stron projektu
     * @return $this
     */
    public function setElementsCount(int $elementsCount)
    {
        $this->elementsCount = $elementsCount;
        return $this;
    }

    public function getElementsCount(): int
    {
        return $this->elementsCount;
    }

    /**
     * @param int $datasetsCount
     * @description Liczba stron projektu
     * @return $this
     */
    public function setDatasetsCount(int $datasetsCount)
    {
        $this->datasetsCount = $datasetsCount;
        return $this;
    }

    public function getDatasetsCount(): int
    {
        return $this->datasetsCount;
    }

    /**
     * @param int $datasetsFieldsCount
     * @description Liczba stron projektu
     * @return $this
     */
    public function setDatasetsFieldsCount(int $datasetsFieldsCount)
    {
        $this->datasetsFieldsCount = $datasetsFieldsCount;
        return $this;
    }

    public function getDatasetsFieldsCount(): int
    {
        return $this->datasetsFieldsCount;
    }

    /**
     * @param array $dataset
     * @description Wartość przechowuje tablice z datą i ilością nowych stron w danym dniu
     * @return $this
     */
    public function setDataset(array $dataset){
        $this->dataset = $dataset;
        return $this;
    }

    public function getDataset(): array {
        return $this->dataset;
    }
}