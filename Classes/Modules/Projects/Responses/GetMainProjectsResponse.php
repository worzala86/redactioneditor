<?php

namespace App\Modules\Projects\Responses;

use App\Containers\ProjectsContainer;
use App\Response;

class GetMainProjectsResponse extends Response
{
    private $projects;

    /**
     * @param ProjectsContainer $products
     * @description Pele zawiera listę projektów
     * @return $this
     */
    public function setProjects(ProjectsContainer $projects)
    {
        $this->projects = $projects;
        return $this;
    }

    public function getProjects(): ProjectsContainer
    {
        return $this->projects;
    }
}