<?php

namespace App\Modules\Projects\Responses;

use App\Response;

class DeleteActiveProjectResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy pomyślnie usunięto projekt
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}