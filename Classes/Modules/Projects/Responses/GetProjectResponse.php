<?php

namespace App\Modules\Projects\Responses;

use App\Response;
use App\Types\Domain;
use App\Types\UUID;

class GetProjectResponse extends Response
{
    private $id;
    private $name;
    private $domain;
    private $description;

    /**
     * @param int $id
     * @description Identyfikator projektu - do wykorzystywania np. przy edycji i usuwaniu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa projektu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Domain $domain
     * @description Zawiera adres url strony projektu
     * @return $this
     */
    public function setDomain(Domain $domain = null)
    {
        $this->domain = $domain;
        return $this;
    }

    public function getDomain(): ?Domain
    {
        return $this->domain;
    }

    /**
     * @param string $description
     * @description Opis projektu
     * @return $this
     */
    public function setDescription(string $description = null)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}