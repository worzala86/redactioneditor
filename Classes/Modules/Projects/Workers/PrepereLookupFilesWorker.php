<?php

namespace App\Modules\Projects\Workers;

use App\Containers\HtmlElementContainer;
use App\Database\DB;
use App\Exceptions\ProjectNotFoundException;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Styles\Collections\StylesCssViewCollection;
use App\Modules\Styles\Collections\StylesFontsCollection;
use App\Types\UUID;
use App\Workers\HtmlWorker;

class PrepereLookupFilesWorker
{
    private $projectUuid;
    private $components;
    private $tags;

    //private $customStyle;

    public function prepare(int $projectId)
    {
        $projectsModel = (new ProjectsModel)
            ->load($projectId);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $this->projectUuid = (string)$projectsModel->getUuid();

        if (is_dir('../../podglad/' . $this->projectUuid)) {
            $this->deleteDir('../../podglad/' . $this->projectUuid);
        }
        mkdir('../../podglad/' . $this->projectUuid);

        $fontsCollection = (new StylesFontsCollection)
            ->where('styles_id=?', $projectsModel->getStyleId())
            ->loadAll();
        $fonts = '';
        foreach ($fontsCollection as $fontsModel) {
            $fonts .= '<link rel="stylesheet" href="' . $fontsModel->getLink() . '">' . PHP_EOL;
        }

        $index = file_get_contents('../../Public/Templates/LookupTemplate.html');
        $index = str_replace('[[runScript]]', '/podglad/' . $this->projectUuid . '/run.js', $index);
        $index = str_replace('[[routesScript]]', '/podglad/' . $this->projectUuid . '/routes.js', $index);
        $index = str_replace('[[rootStyle]]', '/podglad/' . $this->projectUuid . '/rootStyle.css', $index);
        $index = str_replace('[[customStyle]]', '/podglad/' . $this->projectUuid . '/customStyle.css', $index);
        $index = str_replace('[[projectPath]]', '/podglad/' . $this->projectUuid, $index);
        $index = str_replace('[[fonts]]', $fonts, $index);
        file_put_contents('../../podglad/' . $this->projectUuid . '/index.html', $index);
        $run = 'const base = \'/podglad/' . $this->projectUuid . '\'
const apiBase = \'/api\'
const templateBase = \'/Public/Templates/\'
const templates = \'/podglad/' . $this->projectUuid . '/Templates/\'
const projectId=\'' . $this->projectUuid . '\'';
        file_put_contents('../../podglad/' . $this->projectUuid . '/run.js', $run);

        if (!file_exists('../../podglad/' . $this->projectUuid . '/Templates')) {
            mkdir('../../podglad/' . $this->projectUuid . '/Templates');
        }

        $this->loadComponents();

        $this->createRootStyle($projectsModel->getStyleId());
        $this->createCustomStyle($projectsModel->getCustomStyleId());

        $pages = DB::get()->getAll('select pages.id, content_id, pages.uuid, 
            pages_elements.uuid as element_position_id, url from pages 
            left join pages_elements on pages_elements.page_id=pages.id
            where project_id=? and deleted=0', $projectsModel->getId());
        foreach ($pages as $page) {
            $elementContents = DB::get()->getAll('select elements.content_id as id, pages_elements.uuid from elements left join pages_elements 
                on pages_elements.element_id=elements.id where pages_elements.page_id=?', $page['id']);
            $this->createTemplate(null, bin2hex($page['uuid']), $elementContents);
        }

        $headers = DB::get()->getAll('select elements.content_id as id, projects_elements.uuid from projects_elements 
            left join elements on elements.id=projects_elements.element_id 
            where project_id=? and projects_elements.kind=\'header\'', $projectsModel->getId());
        $this->createTemplate(null, 'header', $headers);
        $sidebarLeft = DB::get()->getAll('select elements.content_id as id, projects_elements.uuid from projects_elements 
            left join elements on elements.id=projects_elements.element_id 
            where project_id=? and projects_elements.kind=\'sidebar-left\'', $projectsModel->getId());
        $this->createTemplate(null, 'sidebar-left', $sidebarLeft);
        $sidebarRight = DB::get()->getAll('select elements.content_id as id, projects_elements.uuid from projects_elements 
            left join elements on elements.id=projects_elements.element_id 
            where project_id=? and projects_elements.kind=\'sidebar-right\'', $projectsModel->getId());
        $this->createTemplate(null, 'sidebar-right', $sidebarRight);
        $footer = DB::get()->getAll('select elements.content_id as id, projects_elements.uuid from projects_elements 
            left join elements on elements.id=projects_elements.element_id 
            where project_id=? and projects_elements.kind=\'footer\'', $projectsModel->getId());
        $this->createTemplate(null, 'footer', $footer);

        $this->createRoutes($pages);
    }

    private function createCustomStyle($styleId)
    {
        $stylesCssCollection = (new StylesCssViewCollection)
            ->where('styles_id=?', $styleId)
            ->loadAll();

        $csss = [];
        foreach ($stylesCssCollection as $styleCssModel) {
            $csss[$styleCssModel->getTag()][$styleCssModel->getParam()] = $styleCssModel->getValue();
        }
        $style = '';
        foreach ($csss as $tagName => $css) {
            $tagCss = '';
            foreach ($css as $param => $value) {
                if (($value !== null) && ($value !== '')) {
                    $tagCss .= '    ' . $param . ': ' . $value . ';' . PHP_EOL;
                }
            }
            $style .= $tagName . ' {' . PHP_EOL .
                $tagCss .
                '}' . PHP_EOL . PHP_EOL;
            $this->tags[] = $tagName;
        }
        file_put_contents('../../podglad/' . $this->projectUuid . '/customStyle.css', $style);
    }

    private function createRootStyle($styleId)
    {
        $stylesCssCollection = (new StylesCssViewCollection)
            ->where('styles_id=?', $styleId)
            ->loadAll();

        $csss = [];
        foreach ($stylesCssCollection as $styleCssModel) {
            $csss[$styleCssModel->getTag()][$styleCssModel->getParam()] = $styleCssModel->getValue();
        }
        $style = '';
        foreach ($csss as $tagName => $css) {
            $tagCss = '';
            foreach ($css as $param => $value) {
                if (($value !== null) && ($value !== '')) {
                    $tagCss .= '    ' . $param . ': ' . $value . ';' . PHP_EOL;
                }
            }
            $style .= $tagName . ' {' . PHP_EOL .
                $tagCss .
                '}' . PHP_EOL . PHP_EOL;
        }
        file_put_contents('../../podglad/' . $this->projectUuid . '/rootStyle.css', $style);
    }

    private function createRoutes(array $pages)
    {
        $buf = 'const routes = [' . PHP_EOL;
        foreach ($pages as $page) {
            $buf .= '    {' . PHP_EOL .
                '        url: base+\'' . $page['url'] . '\',' . PHP_EOL .
                '        template: templates+\'' . bin2hex($page['uuid']) . '.html\',' . PHP_EOL .
                '    },' . PHP_EOL;
        }
        $buf .= ']';
        file_put_contents('../../podglad/' . $this->projectUuid . '/routes.js', $buf);
    }

    private function deleteDir(string $dirPath)
    {
        if (!is_dir($dirPath)) {
            throw new \Exception("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    private function loadComponents()
    {
        $files = glob('../../Public/Templates/LookupComponents/*');
        foreach ($files as $file) {
            if (is_file($file)) {
                $fileName = explode('/', $file);
                $this->components[str_replace('.html', '', $fileName[count($fileName) - 1])] = file_get_contents($file);
            }
        }
    }

    private function getComponent(string $name)
    {
        return $this->components[$name];
    }

    private function tagExistsInCustomStyle($html)
    {
        if (!$this->tags) {
            return false;
        }
        foreach ($this->tags as $t) {
            if ($t == '#id-' . ($html->getElementPositionId() . '-' . ($html->getAltId() ? $html->getAltId() : $html->getId()))) {
                return true;
            }
        }
        return false;
    }

    private function getHtml(HtmlElementContainer $html, int $level = 0, $selector = false)
    {
        $buff = '';
        $component = $this->getComponent($html->getType());

        $tag = null;
        if ($html->getType() == 'container') {
            $tag = 'span';
        }

        $tabs = '';
        for ($i = 0; $i < $level; $i++) {
            $tabs .= '   ';
        }

        if ($tag) {
            $id = '';
            if ($this->tagExistsInCustomStyle($html)) {
                $id = ' id="id-' . $html->getElementPositionId() . '-' . $html->getId() . '"';
            }
            $class = '';
            if ($html->getClass()) {
                $class = ' class="' . $html->getClass() . '"';
            }
            $buff = $tabs . '<' . $tag . $id . $class . $selector . '>' . PHP_EOL;
            if ($selector) {
                $selector = false;
            }
            if ($html->getSelector()) {
                $selector = ' ng-repeat="dataSet in dataSet[\'' . $html->setElementPositionId() . '\'][\'' . $html->getSelector() . '\']"';
            }
            ++$level;
            foreach ($html->getChildrens() as $node) {
                $buff .= $this->getHtml($node, $level, $selector) . PHP_EOL;
            }
            $buff .= $tabs . '</' . $tag . '>';
        } else {
            if ($html->getShow()) {
                //if ($selector&&($html->getField()!='')) {
                //    $component = str_replace('$root.dataSet[element.field]', 'element.' . $html->getField(), $component);
                //}
                $component = str_replace(' id="id{{element.altId}}"', $this->tagExistsInCustomStyle($html) ? (' id="id-' . $html->getElementPositionId() . '-' . $html->getAltId() . '"') : '', $component);
                $component = str_replace(' class="{{element.class}}"', $html->getClass() ? (' class="' . $html->getClass() . '"') : '', $component);
                $component = str_replace('element.src', '\'' . $html->getSrc() . '\'', $component);
                $component = str_replace('element.picTitle', '\'' . $html->getPicTitle() . '\'', $component);
                $component = str_replace('[element.field', '[\'' . $html->getElementPositionId() . '\'][\'' . $html->getField() . '\'', $component);
                $component = str_replace('{{element.placeholder}}', $html->getPlaceholder(), $component);
                $component = str_replace('{{element.id}}', $html->getId(), $component);
                $component = str_replace('element.alt', '\'' . $html->getAlt() . '\'', $component);
                $component = str_replace('{{element.target}}', $html->getTarget(), $component);
                $component = str_replace('{{element.href}}', $html->getHref(), $component);
                $component = str_replace('{{element.html}}', $html->getHtml(), $component);
                $buff .= $tabs . $component;
            }
        }

        return $buff;
    }

    private function createTemplate(int $htmlId = null, string $templateName, array $elementContents = null)
    {
        //if (!isset($htmlId)) {
        //    return;
        //}
        $htmlWorker = new HtmlWorker;
        $html = '';
        if ($htmlId) {
            $htmls = $htmlWorker->loadHtmlElements($htmlId);
            if ($htmls) {
                $html = $this->getHtml($htmls);
            }
        } else {
            if ($elementContents) {
                foreach ($elementContents as $content) {
                    $htmls = $htmlWorker->loadHtmlElements($content['id'], isset($content['uuid']) ? (new UUID($content['uuid'])) : null);
                    $html .= $this->getHtml($htmls);
                }
            }
        }
        file_put_contents(ROOT_DIRECTORY . '/podglad/' . $this->projectUuid . '/Templates/' . $templateName . '.html', $html);

        //$style = $this->createStyle($htmls);
        //$this->customStyle .= $style;
    }

    /*private function createStyle(HtmlElementContainer $html)
    {
        $buf = '';
        if ($html->getStyle()) {
            $buf .= '#id' . ($html->getAltId() ? $html->getAltId() : $html->getId()) . ' {' . PHP_EOL;
            foreach ($html->getStyle() as $key => $value) {
                $key = preg_replace_callback('/[A-Z]/', function ($match) {
                    return '-' . lcfirst($match[0]);
                }, $key);
                $buf .= '    ' . $key . ': ' . $value . ';' . PHP_EOL;
            }
            $buf .= '}' . PHP_EOL . PHP_EOL;
        }
        if ($html->getChildrens()) {
            foreach ($html->getChildrens() as $node) {
                $buf .= $this->createStyle($node);
            }
        }
        return $buf;
    }*/
}