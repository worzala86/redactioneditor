<?php

namespace App\Modules\Projects\Exceptions;

use App\NamedException;

class StyleNotFoundException extends \Exception implements NamedException
{
}