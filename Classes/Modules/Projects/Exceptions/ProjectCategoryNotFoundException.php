<?php

namespace App\Modules\Projects\Exceptions;

use App\NamedException;

class ProjectCategoryNotFoundException extends \Exception implements NamedException
{
}