<?php

namespace App\Modules\Projects\Exceptions;

use App\NamedException;

class CategoryNotFoundException extends \Exception implements NamedException
{
}