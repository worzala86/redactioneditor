<?php

namespace App\Modules\Projects\Exceptions;

use App\NamedException;

class ProjectNotFoundException extends \Exception implements NamedException
{
}