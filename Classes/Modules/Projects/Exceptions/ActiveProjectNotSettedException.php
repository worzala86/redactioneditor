<?php

namespace App\Modules\Projects\Exceptions;

use App\NamedException;

class ActiveProjectNotSettedException extends \Exception implements NamedException
{
}