<?php

namespace App\Modules\Projects\Exceptions;

use App\NamedException;

class FileNotFoundException extends \Exception implements NamedException
{
}