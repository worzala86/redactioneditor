<?php

namespace App\Modules\Projects\Exceptions;

use App\NamedException;

class ProjectNotAddedException extends \Exception implements NamedException
{
}