<?php

namespace App\Modules\Projects\Models;

use App\Model;
use App\Types\Domain;
use App\Types\Time;
use App\Types\UUID;

class ProjectsModel extends Model
{
    private $id;
    private $name;
    private $uuid;
    private $userId;
    private $domain;
    private $showLeftSidebar;
    private $showRightSidebar;
    private $config;
    private $forkedId;
    private $added;
    private $updated;
    private $styleId;
    private $customStyleId;
    private $description;

    public function setDescription(string $description = null)
    {
        $this->set('description', $description);
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setCustomStyleId(int $customStyleId = null)
    {
        $this->set('custom_style_id', $customStyleId);
        $this->customStyleId = $customStyleId;
        return $this;
    }

    public function getCustomStyleId(): ?int
    {
        return $this->customStyleId;
    }

    public function setStyleId(int $styleId = null)
    {
        $this->set('style_id', $styleId);
        $this->styleId = $styleId;
        return $this;
    }

    public function getStyleId(): ?int
    {
        return $this->styleId;
    }

    public function setUpdated(Time $updated = null)
    {
        $this->set('updated', $updated);
        $this->updated = $updated;
        return $this;
    }

    public function getUpdated(): ?Time
    {
        return $this->updated;
    }

    public function setAdded(Time $added)
    {
        $this->set('added', $added);
        $this->added = $added;
        return $this;
    }

    public function getAdded(): Time
    {
        return $this->added;
    }

    public function setForkedId(int $forkedId = null)
    {
        $this->set('forked_id', $forkedId);
        $this->forkedId = $forkedId;
        return $this;
    }

    public function getForkedId(): ?int
    {
        return $this->forkedId;
    }

    public function setConfig(string $config = null)
    {
        $this->set('config', $config);
        $this->config = $config;
        return $this;
    }

    public function getConfig(): ?string
    {
        return $this->config;
    }

    public function setShowRightSidebar(bool $showRightSidebar)
    {
        $this->set('show_right_sidebar', $showRightSidebar);
        $this->showRightSidebar = $showRightSidebar;
        return $this;
    }

    public function getShowRightSidebar(): ?bool
    {
        return $this->showRightSidebar;
    }

    public function setShowLeftSidebar(bool $showLeftSidebar)
    {
        $this->set('show_left_sidebar', $showLeftSidebar);
        $this->showLeftSidebar = $showLeftSidebar;
        return $this;
    }

    public function getShowLeftSidebar(): ?bool
    {
        return $this->showLeftSidebar;
    }

    public function setDomain(Domain $domain = null)
    {
        $this->set('domain', $domain);
        $this->domain = $domain;
        return $this;
    }

    public function getDomain(): ?Domain
    {
        return $this->domain;
    }

    public function setUserId(int $userId)
    {
        $this->set('user_id', $userId);
        $this->userId = $userId;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setName(string $name)
    {
        $this->set('name', $name);
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}