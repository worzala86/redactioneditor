<?php

namespace App\Modules\Projects\Models;

use App\Model;
use App\Types\Url;
use App\Types\UUID;

class PagesModel extends Model
{
    private $id;
    private $uuid;
    private $projectId;
    private $url;
    private $userId;

    public function setUserId(int $userId)
    {
        $this->set('user_id', $userId);
        $this->userId = $userId;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUrl(Url $url)
    {
        $this->set('url', $url);
        $this->url = $url;
        return $this;
    }

    public function getUrl(): Url
    {
        return $this->url;
    }

    public function setProjectId(int $projectId)
    {
        $this->set('project_id', $projectId);
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): int
    {
        return $this->projectId;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}