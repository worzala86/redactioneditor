<?php

namespace App\Modules\Others\Handlers;

use App\Containers\HtmlElementContainer;
use App\Database\DB;
use App\Database\Upgrade;
use App\Handler;
use App\Modules\Others\Requests\HtmlRequest;
use App\Modules\Others\Responses\HtmlResponse;
use App\Request;
use App\Response;
use App\Types\Time;
use App\Types\UUID;
use App\Workers\HtmlWorker;

/**
 * Class GridHandler
 * @package App\Modules\Others\Handlers
 * @description Metoda testowa
 */
class GridHandler extends Handler
{
    public function __invoke(Request $request): Response
    {
        $csss['.row']['width'] = '100%';
        $csss['.row']['box-sizing'] = 'border-box';
        $csss['.row']['display'] = 'flex';
        $csss['.row']['flex-direction'] = 'row';

        $csss['.row.middle']['align-items'] = 'center';
        $csss['.row.top']['align-items'] = 'flex-start';
        $csss['.row.bottom']['align-items'] = 'flex-end';

        for ($i = 0; $i < 12; $i++) {
            $width = (100 / 12) * ($i + 1);
            $csss['.col-xs-' . ($i + 1)]['flex-basis'] = round($width, 5) . '%';
        }
        for ($i = 0; $i < 12; $i++) {
            $width = (100 / 12) * ($i + 1);
            $csss['.offset-xs-' . ($i + 1)]['margin-left'] = round($width, 5) . '%';
        }
        for ($i = 0; $i < 12; $i++) {
            $width = (100 / 12) * ($i + 1);
            $csss['.pull-xs-' . ($i + 1)]['margin-right'] = round($width, 5) . '%';
        }

        $buf = '';
        foreach ($csss as $tag => $css) {
            $params = '';
            foreach ($css as $param => $value) {
                $params .= '    ' . $param . ': ' . $value . ';' . PHP_EOL;
            }
            $buf .= $tag . '{' . PHP_EOL .
                $params .
                '}' . PHP_EOL;
        }

        $screens[] = '@media screen and (min-width: 500px)';
        $screens[] = '@media screen and (min-width: 700px)';
        $screens[] = '@media screen and (min-width: 900px)';
        $screens[] = '@media screen and (min-width: 1100px)';

        foreach ($screens as $screen) {
            $buf .= $screen.'{'.PHP_EOL;
            foreach ($csss as $tag => $css) {
                if ((strpos($tag, '.col') !== false) ||
                    (strpos($tag, '.offset') !== false) ||
                    (strpos($tag, '.pull') !== false)) {
                    $params = '';
                    foreach ($css as $param => $value) {
                        $params .= '        ' . $param . ': ' . $value . ';' . PHP_EOL;
                    }
                    $buf .= '   '.$tag . '{' . PHP_EOL .
                        $params .
                        '   }' . PHP_EOL;
                }
            }
            $buf .= '}'.PHP_EOL;
        }

        $buf .= '
@media screen and (max-width: 500px) {
    .hide-xs {
        display: none;
    }
}

@media screen and (min-width: 500px) {
    .hide-sm {
        display: none;
    }
}

@media screen and (min-width: 700px) {
    .hide-md {
        display: none;
    }
}

@media screen and (min-width: 900px) {
    .hide-lg {
        display: none;
    }
}

@media screen and (min-width: 1100px) {
    .hide-xl {
        display: none;
    }
}

@media screen and (max-width: 500px) {
    .show-xs {
        display: block;
    }
}

@media screen and (min-width: 500px) {
    .show-sm {
        display: block;
    }
}

@media screen and (min-width: 700px) {
    .show-md {
        display: block;
    }
}

@media screen and (min-width: 900px) {
    .show-lg {
        display: block;
    }
}

@media screen and (min-width: 1100px) {
    .show-xl {
        display: block;
    }
}';

        echo $buf;

        /*echo '<style>' . $buf . '</style>';

        echo file_get_contents('../../Public/Templates/GridTest.html');
*/
        exit;
    }
}