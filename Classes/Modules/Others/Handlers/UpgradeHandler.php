<?php

namespace App\Modules\Others\Handlers;

use App\Database\Upgrade;
use App\Handler;
use App\Modules\Others\Requests\UpgradeRequest;
use App\Modules\Others\Responses\UpgradeResponse;

/**
 * Class UpgradeHandler
 * @package App\Modules\Others\Handlers
 * @description Metoda służy do upgradeowania strony
 */
class UpgradeHandler extends Handler
{
    public function __invoke(UpgradeRequest $request): UpgradeResponse
    {
        $upgrade = new Upgrade();
        $upgrade->upgrade();

        $version = $upgrade->version();

        return (new UpgradeResponse)
            ->setSuccess(true)
            ->setVersion($version);
    }
}