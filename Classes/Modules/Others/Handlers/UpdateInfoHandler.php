<?php

namespace App\Modules\Others\Handlers;

use App\Database\Upgrade;
use App\Handler;
use App\Modules\Others\Requests\UpdateInfoRequest;
use App\Modules\Others\Responses\UpdateInfoResponse;

/**
 * Class UpdateInfoHandler
 * @package App\Modules\Others\Handlers
 * @description Metoda służy do pobierania informacji o aktualizacji
 */
class UpdateInfoHandler extends Handler
{
    public function __invoke(UpdateInfoRequest $request): UpdateInfoResponse
    {
        $upgrade = new Upgrade();

        $version = $upgrade->version();
        $newVersion = $upgrade->newVersion();

        return (new UpdateInfoResponse)
            ->setVersion($version)
            ->setNewVersion($newVersion);
    }
}