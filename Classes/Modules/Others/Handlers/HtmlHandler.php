<?php

namespace App\Modules\Others\Handlers;

use App\Containers\HtmlElementContainer;
use App\Containers\HtmlElementsContainer;
use App\Curl;
use App\Database\DB;
use App\Database\Upgrade;
use App\Handler;
use App\LoremIpsum;
use App\Modules\Elements\Collections\ElementsDatasetsViewCollection;
use App\Modules\Elements\Models\ElementsDatasetsModel;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Others\Requests\HtmlRequest;
use App\Modules\Others\Responses\HtmlResponse;
use App\Modules\Pages\Models\PagesDatasetsModel;
use App\Modules\Pages\Models\PagesElementsModel;
use App\Request;
use App\Types\ElementKind;
use App\Types\ShortUUID;
use App\Types\Time;
use App\Types\UUID;
use App\Workers\HtmlWorker;
use stdClass;

/**
 * Class HtmlHandler
 * @package App\Modules\Others\Handlers
 * @description Metoda testowa
 */
class HtmlHandler extends Handler
{
    public function __invoke(HtmlRequest $request): HtmlResponse
    {
        //DB::get()->execute('start transaction');

        $jsons = DB::get()->getAll('select * from (select distinct json, count(*) as c from jsons group by json) as x where c>10');

        foreach($jsons as $json) {
            $object = json_decode($json['json']);

            $element = $this->saveHTML($object);
            //$element->setElementId($elementId);
            //$element->setDataSet($dataSet);

            $htmlWorker = new \App\Workers\HtmlWorker;
            $htmlId = $htmlWorker->saveHtmlElements($element);

            $elementId = (new ElementsModel)
                ->setContentId($htmlId)
                ->setUuid(UUID::fake())
                ->setKind(new ElementKind('body'))
                ->setRootStyleId(DEFAULT_STYLE_ID)
                ->setUserId($request->getCurrentUserId())
                ->insert();

            DB::get()->execute('update html set element_id=? where id=?', [$elementId, $htmlId]);
            //exit;
        }

        exit;

        //DB::get()->execute('commit');
    }

    function saveHTML(stdClass $object)
    {
        $html = new \App\Containers\HtmlElementContainer;
        $html->setType(DB::get()->getOne('select `key` from components where id=?', $object->t));
        $html->setAltId(\App\Types\ShortUUID::fake());
        $html->setId(\App\Types\ShortUUID::fake());
        $target = \App\Types\ShortUUID::fake();
        //$lp = new LoremIpsum;
        //$dataSet = $lp->sentences($lp->random());
        $html->setField($target);
        $childrens = new \App\Containers\HtmlElementsContainer;
        if(isset($object->c)) {
            foreach ($object->c as $node) {
                $childrens->add($this->saveHTML($node));
            }
            $html->setChildrens($childrens);
        }
        return $html;
    }
}