<?php

namespace App;

use App\Modules\Others\Handlers;

return [
    ['method'=>'get', 'url'=>'api/html', 'handler'=>Handlers\HtmlHandler::class],
    ['method'=>'get', 'url'=>'api/upgrade', 'handler'=>Handlers\UpgradeHandler::class],
    ['method'=>'get', 'url'=>'api/update-info', 'handler'=>Handlers\UpdateInfoHandler::class],
    ['method'=>'get', 'url'=>'api/grid', 'handler'=>Handlers\GridHandler::class],

    ['method'=>'get', 'url'=>'api/parse/page', 'handler'=>Handlers\ParsePageHandler::class],
    ['method'=>'get', 'url'=>'api/parse/html', 'handler'=>Handlers\ParseHtmlHandler::class],
    ['method'=>'get', 'url'=>'api/parse/html2json', 'handler'=>Handlers\ParseHtml2JsonHandler::class],
    ['method'=>'get', 'url'=>'api/scrap/pages', 'handler'=>Handlers\ScrapPagesHandler::class],
];