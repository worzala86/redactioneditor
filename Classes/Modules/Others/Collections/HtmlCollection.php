<?php

namespace App\Modules\Others\Collections;

use App\CollectionTrait;
use App\Modules\Others\Models\HtmlModel;

class HtmlCollection extends HtmlModel implements \Iterator
{
    use CollectionTrait;
}