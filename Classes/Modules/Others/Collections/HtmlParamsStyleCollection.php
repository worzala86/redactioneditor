<?php

namespace App\Modules\Others\Collections;

use App\CollectionTrait;
use App\Modules\Others\Models\HtmlParamsStyleModel;

class HtmlParamsStyleCollection extends HtmlParamsStyleModel implements \Iterator
{
    use CollectionTrait;
}