<?php

namespace App\Modules\Others\Collections;

use App\CollectionTrait;
use App\Modules\Others\Models\HtmlStyleModel;

class HtmlStyleCollection extends HtmlStyleModel implements \Iterator
{
    use CollectionTrait;
}