<?php

namespace App\Modules\Others\Collections;

use App\CollectionTrait;
use App\Modules\Others\Models\HtmlParamsDatasetsModel;
use App\Modules\Others\Models\HtmlParamsStyleModel;

class HtmlParamsDatasetsCollection extends HtmlParamsDatasetsModel implements \Iterator
{
    use CollectionTrait;
}