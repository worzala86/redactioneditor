<?php

namespace App\Modules\Others\Models;

use App\Model;
use App\Types\ShortUUID;
use App\Types\Time;

class HtmlModel extends Model
{
    public $setDefaultFields = false;

    private $id;
    private $field;
    private $altId;
    private $parentId;
    private $htmlId;
    private $componentId;
    private $htmlClassId;
    private $updated;
    private $elementId;
    private $show;
    private $dataSetActive;
    private $backgroundActive;

    public function setBackgroundActive(bool $backgroundActive)
    {
        $this->set('background_active', $backgroundActive);
        $this->backgroundActive = $backgroundActive;
        return $this;
    }

    public function getBackgroundActive(): bool
    {
        return $this->backgroundActive;
    }

    public function setDataSetActive(bool $dataSetActive)
    {
        $this->set('data_set_active', $dataSetActive);
        $this->dataSetActive = $dataSetActive;
        return $this;
    }

    public function getDataSetActive(): bool
    {
        return $this->dataSetActive;
    }

    public function setShow(bool $show)
    {
        $this->set('show', $show);
        $this->show = $show;
        return $this;
    }

    public function getShow(): bool
    {
        return $this->show;
    }

    public function setElementId(int $elementId = null)
    {
        $this->set('element_id', $elementId);
        $this->elementId = $elementId;
        return $this;
    }

    public function getElementId(): ?int
    {
        return $this->elementId;
    }

    public function setUpdated(Time $updated = null)
    {
        $this->set('updated', $updated);
        $this->updated = $updated;
        return $this;
    }

    public function getUpdated(): ?Time
    {
        return $this->updated;
    }

    public function setHtmlClassId(int $htmlClassId = null)
    {
        $this->set('html_class_id', $htmlClassId);
        $this->htmlClassId = $htmlClassId;
        return $this;
    }

    public function getHtmlClassId(): ?int
    {
        return $this->htmlClassId;
    }

    public function setComponentId(int $componentId)
    {
        $this->set('component_id', $componentId);
        $this->componentId = $componentId;
        return $this;
    }

    public function getComponentId(): int
    {
        return $this->componentId;
    }

    public function setHtmlId(ShortUUID $htmlId = null)
    {
        $this->set('html_id', $htmlId ? hex2bin($htmlId) : null);
        $this->htmlId = $htmlId;
        return $this;
    }

    public function getHtmlId(): ?ShortUUID
    {
        return $this->htmlId;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setParentId(int $parentId = null)
    {
        $this->set('parent_id', $parentId);
        $this->parentId = $parentId;
        return $this;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function setField(string $field = null)
    {
        $this->set('field', $field);
        $this->field = $field;
        return $this;
    }

    public function getField(): ?string
    {
        return $this->field;
    }

    public function setAltId(ShortUUID $altId = null)
    {
        $this->set('alt_id', $altId ? hex2bin($altId) : null);
        $this->altId = $altId;
        return $this;
    }

    public function getAltId(): ?ShortUUID
    {
        return $this->altId;
    }
}