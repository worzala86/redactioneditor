<?php

namespace App\Modules\Others\Models;

use App\Model;

class HtmlParamsDatasetsModel extends Model
{
    public $setDefaultFields = false;

    private $htmlId;
    private $key;
    private $value;

    public function setValue(string $value)
    {
        $this->set('value', $value);
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setKey(string $key)
    {
        $this->set('key', $key);
        $this->key = $key;
        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setHtmlId(int $htmlId)
    {
        $this->set('html_id', $htmlId);
        $this->htmlId = $htmlId;
        return $this;
    }

    public function getHtmlId(): int
    {
        return $this->htmlId;
    }
}