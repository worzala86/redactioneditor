<?php

namespace App\Modules\Others\Models;

use App\Model;

class HtmlParamsStyleValuesModel extends Model
{
    public $setDefaultFields = false;

    private $id;
    private $value;

    public function setValue(string $value)
    {
        $this->set('value', $value);
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setId(int $id)
    {
        $this->set('_id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}