<?php

namespace App\Modules\Others\Models;

use App\Model;

class HtmlParamsStyleModel extends Model
{
    public $setDefaultFields = false;

    private $htmlId;
    private $htmlParamsStyleKindsId;
    private $htmlParamsStyleValueId;

    public function setHtmlParamsStyleValueId(int $htmlParamsStyleValueId)
    {
        $this->set('html_params_style_value_id', $htmlParamsStyleValueId);
        $this->htmlParamsStyleValueId = $htmlParamsStyleValueId;
        return $this;
    }

    public function getHtmlParamsStyleValueId(): int
    {
        return $this->htmlParamsStyleValueId;
    }

    public function setHtmlParamsStyleKindsId(int $htmlParamsStyleKindsId)
    {
        $this->set('html_params_style_kinds_id', $htmlParamsStyleKindsId);
        $this->htmlParamsStyleKindsId = $htmlParamsStyleKindsId;
        return $this;
    }

    public function getHtmlParamsStyleKindsId(): int
    {
        return $this->htmlParamsStyleKindsId;
    }

    public function setHtmlId(int $htmlId)
    {
        $this->set('html_id', $htmlId);
        $this->htmlId = $htmlId;
        return $this;
    }

    public function getHtmlId(): int
    {
        return $this->htmlId;
    }
}