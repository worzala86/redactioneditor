<?php

namespace App\Modules\Others\Models;

use App\Model;

class HtmlParamsModel extends Model
{
    public $setDefaultFields = false;

    private $id;
    private $htmlId;
    private $name;
    private $value;

    public function setValue(string $value)
    {
        $this->set('value', $value);
        $this->value = $value;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setName(string $name)
    {
        $this->set('name', $name);
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setHtmlId(int $htmlId)
    {
        $this->set('html_id', $htmlId);
        $this->htmlId = $htmlId;
        return $this;
    }

    public function getHtmlId(): int
    {
        return $this->htmlId;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}