<?php

namespace App\Modules\Others\Models;

use App\Model;
use App\Types\ShortUUID;

class HtmlClasses extends Model
{
    public $setDefaultFields = false;

    private $id;
    private $name;

    public function setName(string $name)
    {
        $this->set('name', $name);
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}