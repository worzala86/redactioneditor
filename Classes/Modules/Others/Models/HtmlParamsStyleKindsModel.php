<?php

namespace App\Modules\Others\Models;

use App\Model;

class HtmlParamsStyleKindsModel extends Model
{
    public $setDefaultFields = false;

    private $id;
    private $kind;

    public function setKind(string $kind)
    {
        $this->set('kind', $kind);
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): string
    {
        return $this->kind;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}