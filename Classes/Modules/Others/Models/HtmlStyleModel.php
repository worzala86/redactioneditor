<?php

namespace App\Modules\Others\Models;

use App\Model;

class HtmlStyleModel extends Model
{
    public $setDefaultFields = false;

    private $htmlId;
    private $kind;
    private $value;

    public function setValue(string $value)
    {
        $this->set('value', $value);
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setKind(string $kind)
    {
        $this->set('kind', $kind);
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): string
    {
        return $this->kind;
    }

    public function setHtmlId(int $htmlId)
    {
        $this->set('html_id', $htmlId);
        $this->htmlId = $htmlId;
        return $this;
    }

    public function getHtmlId(): int
    {
        return $this->htmlId;
    }
}