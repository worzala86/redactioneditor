<?php

namespace App\Modules\Others\Responses;

use App\Response;

class UpgradeResponse extends Response
{
    private $success;
    private $version;

    /**
     * @param bool $success
     * @description Pole zawiera informację czy pomyślnie wykonano upgrade bazy do nowej wersji
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param string $version
     * @description Wersja bazy danych po aktualizacji
     * @return $this
     */
    public function setVersion(string $version)
    {
        $this->version = $version;
        return $this;
    }

    public function getVersion(): string
    {
        return $this->version;
    }
}