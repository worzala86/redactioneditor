<?php

namespace App\Modules\Others\Responses;

use App\Response;

class UpdateInfoResponse extends Response
{
    private $version;
    private $newVersion;

    /**
     * @param string $version
     * @description Wersja bazy danych po aktualizacji
     * @return $this
     */
    public function setVersion(string $version)
    {
        $this->version = $version;
        return $this;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @param string $newVersion
     * @description Wersja bazy danych do której można zaktualizować stronę
     * @return $this
     */
    public function setNewVersion(string $newVersion)
    {
        $this->newVersion = $newVersion;
        return $this;
    }

    public function getNewVersion(): string
    {
        return $this->newVersion;
    }
}