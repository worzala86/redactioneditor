<?php

namespace App\Modules\Files\Models;

use App\Model;
use App\Types\UUID;

class FilesModel extends Model
{
    private $id;
    private $name;
    private $uuid;
    private $size;
    private $userId;
    private $thumbId;

    public function setThumbId(int $thumbId = null)
    {
        $this->set('thumb_id', $thumbId);
        $this->thumbId = $thumbId;
        return $this;
    }

    public function getThumbId(): ?int
    {
        return $this->thumbId;
    }

    public function setUserId(int $userId)
    {
        $this->set('user_id', $userId);
        $this->userId = $userId;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setSize(int $size)
    {
        $this->set('size', $size);
        $this->size = $size;
        return $this;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setName(string $name)
    {
        $this->set('name', $name);
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}