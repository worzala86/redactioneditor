<?php

namespace App\Modules\Files\Requests;

use App\Containers\FilesContainer;
use App\Types\FileTarget;
use App\UserRequest;

class UploadFilesRequest extends UserRequest
{
    private $files;
    private $target;

    /**
     * @param FilesContainer $files
     * @description Lista plików do uploadu
     * @return $this
     */
    public function setFiles(FilesContainer $files)
    {
        $this->files = $files;
        return $this;
    }

    public function getFiles(): FilesContainer
    {
        return $this->files;
    }

    /**
     * @param FileTarget $target
     * @description Rodzaj uploadowanej treści. Miejsce docelowe do uploadu.
     * @return $this
     */
    public function setTarget(FileTarget $target)
    {
        $this->target = $target;
        return $this;
    }

    public function getTarget(): FileTarget
    {
        return $this->target;
    }
}