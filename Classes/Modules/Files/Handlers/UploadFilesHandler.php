<?php

namespace App\Modules\Files\Handlers;

use App\Containers\ImagesContainer;
use App\File;
use App\Handler;
use App\Modules\Files\Requests\UploadFilesRequest;
use App\Modules\Files\Responses\UploadFilesResponse;

/**
 * Class UploadFilesHandler
 * @package App\Modules\Files\Handlers
 * @description Metoda służy do dodawania pliku
 */
class UploadFilesHandler extends Handler
{
    public function __invoke(UploadFilesRequest $request): UploadFilesResponse
    {
        $files = $request->getFiles();
        $filesResponse = new ImagesContainer;
        foreach ($files as $file) {
            $fileResponse = (new File)->save($file->getName(), base64_decode($file->getFile()));
            $filesResponse->add($fileResponse);
        }

        return (new UploadFilesResponse)
            ->setFiles($filesResponse);
    }
}