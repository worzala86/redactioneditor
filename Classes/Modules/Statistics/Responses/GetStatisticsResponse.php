<?php

namespace App\Modules\Statistics\Responses;

use App\Response;

class GetStatisticsResponse extends Response
{
    private $newPages;
    private $newPagesMonth;

    /**
     * @param array $newPages
     * @description Wartość przechowuje tablice z datą i ilością nowych stron w danym dniu
     * @return $this
     */
    public function setNewPages(array $newPages){
        $this->newPages = $newPages;
        return $this;
    }

    public function getNewPages(): array {
        return $this->newPages;
    }

    /**
     * @param array $newPagesMonth
     * @description Wartość przechowuje tablice z datą i ilością nowych stron w danym miesiącu
     * @return $this
     */
    public function setNewPagesMonth(array $newPagesMonth){
        $this->newPagesMonth = $newPagesMonth;
        return $this;
    }

    public function getNewPagesMonth(): array {
        return $this->newPagesMonth;
    }
}