<?php

namespace App;

use App\Modules\Statistics\Handlers;
use App\Types\UUID;

return [
    ['method'=>'get', 'url'=>'api/statistics', 'handler'=>Handlers\GetStatisticsHandler::class],
];