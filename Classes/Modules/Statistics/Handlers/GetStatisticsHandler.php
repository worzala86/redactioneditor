<?php

namespace App\Modules\Statistics\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Statistics\Requests\GetStatisticsRequest;
use App\Modules\Statistics\Responses\GetStatisticsResponse;

/**
 * Class GetStatisticsHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania statystyk
 */
class GetStatisticsHandler extends Handler
{
    public function __invoke(GetStatisticsRequest $request): GetStatisticsResponse
    {
        $newPages = [];
        for($pastDays=59;$pastDays>=0;$pastDays--) {
            $date = mktime(0, 0, 0, date("m"), date("d") - $pastDays, date("Y"));
            $date = date('Y-m-d', $date);
            $count = DB::get()->getOne('select coalesce((select `count` from pages_day_count_view where `date`=?), 0) as x', [$date]);
            $newPages[$date] = $count;
        }

        $newPagesMonth = [];
        for($pastMonths=11;$pastMonths>=0;$pastMonths--) {
            $date = mktime(0, 0, 0, date("m")-$pastMonths, date("d"), date("Y"));
            $date = date('Y-m', $date);
            $count = DB::get()->getOne('select coalesce((select `count` from pages_month_count_view where `date`=?), 0) as x', [$date]);
            $newPagesMonth[$date] = $count;
        }

        return (new GetStatisticsResponse)
            ->setNewPages($newPages)
            ->setNewPagesMonth($newPagesMonth);
    }
}