<?php

namespace App\Modules\Tags\Requests;

use App\AdminRequest;

class DeleteTagRequest extends AdminRequest
{
    private $id;

    /**
     * @param int $id
     * @description Identyfikator usuwanego tagu
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}