<?php

namespace App\Modules\Tags\Requests;

use App\AdminRequest;

class CreateTagRequest extends AdminRequest
{
    private $name;
    private $caption;

    /**
     * @param string $name
     * @description Nazwa nowego tagu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $caption
     * @description Nazwa wyświetlana tagu
     * @return $this
     */
    public function setCaption(string $caption)
    {
        $this->caption = $caption;
        return $this;
    }

    public function getCaption(): string
    {
        return $this->caption;
    }
}