<?php

namespace App\Modules\Tags\Requests;

use App\AdminRequest;

class UpdateTagRequest extends AdminRequest
{
    private $name;
    private $id;
    private $caption;

    /**
     * @param int $id
     * @description Identyfikator edytowanego tagu
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nowa nazwa tagu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $caption
     * @description Nazwa wyświetlana tagu
     * @return $this
     */
    public function setCaption(string $caption)
    {
        $this->caption = $caption;
        return $this;
    }

    public function getCaption(): string
    {
        return $this->caption;
    }
}