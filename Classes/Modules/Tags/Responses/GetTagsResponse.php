<?php

namespace App\Modules\Tags\Responses;

use App\Containers\TagsContainer;
use App\Response;

class GetTagsResponse extends Response
{
    private $tags;

    /**
     * @param TagsContainer $tags
     * @description Lista tagów
     * @return $this
     */
    public function setTags(TagsContainer $tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getTags(): TagsContainer
    {
        return $this->tags;
    }
}