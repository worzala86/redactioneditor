<?php

namespace App\Modules\Tags\Responses;

use App\Containers\TagsContainer;
use App\Response;

class CreateTagResponse extends Response
{
    private $id;

    /**
     * @param int $id
     * @description Identyfikator nowego tagu
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}