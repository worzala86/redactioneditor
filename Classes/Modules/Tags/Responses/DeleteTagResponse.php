<?php

namespace App\Modules\Tags\Responses;

use App\Containers\TagsContainer;
use App\Response;

class DeleteTagResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Informacja o powodzeniu usuniecia tagu
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }
}