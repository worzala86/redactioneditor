<?php

namespace App\Modules\Tags\Handlers;

use App\Handler;
use App\Modules\Tags\Models\TagsModel;
use App\Modules\Tags\Requests\CreateTagRequest;
use App\Modules\Tags\Responses\CreateTagResponse;

/**
 * Class CreateTagHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do dodania nowego tagu
 */
class CreateTagHandler extends Handler
{
    public function __invoke(CreateTagRequest $request): CreateTagResponse
    {
        $id = (new TagsModel)
            ->setName($request->getName())
            ->setCaption($request->getCaption())
            ->insert();

        return (new CreateTagResponse)
            ->setId($id);
    }
}