<?php

namespace App\Modules\Tags\Handlers;

use App\Handler;
use App\Modules\Tags\Models\TagsModel;
use App\Modules\Tags\Requests\UpdateTagRequest;
use App\Modules\Tags\Responses\UpdateTagResponse;

/**
 * Class UpdateTagHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do updateowania tagu
 */
class UpdateTagHandler extends Handler
{
    public function __invoke(UpdateTagRequest $request): UpdateTagResponse
    {
        $updated = (new TagsModel)
            ->setName($request->getName())
            ->setCaption($request->getCaption())
            ->update($request->getId());

        return (new UpdateTagResponse)
            ->setSuccess($updated);
    }
}