<?php

namespace App\Modules\Tags\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Tags\Requests\DeleteTagRequest;
use App\Modules\Tags\Responses\DeleteTagResponse;

/**
 * Class DeleteTagHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do usuwania tagu
 */
class DeleteTagHandler extends Handler
{
    public function __invoke(DeleteTagRequest $request): DeleteTagResponse
    {
        $deleted = DB::get()->execute('delete from tags where id=?', $request->getId());

        return (new DeleteTagResponse)
            ->setSuccess($deleted);
    }
}