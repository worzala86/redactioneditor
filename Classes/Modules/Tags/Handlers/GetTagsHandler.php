<?php

namespace App\Modules\Tags\Handlers;

use App\Containers\TagContainer;
use App\Containers\TagsContainer;
use App\Database\DB;
use App\Handler;
use App\Modules\Tags\Collections\TagsCollection;
use App\Modules\Tags\Requests\GetTagsRequest;
use App\Modules\Tags\Responses\GetTagsResponse;

/**
 * Class GetTagsHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do pobrania listy tagów
 */
class GetTagsHandler extends Handler
{
    public function __invoke(GetTagsRequest $request): GetTagsResponse
    {
        $tagsCollection = (new TagsCollection)
            ->loadAll();

        $tags = new TagsContainer;
        foreach ($tagsCollection as $tagsModel) {
            $count = DB::get()->getOne('select sum(`count`) as `count` from (
                    select count(*) as `count` from tags_elements where tag_id=:tag_id
                    union
                    select count(*) as `count` from tags_images where tag_id=:tag_id
                    union
                    select count(*) as `count` from tags_projects where tag_id=:tag_id
                ) as x', ['tag_id'=>$tagsModel->getId()]);
            $tags->add(
                (new TagContainer)
                    ->setId($tagsModel->getId())
                    ->setName($tagsModel->getName())
                    ->setCount($count)
                    ->setCaption($tagsModel->getCaption())
            );
        }

        return (new GetTagsResponse)
            ->setTags($tags);
    }
}