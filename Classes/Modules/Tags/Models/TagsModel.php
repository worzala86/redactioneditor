<?php

namespace App\Modules\Tags\Models;

use App\Model;

class TagsModel extends Model
{
    public $setDefaultFields = false;

    private $id;
    private $name;
    private $caption;

    public function setCaption(string $caption)
    {
        $this->set('caption', $caption);
        $this->caption = $caption;
        return $this;
    }

    public function getCaption(): string
    {
        return $this->caption;
    }

    public function setName(string $name)
    {
        $this->set('name', $name);
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}