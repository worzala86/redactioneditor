<?php

namespace App;

use App\Modules\Tags\Handlers;

return [
    ['method'=>'get', 'url'=>'api/tags', 'handler'=>Handlers\GetTagsHandler::class],
    ['method'=>'post', 'url'=>'api/tags', 'handler'=>Handlers\CreateTagHandler::class],
    ['method'=>'put', 'url'=>'api/tags/:id', 'handler'=>Handlers\UpdateTagHandler::class, 'regex'=>['id'=>'([0-9]+)']],
    ['method'=>'delete', 'url'=>'api/tags/:id', 'handler'=>Handlers\DeleteTagHandler::class, 'regex'=>['id'=>'([0-9]+)']],
];