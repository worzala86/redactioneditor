<?php

namespace App\Modules\Tags\Collections;

use App\CollectionTrait;
use App\Modules\Tags\Models\TagsModel;

class TagsCollection extends TagsModel implements \Iterator
{
    use CollectionTrait;
}