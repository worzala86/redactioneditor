<?php

namespace App\Modules\AdminDashboard\Handlers;

use App\Database\DB;
use App\Database\Upgrade;
use App\Handler;
use App\Modules\AdminDashboard\Requests\GetDashboardRequest;
use App\Modules\AdminDashboard\Responses\GetDashboardResponse;

/**
 * Class GetDashboardHandler
 * @package App\Modules\Files\Handlers
 * @description Metoda służy do pobrania informacji startowych
 */
class GetDashboardHandler extends Handler
{
    public function __invoke(GetDashboardRequest $request): GetDashboardResponse
    {
        $pagesCount = DB::get()->getOne('select count(*) from pages where user_id=?', $request->getCurrentUserId());

        $newPages = [];
        for($pastDays=13;$pastDays>=0;$pastDays--) {
            $date = mktime(0, 0, 0, date("m"), date("d") - $pastDays, date("Y"));
            $date = date('Y-m-d', $date);
            $count = DB::get()->getOne('select coalesce((select `count` from pages_day_count_view where `date`=?), 0) as x', [$date]);
            $newPages[$date] = $count;
        }

        $htmlStyleKindsPopularity = DB::get()->getAll('select * from 
            (SELECT distinct `kind` as name, count(*) as `count` from html_style group by kind) as x 
            order by `count` desc');
        foreach ($htmlStyleKindsPopularity as $key => $value) {
            $values = DB::get()->getAll('select * from 
            (SELECT distinct `value` as name, count(*) as `count` from html_style where kind=? group by value) as x 
            order by `count` desc limit 10', $value['name']);
            $htmlStyleKindsPopularity[$key]['values'] = $values;
        }

        $fileSystemSize = DB::get()->getOne('select coalesce(sum(`size`),0) as `size` from files');

        $databaseSize = DB::get()->getOne(/*select table_name, */
            'SELECT sum(data_length + index_length) as size 
            FROM information_schema.TABLES WHERE table_schema = ? ORDER BY size DESC',
            DATABASE_CONNECTION[DEFAULT_DATABASE_CONNECTION]['DATABASE']);

        $databaseVersion = (new Upgrade)->version();

        return (new GetDashboardResponse)
            ->setPagesCount($pagesCount)
            ->setNewPages($newPages)
            ->setHtmlStyleKindsPopularity($htmlStyleKindsPopularity)
            ->setFileSystemSize($fileSystemSize)
            ->setDatabaseSize($databaseSize)
            ->setDatabaseVersion($databaseVersion);
    }
}