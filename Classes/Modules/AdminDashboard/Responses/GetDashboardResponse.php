<?php

namespace App\Modules\AdminDashboard\Responses;

use App\Response;

class GetDashboardResponse extends Response
{
    private $pagesCount;
    private $newPages;
    private $htmlStyleKindsPopularity;
    private $fileSystemSize;
    private $databaseSize;
    private $databaseVersion;

    /**
     * @param int $pagesCount
     * @description Ilość projektów aktywnego użytkownika
     * @return $this
     */
    public function setPagesCount(int $pagesCount)
    {
        $this->pagesCount = $pagesCount;
        return $this;
    }

    public function getPagesCount(): int
    {
        return $this->pagesCount;
    }

    /**
     * @param array $newPages
     * @description Lista dodanych stron w ciągu ostatnich 7 dni
     * @return $this
     */
    public function setNewPages(array $newPages)
    {
        $this->newPages = $newPages;
        return $this;
    }

    public function getNewPages(): array
    {
        return $this->newPages;
    }

    /**
     * @param array $htmlStyleKindsPopularity
     * @description Popularność styli
     * @return $this
     */
    public function setHtmlStyleKindsPopularity(array $htmlStyleKindsPopularity)
    {
        $this->htmlStyleKindsPopularity = $htmlStyleKindsPopularity;
        return $this;
    }

    public function getHtmlStyleKindsPopularity(): array
    {
        return $this->htmlStyleKindsPopularity;
    }

    /**
     * @param int $fileSystemSize
     * @description Ilość projektów aktywnego użytkownika
     * @return $this
     */
    public function setFileSystemSize(int $fileSystemSize)
    {
        $this->fileSystemSize = $fileSystemSize;
        return $this;
    }

    public function getFileSystemSize(): int
    {
        return $this->fileSystemSize;
    }

    /**
     * @param float $databaseSize
     * @description Rozmiar bazy danych
     * @return $this
     */
    public function setDatabaseSize(float $databaseSize)
    {
        $this->databaseSize = $databaseSize;
        return $this;
    }

    public function getDatabaseSize(): float
    {
        return $this->databaseSize;
    }

    /**
     * @param string $databaseVersion
     * @description Wersja bazy danych
     * @return $this
     */
    public function setDatabaseVersion(string $databaseVersion)
    {
        $this->databaseVersion = $databaseVersion;
        return $this;
    }

    public function getDatabaseVersion(): string
    {
        return $this->databaseVersion;
    }
}