<?php

namespace App;

use App\Modules\AdminDashboard\Handlers;

return [
    ['method'=>'get', 'url'=>'api/admin/dashboard', 'handler'=>Handlers\GetDashboardHandler::class],
];