<?php

namespace App\Modules\Pixabay\Handlers;

use App\Containers\PixabayImageContainer;
use App\Containers\PixabayImagesContainer;
use App\Curl;
use App\Database\DB;
use App\Handler;
use App\Modules\Pixabay\Requests\SearchImagesRequest;
use App\Modules\Pixabay\Responses\SearchImagesResponse;
use App\Types\PixabayID;

/**
 * Class SearchImagesHandler
 * @package App\Modules\Pixabay\Handlers
 * @description Metoda służy do wyszukiwania zdjęć
 */
class SearchImagesHandler extends Handler
{
    public function __invoke(SearchImagesRequest $request): SearchImagesResponse
    {
        $images = new PixabayImagesContainer;

        $key = urlencode($request->getKey());
        $page = $request->getPage()?$request->getPage():1;
        $curl = new Curl;
        $result = $curl->get('https://pixabay.com/api/?key=2026695-a56302a590de6bab598235ae8&q='.$key.'&page='.$page);
        $result = json_decode($result);

        if($result->hits) {
            foreach ($result->hits as $hit) {
                if (!DB::get()->getOne('select 1 from pixabay where pixa_id=?', [$hit->id])) {
                    DB::get()->execute('insert into pixabay set pixa_id=?, image=?, thumb=?, link=?',
                        [$hit->id, $hit->largeImageURL, $hit->webformatURL, $hit->pageURL]);
                }else{
                    DB::get()->execute('update pixabay set image=?, thumb=?, link=? where pixa_id=?',
                        [$hit->largeImageURL, $hit->webformatURL, $hit->pageURL, $hit->id]);
                }
                $images->add(
                    (new PixabayImageContainer)
                        ->setId(new PixabayID($hit->id))
                        ->setUrl($hit->webformatURL)
                );
            }
        }

        return (new SearchImagesResponse)
            ->setImages($images);
    }
}