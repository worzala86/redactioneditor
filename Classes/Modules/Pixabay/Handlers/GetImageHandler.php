<?php

namespace App\Modules\Pixabay\Handlers;

use App\Database\DB;
use App\File;
use App\Handler;
use App\Modules\Images\Models\ImagesModel;
use App\Modules\Pixabay\Requests\GetImageRequest;
use App\Modules\Pixabay\Responses\GetImageResponse;
use App\Types\FileTarget;
use App\Types\UUID;

/**
 * Class GetImageHandler
 * @package App\Modules\Pixabay\Handlers
 * @description Metoda służy do pobrania obrazka
 */
class GetImageHandler extends Handler
{
    public function __invoke(GetImageRequest $request): GetImageResponse
    {
        $id = $request->getId();
        $pixabay = DB::get()->getRow('select image, id from pixabay where pixa_id=?', [$id]);

        $file = new File;
        $file->save(str_replace('https://pixabay.com/get/', '', $pixabay['image']), file_get_contents($pixabay['image']));

        $uuid = UUID::fake();
        (new ImagesModel)
            ->setUuid($uuid)
            ->setFileId($file->getId())
            ->setUserId($request->getCurrentUserId())
            ->setPixabayId($pixabay['id'])
            ->insert();

        return (new GetImageResponse)
            ->setId($uuid)
            ->setName($file->getName());
    }
}