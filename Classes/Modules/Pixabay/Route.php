<?php

namespace App;

use App\Modules\Pixabay\Handlers;
use App\Types\PixabayID;

return [
    ['method'=>'post', 'url'=>'api/images/search', 'handler'=>Handlers\SearchImagesHandler::class],
    ['method'=>'get', 'url'=>'api/images/:id/download', 'handler'=>Handlers\GetImageHandler::class, 'regex'=>['id'=>PixabayID::$pattern]],
];