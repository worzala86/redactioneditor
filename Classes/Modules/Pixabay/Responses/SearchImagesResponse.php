<?php

namespace App\Modules\Pixabay\Responses;

use App\Containers\ImagesContainer;
use App\Containers\PixabayImagesContainer;
use App\Response;

class SearchImagesResponse extends Response
{
    private $images;

    /**
     * @param ImagesContainer $images
     * @description Lista wyszukiwanych zdjęć
     * @return $this
     */
    public function setImages(PixabayImagesContainer $images)
    {
        $this->images = $images;
        return $this;
    }

    public function getImages(): PixabayImagesContainer
    {
        return $this->images;
    }
}