<?php

namespace App\Modules\Pixabay\Responses;

use App\Response;
use App\Types\UUID;

class GetImageResponse extends Response
{
    private $id;
    private $name;

    /**
     * @param UUID $id
     * @description Identyfikator zdjęcia
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?UUID
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa zdjęcia
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
}