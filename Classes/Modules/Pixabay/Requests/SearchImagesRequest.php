<?php

namespace App\Modules\Pixabay\Requests;

use App\UserRequest;

class SearchImagesRequest extends UserRequest
{
    private $key;
    private $page;

    /**
     * @param string $key
     * @description Wyszukiwana fraza
     * @return $this
     */
    public function setKey(string $key)
    {
        $this->key = $key;
        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param int $page
     * @description Strona z wynikami
     * @return $this
     */
    public function setPage(int $page)
    {
        $this->page = $page;
        return $this;
    }

    public function getPage(): ?int
    {
        return $this->page;
    }
}