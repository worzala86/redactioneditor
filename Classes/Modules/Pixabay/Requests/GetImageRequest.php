<?php

namespace App\Modules\Pixabay\Requests;

use App\Types\PixabayID;
use App\UserRequest;

class GetImageRequest extends UserRequest
{
    private $id;

    /**
     * @param int $id
     * @description Numer zdjęcia
     * @return $this
     */
    public function setId(PixabayID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): PixabayID
    {
        return $this->id;
    }
}