<?php

namespace App\Modules\Pages\Requests;

use App\Types\UUID;
use App\UserRequest;

class UpdatePagePaginationRequest extends UserRequest
{
    private $id;
    private $projectId;
    private $paginationUrl;
    private $paginationUrlParam;
    private $paginationLimit;

    /**
     * @param UUID $id
     * @description Identyfikator strony
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param UUID $projectId
     * @description Identyfikator projektu
     * @return $this
     */
    public function setProjectId(UUID $projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): UUID
    {
        return $this->projectId;
    }

    /**
     * @param string $paginationUrl
     * @description Adres url poprzedzający numer podstrony
     * @return $this
     */
    public function setPaginationUrl(string $paginationUrl)
    {
        $this->paginationUrl = $paginationUrl;
        return $this;
    }

    public function getPaginationUrl(): string
    {
        return $this->paginationUrl;
    }

    /**
     * @param int $paginationUrlParam
     * @description Numer fragmentu url do paginacji
     * @return $this
     */
    public function setPaginationUrlParam(int $paginationUrlParam)
    {
        $this->paginationUrlParam = $paginationUrlParam;
        return $this;
    }

    public function getPaginationUrlParam(): int
    {
        return $this->paginationUrlParam;
    }

    /**
     * @param int $paginationLimit
     * @description Limit elementów na stronę
     * @return $this
     */
    public function setPaginationLimit(int $paginationLimit)
    {
        $this->paginationLimit = $paginationLimit;
        return $this;
    }

    public function getPaginationLimit(): int
    {
        return $this->paginationLimit;
    }
}