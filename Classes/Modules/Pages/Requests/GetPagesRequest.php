<?php

namespace App\Modules\Pages\Requests;

use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;
use App\Types\UUID;
use App\UserRequest;

class GetPagesRequest extends UserRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;

    private $projectId;

    /**
     * @param UUID $projectId
     * @description Identyfikator projektu
     * @return $this
     */
    public function setProjectId(UUID $projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): UUID
    {
        return $this->projectId;
    }
}