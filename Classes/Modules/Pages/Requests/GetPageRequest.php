<?php

namespace App\Modules\Pages\Requests;

use App\Types\Url;
use App\Types\UUID;
use App\Request;

class GetPageRequest extends Request
{
    private $id;
    private $projectId;
    private $url;

    /**
     * @param UUID $id
     * @description Identyfikator strony
     * @return $this
     */
    public function setId(UUID $id = null)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?UUID
    {
        return $this->id;
    }

    /**
     * @param UUID $projectId
     * @description Identyfikator projektu
     * @return $this
     */
    public function setProjectId(UUID $projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): UUID
    {
        return $this->projectId;
    }

    /**
     * @param Url $url
     * @description Url strony do załadowania
     * @return $this
     */
    public function setUrl(Url $url = null)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl(): ?Url
    {
        return $this->url;
    }
}