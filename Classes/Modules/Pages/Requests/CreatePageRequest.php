<?php

namespace App\Modules\Pages\Requests;

use App\Types\Url;
use App\Types\UUID;
use App\UserRequest;

class CreatePageRequest extends UserRequest
{
    private $projectId;
    private $url;

    /**
     * @param UUID $projectId
     * @description Identyfikator projektu
     * @return $this
     */
    public function setProjectId(UUID $projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): UUID
    {
        return $this->projectId;
    }

    /**
     * @param Url $url
     * @description Nowy url strony do zapisania
     * @return $this
     */
    public function setUrl(Url $url)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl(): Url
    {
        return $this->url;
    }
}