<?php

namespace App\Modules\Pages\Requests;

use App\AdminRequest;
use App\Containers\HtmlElementContainer;
use App\Types\UUID;

class AdminUpdatePageRequest extends AdminRequest
{
    private $id;
    private $projectId;
    private $content;
    private $header;
    private $footer;
    private $sidebarLeft;
    private $sidebarRight;
    private $config;
    private $showLeftSidebar;
    private $showRightSidebar;
    private $styleId;

    /**
     * @param UUID $id
     * @description Identyfikator strony
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param UUID $projectId
     * @description Identyfikator projektu
     * @return $this
     */
    public function setProjectId(UUID $projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): UUID
    {
        return $this->projectId;
    }

    /**
     * @param HtmlElementContainer $content
     * @description Treść strony do zapisania
     * @return $this
     */
    public function setContent(HtmlElementContainer $content)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent(): HtmlElementContainer
    {
        return $this->content;
    }
    
    /**
     * @param array $header
     * @description Treść nagłówka strony
     * @return $this
     */
    public function setHeader(HtmlElementContainer $header)
    {
        $this->header = $header;
        return $this;
    }

    public function getHeader(): HtmlElementContainer
    {
        return $this->header;
    }
    
    /**
     * @param array $footer
     * @description Treść stopki strony
     * @return $this
     */
    public function setFooter(HtmlElementContainer $footer)
    {
        $this->footer = $footer;
        return $this;
    }

    public function getFooter(): HtmlElementContainer
    {
        return $this->footer;
    }
    
    /**
     * @param array $sidebarLeft
     * @description Treść lewego paska bocznego strony
     * @return $this
     */
    public function setSidebarLeft(HtmlElementContainer $sidebarLeft)
    {
        $this->sidebarLeft = $sidebarLeft;
        return $this;
    }

    public function getSidebarLeft(): HtmlElementContainer
    {
        return $this->sidebarLeft;
    }
    
    /**
     * @param array $sidebarRight
     * @description Treść lewego paska bocznego strony
     * @return $this
     */
    public function setSidebarRight(HtmlElementContainer $sidebarRight)
    {
        $this->sidebarRight = $sidebarRight;
        return $this;
    }

    public function getSidebarRight(): HtmlElementContainer
    {
        return $this->sidebarRight;
    }

    /**
     * @param string $config
     * @description W tej wartości trzeba podać konfig projektu
     * @return $this
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
        return $this;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param bool $showLeftSidebar
     * @description W tej wartości jest przechowywana informacja o widocznośći lewego paska bocznego
     * @return $this
     */
    public function setShowLeftSidebar(bool $showLeftSidebar = null)
    {
        $this->showLeftSidebar = $showLeftSidebar;
        return $this;
    }

    public function getShowLeftSidebar(): ?bool
    {
        return $this->showLeftSidebar;
    }

    /**
     * @param bool $showRightSidebar
     * @description W tej wartości jest przechowywana informacja o widocznośći prawego paska bocznego
     * @return $this
     */
    public function setShowRightSidebar(bool $showRightSidebar = null)
    {
        $this->showRightSidebar = $showRightSidebar;
        return $this;
    }

    public function getShowRightSidebar(): ?bool
    {
        return $this->showRightSidebar;
    }

    /**
     * @param UUID $styleId
     * @description W tej wartości jest przechowywany identyfikator stylu
     * @return $this
     */
    public function setStyleId(UUID $styleId)
    {
        $this->styleId = $styleId;
        return $this;
    }

    public function getStyleId(): UUID
    {
        return $this->styleId;
    }
}