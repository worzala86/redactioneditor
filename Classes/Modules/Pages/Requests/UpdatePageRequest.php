<?php

namespace App\Modules\Pages\Requests;

use App\Containers\FontContainer;
use App\Containers\HtmlElementContainer;
use App\Containers\PageElementsContainer;
use App\Types\UUID;
use App\UserRequest;

class UpdatePageRequest extends UserRequest
{
    private $id;
    private $projectId;
    private $header;
    private $footer;
    private $sidebarLeft;
    private $sidebarRight;
    private $config;
    private $showLeftSidebar;
    private $showRightSidebar;
    private $styleId;
    private $body;

    /**
     * @param UUID $id
     * @description Identyfikator strony
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param UUID $projectId
     * @description Identyfikator projektu
     * @return $this
     */
    public function setProjectId(UUID $projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): UUID
    {
        return $this->projectId;
    }
    
    /**
     * @param array $header
     * @description Treść nagłówka strony
     * @return $this
     */
    public function setHeader(PageElementsContainer $header)
    {
        $this->header = $header;
        return $this;
    }

    public function getHeader(): PageElementsContainer
    {
        return $this->header;
    }
    
    /**
     * @param array $footer
     * @description Treść stopki strony
     * @return $this
     */
    public function setFooter(PageElementsContainer $footer)
    {
        $this->footer = $footer;
        return $this;
    }

    public function getFooter(): PageElementsContainer
    {
        return $this->footer;
    }
    
    /**
     * @param array $sidebarLeft
     * @description Treść lewego paska bocznego strony
     * @return $this
     */
    public function setSidebarLeft(PageElementsContainer $sidebarLeft)
    {
        $this->sidebarLeft = $sidebarLeft;
        return $this;
    }

    public function getSidebarLeft(): PageElementsContainer
    {
        return $this->sidebarLeft;
    }
    
    /**
     * @param array $sidebarRight
     * @description Treść lewego paska bocznego strony
     * @return $this
     */
    public function setSidebarRight(PageElementsContainer $sidebarRight)
    {
        $this->sidebarRight = $sidebarRight;
        return $this;
    }

    public function getSidebarRight(): PageElementsContainer
    {
        return $this->sidebarRight;
    }

    /**
     * @param string $config
     * @description W tej wartości trzeba podać konfig projektu
     * @return $this
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
        return $this;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param bool $showLeftSidebar
     * @description W tej wartości jest przechowywana informacja o widocznośći lewego paska bocznego
     * @return $this
     */
    public function setShowLeftSidebar(bool $showLeftSidebar = null)
    {
        $this->showLeftSidebar = $showLeftSidebar;
        return $this;
    }

    public function getShowLeftSidebar(): ?bool
    {
        return $this->showLeftSidebar;
    }

    /**
     * @param bool $showRightSidebar
     * @description W tej wartości jest przechowywana informacja o widocznośći prawego paska bocznego
     * @return $this
     */
    public function setShowRightSidebar(bool $showRightSidebar = null)
    {
        $this->showRightSidebar = $showRightSidebar;
        return $this;
    }

    public function getShowRightSidebar(): ?bool
    {
        return $this->showRightSidebar;
    }

    /**
     * @param UUID $styleId
     * @description W tej wartości jest przechowywany identyfikator stylu
     * @return $this
     */
    public function setStyleId(UUID $styleId)
    {
        $this->styleId = $styleId;
        return $this;
    }

    public function getStyleId(): UUID
    {
        return $this->styleId;
    }

    /**
     * @param PageElementsContainer $body
     * @description Tablica z identyfikatorami elementów z których składa się treść
     * @return $this
     */
    public function setBody(PageElementsContainer $body)
    {
        $this->body = $body;
        return $this;
    }

    public function getBody(): PageElementsContainer
    {
        return $this->body;
    }

    /**
     * @param array $dataSet
     * @description Tablica z treściami - dataset
     * @return $this
     */
    public function setDataSet(array $dataSet)
    {
        $this->dataSet = $dataSet;
        return $this;
    }

    public function getDataSet(): array
    {
        return $this->dataSet;
    }
}