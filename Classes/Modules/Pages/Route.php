<?php

namespace App;

use App\Modules\Pages\Handlers;
use App\Types\UUID;

return [
    ['method'=>'post', 'url'=>'api/projects/:projectId/pages/:id/pagination', 'handler'=>Handlers\UpdatePagePaginationHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
    ['method'=>'get', 'url'=>'api/projects/:projectId/pages', 'handler'=>Handlers\GetPagesHandler::class, 'regex'=>['projectId'=>UUID::$pattern]],
    ['method'=>'put', 'url'=>'api/projects/:projectId/pages/:id/url', 'handler'=>Handlers\UpdatePageUrlHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/projects/:projectId/pages', 'handler'=>Handlers\CreatePageHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
    ['method'=>'delete', 'url'=>'api/projects/:projectId/pages/:id', 'handler'=>Handlers\DeletePageHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
    ['method'=>'get', 'url'=>'api/projects/:projectId/pages/:id', 'handler'=>Handlers\GetPageHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
    ['method'=>'get', 'url'=>'api/projects/:projectId/pages/default', 'handler'=>Handlers\GetPageHandler::class, 'regex'=>['projectId'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/projects/:projectId/pages/url', 'handler'=>Handlers\GetPageHandler::class, 'regex'=>['projectId'=>UUID::$pattern]],
    ['method'=>'put', 'url'=>'api/admin/projects/:projectId/pages/:id', 'handler'=>Handlers\AdminUpdatePageHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
    ['method'=>'put', 'url'=>'api/projects/:projectId/pages/:id', 'handler'=>Handlers\UpdatePageHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
];