<?php

namespace App\Modules\Pages\Exceptions;

use App\NamedException;

class ElementNotFoundException extends \Exception implements NamedException
{
}