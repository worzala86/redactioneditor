<?php

namespace App\Modules\Pages\Exceptions;

use App\NamedException;

class ProjectIsNotYourException extends \Exception implements NamedException
{
}