<?php

namespace App\Modules\Pages\Exceptions;

use App\NamedException;

class ProjectConfigNotFoundException extends \Exception implements NamedException
{
}