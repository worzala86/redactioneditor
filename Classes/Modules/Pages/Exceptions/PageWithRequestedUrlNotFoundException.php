<?php

namespace App\Modules\Pages\Exceptions;

use App\NamedException;

class PageWithRequestedUrlNotFoundException extends \Exception implements NamedException
{
}