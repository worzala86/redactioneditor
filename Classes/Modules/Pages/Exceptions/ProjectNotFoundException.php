<?php

namespace App\Modules\Pages\Exceptions;

use App\NamedException;

class ProjectNotFoundException extends \Exception implements NamedException
{
}