<?php

namespace App\Modules\Pages\Exceptions;

use App\NamedException;

class StyleNotFoundException extends \Exception implements NamedException
{
}