<?php

namespace App\Modules\Pages\Exceptions;

use App\NamedException;

class PageNotFoundException extends \Exception implements NamedException
{
}