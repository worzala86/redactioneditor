<?php

namespace App\Modules\Pages\Responses;

use App\Response;

class UpdatePagePaginationResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy pomyślnie zapisano ustawienia paginacji
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}