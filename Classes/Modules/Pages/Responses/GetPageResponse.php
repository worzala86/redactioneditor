<?php

namespace App\Modules\Pages\Responses;

use App\Containers\FontContainer;
use App\Containers\HtmlElementContainer;
use App\Response;
use App\Types\Domain;
use App\Types\Url;
use App\Types\UUID;

class GetPageResponse extends Response
{
    private $id;
    private $url;
    private $header;
    private $footer;
    private $sidebarLeft;
    private $sidebarRight;
    private $config;
    private $showLeftSidebar;
    private $showRightSidebar;
    private $domain;
    private $body;
    private $paginationLimit;
    private $paginationUrl;
    private $paginationUrlParam;
    private $styleId;
    private $customStyleId;
    private $dataSet;

    /**
     * @param UUID $id
     * @description Pole zawiera identyfikator strony
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param Url $url
     * @description Adres url strony
     * @return $this
     */
    public function setUrl(Url $url)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl(): Url
    {
        return $this->url;
    }

    /**
     * @param array $header
     * @description Treść nagłówka strony
     * @return $this
     */
    public function setHeader(HtmlElementContainer $header = null)
    {
        $this->header = $header;
        return $this;
    }

    public function getHeader(): ?HtmlElementContainer
    {
        return $this->header;
    }

    /**
     * @param array $footer
     * @description Treść stopki strony
     * @return $this
     */
    public function setFooter(HtmlElementContainer $footer = null)
    {
        $this->footer = $footer;
        return $this;
    }

    public function getFooter(): ?HtmlElementContainer
    {
        return $this->footer;
    }

    /**
     * @param array $sidebarLeft
     * @description Treść lewego paska bocznego strony
     * @return $this
     */
    public function setSidebarLeft(HtmlElementContainer $sidebarLeft = null)
    {
        $this->sidebarLeft = $sidebarLeft;
        return $this;
    }

    public function getSidebarLeft(): ?HtmlElementContainer
    {
        return $this->sidebarLeft;
    }

    /**
     * @param array $sidebarRight
     * @description Treść lewego paska bocznego strony
     * @return $this
     */
    public function setSidebarRight(HtmlElementContainer $sidebarRight = null)
    {
        $this->sidebarRight = $sidebarRight;
        return $this;
    }

    public function getSidebarRight(): ?HtmlElementContainer
    {
        return $this->sidebarRight;
    }

    /**
     * @param array $config
     * @description Pole zawiera tablicę z ustawieniami projektu
     * @return $this
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
        return $this;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param bool $showLeftSidebar
     * @description Informacja o tym czy ma być wyświetlany lewy pasek
     * @return $this
     */
    public function setShowLeftSidebar(bool $showLeftSidebar)
    {
        $this->showLeftSidebar = $showLeftSidebar;
        return $this;
    }

    public function getShowLeftSidebar(): ?bool
    {
        return $this->showLeftSidebar;
    }

    /**
     * @param bool $showRightSidebar
     * @description Informacja o tym czy ma być wyświetlany prawy pasek
     * @return $this
     */
    public function setShowRightSidebar(bool $showRightSidebar)
    {
        $this->showRightSidebar = $showRightSidebar;
        return $this;
    }

    public function getShowRightSidebar(): ?bool
    {
        return $this->showRightSidebar;
    }

    /**
     * @param Domain $domain
     * @description Adres strony - domena przypisana do projektu
     * @return $this
     */
    public function setDomain(Domain $domain = null)
    {
        $this->domain = $domain;
        return $this;
    }

    public function getDomain(): ?Domain
    {
        return $this->domain;
    }

    /**
     * @param HtmlElementContainer $body
     * @description Ciało strony
     * @return $this
     */
    public function setBody(HtmlElementContainer $body = null)
    {
        $this->body = $body;
        return $this;
    }

    public function getBody(): ?HtmlElementContainer
    {
        return $this->body;
    }

    /**
     * @param int $paginationLimit
     * @description Limit paginacji np. 10
     * @return $this
     */
    public function setPaginationLimit(int $paginationLimit = null)
    {
        $this->paginationLimit = $paginationLimit;
        return $this;
    }

    public function getPaginationLimit(): ?int
    {
        return $this->paginationLimit;
    }

    /**
     * @param int $paginationUrlParam
     * @description Fragment adresu url ze stroną do wczytania np. 2 dla produkty/2
     * @return $this
     */
    public function setPaginationUrlParam(int $paginationUrlParam = null)
    {
        $this->paginationUrlParam = $paginationUrlParam;
        return $this;
    }

    public function getPaginationUrlParam(): ?int
    {
        return $this->paginationUrlParam;
    }

    /**
     * @param string $paginationUrl
     * @description Adres url strony do paginacji - poprzedzajacy numer strony
     * @return $this
     */
    public function setPaginationUrl(string $paginationUrl = null)
    {
        $this->paginationUrl = $paginationUrl;
        return $this;
    }

    public function getPaginationUrl(): ?string
    {
        return $this->paginationUrl;
    }

    /**
     * @param UUID $styleId
     * @description Identyfikator stylu
     * @return $this
     */
    public function setStyleId(UUID $styleId)
    {
        $this->styleId = $styleId;
        return $this;
    }

    public function getStyleId(): UUID
    {
        return $this->styleId;
    }

    /**
     * @param UUID $customStyleId
     * @description Identyfikator customowego stylu
     * @return $this
     */
    public function setCustomStyleId(UUID $customStyleId)
    {
        $this->customStyleId = $customStyleId;
        return $this;
    }

    public function getCustomStyleId(): UUID
    {
        return $this->customStyleId;
    }

    /**
     * @param array $dataSet
     * @description Tablic z datasetem do strony
     * @return $this
     */
    public function setDataSet(array $dataSet)
    {
        $this->dataSet = $dataSet;
        return $this;
    }

    public function getDataSet(): array
    {
        return $this->dataSet;
    }
}