<?php

namespace App\Modules\Pages\Responses;

use App\Response;
use App\Types\UUID;

class CreatePageResponse extends Response
{
    private $id;

    /**
     * @param UUID $id
     * @description Pole zawiera identyfikator nowej strony
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}