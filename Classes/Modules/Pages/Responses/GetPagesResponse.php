<?php

namespace App\Modules\Pages\Responses;

use App\Containers\PagesContainer;
use App\Response;

class GetPagesResponse extends Response
{
    private $pages;

    /**
     * @param PagesContainer $pages
     * @description Lista stron w projekcie
     * @return $this
     */
    public function setPages(PagesContainer $pages)
    {
        $this->pages = $pages;
        return $this;
    }

    public function getPages(): PagesContainer
    {
        return $this->pages;
    }
}