<?php

namespace App\Modules\Pages\Collections;

use App\CollectionTrait;
use App\Modules\Pages\Models\PagesElementsFieldsModel;
use App\Modules\Pages\Models\PagesElementsModel;
use App\Modules\Pages\Models\PagesModel;
use App\Modules\Pages\Models\ProjectsElementsFieldsModel;
use App\PaginationTrait;

class ProjectsElementsFieldsCollection extends ProjectsElementsFieldsModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}