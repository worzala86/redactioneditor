<?php

namespace App\Modules\Pages\Collections;

use App\CollectionTrait;
use App\Modules\Pages\Models\PagesDatasetsModel;
use App\Modules\Pages\Models\PagesElementsModel;
use App\Modules\Pages\Models\PagesModel;
use App\PaginationTrait;

class PagesDatasetsCollection extends PagesDatasetsModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}