<?php

namespace App\Modules\Pages\Collections;

use App\CollectionTrait;
use App\Modules\Pages\Models\PagesElementsModel;
use App\Modules\Pages\Models\PagesModel;
use App\PaginationTrait;

class PagesElementsCollection extends PagesElementsModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}