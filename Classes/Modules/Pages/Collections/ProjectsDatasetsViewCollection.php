<?php

namespace App\Modules\Pages\Collections;

use App\CollectionTrait;
use App\Modules\Pages\Models\PagesDatasetsModel;
use App\Modules\Pages\Models\PagesDatasetsViewModel;
use App\Modules\Pages\Models\PagesElementsModel;
use App\Modules\Pages\Models\PagesModel;
use App\Modules\Pages\Models\ProjectsDatasetsViewModel;
use App\PaginationTrait;

class ProjectsDatasetsViewCollection extends ProjectsDatasetsViewModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}