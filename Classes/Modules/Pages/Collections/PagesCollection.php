<?php

namespace App\Modules\Pages\Collections;

use App\CollectionTrait;
use App\Modules\Pages\Models\PagesModel;
use App\PaginationTrait;

class PagesCollection extends PagesModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}