<?php

namespace App\Modules\Pages\Collections;

use App\CollectionTrait;
use App\Modules\Pages\Models\ProjectsElementsModel;
use App\PaginationTrait;

class ProjectsElementsCollection extends ProjectsElementsModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}