<?php

namespace App\Modules\Pages\Models;

use App\Model;
use App\Types\Url;
use App\Types\UUID;

class PagesModel extends Model
{
    private $id;
    private $uuid;
    private $projectId;
    private $url;
    private $userId;
    private $paginationUrl;
    private $paginationUrlParam;
    private $paginationLimit;
    private $contentId;

    public function setContentId(int $contentId = null)
    {
        $this->set('content_id', $contentId);
        $this->contentId = $contentId;
        return $this;
    }

    public function getContentId(): ?int
    {
        return $this->contentId;
    }

    public function setPaginationLimit(int $paginationLimit = null)
    {
        $this->set('pagination_limit', $paginationLimit);
        $this->paginationLimit = $paginationLimit;
        return $this;
    }

    public function getPaginationLimit(): ?int
    {
        return $this->paginationLimit;
    }
    
    public function setPaginationUrlParam(int $paginationUrlParam = null)
    {
        $this->set('pagination_url_param', $paginationUrlParam);
        $this->paginationUrlParam = $paginationUrlParam;
        return $this;
    }

    public function getPaginationUrlParam(): ?int
    {
        return $this->paginationUrlParam;
    }

    public function setPaginationUrl(string $paginationUrl = null)
    {
        $this->set('pagination_url', $paginationUrl);
        $this->paginationUrl = $paginationUrl;
        return $this;
    }

    public function getPaginationUrl(): ?string
    {
        return $this->paginationUrl;
    }

    public function setUserId(int $userId)
    {
        $this->set('user_id', $userId);
        $this->userId = $userId;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUrl(Url $url)
    {
        $this->set('url', $url);
        $this->url = $url;
        return $this;
    }

    public function getUrl(): Url
    {
        return $this->url;
    }

    public function setProjectId(int $projectId)
    {
        $this->set('project_id', $projectId);
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): int
    {
        return $this->projectId;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}