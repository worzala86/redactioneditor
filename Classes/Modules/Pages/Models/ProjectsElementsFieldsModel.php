<?php

namespace App\Modules\Pages\Models;

use App\Model;
use App\Types\ShortUUID;
use App\Types\Time;
use App\Types\UUID;

class ProjectsElementsFieldsModel extends Model
{
    public $setDefaultFields = false;

    private $field;
    private $value;
    private $projectElementId;
    private $altId;
    private $id;

    public function setId(ShortUUID $id = null)
    {
        $this->set('id', hex2bin($id));
        $this->id = $id;
        return $this;
    }

    public function getId(): ?ShortUUID
    {
        return $this->id;
    }

    public function setAltId(ShortUUID $altId = null)
    {
        $this->set('alt_id', hex2bin($altId));
        $this->altId = $altId;
        return $this;
    }

    public function getAltId(): ?ShortUUID
    {
        return $this->altId;
    }

    public function setProjectElementId(int $projectElementId)
    {
        $this->set('project_element_id', $projectElementId);
        $this->projectElementId = $projectElementId;
        return $this;
    }

    public function getProjectElementId(): int
    {
        return $this->projectElementId;
    }

    public function setValue(string $value)
    {
        $this->set('value', $value);
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setField(ShortUUID $field)
    {
        $this->set('field', hex2bin($field));
        $this->field = $field;
        return $this;
    }

    public function getField(): ShortUUID
    {
        return $this->field;
    }
}