<?php

namespace App\Modules\Pages\Models;

use App\Model;
use App\Types\Time;
use App\Types\UUID;

class ProjectsElementsModel extends Model
{
    public $setDefaultFields = false;

    private $id;
    private $elementId;
    private $projectId;
    private $added;
    private $uuid;
    private $selector;
    private $kind;

    public function setKind(string $kind)
    {
        $this->set('kind', $kind);
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): string
    {
        return $this->kind;
    }

    public function setSelector(string $selector = null)
    {
        $this->set('selector', $selector);
        $this->selector = $selector;
        return $this;
    }

    public function getSelector(): ?string
    {
        return $this->selector;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setAdded(Time $added)
    {
        $this->set('added', $added);
        $this->added = $added;
        return $this;
    }

    public function getAdded(): Time
    {
        return $this->added;
    }

    public function setProjectId(int $projectId)
    {
        $this->set('project_id', $projectId);
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): int
    {
        return $this->projectId;
    }

    public function setElementId(int $elementId)
    {
        $this->set('element_id', $elementId);
        $this->elementId = $elementId;
        return $this;
    }

    public function getElementId(): int
    {
        return $this->elementId;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}