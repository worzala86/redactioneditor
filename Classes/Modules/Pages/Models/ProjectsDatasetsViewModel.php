<?php

namespace App\Modules\Pages\Models;

use App\Model;
use App\Types\ShortUUID;
use App\Types\Time;
use App\Types\UUID;

class ProjectsDatasetsViewModel extends Model
{
    public $setDefaultFields = false;

    private $projectId;
    private $key;
    private $value;
    private $elementPositionId;

    public function setElementPositionId(UUID $elementPositionId)
    {
        $this->set('element_position_id', $elementPositionId);
        $this->elementPositionId = $elementPositionId;
        return $this;
    }

    public function getElementPositionId(): UUID
    {
        return $this->elementPositionId;
    }

    public function setValue(string $value)
    {
        $this->set('value', $value);
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setKey(ShortUUID $key)
    {
        $this->set('key', hex2bin($key));
        $this->key = $key;
        return $this;
    }

    public function getKey(): ShortUUID
    {
        return $this->key;
    }

    public function setProjectId(int $projectId)
    {
        $this->set('page_id', $projectId);
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): int
    {
        return $this->projectId;
    }
}