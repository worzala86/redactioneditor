<?php

namespace App\Modules\Pages\Models;

use App\Model;
use App\Types\ShortUUID;
use App\Types\Time;
use App\Types\UUID;

class PagesElementsFieldsModel extends Model
{
    public $setDefaultFields = false;

    private $field;
    private $value;
    private $pageElementId;
    private $id;

    public function setId(ShortUUID $id = null)
    {
        $this->set('id', hex2bin($id));
        $this->id = $id;
        return $this;
    }

    public function getId(): ?ShortUUID
    {
        return $this->id;
    }
    private $altId;

    public function setAltId(ShortUUID $altId = null)
    {
        $this->set('alt_id', hex2bin($altId));
        $this->altId = $altId;
        return $this;
    }

    public function getAltId(): ?ShortUUID
    {
        return $this->altId;
    }

    public function setPageElementId(int $pageElementId)
    {
        $this->set('page_element_id', $pageElementId);
        $this->pageElementId = $pageElementId;
        return $this;
    }

    public function getPageElementId(): int
    {
        return $this->pageElementId;
    }

    public function setValue(string $value)
    {
        $this->set('value', $value);
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setField(ShortUUID $field)
    {
        $this->set('field', hex2bin($field));
        $this->field = $field;
        return $this;
    }

    public function getField(): ShortUUID
    {
        return $this->field;
    }
}