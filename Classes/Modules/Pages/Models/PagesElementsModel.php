<?php

namespace App\Modules\Pages\Models;

use App\Model;
use App\Types\Time;
use App\Types\UUID;

class PagesElementsModel extends Model
{
    public $setDefaultFields = false;

    private $id;
    private $elementId;
    private $pageId;
    private $added;
    private $uuid;
    private $selector;

    public function setSelector(string $selector = null)
    {
        $this->set('selector', $selector);
        $this->selector = $selector;
        return $this;
    }

    public function getSelector(): ?string
    {
        return $this->selector;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setAdded(Time $added)
    {
        $this->set('added', $added);
        $this->added = $added;
        return $this;
    }

    public function getAdded(): Time
    {
        return $this->added;
    }

    public function setPageId(int $pageId)
    {
        $this->set('page_id', $pageId);
        $this->pageId = $pageId;
        return $this;
    }

    public function getPageId(): int
    {
        return $this->pageId;
    }

    public function setElementId(int $elementId)
    {
        $this->set('element_id', $elementId);
        $this->elementId = $elementId;
        return $this;
    }

    public function getElementId(): int
    {
        return $this->elementId;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}