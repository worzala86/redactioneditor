<?php

namespace App\Modules\Pages\Handlers;

use App\Handler;
use App\Modules\Pages\Exceptions\PageNotFoundException;
use App\Modules\Pages\Exceptions\ProjectNotFoundException;
use App\Modules\Pages\Models\PagesModel;
use App\Modules\Pages\Requests\UpdatePagePaginationRequest;
use App\Modules\Pages\Responses\UpdatePagePaginationResponse;
use App\Modules\Projects\Models\ProjectsModel;

/**
 * Class UpdatePagePaginationHandler
 * @package App\Modules\Pages\Handlers
 * @description Metoda służy do zapisu ustawień paginacji
 */
class UpdatePagePaginationHandler extends Handler
{
    public function __invoke(UpdatePagePaginationRequest $request): UpdatePagePaginationResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $pageModel = (new PagesModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where(' `project_id`=? ', $projectsModel->getId())
            ->load($request->getId(), true);

        if (!$pageModel->isLoaded()) {
            throw new PageNotFoundException;
        }

        $updated = (new PagesModel)
            ->setPaginationUrl($request->getPaginationUrl())
            ->setPaginationUrlParam($request->getPaginationUrlParam())
            ->setPaginationLimit($request->getPaginationLimit())
            ->update($pageModel->getId());

        return (new UpdatePagePaginationResponse)
            ->setSuccess($updated);
    }
}