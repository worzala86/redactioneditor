<?php

namespace App\Modules\Pages\Handlers;

use App\Handler;
use App\Modules\Pages\Exceptions\PageNotFoundException;
use App\Modules\Pages\Exceptions\ProjectNotFoundException;
use App\Modules\Pages\Exceptions\StyleNotFoundException;
use App\Modules\Pages\Models\PagesModel;
use App\Modules\Pages\Requests\AdminUpdatePageRequest;
use App\Modules\Pages\Responses\AdminUpdatePageResponse;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Workers\PrepereLookupFilesWorker;
use App\Modules\Styles\Models\StylesModel;
use App\Workers\HtmlWorker;

/**
 * Class AdminUpdatePageHandler
 * @package App\Modules\Pages\Handlers
 * @description Metoda służy do zapisania strony - tylko administrator
 */
class AdminUpdatePageHandler extends Handler
{
    public function __invoke(AdminUpdatePageRequest $request): AdminUpdatePageResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $pageModel = (new PagesModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where(' `project_id`=? ', $projectsModel->getId())
            ->load($request->getId(), true);

        if (!$pageModel->isLoaded()) {
            throw new PageNotFoundException;
        }

        (new ProjectsModel)->startTransaction();

        $oldHtmlId = $pageModel->getContentId();

        $htmlWorker = new HtmlWorker;
        $htmlId = $htmlWorker->saveHtmlElements($request->getContent());

        $updated = (new PagesModel)
            ->setContentId($htmlId)
            ->update($pageModel->getId());

        $htmlWorker->delete($oldHtmlId);

        $projectsModel2 = (new ProjectsModel)
            ->setId($projectsModel->getId());

        $headerId = $projectsModel->getHeaderId();
        $footerId = $projectsModel->getFooterId();
        $sidebarLeftId = $projectsModel->getSidebarLeftId();
        $sidebarRightId = $projectsModel->getSidebarRightId();

        $projectsModel2->setHeaderId($htmlWorker->saveHtmlElements($request->getHeader()));
        $projectsModel2->setFooterId($htmlWorker->saveHtmlElements($request->getFooter()));
        $projectsModel2->setSidebarLeftId($htmlWorker->saveHtmlElements($request->getSidebarLeft()));
        $projectsModel2->setSidebarRightId($htmlWorker->saveHtmlElements($request->getSidebarRight()));

        $stylesModel = (new StylesModel)
            ->load($request->getStyleId(), true);

        if (!$stylesModel->isLoaded()) {
            throw new StyleNotFoundException;
        }

        $projectsModel2->setConfig(json_encode($request->getConfig()))
            ->setShowLeftSidebar($request->getShowLeftSidebar())
            ->setShowRightSidebar($request->getShowRightSidebar())
            ->setStyleId($stylesModel->getId());

        $updated = $updated && $projectsModel2->update();

        $htmlWorker->delete($headerId);
        $htmlWorker->delete($footerId);
        $htmlWorker->delete($sidebarLeftId);
        $htmlWorker->delete($sidebarRightId);

        $lookupFilesWorker = new PrepereLookupFilesWorker();
        $lookupFilesWorker->prepare($projectsModel2->getId());

        (new ProjectsModel)->commit();

        return (new AdminUpdatePageResponse)
            ->setSuccess($updated);
    }
}