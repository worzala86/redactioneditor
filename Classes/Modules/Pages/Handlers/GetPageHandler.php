<?php

namespace App\Modules\Pages\Handlers;

use App\Containers\FontContainer;
use App\Containers\HtmlElementContainer;
use App\Containers\HtmlElementsContainer;
use App\Handler;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Pages\Collections\PagesCollection;
use App\Modules\Pages\Collections\PagesDatasetsCollection;
use App\Modules\Pages\Collections\PagesDatasetsViewCollection;
use App\Modules\Pages\Collections\PagesElementsCollection;
use App\Modules\Pages\Collections\PagesElementsFieldsCollection;
use App\Modules\Pages\Collections\ProjectsDatasetsViewCollection;
use App\Modules\Pages\Collections\ProjectsElementsCollection;
use App\Modules\Pages\Collections\ProjectsElementsFieldsCollection;
use App\Modules\Pages\Exceptions\PageNotFoundException;
use App\Modules\Pages\Exceptions\PageWithRequestedUrlNotFoundException;
use App\Modules\Pages\Exceptions\ProjectIsNotYourException;
use App\Modules\Pages\Exceptions\ProjectNotFoundException;
use App\Modules\Pages\Exceptions\StyleNotFoundException;
use App\Modules\Pages\Models\PagesElementsModel;
use App\Modules\Pages\Models\PagesModel;
use App\Modules\Pages\Models\ProjectsElementsModel;
use App\Modules\Pages\Requests\GetPageRequest;
use App\Modules\Pages\Responses\GetPageResponse;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Styles\Models\StylesModel;
use App\User;
use App\Workers\HtmlWorker;

/**
 * Class GetPageHandler
 * @package App\Modules\Pages\Handlers
 * @description Metoda służy do pobrania strony
 */
class GetPageHandler extends Handler
{
    public function __invoke(GetPageRequest $request): GetPageResponse
    {
        $projectsModel = (new ProjectsModel)
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        if (($projectsModel->getUserId()!==$request->getCurrentUserId())&&!$request->getUser()->isAdmin()) {
            throw new ProjectIsNotYourException;
        }

        $config = json_decode($projectsModel->getConfig());

        $pageId = null;
        if ($request->getUrl()) {
            $pagesCollection = (new PagesCollection)
                ->where('(user_id=? or exists(select 1 from projects where id=project_id))', $request->getCurrentUserId())
                ->where('project_id=?', $projectsModel->getId())
                ->loadAll();
            $pageId = null;
            foreach ($pagesCollection as $pageModel) {
                $pageUrl = $pageModel->getUrl();
                $pageUrl = str_replace('/', '\/', $pageUrl);
                preg_match('@' . $pageUrl . '$@', $request->getUrl(), $matches);
                if ($matches) {
                    $pageId = $pageModel->getId();
                    break;
                }
            }
            if (!$pageId) {
                throw new PageWithRequestedUrlNotFoundException;
            }
        }

        $isUuid = false;
        if (!$pageId) {
            $pageId = ($request->getId() ? $request->getId() : ($config?$config->defaultPageId:null));
            $isUuid = true;
        }

        $pageModel = (new PagesModel)
            ->where(' `project_id`=? ', $projectsModel->getId())
            ->load($pageId, $isUuid);

        if ($request->getId()) {
            $config->defaultPageId = (string)$request->getId();
            $config->defaultPageUrl = (string)$pageModel->getUrl();
            (new ProjectsModel)
                ->setConfig(json_encode($config))
                ->update($projectsModel->getId());
        }

        if (!$pageModel->isLoaded()) {
            throw new PageNotFoundException;
        }

        $projectsModel = (new ProjectsModel)
            ->load($projectsModel->getId());

        $htmlWorker = new HtmlWorker;
        //$body = $htmlWorker->loadHtmlElements($pageModel->getContentId());
        $body = $this->getBody($pageModel->getId(), $htmlWorker);
        //$body->setDataSet($elements->current()->getDataSet());//todo: poprawić na ładowanie zbiorcze
        $dataSets = [];
        $pageDatasetsViewCollection = (new PagesDatasetsViewCollection)
            ->where('`page_id`=?', $pageModel->getId())
            ->loadAll();
        foreach ($pageDatasetsViewCollection as $pageDatasetViewModel){
            $dataSets[(string)$pageDatasetViewModel->getElementPositionId()][(string)$pageDatasetViewModel->getKey()] = $pageDatasetViewModel->getValue();
        }

        $projectsDatasetsViewCollection = (new ProjectsDatasetsViewCollection)
            ->where('`project_id`=?', $projectsModel->getId())
            ->loadAll();
        foreach ($projectsDatasetsViewCollection as $projectDatasetViewModel){
            $dataSets[(string)$projectDatasetViewModel->getElementPositionId()][(string)$projectDatasetViewModel->getKey()] = $projectDatasetViewModel->getValue();
        }
        //

        //$header = $htmlWorker->loadHtmlElements($projectsModel->getHeaderId());
        //$footer = $htmlWorker->loadHtmlElements($projectsModel->getFooterId());
        //$sidebarLeft = $htmlWorker->loadHtmlElements($projectsModel->getSidebarLeftId());
        //$sidebarRight = $htmlWorker->loadHtmlElements($projectsModel->getSidebarRightId());

        $header = $this->getHtmlFromProject('header', $projectsModel->getId(), $htmlWorker);
        $sidebarLeft = $this->getHtmlFromProject('sidebar-left', $projectsModel->getId(), $htmlWorker);
        $sidebarRight = $this->getHtmlFromProject('sidebar-right', $projectsModel->getId(), $htmlWorker);
        $footer = $this->getHtmlFromProject('footer', $projectsModel->getId(), $htmlWorker);

        $stylesModel = (new StylesModel)
            ->load($projectsModel->getStyleId());
        if (!$stylesModel->isLoaded()) {
            throw new StyleNotFoundException;
        }
        $customStylesModel = (new StylesModel)
            ->load($projectsModel->getCustomStyleId());
        if (!$customStylesModel->isLoaded()) {
            throw new StyleNotFoundException;
        }

        return (new GetPageResponse)
            ->setId($pageModel->getUuid())
            ->setUrl($pageModel->getUrl())
            ->setBody($body)
            ->setHeader($header)
            ->setFooter($footer)
            ->setSidebarLeft($sidebarLeft)
            ->setSidebarRight($sidebarRight)
            ->setConfig((array)$config)
            ->setShowLeftSidebar($projectsModel->getShowLeftSidebar())
            ->setShowRightSidebar($projectsModel->getShowRightSidebar())
            ->setDomain($projectsModel->getDomain())
            ->setPaginationLimit($pageModel->getPaginationLimit())
            ->setPaginationUrl($pageModel->getPaginationUrl())
            ->setPaginationUrlParam($pageModel->getPaginationUrlParam())
            ->setStyleId($stylesModel->getUuid())
            ->setCustomStyleId($customStylesModel->getUuid())
            ->setDataSet($dataSets);
    }

    private function getHtmlFromProject($kind, $projectId, &$htmlWorker){
        $projectsElementsCollection = (new ProjectsElementsCollection)
            ->where('`kind`=?', $kind)
            ->where('`project_id`=?', $projectId)
            ->loadAll();
        $html = new HtmlElementContainer;
        $elements = new HtmlElementsContainer;
        foreach ($projectsElementsCollection as $projectElement){
            $elementModel = (new ElementsModel)
                ->load($projectElement->getElementId());
            $htmls = $htmlWorker->loadHtmlElements($elementModel->getContentId(), $projectElement->getUuid());
            $htmls->setElementId($elementModel->getUuid());
            $htmls->setElementPositionId($projectElement->getUuid());
            $htmls->setSelector($projectElement->getSelector());
            $projectElementFieldCollection = (new ProjectsElementsFieldsCollection)
                ->where('`project_element_id`=?', $projectElement->getId())
                ->loadAll();
            /*$fields = [];
            foreach ($projectElementFieldCollection as $projectElementFieldModel){
                $fields[(string)$projectElementFieldModel->getField()] = [
                    'value'=>(string)$projectElementFieldModel->getValue(),
                    'altId'=>$projectElementFieldModel->getAltId(),
                    'Id'=>$projectElementFieldModel->getId(),
                ];
            }
            $htmls->setFields($fields);*/
            $elements->add($htmls);
        }
        return $html->setChildrens($elements);
    }

    private function getBody($pageId, &$htmlWorker){
        $pageElementsCollection = (new PagesElementsCollection)
            ->where('`page_id`=?', $pageId)
            ->loadAll();
        $body = new HtmlElementContainer;
        $elements = new HtmlElementsContainer;
        foreach ($pageElementsCollection as $pageElement){
            $elementModel = (new ElementsModel)
                ->load($pageElement->getElementId());
            $htmls = $htmlWorker->loadHtmlElements($elementModel->getContentId(), $pageElement->getUuid());
            $htmls->setElementId($elementModel->getUuid());
            $htmls->setElementPositionId($pageElement->getUuid());
            $htmls->setSelector($pageElement->getSelector());
            /*$pageElementFieldCollection = (new PagesElementsFieldsCollection)
                ->where('`page_element_id`=?', $pageElement->getId())
                ->loadAll();
            $fields = [];
            foreach ($pageElementFieldCollection as $pageElementFieldModel){
                $fields[(string)$pageElementFieldModel->getField()] = [
                    'value'=>(string)$pageElementFieldModel->getValue(),
                    'altId'=>$pageElementFieldModel->getAltId(),
                    'id'=>$pageElementFieldModel->getId(),
                ];
            }
            $htmls->setFields($fields);*/
            $elements->add($htmls);
        }
        return $body->setChildrens($elements);
    }
}