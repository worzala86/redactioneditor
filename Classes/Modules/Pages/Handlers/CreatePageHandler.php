<?php

namespace App\Modules\Pages\Handlers;

use App\Handler;
use App\Modules\Pages\Exceptions\ProjectNotFoundException;
use App\Modules\Pages\Models\PagesModel;
use App\Modules\Pages\Requests\CreatePageRequest;
use App\Modules\Pages\Responses\CreatePageResponse;
use App\Modules\Projects\Models\ProjectsModel;
use App\Types\UUID;

/**
 * Class CreatePageHandler
 * @package App\Modules\Pages\Handlers
 * @description Metoda służy do zmiany adresu url strony
 */
class CreatePageHandler extends Handler
{
    public function __invoke(CreatePageRequest $request): CreatePageResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $uuid = UUID::fake();
        (new PagesModel)
            ->setUserId($request->getCurrentUserId())
            ->setUuid($uuid)
            ->setUrl($request->getUrl())
            ->setProjectId($projectsModel->getId())
            ->insert();

        return (new CreatePageResponse)
            ->setId($uuid);
    }
}