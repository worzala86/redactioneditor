<?php

namespace App\Modules\Pages\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Pages\Exceptions\ElementNotFoundException;
use App\Modules\Pages\Exceptions\PageNotFoundException;
use App\Modules\Pages\Exceptions\ProjectNotFoundException;
use App\Modules\Pages\Exceptions\StyleNotFoundException;
use App\Modules\Pages\Models\PagesDatasetsModel;
use App\Modules\Pages\Models\PagesElementsFieldsModel;
use App\Modules\Pages\Models\PagesElementsModel;
use App\Modules\Pages\Models\PagesModel;
use App\Modules\Pages\Models\ProjectsDatasetsModel;
use App\Modules\Pages\Models\ProjectsElementsFieldsModel;
use App\Modules\Pages\Models\ProjectsElementsModel;
use App\Modules\Pages\Requests\UpdatePageRequest;
use App\Modules\Pages\Responses\UpdatePageResponse;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\Projects\Workers\PrepereLookupFilesWorker;
use App\Modules\Styles\Models\StylesModel;
use App\Types\ShortUUID;
use App\Types\Time;
use App\Workers\HtmlWorker;

/**
 * Class UpdatePageHandler
 * @package App\Modules\Pages\Handlers
 * @description Metoda służy do zapisania strony
 */
class UpdatePageHandler extends Handler
{
    public function __invoke(UpdatePageRequest $request): UpdatePageResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $pageModel = (new PagesModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where(' `project_id`=? ', $projectsModel->getId())
            ->load($request->getId(), true);

        if (!$pageModel->isLoaded()) {
            throw new PageNotFoundException;
        }

        (new ProjectsModel)->startTransaction();

        //$oldHtmlId = $pageModel->getContentId();

        $htmlWorker = new HtmlWorker;
        //$htmlId = $htmlWorker->saveHtmlElements($request->getContent());
        $this->saveBody($pageModel, $request->getBody());

        $this->saveBodyDataSet($request->getDataSet(), $pageModel->getId());

        /*$updated = (new PagesModel)
            ->setContentId($htmlId)
            ->update($pageModel->getId());*/

        //$htmlWorker->delete($oldHtmlId);

        $projectsModel2 = (new ProjectsModel)
            ->setId($projectsModel->getId());

        $this->saveProjectElement('header', $projectsModel, $request->getHeader());
        $this->saveProjectElement('sidebar-left', $projectsModel, $request->getSidebarLeft());
        $this->saveProjectElement('sidebar-right', $projectsModel, $request->getSidebarRight());
        $this->saveProjectElement('footer', $projectsModel, $request->getFooter());

        $this->saveProjectDataSet($request->getDataSet(), $projectsModel->getId());

        /*$headerId = $projectsModel->getHeaderId();
        $footerId = $projectsModel->getFooterId();
        $sidebarLeftId = $projectsModel->getSidebarLeftId();
        $sidebarRightId = $projectsModel->getSidebarRightId();

        $projectsModel2->setHeaderId($htmlWorker->saveHtmlElements($request->getHeader()));
        $projectsModel2->setFooterId($htmlWorker->saveHtmlElements($request->getFooter()));
        $projectsModel2->setSidebarLeftId($htmlWorker->saveHtmlElements($request->getSidebarLeft()));
        $projectsModel2->setSidebarRightId($htmlWorker->saveHtmlElements($request->getSidebarRight()));*/

        $stylesModel = (new StylesModel)
            ->load($request->getStyleId(), true);

        if (!$stylesModel->isLoaded()) {
            throw new StyleNotFoundException;
        }

        $projectsModel2->setConfig(json_encode($request->getConfig()))
            ->setShowLeftSidebar($request->getShowLeftSidebar())
            ->setShowRightSidebar($request->getShowRightSidebar())
            ->setStyleId($stylesModel->getId());

        $updated = $projectsModel2->update();

        /*$htmlWorker->delete($headerId);
        $htmlWorker->delete($footerId);
        $htmlWorker->delete($sidebarLeftId);
        $htmlWorker->delete($sidebarRightId);*/

        $lookupFilesWorker = new PrepereLookupFilesWorker();
        $lookupFilesWorker->prepare($projectsModel2->getId());

        (new ProjectsModel)->commit();

        return (new UpdatePageResponse)
            ->setSuccess($updated);
    }

    private function saveProjectElement($kind, $projectModel, $pageElements){
        $array = DB::get()->getAll('select uuid, id from projects_elements where project_id=? and kind=?',
            [$projectModel->getId(), $kind]);
        $oldIds = [];
        foreach ($array as $a) {
            $oldIds[bin2hex($a['uuid'])] = $a['id'];
        }
        foreach ($pageElements as $pageElement) {
            if ($pageElement->getId()) {
                $elementModel = (new ElementsModel)
                    ->load($pageElement->getElementId(), true);
                if (!$elementModel->isLoaded()) {
                    throw new ElementNotFoundException;
                }
                $projectElementId = null;
                $newPageElementUuid = null;
                $projectElementModel = (new ProjectsElementsModel)
                    ->load($pageElement->getId(), true);
                if ($projectElementModel->isLoaded()) {
                    (new ProjectsElementsModel)
                        ->setSelector($pageElement->getSelector())
                        ->setKind($kind)
                        ->update($projectElementModel->getId());
                    $projectElementId = $projectElementModel->getId();
                } else {
                    $newPageElementUuid = $pageElement->getId();
                    $projectElementId = (new ProjectsElementsModel)
                        ->setUuid($newPageElementUuid)
                        ->setProjectId($projectModel->getId())
                        ->setElementId($elementModel->getId())
                        ->setAdded(Time::fake())
                        ->setSelector($pageElement->getSelector())
                        ->setKind($kind)
                        ->insert();
                }
                DB::get()->execute('delete from projects_elements_fields where project_element_id=?', $projectElementId);
                foreach ($pageElement->getFields() as $field) {
                    (new ProjectsElementsFieldsModel)
                        ->setField($field->getField())
                        ->setValue($field->getValue())
                        ->setAltId($field->getAltId())
                        ->setId($field->getId())
                        ->setProjectElementId($projectElementId)
                        ->insert();
                }
            }
            if (isset($oldIds[(string)$pageElement->getId()])) {
                unset($oldIds[(string)$pageElement->getId()]);
            }
        }
        $quote = [];
        $params = [];
        foreach ($oldIds as $id) {
            $quote[] = '?';
            $params[] = $id;
        }
        if (count($params) > 0) {
            DB::get()->execute('delete from projects_datasets where element_position_id in (' . join(', ', $quote) . ')', $params);
            DB::get()->execute('delete from projects_elements_fields where project_element_id in (' . join(', ', $quote) . ')', $params);
            DB::get()->execute('delete from projects_elements where id in (' . join(', ', $quote) . ')', $params);
        }
    }

    private function saveBody($pageModel, $pageElements){
        $array = DB::get()->getAll('select uuid, id from pages_elements where page_id=?', $pageModel->getId());
        $oldIds = [];
        foreach ($array as $a) {
            $oldIds[bin2hex($a['uuid'])] = $a['id'];
        }
        foreach ($pageElements as $pageElement) {
            if ($pageElement->getId()) {
                $elementModel = (new ElementsModel)
                    ->load($pageElement->getElementId(), true);
                if (!$elementModel->isLoaded()) {
                    throw new ElementNotFoundException;
                }
                $pageElementId = null;
                $newPageElementUuid = null;
                $pageElementModel = (new PagesElementsModel)
                    ->load($pageElement->getId(), true);
                if ($pageElementModel->isLoaded()) {
                    (new PagesElementsModel)
                        ->setSelector($pageElement->getSelector())
                        ->update($pageElementModel->getId());
                    $pageElementId = $pageElementModel->getId();
                } else {
                    $newPageElementUuid = $pageElement->getId();
                    $pageElementId = (new PagesElementsModel)
                        ->setUuid($newPageElementUuid)
                        ->setPageId($pageModel->getId())
                        ->setElementId($elementModel->getId())
                        ->setAdded(Time::fake())
                        ->setSelector($pageElement->getSelector())
                        ->insert();
                }
                DB::get()->execute('delete from pages_elements_fields where page_element_id=?', $pageElementId);
                foreach ($pageElement->getFields() as $field) {
                    (new PagesElementsFieldsModel)
                        ->setField($field->getField())
                        ->setValue($field->getValue())
                        ->setAltId($field->getAltId())
                        ->setId($field->getId())
                        ->setPageElementId($pageElementId)
                        ->insert();
                }
            }
            if (isset($oldIds[(string)$pageElement->getId()])) {
                unset($oldIds[(string)$pageElement->getId()]);
            }
        }
        $quote = [];
        $params = [];
        foreach ($oldIds as $id) {
            $quote[] = '?';
            $params[] = $id;
        }
        if (count($params) > 0) {
            DB::get()->execute('delete from pages_datasets where element_position_id in (' . join(', ', $quote) . ')', $params);
            DB::get()->execute('delete from pages_elements_fields where page_element_id in (' . join(', ', $quote) . ')', $params);
            DB::get()->execute('delete from pages_elements where id in (' . join(', ', $quote) . ')', $params);
        }
    }

    private function saveBodyDataSet($dataSet, $pageId){
        DB::get()->execute('delete from pages_datasets where page_id=?', $pageId);
        foreach ($dataSet as $elementPositionId => $dataset) {
            $pagesElementModel = (new PagesElementsModel)
                ->load($elementPositionId, true);
            if(!$pagesElementModel->isLoaded()){
                continue;
            }
            foreach ($dataset as $key => $dataSet) {
                (new PagesDatasetsModel)
                    ->setKey(new ShortUUID($key))
                    ->setPageId($pageId)
                    ->setValue($dataSet)
                    ->setElementPositionId($pagesElementModel->getId())
                    ->insert();
            }
        }
    }

    private function saveProjectDataSet($dataSet, $projectId){
        DB::get()->execute('delete from projects_datasets where project_id=?', $projectId);
        foreach ($dataSet as $elementPositionId => $dataset) {
            $projectElementModel = (new ProjectsElementsModel)
                ->load($elementPositionId, true);
            if(!$projectElementModel->isLoaded()){
                continue;
            }
            foreach ($dataset as $key => $dataSet) {
                (new ProjectsDatasetsModel)
                    ->setKey(new ShortUUID($key))
                    ->setProjectId($projectId)
                    ->setValue($dataSet)
                    ->setElementPositionId($projectElementModel->getId())
                    ->insert();
            }
        }
    }
}