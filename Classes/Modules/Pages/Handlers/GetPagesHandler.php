<?php

namespace App\Modules\Pages\Handlers;

use App\Containers\PageContainer;
use App\Containers\PagesContainer;
use App\Handler;
use App\Modules\Pages\Collections\PagesCollection;
use App\Modules\Pages\Exceptions\ProjectNotFoundException;
use App\Modules\Pages\Requests\GetPagesRequest;
use App\Modules\Pages\Responses\GetPagesResponse;
use App\Modules\Projects\Models\ProjectsModel;

/**
 * Class GetPagesHandler
 * @package App\Modules\Pages\Handlers
 * @description Metoda służy do pobrania listy stron
 */
class GetPagesHandler extends Handler
{
    public function __invoke(GetPagesRequest $request): GetPagesResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $pagesCollection = (new PagesCollection)
            ->setPage($request->getPage())
            ->setLimit($request->getLimit())
            ->setSortName($request->getSortName())
            ->setSortKind($request->getSortKind())
            ->setFilters($request->getFilters())
            ->where('project_id=?', $projectsModel->getId())
            ->loadAll();

        $pages = new PagesContainer;
        foreach ($pagesCollection as $pagesModel) {
            $pages->add(
                (new PageContainer)
                    ->setId($pagesModel->getUuid())
                    ->setUrl($pagesModel->getUrl())
            );
        }

        return (new GetPagesResponse)
            ->setPages($pages);
    }
}