<?php

namespace App\Modules\Pages\Handlers;

use App\Handler;
use App\Modules\Pages\Exceptions\PageNotFoundException;
use App\Modules\Pages\Exceptions\ProjectNotFoundException;
use App\Modules\Pages\Models\PagesModel;
use App\Modules\Pages\Requests\UpdatePageUrlRequest;
use App\Modules\Pages\Responses\UpdatePageUrlResponse;
use App\Modules\Projects\Models\ProjectsModel;

/**
 * Class UpdatePageUrlHandler
 * @package App\Modules\Pages\Handlers
 * @description Metoda służy do zmiany adresu url strony
 */
class UpdatePageUrlHandler extends Handler
{
    public function __invoke(UpdatePageUrlRequest $request): UpdatePageUrlResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $pageModel = (new PagesModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where(' `project_id`=? ', $projectsModel->getId())
            ->load($request->getId(), true);

        if (!$pageModel->isLoaded()) {
            throw new PageNotFoundException;
        }

        $updated = $pageModel
            ->setUrl($request->getUrl())
            ->update();

        return (new UpdatePageUrlResponse)
            ->setSuccess($updated);
    }
}