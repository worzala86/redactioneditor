<?php

namespace App\Modules\Pages\Handlers;

use App\Handler;
use App\Modules\Pages\Exceptions\PageNotFoundException;
use App\Modules\Pages\Exceptions\ProjectNotFoundException;
use App\Modules\Pages\Models\PagesModel;
use App\Modules\Pages\Requests\DeletePageRequest;
use App\Modules\Pages\Responses\DeletePageResponse;
use App\Modules\Projects\Models\ProjectsModel;

/**
 * Class DeletePageHandler
 * @package App\Modules\Pages\Handlers
 * @description Metoda służy do usunięcia strony
 */
class DeletePageHandler extends Handler
{
    public function __invoke(DeletePageRequest $request): DeletePageResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $pageModel = (new PagesModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where(' `project_id`=? ', $projectsModel->getId())
            ->load($request->getId(), true);

        if (!$pageModel->isLoaded()) {
            throw new PageNotFoundException;
        }

        $deleted = $pageModel->delete();

        return (new DeletePageResponse)
            ->setSuccess($deleted);
    }
}