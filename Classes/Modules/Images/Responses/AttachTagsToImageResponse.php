<?php

namespace App\Modules\Images\Responses;

use App\Response;

class AttachTagsToImageResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Inormuje czy pomyślnie przypisano tagi do obrazka
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }
}