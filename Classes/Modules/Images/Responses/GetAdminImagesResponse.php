<?php

namespace App\Modules\Images\Responses;

use App\Containers\ImagesContainer;
use App\Response;
use App\TotalCountTrait;

class GetAdminImagesResponse extends Response
{
    use TotalCountTrait;

    private $images;

    /**
     * @param ImagesContainer $images
     * @description Lista obrazków
     * @return $this
     */
    public function setImages(ImagesContainer $images)
    {
        $this->images = $images;
        return $this;
    }

    public function getImages(): ImagesContainer
    {
        return $this->images;
    }
}