<?php

namespace App\Modules\Images\Responses;

use App\Response;

class DeleteImageResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy pomyślnie usunięto zdjęcie
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}