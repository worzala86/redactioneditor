<?php

namespace App\Modules\Images\Collections;

use App\CollectionTrait;
use App\Modules\Images\Models\ImagesCategoriesModel;

class ImagesCategoriesCollection extends ImagesCategoriesModel implements \Iterator
{
    use CollectionTrait;
}