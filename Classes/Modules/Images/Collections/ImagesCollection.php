<?php

namespace App\Modules\Images\Collections;

use App\CollectionTrait;
use App\Modules\Images\Models\ImagesModel;
use App\PaginationTrait;

class ImagesCollection extends ImagesModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}