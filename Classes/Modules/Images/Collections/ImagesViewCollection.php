<?php

namespace App\Modules\Images\Collections;

use App\CollectionTrait;
use App\Modules\Images\Models\ImagesViewModel;
use App\PaginationTrait;

class ImagesViewCollection extends ImagesViewModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}