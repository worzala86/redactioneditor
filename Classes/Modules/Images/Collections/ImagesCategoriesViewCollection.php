<?php

namespace App\Modules\Images\Collections;

use App\CollectionTrait;
use App\Modules\Images\Models\ImagesCategoriesViewModel;

class ImagesCategoriesViewCollection extends ImagesCategoriesViewModel implements \Iterator
{
    use CollectionTrait;
}