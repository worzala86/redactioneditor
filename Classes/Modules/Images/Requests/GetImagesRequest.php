<?php

namespace App\Modules\Images\Requests;

use App\FiltersTrait;
use App\PaginationTrait;
use App\Types\ImagesKind;
use App\UserRequest;

class GetImagesRequest extends UserRequest
{
    use PaginationTrait;
    use FiltersTrait;

    private $kind;

    /**
     * @param string $kind
     * @description Rozdaj obrazka custom|system
     * @return $this
     */
    public function setKind(ImagesKind $kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): ImagesKind
    {
        return $this->kind;
    }
}