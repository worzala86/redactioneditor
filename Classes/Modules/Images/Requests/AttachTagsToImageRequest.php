<?php

namespace App\Modules\Images\Requests;

use App\Types\UUID;
use App\UserRequest;

class AttachTagsToImageRequest extends UserRequest
{
    private $imageId;
    private $tags;

    /**
     * @param UUID $imageId
     * @description Identyfikator zdjęcia
     * @return $this
     */
    public function setImageId(UUID $imageId)
    {
        $this->imageId = $imageId;
        return $this;
    }

    public function getImageId(): string
    {
        return $this->imageId;
    }

    /**
     * @param string $tags
     * @description Tagi do przypisania do zdjęcia
     * @return $this
     */
    public function setTags(string $tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getTags(): string
    {
        return $this->tags;
    }
}