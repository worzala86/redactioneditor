<?php

namespace App\Modules\Images\Requests;

use App\Types\UUID;
use App\UserRequest;

class AssignImageRequest extends UserRequest
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator pliku do przypisania do zdjęć
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}