<?php

namespace App\Modules\Images\Requests;

use App\AdminRequest;
use App\FiltersTrait;
use App\PaginationTrait;

class GetAdminImagesRequest extends AdminRequest
{
    use FiltersTrait;
    use PaginationTrait;
}