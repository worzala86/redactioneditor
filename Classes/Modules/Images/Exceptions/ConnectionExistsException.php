<?php

namespace App\Modules\Images\Exceptions;

use App\NamedException;

class ConnectionExistsException extends \Exception implements NamedException
{
}