<?php

namespace App\Modules\Images\Exceptions;

use App\NamedException;

class ConnectionNotExistsException extends \Exception implements NamedException
{
}