<?php

namespace App\Modules\Images\Exceptions;

use App\NamedException;

class ImageNotFoundException extends \Exception implements NamedException
{
}