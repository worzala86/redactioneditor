<?php

namespace App\Modules\Images\Exceptions;

use App\NamedException;

class ImageCategoryNotFoundException extends \Exception implements NamedException
{
}