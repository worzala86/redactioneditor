<?php

namespace App\Modules\Images\Models;

use App\Model;
use App\Types\UUID;

class ImagesModel extends Model
{
    private $id;
    private $uuid;
    private $userId;
    private $fileId;
    private $pixabayId;

    public function setPixabayId(int $pixabayId = null)
    {
        $this->set('pixabay_id', $pixabayId);
        $this->pixabayId = $pixabayId;
        return $this;
    }

    public function getPixabayId(): ?int
    {
        return $this->pixabayId;
    }

    public function setFileId(int $fileId)
    {
        $this->set('file_id', $fileId);
        $this->fileId = $fileId;
        return $this;
    }

    public function getFileId(): int
    {
        return $this->fileId;
    }

    public function setUserId(int $userId)
    {
        $this->set('user_id', $userId);
        $this->userId = $userId;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}