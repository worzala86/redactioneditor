<?php

namespace App\Modules\Images\Handlers;

use App\Containers\ImageContainer;
use App\Containers\ImagesContainer;
use App\Containers\ImageThumbContainer;
use App\File;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Images\Collections\ImagesCollection;
use App\Modules\Images\Requests\GetImagesRequest;
use App\Modules\Images\Responses\GetImagesResponse;

/**
 * Class GetImagesHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania listy obrazków
 */
class GetImagesHandler extends Handler
{
    public function __invoke(GetImagesRequest $request): GetImagesResponse
    {
        $imagesCollection = (new ImagesCollection)
            ->setLimit($request->getLimit())
            ->setPage($request->getPage());
        if ((string)$request->getKind() == 'custom') {
            $imagesCollection->where('user_id=?', $request->getCurrentUserId());
        }
        $imagesCollection->loadAll();

        $images = new ImagesContainer;
        foreach ($imagesCollection as $imagesModel) {
            /*$filesModel = (new FilesModel)
                ->load($imagesModel->getFileId());
            $image = new ImageContainer;
            $thumb = new ImageThumbContainer;
            $fileThumb = (new FilesModel)
                ->load($filesModel->getThumbId());
            if ($fileThumb->isLoaded()) {
                $thumb->setUrl($fileThumb->getUrl())
                    ->setName($fileThumb->getName())
                    ->setId($fileThumb->getUuid())
                    ->setSize($fileThumb->getSize());
            }
            $image->setUrl($filesModel->getUrl())
                ->setName($filesModel->getName())
                ->setId($filesModel->getUuid())
                ->setSize($filesModel->getSize())
                ->setThumb($thumb);
            $images->add($image);*/
            $file = new File;
            $row=$file->load($imagesModel->getFileId());
            $images->add($row);
        }

        return (new GetImagesResponse)
            ->setImages($images)
            ->setTotalCount($imagesCollection->getTotalCount());
    }
}