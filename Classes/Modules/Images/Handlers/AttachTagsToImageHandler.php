<?php

namespace App\Modules\Images\Handlers;

use App\Handler;
use App\Modules\Images\Exceptions\ImageNotFoundException;
use App\Modules\Images\Models\ImagesModel;
use App\Modules\Images\Requests\AttachTagsToImageRequest;
use App\Modules\Images\Responses\AttachTagsToImageResponse;
use App\Workers\TagsWorker;

/**
 * Class AttachCategoryToImageHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do przypisywania obrazkom kategorii
 */
class AttachTagsToImageHandler extends Handler
{
    public function __invoke(AttachTagsToImageRequest $request): AttachTagsToImageResponse
    {
        $imagesModel = (new ImagesModel)
            ->load($request->getImageId(), true);

        if(!$imagesModel->isLoaded()){
            throw new ImageNotFoundException;
        }

        $tagsWorker = new TagsWorker;
        $tagsWorker->setImageId($imagesModel->getId());
        $tagsWorker->saveTagsToDatabase($request->getTags());

        return (new AttachTagsToImageResponse)
            ->setSuccess(true);
    }
}