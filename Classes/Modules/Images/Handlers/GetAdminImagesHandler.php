<?php

namespace App\Modules\Images\Handlers;

use App\Containers\ImageContainer;
use App\Containers\ImagesContainer;
use App\Containers\ImageThumbContainer;
use App\Database\DB;
use App\File;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Images\Collections\ImagesViewCollection;
use App\Modules\Images\Requests\GetAdminImagesRequest;
use App\Modules\Images\Responses\GetAdminImagesResponse;
use App\Modules\User\Collections\UsersCollection;
use App\Workers\TagsWorker;

/**
 * Class GetAdminImagesHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania listy obrazków
 */
class GetAdminImagesHandler extends Handler
{
    public function __invoke(GetAdminImagesRequest $request): GetAdminImagesResponse
    {
        $usersCollection = (new UsersCollection)
            ->where('editor=?', 1)
            ->loadAll();
        $usersSql = [];
        $usersIds = [];
        foreach ($usersCollection as $usersModel) {
            $usersIds[] = $usersModel->getId();
            $usersSql[] = '?';
        }

        $imagesCollection = (new ImagesViewCollection)
            ->setFilters($request->getFilters())
            ->setLimit($request->getLimit())
            ->setPage($request->getPage())
            ->where('user_id in (' . join(', ', $usersSql) . ')', $usersIds)
            ->loadAll();

        $images = new ImagesContainer;
        foreach ($imagesCollection as $imagesModel) {
            $image = (new File)->load($imagesModel->getFileId());
            $images->add($image);
        }

        return (new GetAdminImagesResponse)
            ->setImages($images)
            ->setTotalCount($imagesCollection->getTotalCount());
    }
}