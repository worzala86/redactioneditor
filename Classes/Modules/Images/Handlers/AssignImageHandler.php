<?php

namespace App\Modules\Images\Handlers;

use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Images\Exceptions\ConnectionExistsException;
use App\Modules\Images\Exceptions\ImageNotFoundException;
use App\Modules\Images\Models\ImagesModel;
use App\Modules\Images\Requests\AssignImageRequest;
use App\Modules\Images\Responses\AssignImageResponse;
use App\Types\UUID;

/**
 * Class GetImagesHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do pobrania listy obrazków
 */
class AssignImageHandler extends Handler
{
    public function __invoke(AssignImageRequest $request): AssignImageResponse
    {
        $filesModel = (new FilesModel)
            ->where('user_id=?', $request->getCurrentUserId())
            ->load($request->getId(), true);

        if (!$filesModel->isLoaded()) {
            throw new ImageNotFoundException;
        }

        $imagesModel = (new ImagesModel)
            ->where('file_id=?', $filesModel->getId())
            ->where('user_id=?', $request->getCurrentUserId())
            ->load();

        if ($imagesModel->isLoaded()) {
            throw new ConnectionExistsException;
        }

        $id = (new ImagesModel)
            ->setUuid(UUID::fake())
            ->setFileId($filesModel->getId())
            ->setUserId($request->getCurrentUserId())
            ->insert();

        return (new AssignImageResponse)
            ->setSuccess($id ? true : false);
    }
}