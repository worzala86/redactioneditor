<?php

namespace App\Modules\Images\Handlers;

use App\File;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Images\Exceptions\ConnectionNotExistsException;
use App\Modules\Images\Exceptions\ImageNotFoundException;
use App\Modules\Images\Models\ImagesModel;
use App\Modules\Images\Requests\DeleteImageRequest;
use App\Modules\Images\Responses\DeleteImageResponse;
use App\Types\FileTarget;

/**
 * Class DeleteImageHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do usuwania zdjęcia wedłu ID
 */
class DeleteImageHandler extends Handler
{
    public function __invoke(DeleteImageRequest $request): DeleteImageResponse
    {
        $filesModel = (new FilesModel)
            ->where('user_id=?', $request->getCurrentUserId())
            ->load($request->getId(), true);

        if (!$filesModel->isLoaded()) {
            throw new ImageNotFoundException;
        }

        $imagesModel = (new ImagesModel)
            ->where('file_id=?', $filesModel->getId())
            ->where('user_id=?', $request->getCurrentUserId())
            ->load();

        if (!$imagesModel->isLoaded()) {
            throw new ConnectionNotExistsException;
        }

        $deleted = $imagesModel->delete();

        return (new DeleteImageResponse)
            ->setSuccess($deleted);
    }
}