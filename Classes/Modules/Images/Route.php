<?php

namespace App;

use App\Modules\Images\Handlers;
use App\Types\ImagesKind;
use App\Types\UUID;

return [
    ['method'=>'get', 'url'=>'api/images/:kind', 'handler'=>Handlers\GetImagesHandler::class, 'regex'=>['kind'=>ImagesKind::$pattern]],
    ['method'=>'get', 'url'=>'api/admin-images', 'handler'=>Handlers\GetAdminImagesHandler::class],
    ['method'=>'put', 'url'=>'api/images/:id', 'handler'=>Handlers\AssignImageHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'delete', 'url'=>'api/images/:id', 'handler'=>Handlers\DeleteImageHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/images/:imageId/tags', 'handler'=>Handlers\AttachTagsToImageHandler::class, 'regex'=>['imageId'=>UUID::$pattern]],
];