<?php

namespace App;

use App\Modules\Fonts\Handlers;
use App\Types\UUID;

return [
    ['method'=>'get', 'url'=>'api/fonts', 'handler'=>Handlers\GetFontsHandler::class],
];