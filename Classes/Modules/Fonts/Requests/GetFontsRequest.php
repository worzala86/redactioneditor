<?php

namespace App\Modules\Fonts\Requests;

use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;
use App\UserRequest;

class GetFontsRequest extends UserRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;
}