<?php

namespace App\Modules\Fonts\Responses;

use App\Containers\FontsContainer;
use App\Response;

class GetFontsResponse extends Response
{
    private $fonts;

    /**
     * @param FontsContainer $fonts
     * @description Pole zawiera listę czcionek
     * @return $this
     */
    public function setFonts(FontsContainer $fonts)
    {
        $this->fonts = $fonts;
        return $this;
    }

    public function getFonts(): FontsContainer
    {
        return $this->fonts;
    }
}