<?php

namespace App\Modules\Fonts\Handlers;

use App\Containers\FontContainer;
use App\Containers\FontsContainer;
use App\Curl;
use App\Handler;
use App\Modules\Fonts\Requests\GetFontsRequest;
use App\Modules\Fonts\Responses\GetFontsResponse;

/**
 * Class GetFontsHandler
 * @package App\Modules\Fonts\Handlers
 * @description Metoda służy do pobrania listy styli
 */
class GetFontsHandler extends Handler
{
    public function __invoke(GetFontsRequest $request): GetFontsResponse
    {
        $fonts = new FontsContainer;

        $curl = new Curl;
        $result = $curl->get('https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity&key=AIzaSyAHuHNrDglYNoigbzU0PhtncSLRCZBJE_s');

        $fontsObject = json_decode($result);

        foreach($fontsObject->items as $font) {
            $name = str_replace(' ', '+', $font->family);
            $variants = [];
            foreach($font->variants as $variant){
                $variant = str_replace('italic', 'i', $variant);
                $variant = str_replace('regular', '400', $variant);
                $variants[] = $variant;
            }
            $variants = join(',', $variants);
            $link = 'https://fonts.googleapis.com/css?family=' . $name . ':'.$variants;
            $fonts->add(
                (new FontContainer)
                    ->setName($font->family)
                    ->setCategory($font->category)
                    ->setLink($link)
            );
        }

        return (new GetFontsResponse)
            ->setFonts($fonts);
    }
}