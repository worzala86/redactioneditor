<?php

namespace App\Modules\Crm\Models;

use App\Model;
use App\Types\Url;
use App\Types\UUID;

class CrmPagesModel extends Model
{
    private $id;
    private $uuid;
    private $title;
    private $content;
    private $url;

    public function setUrl(Url $url)
    {
        $this->set('url', $url);
        $this->url = $url;
        return $this;
    }

    public function getUrl(): Url
    {
        return $this->url;
    }

    public function setContent(string $content)
    {
        $this->set('content', $content);
        $this->content = $content;
        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setTitle(string $title)
    {
        $this->set('title', $title);
        $this->title = $title;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', $uuid);
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}