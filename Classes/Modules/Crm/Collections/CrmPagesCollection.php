<?php

namespace App\Modules\Crm\Collections;

use App\CollectionTrait;
use App\Modules\Crm\Models\CrmPagesModel;
use App\PaginationTrait;

class CrmPagesCollection extends CrmPagesModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}