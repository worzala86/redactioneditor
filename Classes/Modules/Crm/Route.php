<?php

namespace App;

use App\Modules\Crm\Handlers;
use App\Types\UUID;

return [
    ['method'=>'get', 'url'=>'api/crm/pages', 'handler'=>Handlers\GetCrmPagesHandler::class],
    ['method'=>'post', 'url'=>'api/crm-page', 'handler'=>Handlers\GetUserCrmPageHandler::class],
    ['method'=>'post', 'url'=>'api/crm/pages', 'handler'=>Handlers\CreateCrmPageHandler::class],
    ['method'=>'get', 'url'=>'api/crm/pages/:id', 'handler'=>Handlers\GetCrmPageHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'put', 'url'=>'api/crm/pages/:id', 'handler'=>Handlers\UpdateCrmPageHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'delete', 'url'=>'api/crm/pages/:id', 'handler'=>Handlers\DeleteCrmPageHandler::class, 'regex'=>['id'=>UUID::$pattern]],
];