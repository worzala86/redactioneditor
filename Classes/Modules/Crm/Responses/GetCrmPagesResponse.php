<?php

namespace App\Modules\Crm\Responses;

use App\Containers\CrmPagesContainer;
use App\Response;

class GetCrmPagesResponse extends Response
{
    private $pages;

    /**
     * @param CrmPagesContainer $pages
     * @description Lista stron w Crm
     * @return $this
     */
    public function setPages(CrmPagesContainer $pages)
    {
        $this->pages = $pages;
        return $this;
    }

    public function getPages(): CrmPagesContainer
    {
        return $this->pages;
    }
}