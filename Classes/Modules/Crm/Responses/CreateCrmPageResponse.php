<?php

namespace App\Modules\Crm\Responses;

use App\Response;
use App\Types\UUID;

class CreateCrmPageResponse extends Response
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator nowo dodanej strony
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}