<?php

namespace App\Modules\Crm\Responses;

use App\Response;
use App\Types\UUID;

class UpdateCrmPageResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Informacja o powodzeneniu update strony w Crm
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }
}