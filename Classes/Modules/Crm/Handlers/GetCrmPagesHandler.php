<?php

namespace App\Modules\Crm\Handlers;

use App\Containers\CrmPageContainer;
use App\Containers\CrmPagesContainer;
use App\Handler;
use App\Modules\Crm\Requests\GetCrmPagesRequest;
use App\Modules\Crm\Responses\GetCrmPagesResponse;
use App\Modules\Crm\Collections\CrmPagesCollection;

/**
 * Class GetCrmPagesHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do pobrania listy stron w Crm
 */
class GetCrmPagesHandler extends Handler
{
    public function __invoke(GetCrmPagesRequest $request): GetCrmPagesResponse
    {
        $pagesCollection = (new CrmPagesCollection)
            ->setPage($request->getPage())
            ->setLimit($request->getLimit())
            ->setSortName($request->getSortName())
            ->setSortKind($request->getSortKind())
            ->setFilters($request->getFilters())
            ->loadAll();

        $pages = (new CrmPagesContainer);
        foreach ($pagesCollection as $pageModel) {
            $pages->add(
                (new CrmPageContainer)
                    ->setId($pageModel->getUuid())
                    ->setTitle($pageModel->getTitle())
                    ->setUrl($pageModel->getUrl())
            );
        }

        return (new GetCrmPagesResponse)
            ->setPages($pages);
    }
}