<?php

namespace App\Modules\Crm\Handlers;

use App\Containers\CrmPageContainer;
use App\Containers\CrmPagesContainer;
use App\Handler;
use App\Modules\Crm\Models\CrmPagesModel;
use App\Modules\Crm\Requests\GetCrmPageRequest;
use App\Modules\Crm\Requests\GetCrmPagesRequest;
use App\Modules\Crm\Responses\GetCrmPageResponse;
use App\Modules\Crm\Responses\GetCrmPagesResponse;
use App\Modules\Crm\Collections\CrmPagesCollection;

/**
 * Class GetCrmPageHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do pobrania strony z Crm
 */
class GetCrmPageHandler extends Handler
{
    public function __invoke(GetCrmPageRequest $request): GetCrmPageResponse
    {
        $pageModel = (new CrmPagesModel)
            ->load($request->getId(), true);

        return (new GetCrmPageResponse)
            ->setId($pageModel->getUuid())
            ->setTitle($pageModel->getTitle())
            ->setUrl($pageModel->getUrl())
            ->setContent($pageModel->getContent());
    }
}