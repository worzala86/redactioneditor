<?php

namespace App\Modules\Crm\Handlers;

use App\Handler;
use App\Modules\Crm\Models\CrmPagesModel;
use App\Modules\Crm\Requests\UpdateCrmPageRequest;
use App\Modules\Crm\Responses\UpdateCrmPageResponse;

/**
 * Class UpdateCrmPageHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do aktualizacji strony w Crm
 */
class UpdateCrmPageHandler extends Handler
{
    public function __invoke(UpdateCrmPageRequest $request): UpdateCrmPageResponse
    {
        $return = (new CrmPagesModel)
            ->setUrl($request->getUrl())
            ->setTitle($request->getTitle())
            ->setContent($request->getContent())
            ->update($request->getId(), true);

        return (new UpdateCrmPageResponse)
            ->setSuccess($return);
    }
}