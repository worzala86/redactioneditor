<?php

namespace App\Modules\Crm\Handlers;

use App\Handler;
use App\Modules\Crm\Models\CrmPagesModel;
use App\Modules\Crm\Requests\CreateCrmPageRequest;
use App\Modules\Crm\Responses\CreateCrmPageResponse;
use App\Types\UUID;

/**
 * Class CreateCrmPageHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do zapisania strony w Crm
 */
class CreateCrmPageHandler extends Handler
{
    public function __invoke(CreateCrmPageRequest $request): CreateCrmPageResponse
    {
        $uuid = UUID::fake();
        (new CrmPagesModel)
            ->setUuid($uuid)
            ->setUrl($request->getUrl())
            ->setTitle($request->getTitle())
            ->setContent($request->getContent())
            ->insert();

        return (new CreateCrmPageResponse)
            ->setId($uuid);
    }
}