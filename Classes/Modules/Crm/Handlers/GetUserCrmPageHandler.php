<?php

namespace App\Modules\Crm\Handlers;

use App\Handler;
use App\Modules\Crm\Models\CrmPagesModel;
use App\Modules\Crm\Requests\GetUserCrmPageRequest;
use App\Modules\Crm\Responses\GetUserCrmPageResponse;

/**
 * Class GetUserCrmPageHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do pobrania strony z Crm
 */
class GetUserCrmPageHandler extends Handler
{
    public function __invoke(GetUserCrmPageRequest $request): GetUserCrmPageResponse
    {
        $pageModel = (new CrmPagesModel)
            ->where('url=?', $request->getUrl())
            ->load();

        return (new GetUserCrmPageResponse)
            ->setId($pageModel->getUuid())
            ->setTitle($pageModel->getTitle())
            ->setUrl($pageModel->getUrl())
            ->setContent($pageModel->getContent());
    }
}