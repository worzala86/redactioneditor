<?php

namespace App\Modules\Crm\Handlers;

use App\Handler;
use App\Modules\Crm\Models\CrmPagesModel;
use App\Modules\Crm\Requests\DeleteCrmPageRequest;
use App\Modules\Crm\Requests\UpdateCrmPageRequest;
use App\Modules\Crm\Responses\DeleteCrmPageResponse;
use App\Modules\Crm\Responses\UpdateCrmPageResponse;

/**
 * Class DeleteCrmPageHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do usuwania strony z Crm
 */
class DeleteCrmPageHandler extends Handler
{
    public function __invoke(DeleteCrmPageRequest $request): DeleteCrmPageResponse
    {
        $return = (new CrmPagesModel)
            ->delete($request->getId(), true);

        return (new DeleteCrmPageResponse)
            ->setSuccess($return);
    }
}