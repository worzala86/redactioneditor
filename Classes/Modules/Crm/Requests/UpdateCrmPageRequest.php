<?php

namespace App\Modules\Crm\Requests;

use App\AdminRequest;
use App\Types\Url;
use App\Types\UUID;

class UpdateCrmPageRequest extends AdminRequest
{
    private $id;
    private $url;
    private $title;
    private $content;

    /**
     * @param UUID $id
     * @description Identyfikator aktualizowanej strony
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param Url $url
     * @description Adres url strony
     * @return $this
     */
    public function setUrl(Url $url)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl(): Url
    {
        return $this->url;
    }

    /**
     * @param string $title
     * @description Tytuł strony
     * @return $this
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $content
     * @description Treść strony
     * @return $this
     */
    public function setContent(string $content)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}