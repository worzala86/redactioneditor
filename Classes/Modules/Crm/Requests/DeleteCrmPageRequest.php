<?php

namespace App\Modules\Crm\Requests;

use App\AdminRequest;
use App\Types\Url;
use App\Types\UUID;

class DeleteCrmPageRequest extends AdminRequest
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator usuwanej strony
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}