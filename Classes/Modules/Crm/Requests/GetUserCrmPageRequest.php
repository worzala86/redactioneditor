<?php

namespace App\Modules\Crm\Requests;

use App\Request;
use App\Types\Url;

class GetUserCrmPageRequest extends Request
{
    private $url;

    /**
     * @param Url $url
     * @description Url strony
     * @return $this
     */
    public function setUrl(Url $url)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl(): Url
    {
        return $this->url;
    }
}