<?php

namespace App\Modules\Crm\Requests;

use App\AdminRequest;
use App\Types\UUID;

class GetCrmPageRequest extends AdminRequest
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator strony
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}