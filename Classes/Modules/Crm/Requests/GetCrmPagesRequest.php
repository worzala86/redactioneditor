<?php

namespace App\Modules\Crm\Requests;

use App\AdminRequest;
use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;

class GetCrmPagesRequest extends AdminRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;
}