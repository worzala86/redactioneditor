<?php

namespace App\Modules\Crm\Requests;

use App\AdminRequest;
use App\Types\Url;

class CreateCrmPageRequest extends AdminRequest
{
    private $url;
    private $title;
    private $content;

    /**
     * @param Url $url
     * @description Adres url strony
     * @return $this
     */
    public function setUrl(Url $url)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl(): Url
    {
        return $this->url;
    }

    /**
     * @param string $title
     * @description Tytuł strony
     * @return $this
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $content
     * @description Treść strony
     * @return $this
     */
    public function setContent(string $content)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}