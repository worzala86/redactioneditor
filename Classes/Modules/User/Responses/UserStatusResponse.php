<?php

namespace App\Modules\User\Responses;

use App\Response;
use App\Types\UUID;

class UserStatusResponse extends Response
{
    private $logged;
    private $isAdmin;
    private $projectsCount;
    private $firstName;
    private $lastName;
    private $activeProject;

    /**
     * @param UUID $activeProject
     * @description Identyfikator aktywnego projektu
     * @return $this
     */
    public function setActiveProject(UUID $activeProject = null)
    {
        $this->activeProject = $activeProject;
        return $this;
    }

    public function getActiveProject(): ?UUID
    {
        return $this->activeProject;
    }

    /**
     * @param bool $logged
     * @description Wartość informuje o tym czy użytkownik w sessjii jest zalogowany
     * @return $this
     */
    public function setLogged(bool $logged)
    {
        $this->logged = $logged;
        return $this;
    }

    public function getLogged(): bool
    {
        return $this->logged;
    }

    /**
     * @param bool $isAdmin
     * @description Wartość informuje o tym czy użytkownik jest administratorem
     * @return $this
     */
    public function setIsAdmin(bool $isAdmin)
    {
        $this->isAdmin = $isAdmin;
        return $this;
    }

    public function getIsAdmin(): bool
    {
        return $this->isAdmin;
    }

    /**
     * @param int $projectsCount
     * @description Liczba projektów użytkownika
     * @return $this
     */
    public function setProjectsCount(int $projectsCount)
    {
        $this->projectsCount = $projectsCount;
        return $this;
    }

    public function getProjectsCount(): int
    {
        return $this->projectsCount;
    }

    /**
     * @param string $firstName
     * @description Imię użytkownika
     * @return $this
     */
    public function setFirstName(string $firstName = null)
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     * @description Nazwisko użytkownika
     * @return $this
     */
    public function setLastName(string $lastName = null)
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }
}