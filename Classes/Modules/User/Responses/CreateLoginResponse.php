<?php

namespace App\Modules\User\Responses;

use App\Response;

class CreateLoginResponse extends Response
{
    private $redirect;

    /**
     * @param string $success
     * @description Zwrotne przekierowanie
     * @return $this
     */
    public function setRedirect(string $redirect){
        $this->redirect = $redirect;
        return $this;
    }

    public function getRedirect(): string {
        return $this->redirect;
    }
}