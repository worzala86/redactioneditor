<?php

namespace App\Modules\User\Responses;

use App\Response;

class NewPasswordResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy  nowe hasło zostało zapisane
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}