<?php

namespace App\Modules\User\Responses;

use App\Response;

class ActivateAccountResponse extends Response
{
    private $success;
    private $accountIsActive;

    /**
     * @param bool $accountIsActive
     * @description Wartość informuje o tym czy konto jest już aktywowane
     * @return $this
     */
    public function setAccountIsActive(bool $accountIsActive){
        $this->accountIsActive = $accountIsActive;
        return $this;
    }

    public function getAccountIsActive(): bool {
        return $this->accountIsActive;
    }

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy aktywacja przebiegła pomyślnie
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}