<?php

namespace App\Modules\User\Responses;

use App\Response;

class GetProfileResponse extends Response
{
    private $firstName;
    private $lastName;

    /**
     * @param string $firstName
     * @description Imię użytkownika
     * @return $this
     */
    public function setFirstName(string $firstName = null)
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     * @description Nazwisko użytkownika
     * @return $this
     */
    public function setLastName(string $lastName = null)
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }
}