<?php

namespace App\Modules\User\Responses;

use App\Response;

class RegisterUserResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy rejestracja przebiegła pomyślnie
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}