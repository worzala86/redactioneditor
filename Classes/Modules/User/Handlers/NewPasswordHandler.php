<?php

namespace App\Modules\User\Handlers;

use App\Handler;
use App\Modules\User\Models\UsersModel;
use App\Modules\User\Requests\NewPasswordRequest;
use App\Modules\User\Responses\NewPasswordResponse;

/**
 * Class NewPasswordHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do zapisu nowego hasła w serwisie
 */

class NewPasswordHandler extends Handler
{
    public function __invoke(NewPasswordRequest $request): NewPasswordResponse
    {
        $user = (new UsersModel)
            ->where('`reset`=?', $request->getCode())
            ->load();

        if($user->isLoaded()){
            $user->setPassword($request->getPassword())
                ->update();
        }

        return (new NewPasswordResponse)
            ->setSuccess(($user->isLoaded())?true:false);
    }
}