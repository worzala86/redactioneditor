<?php

namespace App\Modules\User\Handlers;

use App\Handler;
use App\Modules\Projects\Exceptions\TheSamePasswordException;
use App\Modules\Projects\Exceptions\WrongPasswordException;
use App\Modules\User\Models\UsersModel;
use App\Modules\User\Requests\GetProfileRequest;
use App\Modules\User\Requests\UpdatePasswordRequest;
use App\Modules\User\Requests\UpdateProfileRequest;
use App\Modules\User\Responses\GetProfileResponse;
use App\Modules\User\Responses\UpdatePasswordResponse;
use App\Modules\User\Responses\UpdateProfileResponse;

/**
 * Class UpdatePasswordHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do zmiany hasła użytkownika
 */
class UpdatePasswordHandler extends Handler
{
    public function __invoke(UpdatePasswordRequest $request): UpdatePasswordResponse
    {
        if($request->getOldPassword()==$request->getNewPassword()){
            throw new TheSamePasswordException;
        }

        $user = (new UsersModel)
            ->where('password=?', $request->getOldPassword())
            ->load($request->getCurrentUserId());

        if(!$user->isLoaded()){
            throw new WrongPasswordException;
        }

        $updated = (new UsersModel)
            ->setPassword($request->getNewPassword())
            ->update($request->getCurrentUserId());

        return (new UpdatePasswordResponse)
            ->setSuccess($updated);
    }
}