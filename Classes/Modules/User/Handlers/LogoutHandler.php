<?php

namespace App\Modules\User\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\User\Requests\LoginRequest;
use App\Modules\User\Responses\LogoutResponse;
use App\Types\SESSID;

/**
 * Class LogoutHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do logowania w serwisie
 */
class LogoutHandler extends Handler
{
    public function __invoke(LoginRequest $request): LogoutResponse
    {
        DB::get()->execute('update `session` set deleted=? where sessid=?', [time(), hex2bin(session_id())]);

        session_id(SESSID::fake());
        $_COOKIE[session_name()] = null;
        $_SESSION['user_id'] = null;

        return (new LogoutResponse)
            ->setSuccess(true);
    }
}