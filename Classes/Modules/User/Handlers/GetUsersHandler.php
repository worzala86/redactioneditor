<?php

namespace App\Modules\User\Handlers;

use App\Containers\UserContainer;
use App\Containers\UsersContainer;
use App\Database\DB;
use App\Handler;
use App\Modules\User\Requests\GetUsersRequest;
use App\Modules\User\Responses\GetUsersResponse;
use App\Modules\User\Collections\UsersCollection;
use App\Types\Time;

/**
 * Class GetUsersHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do pobrania listy użytkowników
 */
class GetUsersHandler extends Handler
{
    public function __invoke(GetUsersRequest $request): GetUsersResponse
    {
        $usersCollection = (new UsersCollection)
            ->setFilters($request->getFilters())
            ->loadAll();

        $users = new UsersContainer;
        foreach ($usersCollection as $userModel) {
            $lastSeen = DB::get()->getOne('select access from `session` where user_id=? order by access desc limit 1', $userModel->getId());
            $users->add(
                (new UserContainer)
                    ->setId($userModel->getUuid())
                    ->setMail($userModel->getMail())
                    ->setFirstName($userModel->getFirstName())
                    ->setLastName($userModel->getLastName())
                    ->setJoinDate($userModel->getAdded())
                    ->setLastSeen($lastSeen?new Time($lastSeen):null)
            );
        }

        return (new GetUsersResponse)
            ->setUsers($users);
    }
}