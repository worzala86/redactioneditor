<?php

namespace App\Modules\User\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Projects\Models\ProjectsModel;
use App\Modules\User\Responses\UserStatusResponse;
use App\Request;

/**
 * Class UserStatusHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do pobrania statusu użytkownika.
 */
class UserStatusHandler extends Handler
{
    public function __invoke(Request $request): UserStatusResponse
    {
        $projectsCount = 0;
        if ($request->getCurrentUserId()) {
            $projectsCount = DB::get()->getOne('select count(*) from projects where user_id=? and deleted=0',
                $request->getCurrentUserId());
        }

        $user = $request->getUser();

        $activeProjectUuid = null;
        if ($user) {
            $activeProject = (new ProjectsModel)->load($user->getActiveProjectId());
            if ($activeProject->isLoaded()) {
                $activeProjectUuid = $activeProject->getUuid();
            }
        }

        return (new UserStatusResponse)
            ->setLogged(($request->getCurrentUserId() > 0) ? true : false)
            ->setIsAdmin($user ? $user->isAdmin() : false)
            ->setProjectsCount($projectsCount)
            ->setFirstName($user ? $user->getFirstName() : null)
            ->setLastName($user ? $user->getLastName() : null)
            ->setActiveProject($activeProjectUuid);
    }
}