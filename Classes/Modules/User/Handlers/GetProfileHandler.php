<?php

namespace App\Modules\User\Handlers;

use App\Handler;
use App\Modules\User\Requests\GetProfileRequest;
use App\Modules\User\Responses\GetProfileResponse;
/**
 * Class GetProfileHandler
 * @package App\Modules\User\Handlers
 * @description Metoda służy do pobrania danych użytkownika.
 */
class GetProfileHandler extends Handler
{
    public function __invoke(GetProfileRequest $request): GetProfileResponse
    {
        $user = $request->getUser();

        return (new GetProfileResponse)
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName());
    }
}