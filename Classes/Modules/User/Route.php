<?php

namespace App;

use App\Modules\User\Handlers;
use App\Types\UUID;

return [
    ['method'=>'post', 'url'=>'api/register', 'handler'=>Handlers\RegisterUserHandler::class],
    ['method'=>'get', 'url'=>'api/activate-account/:code', 'handler'=>Handlers\ActivateAccountHandler::class, 'regex'=>['code'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/login', 'handler'=>Handlers\CreateLoginHandler::class],
    ['method'=>'get', 'url'=>'api/user-status', 'handler'=>Handlers\UserStatusHandler::class],
    ['method'=>'get', 'url'=>'api/logout', 'handler'=>Handlers\LogoutHandler::class],
    ['method'=>'post', 'url'=>'api/reset-password', 'handler'=>Handlers\ResetPasswordHandler::class],
    ['method'=>'post', 'url'=>'api/new-password', 'handler'=>Handlers\NewPasswordHandler::class],
    ['method'=>'get', 'url'=>'api/users', 'handler'=>Handlers\GetUsersHandler::class],
    ['method'=>'get', 'url'=>'api/profil', 'handler'=>Handlers\GetProfileHandler::class],
    ['method'=>'post', 'url'=>'api/profil', 'handler'=>Handlers\UpdateProfileHandler::class],
    ['method'=>'post', 'url'=>'api/password', 'handler'=>Handlers\UpdatePasswordHandler::class],
];