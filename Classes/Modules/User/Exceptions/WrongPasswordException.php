<?php

namespace App\Modules\Projects\Exceptions;

use App\NamedException;

class WrongPasswordException extends \Exception implements NamedException
{
}