<?php

namespace App\Modules\Projects\Exceptions;

use App\NamedException;

class TheSamePasswordException extends \Exception implements NamedException
{
}