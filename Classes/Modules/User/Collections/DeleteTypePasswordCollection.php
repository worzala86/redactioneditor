<?php

namespace App\Modules\User\Collections;

use App\CollectionTrait;
use App\FiltersTrait;
use App\Modules\User\Models\UsersModel;

class DeleteTypePasswordCollection extends UsersModel implements \Iterator
{
    use CollectionTrait;
}