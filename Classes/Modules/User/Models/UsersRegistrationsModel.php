<?php

namespace App\Modules\User\Models;

use App\Model;
use App\Types\Mail;
use App\Types\Password;
use App\Types\UUID;

class UsersRegistrationsModel extends Model
{
    private $mail;
    private $password;
    private $newsletter;
    private $code;
    private $uuid;
    private $firstName;
    private $lastName;

    public function setLastName(string $lastName)
    {
        $this->set('last_name', $lastName);
        $this->lastName = $lastName;
        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setFirstName(string $firstName)
    {
        $this->set('first_name', $firstName);
        $this->firstName = $firstName;
        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', $uuid);
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setCode(UUID $code)
    {
        $this->set('code', $code);
        $this->code = $code;
        return $this;
    }

    public function getCode(): UUID
    {
        return $this->code;
    }

    public function setMail(Mail $mail)
    {
        $this->set('mail', $mail);
        $this->mail = $mail;
        return $this;
    }

    public function getMail(): Mail
    {
        return $this->mail;
    }

    public function setPassword(Password $password)
    {
        $this->set('password', $password);
        $this->password = $password;
        return $this;
    }

    public function getPassword(): Password
    {
        return $this->password;
    }

    public function setNewsletter(bool $newsletter)
    {
        $this->set('newsletter', $newsletter);
        $this->newsletter = $newsletter;
        return $this;
    }

    public function getNewsletter(): bool
    {
        return $this->newsletter;
    }
}