<?php

namespace App\Modules\User\Requests;

use App\Types\Password;
use App\UserRequest;

class UpdatePasswordRequest extends UserRequest
{
    private $oldPassword;
    private $newPassword;

    /**
     * @param Password $oldPassword
     * @description Stare hasło
     * @return $this
     */
    public function setOldPassword(Password $oldPassword)
    {
        $this->oldPassword = $oldPassword;
        return $this;
    }

    public function getOldPassword(): Password
    {
        return $this->oldPassword;
    }

    /**
     * @param Password $newPassword
     * @description Nowe hasło
     * @return $this
     */
    public function setNewPassword(Password $newPassword)
    {
        $this->newPassword = $newPassword;
        return $this;
    }

    public function getNewPassword(): Password
    {
        return $this->newPassword;
    }
}