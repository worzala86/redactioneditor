<?php

namespace App\Modules\User\Requests;

use App\Request;
use App\Types\Mail;
use App\Types\Password;

class CreateLoginRequest extends Request
{
    private $mail;
    private $password;

    /**
     * @param Mail $mail
     * @description Adres mailowy użytkownika - służy do zalogowania
     * @return $this
     */
    public function setMail(Mail $mail = null)
    {
        $this->mail = $mail;
        return $this;
    }

    public function getMail(): Mail
    {
        return $this->mail;
    }

    /**
     * @param Password $password
     * @description Hasło zakodowane w SHA512
     * @return $this
     */
    public function setPassword(Password $password = null)
    {
        $this->password = $password;
        return $this;
    }

    public function getPassword(): Password
    {
        return $this->password;
    }
}