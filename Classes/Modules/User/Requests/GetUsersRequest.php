<?php

namespace App\Modules\User\Requests;

use App\AdminRequest;
use App\FiltersTrait;

class GetUsersRequest extends AdminRequest
{
    use FiltersTrait;
}