<?php

namespace App\Modules\User\Requests;

use App\Request;
use App\Types\Mail;
use App\Types\Password;

class RegisterUserRequest extends Request
{
    private $mail;
    private $password;
    private $newsletter;
    private $firstName;

    /**
     * @param Mail $mail
     * @description Adres mailowy użytkownika, na ten adres będzie wysłany kod z potwierdzeniem rejestracjii. Adres mailowy może zawierać tylko literki, cyfry i kropkę. Musi zawierać znak @ i mieć poprawną domenę. Format adresu to [a-zA-Z0-9.]+@[a-zA-Z0-9.]+.[a-zA-Z0-9.]+
     * @return $this
     */
    public function setMail(Mail $mail = null)
    {
        $this->mail = $mail;
        return $this;
    }

    public function getMail(): Mail
    {
        return $this->mail;
    }

    /**
     * @param Password $password
     * @description Hasło zakodowane w SHA512
     * @return $this
     */
    public function setPassword(Password $password = null)
    {
        $this->password = $password;
        return $this;
    }

    public function getPassword(): Password
    {
        return $this->password;
    }

    /**
     * @param bool $newsletter
     * @description Wartość określa czy użytkownik chce otrzymywać newsletter
     * @return $this
     */
    public function setNewsletter(bool $newsletter)
    {
        $this->newsletter = $newsletter;
        return $this;
    }

    public function getNewsletter(): bool
    {
        return $this->newsletter;
    }

    /**
     * @param string $firstName
     * @description Imię użytkownika
     * @return $this
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     * @description Nazwisko użytkownika
     * @return $this
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}