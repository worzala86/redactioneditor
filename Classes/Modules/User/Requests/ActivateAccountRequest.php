<?php

namespace App\Modules\User\Requests;

use App\Request;
use App\Types\UUID;

class ActivateAccountRequest extends Request
{
    private $code;

    /**
     * @param UUID $code
     * @description Parametrem jest kod aktywacyjny otrzymany na maila podanego przy rejestracjii
     * @return $this
     */
    public function setCode(UUID $code = null)
    {
        $this->code = $code;
        return $this;
    }

    public function getCode(): UUID
    {
        return $this->code;
    }
}