<?php

namespace App\Modules\User\Requests;

use App\Types\Password;
use App\UserRequest;

class DeleteTypePasswordRequest extends UserRequest
{
    private $search;

    /**
     * @param DeleteTypePasswordRequest $search
     * @description Nowe słowo
     * @return $this
     */
    public function setSearch(DeleteTypePasswordRequest $search)
    {
        $this->search = $search;
        return $this;
    }

    public function getSearch(): DeleteTypePasswordRequest
    {
        return $this->search;
    }
}