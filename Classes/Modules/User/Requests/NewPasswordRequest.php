<?php

namespace App\Modules\User\Requests;

use App\Request;
use App\Types\Password;
use App\Types\UUID;

class NewPasswordRequest extends Request
{
    private $password;
    private $code;

    /**
     * @param Password $password
     * @description Hasło zakodowane w SHA512
     * @return $this
     */
    public function setPassword(Password $password = null)
    {
        $this->password = $password;
        return $this;
    }

    public function getPassword(): Password
    {
        return $this->password;
    }

    /**
     * @param UUID $code
     * @description Kod otrzymany w mailu do zresetowania hasła
     * @return $this
     */
    public function setCode(UUID $code = null)
    {
        $this->code = $code;
        return $this;
    }

    public function getCode(): UUID
    {
        return $this->code;
    }
}