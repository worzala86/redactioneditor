<?php

namespace App\Modules\Datasets\Responses;

use App\Response;

class GetDatasetDataByNameResponse extends Response
{
    private $name;
    private $dataset;
    private $totalCount;

    /**
     * @param string $name
     * @description Nazwa dataseta
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param array $dataset
     * @description Zawartość dataseta
     * @return $this
     */
    public function setDataset(array $dataset)
    {
        $this->dataset = $dataset;
        return $this;
    }

    public function getDataset(): array
    {
        return $this->dataset;
    }

    /**
     * @param int $totalCount
     * @description Całkowita liczba wierszy w datasecie
     * @return $this
     */
    public function setTotalCount(int $totalCount)
    {
        $this->totalCount = $totalCount;
        return $this;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }
}