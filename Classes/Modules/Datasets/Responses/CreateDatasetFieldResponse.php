<?php

namespace App\Modules\Datasets\Responses;

use App\Response;
use App\Types\UUID;

class CreateDatasetFieldResponse extends Response
{
    private $id;

    /**
     * @param UUID $id
     * @description Pole zawiera identyfikator nowego pola z dataseta
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}