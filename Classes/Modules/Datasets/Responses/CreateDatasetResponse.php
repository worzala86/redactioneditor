<?php

namespace App\Modules\Datasets\Responses;

use App\Response;
use App\Types\DatasetFieldName;
use App\Types\DatasetName;
use App\Types\UUID;

class CreateDatasetResponse extends Response
{
    private $id;
    private $name;

    /**
     * @param UUID $id
     * @description Pole zawiera identyfikator nowego dataseta
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param DatasetName $name
     * @description Pole zawiera nazwę dodanego dataseta
     * @return $this
     */
    public function setName(DatasetName $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): DatasetName
    {
        return $this->name;
    }
}