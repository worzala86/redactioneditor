<?php

namespace App\Modules\Datasets\Responses;

use App\Response;

class DeleteDatasetFieldResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy pomyślnie usnięto pole dataseta
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}