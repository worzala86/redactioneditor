<?php

namespace App\Modules\Datasets\Responses;

use App\Containers\DatasetsContainer;
use App\Response;

class GetDatasetsResponse extends Response
{
    private $datasets;

    /**
     * @param DatasetsContainer $datasets
     * @description Pole zawiera listę datasetów
     * @return $this
     */
    public function setDatasets(DatasetsContainer $datasets)
    {
        $this->datasets = $datasets;
        return $this;
    }

    public function getDatasets(): DatasetsContainer
    {
        return $this->datasets;
    }
}