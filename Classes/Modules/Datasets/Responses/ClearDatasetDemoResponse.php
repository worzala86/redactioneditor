<?php

namespace App\Modules\Datasets\Responses;

use App\Response;

class ClearDatasetDemoResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy pomyślnie wyczyszczono dane demo
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}