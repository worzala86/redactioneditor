<?php

namespace App\Modules\Datasets\Workers;

use App\Database\DB;

class CreateDataSetsShemaWorker
{
    public function create(int $userId, int $projectId){
        $usersForeignsCreate = false;
        $ipsForeignsCreate = false;
        $sessionForeignsCreate = false;

        DB::get('REDACTION_DATASETS')->execute('start transaction');

        $tableUsersName = 'datasets_' . $userId . '_' . $projectId . '_users';
        if(!DB::get('REDACTION_DATASETS')->getOne('SELECT 1 FROM information_schema.tables WHERE table_schema=? 
              and table_name=?', [DATABASE_CONNECTION['REDACTION_DATASETS']['DATABASE'], $tableUsersName])) {
            $usersForeignsCreate = true;
            DB::get('REDACTION_DATASETS')->execute('CREATE TABLE `' . $tableUsersName . '` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `mail` varchar(90) DEFAULT NULL,
              `uuid` binary(8) NOT NULL,
              `added` int(10) unsigned NOT NULL,
              `added_by` int(10) unsigned DEFAULT NULL,
              `added_ip_id` int(10) unsigned NOT NULL,
              `deleted` int(10) unsigned NOT NULL DEFAULT \'0\',
              `deleted_by` int(10) unsigned DEFAULT NULL,
              `deleted_ip_id` int(10) unsigned DEFAULT NULL,
              `updated` int(10) unsigned DEFAULT NULL,
              `updated_by` int(10) unsigned DEFAULT NULL,
              `updated_ip_id` int(10) unsigned DEFAULT NULL,
              `password` varchar(128) DEFAULT NULL,
              `reset` varchar(16) DEFAULT NULL,
              `admin` tinyint(1) DEFAULT \'0\',
              `first_name` varchar(250) DEFAULT NULL,
              `last_name` varchar(250) DEFAULT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `uuid` (`uuid`),
              UNIQUE KEY `email` (`mail`,`deleted`)
            )');
        }

        $tableIpName = 'datasets_' . $userId . '_' . $projectId . '_ip';
        if(!DB::get('REDACTION_DATASETS')->getOne('SELECT 1 FROM information_schema.tables WHERE table_schema=? 
              and table_name=?', [DATABASE_CONNECTION['REDACTION_DATASETS']['DATABASE'], $tableIpName])) {
            $ipsForeignsCreate = true;
            DB::get('REDACTION_DATASETS')->execute('CREATE TABLE `' . $tableIpName . '` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `ip` int(10) unsigned NOT NULL,
              `user_id` int(10) unsigned DEFAULT NULL,
              `date` int(11) DEFAULT NULL,
              `session_id` int(10) unsigned DEFAULT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `ip` (`ip`,`user_id`)
            )');
        }

        $tableSessionName = 'datasets_' . $userId . '_' . $projectId . '_session';
        if(!DB::get('REDACTION_DATASETS')->getOne('SELECT 1 FROM information_schema.tables WHERE table_schema=? 
              and table_name=?', [DATABASE_CONNECTION['REDACTION_DATASETS']['DATABASE'], $tableSessionName])) {
            $sessionForeignsCreate = true;
            DB::get('REDACTION_DATASETS')->execute('CREATE TABLE `' . $tableSessionName . '` (
              `sessid` binary(16) NOT NULL,
              `access` int(10) unsigned DEFAULT NULL,
              `data` text,
              `user_id` int(10) unsigned DEFAULT NULL,
              `deleted` int(10) unsigned NOT NULL DEFAULT \'0\',
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              PRIMARY KEY (`id`),
              UNIQUE KEY `sessid` (`sessid`)
            )');
        }

        if($usersForeignsCreate) {
            DB::get('REDACTION_DATASETS')->execute('alter table ' . $tableUsersName . ' add CONSTRAINT `fk_' . $tableUsersName . '_added_by` FOREIGN KEY (`added_by`) REFERENCES `' . $tableUsersName . '` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;');
            DB::get('REDACTION_DATASETS')->execute('alter table ' . $tableUsersName . ' add CONSTRAINT `fk_' . $tableUsersName . '_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `' . $tableIpName . '` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;');
            DB::get('REDACTION_DATASETS')->execute('alter table ' . $tableUsersName . ' add CONSTRAINT `fk_' . $tableUsersName . '_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `' . $tableUsersName . '` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;');
            DB::get('REDACTION_DATASETS')->execute('alter table ' . $tableUsersName . ' add CONSTRAINT `fk_' . $tableUsersName . '_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `' . $tableIpName . '` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;');
            DB::get('REDACTION_DATASETS')->execute('alter table ' . $tableUsersName . ' add CONSTRAINT `fk_' . $tableUsersName . '_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `' . $tableUsersName . '` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;');
            DB::get('REDACTION_DATASETS')->execute('alter table ' . $tableUsersName . ' add CONSTRAINT `fk_' . $tableUsersName . '_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `' . $tableIpName . '` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;');
        }
        if($ipsForeignsCreate) {
            DB::get('REDACTION_DATASETS')->execute('alter table ' . $tableIpName . ' add CONSTRAINT `fk_' . $tableIpName . '_user_id` FOREIGN KEY (`user_id`) REFERENCES `' . $tableUsersName . '` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;');
            DB::get('REDACTION_DATASETS')->execute('alter table ' . $tableIpName . ' add CONSTRAINT `fk_' . $tableIpName . '_session_id` FOREIGN KEY (`session_id`) REFERENCES `'.$tableSessionName.'` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;');
        }
        if($sessionForeignsCreate) {
            DB::get('REDACTION_DATASETS')->execute('alter table ' . $tableSessionName . ' add CONSTRAINT `fk_' . $tableSessionName . '_user_id` FOREIGN KEY (`user_id`) REFERENCES `' . $tableUsersName . '` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;');
        }

        DB::get('REDACTION_DATASETS')->execute('commit');
    }
}