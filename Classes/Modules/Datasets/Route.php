<?php

namespace App;

use App\Modules\Datasets\Handlers;
use App\Types\DatasetName;
use App\Types\UUID;

return [
    ['method'=>'get', 'url'=>'api/projects/:projectId/datasets', 'handler'=>Handlers\GetDatasetsHandler::class, 'regex'=>['projectId'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/projects/:projectId/datasets', 'handler'=>Handlers\CreateDatasetHandler::class, 'regex'=>['projectId'=>UUID::$pattern]],
    ['method'=>'delete', 'url'=>'api/projects/:projectId/datasets/:id', 'handler'=>Handlers\DeleteDatasetHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
    ['method'=>'get', 'url'=>'api/projects/:projectId/datasets/:id', 'handler'=>Handlers\GetDatasetDataHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
    ['method'=>'get', 'url'=>'api/projects/:projectId/datasets/selector/:name', 'handler'=>Handlers\GetDatasetDataByNameHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'name'=>DatasetName::$pattern]],
    ['method'=>'post', 'url'=>'api/projects/:projectId/datasets/:id/fields', 'handler'=>Handlers\CreateDatasetFieldHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
    ['method'=>'put', 'url'=>'api/projects/:projectId/datasets/:id/demo', 'handler'=>Handlers\CreateDatasetDemoHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
    ['method'=>'delete', 'url'=>'api/projects/:projectId/datasets/:id/demo', 'handler'=>Handlers\ClearDatasetDemoHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
    ['method'=>'delete', 'url'=>'api/projects/:projectId/datasets/:datasetId/fields/:id', 'handler'=>Handlers\DeleteDatasetFieldHandler::class, 'regex'=>['projectId'=>UUID::$pattern, 'datasetId'=>UUID::$pattern, 'id'=>UUID::$pattern]],
];