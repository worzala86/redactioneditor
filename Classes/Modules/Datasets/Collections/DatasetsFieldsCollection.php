<?php

namespace App\Modules\Datasets\Collections;

use App\CollectionTrait;
use App\Modules\Datasets\Models\DatasetsFieldsModel;

class DatasetsFieldsCollection extends DatasetsFieldsModel implements \Iterator
{
    use CollectionTrait;
}