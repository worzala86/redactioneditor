<?php

namespace App\Modules\Datasets\Collections;

use App\CollectionTrait;
use App\Modules\Datasets\Models\DatasetsModel;

class DatasetsCollection extends DatasetsModel implements \Iterator
{
    use CollectionTrait;
}