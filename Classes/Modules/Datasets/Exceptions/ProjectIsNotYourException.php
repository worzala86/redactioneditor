<?php

namespace App\Modules\Datasets\Exceptions;

use App\NamedException;

class ProjectIsNotYourException extends \Exception implements NamedException
{
}