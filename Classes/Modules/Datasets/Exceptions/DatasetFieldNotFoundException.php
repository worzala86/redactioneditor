<?php

namespace App\Modules\Datasets\Exceptions;

use App\NamedException;

class DatasetFieldNotFoundException extends \Exception implements NamedException
{
}