<?php

namespace App\Modules\Datasets\Exceptions;

use App\NamedException;

class DatasetNotFoundException extends \Exception implements NamedException
{
}