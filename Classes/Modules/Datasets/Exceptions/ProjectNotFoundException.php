<?php

namespace App\Modules\Datasets\Exceptions;

use App\NamedException;

class ProjectNotFoundException extends \Exception implements NamedException
{
}