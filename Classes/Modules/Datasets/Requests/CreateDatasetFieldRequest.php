<?php

namespace App\Modules\Datasets\Requests;

use App\Types\DatasetFieldKind;
use App\Types\DatasetFieldName;
use App\Types\UUID;
use App\UserRequest;

class CreateDatasetFieldRequest extends UserRequest
{
    private $id;
    private $projectId;
    private $name;
    private $type;

    /**
     * @param UUID $projectId
     * @description Identyfikator dataseta
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param UUID $projectId
     * @description Identyfikator pola dataseta
     * @return $this
     */
    public function setProjectId(UUID $projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): UUID
    {
        return $this->projectId;
    }

    /**
     * @param DatasetFieldName $name
     * @description Nazwa pola dataseta
     * @return $this
     */
    public function setName(DatasetFieldName $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): DatasetFieldName
    {
        return $this->name;
    }

    /**
     * @param DatasetFieldKind $type
     * @description Rodzaj pola datasta np. textarea
     * @return $this
     */
    public function setType(DatasetFieldKind $type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType(): DatasetFieldKind
    {
        return $this->type;
    }
}