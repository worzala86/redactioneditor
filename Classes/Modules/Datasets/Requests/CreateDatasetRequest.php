<?php

namespace App\Modules\Datasets\Requests;

use App\Types\DatasetName;
use App\Types\UUID;
use App\UserRequest;

class CreateDatasetRequest extends UserRequest
{
    private $projectId;
    private $name;

    /**
     * @param UUID $projectId
     * @description Identyfikator dataseta
     * @return $this
     */
    public function setProjectId(UUID $projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): UUID
    {
        return $this->projectId;
    }

    /**
     * @param DatasetName $name
     * @description Nazwa dataseta
     * @return $this
     */
    public function setName(DatasetName $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): DatasetName
    {
        return $this->name;
    }
}