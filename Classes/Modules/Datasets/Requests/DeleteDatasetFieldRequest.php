<?php

namespace App\Modules\Datasets\Requests;

use App\Types\UUID;
use App\UserRequest;

class DeleteDatasetFieldRequest extends UserRequest
{
    private $projectId;
    private $datasetId;
    private $id;

    /**
     * @param UUID $projectId
     * @description Identyfikator projektu
     * @return $this
     */
    public function setProjectId(UUID $projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): UUID
    {
        return $this->projectId;
    }

    /**
     * @param UUID $datasetId
     * @description Identyfikator dataseta
     * @return $this
     */
    public function setDatasetId(UUID $datasetId)
    {
        $this->datasetId = $datasetId;
        return $this;
    }

    public function getDatasetId(): UUID
    {
        return $this->datasetId;
    }

    /**
     * @param UUID $id
     * @description Identyfikator pola dataseta
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}