<?php

namespace App\Modules\Datasets\Requests;

use App\Types\UUID;
use App\UserRequest;

class ClearDatasetDemoRequest extends UserRequest
{
    private $projectId;
    private $id;

    /**
     * @param UUID $projectId
     * @description Identyfikator projektu
     * @return $this
     */
    public function setProjectId(UUID $projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): UUID
    {
        return $this->projectId;
    }

    /**
     * @param UUID $id
     * @description Identyfikator datasetu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}