<?php

namespace App\Modules\Datasets\Requests;

use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;
use App\Types\UUID;
use App\UserRequest;

class GetDatasetsRequest extends UserRequest
{
    use PaginationTrait;
    use FiltersTrait;
    use SortTrait;

    private $projectId;

    /**
     * @param UUID $projectId
     * @description Identyfikator projektu
     * @return $this
     */
    public function setProjectId(UUID $projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): UUID
    {
        return $this->projectId;
    }
}