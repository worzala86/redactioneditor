<?php

namespace App\Modules\Datasets\Models;

use App\Model;
use App\Types\DatasetFieldKind;
use App\Types\DatasetFieldName;
use App\Types\UUID;

class DatasetsFieldsModel extends Model
{
    private $id;
    private $uuid;
    private $name;
    private $type;
    private $datasetsId;

    public function setDatasetsId(int $datasetsId)
    {
        $this->set('datasets_id', $datasetsId);
        $this->datasetsId = $datasetsId;
        return $this;
    }

    public function getDatasetsId(): int
    {
        return $this->datasetsId;
    }

    public function setType(DatasetFieldKind $type)
    {
        $this->set('type', $type);
        $this->type = $type;
        return $this;
    }

    public function getType(): DatasetFieldKind
    {
        return $this->type;
    }

    public function setName(DatasetFieldName $name)
    {
        $this->set('name', $name);
        $this->name = $name;
        return $this;
    }

    public function getName(): DatasetFieldName
    {
        return $this->name;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}