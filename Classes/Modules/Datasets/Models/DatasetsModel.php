<?php

namespace App\Modules\Datasets\Models;

use App\Model;
use App\Types\DatasetName;
use App\Types\UUID;

class DatasetsModel extends Model
{
    private $id;
    private $uuid;
    private $projectId;
    private $name;
    private $userId;

    public function setUserId(int $userId)
    {
        $this->set('user_id', $userId);
        $this->userId = $userId;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setName(DatasetName $name)
    {
        $this->set('name', $name);
        $this->name = $name;
        return $this;
    }

    public function getName(): DatasetName
    {
        return $this->name;
    }

    public function setProjectId(int $projectId)
    {
        $this->set('project_id', $projectId);
        $this->projectId = $projectId;
        return $this;
    }

    public function getProjectId(): int
    {
        return $this->projectId;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}