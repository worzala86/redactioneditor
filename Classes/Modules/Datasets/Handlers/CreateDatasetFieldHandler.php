<?php

namespace App\Modules\Datasets\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Datasets\Exceptions\DatasetNotFoundException;
use App\Modules\Datasets\Exceptions\ProjectNotFoundException;
use App\Modules\Datasets\Models\DatasetsFieldsModel;
use App\Modules\Datasets\Models\DatasetsModel;
use App\Modules\Datasets\Requests\CreateDatasetFieldRequest;
use App\Modules\Datasets\Responses\CreateDatasetFieldResponse;
use App\Modules\Projects\Models\ProjectsModel;
use App\Types\UUID;

/**
 * Class CreateDatasetFieldHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do zapisu pola dataseta
 */
class CreateDatasetFieldHandler extends Handler
{
    public function __invoke(CreateDatasetFieldRequest $request): CreateDatasetFieldResponse
    {
        (new ProjectsModel)->startTransaction();

        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $datasetsModel = (new DatasetsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where(' `project_id`=? ', $projectsModel->getId())
            ->load($request->getId(), true);

        if (!$datasetsModel->isLoaded()) {
            throw new DatasetNotFoundException;
        }

        $uuid = UUID::fake();
        (new DatasetsFieldsModel)
            ->setUuid($uuid)
            ->setDatasetsId($datasetsModel->getId())
            ->setName($request->getName())
            ->setType($request->getType())
            ->insert();

        $type = ' varchar(250) ';
        if($request->getType()=='text'){
            $type = ' varchar(250) ';
        }else if($request->getType()=='textarea'){
            $type = ' text ';
        }else if($request->getType()=='image'){
            $type = ' varchar(250) ';
        }
        $tableName = 'datasets_' . $request->getCurrentUserId() . '_' . $projectsModel->getId() . '_' . $datasetsModel->getName();
        DB::get('REDACTION_DATASETS')->execute('alter table `' . $tableName . '` add `'.$request->getName().'` '.$type);

        (new ProjectsModel)->commit();

        return (new CreateDatasetFieldResponse)
            ->setId($uuid);
    }
}