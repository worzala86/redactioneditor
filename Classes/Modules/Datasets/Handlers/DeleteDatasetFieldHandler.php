<?php

namespace App\Modules\Datasets\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Datasets\Exceptions\DatasetFieldNotFoundException;
use App\Modules\Datasets\Exceptions\DatasetNotFoundException;
use App\Modules\Datasets\Exceptions\ProjectNotFoundException;
use App\Modules\Datasets\Models\DatasetsFieldsModel;
use App\Modules\Datasets\Models\DatasetsModel;
use App\Modules\Datasets\Requests\DeleteDatasetFieldRequest;
use App\Modules\Datasets\Responses\DeleteDatasetFieldResponse;
use App\Modules\Projects\Models\ProjectsModel;

/**
 * Class DeleteDatasetFieldHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do usuniecia pola dataseta
 */
class DeleteDatasetFieldHandler extends Handler
{
    public function __invoke(DeleteDatasetFieldRequest $request): DeleteDatasetFieldResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $datasetsModel = (new DatasetsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where('project_id=?', $projectsModel->getId())
            ->load($request->getDatasetId(), true);

        if (!$datasetsModel->isLoaded()) {
            throw new DatasetNotFoundException;
        }

        $datasetsFieldModel = (new DatasetsFieldsModel)
            ->where('datasets_id=?', $datasetsModel->getId())
            ->load($request->getId(), true);

        if (!$datasetsFieldModel->isLoaded()) {
            throw new DatasetFieldNotFoundException;
        }

        (new ProjectsModel)->startTransaction();

        $datasetName = $datasetsModel->getName();
        $datasetFieldName = $datasetsFieldModel->getName();
        $deleted = $datasetsFieldModel->delete();

        $tableName = 'datasets_' . $request->getCurrentUserId() . '_' . $projectsModel->getId() . '_' . $datasetName;
        DB::get('REDACTION_DATASETS')->execute('alter table `' . $tableName . '` drop `' . $datasetFieldName.'`');

        (new ProjectsModel)->commit();

        return (new DeleteDatasetFieldResponse)
            ->setSuccess($deleted);
    }
}