<?php

namespace App\Modules\Datasets\Handlers;

use App\Containers\DatasetContainer;
use App\Containers\DatasetFieldContainer;
use App\Containers\DatasetFieldsContainer;
use App\Containers\DatasetsContainer;
use App\Handler;
use App\Modules\Datasets\Collections\DatasetsCollection;
use App\Modules\Datasets\Collections\DatasetsFieldsCollection;
use App\Modules\Datasets\Exceptions\ProjectNotFoundException;
use App\Modules\Datasets\Requests\GetDatasetsRequest;
use App\Modules\Datasets\Responses\GetDatasetsResponse;
use App\Modules\Projects\Models\ProjectsModel;

/**
 * Class GetDatasetsHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do pobrania listy datasetów w projekcie
 */
class GetDatasetsHandler extends Handler
{
    public function __invoke(GetDatasetsRequest $request): GetDatasetsResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $datasetsCollection = (new DatasetsCollection)
            //->setPage($request->getPage())
            //->setLimit($request->getLimit())
            ->setSortName($request->getSortName())
            ->setSortKind($request->getSortKind())
            ->setFilters($request->getFilters())
            ->where('project_id=?', $projectsModel->getId())
            ->loadAll();

        $datasets = new DatasetsContainer;
        foreach ($datasetsCollection as $datasetsModel) {
            $datasetsFieldsCollection = (new DatasetsFieldsCollection)
                ->where('datasets_id=?', $datasetsModel->getId())
                ->loadAll();
            $fields = new DatasetFieldsContainer;
            foreach ($datasetsFieldsCollection as $datasetFieldModel) {
                $fields->add(
                    (new DatasetFieldContainer)
                        ->setId($datasetFieldModel->getUuid())
                        ->setName($datasetFieldModel->getName())
                        ->setType($datasetFieldModel->getType())
                );
            }
            $datasets->add(
                (new DatasetContainer)
                    ->setId($datasetsModel->getUuid())
                    ->setName($datasetsModel->getName())
                    ->setFields($fields)
            );
        }

        return (new GetDatasetsResponse)
            ->setDatasets($datasets);
    }
}