<?php

namespace App\Modules\Datasets\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Datasets\Collections\DatasetsFieldsCollection;
use App\Modules\Datasets\Exceptions\DatasetNotFoundException;
use App\Modules\Datasets\Exceptions\ProjectIsNotYourException;
use App\Modules\Datasets\Exceptions\ProjectNotFoundException;
use App\Modules\Datasets\Models\DatasetsModel;
use App\Modules\Datasets\Requests\GetDatasetDataByNameRequest;
use App\Modules\Datasets\Responses\GetDatasetDataByNameResponse;
use App\Modules\Projects\Models\ProjectsModel;
use App\User;

/**
 * Class GetDatasetDataHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do pobrania danych dataseta
 */
class GetDatasetDataByNameHandler extends Handler
{
    public function __invoke(GetDatasetDataByNameRequest $request): GetDatasetDataByNameResponse
    {
        $projectsModel = (new ProjectsModel)
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        if (($projectsModel->getUserId()!==$request->getCurrentUserId())&&!$request->getUser()->isAdmin()) {
            throw new ProjectIsNotYourException;
        }

        $datasetsModel = (new DatasetsModel)
            ->where(' `project_id`=? ', $projectsModel->getId())
            ->where(' `name`=? ', $request->getName())
            ->load();

        if (!$datasetsModel->isLoaded()) {
            throw new DatasetNotFoundException;
        }

        $datasetsFieldsCollection = (new DatasetsFieldsCollection)
            ->where('datasets_id=?', $datasetsModel->getId())
            ->loadAll();

        $selectSql = [];
        foreach ($datasetsFieldsCollection as $datasetsFieldsModel) {
            $selectSql[] = $datasetsFieldsModel->getName();
        }

        $limit = '';
        if ($request->getLimit()) {
            $limit = ' limit ' . $request->getLimit() . ' offset ' . (($request->getPage()-1) * $request->getLimit()) . ' ';
        }

        $tableName = 'datasets_' . $datasetsModel->getUserId() . '_' . $projectsModel->getId() . '_' . $datasetsModel->getName();
        $count = DB::get('REDACTION_DATASETS')->getOne('select count(*) from `' . $tableName . '`');
        $rows = DB::get('REDACTION_DATASETS')->getAll('select `' . join('`, `', $selectSql) . '` from `' . $tableName . '`' . $limit);

        $dataset = [];
        foreach ($rows as $row) {
            $ret = [];
            foreach ($datasetsFieldsCollection as $datasetsFieldsModel) {
                $ret[(string)$datasetsFieldsModel->getName()] = $row[(string)$datasetsFieldsModel->getName()];
            }
            $dataset[] = $ret;
        }

        return (new GetDatasetDataByNameResponse)
            ->setName($datasetsModel->getName())
            ->setDataset($dataset)
            ->setTotalCount($count);
    }
}