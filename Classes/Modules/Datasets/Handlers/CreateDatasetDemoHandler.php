<?php

namespace App\Modules\Datasets\Handlers;

use App\Database\DB;
use App\Handler;
use App\LoremIpsum;
use App\Modules\Datasets\Collections\DatasetsFieldsCollection;
use App\Modules\Datasets\Exceptions\DatasetNotFoundException;
use App\Modules\Datasets\Exceptions\ProjectNotFoundException;
use App\Modules\Datasets\Models\DatasetsModel;
use App\Modules\Datasets\Requests\CreateDatasetDemoRequest;
use App\Modules\Datasets\Responses\CreateDatasetDemoResponse;
use App\Modules\Projects\Models\ProjectsModel;
use App\Types\UUID;

/**
 * Class CreateDatasetDemoHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do generowania danych demonstracyjnych
 */
class CreateDatasetDemoHandler extends Handler
{
    public function __invoke(CreateDatasetDemoRequest $request): CreateDatasetDemoResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $datasetsModel = (new DatasetsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where(' `project_id`=? ', $projectsModel->getId())
            ->load($request->getId(), true);

        if (!$datasetsModel->isLoaded()) {
            throw new DatasetNotFoundException;
        }

        $datasetFieldsCollection = (new DatasetsFieldsCollection)
            ->where('datasets_id=?', $datasetsModel->getId())
            ->loadAll();

        $files = DB::get()->getAll('select * from (select images.id, images.uuid, files.url, count(*) as `count` 
            from images 
            left join files on files.id=images.file_id 
            left join tags_images on tags_images.image_id=images.id 
            left join tags t on t.id=tags_images.tag_id 
            where 
            exists(select 1 from tags where name=\'słuchawki\' and id=t.id) or 
            exists(select 1 from tags where name=\'czarne\' and id=t.id) or 
            exists(select 1 from tags where name=\'sklep\' and id=t.id) 
            group by files.url) as x where x.`count`=3');

        (new ProjectsModel)->startTransaction();

        $loremIpsum = new LoremIpsum;
        $tableName = 'datasets_' . $request->getCurrentUserId() . '_' . $projectsModel->getId() . '_' . $datasetsModel->getName();
        for ($i = 0; $i < 3; $i++) {
            $sql = [];
            $params = [];
            foreach ($datasetFieldsCollection as $datasetFieldModel) {
                $type = (string)$datasetFieldModel->getType();
                $param = $loremIpsum->sentences(1);
                if ($type == "textarea") {
                    $param = $loremIpsum->sentences(6, 10);
                } else if ($type == 'image') {
                    $param = $files[rand(0, count($files) - 1)]['url'];
                } else if ($type == 'date') {
                    $param = $loremIpsum->words(1).' '.rand(1, 31);
                }
                $sql[] = ' `' . $datasetFieldModel->getName() . '`=? ';
                $params[] = $param;
            }
            $sql[] = '`uuid`=?';
            $params[] = hex2bin(UUID::fake());
            DB::get('REDACTION_DATASETS')->execute('insert into `' . $tableName . '` set ' . join(', ', $sql), $params);
        }

        (new ProjectsModel)->commit();

        return (new CreateDatasetDemoResponse)
            ->setSuccess(true);
    }
}