<?php

namespace App\Modules\Datasets\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Datasets\Exceptions\DatasetNotFoundException;
use App\Modules\Datasets\Exceptions\ProjectNotFoundException;
use App\Modules\Datasets\Models\DatasetsModel;
use App\Modules\Datasets\Requests\ClearDatasetDemoRequest;
use App\Modules\Datasets\Responses\ClearDatasetDemoResponse;
use App\Modules\Projects\Models\ProjectsModel;

/**
 * Class ClearDatasetDemoHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do czyszczenia danych demonstracyjnych
 */
class ClearDatasetDemoHandler extends Handler
{
    public function __invoke(ClearDatasetDemoRequest $request): ClearDatasetDemoResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $datasetsModel = (new DatasetsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where(' `project_id`=? ', $projectsModel->getId())
            ->load($request->getId(), true);

        if (!$datasetsModel->isLoaded()) {
            throw new DatasetNotFoundException;
        }

        $tableName = 'datasets_' . $request->getCurrentUserId() . '_' . $projectsModel->getId() . '_' . $datasetsModel->getName();
        DB::get('REDACTION_DATASETS')->execute('truncate `' . $tableName.'`');

        return (new ClearDatasetDemoResponse)
            ->setSuccess(true);
    }
}