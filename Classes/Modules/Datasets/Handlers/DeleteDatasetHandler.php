<?php

namespace App\Modules\Datasets\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Datasets\Exceptions\DatasetNotFoundException;
use App\Modules\Datasets\Exceptions\ProjectNotFoundException;
use App\Modules\Datasets\Models\DatasetsModel;
use App\Modules\Datasets\Requests\DeleteDatasetRequest;
use App\Modules\Datasets\Responses\DeleteDatasetResponse;
use App\Modules\Projects\Models\ProjectsModel;

/**
 * Class DeleteDatasetHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do usuniecia dataseta
 */
class DeleteDatasetHandler extends Handler
{
    public function __invoke(DeleteDatasetRequest $request): DeleteDatasetResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $datasetsModel = (new DatasetsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where('project_id=?', $projectsModel->getId())
            ->load($request->getId(), true);

        if (!$datasetsModel->isLoaded()) {
            throw new DatasetNotFoundException;
        }

        (new ProjectsModel)->startTransaction();

        $datasetName = $datasetsModel->getName();
        $deleted = $datasetsModel->delete();

        $tableName = 'datasets_' . $request->getCurrentUserId() . '_' . $projectsModel->getId() . '_' . $datasetName;
        DB::get('REDACTION_DATASETS')->execute('drop table `' . $tableName.'`');

        (new ProjectsModel)->commit();

        return (new DeleteDatasetResponse)
            ->setSuccess($deleted);
    }
}