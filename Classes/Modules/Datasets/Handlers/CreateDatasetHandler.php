<?php

namespace App\Modules\Datasets\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Datasets\Exceptions\ProjectNotFoundException;
use App\Modules\Datasets\Models\DatasetsModel;
use App\Modules\Datasets\Requests\CreateDatasetRequest;
use App\Modules\Datasets\Responses\CreateDatasetResponse;
use App\Modules\Datasets\Workers\CreateDataSetsShemaWorker;
use App\Modules\Projects\Models\ProjectsModel;
use App\Types\UUID;

/**
 * Class CreateDatasetHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do zapisu dataseta
 */
class CreateDatasetHandler extends Handler
{
    public function __invoke(CreateDatasetRequest $request): CreateDatasetResponse
    {
        (new ProjectsModel)->startTransaction();

        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $uuid = UUID::fake();
        (new DatasetsModel)
            ->setUuid($uuid)
            ->setName($request->getName())
            ->setProjectId($projectsModel->getId())
            ->setUserId($request->getCurrentUserId())
            ->insert();

        $createDataSetsShemaWorker = new CreateDataSetsShemaWorker;
        $createDataSetsShemaWorker->create($request->getCurrentUserId(), $projectsModel->getId());

        $tableUsersName = 'datasets_' . $request->getCurrentUserId() . '_' . $projectsModel->getId() . '_users';
        $tableIpName = 'datasets_' . $request->getCurrentUserId() . '_' . $projectsModel->getId() . '_ip';

        $tableName = 'datasets_' . $request->getCurrentUserId() . '_' . $projectsModel->getId() . '_' . $request->getName();
        DB::get('REDACTION_DATASETS')->execute('create table `' . $tableName . '`(
            id int primary key auto_increment,
            uuid binary(8),
            unique(uuid),
            added int unsigned default null,
            added_by int unsigned default null,
            added_ip_id int unsigned default null,
            updated int unsigned default null,
            updated_by int unsigned default null,
            updated_ip_id int unsigned default null,
            deleted int unsigned not null default 0,
            deleted_by int unsigned default null,
            deleted_ip_id int unsigned default null,
            CONSTRAINT `fk_'.$tableName.'_added_by` FOREIGN KEY (added_by) REFERENCES '.$tableUsersName.'(id) ON DELETE NO ACTION ON UPDATE CASCADE,
            CONSTRAINT `fk_'.$tableName.'_deleted_by` FOREIGN KEY (deleted_by) REFERENCES '.$tableUsersName.'(id) ON DELETE NO ACTION ON UPDATE CASCADE,
            CONSTRAINT `fk_'.$tableName.'_updated_by` FOREIGN KEY (updated_by) REFERENCES '.$tableUsersName.'(id) ON DELETE NO ACTION ON UPDATE CASCADE,
            CONSTRAINT `fk_'.$tableName.'_added_ip_id` FOREIGN KEY (added_ip_id) REFERENCES '.$tableIpName.'(id) ON DELETE NO ACTION ON UPDATE CASCADE,
            CONSTRAINT `fk_'.$tableName.'_deleted_ip_id` FOREIGN KEY (deleted_ip_id) REFERENCES '.$tableIpName.'(id) ON DELETE NO ACTION ON UPDATE CASCADE,
            CONSTRAINT `fk_'.$tableName.'_updated_ip_id` FOREIGN KEY (updated_ip_id) REFERENCES '.$tableIpName.'(id) ON DELETE NO ACTION ON UPDATE CASCADE
            );');

        (new ProjectsModel)->commit();

        return (new CreateDatasetResponse)
            ->setId($uuid)
            ->setName($request->getName());
    }
}