<?php

namespace App\Modules\Datasets\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Datasets\Collections\DatasetsFieldsCollection;
use App\Modules\Datasets\Exceptions\DatasetNotFoundException;
use App\Modules\Datasets\Exceptions\ProjectNotFoundException;
use App\Modules\Datasets\Models\DatasetsModel;
use App\Modules\Datasets\Requests\GetDatasetDataRequest;
use App\Modules\Datasets\Responses\GetDatasetDataResponse;
use App\Modules\Projects\Models\ProjectsModel;
use App\User;

/**
 * Class GetDatasetDataHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do pobrania danych dataseta
 */
class GetDatasetDataHandler extends Handler
{
    public function __invoke(GetDatasetDataRequest $request): GetDatasetDataResponse
    {
        $projectsModel = (new ProjectsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->load($request->getProjectId(), true);

        if (!$projectsModel->isLoaded()) {
            throw new ProjectNotFoundException;
        }

        $datasetsModel = (new DatasetsModel)
            ->where(' `user_id`=? ', $request->getCurrentUserId())
            ->where(' `project_id`=? ', $projectsModel->getId())
            ->load($request->getId(), true);

        if (!$datasetsModel->isLoaded()) {
            throw new DatasetNotFoundException;
        }

        $datasetsFieldsCollection = (new DatasetsFieldsCollection)
            ->where('datasets_id=?', $datasetsModel->getId())
            ->loadAll();

        $selectSql = [];
        foreach ($datasetsFieldsCollection as $datasetsFieldsModel) {
            $selectSql[] = $datasetsFieldsModel->getName();
        }

        $limit = '';
        if ($request->getLimit()) {
            $limit = ' limit ' . $request->getLimit() . ' offset ' . (($request->getPage()-1) * $request->getLimit()) . ' ';
        }

        $tableName = 'datasets_' . $request->getCurrentUserId() . '_' . $projectsModel->getId() . '_' . $datasetsModel->getName();
        $count = DB::get('REDACTION_DATASETS')->getOne('select count(*) from `' . $tableName . '`');
        $rows = DB::get('REDACTION_DATASETS')->getAll('select `' . join('`, `', $selectSql) . '` from `' . $tableName . '`'.$limit);

        $dataset = [];
        foreach ($rows as $row) {
            $ret = [];
            foreach ($datasetsFieldsCollection as $datasetsFieldsModel) {
                $ret[(string)$datasetsFieldsModel->getName()] = $row[(string)$datasetsFieldsModel->getName()];
            }
            $dataset[] = $ret;
        }

        return (new GetDatasetDataResponse)
            ->setName($datasetsModel->getName())
            ->setDataset($dataset)
            ->setTotalCount($count);
    }
}