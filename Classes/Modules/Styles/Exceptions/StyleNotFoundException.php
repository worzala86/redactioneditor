<?php

namespace App\Modules\Styles\Exceptions;

use App\NamedException;

class StyleNotFoundException extends \Exception implements NamedException
{
}