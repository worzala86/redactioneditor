<?php

namespace App\Modules\Styles\Handlers;

use App\Containers\CssContainer;
use App\Containers\CsssContainer;
use App\Containers\CssTagContainer;
use App\Containers\CssTagsContainer;
use App\Containers\FontContainer;
use App\Containers\FontsContainer;
use App\Database\DB;
use App\Handler;
use App\Modules\Styles\Collections\StylesCssViewCollection;
use App\Modules\Styles\Collections\StylesFontsCollection;
use App\Modules\Styles\Exceptions\StyleNotFoundException;
use App\Modules\Styles\Models\StylesModel;
use App\Modules\Styles\Requests\GetStyleCssRequest;
use App\Modules\Styles\Requests\SaveStyleCssRequest;
use App\Modules\Styles\Responses\GetStyleCssResponse;
use App\Modules\Styles\Responses\SaveStyleCssResponse;

/**
 * Class SaveStyleCssHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do aktualizacji stylu css
 */
class GetStyleCssHandler extends Handler
{
    public function __invoke(GetStyleCssRequest $request): GetStyleCssResponse
    {
        $stylesModel = (new StylesModel)
            ->load($request->getId(), true);

        if(!$stylesModel->isLoaded()){
            throw new StyleNotFoundException;
        }

        $styleId = $stylesModel->getId();

        $stylesCssCollection = (new StylesCssViewCollection)
            ->where('styles_id=?', $styleId)
            ->loadAll();

        $csss = [];
        foreach ($stylesCssCollection as $styleCssModel) {
            $csss[$styleCssModel->getTag()][$styleCssModel->getParam()] = $styleCssModel->getValue();
        }

        $resultCssTags = new CssTagsContainer;
        foreach ($csss as $tagName => $css) {
            $tagCss = new CsssContainer;
            foreach ($css as $param => $value) {
                $tagCss->add(
                    (new CssContainer)
                        ->setParameter($param)
                        ->setValue($value)
                );
            }
            $resultCssTags->add(
                (new CssTagContainer)
                    ->setTag($tagName)
                    ->setCss($tagCss)
            );
        }

        $fontsCollection = (new StylesFontsCollection)
            ->where('styles_id=?', $styleId)
            ->loadAll();
        $resultFonts = new FontsContainer;
        foreach ($fontsCollection as $fontsModel) {
            $resultFonts->add(
                (new FontContainer)
                    ->setName($fontsModel->getName())
                    ->setCategory($fontsModel->getCategory())
                    ->setLink($fontsModel->getLink())
            );
        }

        return (new GetStyleCssResponse)
            ->setCss($resultCssTags)
            ->setFonts($resultFonts);
    }
}