<?php

namespace App\Modules\Styles\Handlers;

use App\Handler;
use App\Modules\Styles\Models\StylesModel;
use App\Modules\Styles\Requests\CreateStyleRequest;
use App\Modules\Styles\Responses\CreateStyleResponse;
use App\Types\UUID;

/**
 * Class CreateStyleHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do dodawania nowych styli
 */
class CreateStyleHandler extends Handler
{
    public function __invoke(CreateStyleRequest $request): CreateStyleResponse
    {
        $uuid = UUID::fake();
        (new StylesModel)
            ->setUuid($uuid)
            ->setName($request->getName())
            ->setSystem(false)
            ->setUserId($request->getCurrentUserId())
            ->insert();

        return (new CreateStyleResponse)
            ->setId($uuid);
    }
}