<?php

namespace App\Modules\Styles\Handlers;

use App\Containers\ImageContainer;
use App\Containers\ImageThumbContainer;
use App\Containers\StyleContainer;
use App\Containers\StylesContainer;
use App\Database\DB;
use App\File;
use App\Handler;
use App\Modules\Files\Models\FilesModel;
use App\Modules\Styles\Collections\StylesCollection;
use App\Modules\Styles\Requests\GetAdminStylesRequest;
use App\Modules\Styles\Requests\GetStylesRequest;
use App\Modules\Styles\Responses\GetAdminStylesResponse;
use App\Modules\Styles\Responses\GetStylesResponse;
use App\Modules\User\Models\UsersModel;

/**
 * Class GetAdminStylesHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do pobrania styli - dla admina
 */
class GetAdminStylesHandler extends Handler
{
    public function __invoke(GetAdminStylesRequest $request): GetAdminStylesResponse
    {
        $stylesCollection = (new StylesCollection)
            ->where('`system`=?', false)
            ->loadAll();

        $styles = (new StylesContainer);
        foreach ($stylesCollection as $stylesModel) {
            /*$file = (new FilesModel)
                ->load($stylesModel->getImageId());
            $image = new ImageContainer;
            if($file->isLoaded()){
                $thumb = new ImageThumbContainer;
                $fileThumb = (new FilesModel)
                    ->load($file->getThumbId());
                if($fileThumb->isLoaded()){
                    $thumb->setUrl($fileThumb->getUrl());
                }
                $image->setUrl($file->getUrl())
                    ->setThumb($thumb);
            }*/
            $image = null;
            if($stylesModel->getImageId()) {
                $image = (new File)->load($stylesModel->getImageId());
            }
            $userModel = (new UsersModel)
                ->load($stylesModel->getUserId());
            $count = DB::get()->getOne('select count(*) from projects where style_id=?', $stylesModel->getId());
            $styles->add(
                (new StyleContainer)
                    ->setId($stylesModel->getUuid())
                    ->setName($stylesModel->getName())
                    ->setImage($image)
                    ->setUserName($userModel->getFirstName().' '.$userModel->getLastName())
                    ->setUsedTimes($count)
                    ->setPublic($stylesModel->getPublic())
            );
        }

        return (new GetAdminStylesResponse)
            ->setStyles($styles);
    }
}