<?php

namespace App\Modules\Styles\Handlers;

use App\Containers\StyleContainer;
use App\Containers\StylesContainer;
use App\File;
use App\Handler;
use App\Modules\Styles\Collections\StylesCollection;
use App\Modules\Styles\Requests\GetStylesRequest;
use App\Modules\Styles\Responses\GetStylesResponse;

/**
 * Class GetStylesHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do pobrania listy styli
 */
class GetStylesHandler extends Handler
{
    public function __invoke(GetStylesRequest $request): GetStylesResponse
    {
        $stylesCollection = (new StylesCollection);
        if((string)$request->getKind()==='private'){
            $stylesCollection->where('user_id=?', $request->getCurrentUserId());
        }else if((string)$request->getKind()==='public'){
            $stylesCollection->where('public=?', true);
        }
        $stylesCollection->where('`system`=?', false);
        $stylesCollection->loadAll();

        $styles = (new StylesContainer);
        foreach ($stylesCollection as $stylesModel) {
            /*$file = (new FilesModel)
                ->load($stylesModel->getImageId());
            $image = new ImageContainer;
            if($file->isLoaded()){
                $thumb = new ImageThumbContainer;
                $fileThumb = (new FilesModel)
                    ->load($file->getThumbId());
                if($fileThumb->isLoaded()){
                    $thumb->setUrl($fileThumb->getUrl());
                }
                $image->setUrl($file->getUrl())
                    ->setThumb($thumb);
            }*/
            $image = (new File)->load($stylesModel->getImageId());
            $styles->add(
                (new StyleContainer)
                    ->setId($stylesModel->getUuid())
                    ->setName($stylesModel->getName())
                    ->setImage($image)
            );
        }

        return (new GetStylesResponse)
            ->setStyles($styles);
    }
}