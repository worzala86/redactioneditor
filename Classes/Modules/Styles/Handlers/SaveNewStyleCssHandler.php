<?php

namespace App\Modules\Styles\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Styles\Exceptions\StyleNotFoundException;
use App\Modules\Styles\Models\StylesModel;
use App\Modules\Styles\Requests\SaveNewStyleCssRequest;
use App\Modules\Styles\Requests\SaveStyleCssRequest;
use App\Modules\Styles\Responses\SaveNewStyleCssResponse;
use App\Modules\Styles\Responses\SaveStyleCssResponse;
use App\Types\UUID;

/**
 * Class SaveNewStyleCssHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do zapisu stylu css np. elementu
 */
class SaveNewStyleCssHandler extends Handler
{
    public function __invoke(SaveNewStyleCssRequest $request): SaveNewStyleCssResponse
    {
        /*$stylesModel = (new StylesModel)
            ->load($request->getId(), true);

        if(!$stylesModel->isLoaded()){
            throw new StyleNotFoundException;
        }*/

        //$styleId = $stylesModel->getId();

        $imageId = DB::get()->getOne('select id from files where uuid=?', hex2bin($request->getImageId()));

        DB::get()->execute('start transaction');
        /*if($imageId) {
            DB::get()->execute('update styles set image_id=? where id=?', [$imageId, $styleId]);
        }*/
        //DB::get()->execute('delete from styles_style where styles_id=?', $styleId);

        $uuid = UUID::fake();
        $styleId = (new StylesModel)
            ->setUserId($request->getCurrentUserId())
            ->setUuid($uuid)
            ->insert();

        $tags = $request->getCss();
        foreach ($tags as $tag) {
            $tagName = $tag->getTag();
            $csss = $tag->getCss();
            $count = 0;
            foreach ($csss as $css) {
                $value = $css->getValue();
                if (!empty($value)) {
                    $count++;
                }
            }
            if($count==0){
                continue;
            }

            $tagId = DB::get()->getOne('select id from styles_tags where `name`=?', $tagName);
            if (!$tagId) {
                DB::get()->execute('insert into styles_tags set `name`=?', $tagName);
                $tagId = DB::get()->insertId();
            }

            foreach ($csss as $css) {
                $parameter = $css->getParameter();
                $value = $css->getValue();
                if(empty($value)){
                    continue;
                }

                $parameterId = DB::get()->getOne('select id from styles_params where `name`=?', $parameter);
                if (!$parameterId) {
                    DB::get()->execute('insert into styles_params set `name`=?', $parameter);
                    $parameterId = DB::get()->insertId();
                }

                $valueId = DB::get()->getOne('select id from styles_values where `value`=?', $value);
                if (!$valueId) {
                    DB::get()->execute('insert into styles_values set `value`=?', $value);
                    $valueId = DB::get()->insertId();
                }

                DB::get()->execute('insert into styles_style set styles_id=:styles_id, 
                param_id=:param_id, value_id=:value_id, tag_id=:tag_id', [
                    'styles_id' => $styleId,
                    'param_id' => $parameterId,
                    'value_id' => $valueId,
                    'tag_id' => $tagId,
                ]);
            }
        }

        DB::get()->execute('delete from styles_fonts where styles_id=?', $styleId);
        $fonts = $request->getFonts();
        if($fonts) {
            foreach ($fonts as $font) {
                DB::get()->execute('insert into styles_fonts set styles_id=:styles_id, 
                `name`=:name, category=:category, link=:link', [
                    'styles_id' => $styleId,
                    'name' => $font->getName(),
                    'category' => $font->getCategory(),
                    'link' => $font->getLink(),
                ]);
            }
        }

        DB::get()->execute('commit');

        return (new SaveNewStyleCssResponse)
            ->setId($uuid);
    }
}