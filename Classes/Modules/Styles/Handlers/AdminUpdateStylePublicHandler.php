<?php

namespace App\Modules\Styles\Handlers;

use App\Handler;
use App\Modules\Styles\Exceptions\StyleNotFoundException;
use App\Modules\Styles\Models\StylesModel;
use App\Modules\Styles\Requests\AdminUpdateStylePublicRequest;
use App\Modules\Styles\Responses\AdminUpdateStylePublicResponse;

/**
 * Class AdminUpdateStylePublicHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do aktualizacji sstatusu stylu - public czy private
 */
class AdminUpdateStylePublicHandler extends Handler
{
    public function __invoke(AdminUpdateStylePublicRequest $request): AdminUpdateStylePublicResponse
    {
        $stylesModel = (new StylesModel)
            ->load($request->getId(), true);

        if (!$stylesModel->isLoaded()) {
            throw new StyleNotFoundException;
        }

        $updated = $stylesModel
            ->setPublic($request->getPublic())
            ->update($stylesModel->getId());

        return (new AdminUpdateStylePublicResponse)
            ->setSuccess($updated);
    }
}