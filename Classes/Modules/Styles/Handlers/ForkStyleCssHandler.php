<?php

namespace App\Modules\Styles\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Styles\Exceptions\StyleNotFoundException;
use App\Modules\Styles\Models\StylesModel;
use App\Modules\Styles\Requests\ForkStyleCssRequest;
use App\Modules\Styles\Requests\SaveStyleCssRequest;
use App\Modules\Styles\Responses\ForkStyleCssResponse;
use App\Modules\Styles\Responses\SaveStyleCssResponse;
use App\Types\UUID;

/**
 * Class ForkStyleCssHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do powielenia stylu do dalszej edycji
 */
class ForkStyleCssHandler extends Handler
{
    public function __invoke(ForkStyleCssRequest $request): ForkStyleCssResponse
    {
        $stylesModel = (new StylesModel)
            ->load($request->getId(), true);

        if (!$stylesModel->isLoaded()) {
            throw new StyleNotFoundException;
        }

        $styleId = $stylesModel->getId();

        //$imageId = DB::get()->getOne('select id from files where uuid=?', hex2bin($request->getImageId()));

        DB::get()->execute('start transaction');

        $uuid = UUID::fake();
        $styles = DB::get()->getRow('select * from styles where id=?', $styleId);
        DB::get()->execute('insert into styles set uuid=?, added=?, added_by=?, added_ip_id=?, `name`=?, user_id=?, `system`=false',
            [hex2bin($uuid), time(), $request->getCurrentUserId(), $request->getIpId(), $styles['name'], $request->getCurrentUserId()]);
        $newStyleId = DB::get()->insertId();

        //DB::get()->execute('update styles set image_id=? where id=?', [$imageId, $styleId]);
        //DB::get()->execute('delete from styles_style where styles_id=?', $styleId);
        $tags = DB::get()->getAll('select * from styles_style where styles_id=?', $styleId);
        //$tags = $request->getCss();
        foreach ($tags as $tag) {
            /*$tagName = $tag->getTag();
            $tagId = DB::get()->getOne('select id from styles_tags where `name`=?', $tagName);
            if (!$tagId) {
                DB::get()->execute('insert into styles_tags set `name`=?', $tagName);
                $tagId = DB::get()->insertId();
            }*/

            //$csss = $tag->getCss();
            //foreach ($csss as $css) {
                /*$parameter = $css->getParameter();
                $parameterId = DB::get()->getOne('select id from styles_params where `name`=?', $parameter);
                if (!$parameterId) {
                    DB::get()->execute('insert into styles_params set `name`=?', $parameter);
                    $parameterId = DB::get()->insertId();
                }*/

                /*$value = $css->getValue();
                $valueId = DB::get()->getOne('select id from styles_values where `value`=?', $value);
                if (!$valueId) {
                    DB::get()->execute('insert into styles_values set `value`=?', $value);
                    $valueId = DB::get()->insertId();
                }*/

                DB::get()->execute('insert into styles_style set styles_id=:styles_id, 
                param_id=:param_id, value_id=:value_id, tag_id=:tag_id', [
                    'styles_id' => $newStyleId,
                    'param_id' => $tag['param_id'],
                    'value_id' => $tag['value_id'],
                    'tag_id' => $tag['tag_id'],
                ]);
            //}
        }

        //DB::get()->execute('delete from styles_fonts where styles_id=?', $styleId);
        //$fonts = $request->getFonts();
        $fonts = DB::get()->getAll('select * from styles_fonts where styles_id=?', $styleId);
        foreach ($fonts as $font) {
            DB::get()->execute('insert into styles_fonts set styles_id=:styles_id, 
                `name`=:name, category=:category, link=:link', [
                'styles_id' => $newStyleId,
                'name' => $font['name'],
                'category' => $font['category'],
                'link' => $font['link'],
            ]);
        }

        DB::get()->execute('commit');

        return (new ForkStyleCssResponse())
            ->setId($uuid);
    }
}