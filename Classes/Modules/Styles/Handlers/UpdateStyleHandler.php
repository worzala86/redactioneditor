<?php

namespace App\Modules\Styles\Handlers;

use App\Handler;
use App\Modules\Styles\Exceptions\StyleNotFoundException;
use App\Modules\Styles\Models\StylesModel;
use App\Modules\Styles\Requests\UpdateStyleRequest;
use App\Modules\Styles\Responses\UpdateStyleResponse;

/**
 * Class UpdateStyleHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do aktualizacji stylu
 */
class UpdateStyleHandler extends Handler
{
    public function __invoke(UpdateStyleRequest $request): UpdateStyleResponse
    {
        $stylesModel = (new StylesModel)
            ->load($request->getId(), true);

        if(!$stylesModel->isLoaded()){
            throw new StyleNotFoundException;
        }

        $updated = $stylesModel
            ->setName($request->getName())
            ->update($stylesModel->getId());

        return (new UpdateStyleResponse)
            ->setSuccess($updated);
    }
}