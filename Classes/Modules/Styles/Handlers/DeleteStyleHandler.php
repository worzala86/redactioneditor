<?php

namespace App\Modules\Styles\Handlers;

use App\Handler;
use App\Modules\Styles\Models\StylesModel;
use App\Modules\Styles\Requests\DeleteStyleRequest;
use App\Modules\Styles\Requests\UpdateStyleRequest;
use App\Modules\Styles\Responses\DeleteStyleResponse;
use App\Modules\Styles\Responses\UpdateStyleResponse;

/**
 * Class DeleteStyleHandler
 * @package App\Modules\Styles\Handlers
 * @description Metoda służy do usunięcia stylu
 */
class DeleteStyleHandler extends Handler
{
    public function __invoke(DeleteStyleRequest $request): DeleteStyleResponse
    {
        $deleted = (new StylesModel)
            ->delete($request->getId(), true);

        return (new DeleteStyleResponse)
            ->setSuccess($deleted);
    }
}