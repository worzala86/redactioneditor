<?php

namespace App;

use App\Modules\Styles\Handlers;
use App\Types\StyleKind;
use App\Types\UUID;

return [
    ['method'=>'post', 'url'=>'api/styles', 'handler'=>Handlers\CreateStyleHandler::class],
    ['method'=>'get', 'url'=>'api/styles/:kind', 'handler'=>Handlers\GetStylesHandler::class, 'regex'=>['kind'=>StyleKind::$pattern]],
    ['method'=>'get', 'url'=>'api/admin-styles', 'handler'=>Handlers\GetAdminStylesHandler::class],
    ['method'=>'put', 'url'=>'api/styles/:id', 'handler'=>Handlers\UpdateStyleHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/admin-styles/:id/public', 'handler'=>Handlers\AdminUpdateStylePublicHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'delete', 'url'=>'api/styles/:id', 'handler'=>Handlers\DeleteStyleHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/styles/:id/css', 'handler'=>Handlers\SaveStyleCssHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/styles/css', 'handler'=>Handlers\SaveNewStyleCssHandler::class],
    ['method'=>'get', 'url'=>'api/styles/:id/css', 'handler'=>Handlers\GetStyleCssHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/styles/:id/fork', 'handler'=>Handlers\ForkStyleCssHandler::class, 'regex'=>['id'=>UUID::$pattern]],
];