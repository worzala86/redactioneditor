<?php

namespace App\Modules\Styles\Models;

use App\Model;

class StylesFontsModel extends Model
{
    public $setDefaultFields = false;

    private $stylesId;
    private $name;
    private $category;
    private $link;

    public function setLink(string $link)
    {
        $this->set('link', $link);
        $this->link = $link;
        return $this;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setCategory(string $category)
    {
        $this->set('category', $category);
        $this->category = $category;
        return $this;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function setName(string $name)
    {
        $this->set('name', $name);
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setStylesId(int $stylesId)
    {
        $this->set('styles_id', $stylesId);
        $this->stylesId = $stylesId;
        return $this;
    }

    public function getStylesId(): int
    {
        return $this->stylesId;
    }
}