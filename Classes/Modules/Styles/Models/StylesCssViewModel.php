<?php

namespace App\Modules\Styles\Models;

use App\Model;

class StylesCssViewModel extends Model
{
    public $setDefaultFields = false;

    private $stylesId;
    private $tag;
    private $param;
    private $value;

    public function setValue(string $value)
    {
        $this->set('value', $value);
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setParam(string $param)
    {
        $this->set('param', $param);
        $this->param = $param;
        return $this;
    }

    public function getParam(): string
    {
        return $this->param;
    }

    public function setTag(string $tag)
    {
        $this->set('tag', $tag);
        $this->tag = $tag;
        return $this;
    }

    public function getTag(): string
    {
        return $this->tag;
    }

    public function setStylesId(int $stylesId)
    {
        $this->set('styles_id', $stylesId);
        $this->stylesId = $stylesId;
        return $this;
    }

    public function getStylesId(): int
    {
        return $this->stylesId;
    }
}