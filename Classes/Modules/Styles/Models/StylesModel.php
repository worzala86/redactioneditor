<?php

namespace App\Modules\Styles\Models;

use App\Model;
use App\Types\UUID;

class StylesModel extends Model
{
    private $id;
    private $uuid;
    private $name;
    private $imageId;
    private $userId;
    private $public;
    private $system;

    public function setSystem(bool $system)
    {
        $this->set('system', $system);
        $this->system = $system;
        return $this;
    }

    public function getSystem(): bool
    {
        return $this->system;
    }

    public function setPublic(bool $public)
    {
        $this->set('public', $public);
        $this->public = $public;
        return $this;
    }

    public function getPublic(): bool
    {
        return $this->public;
    }

    public function setUserId(int $userId)
    {
        $this->set('user_id', $userId);
        $this->userId = $userId;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setImageId(int $imageId = null)
    {
        $this->set('image_id', $imageId);
        $this->imageId = $imageId;
        return $this;
    }

    public function getImageId(): ?int
    {
        return $this->imageId;
    }

    public function setName(string $name)
    {
        $this->set('name', $name);
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}