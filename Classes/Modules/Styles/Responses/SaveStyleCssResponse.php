<?php

namespace App\Modules\Styles\Responses;

use App\Response;

class SaveStyleCssResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Pole zwraca czy pomyślnie zapisano styl
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }
}