<?php

namespace App\Modules\Styles\Responses;

use App\Containers\StylesContainer;
use App\Response;

class GetStylesResponse extends Response
{
    private $styles;

    /**
     * @param StylesContainer $styles
     * @description Pole zawiera listę styli
     * @return $this
     */
    public function setStyles(StylesContainer $styles)
    {
        $this->styles = $styles;
        return $this;
    }

    public function getStyles(): StylesContainer
    {
        return $this->styles;
    }
}