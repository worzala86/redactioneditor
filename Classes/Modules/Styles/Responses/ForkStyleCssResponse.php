<?php

namespace App\Modules\Styles\Responses;

use App\Response;
use App\Types\UUID;

class ForkStyleCssResponse extends Response
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator nowego stylu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}