<?php

namespace App\Modules\Styles\Responses;

use App\Response;

class AdminUpdateStylePublicResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Pole zwraca czy pomyślnie zmieniono status
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }
}