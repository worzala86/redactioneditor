<?php

namespace App\Modules\Styles\Responses;

use App\Containers\CssTagsContainer;
use App\Containers\FontsContainer;
use App\Response;

class GetStyleCssResponse extends Response
{
    private $css;
    private $fonts;

    /**
     * @param CssTagsContainer $css
     * @description Tablica ze stylami
     * @return $this
     */
    public function setCss(CssTagsContainer $css)
    {
        $this->css = $css;
        return $this;
    }

    public function getCss(): CssTagsContainer
    {
        return $this->css;
    }

    /**
     * @param FontsContainer $fonts
     * @description Tablica z czcionkami
     * @return $this
     */
    public function setFonts(FontsContainer $fonts)
    {
        $this->fonts = $fonts;
        return $this;
    }

    public function getFonts(): FontsContainer
    {
        return $this->fonts;
    }
}