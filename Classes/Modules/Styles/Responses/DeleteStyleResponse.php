<?php

namespace App\Modules\Styles\Responses;

use App\Response;

class DeleteStyleResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Pole zwraca czy pomyślnie usunięto styl
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }
}