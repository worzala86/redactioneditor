<?php

namespace App\Modules\Styles\Collections;

use App\CollectionTrait;
use App\Modules\Styles\Models\StylesFontsModel;

class StylesFontsCollection extends StylesFontsModel implements \Iterator
{
    use CollectionTrait;
}