<?php

namespace App\Modules\Styles\Collections;

use App\CollectionTrait;
use App\Modules\Styles\Models\StylesCssViewModel;

class StylesCssViewCollection extends StylesCssViewModel implements \Iterator
{
    use CollectionTrait;
}