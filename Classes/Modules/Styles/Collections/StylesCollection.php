<?php

namespace App\Modules\Styles\Collections;

use App\CollectionTrait;
use App\Modules\Styles\Models\StylesModel;

class StylesCollection extends StylesModel implements \Iterator
{
    use CollectionTrait;
}