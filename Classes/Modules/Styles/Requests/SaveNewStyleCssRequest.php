<?php

namespace App\Modules\Styles\Requests;

use App\AdminRequest;
use App\Containers\CssTagsContainer;
use App\Containers\FontsContainer;
use App\Types\UUID;

class SaveNewStyleCssRequest extends AdminRequest
{
    private $css;
    private $fonts;
    private $imageId;

    /**
     * @param CssTagsContainer $css
     * @description Tablica ze stylami
     * @return $this
     */
    public function setCss(CssTagsContainer $css)
    {
        $this->css = $css;
        return $this;
    }

    public function getCss(): CssTagsContainer
    {
        return $this->css;
    }

    /**
     * @param FontsContainer $fonts
     * @description Tablica z czcionkami
     * @return $this
     */
    public function setFonts(FontsContainer $fonts = null)
    {
        $this->fonts = $fonts;
        return $this;
    }

    public function getFonts(): ?FontsContainer
    {
        return $this->fonts;
    }

    /**
     * @param UUID $imageId
     * @description Identyfikator pliku
     * @return $this
     */
    public function setImageId(UUID $imageId = null)
    {
        $this->imageId = $imageId;
        return $this;
    }

    public function getImageId(): ?UUID
    {
        return $this->imageId;
    }
}