<?php

namespace App\Modules\Styles\Requests;

use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;
use App\Types\StyleKind;
use App\UserRequest;

class GetStylesRequest extends UserRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;

    private $id;

    /**
     * @param StyleKind $kind
     * @description Rodzaj stylu private|public
     * @return $this
     */
    public function setKind(StyleKind $kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): StyleKind
    {
        return $this->kind;
    }
}