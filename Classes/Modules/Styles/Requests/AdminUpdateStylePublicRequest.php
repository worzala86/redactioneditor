<?php

namespace App\Modules\Styles\Requests;

use App\AdminRequest;
use App\Types\UUID;

class AdminUpdateStylePublicRequest extends AdminRequest
{
    private $id;
    private $public;

    /**
     * @param int $id
     * @description Identyfikator stylu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param bool $public
     * @description Informuje o tym czy styl jest publiczny
     * @return $this
     */
    public function setPublic(bool $public)
    {
        $this->public = $public;
        return $this;
    }

    public function getPublic(): bool
    {
        return $this->public;
    }
}