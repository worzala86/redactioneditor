<?php

namespace App\Modules\Styles\Requests;

use App\AdminRequest;
use App\Types\UUID;

class UpdateStyleRequest extends AdminRequest
{
    private $id;
    private $name;

    /**
     * @param int $id
     * @description Identyfikator stylu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa stylu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }
}