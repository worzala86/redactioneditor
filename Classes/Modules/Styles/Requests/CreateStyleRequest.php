<?php

namespace App\Modules\Styles\Requests;

use App\AdminRequest;
use App\Types\UUID;

class CreateStyleRequest extends AdminRequest
{
    private $name;
    private $file;

    /**
     * @param string $name
     * @description Nazwa stylu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $file
     * @description Nazwa pliku styli
     * @return $this
     */
    public function setFile(string $file)
    {
        $this->file = $file;
        return $this;
    }

    public function getFile(): string
    {
        return $this->file;
    }
}