<?php

namespace App\Modules\Styles\Requests;

use App\AdminRequest;
use App\Types\UUID;

class DeleteStyleRequest extends AdminRequest
{
    private $id;

    /**
     * @param int $id
     * @description Identyfikator stylu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}