<?php

namespace App\Modules\Styles\Requests;

use App\AdminRequest;
use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;

class GetAdminStylesRequest extends AdminRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;
}