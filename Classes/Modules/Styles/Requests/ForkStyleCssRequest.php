<?php

namespace App\Modules\Styles\Requests;

use App\Types\UUID;
use App\UserRequest;

class ForkStyleCssRequest extends UserRequest
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator stylu do zforkowania
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}