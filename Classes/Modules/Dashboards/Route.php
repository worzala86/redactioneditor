<?php

namespace App;

use App\Modules\Dashboards\Handlers;

return [
    ['method'=>'get', 'url'=>'api/dashboards', 'handler'=>Handlers\GetDashboardHandler::class],
];