<?php

namespace App\Modules\Dashboards\Responses;

use App\Response;

class GetDashboardResponse extends Response
{
    private $projectsCount;

    /**
     * @param int $projectsCount
     * @description Ilość projektów aktywnego użytkownika
     * @return $this
     */
    public function setProjectsCount(int $projectsCount)
    {
        $this->projectsCount = $projectsCount;
        return $this;
    }

    public function getProjectsCount(): int
    {
        return $this->projectsCount;
    }
}