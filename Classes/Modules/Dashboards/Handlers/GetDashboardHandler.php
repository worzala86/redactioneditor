<?php

namespace App\Modules\Dashboards\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Dashboards\Requests\GetDashboardRequest;
use App\Modules\Dashboards\Responses\GetDashboardResponse;

/**
 * Class GetDashboardHandler
 * @package App\Modules\Files\Handlers
 * @description Metoda służy do pobrania informacji startowych
 */
class GetDashboardHandler extends Handler
{
    public function __invoke(GetDashboardRequest $request): GetDashboardResponse
    {
        $projectsCount = DB::get()->getOne('select count(*) from projects where user_id=? and deleted=0', $request->getCurrentUserId());

        return (new GetDashboardResponse)
            ->setProjectsCount($projectsCount);
    }
}