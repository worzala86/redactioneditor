<?php

namespace App\Modules\Elements\Collections;

use App\CollectionTrait;
use App\Modules\Elements\Models\ElementsModel;
use App\PaginationTrait;

class ElementsCollection extends ElementsModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}