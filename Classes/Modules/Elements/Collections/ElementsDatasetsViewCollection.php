<?php

namespace App\Modules\Elements\Collections;

use App\CollectionTrait;
use App\Modules\Elements\Models\ElementsDatasetsViewModel;
use App\Modules\Elements\Models\ElementsModel;
use App\PaginationTrait;

class ElementsDatasetsViewCollection extends ElementsDatasetsViewModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}