<?php

namespace App\Modules\Elements\Collections;

use App\CollectionTrait;
use App\Modules\Elements\Models\ElementsDatasetsModel;
use App\Modules\Elements\Models\ElementsModel;
use App\PaginationTrait;

class ElementsDatasetsCollection extends ElementsDatasetsModel implements \Iterator
{
    use CollectionTrait;
    use PaginationTrait;
}