<?php

namespace App\Modules\Elements\Models;

use App\Model;
use App\Types\ElementKind;
use App\Types\UUID;

class ElementsModel extends Model
{
    private $id;
    private $uuid;
    private $userId;
    private $imageId;
    private $kind;
    private $contentId;
    private $downloadTimes;
    private $styleId;
    private $rootStyleId;

    public function setRootStyleId(int $rootStyleId = null)
    {
        $this->set('root_style_id', $rootStyleId);
        $this->rootStyleId = $rootStyleId;
        return $this;
    }

    public function getRootStyleId(): ?int
    {
        return $this->rootStyleId;
    }

    public function setStyleId(int $styleId = null)
    {
        $this->set('style_id', $styleId);
        $this->styleId = $styleId;
        return $this;
    }

    public function getStyleId(): ?int
    {
        return $this->styleId;
    }

    public function setDownloadTimes(int $downloadTimes)
    {
        $this->set('download_times', $downloadTimes);
        $this->downloadTimes = $downloadTimes;
        return $this;
    }

    public function getDownloadTimes(): int
    {
        return $this->downloadTimes;
    }

    public function setContentId(int $contentId = null)
    {
        $this->set('content_id', $contentId);
        $this->contentId = $contentId;
        return $this;
    }

    public function getContentId(): ?int
    {
        return $this->contentId;
    }

    public function setKind(ElementKind $kind)
    {
        $this->set('kind', $kind);
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): ElementKind
    {
        return $this->kind;
    }

    public function setImageId(int $imageId = null)
    {
        $this->set('image_id', $imageId);
        $this->imageId = $imageId;
        return $this;
    }

    public function getImageId(): ?int
    {
        return $this->imageId;
    }

    public function setUserId(int $userId)
    {
        $this->set('user_id', $userId);
        $this->userId = $userId;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUuid(UUID $uuid)
    {
        $this->set('uuid', hex2bin($uuid));
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid(): UUID
    {
        return $this->uuid;
    }

    public function setId(int $id)
    {
        $this->set('id', $id);
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}