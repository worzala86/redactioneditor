<?php

namespace App\Modules\Elements\Models;

use App\Model;
use App\Types\ShortUUID;
use App\Types\Time;
use App\Types\UUID;

class ElementsDatasetsViewModel extends Model
{
    public $setDefaultFields = false;

    private $elementId;
    private $key;
    private $value;
    private $elementPositionId;

    public function setElementPositionId(UUID $elementPositionId)
    {
        $this->set('element_position_id', $elementPositionId);
        $this->elementPositionId = $elementPositionId;
        return $this;
    }

    public function getElementPositionId(): UUID
    {
        return $this->elementPositionId;
    }

    public function setValue(string $value)
    {
        $this->set('value', $value);
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setKey(ShortUUID $key)
    {
        $this->set('key', hex2bin($key));
        $this->key = $key;
        return $this;
    }

    public function getKey(): ShortUUID
    {
        return $this->key;
    }

    public function setElementId(int $elementId)
    {
        $this->set('element_id', $elementId);
        $this->elementId = $elementId;
        return $this;
    }

    public function getElementId(): int
    {
        return $this->elementId;
    }
}