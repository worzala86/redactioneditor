<?php

namespace App\Modules\Elements\Requests;

use App\AdminRequest;
use App\Types\UUID;

class AssignUserImageToElementRequest extends AdminRequest
{
    private $id;
    private $imageId;

    /**
     * @param UUID $id
     * @description Identyfikator edytowanego projektu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param UUID $imageId
     * @description Identyfikator pliku do przypisania do projektu
     * @return $this
     */
    public function setImageId(UUID $imageId)
    {
        $this->imageId = $imageId;
        return $this;
    }

    public function getImageId(): UUID
    {
        return $this->imageId;
    }
}