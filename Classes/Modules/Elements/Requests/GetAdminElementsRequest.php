<?php

namespace App\Modules\Elements\Requests;

use App\AdminRequest;
use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;

class GetAdminElementsRequest extends AdminRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;
}