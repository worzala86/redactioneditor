<?php

namespace App\Modules\Elements\Requests;

use App\AdminRequest;
use App\Containers\HtmlElementContainer;
use App\Types\ElementKind;
use App\Types\UUID;

class CreateElementRequest extends AdminRequest
{
    private $content;
    private $kind;
    private $styleId;
    private $rootStyleId;

    /**
     * @param HtmlElementContainer $content
     * @description Treść elementu
     * @return $this
     */
    public function setContent(HtmlElementContainer $content)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent(): HtmlElementContainer
    {
        return $this->content;
    }

    /**
     * @param ElementKind $kind
     * @description Rodzaj elementu
     * @return $this
     */
    public function setKind(ElementKind $kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): ElementKind
    {
        return $this->kind;
    }

    /**
     * @param UUID $styleId
     * @description Identyfikator stylu elementu
     * @return $this
     */
    public function setStyleId(UUID $styleId)
    {
        $this->styleId = $styleId;
        return $this;
    }

    public function getStyleId(): UUID
    {
        return $this->styleId;
    }

    /**
     * @param UUID $rootStyleId
     * @description Identyfikator głównego stylu strony - w elelencie jest to w celach podglądowych
     * @return $this
     */
    public function setRootStyleId(UUID $rootStyleId)
    {
        $this->rootStyleId = $rootStyleId;
        return $this;
    }

    public function getRootStyleId(): UUID
    {
        return $this->rootStyleId;
    }
}