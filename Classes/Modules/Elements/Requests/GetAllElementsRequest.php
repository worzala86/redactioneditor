<?php

namespace App\Modules\Elements\Requests;

use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;
use App\Types\ElementKind;
use App\Types\ElementType;
use App\UserRequest;

class GetAllElementsRequest extends UserRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;
}