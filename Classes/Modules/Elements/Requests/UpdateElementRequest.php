<?php

namespace App\Modules\Elements\Requests;

use App\AdminRequest;
use App\Containers\HtmlElementContainer;
use App\Types\UUID;

class UpdateElementRequest extends AdminRequest
{
    private $id;
    private $content;
    private $rootStyleId;
    private $dataSet;

    /**
     * @param UUID $id
     * @description Identyfikator elementu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param HtmlElementContainer $content
     * @description Treść elementu
     * @return $this
     */
    public function setContent(HtmlElementContainer $content)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent(): HtmlElementContainer
    {
        return $this->content;
    }

    /**
     * @param UUID $rootStyleId
     * @description Identyfikator głównego pliku ze stylem podglądowym
     * @return $this
     */
    public function setRootStyleId(UUID $rootStyleId)
    {
        $this->rootStyleId = $rootStyleId;
        return $this;
    }

    public function getRootStyleId(): ?UUID
    {
        return $this->rootStyleId;
    }

    /**
     * @param array $dataSet
     * @description Zbiur dataset - tytuły i treści itp
     * @return $this
     */
    public function setDataSet(array $dataSet)
    {
        $this->dataSet = $dataSet;
        return $this;
    }

    public function getDataSet(): ?array
    {
        return $this->dataSet;
    }
}