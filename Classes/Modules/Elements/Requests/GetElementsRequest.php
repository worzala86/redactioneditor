<?php

namespace App\Modules\Elements\Requests;

use App\FiltersTrait;
use App\PaginationTrait;
use App\SortTrait;
use App\Types\ElementKind;
use App\Types\ElementType;
use App\UserRequest;

class GetElementsRequest extends UserRequest
{
    use PaginationTrait;
    use SortTrait;
    use FiltersTrait;

    private $type;
    private $kind;

    /**
     * @param ElementType $type
     * @description Rodzaj elementów custom|system
     * @return $this
     */
    public function setType(ElementType $type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType(): ElementType
    {
        return $this->type;
    }

    /**
     * @param ElementKind $kind
     * @description Element typu header|footer|body etc.
     * @return $this
     */
    public function setKind(ElementKind $kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): ElementKind
    {
        return $this->kind;
    }
}