<?php

namespace App\Modules\Elements\Requests;

use App\AdminRequest;
use App\Types\UUID;

class DeleteElementRequest extends AdminRequest
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator elementu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}