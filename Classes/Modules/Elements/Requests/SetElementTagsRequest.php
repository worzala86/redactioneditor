<?php

namespace App\Modules\Elements\Requests;

use App\AdminRequest;
use App\Containers\HtmlElementContainer;
use App\Types\UUID;

class SetElementTagsRequest extends AdminRequest
{
    private $id;
    private $tags;

    /**
     * @param UUID $id
     * @description Identyfikator elementu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $tags
     * @description Lista tagów
     * @return $this
     */
    public function setTags(string $tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getTags(): string
    {
        return $this->tags;
    }
}