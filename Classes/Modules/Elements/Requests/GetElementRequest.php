<?php

namespace App\Modules\Elements\Requests;

use App\Types\UUID;
use App\UserRequest;

class GetElementRequest extends UserRequest
{
    private $id;

    /**
     * @param UUID $id
     * @description Identyfikator elementu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}