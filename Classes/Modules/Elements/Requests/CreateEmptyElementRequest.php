<?php

namespace App\Modules\Elements\Requests;

use App\Containers\HtmlElementContainer;
use App\Types\ElementKind;
use App\UserRequest;

class CreateEmptyElementRequest extends UserRequest
{
    private $kind;

    /**
     * @param ElementKind $kind
     * @description Rodzaj elementu
     * @return $this
     */
    public function setKind(ElementKind $kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): ElementKind
    {
        return $this->kind;
    }
}