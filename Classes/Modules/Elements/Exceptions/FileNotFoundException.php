<?php

namespace App\Modules\Elements\Exceptions;

use App\NamedException;

class FileNotFoundException extends \Exception implements NamedException
{
}