<?php

namespace App\Modules\Elements\Exceptions;

use App\NamedException;

class ElementNotFoundException extends \Exception implements NamedException
{
}