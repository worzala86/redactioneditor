<?php

namespace App\Modules\Elements\Exceptions;

use App\NamedException;

class StyleNotFoundException extends \Exception implements NamedException
{
}