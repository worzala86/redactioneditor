<?php

namespace App;

use App\Modules\Elements\Handlers;
use App\Types\ElementKind;
use App\Types\ElementType;
use App\Types\UUID;

return [
    ['method'=>'get', 'url'=>'api/elements/all', 'handler'=>Handlers\GetAllElementsHandler::class, 'regex'=>['type'=>ElementType::$pattern, 'kind'=>ElementKind::$pattern]],
    ['method'=>'get', 'url'=>'api/admin-elements', 'handler'=>Handlers\GetAdminElementsHandler::class],
    ['method'=>'get', 'url'=>'api/elements/:id', 'handler'=>Handlers\GetElementHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'delete', 'url'=>'api/elements/:id', 'handler'=>Handlers\DeleteElementHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/elements', 'handler'=>Handlers\CreateElementHandler::class],
    ['method'=>'put', 'url'=>'api/elements/:id', 'handler'=>Handlers\UpdateElementHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/elements/:id/images/:imageId', 'handler'=>Handlers\AssignUserImageToElementHandler::class, 'regex'=>['id'=>UUID::$pattern, 'imageId'=>UUID::$pattern]],
    ['method'=>'post', 'url'=>'api/elements/:id/tags', 'handler'=>Handlers\SetElementTagsHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'get', 'url'=>'api/elements/:id/tags', 'handler'=>Handlers\GetElementTagsHandler::class, 'regex'=>['id'=>UUID::$pattern]],
    ['method'=>'get', 'url'=>'api/elements/tags', 'handler'=>Handlers\GetElementsTagsHandler::class],
    ['method'=>'post', 'url'=>'api/elements/create', 'handler'=>Handlers\CreateEmptyElementHandler::class],
];