<?php

namespace App\Modules\Elements\Responses;

use App\Response;

class AssignUserImageToElementResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Wartość informuje o tym czy pomyślnie przypisano zdjęcie
     * @return $this
     */
    public function setSuccess(bool $success){
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool {
        return $this->success;
    }
}