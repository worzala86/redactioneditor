<?php

namespace App\Modules\Elements\Responses;

use App\Containers\ElementsContainer;
use App\Response;
use App\TotalCountTrait;

class GetAdminElementsResponse extends Response
{
    use TotalCountTrait;

    private $elements;

    /**
     * @param ElementsContainer $elements
     * @description Pole zawiera listę elementów
     * @return $this
     */
    public function setElements(ElementsContainer $elements)
    {
        $this->elements = $elements;
        return $this;
    }

    public function getElements(): ElementsContainer
    {
        return $this->elements;
    }
}