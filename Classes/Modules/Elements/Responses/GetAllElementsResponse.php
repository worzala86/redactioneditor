<?php

namespace App\Modules\Elements\Responses;

use App\Containers\ElementsContainer;
use App\Containers\SectionsContainer;
use App\Response;

class GetAllElementsResponse extends Response
{
    private $elements;

    /**
     * @param ElementsContainer $elements
     * @description Pole zawiera listę elementów
     * @return $this
     */
    public function setElements(ElementsContainer $elements)
    {
        $this->elements = $elements;
        return $this;
    }

    public function getElements(): ElementsContainer
    {
        return $this->elements;
    }
}