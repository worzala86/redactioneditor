<?php

namespace App\Modules\Elements\Responses;

use App\Response;
use App\Types\UUID;

class CreateElementResponse extends Response
{
    private $id;

    /**
     * @param UUID $id
     * @description Pole zawiera identyfikator nowejgo elementu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }
}