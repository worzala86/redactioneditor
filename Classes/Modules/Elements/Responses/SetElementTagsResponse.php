<?php

namespace App\Modules\Elements\Responses;

use App\Response;

class SetElementTagsResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Pole zawiera informację czy pomyślnie zapisano tagi do elementu
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }
}