<?php

namespace App\Modules\Elements\Responses;

use App\Response;

class UpdateElementResponse extends Response
{
    private $success;

    /**
     * @param bool $success
     * @description Pole zawiera informację czy pomyślnie wyedytowano element
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    public function getSuccess(): bool
    {
        return $this->success;
    }
}