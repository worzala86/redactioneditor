<?php

namespace App\Modules\Elements\Responses;

use App\Containers\HtmlElementContainer;
use App\Response;
use App\Types\UUID;

class GetElementResponse extends Response
{
    private $id;
    private $content;
    private $styleId;
    private $rootStyleId;
    private $dataSet;

    /**
     * @param UUID $id
     * @description Pole zawiera identyfikator elementu
     * @return $this
     */
    public function setId(UUID $id = null)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?UUID
    {
        return $this->id;
    }

    /**
     * @param HtmlElementContainer $content
     * @description Pole treść elementu
     * @return $this
     */
    public function setContent(HtmlElementContainer $content = null)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent(): ?HtmlElementContainer
    {
        return $this->content;
    }

    /**
     * @param UUID $styleId
     * @description Identyfikator stylu elementu
     * @return $this
     */
    public function setStyleId(string $styleId = null)
    {
        $this->styleId = $styleId;
        return $this;
    }

    public function getStyleId(): ?string
    {
        return $this->styleId;
    }

    /**
     * @param UUID $rootStyleId
     * @description Identyfikator głównego pliku ze stylem podglądowym
     * @return $this
     */
    public function setRootStyleId(UUID $rootStyleId = null)
    {
        $this->rootStyleId = $rootStyleId;
        return $this;
    }

    public function getRootStyleId(): ?UUID
    {
        return $this->rootStyleId;
    }

    /**
     * @param array $dataSet
     * @description Tablica z datasetem - wartości eleemntu
     * @return $this
     */
    public function setDataSet(array $dataSet)
    {
        $this->dataSet = $dataSet;
        return $this;
    }

    public function getDataSet(): array
    {
        return $this->dataSet;
    }
}