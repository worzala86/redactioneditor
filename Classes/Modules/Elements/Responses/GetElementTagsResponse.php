<?php

namespace App\Modules\Elements\Responses;

use App\Response;

class GetElementTagsResponse extends Response
{
    private $tags;

    /**
     * @param string $tags
     * @description Lista tagów
     * @return $this
     */
    public function setTags(string $tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getTags(): string
    {
        return $this->tags;
    }
}