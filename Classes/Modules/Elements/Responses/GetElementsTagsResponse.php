<?php

namespace App\Modules\Elements\Responses;

use App\Containers\TagsContainer;
use App\Response;

class GetElementsTagsResponse extends Response
{
    private $tags;

    /**
     * @param TagsContainer $tags
     * @description Lista wszystkich tagów w elementach
     * @return $this
     */
    public function setTags(TagsContainer $tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getTags(): TagsContainer
    {
        return $this->tags;
    }
}