<?php

namespace App\Modules\Elements\Handlers;

use App\Handler;
use App\Modules\Elements\Exceptions\ElementNotFoundException;
use App\Modules\Elements\Exceptions\FileNotFoundException;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Elements\Requests\AssignUserImageToElementRequest;
use App\Modules\Elements\Responses\AssignUserImageToElementResponse;
use App\Modules\Files\Models\FilesModel;

/**
 * Class AssignUserImageToElementHandler
 * @package App\Modules\Projects\Handlers
 * @description Metoda służy do przypisania zdjęcia elementowi
 */
class AssignUserImageToElementHandler extends Handler
{
    public function __invoke(AssignUserImageToElementRequest $request): AssignUserImageToElementResponse
    {
        $elementsModel = (new ElementsModel)
            ->where('user_id=?', $request->getCurrentUserId())
            ->load($request->getId(), true);

        if (!$elementsModel->isLoaded()) {
            throw new ElementNotFoundException;
        }

        (new ElementsModel)->startTransaction();

        $filesModel = (new FilesModel)
            ->where('user_id=?', $request->getCurrentUserId())
            ->load($request->getImageId(), true);

        if (!$filesModel->isLoaded()) {
            throw new FileNotFoundException;
        }

        $id = (new ElementsModel)
            ->setImageId($filesModel->getId())
            ->update($elementsModel->getId());

        (new ElementsModel)->commit();

        return (new AssignUserImageToElementResponse)
            ->setSuccess($id ? true : false);
    }
}