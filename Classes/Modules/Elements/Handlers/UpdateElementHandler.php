<?php

namespace App\Modules\Elements\Handlers;

use App\Database\DB;
use App\Handler;
use App\Modules\Elements\Exceptions\ElementNotFoundException;
use App\Modules\Elements\Exceptions\StyleNotFoundException;
use App\Modules\Elements\Models\ElementsDatasetsModel;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Elements\Requests\UpdateElementRequest;
use App\Modules\Elements\Responses\UpdateElementResponse;
use App\Modules\Pages\Models\PagesDatasetsModel;
use App\Modules\Pages\Models\PagesElementsModel;
use App\Modules\Styles\Models\StylesModel;
use App\Types\ShortUUID;
use App\Workers\HtmlWorker;

/**
 * Class UpdateElementHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do edycji elementu strony
 */
class UpdateElementHandler extends Handler
{
    public function __invoke(UpdateElementRequest $request): UpdateElementResponse
    {
        $elementsModel = (new ElementsModel)
            ->load($request->getId(), true);

        if(!$elementsModel->isLoaded()){
            throw new ElementNotFoundException;
        }

        $stylesModel = (new StylesModel)
            ->load($request->getRootStyleId(), true);

        $rootStylesId = null;
        if($request->getRootStyleId()&&!$stylesModel->isLoaded()){
            throw new StyleNotFoundException;
        }else{
            if($stylesModel->isLoaded()) {
                $rootStylesId = $stylesModel->getId();
            }
        }

        (new ElementsModel)->startTransaction();

        (new ElementsModel)
            ->setContentId(null)
            ->update($elementsModel->getId());

        $htmlWorker = new HtmlWorker;
        $htmlWorker->delete($elementsModel->getContentId());
        $htmlId = $htmlWorker->saveHtmlElements($request->getContent());

        $this->saveDataSet($request->getDataSet(), $elementsModel->getId());

        $updated = (new ElementsModel)
            ->setContentId($htmlId)
            ->setRootStyleId($rootStylesId)
            ->update($elementsModel->getId());

        (new ElementsModel)->commit();

        return (new UpdateElementResponse)
            ->setSuccess($updated);
    }

    private function saveDataSet($dataSet, $elementId){
        DB::get()->execute('delete from elements_datasets where element_id=?', $elementId);
        foreach ($dataSet as $elementPositionId => $dataset) {
            /*$pagesElementModel = (new PagesElementsModel)
                ->load($elementPositionId, true);
            if(!$pagesElementModel->isLoaded()){
                continue;
            }*/
            foreach ($dataset as $key => $dataSet) {
                (new ElementsDatasetsModel)
                    ->setKey(new ShortUUID($key))
                    ->setElementId($elementId)
                    ->setValue($dataSet)
                    ->setElementPositionId($elementId)
                    ->insert();
            }
        }
    }
}