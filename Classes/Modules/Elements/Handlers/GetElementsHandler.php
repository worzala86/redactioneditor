<?php

namespace App\Modules\Elements\Handlers;

use App\Containers\ElementContainer;
use App\Containers\ElementsContainer;
use App\Containers\ImageContainer;
use App\Containers\ImageThumbContainer;
use App\Handler;
use App\Modules\Elements\Collections\ElementsCollection;
use App\Modules\Elements\Requests\GetElementsRequest;
use App\Modules\Elements\Responses\GetElementsResponse;
use App\Modules\Files\Models\FilesModel;

/**
 * Class GetElementsHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do pobrania elementów składowych strony
 */
class GetElementsHandler extends Handler
{
    public function __invoke(GetElementsRequest $request): GetElementsResponse
    {
        $elementsCollection = (new ElementsCollection)
            ->setPage($request->getPage())
            ->setLimit($request->getLimit())
            ->setSortName($request->getSortName())
            ->setSortKind($request->getSortKind())
            ->setFilters($request->getFilters())
            ->where('hidden=?', 0);
        if ($request->getType() == 'custom') {
            $elementsCollection->where(' `user_id`=? ', $request->getCurrentUserId());
        } else if ($request->getType() == 'system') {
            $elementsCollection->where(' public=true ', null);
        }
        $elementsCollection
            ->where(' kind=? ', (string)$request->getKind())
            ->loadAll();

        $elements = (new ElementsContainer);
        foreach ($elementsCollection as $elementsModel) {
            $file = (new FilesModel)
                ->load($elementsModel->getImageId());
            $image = new ImageContainer;
            if ($file->isLoaded()) {
                $thumb = new ImageThumbContainer;
                $fileThumb = (new FilesModel)
                    ->load($file->getThumbId());
                if ($fileThumb->isLoaded()) {
                    $thumb->setUrl($fileThumb->getUrl())
                        ->setName($fileThumb->getName())
                        ->setId($fileThumb->getUuid())
                        ->setSize($fileThumb->getSize());
                }
                $image->setUrl($file->getUrl())
                    ->setName($file->getName())
                    ->setId($file->getUuid())
                    ->setSize($file->getSize())
                    ->setThumb($thumb);
            }
            $elements->add(
                (new ElementContainer)
                    ->setId($elementsModel->getUuid())
                    ->setImage($image)
            );
        }

        return (new GetElementsResponse)
            ->setElements($elements);
    }
}