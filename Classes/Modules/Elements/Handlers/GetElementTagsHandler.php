<?php

namespace App\Modules\Elements\Handlers;

use App\Handler;
use App\Modules\Elements\Exceptions\ElementNotFoundException;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Elements\Models\SectionsModel;
use App\Modules\Elements\Requests\CreateElementRequest;
use App\Modules\Elements\Requests\GetElementTagsRequest;
use App\Modules\Elements\Requests\SetElementTagsRequest;
use App\Modules\Elements\Requests\UpdateElementRequest;
use App\Modules\Elements\Responses\CreateElementResponse;
use App\Modules\Elements\Responses\GetElementTagsResponse;
use App\Modules\Elements\Responses\UpdateElementResponse;
use App\Types\UUID;
use App\Workers\HtmlWorker;
use App\Workers\TagsWorker;

/**
 * Class GetElementTagsHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do pobrania tagów elementu
 */
class GetElementTagsHandler extends Handler
{
    public function __invoke(GetElementTagsRequest $request): GetElementTagsResponse
    {
        $elementsModel = (new ElementsModel)
            ->load($request->getId(), true);

        if(!$elementsModel->isLoaded()){
            throw new ElementNotFoundException;
        }

        $tagsWorker = new TagsWorker;
        $tagsWorker->setElementId($elementsModel->getId());
        $tags = $tagsWorker->getTags();

        return (new GetElementTagsResponse)
            ->setTags($tags);
    }
}