<?php

namespace App\Modules\Elements\Handlers;

use App\Handler;
use App\Modules\Elements\Collections\ElementsDatasetsCollection;
use App\Modules\Elements\Collections\ElementsDatasetsViewCollection;
use App\Modules\Elements\Exceptions\ElementNotFoundException;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Elements\Requests\GetElementRequest;
use App\Modules\Elements\Responses\GetElementResponse;
use App\Modules\Pages\Collections\PagesDatasetsViewCollection;
use App\Modules\Pages\Collections\ProjectsDatasetsViewCollection;
use App\Modules\Styles\Models\StylesModel;
use App\Workers\HtmlWorker;

/**
 * Class GetElementHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do wczytania elementu strony
 */
class GetElementHandler extends Handler
{
    public function __invoke(GetElementRequest $request): GetElementResponse
    {
        $elementsModel = (new ElementsModel)
            ->load($request->getId(), true);

        if (!$elementsModel->isLoaded()) {
            throw new ElementNotFoundException;
        }

        $downloadTimes = $elementsModel->getDownloadTimes();
        $downloadTimes++;
        (new ElementsModel)
            ->setDownloadTimes($downloadTimes)
            ->update($request->getId(), true);

        $htmlWorker = new HtmlWorker;
        $content = $htmlWorker->loadHtmlElements($elementsModel->getContentId(), $elementsModel->getUuid());

        $styleId = null;
        $stylesModel = (new StylesModel)
            ->load($elementsModel->getStyleId());
        if($stylesModel->isLoaded()){
            $styleId = $stylesModel->getUuid();
        }

        $rootStyleId = null;
        $stylesModel = (new StylesModel)
            ->load($elementsModel->getRootStyleId());
        if($stylesModel->isLoaded()){
            $rootStyleId = $stylesModel->getUuid();
        }

        $dataSets = [];
        $elementsDatasetsViewCollection = (new ElementsDatasetsViewCollection)
            ->where('`element_id`=?', $elementsModel->getId())
            ->loadAll();
        foreach ($elementsDatasetsViewCollection as $elementDatasetViewModel){
            $dataSets[(string)$elementDatasetViewModel->getElementPositionId()][(string)$elementDatasetViewModel->getKey()] = $elementDatasetViewModel->getValue();
        }

        return (new GetElementResponse)
            ->setId($elementsModel->getUuid())
            ->setContent($content)
            ->setStyleId($styleId)
            ->setRootStyleId($rootStyleId)
            ->setDataSet($dataSets);
    }
}