<?php

namespace App\Modules\Elements\Handlers;

use App\Handler;
use App\Modules\Elements\Exceptions\ElementNotFoundException;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Elements\Requests\SetElementTagsRequest;
use App\Modules\Elements\Responses\SetElementTagsResponse;
use App\Workers\TagsWorker;

/**
 * Class SetElementTagsHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do edycji tagów elementu
 */
class SetElementTagsHandler extends Handler
{
    public function __invoke(SetElementTagsRequest $request): SetElementTagsResponse
    {
        $elementsModel = (new ElementsModel)
            ->load($request->getId(), true);

        if(!$elementsModel->isLoaded()){
            throw new ElementNotFoundException;
        }

        $tagsWorker = new TagsWorker;
        $tagsWorker->setElementId($elementsModel->getId());
        $tagsWorker->saveTagsToDatabase($request->getTags());

        return (new SetElementTagsResponse)
            ->setSuccess(true);
    }
}