<?php

namespace App\Modules\Elements\Handlers;

use App\Handler;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Elements\Requests\CreateElementRequest;
use App\Modules\Elements\Responses\CreateElementResponse;
use App\Modules\Styles\Models\StylesModel;
use App\Types\UUID;
use App\Workers\HtmlWorker;

/**
 * Class CreateElementHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do zapisu elementu strony
 */
class CreateElementHandler extends Handler
{
    public function __invoke(CreateElementRequest $request): CreateElementResponse
    {
        $htmlWorker = new HtmlWorker;
        $htmlId = $htmlWorker->saveHtmlElements($request->getContent());

        /*$sectionModel = null;
        if($request->getSection()&&!empty($request->getSection())){
            $sectionModel = (new SectionsModel)
                ->where('`key`=?', $request->getSection())
                ->load();
        }*/

        $styleId = null;
        if ($request->getStyleId()) {
            $stylesModel = (new StylesModel)
                ->load($request->getStyleId(), true);
            if ($stylesModel->isLoaded()) {
                $styleId = $stylesModel->getId();
            }
        }

        $rootStyleId = null;
        if ($request->getRootStyleId()) {
            $stylesModel = (new StylesModel)
                ->load($request->getRootStyleId(), true);
            if ($stylesModel->isLoaded()) {
                $rootStyleId = $stylesModel->getId();
            }
        }

        $uuid = UUID::fake();
        (new ElementsModel)
            ->setUuid($uuid)
            ->setUserId($request->getCurrentUserId())
            ->setKind($request->getKind())
            ->setContentId($htmlId)
            ->setStyleId($styleId)
            ->setRootStyleId($rootStyleId)
            ->insert();

        return (new CreateElementResponse)
            ->setId($uuid);
    }
}