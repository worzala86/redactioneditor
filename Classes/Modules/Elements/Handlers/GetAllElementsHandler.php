<?php

namespace App\Modules\Elements\Handlers;

use App\Containers\ElementContainer;
use App\Containers\ElementsContainer;
use App\Containers\ImageContainer;
use App\Containers\ImageThumbContainer;
use App\Containers\SectionContainer;
use App\Containers\SectionsContainer;
use App\File;
use App\Handler;
use App\Modules\Elements\Collections\ElementsCollection;
use App\Modules\Elements\Collections\SectionsCollection;
use App\Modules\Elements\Models\SectionsModel;
use App\Modules\Elements\Requests\GetAllElementsRequest;
use App\Modules\Elements\Responses\GetAllElementsResponse;
use App\Modules\Files\Models\FilesModel;
use App\Types\SortKind;
use App\Workers\TagsWorker;

/**
 * Class GetAllElementsHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do pobrania elementów składowych strony
 */
class GetAllElementsHandler extends Handler
{
    public function __invoke(GetAllElementsRequest $request): GetAllElementsResponse
    {
        $elementsCollection = (new ElementsCollection)
            ->setPage($request->getPage())
            ->setLimit($request->getLimit())
            ->setSortName($request->getSortName())
            ->setSortKind($request->getSortKind())
            ->setFilters($request->getFilters())
            ->where('hidden=?', 0)
            ->loadAll();

        $elements = new ElementsContainer;
        foreach ($elementsCollection as $elementsModel) {
            $file = (new File)->load($elementsModel->getImageId());
            $tags = null;
            $tagsWorker = new TagsWorker;
            $tagsWorker->setElementId($elementsModel->getId());
            $tags = $tagsWorker->getTagsArray();
            $elements->add(
                (new ElementContainer)
                    ->setId($elementsModel->getUuid())
                    ->setImage($file)
                    ->setKind($elementsModel->getKind())
                    ->setUserIsOwner($elementsModel->getUserId()==$request->getCurrentUserId())
                    ->setTags($tags)
            );
        }

        return (new GetAllElementsResponse)
            ->setElements($elements);
    }
}