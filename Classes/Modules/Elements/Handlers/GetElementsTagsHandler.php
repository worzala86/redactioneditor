<?php

namespace App\Modules\Elements\Handlers;

use App\Containers\TagContainer;
use App\Containers\TagsContainer;
use App\Handler;
use App\Modules\Elements\Requests\GetElementsTagsRequest;
use App\Modules\Elements\Responses\GetElementsTagsResponse;
use App\Workers\TagsWorker;

/**
 * Class GetElementsTagsHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do pobrania wszystkich tagów dla elementów
 */
class GetElementsTagsHandler extends Handler
{
    public function __invoke(GetElementsTagsRequest $request): GetElementsTagsResponse
    {
        $tagsWorker = new TagsWorker;
        $tags = $tagsWorker->getElementsTags();

        $tagsResult = new TagsContainer;
        foreach ($tags as $tag){
            $tagsResult->add(
                (new TagContainer)
                    ->setName($tag[0])
                    ->setCaption($tag[1])
            );
        }

        return (new GetElementsTagsResponse)
            ->setTags($tagsResult);
    }
}