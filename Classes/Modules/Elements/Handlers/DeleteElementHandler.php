<?php

namespace App\Modules\Elements\Handlers;

use App\Handler;
use App\Modules\Elements\Exceptions\ElementNotFoundException;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Elements\Requests\DeleteElementRequest;
use App\Modules\Elements\Responses\DeleteElementResponse;

/**
 * Class DeleteElementHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do usuwania elementu strony
 */
class DeleteElementHandler extends Handler
{
    public function __invoke(DeleteElementRequest $request): DeleteElementResponse
    {
        $elementsModel = (new ElementsModel)
            ->where('user_id=?', $request->getCurrentUserId())
            ->load($request->getId(), true);

        if(!$elementsModel->isLoaded()){
            throw new ElementNotFoundException;
        }

        $deleted = $elementsModel->delete();

        return (new DeleteElementResponse())
            ->setSuccess($deleted);
    }
}