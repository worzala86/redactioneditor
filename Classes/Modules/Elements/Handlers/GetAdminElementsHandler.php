<?php

namespace App\Modules\Elements\Handlers;

use App\Containers\ElementContainer;
use App\Containers\ElementsContainer;
use App\Containers\ImageContainer;
use App\Containers\ImageThumbContainer;
use App\Database\DB;
use App\File;
use App\Handler;
use App\Modules\Elements\Collections\ElementsCollection;
use App\Modules\Elements\Requests\GetAdminElementsRequest;
use App\Modules\Elements\Responses\GetAdminElementsResponse;
use App\Modules\Files\Models\FilesModel;
use App\Modules\User\Collections\UsersCollection;
use App\Modules\User\Models\UsersModel;
use App\Types\SortKind;

/**
 * Class GetAdminElementsHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do pobrania elementów składowych strony
 */
class GetAdminElementsHandler extends Handler
{
    public function __invoke(GetAdminElementsRequest $request): GetAdminElementsResponse
    {
        $usersCollection = (new UsersCollection)
            ->where('editor=?', 1)
            ->loadAll();
        $usersSql = [];
        $usersIds = [];
        foreach ($usersCollection as $usersModel) {
            $usersIds[] = $usersModel->getId();
            $usersSql[] = '?';
        }

        $elementsCollection = (new ElementsCollection)
            ->setPage($request->getPage())
            ->setLimit($request->getLimit())
            ->setSortName($request->getSortName())
            ->setSortKind($request->getSortKind())
            ->setFilters($request->getFilters())
            ->where('user_id in (' . join(', ', $usersSql) . ')', $usersIds)
            //->where('hidden=?', 0)
            ->setSortName('id')
            ->setSortKind(new SortKind('desc'))
            ->loadAll();

        $elements = (new ElementsContainer);
        foreach ($elementsCollection as $elementsModel) {
            /*$file = (new FilesModel)
                ->load($elementsModel->getImageId());
            $image = new ImageContainer;
            if ($file->isLoaded()) {
                $thumb = new ImageThumbContainer;
                $fileThumb = (new FilesModel)
                    ->load($file->getThumbId());
                if ($fileThumb->isLoaded()) {
                    $thumb->setUrl($fileThumb->getUrl())
                        ->setName($fileThumb->getName())
                        ->setId($fileThumb->getUuid())
                        ->setSize($fileThumb->getSize());
                }
                $image->setUrl($file->getUrl())
                    ->setName($file->getName())
                    ->setId($file->getUuid())
                    ->setSize($file->getSize())
                    ->setThumb($thumb);
            }*/
            $image = null;
            if($elementsModel->getImageId()) {
                $image = (new File)->load($elementsModel->getImageId());
            }
            $userModel = (new UsersModel)
                ->load($elementsModel->getUserId());
            $usedTimes = DB::get()->getOne('select count(*) from html where element_id=?', $elementsModel->getId());
            $elements->add(
                (new ElementContainer)
                    ->setId($elementsModel->getUuid())
                    ->setImage($image)
                    ->setUserName($userModel->getFirstName() . ' ' . $userModel->getLastName())
                    ->setDownloadTimes($elementsModel->getDownloadTimes())
                    ->setUsedTimes($usedTimes)
            );
        }

        return (new GetAdminElementsResponse)
            ->setElements($elements)
            ->setTotalCount($elementsCollection->getTotalCount());
    }
}