<?php

namespace App\Modules\Elements\Handlers;

use App\Handler;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Elements\Requests\CreateElementRequest;
use App\Modules\Elements\Responses\CreateElementResponse;
use App\Modules\Styles\Models\StylesModel;
use App\Types\UUID;
use App\Workers\HtmlWorker;

/**
 * Class CreateEmptyElementHandler
 * @package App\Modules\Elements\Handlers
 * @description Metoda służy do stworzenia elementu strony
 */
class CreateEmptyElementHandler extends Handler
{
    public function __invoke(CreateElementRequest $request): CreateElementResponse
    {
        //$htmlWorker = new HtmlWorker;
        //$htmlId = $htmlWorker->saveHtmlElements($request->getContent());

        /*$sectionModel = null;
        if($request->getSection()&&!empty($request->getSection())){
            $sectionModel = (new SectionsModel)
                ->where('`key`=?', $request->getSection())
                ->load();
        }*/

        $styleId = (new StylesModel)
            ->setUuid(UUID::fake())
            ->setUserId($request->getCurrentUserId())
            ->insert();

        $uuid = UUID::fake();
        (new ElementsModel)
            ->setUuid($uuid)
            ->setUserId($request->getCurrentUserId())
            ->setKind($request->getKind())
            ->setContentId(null)
            ->setStyleId($styleId)
            ->insert();

        return (new CreateElementResponse)
            ->setId($uuid);
    }
}