create table jsons(
    id int unsigned primary key auto_increment not null,
    json text not null
);
alter table elements drop json;
alter table elements add json bool default false;
alter table elements add index(json);