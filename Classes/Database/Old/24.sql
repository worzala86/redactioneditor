create or replace view elements_datasets_view as
SELECT element_id, `key`, `value`, elements.uuid as element_position_id FROM `elements_datasets`
    left join elements on elements.id=elements_datasets.element_position_id