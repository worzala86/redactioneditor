alter table projects drop foreign key `fk_projects_header_id`;
alter table projects drop foreign key `fk_projects_footer_id`;
alter table projects drop foreign key `fk_projects_sidebar_left_id`;
alter table projects drop foreign key `fk_projects_sidebar_right_id`;
alter table projects drop header_id;
alter table projects drop footer_id;
alter table projects drop sidebar_left_id;
alter table projects drop sidebar_right_id;