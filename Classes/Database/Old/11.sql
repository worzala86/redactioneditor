create or replace view pages_datasets_view as
select pages_datasets.page_id, `key`, `value`, pages_elements.uuid as element_position_id from pages_datasets
    left join pages_elements on pages_elements.id=pages_datasets.element_position_id;