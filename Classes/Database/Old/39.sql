create table scrapped_pages(
    id int unsigned primary key auto_increment,
    url varchar(250) not null,
    html text default null
);