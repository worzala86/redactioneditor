alter table projects_elements_fields drop column altId;
alter table pages_elements_fields drop column altId;
alter table projects_elements_fields add alt_id binary(4);
alter table pages_elements_fields add alt_id binary(4);