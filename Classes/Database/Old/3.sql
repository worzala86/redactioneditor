alter table users add active_project_id int unsigned default null;
alter table users add constraint `fk_users_active_project_id` foreign key (active_project_id)
    references projects(id) on update cascade on delete no action;