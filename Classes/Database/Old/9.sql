create table elements_datasets(
    id int unsigned primary key auto_increment not null,
    page_id int unsigned not null,
    constraint `fk_elements_datasets_page_id` foreign key (page_id) references pages(id) on update cascade on delete no action,
    `key` binary(4),
    unique (page_id, `key`),
    `value` text
)