alter table ip add country_name varchar(250);
alter table ip add region_name varchar(250);
alter table ip add city varchar(250);
alter table ip add zip_code varchar(50);
alter table ip add latitude float;
alter table ip add longitude float;