delete from pages_datasets;
alter table pages_datasets add element_position_id int unsigned not null;
alter table pages_datasets add constraint `fk_pages_datasets_element_position_id` foreign key (element_position_id)
    references pages_elements(id) on update cascade on delete no action;