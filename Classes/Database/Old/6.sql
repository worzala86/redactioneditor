create table pages_elements(
    id int unsigned primary key auto_increment not null,
    element_id int unsigned not null,
    constraint `fk_pages_elements_element_id` foreign key (element_id) references elements(id) on update cascade on delete no action,
    page_id int unsigned not null,
    constraint `fk_pages_elements_page_id` foreign key (page_id) references pages(id) on update cascade on delete no action,
    added int unsigned not null
)