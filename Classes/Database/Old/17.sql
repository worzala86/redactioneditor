create or replace view projects_datasets_view as
select projects_datasets.project_id, `key`, `value`, projects_elements.uuid as element_position_id from projects_datasets
     left join projects_elements on projects_elements.id=projects_datasets.element_position_id;