drop table `elements_datasets`;
CREATE TABLE `elements_datasets`
(
    `element_id`          int(10) unsigned NOT NULL,
    `key`                 binary(4)        NOT NULL,
    `value`               text,
    `element_position_id` int(10) unsigned NOT NULL,
    PRIMARY KEY (`element_id`, `key`, `element_position_id`) USING BTREE,
    KEY `fk_elements_datasets_element_position_id` (`element_position_id`),
    CONSTRAINT `fk_elements_datasets_element_id` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
);