<?php

namespace App\Database;

use App\Types\Time;

class Upgrade
{
    private $versions;

    public function __construct()
    {
        foreach (glob(__DIR__ . '/Versions/Old/*.sql') as $fileName) {
            preg_match('/Versions\/old\/([0-9]+).sql/', $fileName, $match);
            $this->versions[] = [(int)$match[1], $fileName];
        }
        foreach (glob(__DIR__ . '/Versions/*.sql') as $fileName) {
            preg_match('/Versions\/([0-9]+).sql/', $fileName, $match);
            $this->versions[] = [(int)$match[1], $fileName];
        }
        array_multisort($this->versions);
    }

    public function upgrade()
    {
        $version = (int)DB::get()->getOne('select `number` from version order by id desc limit 1');
        foreach ($this->versions as $newVersion) {
            if ($newVersion[0] > $version) {
                $fileContent = file_get_contents($newVersion[1]);
                $lines = explode(';', $fileContent);
                DB::get()->execute('start transaction');
                foreach ($lines as $line) {
                    if(empty($line)){
                        continue;
                    }
                    $result = DB::get()->execute($line);
                }
                if (!$result) {
                    throw new \Exception('Błąd zapytania');
                }
                DB::get()->execute('insert into version set `number`=?, `date`=?', [$newVersion[0], Time::fake()]);
                DB::get()->execute('commit');
            }
        }
    }

    private function getStringNumber($version)
    {
        $version = (string)$version;
        $textVersion = [];
        for ($index = 0; $index < 3 - strlen($version); $index++) {
            $textVersion[] = '0';
        }
        for ($index = 0; $index < 3; $index++) {
            if ($index < strlen($version)) {
                $textVersion[] = $version[$index];
            }
        }
        return join('.', $textVersion);
    }

    public function version(): string
    {
        $version = DB::get()->getOne('select `number` from version order by id desc limit 1');
        return $this->getStringNumber($version);
    }

    public function newVersion(): string
    {
        $version = $this->versions[count($this->versions) - 1][0];
        return $this->getStringNumber($version);
    }
}