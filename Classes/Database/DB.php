<?php

namespace App\Database;

class DB
{
    static $connections;

    static public function get(string $connectionNameParameter = null): Database{
        if(!isset(self::$connections)){
            foreach(DATABASE_CONNECTION as $connectionName=>$connectionAccess){
                self::$connections[$connectionName] = new Database(
                    $connectionAccess['HOST'],
                    $connectionAccess['DATABASE'],
                    $connectionAccess['USER'],
                    $connectionAccess['PASSWORD']);
            }
        }
        if(!isset($connectionNameParameter)){
            $connectionNameParameter = DEFAULT_DATABASE_CONNECTION;
        }
        return self::$connections[$connectionNameParameter];
    }
}