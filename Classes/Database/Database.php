<?php

namespace App\Database;

use PDO;

class Database
{

    public $dbh;

    public function __construct($host, $name, $user, $password)
    {
        $this->dbh = new PDO("mysql:host=" . $host . ";dbname=" . $name, $user, $password);
        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->dbh->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        $this->execute('set names utf8mb4');
    }

    public function execute(string $command, $params = null)
    {
        $result = false;
        $params = $this->prepereParams($params);
        $sth = $this->dbh->prepare($command);
        //try {
            $result = $sth->execute($params);
        /*} catch (\PDOException $e) {
            if (DEBUG) {
                print_r([$e->getMessage(), $command, $params]);
                exit;
            }
        }*/
        return $result;
    }

    public function getOne(string $command, $params = null)
    {
        $params = $this->prepereParams($params);
        $sth = $this->dbh->prepare($command);
        //try {
            $sth->execute($params);
        /*} catch (\PDOException $e) {
            if (DEBUG) {
                print_r([$e->getMessage(), $command, $params]);
                exit;
            }
        }*/
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            return null;
        }
        foreach ($result as $res) {
            return $res;
        }
    }

    public function getAll(string $command, $params = null)
    {
        $params = $this->prepereParams($params);
        $sth = $this->dbh->prepare($command);
        //try {
            $sth->execute($params);
        /*} catch (\PDOException $e) {
            if (DEBUG) {
                print_r([$e->getMessage(), $command, $params]);
                exit;
            }
        }*/
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getRow(string $command, $params = null)
    {
        $params = $this->prepereParams($params);
        $sth = $this->dbh->prepare($command);
        //try {
            $sth->execute($params);
        /*} catch (\PDOException $e) {
            if (DEBUG) {
                print_r([$e->getMessage(), $command, $params]);
            }
        }*/
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    private function prepereParams($params)
    {
        if (is_null($params)) {
            $params = [];
        } else if (!is_array($params)) {
            $params = [$params];
        }
        return $params;
    }

    public function insertId()
    {
        return $this->dbh->lastInsertId();
    }
}