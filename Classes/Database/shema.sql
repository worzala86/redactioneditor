-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: redaction
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `components`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(20) DEFAULT NULL,
  `description` text,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `file` varchar(25) NOT NULL COMMENT 'Nazwa pliku z fragmentem html używanym przez edytor',
  `kind` varchar(90) NOT NULL COMMENT 'Rodzaj pola. W edycji komponentu jest używane to pole do rodzaju pola wprowadzania danych.',
  `simple_view` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `key` (`key`) USING BTREE,
  KEY `fk_components_added_by` (`added_by`),
  KEY `fk_components_deleted_by` (`deleted_by`),
  KEY `fk_components_updated_by` (`updated_by`),
  KEY `fk_components_added_ip_id` (`added_ip_id`),
  KEY `fk_components_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_components_updated_ip_id` (`updated_ip_id`),
  CONSTRAINT `fk_components_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_components_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_components_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_components_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_components_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_components_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `components`
--

LOCK TABLES `components` WRITE;
/*!40000 ALTER TABLE `components` DISABLE KEYS */;
INSERT INTO `components` VALUES (1,'container','Standardowy kontener dla innych elementów. Wnętrze elementu można zapętlać datasetami.','$�ky�韟',4294967295,1,1,0,NULL,NULL,1552595873,1,1,'Kontener','container.html','text',0),(2,'header-1','Nagłowek pierwszego poziomu.','I��`k��,',4294967295,1,1,0,NULL,NULL,1556607574,1,1,'Nagłówek x1','header-1.html','text',1),(3,'header-2','Nagłowek drugiego poziomu.','�Nt�=g��',4294967295,1,1,0,NULL,NULL,1556607570,1,1,'Nagłówek x2','header-2.html','text',1),(4,'header-3','Nagłowek trzeciego poziomu.','~|��\\��',4294967295,1,1,0,NULL,NULL,1556607566,1,1,'Nagłówek x3','header-3.html','text',1),(5,'header-4','Nagłowek czwartego poziomu.','���e��U',4294967295,1,1,0,NULL,NULL,1556607562,1,1,'Nagłówek x4','header-4.html','text',1),(6,'header-5','Nagłowek piątego poziomu.','�P�;8!',4294967295,1,1,0,NULL,NULL,1556607558,1,1,'Nagłówek x5','header-5.html','text',1),(7,'header-6','Nagłowek szóstego poziomu.','�z�Q,SBJ',4294967295,1,1,0,NULL,NULL,1556607554,1,1,'Nagłówek x6','header-6.html','text',1),(8,'paragraph','Paragfaf - służy do przechowywania na ogół opisu lub dłuższego tekstu.','�d�C8iW',4294967295,1,1,0,NULL,NULL,1556607587,1,1,'Artykuł - paragraf','paragraph.html','textarea',1),(9,'image','Element jest obrazkiem.','~���t��',4294967295,1,1,0,NULL,NULL,1556607601,1,1,'Obrazek','image.html','image',1),(10,'link','Link do innej podstrony.','7�S�����',4294967295,1,1,0,NULL,NULL,1556607539,1,1,'Odnośnik','link.html','text',1),(11,'pagination','Komponent umożliwia przechodzenie pomiedzy podstronami dataseta.','���%���W',4294967295,1,1,0,NULL,NULL,1552503197,1,1,'Stronnicowanie','pagination.html','text',0),(12,'button','Standardowy przycisk który może służyć na przykład jako link.','tſ[�C�',4294967295,1,1,0,NULL,NULL,1556840009,1,1,'Przycisk','button.html','text',1),(15,'input','Element służy do wprowadzania danych np. w formularzu.','��i\Zܒb�',1553482661,1,1,0,NULL,NULL,NULL,NULL,1,'Input','input.html','text',0),(16,'label','Etykieta jest najczęściej wykorzystywana do pokazania tytułu Inputa.','��\'{��',1553560800,1,1,0,NULL,NULL,1556607594,1,1,'Etykieta','label.html','text',1),(17,'textarea','Eleement służy do wprowadzania długiego tekstu do storny.','�ie�٩��',1553572815,1,1,0,NULL,NULL,1556607029,1,1,'Textarea','textarea.html','textarea',0),(18,'html','Do tego elementu można wstawić własny kod html np. z mapami Google','�V\Z�k\"',1553573910,1,1,0,NULL,NULL,1553931198,1,1,'Kod HTML','html.html','text',0),(19,'article','Jest to element przeznaczony specjalnie dla artykułu.',']��h6�aG',1555283241,1,1,0,NULL,NULL,1557621216,1,1,'Artykuł','article.html','textarea',1);
/*!40000 ALTER TABLE `components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crm_pages`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crm_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_crm_pages_added_by` (`added_by`),
  KEY `fk_crm_pages_deleted_by` (`deleted_by`),
  KEY `fk_crm_pages_updated_by` (`updated_by`),
  KEY `fk_crm_pages_added_ip_id` (`added_ip_id`),
  KEY `fk_crm_pages_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_crm_pages_updated_ip_id` (`updated_ip_id`),
  CONSTRAINT `fk_crm_pages_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_crm_pages_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_crm_pages_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_crm_pages_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_crm_pages_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_crm_pages_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crm_pages`
--

LOCK TABLES `crm_pages` WRITE;
/*!40000 ALTER TABLE `crm_pages` DISABLE KEYS */;
INSERT INTO `crm_pages` VALUES (1,'b3�[�3�',1550452397,1,1,1553981789,1,1,0,NULL,NULL,'/regulamin','Regulamin','<center>Regulamin</center>\n<br>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tincidunt aliquam ligula. Nam at nisl id arcu molestie molestie sed nec lacus. Ut ut dignissim arcu. Curabitur in hendrerit ante. Vestibulum dapibus porttitor posuere. Etiam nibh nibh, finibus sed ligula eget, dictum ornare mauris. Quisque ornare tortor lorem. Donec dictum justo nec magna pharetra, sed mattis purus eleifend. Nunc mi tellus, tempus vitae ipsum a, scelerisque posuere augue. Phasellus ullamcorper velit id tortor porta, eu sagittis risus pulvinar. In ornare euismod leo a ultrices. Maecenas auctor nisi sit amet lacus dictum imperdiet. Curabitur interdum cursus enim eu venenatis.\n<br>\nSuspendisse egestas ex eleifend lacus posuere, non facilisis est cursus. Curabitur auctor metus sit amet ultricies pretium. Quisque volutpat purus et risus suscipit consectetur. Etiam dictum, est id mattis convallis, libero nisi vehicula libero, vel pharetra mi sapien nec odio. Cras vitae eleifend dolor. Nam porttitor augue tortor, eu commodo quam gravida at. Vestibulum tempus ullamcorper eros, ut posuere nulla pellentesque non. Nulla faucibus eget arcu id rutrum. Integer sed quam non lacus accumsan posuere. In leo libero, pulvinar convallis auctor et, bibendum et purus.\n<br>\nMorbi eget lacus tempus, fermentum nibh vitae, lacinia urna. Quisque dui libero, faucibus non tortor ut, euismod sodales urna. Fusce lacinia pretium malesuada. Sed placerat auctor nibh, at commodo nulla sodales a. Vestibulum sed augue at justo iaculis lacinia. Morbi finibus porttitor dui eu molestie. Curabitur ut odio quis odio malesuada congue. In feugiat elit vitae leo consectetur sodales. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus tempor ac diam nec tempus. Duis suscipit eros et lacus finibus, in congue nibh porttitor. Sed congue mi porta nulla tristique rhoncus a quis arcu. Morbi sed ligula felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque et auctor lectus.\n<br>\nCurabitur auctor vulputate lobortis. Morbi faucibus metus consectetur, euismod ligula non, ultricies est. Cras ut augue dignissim, semper massa non, consequat nibh. Mauris nisl velit, varius ac pharetra sit amet, faucibus quis nisi. Aliquam condimentum mollis leo non semper. Nullam aliquam cursus turpis. Proin et feugiat est. Fusce mollis ligula id turpis ultricies, in consequat mi auctor. Etiam eget hendrerit tellus, id pellentesque massa. Aliquam pulvinar dolor in ipsum tincidunt, vitae egestas nibh porta. Vivamus non erat vitae tellus pretium vehicula id sit amet lectus. Nunc vitae libero venenatis, convallis enim eget, pretium nibh. Nulla vel cursus nisl. Proin a consectetur est, ut gravida augue.\n<br>\nSed sapien tortor, placerat eget dolor eu, consectetur sodales orci. Etiam id ex at velit rutrum dignissim. Proin vel dolor a elit laoreet accumsan. Nam imperdiet porta dui ac consequat. Vivamus ac porta libero. Maecenas pharetra, orci a ullamcorper sagittis, odio risus tempor dolor, vitae consequat magna libero sed elit. Duis ullamcorper bibendum risus, et laoreet purus tincidunt ac. Mauris nec arcu nisi. Vivamus blandit arcu et leo sodales ultricies ullamcorper non orci. Aliquam vel ipsum nec nibh varius laoreet. Nam pharetra purus feugiat justo commodo commodo. Donec ipsum odio, elementum sit amet tincidunt eu, condimentum quis purus. Quisque ac consequat risus. Etiam consectetur commodo lectus sed faucibus.'),(2,',Dfm��K\n',1550452421,1,1,1553981785,1,1,0,NULL,NULL,'/polityka-prywatnosci','Polityka prywatności','<center>Polityka prywatności</center>\n<br>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tincidunt aliquam ligula. Nam at nisl id arcu molestie molestie sed nec lacus. Ut ut dignissim arcu. Curabitur in hendrerit ante. Vestibulum dapibus porttitor posuere. Etiam nibh nibh, finibus sed ligula eget, dictum ornare mauris. Quisque ornare tortor lorem. Donec dictum justo nec magna pharetra, sed mattis purus eleifend. Nunc mi tellus, tempus vitae ipsum a, scelerisque posuere augue. Phasellus ullamcorper velit id tortor porta, eu sagittis risus pulvinar. In ornare euismod leo a ultrices. Maecenas auctor nisi sit amet lacus dictum imperdiet. Curabitur interdum cursus enim eu venenatis.\n<br>\nSuspendisse egestas ex eleifend lacus posuere, non facilisis est cursus. Curabitur auctor metus sit amet ultricies pretium. Quisque volutpat purus et risus suscipit consectetur. Etiam dictum, est id mattis convallis, libero nisi vehicula libero, vel pharetra mi sapien nec odio. Cras vitae eleifend dolor. Nam porttitor augue tortor, eu commodo quam gravida at. Vestibulum tempus ullamcorper eros, ut posuere nulla pellentesque non. Nulla faucibus eget arcu id rutrum. Integer sed quam non lacus accumsan posuere. In leo libero, pulvinar convallis auctor et, bibendum et purus.\n<br>\nMorbi eget lacus tempus, fermentum nibh vitae, lacinia urna. Quisque dui libero, faucibus non tortor ut, euismod sodales urna. Fusce lacinia pretium malesuada. Sed placerat auctor nibh, at commodo nulla sodales a. Vestibulum sed augue at justo iaculis lacinia. Morbi finibus porttitor dui eu molestie. Curabitur ut odio quis odio malesuada congue. In feugiat elit vitae leo consectetur sodales. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus tempor ac diam nec tempus. Duis suscipit eros et lacus finibus, in congue nibh porttitor. Sed congue mi porta nulla tristique rhoncus a quis arcu. Morbi sed ligula felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque et auctor lectus.\n<br>\nCurabitur auctor vulputate lobortis. Morbi faucibus metus consectetur, euismod ligula non, ultricies est. Cras ut augue dignissim, semper massa non, consequat nibh. Mauris nisl velit, varius ac pharetra sit amet, faucibus quis nisi. Aliquam condimentum mollis leo non semper. Nullam aliquam cursus turpis. Proin et feugiat est. Fusce mollis ligula id turpis ultricies, in consequat mi auctor. Etiam eget hendrerit tellus, id pellentesque massa. Aliquam pulvinar dolor in ipsum tincidunt, vitae egestas nibh porta. Vivamus non erat vitae tellus pretium vehicula id sit amet lectus. Nunc vitae libero venenatis, convallis enim eget, pretium nibh. Nulla vel cursus nisl. Proin a consectetur est, ut gravida augue.\n<br>\nSed sapien tortor, placerat eget dolor eu, consectetur sodales orci. Etiam id ex at velit rutrum dignissim. Proin vel dolor a elit laoreet accumsan. Nam imperdiet porta dui ac consequat. Vivamus ac porta libero. Maecenas pharetra, orci a ullamcorper sagittis, odio risus tempor dolor, vitae consequat magna libero sed elit. Duis ullamcorper bibendum risus, et laoreet purus tincidunt ac. Mauris nec arcu nisi. Vivamus blandit arcu et leo sodales ultricies ullamcorper non orci. Aliquam vel ipsum nec nibh varius laoreet. Nam pharetra purus feugiat justo commodo commodo. Donec ipsum odio, elementum sit amet tincidunt eu, condimentum quis purus. Quisque ac consequat risus. Etiam consectetur commodo lectus sed faucibus.'),(3,'v!\'�,��',1550452438,1,1,1553981782,1,1,0,NULL,NULL,'/ciasteczka','Ciasteczka','<center>Ciasteczka</center>\n<br>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tincidunt aliquam ligula. Nam at nisl id arcu molestie molestie sed nec lacus. Ut ut dignissim arcu. Curabitur in hendrerit ante. Vestibulum dapibus porttitor posuere. Etiam nibh nibh, finibus sed ligula eget, dictum ornare mauris. Quisque ornare tortor lorem. Donec dictum justo nec magna pharetra, sed mattis purus eleifend. Nunc mi tellus, tempus vitae ipsum a, scelerisque posuere augue. Phasellus ullamcorper velit id tortor porta, eu sagittis risus pulvinar. In ornare euismod leo a ultrices. Maecenas auctor nisi sit amet lacus dictum imperdiet. Curabitur interdum cursus enim eu venenatis.\n<br>\nSuspendisse egestas ex eleifend lacus posuere, non facilisis est cursus. Curabitur auctor metus sit amet ultricies pretium. Quisque volutpat purus et risus suscipit consectetur. Etiam dictum, est id mattis convallis, libero nisi vehicula libero, vel pharetra mi sapien nec odio. Cras vitae eleifend dolor. Nam porttitor augue tortor, eu commodo quam gravida at. Vestibulum tempus ullamcorper eros, ut posuere nulla pellentesque non. Nulla faucibus eget arcu id rutrum. Integer sed quam non lacus accumsan posuere. In leo libero, pulvinar convallis auctor et, bibendum et purus.\n<br>\nMorbi eget lacus tempus, fermentum nibh vitae, lacinia urna. Quisque dui libero, faucibus non tortor ut, euismod sodales urna. Fusce lacinia pretium malesuada. Sed placerat auctor nibh, at commodo nulla sodales a. Vestibulum sed augue at justo iaculis lacinia. Morbi finibus porttitor dui eu molestie. Curabitur ut odio quis odio malesuada congue. In feugiat elit vitae leo consectetur sodales. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus tempor ac diam nec tempus. Duis suscipit eros et lacus finibus, in congue nibh porttitor. Sed congue mi porta nulla tristique rhoncus a quis arcu. Morbi sed ligula felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque et auctor lectus.\n<br>\nCurabitur auctor vulputate lobortis. Morbi faucibus metus consectetur, euismod ligula non, ultricies est. Cras ut augue dignissim, semper massa non, consequat nibh. Mauris nisl velit, varius ac pharetra sit amet, faucibus quis nisi. Aliquam condimentum mollis leo non semper. Nullam aliquam cursus turpis. Proin et feugiat est. Fusce mollis ligula id turpis ultricies, in consequat mi auctor. Etiam eget hendrerit tellus, id pellentesque massa. Aliquam pulvinar dolor in ipsum tincidunt, vitae egestas nibh porta. Vivamus non erat vitae tellus pretium vehicula id sit amet lectus. Nunc vitae libero venenatis, convallis enim eget, pretium nibh. Nulla vel cursus nisl. Proin a consectetur est, ut gravida augue.\n<br>\nSed sapien tortor, placerat eget dolor eu, consectetur sodales orci. Etiam id ex at velit rutrum dignissim. Proin vel dolor a elit laoreet accumsan. Nam imperdiet porta dui ac consequat. Vivamus ac porta libero. Maecenas pharetra, orci a ullamcorper sagittis, odio risus tempor dolor, vitae consequat magna libero sed elit. Duis ullamcorper bibendum risus, et laoreet purus tincidunt ac. Mauris nec arcu nisi. Vivamus blandit arcu et leo sodales ultricies ullamcorper non orci. Aliquam vel ipsum nec nibh varius laoreet. Nam pharetra purus feugiat justo commodo commodo. Donec ipsum odio, elementum sit amet tincidunt eu, condimentum quis purus. Quisque ac consequat risus. Etiam consectetur commodo lectus sed faucibus.');
/*!40000 ALTER TABLE `crm_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datasets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) DEFAULT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `name` (`name`,`project_id`,`deleted`,`user_id`),
  KEY `fk_datasets_added_by` (`added_by`),
  KEY `fk_datasets_deleted_by` (`deleted_by`),
  KEY `fk_datasets_updated_by` (`updated_by`),
  KEY `fk_datasets_added_ip_id` (`added_ip_id`),
  KEY `fk_datasets_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_datasets_updated_ip_id` (`updated_ip_id`),
  KEY `fk_datasets_project_id` (`project_id`),
  KEY `fk_datasets_user_id` (`user_id`),
  CONSTRAINT `fk_datasets_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datasets`
--

LOCK TABLES `datasets` WRITE;
/*!40000 ALTER TABLE `datasets` DISABLE KEYS */;
/*!40000 ALTER TABLE `datasets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datasets_fields`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasets_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) DEFAULT NULL,
  `datasets_id` int(10) unsigned NOT NULL,
  `type` varchar(250) DEFAULT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `name` (`name`,`datasets_id`,`deleted`),
  KEY `fk_datasets_fields_added_by` (`added_by`),
  KEY `fk_datasets_fields_deleted_by` (`deleted_by`),
  KEY `fk_datasets_fields_updated_by` (`updated_by`),
  KEY `fk_datasets_fields_added_ip_id` (`added_ip_id`),
  KEY `fk_datasets_fields_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_datasets_fields_updated_ip_id` (`updated_ip_id`),
  KEY `fk_datasets_fields_datasets_id` (`datasets_id`),
  CONSTRAINT `fk_datasets_fields_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_datasets_id` FOREIGN KEY (`datasets_id`) REFERENCES `datasets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datasets_fields`
--

LOCK TABLES `datasets_fields` WRITE;
/*!40000 ALTER TABLE `datasets_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `datasets_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `public` tinyint(1) DEFAULT '0',
  `image_id` int(10) unsigned DEFAULT NULL,
  `kind` varchar(20) DEFAULT NULL,
  `content_id` int(10) unsigned DEFAULT NULL,
  `download_times` int(10) unsigned DEFAULT '0' COMMENT 'Jest to liczba pobrań. Określa ona ile razy w eddytorze wskazany element został prawdopodobnie użyty',
  `style_id` int(10) unsigned DEFAULT NULL,
  `root_style_id` int(10) unsigned DEFAULT NULL COMMENT 'Jest to domyślny główny styl wybranego elementu - tylko w celach podglądowych',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `public` (`public`),
  KEY `fk_elements_added_by` (`added_by`),
  KEY `fk_elements_deleted_by` (`deleted_by`),
  KEY `fk_elements_updated_by` (`updated_by`),
  KEY `fk_elements_added_ip_id` (`added_ip_id`),
  KEY `fk_elements_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_elements_updated_ip_id` (`updated_ip_id`),
  KEY `fk_elements_user_id` (`user_id`),
  KEY `fk_elements_image_id` (`image_id`),
  KEY `fk_elements_content_id` (`content_id`),
  KEY `fk_elements_style_id` (`style_id`),
  KEY `fk_elements_root_style_id` (`root_style_id`),
  CONSTRAINT `fk_elements_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_content_id` FOREIGN KEY (`content_id`) REFERENCES `html` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_image_id` FOREIGN KEY (`image_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_root_style_id` FOREIGN KEY (`root_style_id`) REFERENCES `styles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_style_id` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements`
--

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `thumb_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_files_added_by` (`added_by`),
  KEY `fk_files_deleted_by` (`deleted_by`),
  KEY `fk_files_updated_by` (`updated_by`),
  KEY `fk_files_added_ip_id` (`added_ip_id`),
  KEY `fk_files_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_files_updated_ip_id` (`updated_ip_id`),
  KEY `fk_files_user_id` (`user_id`),
  KEY `fk_files_thumb_id` (`thumb_id`),
  CONSTRAINT `fk_files_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_thumb_id` FOREIGN KEY (`thumb_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `html`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `component_id` int(10) unsigned NOT NULL COMMENT 'Pole zawiera wskażnik do komponentu do tabeli components',
  `html_id` binary(4) DEFAULT NULL,
  `alt_id` binary(4) DEFAULT NULL,
  `html_class_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL COMMENT 'Pole zawiera datę ostatniej aktualizacji głównego wpisu do bazy. Takiego który nie zawiera rodzica.',
  `field` varchar(90) DEFAULT NULL COMMENT 'jest to kod pola z dataseta który trzyma treść lub nazwa pola z dataseta',
  `element_id` int(10) unsigned DEFAULT NULL,
  `show` tinyint(1) DEFAULT '1' COMMENT 'Jak jest true to element html jest wyświetlany na stronie',
  `data_set_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Jeżeli wartość jest true to znaczy że element można podpiąć pod selectora do dataSeta. Jego treść będzie rotowalna.',
  `background_active` tinyint(1) DEFAULT '0' COMMENT 'Jeżeli jest true to w edycji SimpleView można temu elementowi ustawić kolor tła lub obrazek',
  PRIMARY KEY (`id`),
  KEY `fk_html_parent_id` (`parent_id`),
  KEY `fk_html_component_id` (`component_id`),
  KEY `fk_html_html_class_id` (`html_class_id`),
  KEY `fk_html_element_id` (`element_id`),
  CONSTRAINT `fk_html_component_id` FOREIGN KEY (`component_id`) REFERENCES `components` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_html_element_id` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_html_html_class_id` FOREIGN KEY (`html_class_id`) REFERENCES `html_classes` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_html_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `html` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `html`
--

LOCK TABLES `html` WRITE;
/*!40000 ALTER TABLE `html` DISABLE KEYS */;
/*!40000 ALTER TABLE `html` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `html_classes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `html_classes`
--

LOCK TABLES `html_classes` WRITE;
/*!40000 ALTER TABLE `html_classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `html_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `html_params`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_params` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `html_id` int(10) unsigned NOT NULL,
  `value` varchar(250) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_html_params_html_id` (`html_id`),
  CONSTRAINT `fk_html_params_html_id` FOREIGN KEY (`html_id`) REFERENCES `html` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `html_params`
--

LOCK TABLES `html_params` WRITE;
/*!40000 ALTER TABLE `html_params` DISABLE KEYS */;
/*!40000 ALTER TABLE `html_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `html_params_datasets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_params_datasets` (
  `html_id` int(10) unsigned NOT NULL,
  `key` varchar(8) NOT NULL,
  `value` varchar(250) NOT NULL,
  KEY `fk_html_params_datasets_html_id` (`html_id`),
  CONSTRAINT `fk_html_params_datasets_html_id` FOREIGN KEY (`html_id`) REFERENCES `html` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `html_params_datasets`
--

LOCK TABLES `html_params_datasets` WRITE;
/*!40000 ALTER TABLE `html_params_datasets` DISABLE KEYS */;
/*!40000 ALTER TABLE `html_params_datasets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `html_params_text`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_params_text` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `html_id` int(10) unsigned NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `fk_html_params_text_html_id` (`html_id`),
  CONSTRAINT `fk_html_params_text_html_id` FOREIGN KEY (`html_id`) REFERENCES `html` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `html_params_text`
--

LOCK TABLES `html_params_text` WRITE;
/*!40000 ALTER TABLE `html_params_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `html_params_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `file_id` int(10) unsigned NOT NULL,
  `public` tinyint(1) DEFAULT '0',
  `pixabay_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `deleted` (`deleted`,`file_id`,`user_id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `public` (`public`),
  KEY `fk_images_added_by` (`added_by`),
  KEY `fk_images_deleted_by` (`deleted_by`),
  KEY `fk_images_updated_by` (`updated_by`),
  KEY `fk_images_added_ip_id` (`added_ip_id`),
  KEY `fk_images_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_images_updated_ip_id` (`updated_ip_id`),
  KEY `fk_images_user_id` (`user_id`),
  KEY `fk_images_file_id` (`file_id`),
  KEY `fk_images_pixabay_id` (`pixabay_id`),
  CONSTRAINT `fk_images_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_file_id` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_pixabay_id` FOREIGN KEY (`pixabay_id`) REFERENCES `pixabay` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ip`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `date` int(10) unsigned NOT NULL,
  `session_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip` (`ip`,`user_id`,`session_id`),
  KEY `fk_ip_user_id` (`user_id`),
  KEY `fk_ip_session_id` (`session_id`),
  CONSTRAINT `fk_ip_session_id` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_ip_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip`
--

LOCK TABLES `ip` WRITE;
/*!40000 ALTER TABLE `ip` DISABLE KEYS */;
INSERT INTO `ip` VALUES (1,1,NULL,1558820801,NULL),(2,1,1,1558823042,1);
/*!40000 ALTER TABLE `ip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `url` varchar(90) DEFAULT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `pagination_url_param` int(11) DEFAULT NULL COMMENT 'parametr x jest do paginacji',
  `pagination_limit` int(11) DEFAULT NULL COMMENT 'Limit elementów na stronę dla paginacji',
  `user_id` int(10) unsigned NOT NULL,
  `content_id` int(10) unsigned DEFAULT NULL,
  `pagination_url` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `project_id` (`project_id`,`url`,`deleted`,`user_id`),
  KEY `projects_id` (`project_id`),
  KEY `fk_pages_added_by` (`added_by`),
  KEY `fk_pages_deleted_by` (`deleted_by`),
  KEY `fk_pages_updated_by` (`updated_by`),
  KEY `fk_pages_added_ip_id` (`added_ip_id`),
  KEY `fk_pages_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_pages_updated_ip_id` (`updated_ip_id`),
  KEY `fk_pages_user_id` (`user_id`),
  KEY `fk_pages_content_id` (`content_id`),
  CONSTRAINT `fk_pages_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_content_id` FOREIGN KEY (`content_id`) REFERENCES `html` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pixabay`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pixabay` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pixa_id` int(10) unsigned NOT NULL,
  `image` varchar(250) DEFAULT NULL COMMENT 'zdjęcie w rozdzielczości w1280',
  `thumb` varchar(250) NOT NULL COMMENT 'zdjęcie w rozdzielczości w640',
  `link` varchar(250) NOT NULL COMMENT 'link do strony ze zdjęciem w Pixabay',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pixa_id` (`pixa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pixabay`
--

LOCK TABLES `pixabay` WRITE;
/*!40000 ALTER TABLE `pixabay` DISABLE KEYS */;
/*!40000 ALTER TABLE `pixabay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `public` tinyint(1) DEFAULT '0' COMMENT 'Jeżeli jest true to projekt jest publicznie dostępny w katalogu projektów',
  `image_id` int(10) unsigned DEFAULT NULL,
  `domain` varchar(250) DEFAULT NULL COMMENT 'Pole z domeną pod którą będzie się znajdował projekt',
  `main` tinyint(1) DEFAULT '0' COMMENT 'Jeżeli jest true to projekt jest wyświetlany dla niezalogowanych na stronie głównej',
  `header_id` int(10) unsigned DEFAULT NULL,
  `footer_id` int(10) unsigned DEFAULT NULL,
  `sidebar_left_id` int(10) unsigned DEFAULT NULL,
  `sidebar_right_id` int(10) unsigned DEFAULT NULL,
  `config` text,
  `show_left_sidebar` tinyint(1) DEFAULT '0',
  `show_right_sidebar` tinyint(1) DEFAULT '0',
  `forked_id` int(10) unsigned DEFAULT NULL COMMENT 'Wskazuje na id z tabeli projects jest to projekt od którego został utworzony ten rekord',
  `style_id` int(10) unsigned DEFAULT NULL,
  `custom_style_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `public` (`public`),
  KEY `fk_projects_added_by` (`added_by`),
  KEY `fk_projects_deleted_by` (`deleted_by`),
  KEY `fk_projects_updated_by` (`updated_by`),
  KEY `fk_projects_added_ip_id` (`added_ip_id`),
  KEY `fk_projects_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_projects_updated_ip_id` (`updated_ip_id`),
  KEY `fk_projects_user_id` (`user_id`),
  KEY `fk_projects_image_id` (`image_id`),
  KEY `fk_projects_header_id` (`header_id`),
  KEY `fk_projects_footer_id` (`footer_id`),
  KEY `fk_projects_sidebar_left_id` (`sidebar_left_id`),
  KEY `fk_projects_sidebar_right_id` (`sidebar_right_id`),
  KEY `fk_projects_forked_id` (`forked_id`),
  KEY `fk_projects_style_id` (`style_id`),
  KEY `fk_projects_custom_style_id` (`custom_style_id`),
  CONSTRAINT `fk_projects_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_custom_style_id` FOREIGN KEY (`custom_style_id`) REFERENCES `styles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_footer_id` FOREIGN KEY (`footer_id`) REFERENCES `html` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_forked_id` FOREIGN KEY (`forked_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_header_id` FOREIGN KEY (`header_id`) REFERENCES `html` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_image_id` FOREIGN KEY (`image_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_sidebar_left_id` FOREIGN KEY (`sidebar_left_id`) REFERENCES `html` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_sidebar_right_id` FOREIGN KEY (`sidebar_right_id`) REFERENCES `html` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_style_id` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `sessid` binary(16) NOT NULL,
  `access` int(10) unsigned DEFAULT NULL,
  `data` text,
  `user_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sessid` (`sessid`),
  KEY `fk_session_user_id` (`user_id`),
  CONSTRAINT `fk_session_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES ('�t(��[dY������',1558823043,'ip_id|s:1:\"2\";user_id|i:1;',1,0,1);
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(8) DEFAULT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) NOT NULL COMMENT 'Nazwa wyświetlana w liście styli',
  `user_id` int(10) unsigned NOT NULL,
  `image_id` int(10) unsigned DEFAULT NULL,
  `public` tinyint(1) DEFAULT '0',
  `system` tinyint(1) DEFAULT '1' COMMENT 'Jeżeli jest true to jest to styl systemowy np. z elementów',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_styles_added_by` (`added_by`),
  KEY `fk_styles_deleted_by` (`deleted_by`),
  KEY `fk_styles_updated_by` (`updated_by`),
  KEY `fk_styles_added_ip_id` (`added_ip_id`),
  KEY `fk_styles_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_styles_updated_ip_id` (`updated_ip_id`),
  KEY `fk_styles_user_id` (`user_id`),
  KEY `fk_styles_image_id` (`image_id`),
  KEY `public` (`public`),
  KEY `system` (`system`),
  CONSTRAINT `fk_styles_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_image_id` FOREIGN KEY (`image_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles`
--

LOCK TABLES `styles` WRITE;
/*!40000 ALTER TABLE `styles` DISABLE KEYS */;
/*!40000 ALTER TABLE `styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles_fonts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles_fonts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `styles_id` int(10) unsigned NOT NULL,
  `name` varchar(250) NOT NULL,
  `category` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_styles_fonts_styles_id` (`styles_id`),
  CONSTRAINT `fk_styles_fonts_styles_id` FOREIGN KEY (`styles_id`) REFERENCES `styles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles_fonts`
--

LOCK TABLES `styles_fonts` WRITE;
/*!40000 ALTER TABLE `styles_fonts` DISABLE KEYS */;
/*!40000 ALTER TABLE `styles_fonts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles_params`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles_params` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles_params`
--

LOCK TABLES `styles_params` WRITE;
/*!40000 ALTER TABLE `styles_params` DISABLE KEYS */;
/*!40000 ALTER TABLE `styles_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles_style`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles_style` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `styles_id` int(10) unsigned NOT NULL,
  `param_id` int(10) unsigned NOT NULL,
  `value_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_styles_style_styles_id` (`styles_id`),
  KEY `fk_styles_style_param_id` (`param_id`),
  KEY `fk_styles_style_value_id` (`value_id`),
  KEY `fk_styles_style_tag_id` (`tag_id`),
  CONSTRAINT `fk_styles_style_param_id` FOREIGN KEY (`param_id`) REFERENCES `styles_params` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_style_styles_id` FOREIGN KEY (`styles_id`) REFERENCES `styles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_style_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `styles_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_style_value_id` FOREIGN KEY (`value_id`) REFERENCES `styles_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles_style`
--

LOCK TABLES `styles_style` WRITE;
/*!40000 ALTER TABLE `styles_style` DISABLE KEYS */;
/*!40000 ALTER TABLE `styles_style` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles_tags`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles_tags`
--

LOCK TABLES `styles_tags` WRITE;
/*!40000 ALTER TABLE `styles_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `styles_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles_values`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles_values`
--

LOCK TABLES `styles_values` WRITE;
/*!40000 ALTER TABLE `styles_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `styles_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `caption` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_elements`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `element_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_id` (`tag_id`,`element_id`),
  KEY `fk_tags_elements_tag_id` (`tag_id`),
  KEY `fk_tags_elements_element_id` (`element_id`),
  CONSTRAINT `fk_tags_elements_element_id` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_tags_elements_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_elements`
--

LOCK TABLES `tags_elements` WRITE;
/*!40000 ALTER TABLE `tags_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_images`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `image_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_id` (`tag_id`,`image_id`),
  KEY `fk_tags_images_tag_id` (`tag_id`),
  KEY `fk_tags_images_image_id` (`image_id`),
  CONSTRAINT `fk_tags_images_image_id` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_tags_images_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_images`
--

LOCK TABLES `tags_images` WRITE;
/*!40000 ALTER TABLE `tags_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_projects`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_id` (`tag_id`,`project_id`),
  KEY `fk_tags_projects_tag_id` (`tag_id`),
  KEY `fk_tags_projects_project_id` (`project_id`),
  CONSTRAINT `fk_tags_projects_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_tags_projects_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_projects`
--

LOCK TABLES `tags_projects` WRITE;
/*!40000 ALTER TABLE `tags_projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mail` varchar(90) DEFAULT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned DEFAULT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `reset` varchar(16) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT '0',
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `editor` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Jeżeli jest ustawione na 1 to znaczy że ten użytkownik edytuje projekty na potrzeby serwisu',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `email` (`mail`),
  UNIQUE KEY `email_2` (`mail`,`deleted`),
  KEY `added_by` (`added_by`),
  KEY `added_ip_id` (`added_ip_id`),
  KEY `deleted_by` (`deleted_by`),
  KEY `deleted_ip_id` (`deleted_ip_id`),
  KEY `updated_by` (`updated_by`),
  KEY `updated_ip_id` (`updated_ip_id`),
  CONSTRAINT `fk_users_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'worzala86@gmail.com','15c6a290',1558820875,NULL,2,0,NULL,NULL,NULL,NULL,NULL,'3ee04957500696f42ec2be6fe35cb1ce9d8ff5c3dc22879950e0f855c78836f8a098619959908d25e60a390282fb3a38f6a756594c124b89dd63cef4b2eafbb1',NULL,1,'Paweł','Worzała',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_registrations`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_registrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned DEFAULT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `mail` varchar(250) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `newsletter` tinyint(1) DEFAULT NULL,
  `code` varchar(16) DEFAULT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `added_by` (`added_by`),
  KEY `added_ip_id` (`added_ip_id`),
  KEY `updated_by` (`updated_by`),
  KEY `updated_ip_id` (`updated_ip_id`),
  KEY `deleted_by` (`deleted_by`),
  KEY `deleted_ip_id` (`deleted_ip_id`),
  CONSTRAINT `fk_users_registrations_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_registrations`
--

LOCK TABLES `users_registrations` WRITE;
/*!40000 ALTER TABLE `users_registrations` DISABLE KEYS */;
INSERT INTO `users_registrations` VALUES (1,'c1405ba9',1558820831,NULL,2,NULL,NULL,NULL,0,NULL,NULL,'worzala86@gmail.com','3ee04957500696f42ec2be6fe35cb1ce9d8ff5c3dc22879950e0f855c78836f8a098619959908d25e60a390282fb3a38f6a756594c124b89dd63cef4b2eafbb1',0,'2385ab6552da219c','Paweł','Worzała');
/*!40000 ALTER TABLE `users_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `version`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `version` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(10) unsigned NOT NULL,
  `date` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `version`
--

LOCK TABLES `version` WRITE;
/*!40000 ALTER TABLE `version` DISABLE KEYS */;
INSERT INTO `version` VALUES (1,0,1558821682);
/*!40000 ALTER TABLE `version` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-26  0:26:40
