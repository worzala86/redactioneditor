<?php

namespace App;

trait TotalCountTrait
{
    private $totalCount;

    /**
     * @param int $totalCount
     * @description Liczba wszystkich rekordów w zapytaniu
     * @return $this
     */
    public function setTotalCount(int $totalCount)
    {
        $this->totalCount = $totalCount;
        return $this;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }
}