<?php

namespace App;

trait PaginationTrait
{
    private $page;
    private $limit;

    /**
     * @param int $limit
     * @description Licza pozycji którę będa wczytane na stronę
     * @return $this
     */
    public function setLimit(int $limit = null)
    {
        $this->limit = $limit;
        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int $page
     * @description Numer strony z danymi która ma być wczytana
     * @return $this
     */
    public function setPage(int $page = null)
    {
        $this->page = $page;
        return $this;
    }

    public function getPage(): ?int
    {
        return $this->page;
    }
}