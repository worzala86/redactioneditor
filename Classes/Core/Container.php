<?php

namespace App;


class Container extends Type
{
    use FlatDataTrait;
    use SetDataTrait;

    public function __construct($data = null)
    {
        $this->setData($data);
        parent::__construct();
    }
}