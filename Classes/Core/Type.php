<?php

namespace App;


class Type
{
    public $data;

    public function __construct($data = null)
    {
        $this->data = $data;
    }

    public function __toString()
    {
        if(($this->data)&&!empty($this->data)) {
            return (string)$this->data;
        }else{
            return null;
        }
    }

    public function getFlatData()
    {
        return $this->__toString();
    }
}