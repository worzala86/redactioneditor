<?php

namespace App;

require_once('ExceptionHandler.php');

require_once('../../Config.php');
require_once('../../vendor/autoload.php');
require_once('Autoload.php');

$routing = new Routing;
$routing();