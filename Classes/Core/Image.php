<?php

namespace App;

class Image
{

    static public function resize($from, $destination, $newWidth)
    {
        $type = '';
        $pathinfo = pathinfo($from);
        $ext = $pathinfo['extension'];

        switch ($ext) {
            case 'png':
                $type = IMAGETYPE_PNG;
                break;
            case 'PNG':
                $type = IMAGETYPE_PNG;
                break;
            case 'jpg':
                $type = IMAGETYPE_JPEG;
                break;
            case 'JPG':
                $type = IMAGETYPE_JPEG;
                break;
            case 'jpeg':
                $type = IMAGETYPE_JPEG;
                break;
            case 'JPEG':
                $type = IMAGETYPE_JPEG;
                break;
            case 'gif':
                $type = IMAGETYPE_GIF;
                break;
            case 'GIF':
                $type = IMAGETYPE_GIF;
                break;
            case 'bmp':
                $type = IMAGETYPE_BMP;
                break;
            case 'BMP':
                $type = IMAGETYPE_BMP;
                break;
        }

        list($width, $height) = getimagesize($from);
        $new_height = abs($newWidth * $height / $width);

        $bg = imagecreatetruecolor($newWidth, $new_height);
        switch (strtolower($type)) {
            case IMAGETYPE_PNG:
                imagesavealpha($bg, true);
                imagealphablending($bg, false);
                $src = imagecreatefrompng($from);
                break;
            case IMAGETYPE_JPEG:
                $src = imagecreatefromjpeg($from);
                break;
            case IMAGETYPE_GIF:
                $src = imagecreatefromgif($from);
                break;
            case IMAGETYPE_BMP:
                $src = imagecreatefrombmp($from);
                break;
            default:
                return;
        }

        $ti = imagecolortransparent($src);
        if ($ti >= 0) {
            $tc = imagecolorsforindex($src, $ti);
            $ti = imagecolorallocate($bg, $tc['red'], $tc['green'], $tc['blue']);
            imagefill($bg, 0, 0, $ti);
            imagecolortransparent($bg, $ti);
        }

        imagecopyresampled($bg, $src, 0, 0, 0, 0, $newWidth, $new_height, imagesx($src), imagesy($src));
        imagedestroy($src);
        if ($type == IMAGETYPE_PNG)
            imagepng($bg, $destination, 9);
        else
            imagejpeg($bg, $destination, 85);
        imagedestroy($bg);
    }
}
