<?php

namespace App;

trait FiltersTrait
{
    private $filters;

    /**
     * @param FiltersContainer $filters
     * @description Tablica z filtrami ktore zostaną użyte na danych
     * @return $this
     */
    public function setFilters(FiltersContainer $filters = null)
    {
        $this->filters = $filters;
        return $this;
    }

    public function getFilters(): ?FiltersContainer
    {
        return $this->filters;
    }
}