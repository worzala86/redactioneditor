<?php

function exceptionHandler($exception)
{
    $className = get_class($exception);
    $classNameParts = explode('\\', $className);
    $exceptionName = $classNameParts[count($classNameParts) - 1];
    if(DEBUG) {
        echo $exceptionName . PHP_EOL;
        echo $exception->getFile() . ' (' . $exception->getLine() . ')' . PHP_EOL;
        $backtrace = $exception->getTrace();
        foreach ($backtrace as $key => $row) {
            if (isset($row['class'])) {
                echo $row['class'] . ' (' . $row['function'] . ')' . PHP_EOL;
            } else {
                echo $row['file'] . ' (' . $row['line'] . ')' . PHP_EOL;
            }
        }
    }else{
        echo json_encode(['error'=>$exceptionName]);
    }
    exit;
}

if(!DEBUG) {
    set_exception_handler('exceptionHandler');
}