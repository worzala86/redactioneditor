<?php

namespace App;

use App\Exceptions\RequiredFieldDontHaveValueException;

trait SetDataTrait
{
    public function setData($data = null)
    {
        if (!$data) {
            return;
        }
        if (in_array(CollectionTrait::class, class_uses($this))) {
            foreach ($data as $value) {
                $parentClass = get_parent_class($this);
                $class = new $parentClass($value);
                $this->add($class);
            }
            return;
        }
        foreach ($data as $fieldName => $value) {
            $setter = 'set' . ucfirst($fieldName);
            $getter = 'get' . ucfirst($fieldName);
            if (!method_exists($this, $getter) || !method_exists($this, $setter)) {
                continue;
            }
            $reflectionMethod = new \ReflectionMethod($this, $setter);
            $parameter = $reflectionMethod->getParameters()[0];
            $className = $parameter->getType()->getName();
            if (!$parameter->isDefaultValueAvailable() && !$parameter->allowsNull() && ($value === null)) {
                print_r([$fieldName, get_class($this)]);
                throw new RequiredFieldDontHaveValueException;
            }
            if ($value === null) {
                continue;
            }
            if (class_exists($className)) {
                $value = new $className($value);
                $this->{$setter}($value);
                continue;
            }
            if (($className == 'string') && is_array($value)) {
                $value = json_encode($value);
            }
            if (($className == 'array') && is_string($value)) {
                $value = (array)json_decode($value, true);
            }
            if (($className == 'int') && is_string($value)) {
                $value = (int)$value;
            }
            $this->{$setter}($value);
        }
    }
}