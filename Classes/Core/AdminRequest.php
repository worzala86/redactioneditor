<?php

namespace App;


use App\Exceptions\DontHaveAdminRightsException;
use App\Exceptions\MustBeLoggedToUseThisEndpointException;

class AdminRequest extends Request
{
    use SetDataTrait;

    public function __construct(int $userId = null)
    {
        parent::__construct($userId);
        if (!$this->getCurrentUserId()) {
            throw new MustBeLoggedToUseThisEndpointException;
        }
        if (!$this->getUser()->isAdmin()) {
            throw new DontHaveAdminRightsException;
        }
    }
}