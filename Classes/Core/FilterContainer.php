<?php

namespace App;

class FilterContainer extends Container
{
    private $name;
    private $kind;
    private $value;

    /**
     * @param string $name
     * @description Nazwa pola do którego tworzony jest filtr
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $kind
     * @description Rodzaj filtru
     * @return $this
     */
    public function setKind(FiltersKind $kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): FiltersKind
    {
        return $this->kind;
    }

    /**
     * @param string $value
     * @description Wartość filtru
     * @return $this
     */
    public function setValue(string $value)
    {
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}