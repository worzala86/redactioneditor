<?php

namespace App;

trait CollectionTrait
{
    public $rows;
    public $index;

    public function getFlatData()
    {
        $rows = [];
        if (!$this->rows) {
            return [];
        }
        foreach ($this->rows as $index => $row) {
            $reflectionClass = new \ReflectionClass($this);
            $methods = $reflectionClass->getMethods();
            $resultRow = [];
            foreach ($methods as $method) {
                if (strpos($method->getName(), 'set') === 0) {
                    $getter = 'get' . substr($method->getName(), 3);
                    if ($reflectionClass->hasMethod($getter)) {
                        $value = $this->rows[$index]->{$getter}();
                        if (is_object($value) && in_array(CollectionTrait::class, class_uses($value))) {
                            $value = $this->rows[$index]->{$getter}()->getFlatData();
                        } else if (is_object($value) && in_array(Container::class, class_parents($value))) {
                            $value = $this->rows[$index]->{$getter}()->getFlatData();
                        } else if (is_object($value) && in_array(Type::class, class_parents($value))) {
                            $value = $this->rows[$index]->{$getter}()->__toString();
                        }
                        if ($value !== null) {
                            $resultRow[lcfirst(substr($method->getName(), 3))] = $value;
                        }
                    }
                }
            }
            $rows[] = $resultRow;
        }
        return $rows;
    }

    public function add($row)
    {
        $this->rows[] = $row;
        return $this;
    }

    public function rewind()
    {
        $this->index = 0;
    }

    public function next()
    {
        ++$this->index;
        return $this;
    }

    public function key()
    {
        return $this->index;
    }

    public function valid()
    {
        return isset($this->rows[$this->index]);
    }

    public function current()
    {
        $this->index = $this->index?$this->index:0;
        return $this->rows[$this->index];
    }

    public function count(){
        if(!$this->rows){
            return 0;
        }
        return count($this->rows);
    }
}