<?php

namespace App;


trait FlatDataTrait
{
    private function getProperties()
    {
        $reflectionClass = new \ReflectionClass($this);
        return $reflectionClass->getProperties();
    }

    public function getFlatData()
    {
        $jsonObject = new \stdClass;
        $properties = $this->getProperties();
        foreach ($properties as $property) {
            $propertyName = $property->getName();
            if (method_exists($this, 'get' . ucfirst($propertyName))
                && method_exists($this, 'set' . ucfirst($propertyName))) {
                $value = $this->{'get' . ucfirst($propertyName)}();
                $reflectionMethod = new \ReflectionMethod($this, 'set' . ucfirst($propertyName));
                $parameter = $reflectionMethod->getParameters()[0];
                if (is_object($value)) {
                    if (in_array(CollectionTrait::class, class_uses($value))) {
                        $data = [];
                        foreach ($value as $key => $val) {
                            if ($val === null) {
                                continue;
                            }
                            if(is_array($val)){
                                foreach ($val as $value){
                                    $data[] = $value->getFlatData();
                                }
                            }else {
                                $data[] = $val->getFlatData();
                            }
                        }
                        $jsonObject->{$propertyName} = $data;
                    } else if (in_array(Container::class, class_parents($value))) {
                        $jsonObject->{$propertyName} = $value->getFlatData();
                    } else if (in_array(Type::class, class_parents($value))) {
                        $jsonObject->{$propertyName} = $value->getFlatData();
                    }
                } else {
                    if ($value !== null) {
                        if ($parameter->getType()->getName() == 'bool') {
                            $jsonObject->{$propertyName} = $value ? true : false;
                        } else {
                            $jsonObject->{$propertyName} = $value;
                        }
                    }
                }
            }
        }
        return $jsonObject;
    }
}