<?php

namespace App;

use App\Containers\ImageContainer;
use App\Containers\ImageThumbContainer;
use App\Exceptions\CannotCreateDirectoryException;
use App\Exceptions\FileNotFoundException;
use App\Exceptions\ThumbNotFoundException;
use App\Modules\Files\Models\FilesModel;
use App\Types\UUID;

class File
{
    private $name;
    private $userId;
    private $stream;
    private $size;
    private $uuid;
    private $id;
    private $thumbName;
    private $thumbSize;
    private $thumbUuid;
    private $thumbUserId;
    private $thumbId;

    public function __construct()
    {
        $this->setUserId($_SESSION['user_id']);
        $this->setName(null);
        $this->setStream(null);
    }

    private function setFile(FilesModel $fileData)
    {
        $this
            ->setName($fileData->getName())
            ->setSize($fileData->getSize())
            ->setUserId($fileData->getUserId())
            ->setUuid($fileData->getUuid())
            ->setId($fileData->getId());
    }

    private function setThumb(FilesModel $fileData)
    {
        $this
            ->setThumbName($fileData->getName())
            ->setThumbSize($fileData->getSize())
            ->setThumbUuid($fileData->getUuid())
            ->setThumbUserId($fileData->getUserId())
            ->setThumbId($fileData->getId());
    }

    public function setThumbId($thumbId)
    {
        $this->thumbId = $thumbId;
        return $this;
    }

    public function getThumbId()
    {
        return $this->thumbId;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setThumbUserId($thumbUserId)
    {
        $this->thumbUserId = $thumbUserId;
        return $this;
    }

    public function getThumbUserId()
    {
        return $this->thumbUserId;
    }

    public function setThumbUuid($thumbUuid)
    {
        $this->thumbUuid = $thumbUuid;
        return $this;
    }

    public function getThumbUuid()
    {
        return $this->thumbUuid;
    }

    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setThumbSize($thumbSize)
    {
        $this->thumbSize = $thumbSize;
        return $this;
    }

    public function getThumbSize()
    {
        return $this->thumbSize;
    }

    public function setThumbName($thumbName)
    {
        $this->thumbName = $thumbName;
        return $this;
    }

    public function getThumbName()
    {
        return $this->thumbName;
    }

    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setStream($stream)
    {
        $this->stream = $stream;
        return $this;
    }

    public function getStream($uuid = null, $name = null)
    {
        if (!$uuid && !$name) {
            return $this->stream;
        }
        if ($this->stream) {
            return;
        }
        $dir = FILES_DIRECTORY . '/' . substr($uuid, 0, 2);
        $dir .= '/' . substr($uuid, 2, 2);
        $dir .= '/' . substr($uuid, 4, 2);
        $streamFile = $dir . '/' . $name;
        if (!file_exists($streamFile)) {
            throw new FileNotFoundException;
        }
        return file_get_contents($streamFile);
    }

    private function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    private function getUserId()
    {
        return $this->userId;
    }

    private function makeDirectory(string $dirName)
    {
        $dirs = explode('/', str_replace(FILES_DIRECTORY . '/', '', $dirName));
        $dirName = FILES_DIRECTORY . '/';
        foreach ($dirs as $dir) {
            $dirName .= $dir . '/';
            if (!is_dir($dirName)) {
                mkdir($dirName);
            }
        }
        return true;
    }

    private function getSaveDirectory(string $name, bool $thumb = false, UUID $uuid): array
    {
        $hash = (string)$uuid;
        $dirName = FILES_DIRECTORY
            . '/' . substr($hash, 0, 2)
            . '/' . substr($hash, 2, 2)
            . '/' . substr($hash, 4, 2);
        if (!$this->makeDirectory($dirName)) {
            throw new CannotCreateDirectoryException;
        }
        $filePatch = $dirName . '/' . ($thumb ? 'thumb_' : '') . $name;
        $index = 0;
        while (file_exists($filePatch)) {
            $index++;
            $filePatch = $dirName . '/' . ($index ? ($index . '_') : '') . ($thumb ? 'thumb' : '') . (($thumb || $index) ? '_' : '') . $name;
        }
        return [
            $filePatch,
            ($index ? ($index . '_') : '') . ($thumb ? 'thumb' : '') . (($thumb || $index) ? '_' : '') . $name
        ];
    }

    public function save($name, $stream)
    {
        $uuid = UUID::fake();
        $thumbUuid = UUID::fake();

        $name = preg_replace_callback('@.([a-z-A-Z]+)$@', function ($match){
            return '.jpg';
        }, $name);

        $this->setName($name);
        $this->setStream($stream);

        list($filePath, $fileName) = $this->getSaveDirectory($this->getName(), false, $uuid);
        list($thumbFilePath, $thumbFileName) = $this->getSaveDirectory($this->getName(), true, $thumbUuid);

        $originalFile = imagecreatefromstring($this->getStream());

        $thumbId = null;
        if ($originalFile) {
            $thumbFile = imagescale($originalFile, 400);
            imagejpeg($thumbFile, $thumbFilePath, 80);

            $thumbModel = (new FilesModel)
                ->setUuid($thumbUuid)
                ->setName($thumbFileName)
                ->setSize(filesize($thumbFilePath))
                ->setUserId($this->getUserId());
            $thumbId = $thumbModel->insert();
            $this->setThumb($thumbModel);
        }

        if ($originalFile) {
            imagejpeg($originalFile, $filePath, 80);
        } else {
            file_put_contents($filePath, $this->data);
        }

        $fileModel = (new FilesModel)
            ->setUuid($uuid)
            ->setName($fileName)
            ->setSize(filesize($filePath))
            ->setUserId($this->getUserId())
            ->setThumbId($thumbId);
        $fileId = $fileModel->insert();
        $this->setFile($fileModel);

        return $this->getContainer();
    }

    public function load($fileId, $uuid = false)
    {
        $fileModel = (new FilesModel)
            ->where('user_id=?', $this->userId)
            ->load($fileId, $uuid);
        if (!$fileModel->isLoaded()) {
            throw new FileNotFoundException;
        }
        $this->setFile($fileModel);

        if ($fileModel->getThumbId()) {
            $thumbModel = (new FilesModel)
                ->where('user_id=?', $this->userId)
                ->load($fileModel->getThumbId());
            if (!$fileModel->isLoaded()) {
                throw new ThumbNotFoundException();
            }
            $this->setThumb($thumbModel);
        }

        return $this->getContainer();
    }

    private function getContainer()
    {
        $file = new ImageContainer;
        $thumb = new ImageThumbContainer;
        if ($this->getThumbUuid()) {
            $thumb
                ->setId($this->getThumbUuid())
                ->setName($this->getThumbName())
                ->setSize($this->getThumbSize());
        }
        $file
            ->setId($this->getUuid())
            ->setName($this->getName())
            ->setSize($this->getSize())
            ->setThumb($thumb);
        return $file;
    }
}