<?php

namespace App;

use App\Database\DB;
use App\Types\SESSID;

class Session implements \SessionHandlerInterface
{
    private $db;
    private $sessionId;

    public function __construct()
    {
        $this->db = DB::get();
        session_set_save_handler($this, true);

        if (isset($_COOKIE[session_name()])) {
            session_id($_COOKIE[session_name()]);
        } else {
            $id = SESSID::fake();
            session_id($id);
        }

        session_start();
        setcookie(session_name(), session_id(), time() + SESSION_TIMEOUT, '/');
    }

    public function open($savePath, $sessionName)
    {
        return true;
    }

    function close()
    {
        return true;
    }

    function read($id)
    {
        $data = $this->db->getOne('select `data` from `session` where sessid=:sessid', ['sessid' => hex2bin($id)]);
        return $data ? $data : '';
    }

    function write($id, $data)
    {
        $id = hex2bin($id);
        $session = $this->db->getRow('select sessid, deleted, user_id from session where sessid=:sessid', ['sessid' => $id]);
        if ($session && ($session['deleted'] == 0)) {
            $this->db->execute('update session set access=:access, data=:data, user_id=:user_id where sessid=:sessid', [
                'sessid' => $id,
                'access' => time(),
                'data' => $data,
                'user_id' => isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null,
            ]);
        } else {
            $id = SESSID::fake();
            setcookie(session_name(), $id, time() + SESSION_TIMEOUT, '/');
            $this->db->execute('insert into session (sessid, access, `data`, user_id) values (?, ?, ?, ?)', [
                hex2bin($id), time(), $data, isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null
            ]);
            $_SESSION['session_id'] = $this->db->insertId();
        }
        return true;
    }

    function destroy($id)
    {
        return $this->db->execute('update session set deleted=? where sessid=?', [time(), hex2bin($id)]) ? true : false;
    }

    function gc($max)
    {
        return $this->db->execute('update session set deleted=? where access=?', [time(), time() - $max]) ? true : false;
    }

    function getUserId()
    {
        return isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
    }

    function getSessionId()
    {
        if (!isset($this->sessionId)) {
            $this->sessionId = DB::get()->getOne('select id from session where sessid=?', hex2bin(session_id()));
        }
        return $this->sessionId;
    }
}