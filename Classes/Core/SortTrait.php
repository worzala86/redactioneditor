<?php

namespace App;

use App\Types\SortKind;

trait SortTrait
{
    private $sortName;
    private $sortKind;

    /**
     * @param string|null $sortName
     * @description Nazwa pola do którego będzie przypisane sortowanie
     * @return $this
     */
    public function setSortName(string $sortName = null)
    {
        $this->sortName = $sortName;
        return $this;
    }

    public function getSortName(): ?string
    {
        return $this->sortName;
    }

    /**
     * @param SortKind|null $sortKind
     * @description Rodzaj sortowania  określa czy sortowanie jest malejące czy rosnące  desc, asc
     * @return $this
     */
    public function setSortKind(SortKind $sortKind = null)
    {
        $this->sortKind = $sortKind;
        return $this;
    }

    public function getSortKind(): ?SortKind
    {
        return $this->sortKind;
    }
}