<?php

spl_autoload_register(function ($className) {
    $names = explode('\\', $className);
    unset($names[0]);
    if(!$names){
        return;
    }
    $fileName = ROOT_DIRECTORY.'/Classes/Core/'.join('/', $names).'.php';
    if(file_exists($fileName)){
        require_once($fileName);
    }else{
        require_once(ROOT_DIRECTORY.'/Classes/'.join('/', $names).'.php');
    }
});