<?php

namespace App;

use App\Modules\User\Models\UsersModel;

class User
{
    private $userModel;

    public function __construct(int $userId)
    {
        $this->userModel = (new UsersModel)->load($userId);
    }

    public function getId(): int
    {
        return $this->userModel->getId();
    }

    public function isAdmin(): bool
    {
        return $this->userModel->getAdmin() == 1;
    }

    public function getFirstName(): string
    {
        return $this->userModel->getFirstName();
    }

    public function getLastName(): string
    {
        return $this->userModel->getLastName();
    }

    public function getActiveProjectId(): ?int
    {
        return $this->userModel->getActiveProjectId();
    }
}