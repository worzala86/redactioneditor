<?php

namespace App;


class DocComment
{
    private $docComment;
    private $lines;

    public function __construct($docComment)
    {
        $this->docComment = $docComment;
        preg_match_all('/^\s*\* (.*)/m', $this->docComment, $matches);
        foreach($matches[1] as $line){
            preg_match('/^@?([a-zA-Z0-9]*) (.*)/', $line, $m);
            $this->lines[$m[1]] = $m[2];
        }
    }

    public function getByTag($tag){
        if(!isset($this->lines[$tag])){
            return '';
        }
        return $this->lines[$tag];
    }
}