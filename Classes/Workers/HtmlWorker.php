<?php

namespace App\Workers;

use App\Containers\HtmlElementContainer;
use App\Containers\HtmlElementsContainer;
use App\Database\DB;
use App\Exceptions\ComponentNotFoundException;
use App\Exceptions\ElementNotFoundException;
use App\Modules\Components\Models\ComponentsModel;
use App\Modules\Elements\Models\ElementsModel;
use App\Modules\Others\Collections\HtmlParamsDatasetsCollection;
use App\Modules\Others\Models\HtmlClasses;
use App\Modules\Others\Models\HtmlModel;
use App\Modules\Others\Collections\HtmlCollection;
use App\Modules\Others\Models\HtmlParamsDatasetsModel;
use App\Modules\Others\Models\HtmlParamsModel;
use App\Modules\Others\Models\HtmlParamsTextModel;
use App\Types\SortKind;
use App\Types\Time;
use App\Types\UUID;

class HtmlWorker
{
    private $datasets;

    public function __construct()
    {
        $this->datasets = [];
    }

    public function delete(int $id = null)
    {
        if (!$id) {
            return;
        }
        DB::get()->execute('delete from html where id=?', [$id]);
    }

    public function saveHtmlElements(HtmlElementContainer $element = null, int $parentId = null)
    {
        if (!$element) {
            return null;
        }

        $componentsModel = (new ComponentsModel)
            ->where('`key`=?', $element->getType())
            ->load();
        if (!$componentsModel->isLoaded()) {
            throw new ComponentNotFoundException;
        }

        $classId = null;
        $htmlClassesModel = (new HtmlClasses)
            ->where('name=?', $element->getClass())
            ->load();
        if ($htmlClassesModel->isLoaded()) {
            $classId = $htmlClassesModel->getId();
        } else if ($element->getClass() && ($element->getClass() !== '')) {
            $classId = (new HtmlClasses)
                ->setName($element->getClass())
                ->insert();
        }

        $elementId = null;
        if($element->getElementId()) {
            $elementsModel = (new ElementsModel)
                ->load($element->getElementId(), true);
            if($elementsModel->isLoaded()) {
                $elementId = $elementsModel->getId();
            }else{
                throw new ElementNotFoundException;
            }
        }

        $htmlModel = (new HtmlModel)
            ->setField($element->getField())
            ->setAltId($element->getAltId())
            ->setParentId($parentId)
            ->setHtmlId($element->getId())
            ->setHtmlClassId($classId)
            ->setComponentId($componentsModel->getId())
            ->setElementId($elementId)
            ->setShow($element->getShow())
            ->setDataSetActive($element->getDataSetActive())
            ->setBackgroundActive($element->getBackgroundActive());
        if (!$parentId) {
            $htmlModel->setUpdated(Time::fake());
        }
        $htmlId = $htmlModel->insert();

        $params = [
            'src' => $element->getSrc(),
            'alt' => $element->getAlt(),
            'picTitle' => $element->getPicTitle(),
            'href' => $element->getHref(),
            'target' => $element->getTarget(),
            'placeholder' => $element->getPlaceholder(),
            'selector' => $element->getSelector(),
        ];
        foreach ($params as $key => $value) {
            if ((!$value) || empty($value) || ($value == '')) {
                continue;
            }
            (new HtmlParamsModel)
                ->setHtmlId($htmlId)
                ->setName($key)
                ->setValue($value)
                ->insert();
        }

        /*$htmlParamsStyleCollection = (new HtmlParamsStyleCollection)
            ->where('html_id=?', $htmlId)
            ->setSortName('html_id')
            ->setSortKind(new SortKind('asc'))
            ->loadAll();
        foreach ($htmlParamsStyleCollection as $htmlParamsStyleModel) {
            $htmlParamsStyleModel->forceDelete();
        }

        if ($element->getStyle()) {
            foreach ($element->getStyle() as $kind => $value) {
                $htmlParamsStyleKindsId = null;
                $htmlParamsStyleKindsModel = (new HtmlParamsStyleKindsModel)
                    ->where('kind=?', $kind)
                    ->load();
                if (!$htmlParamsStyleKindsModel->isLoaded()) {
                    $htmlParamsStyleKindsId = $htmlParamsStyleKindsModel
                        ->setKind($kind)
                        ->insert();
                } else {
                    $htmlParamsStyleKindsId = $htmlParamsStyleKindsModel->getId();
                }
                $htmlParamsStyleValuesId = null;
                $htmlParamsStyleValuesModel = (new HtmlParamsStyleValuesModel)
                    ->where('`value`=?', $value)
                    ->load();
                if (!$htmlParamsStyleValuesModel->isLoaded()) {
                    $htmlParamsStyleValuesId = $htmlParamsStyleValuesModel
                        ->setValue($value)
                        ->insert();
                } else {
                    $htmlParamsStyleValuesId = $htmlParamsStyleValuesModel->getId();
                }
                (new HtmlParamsStyleModel)
                    ->setHtmlId($htmlId)
                    ->setHtmlParamsStyleKindsId($htmlParamsStyleKindsId)
                    ->setHtmlParamsStyleValueId($htmlParamsStyleValuesId)
                    ->insert();
            }
        }*/

        $htmlParamsDatasetsCollection = (new HtmlParamsDatasetsCollection)
            ->where('html_id=?', $htmlId)
            ->setSortName('html_id')
            ->setSortKind(new SortKind('asc'))
            ->loadAll();
        foreach ($htmlParamsDatasetsCollection as $htmlParamsDatasetsModel) {
            $htmlParamsDatasetsModel->forceDelete();
        }

        if ($element->getDataSet()) {
            foreach ($element->getDataSet() as $key => $value) {
                if(!$value){
                    continue;
                }
                (new HtmlParamsDatasetsModel)
                    ->setHtmlId($htmlId)
                    ->setKey($key)
                    ->setValue($value)
                    ->insert();
            }
        }

        $html = $element->getHtml() ? json_encode($element->getHtml()) : null;
        $params = [
            'html' => $html,
        ];
        foreach ($params as $key => $value) {
            if ((!$value) || empty($value) || ($value == '')) {
                continue;
            }
            (new HtmlParamsTextModel)
                ->setHtmlId($htmlId)
                ->setName($key)
                ->setValue($value)
                ->insert();
        }

        $childrens = $element->getChildrens();
        if ($childrens) {
            foreach ($childrens as $children) {
                $this->saveHtmlElements($children, $htmlId);
            }
        }
        return $htmlId;
    }

    private function loadParams(HtmlModel $model, HtmlElementContainer &$element)
    {
        $params = ['src', 'alt', 'picTitle', 'href', 'target', 'placeholder', 'selector'];
        foreach ($params as $param) {
            $htmlParamModel = (new HtmlParamsModel)
                ->where('html_id=?', $model->getId())
                ->where('name=?', $param)
                ->load();
            $value = $htmlParamModel->getValue();
            $setterName = 'set' . ucfirst($param);
            if ($value && method_exists($element, $setterName)) {
                $element->{$setterName}($value);
            }
        }
    }

    private function loadParamsText(HtmlModel $model, HtmlElementContainer &$element)
    {
        $params = ['html'];
        foreach ($params as $param) {
            $htmlParamsTextModel = (new HtmlParamsTextModel)
                ->where('html_id=?', $model->getId())
                ->where('name=?', $param)
                ->load();
            $value = $htmlParamsTextModel->getValue();
            if ($param == 'html') {
                $value = json_decode($value);
            } else {
                $value = (array)json_decode($value);
            }
            $setterName = 'set' . ucfirst($param);
            if ($value && method_exists($element, $setterName)) {
                $element->{$setterName}($value);
            }
        }
    }

    /*private function loadStyle(HtmlModel $model, HtmlElementContainer &$element)
    {
        $htmlStyleCollection = (new HtmlStyleCollection)
            ->where('html_id=?', $model->getId())
            ->setSortName('kind')
            ->setSortKind(new SortKind('asc'))
            ->loadAll();
        $style = [];
        foreach ($htmlStyleCollection as $htmlStyleModel) {
            $style[$htmlStyleModel->getKind()] = $htmlStyleModel->getValue();
        }
        $element->setStyle($style);
    }*/

    private function loadDatasets(HtmlModel $model/*, HtmlElementContainer &$element*/)
    {
        $htmlParamsDatasetsCollection = (new HtmlParamsDatasetsCollection)
            ->where('html_id=?', $model->getId())
            ->setSortName('html_id')
            ->setSortKind(new SortKind('asc'))
            ->loadAll();
        //$this->datasets = [];
        foreach ($htmlParamsDatasetsCollection as $htmlParamsDatasetsModel) {
            $this->datasets[$htmlParamsDatasetsModel->getKey()] = $htmlParamsDatasetsModel->getValue();
        }
        //$element->setDataSet($datasets);
    }

    public function getDataSets(): array
    {
        return $this->datasets;
    }

    public function loadHtmlElements(int $id = null, UUID $elementPositionId = null): ?HtmlElementContainer
    {
        if ($id == null) {
            return null;
        }
        $htmlModel = (new HtmlModel)
            ->load($id);
        $htmlResult = null;
        if ($htmlModel->isLoaded()) {
            $componentModel = (new ComponentsModel)
                ->load($htmlModel->getComponentId());
            $className = null;
            $htmlClassesModel = (new HtmlClasses)
                ->load($htmlModel->getHtmlClassId());
            if ($htmlClassesModel->isLoaded()) {
                $className = $htmlClassesModel->getName();
            }
            $childrens = $this->loadHtmlElementsTree($id, $elementPositionId);
            $htmlResult = (new HtmlElementContainer);
            $this->loadParams($htmlModel, $htmlResult);
            $this->loadParamsText($htmlModel, $htmlResult);
            //$this->loadStyle($htmlModel, $htmlResult);
            $this->loadDatasets($htmlModel/*, $htmlResult*/);
            /*$elementId = null;
            $elementsModel = (new ElementsModel)
                ->load($htmlModel->getElementId());
            if($elementsModel->isLoaded()){
                $elementId = $elementsModel->getUuid();
            }*/
            $htmlResult
                ->setId($htmlModel->getHtmlId())
                ->setType($componentModel->getKey())
                ->setField($htmlModel->getField())
                ->setAltId($htmlModel->getAltId())
                ->setChildrens($childrens)
                ->setClass($className)
                //->setElementId($elementId)
                ->setElementPositionId($elementPositionId)
                ->setShow($htmlModel->getShow())
                ->setDataSetActive($htmlModel->getDataSetActive())
                ->setBackgroundActive($htmlModel->getBackgroundActive())
                ->setOriginHtmlId($htmlModel->getId());
        }

        return $htmlResult;
    }

    private function loadHtmlElementsTree(int $parentId, UUID $elementPositionId = null)
    {
        $htmlCollection = (new HtmlCollection)
            ->setSortName('id')
            ->setSortKind(new SortKind('asc'))
            ->where('parent_id=?', $parentId)
            ->loadAll();
        $htmlResult = new HtmlElementsContainer;
        foreach ($htmlCollection as $htmlModel) {
            $componentModel = (new ComponentsModel)
                ->load($htmlModel->getComponentId());
            $className = null;
            $htmlClassesModel = (new HtmlClasses)
                ->load($htmlModel->getHtmlClassId());
            if ($htmlClassesModel->isLoaded()) {
                $className = $htmlClassesModel->getName();
            }
            $childrens = $this->loadHtmlElementsTree($htmlModel->getId(), $elementPositionId);
            $element = (new HtmlElementContainer);
            $this->loadParams($htmlModel, $element);
            $this->loadParamsText($htmlModel, $element);
            //$this->loadStyle($htmlModel, $element);
            //$this->loadDatasets($htmlModel, $element);
            /*$elementId = null;
            $elementsModel = (new ElementsModel)
                ->load($htmlModel->getElementId());
            if($elementsModel->isLoaded()){
                $elementId = $elementsModel->getUuid();
            }*/
            $htmlResult->add(
                $element
                    ->setId($htmlModel->getHtmlId())
                    ->setType($componentModel->getKey())
                    ->setField($htmlModel->getField())
                    ->setAltId($htmlModel->getAltId())
                    ->setChildrens($childrens)
                    ->setClass($className)
                    //->setElementId($elementId)
                    ->setElementPositionId($elementPositionId)
                    ->setShow($htmlModel->getShow())
                    ->setDataSetActive($htmlModel->getDataSetActive())
                    ->setBackgroundActive($htmlModel->getBackgroundActive())
                    ->setOriginHtmlId($htmlModel->getId())
            );
        }
        return $htmlResult;
    }
}