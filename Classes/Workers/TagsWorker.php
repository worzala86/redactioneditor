<?php

namespace App\Workers;

use App\Database\DB;

class TagsWorker
{
    private $imageId;
    private $elementId;

    public function setImageId(int $imageId)
    {
        $this->imageId = $imageId;
    }

    public function setElementId(int $elementId)
    {
        $this->elementId = $elementId;
    }

    public function saveTagsToDatabase($tags)
    {
        $tagsToSaveIds = [];
        $tags = explode(' ', $tags);
        foreach ($tags as $tag) {
            $tagId = DB::get()->getOne('select id from tags where `name`=?', $tag);
            if (!$tagId && strlen($tag) && !empty($tag)) {
                $tag = lcfirst($tag);
                $caption = ucfirst($tag);
                DB::get()->execute('insert into tags set `name`=?, caption=?', [$tag, $caption]);
                $tagId = DB::get()->insertId();
            }
            if ($tagId) {
                $tagsToSaveIds[] = $tagId;
            }
        }

        if (isset($this->imageId)) {
            DB::get()->execute('delete from tags_images where image_id=?', $this->imageId);
            foreach ($tagsToSaveIds as $tagId) {
                DB::get()->execute('insert into tags_images set tag_id=?, image_id=?', [$tagId, $this->imageId]);
            }
        }

        if (isset($this->elementId)) {
            DB::get()->execute('delete from tags_elements where element_id=?', $this->elementId);
            foreach ($tagsToSaveIds as $tagId) {
                DB::get()->execute('insert into tags_elements set tag_id=?, element_id=?', [$tagId, $this->elementId]);
            }
        }
    }

    public function getTags()
    {
        return join(' ', $this->getTagsArray());
    }

    public function getTagsArray()
    {
        $tags = [];

        if (isset($this->imageId)) {
            $ts = DB::get()->getAll('select name from tags_images left join tags on tags.id=tag_id where image_id=?', $this->imageId);
            foreach ($ts as $t) {
                $tags[] = $t['name'];
            }
        }

        if (isset($this->elementId)) {
            $ts = DB::get()->getAll('select name from tags_elements left join tags on tags.id=tag_id where element_id=?', $this->elementId);
            foreach ($ts as $t) {
                $tags[] = $t['name'];
            }
        }

        return $tags;
    }

    public function getElementsTags()
    {
        $tags = [];

        $ts = DB::get()->getAll('select distinct `name`, caption from tags_elements left join tags on tags.id=tag_id');
        foreach ($ts as $t) {
            $tags[] = [$t['name'], $t['caption']];
        }

        return $tags;
    }
}