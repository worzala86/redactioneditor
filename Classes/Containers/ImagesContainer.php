<?php

namespace App\Containers;

use App\CollectionTrait;

class ImagesContainer extends ImageContainer implements \Iterator
{
    use CollectionTrait;
}