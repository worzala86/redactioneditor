<?php

namespace App\Containers;

use App\Container;
use App\Types\Url;
use App\Types\UUID;

class PageContainer extends Container
{
    private $id;
    private $url;

    /**
     * @param int $id
     * @description Identyfikator komponentu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param Url $url
     * @description Adres url strony
     * @return $this
     */
    public function setUrl(Url $url)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }
}