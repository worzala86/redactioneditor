<?php

namespace App\Containers;

use App\Container;
use App\Types\ElementKind;
use App\Types\UUID;

class ElementContainer extends Container
{
    private $id;
    private $image;
    private $kind;
    private $userIsOwner;
    private $userName;
    private $downloadTimes;
    private $usedTimes;
    private $tags;

    /**
     * @param int $id
     * @description Identyfikator elementu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param ImageContainer $image
     * @description Zdjęcie elementu wraz z miniaturką
     * @return $this
     */
    public function setImage(ImageContainer $image = null)
    {
        $this->image = $image;
        return $this;
    }

    public function getImage(): ?ImageContainer
    {
        return $this->image;
    }

    /**
     * @param ImageContainer $image
     * @description Zdjęcie elementu wraz z miniaturką
     * @return $this
     */
    public function setKind(ElementKind $kind)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): ?ElementKind
    {
        return $this->kind;
    }

    /**
     * @param bool $userIsOwner
     * @description Flaga informuje czy użytkownik jest właścicielem elementu
     * @return $this
     */
    public function setUserIsOwner(bool $userIsOwner)
    {
        $this->userIsOwner = $userIsOwner;
        return $this;
    }

    public function getUserIsOwner(): ?bool
    {
        return $this->userIsOwner;
    }

    /**
     * @param string $userName
     * @description Nazwa użytkownika/moderatora który edytował ten element
     * @return $this
     */
    public function setUserName(string $userName = null)
    {
        $this->userName = $userName;
        return $this;
    }

    public function getUserName(): ?string
    {
        return $this->userName;
    }

    /**
     * @param int $downloadTimes
     * @description Określa ile razy element był pobrany w edytorze przez użytkowników
     * @return $this
     */
    public function setDownloadTimes(int $downloadTimes = null)
    {
        $this->downloadTimes = $downloadTimes;
        return $this;
    }

    public function getDownloadTimes(): ?int
    {
        return $this->downloadTimes;
    }

    /**
     * @param int $usedTimes
     * @description Określa ile razy element był wykorzystany w edytorze przez użytkowników
     * @return $this
     */
    public function setUsedTimes(int $usedTimes = null)
    {
        $this->usedTimes = $usedTimes;
        return $this;
    }

    public function getUsedTimes(): ?int
    {
        return $this->usedTimes;
    }

    /**
     * @param array $usedTimes
     * @description Tablica z tagami elementu
     * @return $this
     */
    public function setTags(array $tags = null)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getTags(): ?array
    {
        return $this->tags;
    }
}