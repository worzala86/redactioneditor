<?php

namespace App\Containers;

use App\Container;
use App\Types\UUID;

class PageElementContainer extends Container
{
    private $id;
    private $elementId;
    private $selector;
    private $fields;

    /**
     * @param UUID $id
     * @description Identyfikator pozycji
     * @return $this
     */
    public function setId(UUID $id = null)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?UUID
    {
        return $this->id;
    }

    /**
     * @param UUID $elementId
     * @description Identyfikator elementu
     * @return $this
     */
    public function setElementId(UUID $elementId = null)
    {
        $this->elementId = $elementId;
        return $this;
    }

    public function getElementId(): ?UUID
    {
        return $this->elementId;
    }

    /**
     * @param string $selector
     * @description Identyfikator elementu
     * @return $this
     */
    public function setSelector(string $selector = null)
    {
        $this->selector = $selector;
        return $this;
    }

    public function getSelector(): ?string
    {
        return $this->selector;
    }

    /**
     * @param PageElementFieldsContainer $fields
     * @description Lista pól powiązanych z datasetem
     * @return $this
     */
    public function setFields(PageElementFieldsContainer $fields = null)
    {
        $this->fields = $fields;
        return $this;
    }

    public function getFields(): ?PageElementFieldsContainer
    {
        return $this->fields;
    }
}