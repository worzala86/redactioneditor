<?php

namespace App\Containers;

use App\Container;
use App\Types\PixabayID;
use App\Types\UUID;

class PixabayImageContainer extends Container
{
    private $id;
    private $url;

    /**
     * @param int $id
     * @description Identyfikator zdjęcia
     * @return $this
     */
    public function setId(PixabayID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): PixabayID
    {
        return $this->id;
    }

    /**
     * @param string $url
     * @description Link do zdjęcia
     * @return $this
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}