<?php

namespace App\Containers;

use App\CollectionTrait;

class HtmlElementsContainer extends HtmlElementContainer implements \Iterator, \Countable
{
    use CollectionTrait;
}