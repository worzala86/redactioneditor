<?php

namespace App\Containers;

use App\CollectionTrait;

class CsssContainer extends CssContainer implements \Iterator
{
    use CollectionTrait;
}