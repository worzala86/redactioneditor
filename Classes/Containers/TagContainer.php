<?php

namespace App\Containers;

use App\Container;
use App\Modules\Components\Requests\CreateComponentRequest;
use App\Types\ComponentKey;
use App\Types\ComponentKind;
use App\Types\UUID;

class TagContainer extends Container
{
    private $id;
    private $name;
    private $count;
    private $caption;

    /**
     * @param int $id
     * @description Identyfikator tagu
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa tagu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param int $count
     * @description Ilość użyć tagu
     * @return $this
     */
    public function setCount(int $count)
    {
        $this->count = $count;
        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * @param string $caption
     * @description Nazwa wyświetlana tagu
     * @return $this
     */
    public function setCaption(string $caption)
    {
        $this->caption = $caption;
        return $this;
    }

    public function getCaption(): string
    {
        return $this->caption;
    }
}