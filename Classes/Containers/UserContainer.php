<?php

namespace App\Containers;

use App\Container;
use App\Types\Mail;
use App\Types\Time;
use App\Types\UUID;

class UserContainer extends Container
{
    private $id;
    private $mail;
    private $firstName;
    private $lastName;
    private $joinDate;
    private $lastSeen;

    /**
     * @param int $id
     * @description Identyfikator użytkownika
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param Mail $mail
     * @description Mail użytkownika
     * @return $this
     */
    public function setMail(Mail $mail)
    {
        $this->mail = $mail;
        return $this;
    }

    public function getMail(): Mail
    {
        return $this->mail;
    }

    /**
     * @param string $firstName
     * @description Imię użytkownika
     * @return $this
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     * @description Nazwisko użytkownika
     * @return $this
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param Time $joinDate
     * @description Data dołączenia do strony
     * @return $this
     */
    public function setJoinDate(Time $joinDate)
    {
        $this->joinDate = $joinDate;
        return $this;
    }

    public function getJoinDate(): Time
    {
        return $this->joinDate;
    }

    /**
     * @param Time $lastSeen
     * @description Data ostatniej aktywności użytkownika
     * @return $this
     */
    public function setLastSeen(Time $lastSeen = null)
    {
        $this->lastSeen = $lastSeen;
        return $this;
    }

    public function getLastSeen(): ?Time
    {
        return $this->lastSeen;
    }
}