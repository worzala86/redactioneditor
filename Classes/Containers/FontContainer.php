<?php

namespace App\Containers;

use App\Container;

class FontContainer extends Container
{
    private $name;
    private $category;
    private $link;

    /**
     * @param string $name
     * @description Nazwa czcionki
     * @return $this
     */
    public function setName(string $name = null)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $category
     * @description Kategoria czcionki
     * @return $this
     */
    public function setCategory(string $category = null)
    {
        $this->category = $category;
        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param string $link
     * @description Link do czcionki
     * @return $this
     */
    public function setLink(string $link = null)
    {
        $this->link = $link;
        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }
}