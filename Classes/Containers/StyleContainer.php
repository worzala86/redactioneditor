<?php

namespace App\Containers;

use App\Container;
use App\Types\UUID;

class StyleContainer extends Container
{
    private $id;
    private $name;
    private $image;
    private $userName;
    private $usedTimes;
    private $public;

    /**
     * @param int $id
     * @description Identyfikator stylu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa stylu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param ImageContainer $image
     * @description Obrazek ze zdjęciem stylu
     * @return $this
     */
    public function setImage(ImageContainer $image = null)
    {
        $this->image = $image;
        return $this;
    }

    public function getImage(): ?ImageContainer
    {
        return $this->image;
    }

    /**
     * @param string $userName
     * @description Osoba która dodała styl
     * @return $this
     */
    public function setUserName(string $userName)
    {
        $this->userName = $userName;
        return $this;
    }

    public function getUserName(): ?string
    {
        return $this->userName;
    }

    /**
     * @param int $usedTimes
     * @description Ilość wykorzystań stylu
     * @return $this
     */
    public function setUsedTimes(int $usedTimes)
    {
        $this->usedTimes = $usedTimes;
        return $this;
    }

    public function getUsedTimes(): ?int
    {
        return $this->usedTimes;
    }

    /**
     * @param bool $public
     * @description Określa czy styl jest publicznie dostępny
     * @return $this
     */
    public function setPublic(bool $public)
    {
        $this->public = $public;
        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }
}