<?php

namespace App\Containers;

use App\CollectionTrait;

class PageElementsContainer extends PageElementContainer implements \Iterator
{
    use CollectionTrait;
}