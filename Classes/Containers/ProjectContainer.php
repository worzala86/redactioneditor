<?php

namespace App\Containers;

use App\Container;
use App\Types\Domain;
use App\Types\Time;
use App\Types\UUID;

class ProjectContainer extends Container
{
    private $id;
    private $name;
    private $domain;
    private $forkedId;
    private $userName;
    private $added;
    private $updated;
    private $description;

    /**
     * @param int $id
     * @description Identyfikator produktu - do wykorzystywania np. przy edycji i usuwaniu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa produktu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Domain $domain
     * @description Domena pod ktorą będzie projekt
     * @return $this
     */
    public function setDomain(Domain $domain = null)
    {
        $this->domain = $domain;
        return $this;
    }

    public function getDomain(): ?Domain
    {
        return $this->domain;
    }

    /**
     * @param UUID $forkedId
     * @description Jest to identyfikator projektu od którego został utworzony bierzący projekt
     * @return $this
     */
    public function setForkedId(UUID $forkedId = null)
    {
        $this->forkedId = $forkedId;
        return $this;
    }

    public function getForkedId(): ?UUID
    {
        return $this->forkedId;
    }

    /**
     * @param string $userName
     * @description Nazwa użytkownika/moderatora treści który dodał projekt
     * @return $this
     */
    public function setUserName(string $userName = null)
    {
        $this->userName = $userName;
        return $this;
    }

    public function getUserName(): ?string
    {
        return $this->userName;
    }

    /**
     * @param Time $added
     * @description Data dodania projektu
     * @return $this
     */
    public function setAdded(Time $added = null)
    {
        $this->added = $added;
        return $this;
    }

    public function getAdded(): ?Time
    {
        return $this->added;
    }

    /**
     * @param Time $updated
     * @description Data ostatniej modyfikacji projektu
     * @return $this
     */
    public function setUpdated(Time $updated = null)
    {
        $this->updated = $updated;
        return $this;
    }

    public function getUpdated(): ?Time
    {
        return $this->updated;
    }

    /**
     * @param string $description
     * @description Opis projektu
     * @return $this
     */
    public function setDescription(string $description = null)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}