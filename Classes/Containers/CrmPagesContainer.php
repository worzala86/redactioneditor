<?php

namespace App\Containers;

use App\CollectionTrait;

class CrmPagesContainer extends CrmPageContainer implements \Iterator
{
    use CollectionTrait;
}