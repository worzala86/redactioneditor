<?php

namespace App\Containers;

use App\Container;
use App\Types\DatasetFieldKind;
use App\Types\DatasetFieldName;
use App\Types\UUID;

class DatasetFieldContainer extends Container
{
    private $id;
    private $name;
    private $type;

    /**
     * @param int $id
     * @description Identyfikator komponentu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param DatasetFieldName $name
     * @description Nazwa komponentu
     * @return $this
     */
    public function setName(DatasetFieldName $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param DatasetFieldKind $type
     * @description Rodzaj pola np. text
     * @return $this
     */
    public function setType(DatasetFieldKind $type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType(): DatasetFieldKind
    {
        return $this->type;
    }
}