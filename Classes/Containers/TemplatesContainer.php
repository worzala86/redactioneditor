<?php

namespace App\Containers;

use App\CollectionTrait;

class TemplatesContainer extends TemplateContainer implements \Iterator
{
    use CollectionTrait;
}