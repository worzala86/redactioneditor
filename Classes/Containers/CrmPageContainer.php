<?php

namespace App\Containers;

use App\Container;
use App\Types\Url;
use App\Types\UUID;

class CrmPageContainer extends Container
{
    private $id;
    private $title;
    private $content;
    private $url;

    /**
     * @param int $id
     * @description Identyfikator elementu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $title
     * @description Tytuł strony
     * @return $this
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $content
     * @description Treść strony
     * @return $this
     */
    public function setContent(string $content)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param Url $url
     * @description Treść strony
     * @return $this
     */
    public function setUrl(Url $url)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl(): Url
    {
        return $this->url;
    }
}