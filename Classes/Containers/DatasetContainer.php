<?php

namespace App\Containers;

use App\Container;
use App\Types\DatasetName;
use App\Types\UUID;

class DatasetContainer extends Container
{
    private $id;
    private $name;
    private $fields;

    /**
     * @param int $id
     * @description Identyfikator komponentu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param DatasetName $name
     * @description Nazwa dataseta
     * @return $this
     */
    public function setName(DatasetName $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param DatasetFieldsContainer $fields
     * @description Lista zawiera pola dataseta
     * @return $this
     */
    public function setFields(DatasetFieldsContainer $fields)
    {
        $this->fields = $fields;
        return $this;
    }

    public function getFields(): DatasetFieldsContainer
    {
        return $this->fields;
    }
}