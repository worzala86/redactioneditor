<?php

namespace App\Containers;

use App\CollectionTrait;

class ComponentsContainer extends ComponentContainer implements \Iterator
{
    use CollectionTrait;
}