<?php

namespace App\Containers;

use App\CollectionTrait;

class DatasetsContainer extends DatasetContainer
{
    use CollectionTrait;
}