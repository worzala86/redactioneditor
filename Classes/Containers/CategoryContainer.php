<?php

namespace App\Containers;

use App\Container;
use App\Types\UUID;

class CategoryContainer extends Container
{
    private $id;
    private $key;
    private $name;
    private $count;

    /**
     * @param UUID $id
     * @description Identyfikator kategorii
     * @return $this
     */
    public function setId(UUID $id=null)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?UUID
    {
        return $this->id;
    }

    /**
     * @param string $key
     * @description Klucz główny kategorii
     * @return $this
     */
    public function setKey(string $key)
    {
        $this->key = $key;
        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $name
     * @description Nazwa kategorii
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $count
     * @description Liczba projektów w kategorii
     * @return $this
     */
    public function setCount(int $count)
    {
        $this->count = $count;
        return $this;
    }

    public function getCount()
    {
        return $this->count;
    }
}