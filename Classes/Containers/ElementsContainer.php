<?php

namespace App\Containers;

use App\CollectionTrait;

class ElementsContainer extends ElementContainer implements \Iterator
{
    use CollectionTrait;
}