<?php

namespace App\Containers;

use App\CollectionTrait;

class PixabayImagesContainer extends PixabayImageContainer implements \Iterator
{
    use CollectionTrait;
}