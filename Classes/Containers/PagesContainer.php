<?php

namespace App\Containers;

use App\CollectionTrait;

class PagesContainer extends PageContainer implements \Iterator
{
    use CollectionTrait;
}