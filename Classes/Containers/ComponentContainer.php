<?php

namespace App\Containers;

use App\Container;
use App\Modules\Components\Requests\CreateComponentRequest;
use App\Types\ComponentKey;
use App\Types\ComponentKind;
use App\Types\UUID;

class ComponentContainer extends Container
{
    private $id;
    private $key;
    private $name;
    private $description;
    private $file;
    private $count;
    private $kind;
    private $simpleView;

    /**
     * @param int $id
     * @description Identyfikator komponentu
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param ComponentKey $key
     * @description Klucz komponentu wykorzystywany wewnętrznie
     * @return $this
     */
    public function setKey(ComponentKey $key)
    {
        $this->key = $key;
        return $this;
    }

    public function getKey(): ComponentKey
    {
        return $this->key;
    }

    /**
     * @param string $name
     * @description Nazwa komponentu
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $description
     * @description Opis komponentu
     * @return $this
     */
    public function setDescription(string $description = null)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $file
     * @description Plik z kodem html komponentu
     * @return $this
     */
    public function setFile(string $file)
    {
        $this->file = $file;
        return $this;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @param string $count
     * @description Bierząca liczba wykorzystań w serwisie
     * @return $this
     */
    public function setCount(int $count = null)
    {
        $this->count = $count;
        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * @param ComponentKind $kind
     * @description Rodzaj pola do wprowadzania danych w SimpleEdit
     * @return $this
     */
    public function setKind(ComponentKind $kind = null)
    {
        $this->kind = $kind;
        return $this;
    }

    public function getKind(): ?ComponentKind
    {
        return $this->kind;
    }



    /**
     * @param bool $simpleView
     * @description Jeżeli wartość jest true to komponent jest edytowalny w SimpleView w lewym panelu
     * @return $this
     */
    public function setSimpleView(bool $simpleView)
    {
        $this->simpleView = $simpleView;
        return $this;
    }

    public function getSimpleView(): ?bool
    {
        return $this->simpleView;
    }
}