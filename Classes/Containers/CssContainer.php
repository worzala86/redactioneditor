<?php

namespace App\Containers;

use App\Container;

class CssContainer extends Container
{
    private $parameter;
    private $value;

    /**
     * @param string $parameter
     * @description Nazwa parametru css
     * @return $this
     */
    public function setParameter(string $parameter)
    {
        $this->parameter = $parameter;
        return $this;
    }

    public function getParameter(): string
    {
        return $this->parameter;
    }

    /**
     * @param string $value
     * @description Wartość parametru css
     * @return $this
     */
    public function setValue(string $value)
    {
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}