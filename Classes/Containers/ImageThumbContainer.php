<?php

namespace App\Containers;

use App\Container;
use App\Types\UUID;

class ImageThumbContainer extends Container
{
    private $id;
    private $name;
    private $type;
    private $url;
    private $size;

    /**
     * @param UUID $id
     * @description Identyfikator miniaturki
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa miniaturki
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $type
     * @description Typ miniaturki
     * @return $this
     */
    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $url
     * @description Link do miniaturki
     * @return $this
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param int $size
     * @description Rozmiar miniaturki
     * @return $this
     */
    public function setSize(int $size)
    {
        $this->size = $size;
        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }
}