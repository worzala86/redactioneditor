<?php

namespace App\Containers;

use App\CollectionTrait;

class StylesContainer extends StyleContainer implements \Iterator
{
    use CollectionTrait;
}