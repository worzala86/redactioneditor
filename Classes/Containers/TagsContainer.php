<?php

namespace App\Containers;

use App\CollectionTrait;

class TagsContainer extends TagContainer implements \Iterator
{
    use CollectionTrait;
}