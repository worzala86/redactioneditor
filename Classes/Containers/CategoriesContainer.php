<?php

namespace App\Containers;

use App\CollectionTrait;

class CategoriesContainer extends CategoryContainer implements \Iterator
{
    use CollectionTrait;
}