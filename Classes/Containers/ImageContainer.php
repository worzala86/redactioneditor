<?php

namespace App\Containers;

use App\Container;
use App\Types\UUID;

class ImageContainer extends Container
{
    private $id;
    private $name;
    private $type;
    private $size;
    private $thumb;
    private $pixabayLink;
    private $tags;

    /**
     * @param UUID $id
     * @description Identyfikator zdjęcia
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?UUID
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa zdjęcia
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $type
     * @description Typ zdjęcia
     * @return $this
     */
    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param int $size
     * @description Rozmiar zdjęcia
     * @return $this
     */
    public function setSize(int $size)
    {
        $this->size = $size;
        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @param ImageThumbContainer $thumb
     * @description Dane miniaturki
     * @return $this
     */
    public function setThumb(ImageThumbContainer $thumb)
    {
        $this->thumb = $thumb;
        return $this;
    }

    public function getThumb(): ?ImageThumbContainer
    {
        return $this->thumb;
    }

    /**
     * @param string $pixabayLink
     * @description Link do zdjęcia w serwisie pixabay
     * @return $this
     */
    public function setPixabayLink(string $pixabayLink = null)
    {
        $this->pixabayLink = $pixabayLink;
        return $this;
    }

    public function getPixabayLink(): ?string
    {
        return $this->pixabayLink;
    }

    /**
     * @param int $tags
     * @description Tagi przypisane do zdjęcia
     * @return $this
     */
    public function setTags(string $tags = null)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }
}