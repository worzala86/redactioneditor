<?php

namespace App\Containers;

use App\Container;

class FileContainer extends Container
{
    private $name;
    private $file;

    /**
     * @param string $name
     * @description Nazwa pliku
     * @return $this
     */
    public function setName(string $name){
        $this->name = $name;
        return $this;
    }

    public function getName(){
        return $this->name;
    }

    /**
     * @param string $file
     * @description Treść pliku zapisana w formacie base64
     * @return $this
     */
    public function setFile(string $file){
        $this->file = $file;
        return $this;
    }

    public function getFile(){
        return $this->file;
    }
}