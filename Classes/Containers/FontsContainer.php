<?php

namespace App\Containers;

use App\CollectionTrait;

class FontsContainer extends FontContainer implements \Iterator
{
    use CollectionTrait;
}