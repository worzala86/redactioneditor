<?php

namespace App\Containers;

use App\CollectionTrait;

class UsersContainer extends UserContainer implements \Iterator
{
    use CollectionTrait;
}