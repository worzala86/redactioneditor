<?php

namespace App\Containers;

use App\CollectionTrait;

class FilesContainer extends FileContainer implements \Iterator
{
    use CollectionTrait;
}