<?php

namespace App\Containers;

use App\CollectionTrait;

class DatasetFieldsContainer extends DatasetFieldContainer implements \Iterator
{
    use CollectionTrait;
}