<?php

namespace App\Containers;

use App\Container;
use App\Types\UUID;

class TemplateContainer extends Container
{
    private $id;
    private $name;
    private $description;
    private $public;
    private $image;

    /**
     * @param int $id
     * @description Identyfikator template'a
     * @return $this
     */
    public function setId(UUID $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): UUID
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @description Nazwa template'a
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $description
     * @description Opis template'a
     * @return $this
     */
    public function setDescription(string $description = null)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param bool $public
     * @description Informuje o tym czy template jest publiczny czy prywatny
     * @return $this
     */
    public function setPublic(bool $public = null)
    {
        $this->public = $public;
        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    /**
     * @param ImageContainer $image
     * @description Dane zdjęcia
     * @return $this
     */
    public function setImage(ImageContainer $image = null)
    {
        $this->image = $image;
        return $this;
    }

    public function getImage(): ?ImageContainer
    {
        return $this->image;
    }
}