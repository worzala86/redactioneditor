<?php

namespace App\Containers;

use App\Container;
use App\Types\UUID;

class CssTagContainer extends Container
{
    private $tag;
    private $css;

    /**
     * @param string $tag
     * @description Tag css
     * @return $this
     */
    public function setTag(string $tag)
    {
        $this->tag = $tag;
        return $this;
    }

    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * @param CsssContainer $css
     * @description Wartości css dla tagu
     * @return $this
     */
    public function setCss(CsssContainer $css)
    {
        $this->css = $css;
        return $this;
    }

    public function getCss(): CsssContainer
    {
        return $this->css;
    }
}