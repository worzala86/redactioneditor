<?php

namespace App\Containers;

use App\CollectionTrait;

class PageElementFieldsContainer extends PageElementFieldContainer implements \Iterator
{
    use CollectionTrait;
}