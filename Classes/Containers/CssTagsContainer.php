<?php

namespace App\Containers;

use App\CollectionTrait;

class CssTagsContainer extends CssTagContainer implements \Iterator
{
    use CollectionTrait;
}