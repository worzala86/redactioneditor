<?php

namespace App\Containers;

use App\Container;
use App\Types\ShortUUID;
use App\Types\UUID;

class PageElementFieldContainer extends Container
{
    private $field;
    private $value;
    private $altId;
    private $id;

    /**
     * @param ShortUUID $id
     * @description Identyfikator pola
     * @return $this
     */
    public function setId(ShortUUID $id = null)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?ShortUUID
    {
        return $this->id;
    }

    /**
     * @param ShortUUID $field
     * @description Identyfikator pola
     * @return $this
     */
    public function setField(ShortUUID $field = null)
    {
        $this->field = $field;
        return $this;
    }

    public function getField(): ?ShortUUID
    {
        return $this->field;
    }

    /**
     * @param string $value
     * @description Wartość pola - to pole jest wybierane z przypisanego dataseta
     * @return $this
     */
    public function setValue(string $value)
    {
        $this->value = $value;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param ShortUUID $altId
     * @description Identyfikator do css
     * @return $this
     */
    public function setAltId(ShortUUID $altId = null)
    {
        $this->altId = $altId;
        return $this;
    }

    public function getAltId(): ?ShortUUID
    {
        return $this->altId;
    }
}