<?php

namespace App\Containers;

use App\Container;
use App\Types\ShortUUID;
use App\Types\UUID;

class HtmlElementContainer extends Container
{
    private $id;
    private $childrens;
    private $dataSet;
    private $type;
    private $selector;
    private $field;
    private $src;
    private $altId;
    private $alt;
    private $picTitle;
    private $href;
    private $target;
    private $class;
    private $placeholder;
    private $html;
    private $elementId;
    private $elementPositionId;
    private $show = true;
    private $dataSetActive = false;
    private $backgroundActive = false;
    private $originHtmlId;

    public function setFields(array $fields)
    {
        $index = (string)$this->getField();
        if (!is_null($index)) {
            $this->setField(isset($fields[$index]['value'])?$fields[$index]['value']:null);
            $this->setAltId(isset($fields[$index]['altId'])?$fields[$index]['altId']:null);
            $this->setElementId(isset($fields[$index]['id'])?$fields[$index]['id']:null);
        }
        foreach ($this->getChildrens() as $node) {
            $node->setFields($fields);
        }
    }

    /**
     * @param ShortUUID $id
     * @description Identyfikator elementu
     * @return $this
     */
    public function setId(ShortUUID $id = null)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?ShortUUID
    {
        return $this->id;
    }

    /**
     * @param HtmlElementsContainer $childrens
     * @description Elementy wewnętrzne
     * @return $this
     */
    public function setChildrens(HtmlElementsContainer $childrens = null)
    {
        $this->childrens = $childrens;
        return $this;
    }

    public function getChildrens(): ?HtmlElementsContainer
    {
        return $this->childrens;
    }

    /**
     * @param array $dataSet
     * @description Tablica z żródłami danych
     * @return $this
     */
    public function setDataSet(array $dataSet = null)
    {
        $this->dataSet = $dataSet;
        return $this;
    }

    public function getDataSet(): ?array
    {
        return $this->dataSet;
    }

    /**
     * @param string $type
     * @description Typ elementu
     * @return $this
     */
    public function setType(string $type = null)
    {
        $this->type = $type;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $selector
     * @description Selector do rotacji względem dataseta
     * @return $this
     */
    public function setSelector(string $selector = null)
    {
        if ($this->dataSetActive) {
            $this->selector = $selector;
        } else if($this->getChildrens()){
            foreach ($this->getChildrens() as $node) {
                $node->setSelector($selector);
            }
        }
        return $this;
    }

    public function getSelector(): ?string
    {
        return $this->selector;
    }

    /**
     * @param string $field
     * @description Pole z dataseta
     * @return $this
     */
    public function setField(string $field = null)
    {
        $this->field = $field;
        return $this;
    }

    public function getField(): ?string
    {
        return $this->field;
    }

    /**
     * @param string $src
     * @description Pole z dataseta dla obrazków
     * @return $this
     */
    public function setSrc(string $src = null)
    {
        $this->src = $src;
        return $this;
    }

    public function getSrc(): ?string
    {
        return $this->src;
    }

    /**
     * @param ShortUUID $altId
     * @description Alternatywny identyfikator elementu html
     * @return $this
     */
    public function setAltId(ShortUUID $altId = null)
    {
        $this->altId = $altId;
        return $this;
    }

    public function getAltId(): ?ShortUUID
    {
        return $this->altId;
    }

    /**
     * @param string $alt
     * @description Opis seo obrazka
     * @return $this
     */
    public function setAlt(string $alt = null)
    {
        $this->alt = $alt;
        return $this;
    }

    public function getAlt(): ?string
    {
        return $this->alt;
    }

    /**
     * @param string $picTitle
     * @description Tytuł seo obrazka
     * @return $this
     */
    public function setPicTitle(string $picTitle = null)
    {
        $this->picTitle = $picTitle;
        return $this;
    }

    public function getPicTitle(): ?string
    {
        return $this->picTitle;
    }

    /**
     * @param string $href
     * @description Adres url linku - dla tagów "A"
     * @return $this
     */
    public function setHref(string $href = null)
    {
        $this->href = $href;
        return $this;
    }

    public function getHref(): ?string
    {
        return $this->href;
    }

    /**
     * @param string $target
     * @description Numer lub nazwa z dataseta - służy do odczytu treści
     * @return $this
     */
    public function setTarget(string $target = null)
    {
        $this->target = $target;
        return $this;
    }

    public function getTarget(): ?string
    {
        return $this->target;
    }

    /**
     * @param string $class
     * @description Klasa elementu do CSS
     * @return $this
     */
    public function setClass(string $class = null)
    {
        $this->class = $class;
        return $this;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    /**
     * @param string $placeholder
     * @description Placeholder od inputa
     * @return $this
     */
    public function setPlaceholder(string $placeholder = null)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    public function getPlaceholder(): ?string
    {
        return $this->placeholder;
    }

    /**
     * @param string $html
     * @description Własny kod html np. z mapami Google
     * @return $this
     */
    public function setHtml(string $html = null)
    {
        $this->html = $html;
        return $this;
    }

    public function getHtml(): ?string
    {
        return $this->html;
    }

    /**
     * @param UUID $elementId
     * @description Identyfikator elementu
     * @return $this
     */
    public function setElementId(UUID $elementId = null)
    {
        $this->elementId = $elementId;
        return $this;
    }

    public function getElementId(): ?UUID
    {
        return $this->elementId;
    }

    /**
     * @param UUID $elementPositionId
     * @description Identyfikator pozycji elementu
     * @return $this
     */
    public function setElementPositionId(UUID $elementPositionId = null)
    {
        $this->elementPositionId = $elementPositionId;
        return $this;
    }

    public function getElementPositionId(): ?UUID
    {
        return $this->elementPositionId;
    }

    /**
     * @param bool $show
     * @description Flaga widoczności elementu
     * @return $this
     */
    public function setShow(bool $show)
    {
        $this->show = $show;
        return $this;
    }

    public function getShow(): bool
    {
        return $this->show;
    }

    /**
     * @param bool $dataSetActive
     * @description Flaga informuje czy element można podpiać pod rotatora - dataSet'a
     * @return $this
     */
    public function setDataSetActive(bool $dataSetActive)
    {
        $this->dataSetActive = $dataSetActive;
        return $this;
    }

    public function getDataSetActive(): bool
    {
        return $this->dataSetActive;
    }

    /**
     * @param bool $backgroundActive
     * @description Flaga informuje czy element można temu elementowi przypisać tło - dataSet'a
     * @return $this
     */
    public function setBackgroundActive(bool $backgroundActive)
    {
        $this->backgroundActive = $backgroundActive;
        return $this;
    }

    public function getBackgroundActive(): bool
    {
        return $this->backgroundActive;
    }

    /**
     * @param int $originHtmlId Numer html-id
     * @description Identyfikator elementu
     * @return $this
     */
    public function setOriginHtmlId(int $originHtmlId = null)
    {
        $this->originHtmlId = $originHtmlId;
        return $this;
    }

    public function getOriginHtmlId(): ?int
    {
        return $this->originHtmlId;
    }
}