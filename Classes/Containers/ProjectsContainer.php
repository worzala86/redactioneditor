<?php

namespace App\Containers;

use App\CollectionTrait;

class ProjectsContainer extends ProjectContainer implements \Iterator
{
    use CollectionTrait;
}