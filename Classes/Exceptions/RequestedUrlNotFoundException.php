<?php

namespace App\Exceptions;

use App\NamedException;

class RequestedUrlNotFoundException extends \Exception implements NamedException
{
}