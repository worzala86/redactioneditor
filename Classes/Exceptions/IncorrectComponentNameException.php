<?php

namespace App\Exceptions;

use App\NamedException;

class IncorrectComponentNameException extends \Exception implements NamedException
{
}