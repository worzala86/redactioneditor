<?php

namespace App\Exceptions;

use App\NamedException;

class WrongPageKindException extends \Exception implements NamedException
{
}