<?php

namespace App\Exceptions;

use App\NamedException;

class IncorrectDatasetNameException extends \Exception implements NamedException
{
}