<?php

namespace App\Exceptions;

use App\NamedException;

class IncorrectDomainException extends \Exception implements NamedException
{
}