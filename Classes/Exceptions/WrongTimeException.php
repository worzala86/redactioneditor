<?php

namespace App\Exceptions;

use App\NamedException;

class WrongTimeException extends \Exception implements NamedException
{
}