<?php

namespace App\Exceptions;

use App\NamedException;

class PasswordMustBeInSHA512Exception extends \Exception implements NamedException
{
}