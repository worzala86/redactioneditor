<?php

namespace App\Exceptions;

use App\NamedException;

class IncorrectIdException extends \Exception implements NamedException
{
}