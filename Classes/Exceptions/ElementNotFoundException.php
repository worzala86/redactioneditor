<?php

namespace App\Exceptions;

use App\NamedException;

class ElementNotFoundException extends \Exception implements NamedException
{
}