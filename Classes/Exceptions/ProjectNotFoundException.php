<?php

namespace App\Exceptions;

use App\NamedException;

class ProjectNotFoundException extends \Exception implements NamedException
{
}