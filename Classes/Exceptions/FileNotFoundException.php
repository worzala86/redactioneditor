<?php

namespace App\Exceptions;

use App\NamedException;

class FileNotFoundException extends \Exception implements NamedException
{
}