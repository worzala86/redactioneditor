<?php

namespace App\Exceptions;

use App\NamedException;

class RoutingHandlerNotFoundException extends \Exception implements NamedException
{
}