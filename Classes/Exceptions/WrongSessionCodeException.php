<?php

namespace App\Exceptions;

use App\NamedException;

class WrongSessionCodeException extends \Exception implements NamedException
{
}