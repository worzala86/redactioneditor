<?php

namespace App\Exceptions;

use App\NamedException;

class CannotCreateDirectoryException extends \Exception implements NamedException
{
}