<?php

namespace App\Exceptions;

use App\NamedException;

class IncorrectUrlAddressException extends \Exception implements NamedException
{
}