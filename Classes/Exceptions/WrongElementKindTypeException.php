<?php

namespace App\Exceptions;

use App\NamedException;

class WrongElementKindTypeException extends \Exception implements NamedException
{
}