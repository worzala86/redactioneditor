<?php

namespace App\Exceptions;

use App\NamedException;

class MustBeLoggedToUseThisEndpointException extends \Exception implements NamedException
{
}