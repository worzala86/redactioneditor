<?php

namespace App\Exceptions;

use App\NamedException;

class IncorrectMailAddressException extends \Exception implements NamedException
{
}