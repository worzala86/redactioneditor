<?php

namespace App\Exceptions;

use App\NamedException;

class DontHaveAdminRightsException extends \Exception implements NamedException
{
}