<?php

namespace App\Exceptions;

use App\NamedException;

class ComponentNotFoundException extends \Exception implements NamedException
{
}