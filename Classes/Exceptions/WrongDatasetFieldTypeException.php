<?php

namespace App\Exceptions;

use App\NamedException;

class WrongDatasetFieldTypeException extends \Exception implements NamedException
{
}