<?php

namespace App\Exceptions;

use App\NamedException;

class WrongSortingTypeException extends \Exception implements NamedException
{
}