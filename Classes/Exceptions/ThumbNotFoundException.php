<?php

namespace App\Exceptions;

use App\NamedException;

class ThumbNotFoundException extends \Exception implements NamedException
{
}