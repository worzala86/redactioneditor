-- MySQL dump 10.16  Distrib 10.1.40-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: redaction
-- ------------------------------------------------------
-- Server version	10.1.40-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `components`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(20) DEFAULT NULL,
  `description` text,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `file` varchar(25) NOT NULL COMMENT 'Nazwa pliku z fragmentem html używanym przez edytor',
  `kind` varchar(90) NOT NULL COMMENT 'Rodzaj pola. W edycji komponentu jest używane to pole do rodzaju pola wprowadzania danych.',
  `simple_view` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `key` (`key`) USING BTREE,
  KEY `fk_components_added_by` (`added_by`),
  KEY `fk_components_deleted_by` (`deleted_by`),
  KEY `fk_components_updated_by` (`updated_by`),
  KEY `fk_components_added_ip_id` (`added_ip_id`),
  KEY `fk_components_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_components_updated_ip_id` (`updated_ip_id`),
  CONSTRAINT `fk_components_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_components_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_components_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_components_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_components_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_components_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `components`
--

LOCK TABLES `components` WRITE;
/*!40000 ALTER TABLE `components` DISABLE KEYS */;
INSERT INTO `components` VALUES (1,'container','Standardowy kontener dla innych elementów. Wnętrze elementu można zapętlać datasetami.','$�ky�韟',4294967295,1,1,0,NULL,NULL,1552595873,1,1,'Kontener','container.html','text',0),(2,'header-1','Nagłowek pierwszego poziomu.','I��`k��,',4294967295,1,1,0,NULL,NULL,1556607574,1,1,'Nagłówek x1','header-1.html','text',1),(3,'header-2','Nagłowek drugiego poziomu.','�Nt�=g��',4294967295,1,1,0,NULL,NULL,1556607570,1,1,'Nagłówek x2','header-2.html','text',1),(4,'header-3','Nagłowek trzeciego poziomu.','~|��\\��',4294967295,1,1,0,NULL,NULL,1556607566,1,1,'Nagłówek x3','header-3.html','text',1),(5,'header-4','Nagłowek czwartego poziomu.','���e��U',4294967295,1,1,0,NULL,NULL,1556607562,1,1,'Nagłówek x4','header-4.html','text',1),(6,'header-5','Nagłowek piątego poziomu.','�P�;8!',4294967295,1,1,0,NULL,NULL,1556607558,1,1,'Nagłówek x5','header-5.html','text',1),(7,'header-6','Nagłowek szóstego poziomu.','�z�Q,SBJ',4294967295,1,1,0,NULL,NULL,1556607554,1,1,'Nagłówek x6','header-6.html','text',1),(8,'paragraph','Paragfaf - służy do przechowywania na ogół opisu lub dłuższego tekstu.','�d�C8iW',4294967295,1,1,0,NULL,NULL,1556607587,1,1,'Artykuł - paragraf','paragraph.html','textarea',1),(9,'image','Element jest obrazkiem.','~���t��',4294967295,1,1,0,NULL,NULL,1556607601,1,1,'Obrazek','image.html','image',1),(10,'link','Link do innej podstrony.','7�S�����',4294967295,1,1,0,NULL,NULL,1556607539,1,1,'Odnośnik','link.html','text',1),(11,'pagination','Komponent umożliwia przechodzenie pomiedzy podstronami dataseta.','���%���W',4294967295,1,1,0,NULL,NULL,1552503197,1,1,'Stronnicowanie','pagination.html','text',0),(12,'button','Standardowy przycisk który może służyć na przykład jako link.','tſ[�C�',4294967295,1,1,0,NULL,NULL,1556840009,1,1,'Przycisk','button.html','text',1),(15,'input','Element służy do wprowadzania danych np. w formularzu.','��i\Zܒb�',1553482661,1,1,0,NULL,NULL,NULL,NULL,1,'Input','input.html','text',0),(16,'label','Etykieta jest najczęściej wykorzystywana do pokazania tytułu Inputa.','��\'{��',1553560800,1,1,0,NULL,NULL,1556607594,1,1,'Etykieta','label.html','text',1),(17,'textarea','Eleement służy do wprowadzania długiego tekstu do storny.','�ie�٩��',1553572815,1,1,0,NULL,NULL,1559489520,1,11,'Textarea','textarea.html','textarea',1),(18,'html','Do tego elementu można wstawić własny kod html np. z mapami Google','�V\Z�k\"',1553573910,1,1,0,NULL,NULL,1553931198,1,1,'Kod HTML','html.html','text',0),(19,'article','Jest to element przeznaczony specjalnie dla artykułu.',']��h6�aG',1555283241,1,1,0,NULL,NULL,1557621216,1,1,'Artykuł','article.html','textarea',1),(20,'span','Jest to kontener do treści tekstowej.','Oi:�� J�',1560218056,1,21,0,NULL,NULL,NULL,NULL,NULL,'span','span.html','text',1);
/*!40000 ALTER TABLE `components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `components_view`
--

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `components_view` (
  `id` tinyint NOT NULL,
  `uuid` tinyint NOT NULL,
  `key` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `file` tinyint NOT NULL,
  `deleted` tinyint NOT NULL,
  `count` tinyint NOT NULL,
  `kind` tinyint NOT NULL,
  `simple_view` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `crm_pages`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crm_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_crm_pages_added_by` (`added_by`),
  KEY `fk_crm_pages_deleted_by` (`deleted_by`),
  KEY `fk_crm_pages_updated_by` (`updated_by`),
  KEY `fk_crm_pages_added_ip_id` (`added_ip_id`),
  KEY `fk_crm_pages_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_crm_pages_updated_ip_id` (`updated_ip_id`),
  CONSTRAINT `fk_crm_pages_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_crm_pages_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_crm_pages_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_crm_pages_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_crm_pages_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_crm_pages_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crm_pages`
--

LOCK TABLES `crm_pages` WRITE;
/*!40000 ALTER TABLE `crm_pages` DISABLE KEYS */;
INSERT INTO `crm_pages` VALUES (1,'b3�[�3�',1550452397,1,1,1553981789,1,1,0,NULL,NULL,'/regulamin','Regulamin','<center>Regulamin</center>\n<br>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tincidunt aliquam ligula. Nam at nisl id arcu molestie molestie sed nec lacus. Ut ut dignissim arcu. Curabitur in hendrerit ante. Vestibulum dapibus porttitor posuere. Etiam nibh nibh, finibus sed ligula eget, dictum ornare mauris. Quisque ornare tortor lorem. Donec dictum justo nec magna pharetra, sed mattis purus eleifend. Nunc mi tellus, tempus vitae ipsum a, scelerisque posuere augue. Phasellus ullamcorper velit id tortor porta, eu sagittis risus pulvinar. In ornare euismod leo a ultrices. Maecenas auctor nisi sit amet lacus dictum imperdiet. Curabitur interdum cursus enim eu venenatis.\n<br>\nSuspendisse egestas ex eleifend lacus posuere, non facilisis est cursus. Curabitur auctor metus sit amet ultricies pretium. Quisque volutpat purus et risus suscipit consectetur. Etiam dictum, est id mattis convallis, libero nisi vehicula libero, vel pharetra mi sapien nec odio. Cras vitae eleifend dolor. Nam porttitor augue tortor, eu commodo quam gravida at. Vestibulum tempus ullamcorper eros, ut posuere nulla pellentesque non. Nulla faucibus eget arcu id rutrum. Integer sed quam non lacus accumsan posuere. In leo libero, pulvinar convallis auctor et, bibendum et purus.\n<br>\nMorbi eget lacus tempus, fermentum nibh vitae, lacinia urna. Quisque dui libero, faucibus non tortor ut, euismod sodales urna. Fusce lacinia pretium malesuada. Sed placerat auctor nibh, at commodo nulla sodales a. Vestibulum sed augue at justo iaculis lacinia. Morbi finibus porttitor dui eu molestie. Curabitur ut odio quis odio malesuada congue. In feugiat elit vitae leo consectetur sodales. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus tempor ac diam nec tempus. Duis suscipit eros et lacus finibus, in congue nibh porttitor. Sed congue mi porta nulla tristique rhoncus a quis arcu. Morbi sed ligula felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque et auctor lectus.\n<br>\nCurabitur auctor vulputate lobortis. Morbi faucibus metus consectetur, euismod ligula non, ultricies est. Cras ut augue dignissim, semper massa non, consequat nibh. Mauris nisl velit, varius ac pharetra sit amet, faucibus quis nisi. Aliquam condimentum mollis leo non semper. Nullam aliquam cursus turpis. Proin et feugiat est. Fusce mollis ligula id turpis ultricies, in consequat mi auctor. Etiam eget hendrerit tellus, id pellentesque massa. Aliquam pulvinar dolor in ipsum tincidunt, vitae egestas nibh porta. Vivamus non erat vitae tellus pretium vehicula id sit amet lectus. Nunc vitae libero venenatis, convallis enim eget, pretium nibh. Nulla vel cursus nisl. Proin a consectetur est, ut gravida augue.\n<br>\nSed sapien tortor, placerat eget dolor eu, consectetur sodales orci. Etiam id ex at velit rutrum dignissim. Proin vel dolor a elit laoreet accumsan. Nam imperdiet porta dui ac consequat. Vivamus ac porta libero. Maecenas pharetra, orci a ullamcorper sagittis, odio risus tempor dolor, vitae consequat magna libero sed elit. Duis ullamcorper bibendum risus, et laoreet purus tincidunt ac. Mauris nec arcu nisi. Vivamus blandit arcu et leo sodales ultricies ullamcorper non orci. Aliquam vel ipsum nec nibh varius laoreet. Nam pharetra purus feugiat justo commodo commodo. Donec ipsum odio, elementum sit amet tincidunt eu, condimentum quis purus. Quisque ac consequat risus. Etiam consectetur commodo lectus sed faucibus.'),(2,',Dfm��K\n',1550452421,1,1,1553981785,1,1,0,NULL,NULL,'/polityka-prywatnosci','Polityka prywatności','<center>Polityka prywatności</center>\n<br>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tincidunt aliquam ligula. Nam at nisl id arcu molestie molestie sed nec lacus. Ut ut dignissim arcu. Curabitur in hendrerit ante. Vestibulum dapibus porttitor posuere. Etiam nibh nibh, finibus sed ligula eget, dictum ornare mauris. Quisque ornare tortor lorem. Donec dictum justo nec magna pharetra, sed mattis purus eleifend. Nunc mi tellus, tempus vitae ipsum a, scelerisque posuere augue. Phasellus ullamcorper velit id tortor porta, eu sagittis risus pulvinar. In ornare euismod leo a ultrices. Maecenas auctor nisi sit amet lacus dictum imperdiet. Curabitur interdum cursus enim eu venenatis.\n<br>\nSuspendisse egestas ex eleifend lacus posuere, non facilisis est cursus. Curabitur auctor metus sit amet ultricies pretium. Quisque volutpat purus et risus suscipit consectetur. Etiam dictum, est id mattis convallis, libero nisi vehicula libero, vel pharetra mi sapien nec odio. Cras vitae eleifend dolor. Nam porttitor augue tortor, eu commodo quam gravida at. Vestibulum tempus ullamcorper eros, ut posuere nulla pellentesque non. Nulla faucibus eget arcu id rutrum. Integer sed quam non lacus accumsan posuere. In leo libero, pulvinar convallis auctor et, bibendum et purus.\n<br>\nMorbi eget lacus tempus, fermentum nibh vitae, lacinia urna. Quisque dui libero, faucibus non tortor ut, euismod sodales urna. Fusce lacinia pretium malesuada. Sed placerat auctor nibh, at commodo nulla sodales a. Vestibulum sed augue at justo iaculis lacinia. Morbi finibus porttitor dui eu molestie. Curabitur ut odio quis odio malesuada congue. In feugiat elit vitae leo consectetur sodales. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus tempor ac diam nec tempus. Duis suscipit eros et lacus finibus, in congue nibh porttitor. Sed congue mi porta nulla tristique rhoncus a quis arcu. Morbi sed ligula felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque et auctor lectus.\n<br>\nCurabitur auctor vulputate lobortis. Morbi faucibus metus consectetur, euismod ligula non, ultricies est. Cras ut augue dignissim, semper massa non, consequat nibh. Mauris nisl velit, varius ac pharetra sit amet, faucibus quis nisi. Aliquam condimentum mollis leo non semper. Nullam aliquam cursus turpis. Proin et feugiat est. Fusce mollis ligula id turpis ultricies, in consequat mi auctor. Etiam eget hendrerit tellus, id pellentesque massa. Aliquam pulvinar dolor in ipsum tincidunt, vitae egestas nibh porta. Vivamus non erat vitae tellus pretium vehicula id sit amet lectus. Nunc vitae libero venenatis, convallis enim eget, pretium nibh. Nulla vel cursus nisl. Proin a consectetur est, ut gravida augue.\n<br>\nSed sapien tortor, placerat eget dolor eu, consectetur sodales orci. Etiam id ex at velit rutrum dignissim. Proin vel dolor a elit laoreet accumsan. Nam imperdiet porta dui ac consequat. Vivamus ac porta libero. Maecenas pharetra, orci a ullamcorper sagittis, odio risus tempor dolor, vitae consequat magna libero sed elit. Duis ullamcorper bibendum risus, et laoreet purus tincidunt ac. Mauris nec arcu nisi. Vivamus blandit arcu et leo sodales ultricies ullamcorper non orci. Aliquam vel ipsum nec nibh varius laoreet. Nam pharetra purus feugiat justo commodo commodo. Donec ipsum odio, elementum sit amet tincidunt eu, condimentum quis purus. Quisque ac consequat risus. Etiam consectetur commodo lectus sed faucibus.'),(3,'v!\'�,��',1550452438,1,1,1559461861,1,6,0,NULL,NULL,'/ciasteczka','Ciasteczka','<center>Ciasteczka</center>\n<br>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tincidunt aliquam ligula. Nam at nisl id arcu molestie molestie sed nec lacus. Ut ut dignissim arcu. Curabitur in hendrerit ante. Vestibulum dapibus porttitor posuere. Etiam nibh nibh, finibus sed ligula eget, dictum ornare mauris. Quisque ornare tortor lorem. Donec dictum justo nec magna pharetra, sed mattis purus eleifend. Nunc mi tellus, tempus vitae ipsum a, scelerisque posuere augue. Phasellus ullamcorper velit id tortor porta, eu sagittis risus pulvinar. In ornare euismod leo a ultrices. Maecenas auctor nisi sit amet lacus dictum imperdiet. Curabitur interdum cursus enim eu venenatis.\n<br>\nSuspendisse egestas ex eleifend lacus posuere, non facilisis est cursus. Curabitur auctor metus sit amet ultricies pretium. Quisque volutpat purus et risus suscipit consectetur. Etiam dictum, est id mattis convallis, libero nisi vehicula libero, vel pharetra mi sapien nec odio. Cras vitae eleifend dolor. Nam porttitor augue tortor, eu commodo quam gravida at. Vestibulum tempus ullamcorper eros, ut posuere nulla pellentesque non. Nulla faucibus eget arcu id rutrum. Integer sed quam non lacus accumsan posuere. In leo libero, pulvinar convallis auctor et, bibendum et purus.\n<br>\nMorbi eget lacus tempus, fermentum nibh vitae, lacinia urna. Quisque dui libero, faucibus non tortor ut, euismod sodales urna. Fusce lacinia pretium malesuada. Sed placerat auctor nibh, at commodo nulla sodales a. Vestibulum sed augue at justo iaculis lacinia. Morbi finibus porttitor dui eu molestie. Curabitur ut odio quis odio malesuada congue. In feugiat elit vitae leo consectetur sodales. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus tempor ac diam nec tempus. Duis suscipit eros et lacus finibus, in congue nibh porttitor. Sed congue mi porta nulla tristique rhoncus a quis arcu. Morbi sed ligula felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque et auctor lectus.\n<br>\nCurabitur auctor vulputate lobortis. Morbi faucibus metus consectetur, euismod ligula non, ultricies est. Cras ut augue dignissim, semper massa non, consequat nibh. Mauris nisl velit, varius ac pharetra sit amet, faucibus quis nisi. Aliquam condimentum mollis leo non semper. Nullam aliquam cursus turpis. Proin et feugiat est. Fusce mollis ligula id turpis ultricies, in consequat mi auctor. Etiam eget hendrerit tellus, id pellentesque massa. Aliquam pulvinar dolor in ipsum tincidunt, vitae egestas nibh porta. Vivamus non erat vitae tellus pretium vehicula id sit amet lectus. Nunc vitae libero venenatis, convallis enim eget, pretium nibh. Nulla vel cursus nisl. Proin a consectetur est, ut gravida augue.\n<br>\nSed sapien tortor, placerat eget dolor eu, consectetur sodales orci. Etiam id ex at velit rutrum dignissim. Proin vel dolor a elit laoreet accumsan. Nam imperdiet porta dui ac consequat. Vivamus ac porta libero. Maecenas pharetra, orci a ullamcorper sagittis, odio risus tempor dolor, vitae consequat magna libero sed elit. Duis ullamcorper bibendum risus, et laoreet purus tincidunt ac. Mauris nec arcu nisi. Vivamus blandit arcu et leo sodales ultricies ullamcorper non orci. Aliquam vel ipsum nec nibh varius laoreet. Nam pharetra purus feugiat justo commodo commodo. Donec ipsum odio, elementum sit amet tincidunt eu, condimentum quis purus. Quisque ac consequat risus. Etiam consectetur commodo lectus sed faucibus.');
/*!40000 ALTER TABLE `crm_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datasets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) DEFAULT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `name` (`name`,`project_id`,`deleted`,`user_id`),
  KEY `fk_datasets_added_by` (`added_by`),
  KEY `fk_datasets_deleted_by` (`deleted_by`),
  KEY `fk_datasets_updated_by` (`updated_by`),
  KEY `fk_datasets_added_ip_id` (`added_ip_id`),
  KEY `fk_datasets_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_datasets_updated_ip_id` (`updated_ip_id`),
  KEY `fk_datasets_project_id` (`project_id`),
  KEY `fk_datasets_user_id` (`user_id`),
  CONSTRAINT `fk_datasets_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datasets`
--

LOCK TABLES `datasets` WRITE;
/*!40000 ALTER TABLE `datasets` DISABLE KEYS */;
/*!40000 ALTER TABLE `datasets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datasets_fields`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datasets_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) DEFAULT NULL,
  `datasets_id` int(10) unsigned NOT NULL,
  `type` varchar(250) DEFAULT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `name` (`name`,`datasets_id`,`deleted`),
  KEY `fk_datasets_fields_added_by` (`added_by`),
  KEY `fk_datasets_fields_deleted_by` (`deleted_by`),
  KEY `fk_datasets_fields_updated_by` (`updated_by`),
  KEY `fk_datasets_fields_added_ip_id` (`added_ip_id`),
  KEY `fk_datasets_fields_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_datasets_fields_updated_ip_id` (`updated_ip_id`),
  KEY `fk_datasets_fields_datasets_id` (`datasets_id`),
  CONSTRAINT `fk_datasets_fields_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_datasets_id` FOREIGN KEY (`datasets_id`) REFERENCES `datasets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_datasets_fields_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datasets_fields`
--

LOCK TABLES `datasets_fields` WRITE;
/*!40000 ALTER TABLE `datasets_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `datasets_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `image_id` int(10) unsigned DEFAULT NULL,
  `kind` varchar(20) DEFAULT NULL,
  `content_id` int(10) unsigned DEFAULT NULL,
  `download_times` int(10) unsigned DEFAULT '0' COMMENT 'Jest to liczba pobrań. Określa ona ile razy w eddytorze wskazany element został prawdopodobnie użyty',
  `style_id` int(10) unsigned DEFAULT NULL,
  `root_style_id` int(10) unsigned DEFAULT NULL COMMENT 'Jest to domyślny główny styl wybranego elementu - tylko w celach podglądowych',
  `hidden` tinyint(1) DEFAULT '0',
  `parsed` tinyint(1) DEFAULT '0',
  `json` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_elements_added_by` (`added_by`),
  KEY `fk_elements_deleted_by` (`deleted_by`),
  KEY `fk_elements_updated_by` (`updated_by`),
  KEY `fk_elements_added_ip_id` (`added_ip_id`),
  KEY `fk_elements_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_elements_updated_ip_id` (`updated_ip_id`),
  KEY `fk_elements_user_id` (`user_id`),
  KEY `fk_elements_image_id` (`image_id`),
  KEY `fk_elements_content_id` (`content_id`),
  KEY `fk_elements_style_id` (`style_id`),
  KEY `fk_elements_root_style_id` (`root_style_id`),
  KEY `json` (`json`),
  CONSTRAINT `fk_elements_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_content_id` FOREIGN KEY (`content_id`) REFERENCES `html` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_image_id` FOREIGN KEY (`image_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_root_style_id` FOREIGN KEY (`root_style_id`) REFERENCES `styles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_style_id` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_elements_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements`
--

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;
INSERT INTO `elements` VALUES (1,1,')\"��)\Z* ',1561411717,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1,0,NULL,6,0,0,0),(2,1,'�B��ts�h',1561411718,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',12,0,NULL,6,0,0,0),(3,1,'����Z�',1561411718,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',22,0,NULL,6,0,0,0),(4,1,'r��\nC�&',1561411719,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',35,0,NULL,6,0,0,0),(5,1,'k��mZ�R',1561411720,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',64,0,NULL,6,0,0,0),(6,1,'\'�oF�U��',1561411720,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',73,0,NULL,6,0,0,0),(7,1,'uX�\"�\0�u',1561411721,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',85,0,NULL,6,0,0,0),(8,1,'��(i�M0',1561411722,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',98,0,NULL,6,0,0,0),(9,1,'�D�����',1561411723,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',121,0,NULL,6,0,0,0),(10,1,'\nJĪ\0�y',1561411723,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',149,0,NULL,6,0,0,0),(11,1,'b#*�b',1561411723,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',161,0,NULL,6,0,0,0),(12,1,'�5���R',1561411724,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',173,0,NULL,6,0,0,0),(13,1,'	K�Z�#_�',1561411725,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',181,0,NULL,6,0,0,0),(14,1,'������',1561411726,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',219,0,NULL,6,0,0,0),(15,1,'�:ZH��z',1561411726,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',230,0,NULL,6,0,0,0),(16,1,'��O�#̖',1561411727,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',257,0,NULL,6,0,0,0),(17,1,'�}U���;',1561411728,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',269,0,NULL,6,0,0,0),(18,1,'�j�,�ܸ�',1561411728,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',286,0,NULL,6,0,0,0),(19,1,'=ɣ�(��',1561411729,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',295,0,NULL,6,0,0,0),(20,1,'݄���2u',1561411729,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',305,0,NULL,6,0,0,0),(21,1,'ך=L?��8',1561411730,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',313,0,NULL,6,0,0,0),(22,1,'%6	����',1561411730,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',335,0,NULL,6,0,0,0),(23,1,'�6cz�kTD',1561411731,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',345,0,NULL,6,0,0,0),(24,1,'4����k�c',1561411732,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',364,0,NULL,6,0,0,0),(25,1,'p�%�=?',1561411732,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',391,0,NULL,6,0,0,0),(26,1,'����uw',1561411733,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',406,0,NULL,6,0,0,0),(27,1,'\'\Ztwܚ�',1561411733,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',417,0,NULL,6,0,0,0),(28,1,'\"<��$�\0',1561411734,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',428,0,NULL,6,0,0,0),(29,1,'�5�K�cH�',1561411734,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',437,0,NULL,6,0,0,0),(30,1,'�Yo/�@ژ',1561411735,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',444,0,NULL,6,0,0,0),(31,1,'���e�Z�',1561411736,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',481,0,NULL,6,0,0,0),(32,1,'���4+�x�',1561411736,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',489,0,NULL,6,0,0,0),(33,1,'���m\'�I',1561411737,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',499,0,NULL,6,0,0,0),(34,1,'k��t\0	k�',1561411737,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',514,0,NULL,6,0,0,0),(35,1,']\r�%)�@',1561411738,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',526,0,NULL,6,0,0,0),(36,1,'��K�D�/',1561411739,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',535,0,NULL,6,0,0,0),(37,1,'��</,��Q',1561411739,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',561,0,NULL,6,0,0,0),(38,1,'�s�A�dp',1561411739,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',572,0,NULL,6,0,0,0),(39,1,'��6�_',1561411743,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',580,0,NULL,6,0,0,0),(40,1,'`�y-Z��',1561411744,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',696,0,NULL,6,0,0,0),(41,1,'*18\'B�`U',1561411745,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',705,0,NULL,6,0,0,0),(42,1,'q��\\�fPe',1561411745,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',717,0,NULL,6,0,0,0),(43,1,'��W�E1�',1561411751,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',724,0,NULL,6,0,0,0),(44,1,'�K�{Ա��',1561411752,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',892,0,NULL,6,0,0,0),(45,1,'�|�|,6�)',1561411753,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',916,0,NULL,6,0,0,0),(46,1,'��@�+p',1561411753,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',937,0,NULL,6,0,0,0),(47,1,'u֟;!�_�',1561411753,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',946,0,NULL,6,0,0,0),(48,1,'�fU6�:�K',1561411754,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',956,0,NULL,6,0,0,0),(49,1,'j:�\n���J',1561411755,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',964,0,NULL,6,0,0,0),(50,1,'<C��\'�',1561411756,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',990,0,NULL,6,0,0,0),(51,1,'ф��T��',1561411757,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1008,0,NULL,6,0,0,0),(52,1,'<D-\\�!�',1561411757,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1034,0,NULL,6,0,0,0),(53,1,'���E���',1561411758,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1048,0,NULL,6,0,0,0),(54,1,'��]���G',1561411759,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1058,0,NULL,6,0,0,0),(55,1,'�B_�a��',1561411760,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1079,0,NULL,6,0,0,0),(56,1,'�r(�h�#',1561411760,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1110,0,NULL,6,0,0,0),(57,1,'ze��x�A&',1561411761,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1126,0,NULL,6,0,0,0),(58,1,'����d2',1561411761,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1136,0,NULL,6,0,0,0),(59,1,'A �\Z�Cc	',1561411762,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1142,0,NULL,6,0,0,0),(60,1,'g�w�*�V',1561411763,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1165,0,NULL,6,0,0,0),(61,1,'�\'ƹX;V',1561411764,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1201,0,NULL,6,0,0,0),(62,1,'D�C&�9|',1561411764,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1208,0,NULL,6,0,0,0),(63,1,'��Q0�',1561411764,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1214,0,NULL,6,0,0,0),(64,1,'���<<s',1561411765,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1223,0,NULL,6,0,0,0),(65,1,'K��\0���',1561411765,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1237,0,NULL,6,0,0,0),(66,1,'�q2�W�rl',1561411766,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1248,0,NULL,6,0,0,0),(67,1,'rm��7\Z��',1561411769,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1256,0,NULL,6,0,0,0),(68,1,'���T�!�',1561411770,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1371,0,NULL,6,0,0,0),(69,1,'B��+��*',1561411777,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1396,0,NULL,6,0,0,0),(70,1,'������a5',1561411777,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1563,0,NULL,6,0,0,0),(71,1,')������E',1561411777,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1572,0,NULL,6,0,0,0),(72,1,'C�9W�7|',1561411778,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1582,0,NULL,6,0,0,0),(73,1,'Q\0�ܔ���',1561411778,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1589,0,NULL,6,0,0,0),(74,1,'��Wv:',1561411778,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1597,0,NULL,6,0,0,0),(75,1,'G�7$�e�',1561411779,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1608,0,NULL,6,0,0,0),(76,1,'�+�]2�T',1561411780,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1614,0,NULL,6,0,0,0),(77,1,'�y?�sŀ',1561411781,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1637,0,NULL,6,0,0,0),(78,1,'aZ�f@',1561411781,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1666,0,NULL,6,0,0,0),(79,1,'���M�`L�',1561411782,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1686,0,NULL,6,0,0,0),(80,1,'��;�oD��',1561411782,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1700,0,NULL,6,0,0,0),(81,1,'��t�U@�D',1561411783,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1711,0,NULL,6,0,0,0),(82,1,'/�!c+��',1561411783,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1719,0,NULL,6,0,0,0),(83,1,'_�}���K',1561411783,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1728,0,NULL,6,0,0,0),(84,1,'��͢�D�',1561411788,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1735,0,NULL,6,0,0,0),(85,1,'7�t�Q�',1561411789,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1856,0,NULL,6,0,0,0),(86,1,'��7+7�',1561411790,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1882,0,NULL,6,0,0,0),(87,1,'z�Gaİ',1561411791,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1903,0,NULL,6,0,0,0),(88,1,'\\&��V7:',1561411791,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1939,0,NULL,6,0,0,0),(89,1,'IAm`���',1561411792,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1954,0,NULL,6,0,0,0),(90,1,'��tL/��',1561411793,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',1979,0,NULL,6,0,0,0),(91,1,'�+:H�x/',1561411794,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2008,0,NULL,6,0,0,0),(92,1,'��h��x�',1561411795,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2029,0,NULL,6,0,0,0),(93,1,'�_\Z���z',1561411796,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2054,0,NULL,6,0,0,0),(94,1,'+����	ߥ',1561411796,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2071,0,NULL,6,0,0,0),(95,1,'V7d�}',1561411797,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2084,0,NULL,6,0,0,0),(96,1,'�CCrGRa\\',1561411797,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2093,0,NULL,6,0,0,0),(97,1,'�8į\\',1561411797,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2100,0,NULL,6,0,0,0),(98,1,'W���',1561411798,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2105,0,NULL,6,0,0,0),(99,1,'�W�?���',1561411798,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2115,0,NULL,6,0,0,0),(100,1,'��]��',1561411799,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2137,0,NULL,6,0,0,0),(101,1,'ܬf&Q+��',1561411799,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2143,0,NULL,6,0,0,0),(102,1,'8�]\nC!Ej',1561411799,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2148,0,NULL,6,0,0,0),(103,1,'��)r�\Z�',1561411800,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2156,0,NULL,6,0,0,0),(104,1,'�ˠ���4	',1561411800,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2169,0,NULL,6,0,0,0),(105,1,'�H:�-�E<',1561411801,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2182,0,NULL,6,0,0,0),(106,1,'�ƪBr[�s',1561411801,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2192,0,NULL,6,0,0,0),(107,1,'��)O���\0',1561411802,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2199,0,NULL,6,0,0,0),(108,1,'�ר{ �',1561411802,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2213,0,NULL,6,0,0,0),(109,1,'?;�X7ֺ',1561411802,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2223,0,NULL,6,0,0,0),(110,1,'y��`z�\n',1561411803,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2231,0,NULL,6,0,0,0),(111,1,'�+U��PLz',1561411803,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2237,0,NULL,6,0,0,0),(112,1,'�ӝ:\r�6�',1561411803,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2246,0,NULL,6,0,0,0),(113,1,'EH:]�g�',1561411804,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2253,0,NULL,6,0,0,0),(114,1,'\'�`��zPB',1561411804,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2258,0,NULL,6,0,0,0),(115,1,',���c�5�',1561411805,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2272,0,NULL,6,0,0,0),(116,1,'\n��0���',1561411806,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2289,0,NULL,6,0,0,0),(117,1,':{�hG���',1561411807,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2302,0,NULL,6,0,0,0),(118,1,'������;',1561411808,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2321,0,NULL,6,0,0,0),(119,1,'g����?�',1561411808,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2343,0,NULL,6,0,0,0),(120,1,'�tb(Lp�',1561411809,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2371,0,NULL,6,0,0,0),(121,1,'݇�W)�B#',1561411810,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2396,0,NULL,6,0,0,0),(122,1,'��\'��-��',1561411811,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2412,0,NULL,6,0,0,0),(123,1,'�ZG�ݠ�',1561411811,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2425,0,NULL,6,0,0,0),(124,1,'�9�ŵ��,',1561411811,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2435,0,NULL,6,0,0,0),(125,1,'�t&xf�',1561411812,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2442,0,NULL,6,0,0,0),(126,1,'�� �dP',1561411812,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2458,0,NULL,6,0,0,0),(127,1,'��MQ��E',1561411813,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2466,0,NULL,6,0,0,0),(128,1,'���I��O',1561411813,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2472,0,NULL,6,0,0,0),(129,1,'h@��H��8',1561411813,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2476,0,NULL,6,0,0,0),(130,1,'4��Ҕ}�',1561411813,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2483,0,NULL,6,0,0,0),(131,1,'ߢOm��',1561411814,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2489,0,NULL,6,0,0,0),(132,1,'��`B��v',1561411814,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2498,0,NULL,6,0,0,0),(133,1,'DY:K�]Z',1561411815,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2504,0,NULL,6,0,0,0),(134,1,'0@��\re�',1561411815,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2512,0,NULL,6,0,0,0),(135,1,'�y_���}',1561411816,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2533,0,NULL,6,0,0,0),(136,1,':�1w�x',1561411816,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2538,0,NULL,6,0,0,0),(137,1,'0p�G;�H�',1561411816,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2546,0,NULL,6,0,0,0),(138,1,'Ťv���R<',1561411817,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2550,0,NULL,6,0,0,0),(139,1,'3�dý���',1561411817,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2558,0,NULL,6,0,0,0),(140,1,'�4�b�k',1561411818,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2577,0,NULL,6,0,0,0),(141,1,'��j���R',1561411819,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2599,0,NULL,6,0,0,0),(142,1,'�x�L����',1561411819,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2612,0,NULL,6,0,0,0),(143,1,'jU��ҐD$',1561411819,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2622,0,NULL,6,0,0,0),(144,1,'�@IH��kC',1561411820,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2629,0,NULL,6,0,0,0),(145,1,'}K�V0�',1561411821,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2651,0,NULL,6,0,0,0),(146,1,'S)�7���e',1561411821,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2663,0,NULL,6,0,0,0),(147,1,'�\r�$�hF',1561411822,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2671,0,NULL,6,0,0,0),(148,1,'s�K�H�',1561411822,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2683,0,NULL,6,0,0,0),(149,1,'\\�G��`',1561411822,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2692,0,NULL,6,0,0,0),(150,1,'�0�]��ۧ',1561411822,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2700,0,NULL,6,0,0,0),(151,1,'e�o��9�#',1561411823,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2706,0,NULL,6,0,0,0),(152,1,'˸��1��',1561411823,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2713,0,NULL,6,0,0,0),(153,1,'�\\ÝAO',1561411823,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2719,0,NULL,6,0,0,0),(154,1,'|��|���S',1561411824,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2728,0,NULL,6,0,0,0),(155,1,'E	M�;�',1561411825,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2749,0,NULL,6,0,0,0),(156,1,'C�p\neQ',1561411826,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2768,0,NULL,6,0,0,0),(157,1,'�xw�ge[',1561411826,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2781,0,NULL,6,0,0,0),(158,1,'61_-�Ƀh',1561411826,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2792,0,NULL,6,0,0,0),(159,1,'�R�S���h',1561411827,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2801,0,NULL,6,0,0,0),(160,1,'��3��',1561411827,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2808,0,NULL,6,0,0,0),(161,1,'��`��d',1561411827,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2813,0,NULL,6,0,0,0),(162,1,'��j6۲�\'',1561411828,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2825,0,NULL,6,0,0,0),(163,1,'$T�,W{j',1561411828,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2835,0,NULL,6,0,0,0),(164,1,'�o��?��',1561411828,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2844,0,NULL,6,0,0,0),(165,1,'[�ݳ�{',1561411828,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2850,0,NULL,6,0,0,0),(166,1,'�����t',1561411829,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2854,0,NULL,6,0,0,0),(167,1,'9��<X���',1561411829,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2857,0,NULL,6,0,0,0),(168,1,'x�Ux9��#',1561411829,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2863,0,NULL,6,0,0,0),(169,1,'����C6',1561411830,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2868,0,NULL,6,0,0,0),(170,1,'7�����',1561411830,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2888,0,NULL,6,0,0,0),(171,1,'�<$��=߼',1561411831,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2896,0,NULL,6,0,0,0),(172,1,'�Ƚ��{��',1561411831,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2903,0,NULL,6,0,0,0),(173,1,'9�a3�',1561411831,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2908,0,NULL,6,0,0,0),(174,1,'ӻ:�	-',1561411831,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2912,0,NULL,6,0,0,0),(175,1,'���żI�',1561411832,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2917,0,NULL,6,0,0,0),(176,1,'Ļ����t�',1561411832,1,31,0,NULL,NULL,NULL,NULL,NULL,NULL,'body',2922,0,NULL,6,0,0,0),(177,1,'vM+Y��Ǧ',1561411832,1,31,0,NULL,NULL,1561411931,1,31,NULL,'body',2926,5,NULL,6,0,0,0),(178,1,'�������',1561411832,1,31,0,NULL,NULL,1561411853,1,31,NULL,'body',2929,1,NULL,6,0,0,0);
/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements_datasets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements_datasets` (
  `element_id` int(10) unsigned NOT NULL,
  `key` binary(4) NOT NULL,
  `value` text,
  `element_position_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`element_id`,`key`,`element_position_id`) USING BTREE,
  KEY `fk_elements_datasets_element_position_id` (`element_position_id`),
  CONSTRAINT `fk_elements_datasets_element_id` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements_datasets`
--

LOCK TABLES `elements_datasets` WRITE;
/*!40000 ALTER TABLE `elements_datasets` DISABLE KEYS */;
/*!40000 ALTER TABLE `elements_datasets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `elements_datasets_view`
--

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `elements_datasets_view` (
  `element_id` tinyint NOT NULL,
  `key` tinyint NOT NULL,
  `value` tinyint NOT NULL,
  `element_position_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `files`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `thumb_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_files_added_by` (`added_by`),
  KEY `fk_files_deleted_by` (`deleted_by`),
  KEY `fk_files_updated_by` (`updated_by`),
  KEY `fk_files_added_ip_id` (`added_ip_id`),
  KEY `fk_files_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_files_updated_ip_id` (`updated_ip_id`),
  KEY `fk_files_user_id` (`user_id`),
  KEY `fk_files_thumb_id` (`thumb_id`),
  CONSTRAINT `fk_files_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_thumb_id` FOREIGN KEY (`thumb_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files_downloads`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files_downloads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(8) DEFAULT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `file_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_files_downloads_added_by` (`added_by`),
  KEY `fk_files_downloads_deleted_by` (`deleted_by`),
  KEY `fk_files_downloads_updated_by` (`updated_by`),
  KEY `fk_files_downloads_added_ip_id` (`added_ip_id`),
  KEY `fk_files_downloads_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_files_downloads_updated_ip_id` (`updated_ip_id`),
  KEY `fk_files_downloads_file_id` (`file_id`),
  KEY `fk_files_downloads_user_id` (`user_id`),
  CONSTRAINT `fk_files_downloads_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_downloads_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_downloads_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_downloads_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_downloads_file_id` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_downloads_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_downloads_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_files_downloads_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files_downloads`
--

LOCK TABLES `files_downloads` WRITE;
/*!40000 ALTER TABLE `files_downloads` DISABLE KEYS */;
/*!40000 ALTER TABLE `files_downloads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `html`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `component_id` int(10) unsigned NOT NULL COMMENT 'Pole zawiera wskażnik do komponentu do tabeli components',
  `html_id` binary(4) DEFAULT NULL,
  `alt_id` binary(4) DEFAULT NULL,
  `html_class_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL COMMENT 'Pole zawiera datę ostatniej aktualizacji głównego wpisu do bazy. Takiego który nie zawiera rodzica.',
  `field` varchar(90) DEFAULT NULL COMMENT 'jest to kod pola z dataseta który trzyma treść lub nazwa pola z dataseta',
  `element_id` int(10) unsigned DEFAULT NULL,
  `show` tinyint(1) DEFAULT '1' COMMENT 'Jak jest true to element html jest wyświetlany na stronie',
  `data_set_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Jeżeli wartość jest true to znaczy że element można podpiąć pod selectora do dataSeta. Jego treść będzie rotowalna.',
  `background_active` tinyint(1) DEFAULT '0' COMMENT 'Jeżeli jest true to w edycji SimpleView można temu elementowi ustawić kolor tła lub obrazek',
  PRIMARY KEY (`id`),
  KEY `fk_html_parent_id` (`parent_id`),
  KEY `fk_html_component_id` (`component_id`),
  KEY `fk_html_html_class_id` (`html_class_id`),
  KEY `fk_html_element_id` (`element_id`),
  CONSTRAINT `fk_html_component_id` FOREIGN KEY (`component_id`) REFERENCES `components` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_html_element_id` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_html_html_class_id` FOREIGN KEY (`html_class_id`) REFERENCES `html_classes` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_html_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `html` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2931 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `html`
--

LOCK TABLES `html` WRITE;
/*!40000 ALTER TABLE `html` DISABLE KEYS */;
INSERT INTO `html` VALUES (1,NULL,1,'lp��','VQ',NULL,1561411717,'9afc8f83',1,1,0,0),(2,1,1,'�t�','�\n�',NULL,NULL,'32bbdb0d',NULL,1,0,0),(3,2,1,'\'A�w','���2',NULL,NULL,'38022759',NULL,1,0,0),(4,3,1,'�ak�','\n��	',NULL,NULL,'c8f12841',NULL,1,0,0),(5,4,1,'z���','\\��_',NULL,NULL,'f4b4133a',NULL,1,0,0),(6,5,1,'j��k','�a&I',NULL,NULL,'4fa87353',NULL,1,0,0),(7,6,1,'���&','����',NULL,NULL,'c5aa92a2',NULL,1,0,0),(8,7,1,'A�|<','��32',NULL,NULL,'11b580bd',NULL,1,0,0),(9,8,1,'I:6�','X��',NULL,NULL,'c8340369',NULL,1,0,0),(10,9,1,')kY','*y7',NULL,NULL,'19f1fcc7',NULL,1,0,0),(11,10,8,'eg@L','vm@',NULL,NULL,'58f902b9',NULL,1,0,0),(12,NULL,1,'T�q','<�m',NULL,1561411717,'13260042',2,1,0,0),(13,12,1,'�W�R','a��p',NULL,NULL,'f2f26131',NULL,1,0,0),(14,13,1,'Yu<','8�g�',NULL,NULL,'43a79a74',NULL,1,0,0),(15,14,1,'�','��)2',NULL,NULL,'38c634b0',NULL,1,0,0),(16,15,1,'v?�/','��ŉ',NULL,NULL,'81af4783',NULL,1,0,0),(17,16,1,'l�1','����',NULL,NULL,'bda99f64',NULL,1,0,0),(18,17,1,'4��v','���(',NULL,NULL,'ac9d2cfa',NULL,1,0,0),(19,18,1,'Q�ux','WHu',NULL,NULL,'50a828bd',NULL,1,0,0),(20,19,1,'���','=\0�\"',NULL,NULL,'5452ddd6',NULL,1,0,0),(21,20,8,'�&�_','ۄi',NULL,NULL,'37d93802',NULL,1,0,0),(22,NULL,1,'�]x�','��݈',NULL,1561411718,'23c9c5d1',3,1,0,0),(23,22,1,'lY�','�YH�',NULL,NULL,'940acffc',NULL,1,0,0),(24,23,1,'��Ŀ','Oy��',NULL,NULL,'a75b8c67',NULL,1,0,0),(25,24,1,',O�','��g',NULL,NULL,'1c300474',NULL,1,0,0),(26,25,1,'��l�','�ĵ',NULL,NULL,'1ffaf866',NULL,1,0,0),(27,26,1,'��\0�','���',NULL,NULL,'b115f026',NULL,1,0,0),(28,27,1,'��=','m�}�',NULL,NULL,'c2cdfc62',NULL,1,0,0),(29,28,1,'�aH','�\'�',NULL,NULL,'bb5d785d',NULL,1,0,0),(30,29,8,'T�','IK�',NULL,NULL,'8b972197',NULL,1,0,0),(31,28,1,'�0\0�','�\'��',NULL,NULL,'0f0f9468',NULL,1,0,0),(32,31,1,'b+�	','����',NULL,NULL,'4da1f6df',NULL,1,0,0),(33,32,1,'�P�U','�o�',NULL,NULL,'fdffb478',NULL,1,0,0),(34,33,8,'�:֛','{�Z�',NULL,NULL,'37964faa',NULL,1,0,0),(35,NULL,1,'���f','ZVYx',NULL,1561411718,'23636b02',4,1,0,0),(36,35,1,'9�','a��l',NULL,NULL,'c46c940a',NULL,1,0,0),(37,36,1,'״O','MTv',NULL,NULL,'fba3d686',NULL,1,0,0),(38,37,1,'�6��','�$%�',NULL,NULL,'c6a5db21',NULL,1,0,0),(39,38,1,'�S0�','\n�	H',NULL,NULL,'aa515463',NULL,1,0,0),(40,39,1,'��','�u�g',NULL,NULL,'d43f1ac2',NULL,1,0,0),(41,40,1,'v�l�','H�',NULL,NULL,'c5063631',NULL,1,0,0),(42,41,1,'Acz','�\'f',NULL,NULL,'617b53b2',NULL,1,0,0),(43,42,8,'�2','��',NULL,NULL,'d9502b9b',NULL,1,0,0),(44,39,1,'�U��','�y',NULL,NULL,'3b697553',NULL,1,0,0),(45,44,1,'C�W','\\ZR',NULL,NULL,'4c1f44aa',NULL,1,0,0),(46,45,1,'��dI','��M�',NULL,NULL,'da43305b',NULL,1,0,0),(47,46,8,'t�T8','z&E',NULL,NULL,'ba764c2f',NULL,1,0,0),(48,39,1,'E�<�','�QAQ',NULL,NULL,'76fafd26',NULL,1,0,0),(49,48,1,'�HS','1\Z`�',NULL,NULL,'10133090',NULL,1,0,0),(50,49,1,'�Kp','a2�u',NULL,NULL,'fc004409',NULL,1,0,0),(51,50,8,'��h�','TMp�',NULL,NULL,'04116b33',NULL,1,0,0),(52,39,1,'��m','}=Ц',NULL,NULL,'5967865d',NULL,1,0,0),(53,52,1,'\'=��','z[\0h',NULL,NULL,'79905f89',NULL,1,0,0),(54,53,1,'���','XT@!',NULL,NULL,'ad62c55b',NULL,1,0,0),(55,54,8,'7j0','9�\'E',NULL,NULL,'942bfd85',NULL,1,0,0),(56,39,1,'�','%��5',NULL,NULL,'8004f680',NULL,1,0,0),(57,56,1,'����','![x',NULL,NULL,'50c951d7',NULL,1,0,0),(58,57,1,'OS�\n','K�',NULL,NULL,'cd224518',NULL,1,0,0),(59,58,8,'/K��','�uW',NULL,NULL,'22ad8014',NULL,1,0,0),(60,39,1,'����','g�p�',NULL,NULL,'b352d591',NULL,1,0,0),(61,60,1,'�Z�s','̈́\\',NULL,NULL,'aaf88742',NULL,1,0,0),(62,61,1,'��',')���',NULL,NULL,'d0dd36c8',NULL,1,0,0),(63,62,8,'(lUP','Ic$�',NULL,NULL,'218bf1ff',NULL,1,0,0),(64,NULL,1,'�Y&�','�T?o',NULL,1561411719,'2517bd8f',5,1,0,0),(65,64,1,'��S�','�C%M',NULL,NULL,'ca01f8b0',NULL,1,0,0),(66,65,1,'߸my','\Z!��',NULL,NULL,'588980f5',NULL,1,0,0),(67,66,1,'ۿ�p','�ѫ�',NULL,NULL,'2a777bfd',NULL,1,0,0),(68,67,1,'�ƣ�','��ו',NULL,NULL,'f3977db2',NULL,1,0,0),(69,68,1,'{A�*','r��',NULL,NULL,'149c1577',NULL,1,0,0),(70,69,1,'�Ѭ','Jes`',NULL,NULL,'80b705a1',NULL,1,0,0),(71,70,1,'QLYc','����',NULL,NULL,'b909d665',NULL,1,0,0),(72,71,8,'W��','gs%',NULL,NULL,'2cc7b36f',NULL,1,0,0),(73,NULL,1,'ҁ�r','�Sk2',NULL,1561411720,'c517b5af',6,1,0,0),(74,73,1,'�ع�','Q3',NULL,NULL,'a01adf6f',NULL,1,0,0),(75,74,1,'��Y','�d�',NULL,NULL,'7b91b5cd',NULL,1,0,0),(76,75,1,'4��','�&�6',NULL,NULL,'95cd1f96',NULL,1,0,0),(77,76,1,'jɡ5','�z2�',NULL,NULL,'34c38036',NULL,1,0,0),(78,77,1,'�ɄR','��e�',NULL,NULL,'8b717d94',NULL,1,0,0),(79,78,1,'t��',' {��',NULL,NULL,'d1a581b9',NULL,1,0,0),(80,79,8,'�D�','{xk',NULL,NULL,'85d13cb7',NULL,1,0,0),(81,78,1,'A�p�','fї8',NULL,NULL,'9a516d67',NULL,1,0,0),(82,81,1,'���','�[��',NULL,NULL,'ff6f0414',NULL,1,0,0),(83,82,1,'����','��7\\',NULL,NULL,'4ccf1fb5',NULL,1,0,0),(84,83,8,'$��','���',NULL,NULL,'f37d1223',NULL,1,0,0),(85,NULL,1,'yC�l','1;\\�',NULL,1561411720,'5208c48f',7,1,0,0),(86,85,1,'*�Vy','r�c',NULL,NULL,'b095995f',NULL,1,0,0),(87,86,1,'�Q','�gL�',NULL,NULL,'b02f224d',NULL,1,0,0),(88,87,1,'8�{�','��WB',NULL,NULL,'61d6f938',NULL,1,0,0),(89,88,1,'ϕ0T','�8l',NULL,NULL,'b5fb897b',NULL,1,0,0),(90,89,1,'�y','�wm',NULL,NULL,'9f5863a3',NULL,1,0,0),(91,90,1,'�\nv','Ya�x',NULL,NULL,'8a19b175',NULL,1,0,0),(92,91,8,'Ŀ-�','m��',NULL,NULL,'797b5ccd',NULL,1,0,0),(93,90,1,'Q�v','*̀',NULL,NULL,'176f9a9a',NULL,1,0,0),(94,93,8,'uǺ','f2\"�',NULL,NULL,'91480761',NULL,1,0,0),(95,87,1,'U�q�','Ր�',NULL,NULL,'b9cf428b',NULL,1,0,0),(96,95,1,'�uw','@�',NULL,NULL,'200f28ad',NULL,1,0,0),(97,96,8,'��/�','/���',NULL,NULL,'887a095c',NULL,1,0,0),(98,NULL,1,'fr=�','�m{',NULL,1561411721,'892dffba',8,1,0,0),(99,98,1,'{�t','/k',NULL,NULL,'d5f1fa09',NULL,1,0,0),(100,99,1,'�3�','*�h',NULL,NULL,'cb11522f',NULL,1,0,0),(101,100,1,'��]','7���',NULL,NULL,'a5d43c03',NULL,1,0,0),(102,101,1,'_x','A�\\�',NULL,NULL,'23c4abba',NULL,1,0,0),(103,102,1,'���','D:��',NULL,NULL,'5780dc91',NULL,1,0,0),(104,103,1,'\nP�','��$',NULL,NULL,'f5285d5d',NULL,1,0,0),(105,104,8,'`U','�A��',NULL,NULL,'0f164571',NULL,1,0,0),(106,102,1,'���F','�&�',NULL,NULL,'0276a82f',NULL,1,0,0),(107,106,1,'��o�','�%\\�',NULL,NULL,'dc1f0575',NULL,1,0,0),(108,107,8,'Ǡ�','��<',NULL,NULL,'2d36d980',NULL,1,0,0),(109,102,1,'�l�l','w��c',NULL,NULL,'a0c877bf',NULL,1,0,0),(110,109,1,':\0�','�ݠY',NULL,NULL,'43965a08',NULL,1,0,0),(111,110,8,'�V�','o��',NULL,NULL,'b361fdfa',NULL,1,0,0),(112,102,1,'���','���[',NULL,NULL,'5fb308c7',NULL,1,0,0),(113,112,1,'���','|R�;',NULL,NULL,'d634a0f1',NULL,1,0,0),(114,113,8,'4�','��',NULL,NULL,'2047a853',NULL,1,0,0),(115,102,1,'=�G\Z','����',NULL,NULL,'05d489c2',NULL,1,0,0),(116,115,1,'W�6',',tfm',NULL,NULL,'2ab3a245',NULL,1,0,0),(117,116,8,'F�h','i�Y�',NULL,NULL,'04883352',NULL,1,0,0),(118,102,1,'��E','�ϗW',NULL,NULL,'9b507cc1',NULL,1,0,0),(119,118,1,'�ܑH','ۊ�',NULL,NULL,'06142a5c',NULL,1,0,0),(120,119,8,'��0�','�[`',NULL,NULL,'3b1b3d06',NULL,1,0,0),(121,NULL,1,'�{��','q�e',NULL,1561411722,'7a203a96',9,1,0,0),(122,121,1,'J*�V','�0wQ',NULL,NULL,'ff878713',NULL,1,0,0),(123,122,1,'� M;','o�',NULL,NULL,'71065660',NULL,1,0,0),(124,123,1,';=\'w','�*�',NULL,NULL,'f38bb92d',NULL,1,0,0),(125,124,1,'A-\Z�','�P]/',NULL,NULL,'06b164c9',NULL,1,0,0),(126,125,1,'*#��','����',NULL,NULL,'436035a3',NULL,1,0,0),(127,126,1,'���',':�w�',NULL,NULL,'092d19d8',NULL,1,0,0),(128,127,8,'O�z�','|���',NULL,NULL,'2484b938',NULL,1,0,0),(129,124,1,'T\n�A','V���',NULL,NULL,'b470244c',NULL,1,0,0),(130,129,1,'���-','O�\'�',NULL,NULL,'f85f5dd8',NULL,1,0,0),(131,130,1,'i+E�','��P�',NULL,NULL,'d02ab4d2',NULL,1,0,0),(132,131,8,'g%�','c�D�',NULL,NULL,'84c8567f',NULL,1,0,0),(133,124,1,'�l��','�Cm\0',NULL,NULL,'2726df7b',NULL,1,0,0),(134,133,1,'6�t&','�m',NULL,NULL,'04a0803f',NULL,1,0,0),(135,134,1,'��ɉ','t\"�',NULL,NULL,'1dff90b2',NULL,1,0,0),(136,135,8,'M��',':�\'�',NULL,NULL,'44755250',NULL,1,0,0),(137,124,1,'�@�B','}�i�',NULL,NULL,'74a1cf03',NULL,1,0,0),(138,137,1,'�\rX','֍�',NULL,NULL,'6544d109',NULL,1,0,0),(139,138,1,'���\n','�g�i',NULL,NULL,'77bd6792',NULL,1,0,0),(140,139,8,'͠�Y','U\r��',NULL,NULL,'d6460026',NULL,1,0,0),(141,124,1,'@7��','Xr�',NULL,NULL,'5fc06d16',NULL,1,0,0),(142,141,1,'*��','�Ī',NULL,NULL,'78f0733a',NULL,1,0,0),(143,142,1,'��a','D�3(',NULL,NULL,'a03b52c4',NULL,1,0,0),(144,143,8,'��G','J�x',NULL,NULL,'c54d2088',NULL,1,0,0),(145,124,1,'c���','�_�',NULL,NULL,'6c56d663',NULL,1,0,0),(146,145,1,'�]','YH',NULL,NULL,'60c4fda1',NULL,1,0,0),(147,146,1,'��','�',NULL,NULL,'a7f7273c',NULL,1,0,0),(148,147,8,'+Sx�','���7',NULL,NULL,'72f26336',NULL,1,0,0),(149,NULL,1,'��86','�F|�',NULL,1561411723,'639a19c3',10,1,0,0),(150,149,1,'�T�','�Y�',NULL,NULL,'d6941911',NULL,1,0,0),(151,150,1,'#2�Q','e���',NULL,NULL,'c3011683',NULL,1,0,0),(152,151,1,'�G�S','2�w�',NULL,NULL,'575f8f90',NULL,1,0,0),(153,152,1,'��J�','����',NULL,NULL,'1a8d963d',NULL,1,0,0),(154,153,1,'�K,','�2��',NULL,NULL,'01bc8835',NULL,1,0,0),(155,154,1,'��h�','�j��',NULL,NULL,'a554bdcb',NULL,1,0,0),(156,155,8,'��[2','?��&',NULL,NULL,'71d2bb63',NULL,1,0,0),(157,151,1,'�f�','��t',NULL,NULL,'15cbfa2d',NULL,1,0,0),(158,157,1,'i��0','RƬ�',NULL,NULL,'70bb424f',NULL,1,0,0),(159,158,1,'��t','9�W�',NULL,NULL,'98346157',NULL,1,0,0),(160,159,8,'\'��','õ��',NULL,NULL,'099db9f7',NULL,1,0,0),(161,NULL,1,'	��','u��',NULL,1561411723,'5dc70a7b',11,1,0,0),(162,161,1,'VvU�','UET\'',NULL,NULL,'d4b6d698',NULL,1,0,0),(163,162,1,')S','@��',NULL,NULL,'b2f1179c',NULL,1,0,0),(164,163,1,'[�/','VS��',NULL,NULL,'8960c9b9',NULL,1,0,0),(165,164,1,'� �Y','��',NULL,NULL,'8f844866',NULL,1,0,0),(166,165,1,'/:�','F�s',NULL,NULL,'7213ac46',NULL,1,0,0),(167,166,1,'���e','��',NULL,NULL,'4c7d1f9f',NULL,1,0,0),(168,167,8,'Y�\\�','(yDp',NULL,NULL,'bc078354',NULL,1,0,0),(169,162,1,'�g�J','��X',NULL,NULL,'43dc0ac7',NULL,1,0,0),(170,169,1,'�Dh','?=�',NULL,NULL,'c543a19a',NULL,1,0,0),(171,170,1,'�V��','H*[',NULL,NULL,'9df04f91',NULL,1,0,0),(172,171,8,'�\r&\r','�!�',NULL,NULL,'168faf87',NULL,1,0,0),(173,NULL,1,'\0��A','[�k',NULL,1561411724,'c0600c56',12,1,0,0),(174,173,1,',��p','��H�',NULL,NULL,'3483a19c',NULL,1,0,0),(175,174,1,'�Y\r�','U��',NULL,NULL,'42dffc42',NULL,1,0,0),(176,175,1,'*f߰','Ya�5',NULL,NULL,'89856042',NULL,1,0,0),(177,176,1,'ET��','��Ԑ',NULL,NULL,'4fd08925',NULL,1,0,0),(178,177,1,'���l','Ra��',NULL,NULL,'d84f633c',NULL,1,0,0),(179,178,1,'�Y�','C�I4',NULL,NULL,'529597a3',NULL,1,0,0),(180,179,8,'f!�i','�R\'�',NULL,NULL,'004c69a4',NULL,1,0,0),(181,NULL,1,'���','���Z',NULL,1561411724,'9b89fa64',13,1,0,0),(182,181,1,'�hX�','�gԝ',NULL,NULL,'7f959f7a',NULL,1,0,0),(183,182,1,'4�:�','�e�',NULL,NULL,'df9567f4',NULL,1,0,0),(184,183,1,'�$b�','��',NULL,NULL,'3581bc8d',NULL,1,0,0),(185,184,1,'JD�/','����',NULL,NULL,'12fcd78d',NULL,1,0,0),(186,185,1,'��[','�y#',NULL,NULL,'71891955',NULL,1,0,0),(187,186,8,'$W�','���T',NULL,NULL,'cdb4a9d6',NULL,1,0,0),(188,186,1,'���','s��s',NULL,NULL,'76ddcb4f',NULL,1,0,0),(189,188,8,'�x��','w��\0',NULL,NULL,'dc0aa44c',NULL,1,0,0),(190,183,1,'\ZO-�','6W\rI',NULL,NULL,'30547ca1',NULL,1,0,0),(191,190,1,'S�\"','��ʚ',NULL,NULL,'288b4373',NULL,1,0,0),(192,191,1,'O�s�','�xxC',NULL,NULL,'ab2d77b7',NULL,1,0,0),(193,192,1,'\Zc�(','�MQb',NULL,NULL,'c8c82619',NULL,1,0,0),(194,193,8,'�:\Z','G���',NULL,NULL,'41c695b6',NULL,1,0,0),(195,193,1,'{=�\n','��',NULL,NULL,'c6a9a00f',NULL,1,0,0),(196,195,8,'��\"�','B4=',NULL,NULL,'f83c27db',NULL,1,0,0),(197,192,1,'�b��','P�0',NULL,NULL,'f386b565',NULL,1,0,0),(198,197,8,'w���','2BK',NULL,NULL,'2ffb8009',NULL,1,0,0),(199,197,1,'�R��','Z5=t',NULL,NULL,'cbd47d1d',NULL,1,0,0),(200,199,8,'#iSM','���',NULL,NULL,'95956b90',NULL,1,0,0),(201,192,1,'{�@�','\r�0',NULL,NULL,'18758a61',NULL,1,0,0),(202,201,8,'��1`','Q=�?',NULL,NULL,'df6084b8',NULL,1,0,0),(203,201,1,'S�,','�(x9',NULL,NULL,'8244672c',NULL,1,0,0),(204,203,8,'��<�','S�*',NULL,NULL,'1a988618',NULL,1,0,0),(205,192,1,'\"�V','��',NULL,NULL,'bc8d2ad0',NULL,1,0,0),(206,205,8,'��','D�HJ',NULL,NULL,'6c99f37c',NULL,1,0,0),(207,205,1,'W-rL','`��',NULL,NULL,'008f8524',NULL,1,0,0),(208,207,8,'	�','�k<�',NULL,NULL,'6bd02ad2',NULL,1,0,0),(209,192,1,'�D��','\rb',NULL,NULL,'222f6948',NULL,1,0,0),(210,209,8,'@�C�','�̍',NULL,NULL,'00baab6b',NULL,1,0,0),(211,209,1,'}�ȳ','�]]',NULL,NULL,'5ffa6273',NULL,1,0,0),(212,211,8,'7=u','�!��',NULL,NULL,'d342f90f',NULL,1,0,0),(213,183,1,'AK�','a7�',NULL,NULL,'b64c2d4b',NULL,1,0,0),(214,213,1,'�P��','����',NULL,NULL,'f31d0cd8',NULL,1,0,0),(215,214,1,'��\Zz','�aê',NULL,NULL,'19227f23',NULL,1,0,0),(216,215,8,'k3�','���i',NULL,NULL,'aaa23a7f',NULL,1,0,0),(217,183,1,'їY�','M�A',NULL,NULL,'d82a5c0f',NULL,1,0,0),(218,217,8,'��E\n','�v��',NULL,NULL,'3f453466',NULL,1,0,0),(219,NULL,1,'��+m','f�ϧ',NULL,1561411725,'71825cdb',14,1,0,0),(220,219,1,'h�K�','G�\r�',NULL,NULL,'2f225133',NULL,1,0,0),(221,220,1,'q\Z-','��,�',NULL,NULL,'89f88074',NULL,1,0,0),(222,221,1,'�8W:','k�',NULL,NULL,'5aa90379',NULL,1,0,0),(223,222,1,'Lw��','�3�p',NULL,NULL,'dd3f1a3f',NULL,1,0,0),(224,223,1,'D?:�','&ʜ',NULL,NULL,'15770b53',NULL,1,0,0),(225,224,8,'�}0�','�ߢ�',NULL,NULL,'045149cd',NULL,1,0,0),(226,223,1,'p��','!$l',NULL,NULL,'b29cffad',NULL,1,0,0),(227,226,1,'[\0$)','�r�',NULL,NULL,'908a6378',NULL,1,0,0),(228,227,1,')5,\Z','m��z',NULL,NULL,'ab641700',NULL,1,0,0),(229,228,8,'�x�','+c�h',NULL,NULL,'a2811608',NULL,1,0,0),(230,NULL,1,'���','����',NULL,1561411726,'46551333',15,1,0,0),(231,230,1,'%\n�','x�0�',NULL,NULL,'bc6b95f3',NULL,1,0,0),(232,231,1,'`��z','�C�',NULL,NULL,'665d1c4c',NULL,1,0,0),(233,232,1,'�{��','&P��',NULL,NULL,'3a4219af',NULL,1,0,0),(234,233,1,'E`1�','`��',NULL,NULL,'1bc4923a',NULL,1,0,0),(235,234,1,'3�<�','0���',NULL,NULL,'38332d90',NULL,1,0,0),(236,235,8,'�$(','c�ͅ',NULL,NULL,'7ac27915',NULL,1,0,0),(237,234,1,'�G˦','F$��',NULL,NULL,'4d9d2025',NULL,1,0,0),(238,237,8,'7l(�','�_�',NULL,NULL,'a5772224',NULL,1,0,0),(239,234,1,'l:@','�1�O',NULL,NULL,'2b3bcdb4',NULL,1,0,0),(240,239,8,'(�,�','w���',NULL,NULL,'f2028396',NULL,1,0,0),(241,234,1,'���&','4ߺ',NULL,NULL,'1404abdd',NULL,1,0,0),(242,241,8,'\Z4�','S%D�',NULL,NULL,'0976769f',NULL,1,0,0),(243,234,1,'���','HO',NULL,NULL,'93740fa2',NULL,1,0,0),(244,243,8,'/��','�!D',NULL,NULL,'a75ca42c',NULL,1,0,0),(245,234,1,'�i�M','�K�',NULL,NULL,'a98f4855',NULL,1,0,0),(246,245,8,'5�','B�,;',NULL,NULL,'b646a876',NULL,1,0,0),(247,232,1,'��o�','lU�',NULL,NULL,'5aa40aa5',NULL,1,0,0),(248,247,1,'[�6','��B�',NULL,NULL,'15608676',NULL,1,0,0),(249,248,1,'\'Y(�','����',NULL,NULL,'00c731d1',NULL,1,0,0),(250,249,1,'��<V','���',NULL,NULL,'9718b9a1',NULL,1,0,0),(251,250,8,'F*��','2��d',NULL,NULL,'8f234283',NULL,1,0,0),(252,249,1,'�$�','\\c��',NULL,NULL,'df614088',NULL,1,0,0),(253,252,8,' IH','q���',NULL,NULL,'3af0cfdf',NULL,1,0,0),(254,247,1,'�Y@�','�E�',NULL,NULL,'3f12b373',NULL,1,0,0),(255,254,1,'duú','_6�7',NULL,NULL,'7b474996',NULL,1,0,0),(256,255,8,'ژT','���C',NULL,NULL,'7b43302d',NULL,1,0,0),(257,NULL,1,'ڸ�s','��6',NULL,1561411727,'413c2d51',16,1,0,0),(258,257,1,'\0C��','�\n�',NULL,NULL,'b71a1f6c',NULL,1,0,0),(259,258,1,'�|�$','ßL1',NULL,NULL,'9fb90239',NULL,1,0,0),(260,259,1,'����','I�`',NULL,NULL,'9a3af263',NULL,1,0,0),(261,260,1,'f���','	��',NULL,NULL,'d3939636',NULL,1,0,0),(262,261,1,'PȂ�','�G',NULL,NULL,'6a706c61',NULL,1,0,0),(263,262,8,'����','c���',NULL,NULL,'6f17d733',NULL,1,0,0),(264,261,1,'qě','�4',NULL,NULL,'209a3b69',NULL,1,0,0),(265,264,8,'��','�Bq',NULL,NULL,'75f7b62d',NULL,1,0,0),(266,258,1,'�%l','6�',NULL,NULL,'f081d2ab',NULL,1,0,0),(267,266,1,'{ܿ','ogI�',NULL,NULL,'88cfb42a',NULL,1,0,0),(268,267,8,'�7�	','�(��',NULL,NULL,'a4667142',NULL,1,0,0),(269,NULL,1,'��K','�2m�',NULL,1561411727,'83341021',17,1,0,0),(270,269,1,'�','eA�',NULL,NULL,'715dcb79',NULL,1,0,0),(271,270,1,'\n]','���',NULL,NULL,'3a73d5a9',NULL,1,0,0),(272,271,1,'jb�','�/,W',NULL,NULL,'b84ab19b',NULL,1,0,0),(273,272,1,'�u�','���',NULL,NULL,'f0899d89',NULL,1,0,0),(274,273,1,'��7�','3��(',NULL,NULL,'4ba2844d',NULL,1,0,0),(275,274,8,'��V�','M�',NULL,NULL,'40522848',NULL,1,0,0),(276,273,1,'�?\"�','�W�L',NULL,NULL,'f9ac9f60',NULL,1,0,0),(277,276,8,'�?ɔ','QVB�',NULL,NULL,'ff44a5d2',NULL,1,0,0),(278,269,1,'u/','8�:�',NULL,NULL,'506cc431',NULL,1,0,0),(279,278,1,'���','�oP',NULL,NULL,'6f6af015',NULL,1,0,0),(280,279,1,'�ucq','jн',NULL,NULL,'924214a9',NULL,1,0,0),(281,280,1,'�Ƹ�','��O�',NULL,NULL,'7d216a54',NULL,1,0,0),(282,281,1,'3�t@','��#�',NULL,NULL,'56c23bb7',NULL,1,0,0),(283,282,8,'Va2','pD',NULL,NULL,'37797143',NULL,1,0,0),(284,281,1,' �=J','̷Bq',NULL,NULL,'4dd02dfc',NULL,1,0,0),(285,284,8,'��˺','��',NULL,NULL,'30f098c5',NULL,1,0,0),(286,NULL,1,'�v!�','����',NULL,1561411728,'1b9a073d',18,1,0,0),(287,286,1,'wIq�','[[4�',NULL,NULL,'79f5954a',NULL,1,0,0),(288,287,1,'�Ͷ','a�`�',NULL,NULL,'062a1167',NULL,1,0,0),(289,288,1,'��h','��',NULL,NULL,'b343af2b',NULL,1,0,0),(290,289,1,'R�s','�X�',NULL,NULL,'323033cd',NULL,1,0,0),(291,290,1,'�5��','��_s',NULL,NULL,'7b1cc6d0',NULL,1,0,0),(292,291,8,'V6;P','P\'',NULL,NULL,'727f63f7',NULL,1,0,0),(293,290,1,'��l�','�#]�',NULL,NULL,'df091dc3',NULL,1,0,0),(294,293,8,'?�g','	s?',NULL,NULL,'74db6b5a',NULL,1,0,0),(295,NULL,1,'�{D','f)3�',NULL,1561411728,'d0d40c90',19,1,0,0),(296,295,1,'J5�',' 2�A',NULL,NULL,'6f93a1bb',NULL,1,0,0),(297,296,1,'���','P',NULL,NULL,'7ccf4786',NULL,1,0,0),(298,297,1,'H�S�','Y�3�',NULL,NULL,'910bdf91',NULL,1,0,0),(299,298,1,'����','j��s',NULL,NULL,'50a3bc4c',NULL,1,0,0),(300,299,1,'L� �','R�\0�',NULL,NULL,'7a37b85c',NULL,1,0,0),(301,300,8,',z�','���',NULL,NULL,'6f3dbf94',NULL,1,0,0),(302,299,8,'��o','�˅�',NULL,NULL,'b87c8c13',NULL,1,0,0),(303,298,1,'�Yv','�{uZ',NULL,NULL,'b1248a7b',NULL,1,0,0),(304,303,8,'�߅','͋�',NULL,NULL,'962a7a49',NULL,1,0,0),(305,NULL,1,'�oQ','��vO',NULL,1561411729,'7fb0bdf5',20,1,0,0),(306,305,1,'b=�','k7[B',NULL,NULL,'5b21da20',NULL,1,0,0),(307,306,1,'��Z\Z','l`�\0',NULL,NULL,'8c414405',NULL,1,0,0),(308,307,1,'�8ף','t���',NULL,NULL,'9b02a613',NULL,1,0,0),(309,308,1,'uf�','�%',NULL,NULL,'9d3dcfa7',NULL,1,0,0),(310,309,1,'RV;�','��=',NULL,NULL,'7057b3d1',NULL,1,0,0),(311,310,8,'ܷ�u','y�#U',NULL,NULL,'493bdbb5',NULL,1,0,0),(312,309,8,'Bu�','űJ�',NULL,NULL,'dccf0186',NULL,1,0,0),(313,NULL,1,'�we�','�3��',NULL,1561411729,'d55f8bad',21,1,0,0),(314,313,1,'E�8h','�yt�',NULL,NULL,'f5fb1cdf',NULL,1,0,0),(315,314,1,'�C$','���)',NULL,NULL,'27382961',NULL,1,0,0),(316,315,1,'�8��','b�i',NULL,NULL,'5230392c',NULL,1,0,0),(317,316,1,'����','��e(',NULL,NULL,'a43b36d1',NULL,1,0,0),(318,317,1,'\r���','#��',NULL,NULL,'f28c72b1',NULL,1,0,0),(319,318,8,'�m/ ','��Տ',NULL,NULL,'f2ad9a72',NULL,1,0,0),(320,316,1,'�dIS','���',NULL,NULL,'a660a834',NULL,1,0,0),(321,320,1,'�ĥ','rY2h',NULL,NULL,'b2f8886c',NULL,1,0,0),(322,321,8,'G�Z:','��K',NULL,NULL,'750a0b15',NULL,1,0,0),(323,316,1,'�,��','Z�;F',NULL,NULL,'d46201d9',NULL,1,0,0),(324,323,1,'{x�','�/)�',NULL,NULL,'0c8bc500',NULL,1,0,0),(325,324,8,'Y!E$','��=\"',NULL,NULL,'a8a65442',NULL,1,0,0),(326,316,1,'s��)','s���',NULL,NULL,'c20cabbd',NULL,1,0,0),(327,326,1,'V#�','#�!',NULL,NULL,'fbfd924d',NULL,1,0,0),(328,327,8,'�B','\0���',NULL,NULL,'21aa121d',NULL,1,0,0),(329,316,1,'�0)','b4P',NULL,NULL,'a9acd92c',NULL,1,0,0),(330,329,1,'�E-','�	',NULL,NULL,'a5918723',NULL,1,0,0),(331,330,8,'�EK','�k&k',NULL,NULL,'5af81ff3',NULL,1,0,0),(332,316,1,'����','�8��',NULL,NULL,'b76b9f03',NULL,1,0,0),(333,332,1,'���2','�#�',NULL,NULL,'8cf81b92',NULL,1,0,0),(334,333,8,'/\"�','�X��',NULL,NULL,'08cb087b',NULL,1,0,0),(335,NULL,1,'qi��','DI	�',NULL,1561411730,'6f0b28f0',22,1,0,0),(336,335,1,'�x�',':;��',NULL,NULL,'fd8b44f0',NULL,1,0,0),(337,336,1,'��ɣ','���\\',NULL,NULL,'b0a87bbb',NULL,1,0,0),(338,337,1,'�@�','�bL',NULL,NULL,'afff6f2b',NULL,1,0,0),(339,338,1,')�V','��ڷ',NULL,NULL,'c348d3b8',NULL,1,0,0),(340,339,1,'�d','\rCL�',NULL,NULL,'dddc638a',NULL,1,0,0),(341,340,8,'�fsA','=YÂ',NULL,NULL,'bf38dd2f',NULL,1,0,0),(342,338,1,'�f�','��j�',NULL,NULL,'6d95dcd0',NULL,1,0,0),(343,342,1,'�`','t�ps',NULL,NULL,'ccc5db95',NULL,1,0,0),(344,343,8,'�ƣ�','�\\�m',NULL,NULL,'082db5ab',NULL,1,0,0),(345,NULL,1,'*�',')o\r',NULL,1561411730,'6b6d66af',23,1,0,0),(346,345,1,'h��\0',']Po',NULL,NULL,'27f0b345',NULL,1,0,0),(347,346,1,'����','b�$�',NULL,NULL,'3cb3c795',NULL,1,0,0),(348,347,1,'<�\Zi','�\Z�',NULL,NULL,'8659c294',NULL,1,0,0),(349,348,1,'�ŝ','s��',NULL,NULL,'f9c6aaba',NULL,1,0,0),(350,349,1,'�uQ�','�j�',NULL,NULL,'3dc5756b',NULL,1,0,0),(351,350,8,'�U<�','�C+',NULL,NULL,'4a3197b0',NULL,1,0,0),(352,347,1,'�:8`','1���',NULL,NULL,'6f6666b4',NULL,1,0,0),(353,352,1,'ZJ��','�lz\"',NULL,NULL,'c5f04861',NULL,1,0,0),(354,353,1,')�m�','�TV�',NULL,NULL,'c8f830d8',NULL,1,0,0),(355,354,8,'�ѿ:','���',NULL,NULL,'b7c74a63',NULL,1,0,0),(356,347,1,'�S�','XG� ',NULL,NULL,'7ba52a60',NULL,1,0,0),(357,356,1,'	\n�','�\\d%',NULL,NULL,'39c024d8',NULL,1,0,0),(358,357,1,'#��','�q�',NULL,NULL,'46462c16',NULL,1,0,0),(359,358,8,'�F�)','m�a',NULL,NULL,'0a89c462',NULL,1,0,0),(360,347,1,'�}tQ','\0�	',NULL,NULL,'5d63d3a0',NULL,1,0,0),(361,360,1,'�w��','��',NULL,NULL,'fcfbb1a6',NULL,1,0,0),(362,361,1,'���','ҵFc',NULL,NULL,'c006faa7',NULL,1,0,0),(363,362,8,'$�','7�F_',NULL,NULL,'20f3a383',NULL,1,0,0),(364,NULL,1,'Ŧ��','*�*',NULL,1561411731,'b4db9a7b',24,1,0,0),(365,364,1,'���r','!���',NULL,NULL,'5939d637',NULL,1,0,0),(366,365,1,'f���','���',NULL,NULL,'18147c08',NULL,1,0,0),(367,366,1,'qw�\'','vg� ',NULL,NULL,'1d79c3bb',NULL,1,0,0),(368,367,1,'P��','(���',NULL,NULL,'7afa9347',NULL,1,0,0),(369,368,1,'-+�q','�ٔ]',NULL,NULL,'76767a55',NULL,1,0,0),(370,369,8,'��Sv','���{',NULL,NULL,'5422b8f8',NULL,1,0,0),(371,366,1,'�د]','X�i�',NULL,NULL,'fcabc981',NULL,1,0,0),(372,371,1,'хx�','i	`',NULL,NULL,'dcc2d13c',NULL,1,0,0),(373,372,1,'X;�','�=U',NULL,NULL,'70dfdd63',NULL,1,0,0),(374,373,8,'6�oy','Y�\\�',NULL,NULL,'1f337123',NULL,1,0,0),(375,366,1,'�/O ','H�,�',NULL,NULL,'251041d5',NULL,1,0,0),(376,375,1,'0BZc','{\Z�G',NULL,NULL,'078445d8',NULL,1,0,0),(377,376,1,'�T+�','�t�',NULL,NULL,'85d2b1cb',NULL,1,0,0),(378,377,8,'<���','l�C}',NULL,NULL,'3526f27a',NULL,1,0,0),(379,366,1,'ѱ$-','���',NULL,NULL,'c63a3305',NULL,1,0,0),(380,379,1,'iXX�','Cq��',NULL,NULL,'8481d26f',NULL,1,0,0),(381,380,1,'I��6','vV�',NULL,NULL,'43b13058',NULL,1,0,0),(382,381,8,'��l_','b=��',NULL,NULL,'8ab3f2ab',NULL,1,0,0),(383,366,1,'a@\Z','A�b',NULL,NULL,'fd3d5d30',NULL,1,0,0),(384,383,1,'o��','����',NULL,NULL,'38a9750c',NULL,1,0,0),(385,384,1,'����','=�bg',NULL,NULL,'989bb13d',NULL,1,0,0),(386,385,8,'���_','\"��J',NULL,NULL,'09a43aa0',NULL,1,0,0),(387,366,1,'֡&�','��2',NULL,NULL,'ab8b883d',NULL,1,0,0),(388,387,1,' �6','$�',NULL,NULL,'c379da5a',NULL,1,0,0),(389,388,1,'h5�','��`�',NULL,NULL,'51c31438',NULL,1,0,0),(390,389,8,'{�\\�','b',NULL,NULL,'1547b9cc',NULL,1,0,0),(391,NULL,1,'`/�W','ʼ{�',NULL,1561411732,'3a5c35cf',25,1,0,0),(392,391,1,'��','�p�',NULL,NULL,'1393375c',NULL,1,0,0),(393,392,1,'=��y','��M?',NULL,NULL,'2f26402f',NULL,1,0,0),(394,393,1,'��O]','�',NULL,NULL,'2c722356',NULL,1,0,0),(395,394,1,'m=��','Kmч',NULL,NULL,'1ff9368f',NULL,1,0,0),(396,395,1,'�F}','P��',NULL,NULL,'397a515c',NULL,1,0,0),(397,396,8,'a��}','���',NULL,NULL,'7f57d932',NULL,1,0,0),(398,393,1,'QCO�','Lj�',NULL,NULL,'3abb37af',NULL,1,0,0),(399,398,1,'Y���','ӡl',NULL,NULL,'50684046',NULL,1,0,0),(400,399,1,'�9E�','ԗ�8',NULL,NULL,'86d274a4',NULL,1,0,0),(401,400,8,'����','sgo',NULL,NULL,'48ad168d',NULL,1,0,0),(402,393,1,'Y��','�I�',NULL,NULL,'bc071346',NULL,1,0,0),(403,402,1,'��g','c?�',NULL,NULL,'b4945640',NULL,1,0,0),(404,403,1,'�U�J','��',NULL,NULL,'3301f270',NULL,1,0,0),(405,404,8,'#�]e','��J\0',NULL,NULL,'0c773558',NULL,1,0,0),(406,NULL,1,'����','ٯ�;',NULL,1561411732,'579b7813',26,1,0,0),(407,406,1,'���','���M',NULL,NULL,'aabf7821',NULL,1,0,0),(408,407,1,'(y�g','��	{',NULL,NULL,'f52a3444',NULL,1,0,0),(409,408,1,'��','B�',NULL,NULL,'28c91088',NULL,1,0,0),(410,409,1,'���7','2�b�',NULL,NULL,'f8468933',NULL,1,0,0),(411,410,1,'�]�','S���',NULL,NULL,'832f6d22',NULL,1,0,0),(412,411,8,'Pf��','��=F',NULL,NULL,'0c304fdf',NULL,1,0,0),(413,407,1,'lW��','���',NULL,NULL,'cc5b72a4',NULL,1,0,0),(414,413,1,'S\r+{','���',NULL,NULL,'2cd9057a',NULL,1,0,0),(415,414,1,'��DY','K�WW',NULL,NULL,'30925f64',NULL,1,0,0),(416,415,8,'��5','�BK�',NULL,NULL,'8d47d7f8',NULL,1,0,0),(417,NULL,1,'@h�','��f�',NULL,1561411733,'87b0b497',27,1,0,0),(418,417,1,'T�f�','fy�F',NULL,NULL,'c3478b40',NULL,1,0,0),(419,418,1,'�Đ','`ʂ�',NULL,NULL,'8d4383d9',NULL,1,0,0),(420,419,1,':D�;','��;',NULL,NULL,'49a469db',NULL,1,0,0),(421,420,1,']4-','��L',NULL,NULL,'dc4f9619',NULL,1,0,0),(422,421,1,'5u�','$=W',NULL,NULL,'05145758',NULL,1,0,0),(423,422,8,'5���','�',NULL,NULL,'c06d88d8',NULL,1,0,0),(424,417,1,'��kR','H\Z',NULL,NULL,'26006f98',NULL,1,0,0),(425,424,1,'��)�','\"�6�',NULL,NULL,'04cb3430',NULL,1,0,0),(426,425,1,'{�?|','�q',NULL,NULL,'d80a8c0a',NULL,1,0,0),(427,426,8,'�5��','��8',NULL,NULL,'7218356a',NULL,1,0,0),(428,NULL,1,'�k�','���8',NULL,1561411733,'347123d1',28,1,0,0),(429,428,1,'��3k','fo',NULL,NULL,'3660066c',NULL,1,0,0),(430,429,1,'\'#KB','F�X',NULL,NULL,'cbda0675',NULL,1,0,0),(431,430,1,'y�[-','Q?�-',NULL,NULL,'721b9db3',NULL,1,0,0),(432,431,1,'���M','�\n�6',NULL,NULL,'ab8a138d',NULL,1,0,0),(433,432,1,'K�[�','ƿ�',NULL,NULL,'860b3501',NULL,1,0,0),(434,433,8,'��(c','|��G',NULL,NULL,'3d30a68b',NULL,1,0,0),(435,428,1,'qό','\Z��',NULL,NULL,'3f2b9cc0',NULL,1,0,0),(436,435,8,'�-P','i�U-',NULL,NULL,'77f58378',NULL,1,0,0),(437,NULL,1,'G/V�','���V',NULL,1561411734,'572041ca',29,1,0,0),(438,437,1,'8�p�','��Y',NULL,NULL,'90ba2760',NULL,1,0,0),(439,438,1,'�jI','���6',NULL,NULL,'89cc3361',NULL,1,0,0),(440,439,1,'�S/�','� ',NULL,NULL,'50b4a895',NULL,1,0,0),(441,440,1,'tK�i','rkzh',NULL,NULL,'c91ddda6',NULL,1,0,0),(442,441,1,'A�<','�x��',NULL,NULL,'69019a43',NULL,1,0,0),(443,442,8,'���B','�ڧ�',NULL,NULL,'04014124',NULL,1,0,0),(444,NULL,1,'�k(','io�',NULL,1561411734,'af0784b7',30,1,0,0),(445,444,1,'�)GS','���t',NULL,NULL,'6896d795',NULL,1,0,0),(446,445,1,'�/��','�gm�',NULL,NULL,'39d378d9',NULL,1,0,0),(447,446,1,'���','��aq',NULL,NULL,'374f5d98',NULL,1,0,0),(448,447,1,'��,<','�\rr',NULL,NULL,'d77c090a',NULL,1,0,0),(449,448,8,'�\\m','���\n',NULL,NULL,'01f42c49',NULL,1,0,0),(450,448,1,'Rg�','\Z�M?',NULL,NULL,'7c94f4d7',NULL,1,0,0),(451,450,8,'\'-�','\ZZ�@',NULL,NULL,'10107603',NULL,1,0,0),(452,445,1,'�e*\\','��1�',NULL,NULL,'5b45b2bc',NULL,1,0,0),(453,452,1,'V�AZ','īJ_',NULL,NULL,'07887b84',NULL,1,0,0),(454,453,1,'�x�','��m�',NULL,NULL,'cfb312f1',NULL,1,0,0),(455,454,1,'�=EP','m�V�',NULL,NULL,'25ccb4f1',NULL,1,0,0),(456,455,8,'�W�','}\rC6',NULL,NULL,'42706b99',NULL,1,0,0),(457,455,1,'l��o','-��Q',NULL,NULL,'8c71d90b',NULL,1,0,0),(458,457,8,'��Gu','��As',NULL,NULL,'2c39c3fb',NULL,1,0,0),(459,454,1,'�\n�]','�c�\r',NULL,NULL,'cdfb95c5',NULL,1,0,0),(460,459,8,'�B{','!,�',NULL,NULL,'d9c1c290',NULL,1,0,0),(461,459,1,'��','W�-O',NULL,NULL,'80b3514d',NULL,1,0,0),(462,461,8,'�Э�','��C�',NULL,NULL,'2123f049',NULL,1,0,0),(463,454,1,',G�','{A�',NULL,NULL,'892aa622',NULL,1,0,0),(464,463,8,'��h�','M˴',NULL,NULL,'691d9a27',NULL,1,0,0),(465,463,1,'�Ç','�W�B',NULL,NULL,'2c0263c9',NULL,1,0,0),(466,465,8,'E��(','\09J�',NULL,NULL,'06431703',NULL,1,0,0),(467,454,1,'�W�k','-E�q',NULL,NULL,'13952aaa',NULL,1,0,0),(468,467,8,'\r?��','���',NULL,NULL,'d4ac62bf',NULL,1,0,0),(469,467,1,'�[tu','����',NULL,NULL,'62805ff3',NULL,1,0,0),(470,469,8,'�o','�O2',NULL,NULL,'15b18208',NULL,1,0,0),(471,454,1,'5\ZҐ','���M',NULL,NULL,'15ca0d47',NULL,1,0,0),(472,471,8,'�e\'c','teZ�',NULL,NULL,'db41ff15',NULL,1,0,0),(473,471,1,'JeB','�|#�',NULL,NULL,'3035b942',NULL,1,0,0),(474,473,8,'Zc��','pdU',NULL,NULL,'75bcfa1b',NULL,1,0,0),(475,445,1,'��','Ɓ��',NULL,NULL,'7323a704',NULL,1,0,0),(476,475,1,'���','+r�',NULL,NULL,'560c7985',NULL,1,0,0),(477,476,1,'g�A','���4',NULL,NULL,'2a2ad7a3',NULL,1,0,0),(478,477,8,'5$K3','wglm',NULL,NULL,'9484f30c',NULL,1,0,0),(479,445,1,'B#�','��:�',NULL,NULL,'95f73428',NULL,1,0,0),(480,479,8,'�l*�','ą��',NULL,NULL,'73049721',NULL,1,0,0),(481,NULL,1,'u6%B','0���',NULL,1561411735,'8791d9b6',31,1,0,0),(482,481,1,'pa*','�T',NULL,NULL,'f16aac88',NULL,1,0,0),(483,482,1,'�ڑ)','/u1',NULL,NULL,'8582b024',NULL,1,0,0),(484,483,1,'1r��','wHTa',NULL,NULL,'c0dbbdc2',NULL,1,0,0),(485,484,1,'$/0�','�o_�',NULL,NULL,'db6d573b',NULL,1,0,0),(486,485,8,'��)�','l{[',NULL,NULL,'b237cdd5',NULL,1,0,0),(487,485,1,'\\���','�csW',NULL,NULL,'c0b9169b',NULL,1,0,0),(488,487,8,'�ڇ','����',NULL,NULL,'10f71467',NULL,1,0,0),(489,NULL,1,'�W):','��',NULL,1561411736,'325c1a21',32,1,0,0),(490,489,1,'��,Y','`<�X',NULL,NULL,'a8369759',NULL,1,0,0),(491,490,1,'�Ig','��x�',NULL,NULL,'bd7031c1',NULL,1,0,0),(492,491,1,'��24','8U�#',NULL,NULL,'b67d4602',NULL,1,0,0),(493,492,1,'y�V-','1���',NULL,NULL,'d32ff75d',NULL,1,0,0),(494,493,8,'��L','��M',NULL,NULL,'738d4437',NULL,1,0,0),(495,492,1,'���','�x��',NULL,NULL,'f8d432bd',NULL,1,0,0),(496,495,1,'��','��)',NULL,NULL,'7147bb26',NULL,1,0,0),(497,496,1,'�By�','��\r',NULL,NULL,'c2810cfa',NULL,1,0,0),(498,497,8,'��De','�h�|',NULL,NULL,'d7352dca',NULL,1,0,0),(499,NULL,1,'l*�r',';�\0',NULL,1561411736,'3172c5b6',33,1,0,0),(500,499,1,'�z�-','56}',NULL,NULL,'cf474526',NULL,1,0,0),(501,500,1,'��\0�','F�;�',NULL,NULL,'114fb8c9',NULL,1,0,0),(502,501,1,'��','XW��',NULL,NULL,'277331d1',NULL,1,0,0),(503,502,1,'oZ�','Q�	�',NULL,NULL,'5b253dc7',NULL,1,0,0),(504,503,8,'tg�','|Ӄ',NULL,NULL,'01b73d83',NULL,1,0,0),(505,502,1,'x\Z','�F�#',NULL,NULL,'ababc8da',NULL,1,0,0),(506,505,1,'ܭa','�/�*',NULL,NULL,'22755539',NULL,1,0,0),(507,506,8,'�\\�','b��',NULL,NULL,'5642d360',NULL,1,0,0),(508,505,8,'��U�','����',NULL,NULL,'6323ffd1',NULL,1,0,0),(509,505,1,'� ','c�|',NULL,NULL,'a9344232',NULL,1,0,0),(510,509,8,'B�0�','P*c�',NULL,NULL,'a1cca7da',NULL,1,0,0),(511,505,8,'j̊','�07',NULL,NULL,'a7d10b02',NULL,1,0,0),(512,505,1,'cLjV','�J:',NULL,NULL,'2a8b8ddd',NULL,1,0,0),(513,512,8,'����','yS�',NULL,NULL,'d4d8189a',NULL,1,0,0),(514,NULL,1,'<�S�','��QY',NULL,1561411737,'a3376c36',34,1,0,0),(515,514,1,'ѕs�','$�H',NULL,NULL,'a0142948',NULL,1,0,0),(516,515,1,'hc2','b�',NULL,NULL,'982ad391',NULL,1,0,0),(517,516,1,'�um','��;�',NULL,NULL,'3fb8a761',NULL,1,0,0),(518,517,1,'���t','\0�',NULL,NULL,'c99f49b6',NULL,1,0,0),(519,518,8,'t�˯','v[1�',NULL,NULL,'c04f60cb',NULL,1,0,0),(520,517,1,'��a','�W',NULL,NULL,'3f651c0d',NULL,1,0,0),(521,520,1,'%�2�','�dP',NULL,NULL,'99352a0b',NULL,1,0,0),(522,521,8,'Ɂ�m','���',NULL,NULL,'074b9bd5',NULL,1,0,0),(523,520,8,'����','1�q�',NULL,NULL,'788a40cc',NULL,1,0,0),(524,520,1,'D�v*','[5�',NULL,NULL,'96934a50',NULL,1,0,0),(525,524,8,'*\"�','\'�',NULL,NULL,'59cdf979',NULL,1,0,0),(526,NULL,1,'A�','5K�b',NULL,1561411737,'798a9c89',35,1,0,0),(527,526,1,'ȗӇ','��Q',NULL,NULL,'4357bd27',NULL,1,0,0),(528,527,1,'0���','!; %',NULL,NULL,'5636112c',NULL,1,0,0),(529,528,1,' ���','��l',NULL,NULL,'b100b721',NULL,1,0,0),(530,529,1,'�y�\Z',']*��',NULL,NULL,'f7033b31',NULL,1,0,0),(531,530,8,'q�2','���',NULL,NULL,'ac4274f3',NULL,1,0,0),(532,529,1,'7�mg','8��Q',NULL,NULL,'39a9b2ba',NULL,1,0,0),(533,532,1,'�\\�','�{�',NULL,NULL,'d8b19fb9',NULL,1,0,0),(534,533,8,'WpVO','S\\�d',NULL,NULL,'54c34731',NULL,1,0,0),(535,NULL,1,'F\0�','\r(�)',NULL,1561411738,'f45f751c',36,1,0,0),(536,535,1,'f��_','$�TF',NULL,NULL,'59fa6223',NULL,1,0,0),(537,536,1,'��y','e|U',NULL,NULL,'176ba569',NULL,1,0,0),(538,537,1,'��W�','�U�',NULL,NULL,'b04d5632',NULL,1,0,0),(539,538,1,'���}','�g��',NULL,NULL,'c99c46bb',NULL,1,0,0),(540,539,8,'�kFZ','�L)',NULL,NULL,'c568160c',NULL,1,0,0),(541,538,1,'<��','�?_',NULL,NULL,'578d79db',NULL,1,0,0),(542,541,8,'�$�L','�ĩ�',NULL,NULL,'9269112b',NULL,1,0,0),(543,538,1,'G��Y','Ĩ��',NULL,NULL,'95f25994',NULL,1,0,0),(544,543,8,'27*U','8��o',NULL,NULL,'b3cb6ac2',NULL,1,0,0),(545,538,1,'�_','�җ:',NULL,NULL,'3aabcba5',NULL,1,0,0),(546,545,8,'�ˋ','xC)�',NULL,NULL,'880cc678',NULL,1,0,0),(547,538,1,'���t','w�U�',NULL,NULL,'400443f5',NULL,1,0,0),(548,547,8,'f�*�','P�',NULL,NULL,'b8f31a9d',NULL,1,0,0),(549,538,1,'s��','�[6�',NULL,NULL,'3b76dd15',NULL,1,0,0),(550,549,8,'��E�','�vS3',NULL,NULL,'ba04abda',NULL,1,0,0),(551,536,1,'�J\")',',��u',NULL,NULL,'b80ab5d3',NULL,1,0,0),(552,551,1,'Y9�L','�{�-',NULL,NULL,'7c1a96f2',NULL,1,0,0),(553,552,1,'��U\0',')Z��',NULL,NULL,'212c5d62',NULL,1,0,0),(554,553,1,'S�7','�9S�',NULL,NULL,'f3c4a471',NULL,1,0,0),(555,554,8,'_ʡ','lk',NULL,NULL,'dd36f0d8',NULL,1,0,0),(556,553,1,'a���','�;HT',NULL,NULL,'90725b29',NULL,1,0,0),(557,556,8,'j���','�<��',NULL,NULL,'7003dc40',NULL,1,0,0),(558,551,1,'t_��','RhR�',NULL,NULL,'48a803c1',NULL,1,0,0),(559,558,1,'!ϖ�','\Z�r2',NULL,NULL,'59fb759f',NULL,1,0,0),(560,559,8,'���','c13�',NULL,NULL,'a127c0ff',NULL,1,0,0),(561,NULL,1,'���','��',NULL,1561411739,'b035554c',37,1,0,0),(562,561,1,'�/y','�D[',NULL,NULL,'8db63cc5',NULL,1,0,0),(563,562,1,'���','	|W�',NULL,NULL,'47079608',NULL,1,0,0),(564,563,1,'�s�','w�ͺ',NULL,NULL,'041959a0',NULL,1,0,0),(565,564,1,'_�j','K\rfr',NULL,NULL,'1a0cc416',NULL,1,0,0),(566,565,8,'�x�','Y0m�',NULL,NULL,'8768b8c0',NULL,1,0,0),(567,564,1,'Q��c','��',NULL,NULL,'b1cf936f',NULL,1,0,0),(568,567,8,'\0�B','��8',NULL,NULL,'2fba8d70',NULL,1,0,0),(569,561,1,'8!�','q�y�',NULL,NULL,'2f76591f',NULL,1,0,0),(570,569,1,'2�kD','���I',NULL,NULL,'6ba462b4',NULL,1,0,0),(571,570,8,'/�h','�ݩ',NULL,NULL,'3d0017a9',NULL,1,0,0),(572,NULL,1,'wsW�','�	�e',NULL,1561411739,'79ff55c3',38,1,0,0),(573,572,1,'<$`','�v�m',NULL,NULL,'09fbd458',NULL,1,0,0),(574,573,1,':v�',')���',NULL,NULL,'72b97a13',NULL,1,0,0),(575,574,1,'/-x�','��ڣ',NULL,NULL,'29ba457a',NULL,1,0,0),(576,575,1,'�<�@','����',NULL,NULL,'bc81633a',NULL,1,0,0),(577,576,8,'�h�','r���',NULL,NULL,'82175bc2',NULL,1,0,0),(578,575,1,'MJ$','Ƶ�',NULL,NULL,'d97a743a',NULL,1,0,0),(579,578,8,'��\"','�\0',NULL,NULL,'83af02b0',NULL,1,0,0),(580,NULL,1,'2���','i	M/',NULL,1561411739,'3f40936f',39,1,0,0),(581,580,1,'��','X=�*',NULL,NULL,'92d337fc',NULL,1,0,0),(582,581,1,'�B�','L&\Z�',NULL,NULL,'716c6b02',NULL,1,0,0),(583,582,1,'����','�A��',NULL,NULL,'3a1a0705',NULL,1,0,0),(584,583,1,'?�P�','}u��',NULL,NULL,'4a4cf1ca',NULL,1,0,0),(585,584,1,'�\Z�T','�b+�',NULL,NULL,'c584a895',NULL,1,0,0),(586,585,8,'�l�8','KA	',NULL,NULL,'a3b06f77',NULL,1,0,0),(587,582,1,')�?','S� !',NULL,NULL,'a8f87031',NULL,1,0,0),(588,587,1,'	Pu{','�ʭ�',NULL,NULL,'88d39261',NULL,1,0,0),(589,588,1,'#ݬ�','�չ',NULL,NULL,'921c2260',NULL,1,0,0),(590,589,1,'x�e','��BD',NULL,NULL,'8f537887',NULL,1,0,0),(591,590,1,'�2ӷ','uX�',NULL,NULL,'28213a15',NULL,1,0,0),(592,591,8,'R��','h�7',NULL,NULL,'dd798793',NULL,1,0,0),(593,587,1,'\r]�6','	M�',NULL,NULL,'7d3b1ab1',NULL,1,0,0),(594,593,1,'#���','=��',NULL,NULL,'6ba81ff8',NULL,1,0,0),(595,594,1,'�\0v','��k',NULL,NULL,'60b3daf4',NULL,1,0,0),(596,595,1,':�','�]',NULL,NULL,'c682d96b',NULL,1,0,0),(597,596,8,'�Bb�','d|y',NULL,NULL,'f03acf1b',NULL,1,0,0),(598,587,1,'߽\\h','��@;',NULL,NULL,'5aff8d4f',NULL,1,0,0),(599,598,1,'ut�\"','x��w',NULL,NULL,'83987d08',NULL,1,0,0),(600,599,1,'��6�',')�B�',NULL,NULL,'113015a6',NULL,1,0,0),(601,600,1,'*(!','�,�',NULL,NULL,'8b7fb7bb',NULL,1,0,0),(602,601,8,'��2�','�aD�',NULL,NULL,'9d1d3ba8',NULL,1,0,0),(603,587,1,'��J�','R�S�',NULL,NULL,'1a329767',NULL,1,0,0),(604,603,1,'}PO','b�]q',NULL,NULL,'515da613',NULL,1,0,0),(605,604,1,'���','��(\n',NULL,NULL,'47f37109',NULL,1,0,0),(606,605,1,'R�<�','�%�',NULL,NULL,'95861d03',NULL,1,0,0),(607,606,8,']���','�ئ�',NULL,NULL,'86fc24f7',NULL,1,0,0),(608,581,1,'�t�C','1���',NULL,NULL,'2fbbd6c8',NULL,1,0,0),(609,608,1,'�U��','y/�',NULL,NULL,'08afdcf7',NULL,1,0,0),(610,609,1,'o���','�',NULL,NULL,'a341a32b',NULL,1,0,0),(611,610,1,'Ԛd�','��',NULL,NULL,'39988217',NULL,1,0,0),(612,611,8,'�vCX',' &]\0',NULL,NULL,'630a3ad8',NULL,1,0,0),(613,608,1,')֜�','�E��',NULL,NULL,'03d45190',NULL,1,0,0),(614,613,1,'àio','J\r��',NULL,NULL,'a4b37f9c',NULL,1,0,0),(615,614,1,'���6','5(Z]',NULL,NULL,'12374d6c',NULL,1,0,0),(616,615,1,'H�Ai','4e\rz',NULL,NULL,'2a3868b0',NULL,1,0,0),(617,616,1,'��<�','���F',NULL,NULL,'5d65a4d1',NULL,1,0,0),(618,617,8,'qH`�','���',NULL,NULL,'0633c30b',NULL,1,0,0),(619,613,1,'\0?�o','`f&�',NULL,NULL,'85a754fd',NULL,1,0,0),(620,619,1,'���T','#G�L',NULL,NULL,'18775456',NULL,1,0,0),(621,620,1,'T�Y','�ʊY',NULL,NULL,'d9809294',NULL,1,0,0),(622,621,1,'���','��;',NULL,NULL,'9f08816c',NULL,1,0,0),(623,622,8,'�[i\r','B���',NULL,NULL,'bba9043d',NULL,1,0,0),(624,613,1,'ft,(','D�',NULL,NULL,'bdf9abf3',NULL,1,0,0),(625,624,1,'\0D��','\Z4�%',NULL,NULL,'5daa9f21',NULL,1,0,0),(626,625,1,'���_','T�b�',NULL,NULL,'6456a289',NULL,1,0,0),(627,626,1,']KV','� �',NULL,NULL,'a6d44adc',NULL,1,0,0),(628,627,8,'�V<�','�O�',NULL,NULL,'0024fb82',NULL,1,0,0),(629,581,1,'���1','���',NULL,NULL,'ba3c880d',NULL,1,0,0),(630,629,1,'��9q','�\0_p',NULL,NULL,'d3cfa3cf',NULL,1,0,0),(631,630,1,'���','e��q',NULL,NULL,'9f7acc1b',NULL,1,0,0),(632,631,1,'r�X�','�4�',NULL,NULL,'bc731c5f',NULL,1,0,0),(633,632,8,'O�]','Gq�	',NULL,NULL,'c2911df2',NULL,1,0,0),(634,629,1,'_�B�','&zi�',NULL,NULL,'9d11356c',NULL,1,0,0),(635,634,1,'C�','\\\"��',NULL,NULL,'ca9708b0',NULL,1,0,0),(636,635,1,'kI8E','��',NULL,NULL,'7ff37662',NULL,1,0,0),(637,636,1,'z*\Z','����',NULL,NULL,'9b4b90f1',NULL,1,0,0),(638,637,1,'��=E','9�cO',NULL,NULL,'bc9a8c04',NULL,1,0,0),(639,638,8,'3I�V','E-�',NULL,NULL,'c8f12ba1',NULL,1,0,0),(640,634,1,' ���','��',NULL,NULL,'267210cb',NULL,1,0,0),(641,640,1,'D7A','�#o0',NULL,NULL,'d49d3131',NULL,1,0,0),(642,641,1,'Hځ�','���',NULL,NULL,'0c40dc2f',NULL,1,0,0),(643,642,1,'��b','&)�',NULL,NULL,'8c2d31b5',NULL,1,0,0),(644,643,8,'��u','�t��',NULL,NULL,'9f930188',NULL,1,0,0),(645,634,1,'|�H�','�v]',NULL,NULL,'db8ffa93',NULL,1,0,0),(646,645,1,'�9m','�{',NULL,NULL,'fd6fa027',NULL,1,0,0),(647,646,1,'C�C','2)�+',NULL,NULL,'cadb8288',NULL,1,0,0),(648,647,1,'/','��\')',NULL,NULL,'32ff6f0f',NULL,1,0,0),(649,648,8,'G�0S','\rZ\'',NULL,NULL,'87351a8f',NULL,1,0,0),(650,634,1,'�`/','V��`',NULL,NULL,'8f0aabb4',NULL,1,0,0),(651,650,1,'����','ۺ��',NULL,NULL,'7bf2d169',NULL,1,0,0),(652,651,1,'o�R�','�YB�',NULL,NULL,'2ab68116',NULL,1,0,0),(653,652,1,'M��9','��$�',NULL,NULL,'f6705579',NULL,1,0,0),(654,653,8,'m9��','**!,',NULL,NULL,'d21a32f8',NULL,1,0,0),(655,634,1,'x��K','�]ؿ',NULL,NULL,'14267c96',NULL,1,0,0),(656,655,1,'����','��T`',NULL,NULL,'b1d03451',NULL,1,0,0),(657,656,1,' A��','��l',NULL,NULL,'d1dbf2ff',NULL,1,0,0),(658,657,1,'O�T�','�$',NULL,NULL,'1d8f3228',NULL,1,0,0),(659,658,8,'׆ʓ','X�c',NULL,NULL,'731cd896',NULL,1,0,0),(660,634,1,'�K��','��l',NULL,NULL,'59c5bd13',NULL,1,0,0),(661,660,1,'݂��','���',NULL,NULL,'b2f599c6',NULL,1,0,0),(662,661,1,'�{��','ϐ�\\',NULL,NULL,'94985668',NULL,1,0,0),(663,662,1,'�\r!�','U�z',NULL,NULL,'3dc8d372',NULL,1,0,0),(664,663,8,'���','u�{�',NULL,NULL,'9bd40d3c',NULL,1,0,0),(665,581,1,'GZ�','�bS',NULL,NULL,'fd9a40a6',NULL,1,0,0),(666,665,1,':	�\\','��p�',NULL,NULL,'d5b649c5',NULL,1,0,0),(667,666,1,'-l�','$�GG',NULL,NULL,'5369cf28',NULL,1,0,0),(668,667,1,'��<�','B�\nq',NULL,NULL,'44b88969',NULL,1,0,0),(669,668,8,'����','c��',NULL,NULL,'49453a5a',NULL,1,0,0),(670,665,1,',\rIr','�5�/',NULL,NULL,'323bd05b',NULL,1,0,0),(671,670,1,'E��h','��',NULL,NULL,'972f528d',NULL,1,0,0),(672,671,1,'U}��','O`w[',NULL,NULL,'c49d84b5',NULL,1,0,0),(673,672,1,'<vF(','\Z�qx',NULL,NULL,'26394537',NULL,1,0,0),(674,673,1,'�P�Y','���',NULL,NULL,'a31dd218',NULL,1,0,0),(675,674,8,'���K','����',NULL,NULL,'a995a6b7',NULL,1,0,0),(676,670,1,'�I��','1�|4',NULL,NULL,'9d3707a4',NULL,1,0,0),(677,676,1,'E5��','��',NULL,NULL,'a6441088',NULL,1,0,0),(678,677,1,'Zj��','���r',NULL,NULL,'24424b81',NULL,1,0,0),(679,678,1,'ss��','�?[8',NULL,NULL,'b590606c',NULL,1,0,0),(680,679,8,'���x','��I',NULL,NULL,'a4f2dfa6',NULL,1,0,0),(681,670,1,'�<�4','5���',NULL,NULL,'46bf2d48',NULL,1,0,0),(682,681,1,'���','�At',NULL,NULL,'d5bd7a07',NULL,1,0,0),(683,682,1,'uO','���k',NULL,NULL,'f532a051',NULL,1,0,0),(684,683,1,'�p5b','���S',NULL,NULL,'1ff0a095',NULL,1,0,0),(685,684,8,'Dr�','����',NULL,NULL,'dcbc0770',NULL,1,0,0),(686,670,1,'�','���\'',NULL,NULL,'624fb21d',NULL,1,0,0),(687,686,1,'w���','u���',NULL,NULL,'00a74083',NULL,1,0,0),(688,687,1,'%\Z�','u�%c',NULL,NULL,'a56d0dbb',NULL,1,0,0),(689,688,1,'q[=�','(���',NULL,NULL,'288dc0f9',NULL,1,0,0),(690,689,8,'\rW�','�D��',NULL,NULL,'2941bf78',NULL,1,0,0),(691,581,1,'r\rT','V5�c',NULL,NULL,'f26920a7',NULL,1,0,0),(692,691,1,'����','�#|}',NULL,NULL,'9cd5a4cc',NULL,1,0,0),(693,692,1,'�\'','�15',NULL,NULL,'82c0d30d',NULL,1,0,0),(694,693,1,'5O�','܆/�',NULL,NULL,'16d28f8a',NULL,1,0,0),(695,694,8,'�Z7,','z���',NULL,NULL,'3dd1b84d',NULL,1,0,0),(696,NULL,1,'��h�','_�o�',NULL,1561411743,'c52f4717',40,1,0,0),(697,696,1,'R�&;','-֘|',NULL,NULL,'59c0a7f1',NULL,1,0,0),(698,697,1,'��/�','/9(�',NULL,NULL,'13dff671',NULL,1,0,0),(699,698,1,'j�xM','#H�',NULL,NULL,'d1921654',NULL,1,0,0),(700,699,1,'6ό','׷_',NULL,NULL,'4d8bc5b0',NULL,1,0,0),(701,700,8,'b�A','v���',NULL,NULL,'811856f8',NULL,1,0,0),(702,699,8,',9�','\\�`',NULL,NULL,'4b473fb8',NULL,1,0,0),(703,698,1,'1��','�t�F',NULL,NULL,'459254d3',NULL,1,0,0),(704,703,8,'lJS�','c�[�',NULL,NULL,'5c034f72',NULL,1,0,0),(705,NULL,1,'��Tj','?���',NULL,1561411744,'44c426ab',41,1,0,0),(706,705,1,'#j�]','{W��',NULL,NULL,'8147749b',NULL,1,0,0),(707,706,1,'t�p','��t',NULL,NULL,'0c7f4746',NULL,1,0,0),(708,707,1,'���Q','F\nd�',NULL,NULL,'f7faa099',NULL,1,0,0),(709,708,1,'t:r','vӚq',NULL,NULL,'71398b88',NULL,1,0,0),(710,709,8,'�m�U','�5��',NULL,NULL,'25f2684f',NULL,1,0,0),(711,708,8,'��G�','�Zw�',NULL,NULL,'73d0760a',NULL,1,0,0),(712,706,1,'u�c-','�6�',NULL,NULL,'b6b77693',NULL,1,0,0),(713,712,1,'1a\"','�Xۣ',NULL,NULL,'92f801b1',NULL,1,0,0),(714,713,1,'��c','7=��',NULL,NULL,'a4a66c9c',NULL,1,0,0),(715,714,8,'4��S','J�k�',NULL,NULL,'80320272',NULL,1,0,0),(716,713,8,'�M�o','l��',NULL,NULL,'4bc13961',NULL,1,0,0),(717,NULL,1,'���\n','t6VZ',NULL,1561411745,'67fda797',42,1,0,0),(718,717,1,'rcĘ','��˂',NULL,NULL,'b9733f1d',NULL,1,0,0),(719,718,1,'Ѵ&g','�o�',NULL,NULL,'c9f6ad29',NULL,1,0,0),(720,719,1,'��l','\Z�z�',NULL,NULL,'d403c13c',NULL,1,0,0),(721,720,1,'1,�','%��',NULL,NULL,'043790f6',NULL,1,0,0),(722,721,8,'_�j�','�s��',NULL,NULL,'f81b042a',NULL,1,0,0),(723,720,8,'\'�)�','�A�',NULL,NULL,'b2069624',NULL,1,0,0),(724,NULL,1,'1;��','�rq',NULL,1561411745,'dcb5afa2',43,1,0,0),(725,724,1,'��ϡ','ʪ�s',NULL,NULL,'44662b5d',NULL,1,0,0),(726,725,1,'9�	','rk�',NULL,NULL,'3f981b38',NULL,1,0,0),(727,726,1,'f	�','�s�',NULL,NULL,'a9c1cc20',NULL,1,0,0),(728,727,1,'`9�','���',NULL,NULL,'dbf5f632',NULL,1,0,0),(729,728,1,'&�`','�u��',NULL,NULL,'a209df24',NULL,1,0,0),(730,729,8,'7x̨',' Ё�',NULL,NULL,'59f632fa',NULL,1,0,0),(731,725,1,'R,','�]�',NULL,NULL,'b6f7f729',NULL,1,0,0),(732,731,1,'�o�','�ȡ',NULL,NULL,'9827f3f9',NULL,1,0,0),(733,732,1,'9#!\r','u4',NULL,NULL,'d91c8296',NULL,1,0,0),(734,733,1,'�Z�G','� ',NULL,NULL,'9d366692',NULL,1,0,0),(735,734,8,'�f<','�Q\Z',NULL,NULL,'91ca3873',NULL,1,0,0),(736,725,1,'���','�{�1',NULL,NULL,'cadf2918',NULL,1,0,0),(737,736,1,'y؏u','�Yih',NULL,NULL,'75c1ab73',NULL,1,0,0),(738,737,1,']��{','9�l�',NULL,NULL,'2241d0f1',NULL,1,0,0),(739,738,1,'T\n��','\Z�k�',NULL,NULL,'aa5c7498',NULL,1,0,0),(740,739,8,'�)�G','\r��|',NULL,NULL,'b3cd881b',NULL,1,0,0),(741,736,1,'f$�\r','�\"y�',NULL,NULL,'c1393d79',NULL,1,0,0),(742,741,1,'U+\'','�ի�',NULL,NULL,'5982428d',NULL,1,0,0),(743,742,1,'*��b','���X',NULL,NULL,'9dc8d9af',NULL,1,0,0),(744,743,1,',���','QC�',NULL,NULL,'342ff677',NULL,1,0,0),(745,744,1,')J��','{lVc',NULL,NULL,'6c9c42ca',NULL,1,0,0),(746,745,1,'E�','7�|u',NULL,NULL,'09ab08d7',NULL,1,0,0),(747,746,8,'\nچv','��Ň',NULL,NULL,'8826fd7b',NULL,1,0,0),(748,743,1,':=�','	Ww�',NULL,NULL,'39342045',NULL,1,0,0),(749,748,1,'�#','�U\r',NULL,NULL,'0d1742b8',NULL,1,0,0),(750,749,1,'��\0','���',NULL,NULL,'d37fcb4a',NULL,1,0,0),(751,750,1,'	ۋ�','P]ܚ',NULL,NULL,'92bc969d',NULL,1,0,0),(752,751,1,'iq�','|Ŝ�',NULL,NULL,'f3fa9dc7',NULL,1,0,0),(753,752,8,'��Y!','��f',NULL,NULL,'cc9cbb08',NULL,1,0,0),(754,748,1,'�!f1','��km',NULL,NULL,'d8569301',NULL,1,0,0),(755,754,1,'���K','t��M',NULL,NULL,'7d216838',NULL,1,0,0),(756,755,1,'��&�','���',NULL,NULL,'2cadc8cc',NULL,1,0,0),(757,756,1,' d',',s=W',NULL,NULL,'b279ab1f',NULL,1,0,0),(758,757,8,'��r�','R(',NULL,NULL,'070cb32f',NULL,1,0,0),(759,748,1,'m�c�','�6:|',NULL,NULL,'f2a0b723',NULL,1,0,0),(760,759,1,'y:�','�B�m',NULL,NULL,'076a155d',NULL,1,0,0),(761,760,1,'P��_','\r�#�',NULL,NULL,'c9384146',NULL,1,0,0),(762,761,1,'�ZQ�','C�2�',NULL,NULL,'91915632',NULL,1,0,0),(763,762,8,'��%�','��ם',NULL,NULL,'715d6140',NULL,1,0,0),(764,748,1,'u��1','Ԗ?�',NULL,NULL,'27aacb39',NULL,1,0,0),(765,764,1,'%ßu','�c׍',NULL,NULL,'434bb017',NULL,1,0,0),(766,765,1,'�R��',')��',NULL,NULL,'96921b1a',NULL,1,0,0),(767,766,1,'����','��Sr',NULL,NULL,'4d2da45f',NULL,1,0,0),(768,767,8,'e��_','����',NULL,NULL,'8040986f',NULL,1,0,0),(769,742,1,'@�X�','}�',NULL,NULL,'72b9480a',NULL,1,0,0),(770,769,1,'���L','AI��',NULL,NULL,'3af95a51',NULL,1,0,0),(771,770,1,'}b4T','���!',NULL,NULL,'d4335db7',NULL,1,0,0),(772,771,1,'EO�@','I���',NULL,NULL,'ada71331',NULL,1,0,0),(773,772,8,'\n5�|',',�Ax',NULL,NULL,'56f36c08',NULL,1,0,0),(774,769,1,'����','%��',NULL,NULL,'a1c4baf8',NULL,1,0,0),(775,774,1,'�ƺ;','�,��',NULL,NULL,'a8c2b480',NULL,1,0,0),(776,775,1,'{�,','tG��',NULL,NULL,'01cfff5b',NULL,1,0,0),(777,776,1,'vf<�','��lG',NULL,NULL,'2a4d6514',NULL,1,0,0),(778,777,1,';A�]','�IX',NULL,NULL,'a3356c51',NULL,1,0,0),(779,778,8,'5O�M','\ZսU',NULL,NULL,'c34247f9',NULL,1,0,0),(780,774,1,'ʂ�','��=(',NULL,NULL,'982802c7',NULL,1,0,0),(781,780,1,'ߥ%','���',NULL,NULL,'5f712a1c',NULL,1,0,0),(782,781,1,'T��','8\Z�q',NULL,NULL,'4bc1a45a',NULL,1,0,0),(783,782,1,'x��','�b�I',NULL,NULL,'a2bc6827',NULL,1,0,0),(784,783,8,'BaT;','t\07',NULL,NULL,'17bad719',NULL,1,0,0),(785,774,1,'\\=�','�5e',NULL,NULL,'a5bd2445',NULL,1,0,0),(786,785,1,'��t�','�I�u',NULL,NULL,'ba691bcc',NULL,1,0,0),(787,786,1,'iT�E','���%',NULL,NULL,'8fa8d68a',NULL,1,0,0),(788,787,1,'�`�e','p6j6',NULL,NULL,'32b3f0ab',NULL,1,0,0),(789,788,8,'7��(','WY�',NULL,NULL,'1fa615f8',NULL,1,0,0),(790,742,1,',�','+���',NULL,NULL,'42864f2a',NULL,1,0,0),(791,790,1,'���%','e��',NULL,NULL,'55267f16',NULL,1,0,0),(792,791,1,'p�ɒ','�;��',NULL,NULL,'51903d75',NULL,1,0,0),(793,792,1,'#t=U','��_',NULL,NULL,'f705c6f0',NULL,1,0,0),(794,793,8,'\"���','����',NULL,NULL,'9204d3d7',NULL,1,0,0),(795,790,1,'D*��','b̅�',NULL,NULL,'3894af54',NULL,1,0,0),(796,795,1,'D���','ا[�',NULL,NULL,'82015394',NULL,1,0,0),(797,796,1,'�;�','V�(�',NULL,NULL,'6207f0b8',NULL,1,0,0),(798,797,1,'}��','�j˰',NULL,NULL,'a4506b9a',NULL,1,0,0),(799,798,1,':E�','V��',NULL,NULL,'f59fb4b4',NULL,1,0,0),(800,799,8,'s���','��E,',NULL,NULL,'3fbd04f4',NULL,1,0,0),(801,795,1,'���4','���a',NULL,NULL,'55c42bab',NULL,1,0,0),(802,801,1,'�hB�',' �D)',NULL,NULL,'4d6bba13',NULL,1,0,0),(803,802,1,'v iT','$�)V',NULL,NULL,'634f86f4',NULL,1,0,0),(804,803,1,'\nÍ)','Qm]�',NULL,NULL,'ba8d28d1',NULL,1,0,0),(805,804,8,'\r�_�','��D',NULL,NULL,'aad4fd76',NULL,1,0,0),(806,795,1,'�c�\\','D+�T',NULL,NULL,'42832500',NULL,1,0,0),(807,806,1,'�9�P','�I%�',NULL,NULL,'81aa7abb',NULL,1,0,0),(808,807,1,'�*j�','�9�b',NULL,NULL,'a364d70b',NULL,1,0,0),(809,808,1,'��H�','��L1',NULL,NULL,'d6a44d79',NULL,1,0,0),(810,809,8,'V��w','F��',NULL,NULL,'b33bab7f',NULL,1,0,0),(811,795,1,'��','߄�w',NULL,NULL,'02094920',NULL,1,0,0),(812,811,1,'ʸ�','xTD�',NULL,NULL,'bd34f651',NULL,1,0,0),(813,812,1,'�S��','�vb�',NULL,NULL,'64f93a17',NULL,1,0,0),(814,813,1,'u�[�','�\Z��',NULL,NULL,'f055bd81',NULL,1,0,0),(815,814,8,'trP�','9\'˟',NULL,NULL,'373ab2d3',NULL,1,0,0),(816,795,1,'D�5\"','���',NULL,NULL,'c17a3ba4',NULL,1,0,0),(817,816,1,'Vx-\"','=��Z',NULL,NULL,'71b48bcb',NULL,1,0,0),(818,817,1,'tv3�','�Fw',NULL,NULL,'71357f94',NULL,1,0,0),(819,818,1,'���A','�\"kp',NULL,NULL,'6a482fbf',NULL,1,0,0),(820,819,8,'�c�','q�V&',NULL,NULL,'bffbc0a3',NULL,1,0,0),(821,795,1,'�ؤ�','7!a�',NULL,NULL,'42256157',NULL,1,0,0),(822,821,1,'��`�','̡P',NULL,NULL,'806c9cfb',NULL,1,0,0),(823,822,1,'��A','A�Q',NULL,NULL,'db3714fa',NULL,1,0,0),(824,823,1,'7�c*','_��',NULL,NULL,'f2cc3af0',NULL,1,0,0),(825,824,8,'��','l{��',NULL,NULL,'7b9a8a8f',NULL,1,0,0),(826,742,1,'9�2�','`r�',NULL,NULL,'b9309b91',NULL,1,0,0),(827,826,1,'���','_C�L',NULL,NULL,'3bc6ba34',NULL,1,0,0),(828,827,1,'��?�','̓r',NULL,NULL,'23400708',NULL,1,0,0),(829,828,1,'�q;�','Y��r',NULL,NULL,'c4a9352f',NULL,1,0,0),(830,829,8,'�C\0�','2E',NULL,NULL,'96f64734',NULL,1,0,0),(831,826,1,'�0C�','���',NULL,NULL,'704a79cf',NULL,1,0,0),(832,831,1,'����','�O�',NULL,NULL,'6dd419b9',NULL,1,0,0),(833,832,1,'g���','�Tdg',NULL,NULL,'24ab5b76',NULL,1,0,0),(834,833,1,'S\n\0\'','�\rLx',NULL,NULL,'23004f2a',NULL,1,0,0),(835,834,1,'�m2','�{�c',NULL,NULL,'d9b7c441',NULL,1,0,0),(836,835,8,'	3sy','��!p',NULL,NULL,'9b923264',NULL,1,0,0),(837,831,1,'�F�h','\Z��[',NULL,NULL,'6b88c3c3',NULL,1,0,0),(838,837,1,'��`Q','V���',NULL,NULL,'6cb5fa27',NULL,1,0,0),(839,838,1,'	��G','R{��',NULL,NULL,'a54d00a1',NULL,1,0,0),(840,839,1,'��4�','��h',NULL,NULL,'9527f7b5',NULL,1,0,0),(841,840,8,'��ʦ','#��',NULL,NULL,'85cdbb91',NULL,1,0,0),(842,831,1,'���%','�}�l',NULL,NULL,'3a9fa187',NULL,1,0,0),(843,842,1,'Sp�S','�P�8',NULL,NULL,'df580aa8',NULL,1,0,0),(844,843,1,'U@�D','����',NULL,NULL,'d7c788c6',NULL,1,0,0),(845,844,1,':]�','41�',NULL,NULL,'3b5bb866',NULL,1,0,0),(846,845,8,'�Et�','��0�',NULL,NULL,'dc4f21f7',NULL,1,0,0),(847,831,1,'F6','FEX(',NULL,NULL,'23694760',NULL,1,0,0),(848,847,1,'�+�','Z��O',NULL,NULL,'9f8f6834',NULL,1,0,0),(849,848,1,'\"x�','+�R�',NULL,NULL,'7621bfd0',NULL,1,0,0),(850,849,1,'w�','o�9`',NULL,NULL,'dd2043db',NULL,1,0,0),(851,850,8,'��z','���!',NULL,NULL,'dc10a3f3',NULL,1,0,0),(852,742,1,'�\n&|','�|��',NULL,NULL,'8487adc0',NULL,1,0,0),(853,852,1,'9��Z','b3�',NULL,NULL,'47c35278',NULL,1,0,0),(854,853,1,'�;','�Zu�',NULL,NULL,'4f86bb16',NULL,1,0,0),(855,854,1,'=\n1�','�rJ�',NULL,NULL,'22374d95',NULL,1,0,0),(856,855,8,'a�%�','![LM',NULL,NULL,'ad201454',NULL,1,0,0),(857,725,1,'����','�Ң@',NULL,NULL,'66170226',NULL,1,0,0),(858,857,1,'�āz','?�`',NULL,NULL,'11459110',NULL,1,0,0),(859,858,1,'s	�','�7%',NULL,NULL,'077f9d0f',NULL,1,0,0),(860,859,1,'a�Z:','�\n�7',NULL,NULL,'7a29643a',NULL,1,0,0),(861,860,8,'��g�','��5',NULL,NULL,'41fcc3ad',NULL,1,0,0),(862,725,1,'-q�R','��Be',NULL,NULL,'072379b9',NULL,1,0,0),(863,862,1,'�V�{','��i',NULL,NULL,'66d264f7',NULL,1,0,0),(864,863,1,'p���','�xo�',NULL,NULL,'25fcb83a',NULL,1,0,0),(865,864,1,',K�J','�e}�',NULL,NULL,'b57683f9',NULL,1,0,0),(866,865,8,'4G�8','�2Om',NULL,NULL,'2079455f',NULL,1,0,0),(867,725,1,'C�A�',':�T�',NULL,NULL,'6682c660',NULL,1,0,0),(868,867,1,'ơ�','�o�',NULL,NULL,'5f30af25',NULL,1,0,0),(869,868,1,'�w)�','�Hg',NULL,NULL,'d03a632f',NULL,1,0,0),(870,869,1,'r�а','?T',NULL,NULL,'8547d371',NULL,1,0,0),(871,870,8,'!��','���',NULL,NULL,'16c3c95d',NULL,1,0,0),(872,725,1,'�k�3','Ҷ�',NULL,NULL,'ddfc56b7',NULL,1,0,0),(873,872,1,'T��','���',NULL,NULL,'6928f46f',NULL,1,0,0),(874,873,1,'��)','��',NULL,NULL,'54bd658f',NULL,1,0,0),(875,874,1,'�0�A','E8�',NULL,NULL,'f6f535af',NULL,1,0,0),(876,875,8,'1�\"','Qy',NULL,NULL,'61899f30',NULL,1,0,0),(877,725,1,'���','!F#',NULL,NULL,'892d15ac',NULL,1,0,0),(878,877,1,'3','\0�w',NULL,NULL,'42a8c260',NULL,1,0,0),(879,878,1,':��','օ��',NULL,NULL,'921fb3f7',NULL,1,0,0),(880,879,1,'�\'�','cܧ',NULL,NULL,'3408fdc8',NULL,1,0,0),(881,880,8,'��E�','#;�x',NULL,NULL,'109b733a',NULL,1,0,0),(882,725,1,'�s��','��7B',NULL,NULL,'98cf3156',NULL,1,0,0),(883,882,1,'�\r�','��\"�',NULL,NULL,'0749a298',NULL,1,0,0),(884,883,1,'���2','�3�0',NULL,NULL,'aa269561',NULL,1,0,0),(885,884,1,'��e�','�)�?',NULL,NULL,'4d524288',NULL,1,0,0),(886,885,8,'@g�','���',NULL,NULL,'27237c9a',NULL,1,0,0),(887,725,1,'i��','Ŷ��',NULL,NULL,'45a78a42',NULL,1,0,0),(888,887,1,'7Hk�','	Fݷ',NULL,NULL,'d5cc428f',NULL,1,0,0),(889,888,1,'ȱ\n�','���\0',NULL,NULL,'0600d865',NULL,1,0,0),(890,889,1,'`�`J','�',NULL,NULL,'12b7f6cd',NULL,1,0,0),(891,890,8,'���','t���',NULL,NULL,'a2015443',NULL,1,0,0),(892,NULL,1,'�o<�','H�o�',NULL,1561411751,'7c208a33',44,1,0,0),(893,892,1,':���','��Y',NULL,NULL,'c5173356',NULL,1,0,0),(894,893,1,'�Ft�','dEY',NULL,NULL,'8020b462',NULL,1,0,0),(895,894,1,'�x�','֚\Z�',NULL,NULL,'8afa7faf',NULL,1,0,0),(896,895,1,'�F��','``A\0',NULL,NULL,'a3b45fca',NULL,1,0,0),(897,896,8,'w\r�','ʅ0�',NULL,NULL,'8bd321bc',NULL,1,0,0),(898,894,1,'`2�','@�6�',NULL,NULL,'1b645b36',NULL,1,0,0),(899,898,1,'3wʂ','�h,\r',NULL,NULL,'34c51473',NULL,1,0,0),(900,899,8,'s��','4}�i',NULL,NULL,'b1c37785',NULL,1,0,0),(901,894,1,'��E|','�zA�',NULL,NULL,'a0ca82b8',NULL,1,0,0),(902,901,1,'�*3','�L��',NULL,NULL,'0868363a',NULL,1,0,0),(903,902,8,'j�0d','�80�',NULL,NULL,'505d8bd2',NULL,1,0,0),(904,894,1,'`��','9\\b',NULL,NULL,'810b0329',NULL,1,0,0),(905,904,1,'�p��','a&M�',NULL,NULL,'0ff6abd4',NULL,1,0,0),(906,905,8,'0o\r','�	R?',NULL,NULL,'d21525ac',NULL,1,0,0),(907,894,1,'S\ZϤ','�m�)',NULL,NULL,'611adcb8',NULL,1,0,0),(908,907,1,'$��','���',NULL,NULL,'d527a2a6',NULL,1,0,0),(909,908,8,'L��6','�xv',NULL,NULL,'06474755',NULL,1,0,0),(910,894,1,'	���','� )�',NULL,NULL,'3af2d144',NULL,1,0,0),(911,910,1,'�\n','4���',NULL,NULL,'267c7190',NULL,1,0,0),(912,911,8,'�̶9','{�Ub',NULL,NULL,'7c5f2b20',NULL,1,0,0),(913,894,1,'*��l','�&Ze',NULL,NULL,'ffdafbf2',NULL,1,0,0),(914,913,1,'�pUf',',Tf',NULL,NULL,'99fa2445',NULL,1,0,0),(915,914,8,'�s','-�',NULL,NULL,'193cc632',NULL,1,0,0),(916,NULL,1,'	��:','A�\'',NULL,1561411752,'8d941df9',45,1,0,0),(917,916,1,'�s�','�?�',NULL,NULL,'a9016835',NULL,1,0,0),(918,917,1,'o4w','C�\'�',NULL,NULL,'df0f032c',NULL,1,0,0),(919,918,1,'WPVt','�ǝJ',NULL,NULL,'112c43a9',NULL,1,0,0),(920,919,1,'s�)�','/C)�',NULL,NULL,'8813a16b',NULL,1,0,0),(921,920,8,'�/��','U�Q�',NULL,NULL,'784bcb6f',NULL,1,0,0),(922,918,1,'\Z�\"�','�e�',NULL,NULL,'3bf77c09',NULL,1,0,0),(923,922,1,'/x�t','vmw�',NULL,NULL,'76fa454a',NULL,1,0,0),(924,923,8,'��v','���_',NULL,NULL,'cdf17af3',NULL,1,0,0),(925,918,1,'��',';���',NULL,NULL,'f9cac75c',NULL,1,0,0),(926,925,1,'u��','��#',NULL,NULL,'3727b6c5',NULL,1,0,0),(927,926,8,'��$','Y��r',NULL,NULL,'131c5a73',NULL,1,0,0),(928,918,1,'f�6','�D�z',NULL,NULL,'fb7619c4',NULL,1,0,0),(929,928,1,'�ؕ\0','_T',NULL,NULL,'57063214',NULL,1,0,0),(930,929,8,',���','�G�',NULL,NULL,'43c10751',NULL,1,0,0),(931,918,1,'8���','|�H[',NULL,NULL,'40c5634c',NULL,1,0,0),(932,931,1,'h�Ѩ','\0hr',NULL,NULL,'b2f1c117',NULL,1,0,0),(933,932,8,'�jb�','/\"��',NULL,NULL,'ab8924a7',NULL,1,0,0),(934,918,1,'��l8','�B��',NULL,NULL,'029d0423',NULL,1,0,0),(935,934,1,'�?p�','L��3',NULL,NULL,'6a882bc6',NULL,1,0,0),(936,935,8,'���','7\\�',NULL,NULL,'88a05368',NULL,1,0,0),(937,NULL,1,'��S','<�&�',NULL,1561411753,'3dfc1885',46,1,0,0),(938,937,1,'��w3','d1\\',NULL,NULL,'020bfc61',NULL,1,0,0),(939,938,1,'�v�v','HVe\'',NULL,NULL,'8a9400ac',NULL,1,0,0),(940,939,1,'Ӛ��','�X��',NULL,NULL,'d6151157',NULL,1,0,0),(941,940,1,'G)�7','k-��',NULL,NULL,'63983f71',NULL,1,0,0),(942,941,8,'`}�',';R=',NULL,NULL,'c1513223',NULL,1,0,0),(943,939,1,'m�׉','��M�',NULL,NULL,'ab2cc92b',NULL,1,0,0),(944,943,1,'#�O�','ŐU�',NULL,NULL,'b7472147',NULL,1,0,0),(945,944,8,'`qm`','��ќ',NULL,NULL,'b75b55c9',NULL,1,0,0),(946,NULL,1,'�=4�','<�d�',NULL,1561411753,'94f1b1af',47,1,0,0),(947,946,1,'��',',`�',NULL,NULL,'600cbb8f',NULL,1,0,0),(948,947,1,'\r�','�o_b',NULL,NULL,'7f2f126f',NULL,1,0,0),(949,948,1,'%���','70e�',NULL,NULL,'df7b5078',NULL,1,0,0),(950,949,1,'}Q$','Z�\r�',NULL,NULL,'54c8ca22',NULL,1,0,0),(951,950,8,'h���','O�ɖ',NULL,NULL,'5f8a309a',NULL,1,0,0),(952,948,1,'����','�		�',NULL,NULL,'adfc38a6',NULL,1,0,0),(953,952,8,'\\�Ͱ','�TR�',NULL,NULL,'3770b163',NULL,1,0,0),(954,948,1,'@z\r�','�(b',NULL,NULL,'31180524',NULL,1,0,0),(955,954,8,'�G�','oX1',NULL,NULL,'13109622',NULL,1,0,0),(956,NULL,1,'��','	��[',NULL,1561411754,'88166f86',48,1,0,0),(957,956,1,'ͼ;','��',NULL,NULL,'23d695cf',NULL,1,0,0),(958,957,1,'����','���',NULL,NULL,'84550105',NULL,1,0,0),(959,958,1,'8��2','��ϵ',NULL,NULL,'42975878',NULL,1,0,0),(960,959,1,'���','���',NULL,NULL,'57456792',NULL,1,0,0),(961,960,8,'�Չ�','t3�3',NULL,NULL,'4166938f',NULL,1,0,0),(962,958,1,'��ڃ','��\0y',NULL,NULL,'685b17f3',NULL,1,0,0),(963,962,8,'��Ef','_8?8',NULL,NULL,'64d42536',NULL,1,0,0),(964,NULL,1,'�1g�','�!\\�',NULL,1561411754,'ff82655c',49,1,0,0),(965,964,1,'H��','3�E?',NULL,NULL,'894f3dd6',NULL,1,0,0),(966,965,1,'�`B\'','R�d�',NULL,NULL,'1c6f52aa',NULL,1,0,0),(967,966,1,'ԅ=�','pէ�',NULL,NULL,'1af2ccaa',NULL,1,0,0),(968,967,1,'-�AJ','�<Y',NULL,NULL,'a3643422',NULL,1,0,0),(969,968,8,'tB	','i��',NULL,NULL,'77947866',NULL,1,0,0),(970,965,1,'����','i��r',NULL,NULL,'010f7aa5',NULL,1,0,0),(971,970,1,'򷻀','��+�',NULL,NULL,'3b92b8ba',NULL,1,0,0),(972,971,1,'R��','c�\nf',NULL,NULL,'76d00cc8',NULL,1,0,0),(973,972,1,'���S','\\��',NULL,NULL,'fb993a81',NULL,1,0,0),(974,973,8,'��!H','�r\'',NULL,NULL,'c79cfb3f',NULL,1,0,0),(975,971,1,'8�\Z','�qg_',NULL,NULL,'5d530b7c',NULL,1,0,0),(976,975,1,'����','Vrq�',NULL,NULL,'633c9f20',NULL,1,0,0),(977,976,8,'J�vZ','|gxm',NULL,NULL,'8a860779',NULL,1,0,0),(978,971,1,'-�ʺ','I���',NULL,NULL,'4099b62a',NULL,1,0,0),(979,978,1,'���\0','ڭlS',NULL,NULL,'971a0862',NULL,1,0,0),(980,979,8,'v�','(=��',NULL,NULL,'83bf4207',NULL,1,0,0),(981,971,1,'�hy','�k��',NULL,NULL,'9ab5f244',NULL,1,0,0),(982,981,1,'�u�','�zIq',NULL,NULL,'081534f5',NULL,1,0,0),(983,982,8,'X,mf','אI�',NULL,NULL,'c2570264',NULL,1,0,0),(984,971,1,'	��x','`��}',NULL,NULL,'67001779',NULL,1,0,0),(985,984,1,'��:','k9RH',NULL,NULL,'a291592d',NULL,1,0,0),(986,985,8,'ݨ�','�T',NULL,NULL,'3c8d0f10',NULL,1,0,0),(987,971,1,'�]{�','H�%',NULL,NULL,'072f88bc',NULL,1,0,0),(988,987,1,'���','���',NULL,NULL,'6b1ac5ad',NULL,1,0,0),(989,988,8,'kba�','�B��',NULL,NULL,'743195d0',NULL,1,0,0),(990,NULL,1,'Wo��','�M�x',NULL,1561411755,'3c166342',50,1,0,0),(991,990,1,'6t�','Cխo',NULL,NULL,'84467189',NULL,1,0,0),(992,991,1,'{+xy','p��',NULL,NULL,'98713af4',NULL,1,0,0),(993,992,1,'ZO��','Գ��',NULL,NULL,'4fd2dcb9',NULL,1,0,0),(994,993,1,'�<��','��b9',NULL,NULL,'f3af0289',NULL,1,0,0),(995,994,8,'��v�','y�2',NULL,NULL,'2bd6762b',NULL,1,0,0),(996,991,1,'�!��','��`�',NULL,NULL,'fb682b6a',NULL,1,0,0),(997,996,1,'1YF@','m�',NULL,NULL,'163f6d68',NULL,1,0,0),(998,997,1,'�B�','��7/',NULL,NULL,'b2c21113',NULL,1,0,0),(999,998,8,'��St','���',NULL,NULL,'59319b4a',NULL,1,0,0),(1000,991,1,'i �','[\\_',NULL,NULL,'98d0238a',NULL,1,0,0),(1001,1000,1,'i��p','յgU',NULL,NULL,'499b94ac',NULL,1,0,0),(1002,1001,1,'K���','�k��',NULL,NULL,'87b66863',NULL,1,0,0),(1003,1002,8,'�$�\\',';&_�',NULL,NULL,'2704c390',NULL,1,0,0),(1004,991,1,'$��','�Ë]',NULL,NULL,'60a422dd',NULL,1,0,0),(1005,1004,1,'`k:�','��(=',NULL,NULL,'c4308224',NULL,1,0,0),(1006,1005,1,'&gM�','Ǣ��',NULL,NULL,'d879bf42',NULL,1,0,0),(1007,1006,8,'ISb','lj=�',NULL,NULL,'1357ff4b',NULL,1,0,0),(1008,NULL,1,'$��','d\\%�',NULL,1561411756,'cb8d3fc6',51,1,0,0),(1009,1008,1,'6�','-U�#',NULL,NULL,'84c8abc0',NULL,1,0,0),(1010,1009,1,'��D','����',NULL,NULL,'acaf4a77',NULL,1,0,0),(1011,1010,1,'����','����',NULL,NULL,'cbf7d3f5',NULL,1,0,0),(1012,1011,1,'E|�','A),�',NULL,NULL,'99f4afdd',NULL,1,0,0),(1013,1012,8,'���\0',';�U�',NULL,NULL,'220fa213',NULL,1,0,0),(1014,1009,1,'#�(','�؇*',NULL,NULL,'8d8890fa',NULL,1,0,0),(1015,1014,1,'�p��','U�	',NULL,NULL,'5628323c',NULL,1,0,0),(1016,1015,1,'Z��X','�g��',NULL,NULL,'05c80cf5',NULL,1,0,0),(1017,1016,8,'m�3S','(o1,',NULL,NULL,'b1837382',NULL,1,0,0),(1018,1009,1,'@�{%','���3',NULL,NULL,'4920658c',NULL,1,0,0),(1019,1018,1,'�u�','`t�\n',NULL,NULL,'113dd187',NULL,1,0,0),(1020,1019,1,')K��','����',NULL,NULL,'1a2d5b75',NULL,1,0,0),(1021,1020,8,'YB','�u�',NULL,NULL,'f0b957f6',NULL,1,0,0),(1022,1009,1,'X\n��','��I�',NULL,NULL,'a9431759',NULL,1,0,0),(1023,1022,1,'�Pp�','i��Z',NULL,NULL,'412c621f',NULL,1,0,0),(1024,1023,1,'�`�','@G�\r',NULL,NULL,'1fb9757c',NULL,1,0,0),(1025,1024,8,'U�v$','E��',NULL,NULL,'35a7b1d7',NULL,1,0,0),(1026,1009,1,'�\"JU','�A�',NULL,NULL,'0fbb9d02',NULL,1,0,0),(1027,1026,1,'\"��<','J��M',NULL,NULL,'9f817673',NULL,1,0,0),(1028,1027,1,'\Z���','��X',NULL,NULL,'d89c6ad3',NULL,1,0,0),(1029,1028,8,'sϹ�','z��',NULL,NULL,'47cc7509',NULL,1,0,0),(1030,1009,1,'�/�','l�Gv',NULL,NULL,'dba4f816',NULL,1,0,0),(1031,1030,1,'TY|r','x�P�',NULL,NULL,'0fb4930f',NULL,1,0,0),(1032,1031,1,'�$��','͹\0p',NULL,NULL,'ac2dd697',NULL,1,0,0),(1033,1032,8,'�L','��\Z',NULL,NULL,'07b01c69',NULL,1,0,0),(1034,NULL,1,'��#�','h\\x',NULL,1561411757,'d126bfc3',52,1,0,0),(1035,1034,1,'�U�r','�\r1-',NULL,NULL,'156dc9a2',NULL,1,0,0),(1036,1035,1,'j�K$','�dg�',NULL,NULL,'58896133',NULL,1,0,0),(1037,1036,1,'ǧ�','�/�',NULL,NULL,'cd7290bd',NULL,1,0,0),(1038,1037,1,'ª�2',':�� ',NULL,NULL,'b32d5295',NULL,1,0,0),(1039,1038,8,'�!�','��{�',NULL,NULL,'773032d4',NULL,1,0,0),(1040,1035,1,'d�Q�','��Do',NULL,NULL,'dd0c4780',NULL,1,0,0),(1041,1040,1,'�_,','5��',NULL,NULL,'4bbf2f9f',NULL,1,0,0),(1042,1041,1,'�/�','m�',NULL,NULL,'7d6308cc',NULL,1,0,0),(1043,1042,8,'Go','u�\"m',NULL,NULL,'3faa6c8f',NULL,1,0,0),(1044,1035,1,'p4�','�R2',NULL,NULL,'8b86336f',NULL,1,0,0),(1045,1044,1,'��]w','ځ�',NULL,NULL,'21d792ac',NULL,1,0,0),(1046,1045,1,'���','g��',NULL,NULL,'a5faa6d0',NULL,1,0,0),(1047,1046,8,' �hm','��R',NULL,NULL,'a2a011a4',NULL,1,0,0),(1048,NULL,1,'z|L','���W',NULL,1561411757,'77c4037a',53,1,0,0),(1049,1048,1,'h�T�','�V',NULL,NULL,'dff49c50',NULL,1,0,0),(1050,1049,1,'���?','G�I',NULL,NULL,'3f6dcf2f',NULL,1,0,0),(1051,1050,1,'T�<�','�\ZH�',NULL,NULL,'5aa63f9a',NULL,1,0,0),(1052,1051,1,'+So�','2�&',NULL,NULL,'771d4cbd',NULL,1,0,0),(1053,1052,8,'ƳJ','k}',NULL,NULL,'58b28d8c',NULL,1,0,0),(1054,1049,1,'��W@','5��)',NULL,NULL,'d0459c21',NULL,1,0,0),(1055,1054,1,'���','��',NULL,NULL,'57093133',NULL,1,0,0),(1056,1055,1,'��в','&�LC',NULL,NULL,'d4b9b3cb',NULL,1,0,0),(1057,1056,8,'f5','\'�Z�',NULL,NULL,'3400482b',NULL,1,0,0),(1058,NULL,1,'F�j','f�4',NULL,1561411758,'12d2ddc1',54,1,0,0),(1059,1058,1,'\r!�','�՝?',NULL,NULL,'650c19f9',NULL,1,0,0),(1060,1059,1,'�1q!','R&T`',NULL,NULL,'8fb9aa28',NULL,1,0,0),(1061,1060,1,'%X�i','�@',NULL,NULL,'ddf1230c',NULL,1,0,0),(1062,1061,1,'_7�','�ci',NULL,NULL,'688a465b',NULL,1,0,0),(1063,1062,8,'��','4��E',NULL,NULL,'b0d53272',NULL,1,0,0),(1064,1058,1,'K\r�)','�f�',NULL,NULL,'334813c3',NULL,1,0,0),(1065,1064,1,'��+','8�f',NULL,NULL,'94b37800',NULL,1,0,0),(1066,1065,1,'orK�','2�',NULL,NULL,'5b63bb8d',NULL,1,0,0),(1067,1066,1,'���\Z','���',NULL,NULL,'c9c17cf4',NULL,1,0,0),(1068,1067,8,'\0=��','��w',NULL,NULL,'844815ca',NULL,1,0,0),(1069,1058,1,'a���','%i',NULL,NULL,'d595b904',NULL,1,0,0),(1070,1069,1,'gQ','�\Z� ',NULL,NULL,'11947dd2',NULL,1,0,0),(1071,1070,1,'�\03','hٍ�',NULL,NULL,'14814314',NULL,1,0,0),(1072,1071,1,'��G�','q�q�',NULL,NULL,'0c8a8390',NULL,1,0,0),(1073,1072,8,'V]�c','����',NULL,NULL,'842cb021',NULL,1,0,0),(1074,1058,1,'kZ��','���',NULL,NULL,'a6568fcf',NULL,1,0,0),(1075,1074,1,'�It_','P� ',NULL,NULL,'2c8a0954',NULL,1,0,0),(1076,1075,1,'�a�f','�u�r',NULL,NULL,'4f095979',NULL,1,0,0),(1077,1076,1,'�O|2','�,�',NULL,NULL,'3094386b',NULL,1,0,0),(1078,1077,8,'-�t�','��i',NULL,NULL,'43f2afdd',NULL,1,0,0),(1079,NULL,1,'�&s&','Bj�',NULL,1561411759,'2f8b22c1',55,1,0,0),(1080,1079,1,'��G�',' �5�',NULL,NULL,'368a1aab',NULL,1,0,0),(1081,1080,1,'I�+','v��',NULL,NULL,'8b415b93',NULL,1,0,0),(1082,1081,1,'&�H','�l��',NULL,NULL,'d2a2fc3a',NULL,1,0,0),(1083,1082,1,'&ɍ�','mEI�',NULL,NULL,'5cf7f79d',NULL,1,0,0),(1084,1083,8,'Q�','���',NULL,NULL,'cb192fdf',NULL,1,0,0),(1085,1079,1,'�X��','\"LT\r',NULL,NULL,'f21f8a02',NULL,1,0,0),(1086,1085,1,'�fS_','v3�E',NULL,NULL,'51aa2a28',NULL,1,0,0),(1087,1086,1,'OϚ�','���;',NULL,NULL,'63af803f',NULL,1,0,0),(1088,1087,1,'_-�','Ь�',NULL,NULL,'59523710',NULL,1,0,0),(1089,1088,8,'��','���O',NULL,NULL,'c25986cc',NULL,1,0,0),(1090,1079,1,'\Z�\n',']\'�',NULL,NULL,'5da88078',NULL,1,0,0),(1091,1090,1,'�u9C','J��c',NULL,NULL,'5ddbcbad',NULL,1,0,0),(1092,1091,1,'DQ�&','�ا�',NULL,NULL,'1d1a0f9f',NULL,1,0,0),(1093,1092,1,'=C;�','7�\'',NULL,NULL,'8c89f903',NULL,1,0,0),(1094,1093,8,'7�6B','__\n�',NULL,NULL,'f12c2c11',NULL,1,0,0),(1095,1079,1,'E��','b�3J',NULL,NULL,'9412d1d6',NULL,1,0,0),(1096,1095,1,'54\"�','��M�',NULL,NULL,'814f3a86',NULL,1,0,0),(1097,1096,1,'��-�','(�6Y',NULL,NULL,'d4cb0093',NULL,1,0,0),(1098,1097,1,'��Z','<��',NULL,NULL,'53a5483b',NULL,1,0,0),(1099,1098,8,'Q��','(�8�',NULL,NULL,'4a17df0f',NULL,1,0,0),(1100,1079,1,'���','�\'�',NULL,NULL,'c0108179',NULL,1,0,0),(1101,1100,1,'���;','��A�',NULL,NULL,'8f0f3103',NULL,1,0,0),(1102,1101,1,'�X�j','�k ',NULL,NULL,'c9f3f245',NULL,1,0,0),(1103,1102,1,'�OC','-I�',NULL,NULL,'74369784',NULL,1,0,0),(1104,1103,8,'z%P�',':z�\n',NULL,NULL,'ba805548',NULL,1,0,0),(1105,1079,1,'Kh��','��',NULL,NULL,'0b4f63c5',NULL,1,0,0),(1106,1105,1,'�p','�Z��',NULL,NULL,'b4fab68f',NULL,1,0,0),(1107,1106,1,'�@�A','�ɫK',NULL,NULL,'c08cf6a1',NULL,1,0,0),(1108,1107,1,'U��p','���p',NULL,NULL,'0ba10900',NULL,1,0,0),(1109,1108,8,'8��','�q��',NULL,NULL,'4664229f',NULL,1,0,0),(1110,NULL,1,'-�2V','f�W�',NULL,1561411760,'6600ab8c',56,1,0,0),(1111,1110,1,'Jv�','���',NULL,NULL,'6fbcad5a',NULL,1,0,0),(1112,1111,1,'�OV�','��\Za',NULL,NULL,'2d2a0cd8',NULL,1,0,0),(1113,1112,1,'|@','�%��',NULL,NULL,'61cd4fa7',NULL,1,0,0),(1114,1113,1,'-��M','G��',NULL,NULL,'7490d0cb',NULL,1,0,0),(1115,1114,8,'��','�;�e',NULL,NULL,'752f0d6d',NULL,1,0,0),(1116,1110,1,'xo��','\r_7O',NULL,NULL,'1582aad0',NULL,1,0,0),(1117,1116,1,'(�P�','X�J',NULL,NULL,'895275c7',NULL,1,0,0),(1118,1117,1,'B���','za��',NULL,NULL,'91a721c5',NULL,1,0,0),(1119,1118,1,'�T_',',�)p',NULL,NULL,'9f8c763a',NULL,1,0,0),(1120,1119,8,'��W','��S',NULL,NULL,'8a039872',NULL,1,0,0),(1121,1110,1,'��+U','�s�',NULL,NULL,'7dd4577c',NULL,1,0,0),(1122,1121,1,'��','I�',NULL,NULL,'73c11b97',NULL,1,0,0),(1123,1122,1,'L6jV','v� �',NULL,NULL,'da3f9c9c',NULL,1,0,0),(1124,1123,1,'�Hg','\0i�/',NULL,NULL,'b9b6d636',NULL,1,0,0),(1125,1124,8,'�ĉ','`�Y`',NULL,NULL,'8dd6a393',NULL,1,0,0),(1126,NULL,1,'���','�4f',NULL,1561411761,'a9409b7f',57,1,0,0),(1127,1126,1,'\'�%�','Git',NULL,NULL,'9db13b73',NULL,1,0,0),(1128,1127,1,'�\\�K','�&)@',NULL,NULL,'f23bbb37',NULL,1,0,0),(1129,1128,1,'\"o`�','��T',NULL,NULL,'2c45cac9',NULL,1,0,0),(1130,1129,1,'*P��','��-�',NULL,NULL,'841587c6',NULL,1,0,0),(1131,1130,8,'�tPu','�{T2',NULL,NULL,'1a40a0cc',NULL,1,0,0),(1132,1126,1,'<�','�o0�',NULL,NULL,'7b6fa5a9',NULL,1,0,0),(1133,1132,1,'�f','�(�?',NULL,NULL,'039648f5',NULL,1,0,0),(1134,1133,1,'��A_','6x�',NULL,NULL,'68a8fcb1',NULL,1,0,0),(1135,1134,8,'�j�(','-;�',NULL,NULL,'60a91b72',NULL,1,0,0),(1136,NULL,1,'�\'�\n','q��p',NULL,1561411761,'63271415',58,1,0,0),(1137,1136,1,'�1�','h�Ԣ',NULL,NULL,'99c25153',NULL,1,0,0),(1138,1137,1,'��a','5�|�',NULL,NULL,'848b2275',NULL,1,0,0),(1139,1138,1,'�})','-/_',NULL,NULL,'579392d3',NULL,1,0,0),(1140,1139,1,'�M\0R','Vʅ�',NULL,NULL,'995d070a',NULL,1,0,0),(1141,1140,8,'��','{/Ty',NULL,NULL,'3935a374',NULL,1,0,0),(1142,NULL,1,'�#Ӕ','wJ5',NULL,1561411761,'2a22cff4',59,1,0,0),(1143,1142,1,'׍|}','�+�',NULL,NULL,'1650b22a',NULL,1,0,0),(1144,1143,1,'s��','�hR?',NULL,NULL,'2814f807',NULL,1,0,0),(1145,1144,1,'�j-','y�C',NULL,NULL,'5d9b651a',NULL,1,0,0),(1146,1145,8,'���{','���',NULL,NULL,'70b6a5b7',NULL,1,0,0),(1147,1145,1,'Ic�p','H�c:',NULL,NULL,'2b1f9bbd',NULL,1,0,0),(1148,1147,8,'��p�','&bfi',NULL,NULL,'64d697cf',NULL,1,0,0),(1149,1144,1,'�!�','fx',NULL,NULL,'90fcc3d9',NULL,1,0,0),(1150,1149,8,'�]','{`q�',NULL,NULL,'c4bff540',NULL,1,0,0),(1151,1149,1,'M��\n','�{�',NULL,NULL,'c284b79d',NULL,1,0,0),(1152,1151,8,'�$b�','sO�',NULL,NULL,'31108864',NULL,1,0,0),(1153,1144,1,'a$2-','��yg',NULL,NULL,'b49215b6',NULL,1,0,0),(1154,1153,8,'+��=','\r��',NULL,NULL,'f2df000b',NULL,1,0,0),(1155,1153,1,'\"�[','��to',NULL,NULL,'dab915d0',NULL,1,0,0),(1156,1155,8,'���H','qzHX',NULL,NULL,'68ab6043',NULL,1,0,0),(1157,1144,1,'��','�i�',NULL,NULL,'3096dfc4',NULL,1,0,0),(1158,1157,8,'cI�<','ۻq�',NULL,NULL,'0bb8a47a',NULL,1,0,0),(1159,1157,1,'��Q','��$�',NULL,NULL,'9a2ccb15',NULL,1,0,0),(1160,1159,8,'��2S','�U�',NULL,NULL,'15a9dd6f',NULL,1,0,0),(1161,1144,1,'��S','p�ݛ',NULL,NULL,'a6f13380',NULL,1,0,0),(1162,1161,8,';#]\\','P�1',NULL,NULL,'bc2d0f8f',NULL,1,0,0),(1163,1161,1,'��O<',',e0�',NULL,NULL,'6b1a55d5',NULL,1,0,0),(1164,1163,8,'���S','I�{',NULL,NULL,'9aa095f5',NULL,1,0,0),(1165,NULL,1,'a��y','�Y}',NULL,1561411762,'54b635bb',60,1,0,0),(1166,1165,1,'�0�','O�3',NULL,NULL,'d03805c5',NULL,1,0,0),(1167,1166,1,'��\n','zV�4',NULL,NULL,'c98f84fc',NULL,1,0,0),(1168,1167,1,'�X�9','R��',NULL,NULL,'f6341337',NULL,1,0,0),(1169,1168,8,'���','�g�',NULL,NULL,'77fc7a30',NULL,1,0,0),(1170,1168,1,'}|�','��;�',NULL,NULL,'ad1795ff',NULL,1,0,0),(1171,1170,8,'+t\\�',',�*�',NULL,NULL,'82f26f7a',NULL,1,0,0),(1172,1165,1,'qTу','�XD',NULL,NULL,'037bc51b',NULL,1,0,0),(1173,1172,1,'�hD�','sy�',NULL,NULL,'7dfd901b',NULL,1,0,0),(1174,1173,1,'���=','i)/�',NULL,NULL,'8a867d7f',NULL,1,0,0),(1175,1174,1,'ڀU�','-�R8',NULL,NULL,'badc5b97',NULL,1,0,0),(1176,1175,8,'��Rg','�-)',NULL,NULL,'a8183b76',NULL,1,0,0),(1177,1175,1,'|!�-','����',NULL,NULL,'1c3b4054',NULL,1,0,0),(1178,1177,8,'���','#�',NULL,NULL,'59cf34f7',NULL,1,0,0),(1179,1174,1,'\"��e','g�1',NULL,NULL,'5c598886',NULL,1,0,0),(1180,1179,8,'/���','iB��',NULL,NULL,'f117faac',NULL,1,0,0),(1181,1179,1,'�\nx','4�9',NULL,NULL,'cc54f9dc',NULL,1,0,0),(1182,1181,8,'چ�L','@�j',NULL,NULL,'92869179',NULL,1,0,0),(1183,1174,1,'ey','F\'��',NULL,NULL,'b80c8049',NULL,1,0,0),(1184,1183,8,'��r�','��Ab',NULL,NULL,'27f4133b',NULL,1,0,0),(1185,1183,1,'R_W}','[�ơ',NULL,NULL,'2c7311ca',NULL,1,0,0),(1186,1185,8,'t���','k)��',NULL,NULL,'31dc8f9d',NULL,1,0,0),(1187,1174,1,'�ˁ�','+��#',NULL,NULL,'d8cf7470',NULL,1,0,0),(1188,1187,8,'�*{','��Q�',NULL,NULL,'85910378',NULL,1,0,0),(1189,1187,1,'&A��','rt\'<',NULL,NULL,'bf392739',NULL,1,0,0),(1190,1189,8,'�v�\'','t2�3',NULL,NULL,'80d23999',NULL,1,0,0),(1191,1174,1,'��O\'','�C�o',NULL,NULL,'4d7fc186',NULL,1,0,0),(1192,1191,8,'8qB�','d�#',NULL,NULL,'f942cd5c',NULL,1,0,0),(1193,1191,1,'kOw�','�D��',NULL,NULL,'18a9d7a0',NULL,1,0,0),(1194,1193,8,'J���','8H',NULL,NULL,'57d9bc91',NULL,1,0,0),(1195,1165,1,'}�T','IRiD',NULL,NULL,'02b44d89',NULL,1,0,0),(1196,1195,1,'e�ui',')j��',NULL,NULL,'8a5406cf',NULL,1,0,0),(1197,1196,1,'�2��','�Z@�',NULL,NULL,'7f4242c1',NULL,1,0,0),(1198,1197,8,'��m','T\'�',NULL,NULL,'9038720c',NULL,1,0,0),(1199,1165,1,'�\\_&','e��',NULL,NULL,'20912375',NULL,1,0,0),(1200,1199,8,'oB�{','W��K',NULL,NULL,'3287fd39',NULL,1,0,0),(1201,NULL,1,'��z�','Hi�',NULL,1561411763,'f0f9b702',61,1,0,0),(1202,1201,1,'4�x','��|�',NULL,NULL,'aaa4b520',NULL,1,0,0),(1203,1202,1,'�j','���',NULL,NULL,'dfabd640',NULL,1,0,0),(1204,1203,1,'�i�','���',NULL,NULL,'d2972c90',NULL,1,0,0),(1205,1204,8,'CkK','�=',NULL,NULL,'f92a9573',NULL,1,0,0),(1206,1204,1,'D�\0;','<`pj',NULL,NULL,'15466240',NULL,1,0,0),(1207,1206,8,'��c','��6',NULL,NULL,'c36f90ad',NULL,1,0,0),(1208,NULL,1,'�]\n�','�J,�',NULL,1561411764,'10a51c69',62,1,0,0),(1209,1208,1,'K��l','�=�',NULL,NULL,'c3b3cdac',NULL,1,0,0),(1210,1209,1,')ڐ�','2]e�',NULL,NULL,'6690a3ba',NULL,1,0,0),(1211,1210,1,'T-','Z٭g',NULL,NULL,'2b7fba11',NULL,1,0,0),(1212,1211,8,'�c`v','_w�',NULL,NULL,'146f407c',NULL,1,0,0),(1213,1211,8,',\\�m','���',NULL,NULL,'cc2436a7',NULL,1,0,0),(1214,NULL,1,'\r*]S',')�*\'',NULL,1561411764,'6a167857',63,1,0,0),(1215,1214,1,'2g','@@�m',NULL,NULL,'dcb8d1b2',NULL,1,0,0),(1216,1215,1,'�F�r','(�',NULL,NULL,'68a0c311',NULL,1,0,0),(1217,1216,1,'�ǫL','� �:',NULL,NULL,'5a58a0d9',NULL,1,0,0),(1218,1217,8,'7�͍','��;G',NULL,NULL,'03bbb13b',NULL,1,0,0),(1219,1216,1,'l7��','2�y',NULL,NULL,'aa71a36c',NULL,1,0,0),(1220,1219,1,'�b','�s',NULL,NULL,'947cf975',NULL,1,0,0),(1221,1220,1,'1���','-�\nr',NULL,NULL,'a66b9f53',NULL,1,0,0),(1222,1221,8,'p�+','S-P�',NULL,NULL,'32f86d6c',NULL,1,0,0),(1223,NULL,1,'v$�','F(w�',NULL,1561411764,'94d92fa7',64,1,0,0),(1224,1223,1,'	',']���',NULL,NULL,'7d0a012b',NULL,1,0,0),(1225,1224,1,'WZσ','۳��',NULL,NULL,'fa9738f9',NULL,1,0,0),(1226,1225,1,'Zݔ�','Bl��',NULL,NULL,'fc822aa8',NULL,1,0,0),(1227,1226,8,'G\\i�','`�R�',NULL,NULL,'9273fdf7',NULL,1,0,0),(1228,1225,1,':�@','���',NULL,NULL,'26fc5c90',NULL,1,0,0),(1229,1228,1,'��PH','{Y��',NULL,NULL,'5b85dcda',NULL,1,0,0),(1230,1229,8,'���f','�K',NULL,NULL,'6b2fac82',NULL,1,0,0),(1231,1228,8,';���','���@',NULL,NULL,'99cca092',NULL,1,0,0),(1232,1228,1,'�R','� `�',NULL,NULL,'54113077',NULL,1,0,0),(1233,1232,8,'���<','x��I',NULL,NULL,'0da0506c',NULL,1,0,0),(1234,1228,8,'b�֘','�w<',NULL,NULL,'f02b8d32',NULL,1,0,0),(1235,1228,1,'!2[','m�',NULL,NULL,'69c0cbf6',NULL,1,0,0),(1236,1235,8,'�M�M','��� ',NULL,NULL,'113adb49',NULL,1,0,0),(1237,NULL,1,'�j�V','��8�',NULL,1561411765,'b229738b',65,1,0,0),(1238,1237,1,'�]�:',',JY�',NULL,NULL,'c819517b',NULL,1,0,0),(1239,1238,1,'<GC�','��<�',NULL,NULL,'40650c97',NULL,1,0,0),(1240,1239,1,'��9�','���',NULL,NULL,'5b2471d8',NULL,1,0,0),(1241,1240,8,'�E�U','��}�',NULL,NULL,'a39ca87c',NULL,1,0,0),(1242,1239,1,'B��','�ƨQ',NULL,NULL,'cd8aa911',NULL,1,0,0),(1243,1242,1,'��t�','S�',NULL,NULL,'4bf7d89a',NULL,1,0,0),(1244,1243,8,'���*','\\�-B',NULL,NULL,'32dc2033',NULL,1,0,0),(1245,1242,8,'���|','�)z',NULL,NULL,'8d0af950',NULL,1,0,0),(1246,1242,1,'=�`�','��|',NULL,NULL,'251fc6ab',NULL,1,0,0),(1247,1246,8,'/�\0','b��',NULL,NULL,'f42969b2',NULL,1,0,0),(1248,NULL,1,'\r�1','\r�;',NULL,1561411765,'082a754b',66,1,0,0),(1249,1248,1,'8�|','(�g�',NULL,NULL,'ccb82306',NULL,1,0,0),(1250,1249,1,'!��t','��m',NULL,NULL,'9261569b',NULL,1,0,0),(1251,1250,1,'2h��','�+F',NULL,NULL,'8b22c2f9',NULL,1,0,0),(1252,1251,8,'?�','T�',NULL,NULL,'8b06b339',NULL,1,0,0),(1253,1250,1,'�7�','�[�\Z',NULL,NULL,'b495218a',NULL,1,0,0),(1254,1253,1,'̡0�','C�',NULL,NULL,'73d67d16',NULL,1,0,0),(1255,1254,8,'�jz	','�ص�',NULL,NULL,'1cd9c4f6',NULL,1,0,0),(1256,NULL,1,')�','��Q',NULL,1561411766,'9b3f90b6',67,1,0,0),(1257,1256,1,'����','ۂ�',NULL,NULL,'a35cfd59',NULL,1,0,0),(1258,1257,1,'z���','P$س',NULL,NULL,'3519d6df',NULL,1,0,0),(1259,1258,1,'D�M','�P�3',NULL,NULL,'91dcab5a',NULL,1,0,0),(1260,1259,1,'T7I','CK[m',NULL,NULL,'8979505c',NULL,1,0,0),(1261,1260,8,'\\�|�','6\nLj',NULL,NULL,'0c7c251b',NULL,1,0,0),(1262,1257,1,'#���','�kv�',NULL,NULL,'8aba3855',NULL,1,0,0),(1263,1262,1,'`}yI','=��{',NULL,NULL,'ca51cd42',NULL,1,0,0),(1264,1263,1,'�p@T','y*ۦ',NULL,NULL,'630a34b6',NULL,1,0,0),(1265,1264,1,'$�?\0','@g�)',NULL,NULL,'a9827772',NULL,1,0,0),(1266,1265,1,'�j','%գj',NULL,NULL,'69553f4b',NULL,1,0,0),(1267,1266,8,'MA�\\','VU�E',NULL,NULL,'3999fa67',NULL,1,0,0),(1268,1262,1,'}q�','6mBe',NULL,NULL,'5fcbda12',NULL,1,0,0),(1269,1268,1,'���z','�',NULL,NULL,'810f9b1b',NULL,1,0,0),(1270,1269,1,'�*6f','��yK',NULL,NULL,'a6060376',NULL,1,0,0),(1271,1270,1,'s�t�','�M�T',NULL,NULL,'d723f556',NULL,1,0,0),(1272,1271,8,'���1','A�[�',NULL,NULL,'5a44079b',NULL,1,0,0),(1273,1262,1,'�,\0�','��7',NULL,NULL,'825558ff',NULL,1,0,0),(1274,1273,1,'�`�','�j�',NULL,NULL,'4917d079',NULL,1,0,0),(1275,1274,1,'�0�','���',NULL,NULL,'773648db',NULL,1,0,0),(1276,1275,1,'��}�','V��&',NULL,NULL,'8aaa0d32',NULL,1,0,0),(1277,1276,8,'{�{�','E�J',NULL,NULL,'f8286a7a',NULL,1,0,0),(1278,1262,1,'���','g!�X',NULL,NULL,'a83d3766',NULL,1,0,0),(1279,1278,1,'\"1v','��OV',NULL,NULL,'fc9a3344',NULL,1,0,0),(1280,1279,1,'�0�','��3�',NULL,NULL,'0746901f',NULL,1,0,0),(1281,1280,1,'Y�c�','E�y�',NULL,NULL,'d61a84a0',NULL,1,0,0),(1282,1281,8,'�7\"0','=��',NULL,NULL,'a81137b1',NULL,1,0,0),(1283,1256,1,'�+8','q�%$',NULL,NULL,'fb62f374',NULL,1,0,0),(1284,1283,1,'�0D','�\'��',NULL,NULL,'9d3843a5',NULL,1,0,0),(1285,1284,1,';�O�','o�[)',NULL,NULL,'7d6347f5',NULL,1,0,0),(1286,1285,1,'���','�`��',NULL,NULL,'aa43a6f4',NULL,1,0,0),(1287,1286,8,'ME\r8','ep:�',NULL,NULL,'48d015d1',NULL,1,0,0),(1288,1283,1,'0r�t','GC��',NULL,NULL,'4760059d',NULL,1,0,0),(1289,1288,1,'+;3Y','��v',NULL,NULL,'d3c2251c',NULL,1,0,0),(1290,1289,1,';Q4','����',NULL,NULL,'98a85b69',NULL,1,0,0),(1291,1290,1,'�	','�KU',NULL,NULL,'791bf285',NULL,1,0,0),(1292,1291,1,'ӓ�','e���',NULL,NULL,'a761db3c',NULL,1,0,0),(1293,1292,8,'����','ݟ� ',NULL,NULL,'4c6cca05',NULL,1,0,0),(1294,1288,1,'t�1','�\\�',NULL,NULL,'4f1b7dd4',NULL,1,0,0),(1295,1294,1,'r�\"�','���X',NULL,NULL,'fd69729b',NULL,1,0,0),(1296,1295,1,'vc�','��p	',NULL,NULL,'f6bbfc4c',NULL,1,0,0),(1297,1296,1,'��&�','���',NULL,NULL,'33a4922c',NULL,1,0,0),(1298,1297,8,'A}g�','g���',NULL,NULL,'48753035',NULL,1,0,0),(1299,1288,1,'�\"�','%�\"�',NULL,NULL,'f6291493',NULL,1,0,0),(1300,1299,1,'��R�','k��$',NULL,NULL,'db535a68',NULL,1,0,0),(1301,1300,1,'�t{M','��c7',NULL,NULL,'f142fd32',NULL,1,0,0),(1302,1301,1,'��8�','�9a�',NULL,NULL,'f67cf9ff',NULL,1,0,0),(1303,1302,8,'��H','�hL_',NULL,NULL,'cc606601',NULL,1,0,0),(1304,1256,1,'�1��','2��9',NULL,NULL,'6617a92c',NULL,1,0,0),(1305,1304,1,'�L5I','�ytU',NULL,NULL,'a893b1b7',NULL,1,0,0),(1306,1305,1,'�6�','$��',NULL,NULL,'91cb92c0',NULL,1,0,0),(1307,1306,1,'��A�','��z�',NULL,NULL,'57446235',NULL,1,0,0),(1308,1307,8,'�J�','�}�-',NULL,NULL,'14c53ccd',NULL,1,0,0),(1309,1304,1,'5p*I','%A�',NULL,NULL,'1338dbaf',NULL,1,0,0),(1310,1309,1,'��x','QbM',NULL,NULL,'f4c72401',NULL,1,0,0),(1311,1310,1,'�\0�8','0ʚ�',NULL,NULL,'286b8808',NULL,1,0,0),(1312,1311,1,'��&;','��F',NULL,NULL,'30dabbca',NULL,1,0,0),(1313,1312,1,'�C-g','#�(',NULL,NULL,'bf537bc4',NULL,1,0,0),(1314,1313,8,'����','zih�',NULL,NULL,'bbd2f25c',NULL,1,0,0),(1315,1309,1,'m�','��',NULL,NULL,'39d72986',NULL,1,0,0),(1316,1315,1,'owU','�{{a',NULL,NULL,'d9369a78',NULL,1,0,0),(1317,1316,1,'4rT','\\�b�',NULL,NULL,'58527f36',NULL,1,0,0),(1318,1317,1,'a?','�zsm',NULL,NULL,'2b1dbcfc',NULL,1,0,0),(1319,1318,8,'�	Ub','�{s�',NULL,NULL,'0f656d77',NULL,1,0,0),(1320,1309,1,'Ҷ5','�,�',NULL,NULL,'60d449b3',NULL,1,0,0),(1321,1320,1,')���','�3ǚ',NULL,NULL,'f22da7cc',NULL,1,0,0),(1322,1321,1,'�$8�','͔uf',NULL,NULL,'0f35fda2',NULL,1,0,0),(1323,1322,1,'�6','�K*#',NULL,NULL,'48a64456',NULL,1,0,0),(1324,1323,8,'&�!�','z�RF',NULL,NULL,'76a88737',NULL,1,0,0),(1325,1309,1,'F�}c','�Ya',NULL,NULL,'daa27dca',NULL,1,0,0),(1326,1325,1,'Z��','8�W2',NULL,NULL,'a7257bca',NULL,1,0,0),(1327,1326,1,'�F�<','R��',NULL,NULL,'a4683ff3',NULL,1,0,0),(1328,1327,1,'�V','y�\r�',NULL,NULL,'7231b388',NULL,1,0,0),(1329,1328,8,'����','��K',NULL,NULL,'208fa28d',NULL,1,0,0),(1330,1309,1,';i','u���',NULL,NULL,'8dd98830',NULL,1,0,0),(1331,1330,1,'�Y�','R',NULL,NULL,'76fa7365',NULL,1,0,0),(1332,1331,1,'j\Zul','�l�',NULL,NULL,'d9a77c40',NULL,1,0,0),(1333,1332,1,'���L','�3�',NULL,NULL,'3a53b504',NULL,1,0,0),(1334,1333,8,'z-\"@','�O�',NULL,NULL,'863ca4af',NULL,1,0,0),(1335,1309,1,'��WJ','wxE�',NULL,NULL,'d5a1575b',NULL,1,0,0),(1336,1335,1,'�q�','Zaf�',NULL,NULL,'cdd2c74a',NULL,1,0,0),(1337,1336,1,'KD','��\0',NULL,NULL,'97b73c83',NULL,1,0,0),(1338,1337,1,'Y7�\0','�',NULL,NULL,'33317803',NULL,1,0,0),(1339,1338,8,'l��k','W�w�',NULL,NULL,'33f0fd0d',NULL,1,0,0),(1340,1256,1,'oA �','3��',NULL,NULL,'3fd185cb',NULL,1,0,0),(1341,1340,1,'3%ڃ','�QB',NULL,NULL,'67198f19',NULL,1,0,0),(1342,1341,1,'��|',' o�',NULL,NULL,'96878b54',NULL,1,0,0),(1343,1342,1,'���$','��8',NULL,NULL,'b6ba98b0',NULL,1,0,0),(1344,1343,8,'	� ','��S',NULL,NULL,'b73383cc',NULL,1,0,0),(1345,1340,1,'�g�q','9rƢ',NULL,NULL,'6cd124f4',NULL,1,0,0),(1346,1345,1,'��','ͻ��',NULL,NULL,'ac4f13b5',NULL,1,0,0),(1347,1346,1,'t͓�','D�LU',NULL,NULL,'f1287767',NULL,1,0,0),(1348,1347,1,'C���','+��',NULL,NULL,'646d229d',NULL,1,0,0),(1349,1348,1,'V�','T�u',NULL,NULL,'a599a69f',NULL,1,0,0),(1350,1349,8,'[#�','�)�Y',NULL,NULL,'f02b536f',NULL,1,0,0),(1351,1345,1,'5�Y�','\\OɆ',NULL,NULL,'c07addbc',NULL,1,0,0),(1352,1351,1,'��#H','V@_',NULL,NULL,'33fa1bb0',NULL,1,0,0),(1353,1352,1,'����','�\\��',NULL,NULL,'cb456f9a',NULL,1,0,0),(1354,1353,1,'���\"','M5�',NULL,NULL,'59422391',NULL,1,0,0),(1355,1354,8,';���','i',NULL,NULL,'76db731c',NULL,1,0,0),(1356,1345,1,'_*I','�9',NULL,NULL,'00788bf8',NULL,1,0,0),(1357,1356,1,'(PPa','�6��',NULL,NULL,'4056481f',NULL,1,0,0),(1358,1357,1,'��eF','1h�',NULL,NULL,'3b635d92',NULL,1,0,0),(1359,1358,1,'g=�E','�v,{',NULL,NULL,'1f1ffd27',NULL,1,0,0),(1360,1359,8,'�-�',']Ԫ',NULL,NULL,'3f42c37c',NULL,1,0,0),(1361,1345,1,'��o','��b@',NULL,NULL,'7940f722',NULL,1,0,0),(1362,1361,1,'��6','�6�w',NULL,NULL,'01d5f1a6',NULL,1,0,0),(1363,1362,1,'���','x8��',NULL,NULL,'0b183d78',NULL,1,0,0),(1364,1363,1,'�G�','�d6�',NULL,NULL,'438ad943',NULL,1,0,0),(1365,1364,8,'UF��','g�a�',NULL,NULL,'fcf4d800',NULL,1,0,0),(1366,1256,1,'�w�r','�,JO',NULL,NULL,'f6d9759b',NULL,1,0,0),(1367,1366,1,'�Ǖ','%�',NULL,NULL,'a063c1f6',NULL,1,0,0),(1368,1367,1,'X�','���',NULL,NULL,'3f916276',NULL,1,0,0),(1369,1368,1,'%�','WX�',NULL,NULL,'47bddbad',NULL,1,0,0),(1370,1369,8,']z�G','\n2��',NULL,NULL,'fa7b67f1',NULL,1,0,0),(1371,NULL,1,'VG�','�7�H',NULL,1561411770,'310a0c03',68,1,0,0),(1372,1371,1,'S�\n!','���',NULL,NULL,'f877add0',NULL,1,0,0),(1373,1372,1,'�i�C','5�TD',NULL,NULL,'a58733dd',NULL,1,0,0),(1374,1373,1,'O}��','�\\_r',NULL,NULL,'a66bac6a',NULL,1,0,0),(1375,1374,8,'1�Y�','�Q�',NULL,NULL,'bb677762',NULL,1,0,0),(1376,1373,1,'��ٸ','�U%0',NULL,NULL,'d65fbd64',NULL,1,0,0),(1377,1376,8,'�I�','��G�',NULL,NULL,'d772ddc1',NULL,1,0,0),(1378,1373,1,'�$�v','@�z�',NULL,NULL,'a6b792a9',NULL,1,0,0),(1379,1378,8,'����','A��',NULL,NULL,'027a19b8',NULL,1,0,0),(1380,1373,1,'$wU3','�I8�',NULL,NULL,'38b53045',NULL,1,0,0),(1381,1380,8,'��s�','Q/�\"',NULL,NULL,'184ff093',NULL,1,0,0),(1382,1373,1,'�DqV','Dv��',NULL,NULL,'55104258',NULL,1,0,0),(1383,1382,8,'d�y�','��',NULL,NULL,'017b5d8d',NULL,1,0,0),(1384,1373,1,'��H�','����',NULL,NULL,'69d224b0',NULL,1,0,0),(1385,1384,8,'�Ov2','2X*�',NULL,NULL,'4a15751d',NULL,1,0,0),(1386,1371,1,'b�uZ','ײ��',NULL,NULL,'548b0c34',NULL,1,0,0),(1387,1386,1,'-|�\\','؁�',NULL,NULL,'c8d3c5df',NULL,1,0,0),(1388,1387,1,'wQ�_','��#',NULL,NULL,'1456b3d9',NULL,1,0,0),(1389,1388,1,'!T�','YX�i',NULL,NULL,'c8d98c57',NULL,1,0,0),(1390,1389,8,'M�\"=','l��',NULL,NULL,'53da8d1f',NULL,1,0,0),(1391,1388,1,'ڣ��',',0�x',NULL,NULL,'f1b82681',NULL,1,0,0),(1392,1391,8,'#,�F','sX1',NULL,NULL,'9559152d',NULL,1,0,0),(1393,1386,1,'�jQ','���',NULL,NULL,'1d986c91',NULL,1,0,0),(1394,1393,1,'���}','�v��',NULL,NULL,'6fb11fa8',NULL,1,0,0),(1395,1394,8,'M��','�+lf',NULL,NULL,'2bd50ac5',NULL,1,0,0),(1396,NULL,1,'P�8','�dЗ',NULL,1561411770,'d426439c',69,1,0,0),(1397,1396,1,'3��','<y�K',NULL,NULL,'0f64bd03',NULL,1,0,0),(1398,1397,1,'�;b�','Tb�c',NULL,NULL,'3f2a229d',NULL,1,0,0),(1399,1398,1,'t��F','#Vp9',NULL,NULL,'a582a8c7',NULL,1,0,0),(1400,1399,1,'�q��','�5',NULL,NULL,'1a9d3860',NULL,1,0,0),(1401,1400,8,'B�lp','��?',NULL,NULL,'d84ac887',NULL,1,0,0),(1402,1396,1,'5z�e','e��',NULL,NULL,'144fd1d4',NULL,1,0,0),(1403,1402,1,'/�1�','���',NULL,NULL,'dc0835b0',NULL,1,0,0),(1404,1403,1,'b��','Z[��',NULL,NULL,'774fbc14',NULL,1,0,0),(1405,1404,1,'1W��','��x-',NULL,NULL,'6627221c',NULL,1,0,0),(1406,1405,8,'�\r/�','����',NULL,NULL,'52f6f5d0',NULL,1,0,0),(1407,1396,1,'�U2�','�F2�',NULL,NULL,'f41626da',NULL,1,0,0),(1408,1407,1,'��V','�;�l',NULL,NULL,'2ccb46f8',NULL,1,0,0),(1409,1408,1,'�eI�','��',NULL,NULL,'cd07f413',NULL,1,0,0),(1410,1409,1,'KќX','\r��9',NULL,NULL,'b33c84bf',NULL,1,0,0),(1411,1410,8,'\'i��','g��P',NULL,NULL,'2fd0101d',NULL,1,0,0),(1412,1407,1,'��a�','B��',NULL,NULL,'d637361f',NULL,1,0,0),(1413,1412,1,'o\Z��','���6',NULL,NULL,'1233d09a',NULL,1,0,0),(1414,1413,1,'p��','�P�',NULL,NULL,'1dfa38ba',NULL,1,0,0),(1415,1414,1,'�cm�','��(j',NULL,NULL,'d64925b8',NULL,1,0,0),(1416,1415,1,'�\"�B','P���',NULL,NULL,'c3264652',NULL,1,0,0),(1417,1416,1,'q/,A',')X��',NULL,NULL,'b9901fca',NULL,1,0,0),(1418,1417,8,'qt','�}8y',NULL,NULL,'f9f1da07',NULL,1,0,0),(1419,1414,1,'$#�','�CTY',NULL,NULL,'1f61acfa',NULL,1,0,0),(1420,1419,1,'E�e@','Y;�',NULL,NULL,'a4d189c3',NULL,1,0,0),(1421,1420,1,'{��','M��',NULL,NULL,'69d98351',NULL,1,0,0),(1422,1421,1,')\0�i','-M� ',NULL,NULL,'b1fb5da3',NULL,1,0,0),(1423,1422,1,'�G�','9�l',NULL,NULL,'d29327c6',NULL,1,0,0),(1424,1423,8,'s��','F\0t&',NULL,NULL,'4a84bff3',NULL,1,0,0),(1425,1419,1,'oQ�','��V\0',NULL,NULL,'18aa6115',NULL,1,0,0),(1426,1425,1,'���M','�+L',NULL,NULL,'76362b85',NULL,1,0,0),(1427,1426,1,'t�W�','��?',NULL,NULL,'3421a112',NULL,1,0,0),(1428,1427,1,'�A��','\"ZWx',NULL,NULL,'f9668476',NULL,1,0,0),(1429,1428,8,'4\"��','}Ͳ,',NULL,NULL,'fdc1f1fd',NULL,1,0,0),(1430,1419,1,'wŻ�','mI��',NULL,NULL,'ad94b08c',NULL,1,0,0),(1431,1430,1,'��\0�','g��',NULL,NULL,'2bbf9543',NULL,1,0,0),(1432,1431,1,'m�x','Ho1',NULL,NULL,'1a48b608',NULL,1,0,0),(1433,1432,1,'@b�+','5Ɍ�',NULL,NULL,'6d7a5c09',NULL,1,0,0),(1434,1433,8,'&���','@B�b',NULL,NULL,'a9f54d96',NULL,1,0,0),(1435,1419,1,'i��F','��i�',NULL,NULL,'7795f350',NULL,1,0,0),(1436,1435,1,'�LY�',',�W(',NULL,NULL,'4a84db4b',NULL,1,0,0),(1437,1436,1,'f��','ӟ��',NULL,NULL,'d294aa62',NULL,1,0,0),(1438,1437,1,'ܱ��','��:�',NULL,NULL,'45511d12',NULL,1,0,0),(1439,1438,8,'<�','�@�',NULL,NULL,'89127523',NULL,1,0,0),(1440,1413,1,'<�ik','�t��',NULL,NULL,'b36500c2',NULL,1,0,0),(1441,1440,1,'�!Y','�\r',NULL,NULL,'015f83db',NULL,1,0,0),(1442,1441,1,'�W�','T��-',NULL,NULL,'1b7b16a0',NULL,1,0,0),(1443,1442,1,'h��','�?W',NULL,NULL,'2321623b',NULL,1,0,0),(1444,1443,8,'��','v/�',NULL,NULL,'56379376',NULL,1,0,0),(1445,1440,1,'���','�	�#',NULL,NULL,'8b563f6a',NULL,1,0,0),(1446,1445,1,'��SL','R9#0',NULL,NULL,'79ab3bff',NULL,1,0,0),(1447,1446,1,'���','YI�L',NULL,NULL,'b26a3bdb',NULL,1,0,0),(1448,1447,1,'��M','\r��',NULL,NULL,'c72553a5',NULL,1,0,0),(1449,1448,1,'�\"�P','mv�',NULL,NULL,'66406ad0',NULL,1,0,0),(1450,1449,8,'�{b','��&F',NULL,NULL,'62653f21',NULL,1,0,0),(1451,1445,1,'ј�x','Zc�',NULL,NULL,'ba15320c',NULL,1,0,0),(1452,1451,1,'!o\'$','��U�',NULL,NULL,'a1d9205c',NULL,1,0,0),(1453,1452,1,'�!�','�j`!',NULL,NULL,'f48aac16',NULL,1,0,0),(1454,1453,1,'���','���<',NULL,NULL,'159c2f00',NULL,1,0,0),(1455,1454,8,'�Қ','�VX',NULL,NULL,'578757ad',NULL,1,0,0),(1456,1445,1,'�Б\n','�\\�\Z',NULL,NULL,'360cbdb2',NULL,1,0,0),(1457,1456,1,'�jeC','T��',NULL,NULL,'a1790c11',NULL,1,0,0),(1458,1457,1,'�','��Z�',NULL,NULL,'6292c23f',NULL,1,0,0),(1459,1458,1,'�Sy�','��',NULL,NULL,'34a66443',NULL,1,0,0),(1460,1459,8,'�M�\'','O��',NULL,NULL,'dd5c402d',NULL,1,0,0),(1461,1413,1,'qLF�','�s�P',NULL,NULL,'6170f25b',NULL,1,0,0),(1462,1461,1,'��#�','y�@�',NULL,NULL,'a4258d52',NULL,1,0,0),(1463,1462,1,'f#��','��',NULL,NULL,'a39840b3',NULL,1,0,0),(1464,1463,1,'E%�u','wDl2',NULL,NULL,'db5b52c4',NULL,1,0,0),(1465,1464,8,'(�j','K�P�',NULL,NULL,'258d1100',NULL,1,0,0),(1466,1461,1,'�T��','u\Z:',NULL,NULL,'2bfccf21',NULL,1,0,0),(1467,1466,1,'�Wd','Yv�?',NULL,NULL,'c115fc71',NULL,1,0,0),(1468,1467,1,'�[bC','���',NULL,NULL,'8b756464',NULL,1,0,0),(1469,1468,1,'� ԅ','Q��',NULL,NULL,'249b9cb2',NULL,1,0,0),(1470,1469,1,'pW�','y�hA',NULL,NULL,'9387442d',NULL,1,0,0),(1471,1470,8,'oLЄ','��9`',NULL,NULL,'6a1a117a',NULL,1,0,0),(1472,1466,1,')tP�','V�G[',NULL,NULL,'3a073bc7',NULL,1,0,0),(1473,1472,1,'kx�\\','(�\r',NULL,NULL,'238578b2',NULL,1,0,0),(1474,1473,1,'00��','��',NULL,NULL,'15f4b1a5',NULL,1,0,0),(1475,1474,1,'��','�]',NULL,NULL,'55cbf524',NULL,1,0,0),(1476,1475,8,'�K�','J[ߌ',NULL,NULL,'dad070d1',NULL,1,0,0),(1477,1466,1,'��,','jfu',NULL,NULL,'4881599c',NULL,1,0,0),(1478,1477,1,'�zL','�Ԕ',NULL,NULL,'d6ab5923',NULL,1,0,0),(1479,1478,1,'�1L+','r:a[',NULL,NULL,'2a663528',NULL,1,0,0),(1480,1479,1,'=��B','LV\'a',NULL,NULL,'6469bc27',NULL,1,0,0),(1481,1480,8,'Y9s!','���',NULL,NULL,'233a30b5',NULL,1,0,0),(1482,1466,1,'�t��','m��j',NULL,NULL,'1d3c4b86',NULL,1,0,0),(1483,1482,1,'mQS','$T��',NULL,NULL,'8fa96f68',NULL,1,0,0),(1484,1483,1,'*H�','%��x',NULL,NULL,'31d066ff',NULL,1,0,0),(1485,1484,1,'cV��','�2[f',NULL,NULL,'a9d0043a',NULL,1,0,0),(1486,1485,8,'�2Sk','��G',NULL,NULL,'069658a4',NULL,1,0,0),(1487,1466,1,'W���','�O|',NULL,NULL,'9775da4c',NULL,1,0,0),(1488,1487,1,'!�\Z�','}f�&',NULL,NULL,'722b3010',NULL,1,0,0),(1489,1488,1,'�F%l','H��I',NULL,NULL,'27f7337c',NULL,1,0,0),(1490,1489,1,'=K�h','�ȬR',NULL,NULL,'b2a069d0',NULL,1,0,0),(1491,1490,8,')�','my\"�',NULL,NULL,'af1a2ad1',NULL,1,0,0),(1492,1466,1,'c�0G','�<;p',NULL,NULL,'710b18c8',NULL,1,0,0),(1493,1492,1,'�(�','(\Z�',NULL,NULL,'04c2aa74',NULL,1,0,0),(1494,1493,1,'�U','�Q��',NULL,NULL,'2a713537',NULL,1,0,0),(1495,1494,1,'�ח�','�b',NULL,NULL,'ba3cd420',NULL,1,0,0),(1496,1495,8,'A�b�','���A',NULL,NULL,'446f3731',NULL,1,0,0),(1497,1413,1,'ڀY:','�78{',NULL,NULL,'00c329cc',NULL,1,0,0),(1498,1497,1,'D���','��ն',NULL,NULL,'a2fbb420',NULL,1,0,0),(1499,1498,1,'�b5','8pĐ',NULL,NULL,'f14a06bc',NULL,1,0,0),(1500,1499,1,'�۠1','�Ĺ�',NULL,NULL,'f94fc909',NULL,1,0,0),(1501,1500,8,'t�U�','\n�:F',NULL,NULL,'f4bbf1f0',NULL,1,0,0),(1502,1497,1,';u9','h$J]',NULL,NULL,'096f6d86',NULL,1,0,0),(1503,1502,1,'��','�k\n�',NULL,NULL,'365f120a',NULL,1,0,0),(1504,1503,1,'_��M','5��',NULL,NULL,'252c7414',NULL,1,0,0),(1505,1504,1,'c�J�','���',NULL,NULL,'7cc02f6d',NULL,1,0,0),(1506,1505,1,'��B�','G�`',NULL,NULL,'d11711cc',NULL,1,0,0),(1507,1506,8,'(D�','�v',NULL,NULL,'1f2db526',NULL,1,0,0),(1508,1502,1,'�9�k','B��',NULL,NULL,'36d2dc3a',NULL,1,0,0),(1509,1508,1,'��c�','}7��',NULL,NULL,'c6ca48ca',NULL,1,0,0),(1510,1509,1,'`�T','?L�p',NULL,NULL,'78636f1c',NULL,1,0,0),(1511,1510,1,'l״!','�ZF�',NULL,NULL,'fc2c0815',NULL,1,0,0),(1512,1511,8,':���','\r8r�',NULL,NULL,'bd577261',NULL,1,0,0),(1513,1502,1,'X�?�','I:#z',NULL,NULL,'3a832f3d',NULL,1,0,0),(1514,1513,1,'��T4','�D\\�',NULL,NULL,'3f38ddba',NULL,1,0,0),(1515,1514,1,'V�W�','�o�?',NULL,NULL,'3f40a52b',NULL,1,0,0),(1516,1515,1,'��j','�	��',NULL,NULL,'7f27d2b8',NULL,1,0,0),(1517,1516,8,'����','�Es�',NULL,NULL,'70181452',NULL,1,0,0),(1518,1502,1,'��}�','�H',NULL,NULL,'fd516370',NULL,1,0,0),(1519,1518,1,'�:� ','��',NULL,NULL,'97c9c6f4',NULL,1,0,0),(1520,1519,1,'����','j�8�',NULL,NULL,'a37aa642',NULL,1,0,0),(1521,1520,1,'���','y\n��',NULL,NULL,'0ba4876b',NULL,1,0,0),(1522,1521,8,'ǵ|;','�!B',NULL,NULL,'35c5cfb6',NULL,1,0,0),(1523,1413,1,'cז)','�',NULL,NULL,'2d60461f',NULL,1,0,0),(1524,1523,1,'Dd1�','���',NULL,NULL,'415a3463',NULL,1,0,0),(1525,1524,1,'�59','6	0',NULL,NULL,'5fd48af4',NULL,1,0,0),(1526,1525,1,'(͵�','`�g�',NULL,NULL,'fb82d8d4',NULL,1,0,0),(1527,1526,8,'�b\\E','��|�',NULL,NULL,'2456441d',NULL,1,0,0),(1528,1396,1,'�%H','�]f',NULL,NULL,'0fb56780',NULL,1,0,0),(1529,1528,1,'&(v�','pk�X',NULL,NULL,'c936ab15',NULL,1,0,0),(1530,1529,1,'�P�','1�a�',NULL,NULL,'67850c77',NULL,1,0,0),(1531,1530,1,'� ��','h\ZRt',NULL,NULL,'763b3531',NULL,1,0,0),(1532,1531,8,'(<B',':(6�',NULL,NULL,'05551073',NULL,1,0,0),(1533,1396,1,'tڣ�','j��v',NULL,NULL,'339cf94b',NULL,1,0,0),(1534,1533,1,'O��<','��b',NULL,NULL,'83d3cab5',NULL,1,0,0),(1535,1534,1,'&��-','u-��',NULL,NULL,'da2a0310',NULL,1,0,0),(1536,1535,1,'X��R','U���',NULL,NULL,'a1cb57ff',NULL,1,0,0),(1537,1536,8,'�v<q','��˨',NULL,NULL,'2d8c5a58',NULL,1,0,0),(1538,1396,1,'B��','�Iā',NULL,NULL,'63967387',NULL,1,0,0),(1539,1538,1,'�btL','��|',NULL,NULL,'5f035ccb',NULL,1,0,0),(1540,1539,1,'�U�','�X��',NULL,NULL,'f7c5f3fc',NULL,1,0,0),(1541,1540,1,'c0!(','�P6',NULL,NULL,'06290a62',NULL,1,0,0),(1542,1541,8,'����','�@�',NULL,NULL,'b371159c',NULL,1,0,0),(1543,1396,1,'�,�\'','�W�',NULL,NULL,'df111711',NULL,1,0,0),(1544,1543,1,'/��','D�',NULL,NULL,'f8c6233b',NULL,1,0,0),(1545,1544,1,'��','G���',NULL,NULL,'1899b69c',NULL,1,0,0),(1546,1545,1,'�Ch','8�Jm',NULL,NULL,'41392732',NULL,1,0,0),(1547,1546,8,'��$�','�[F',NULL,NULL,'b43966f1',NULL,1,0,0),(1548,1396,1,'D��','��9�',NULL,NULL,'7aca6986',NULL,1,0,0),(1549,1548,1,'z$\Z\Z','?	@',NULL,NULL,'af9b212b',NULL,1,0,0),(1550,1549,1,'9��','�V��',NULL,NULL,'2714f060',NULL,1,0,0),(1551,1550,1,'p�I<','D��2',NULL,NULL,'24d99bf8',NULL,1,0,0),(1552,1551,8,'\'�v�','�8L�',NULL,NULL,'0b55681b',NULL,1,0,0),(1553,1396,1,'�@�',']�',NULL,NULL,'5f2903a3',NULL,1,0,0),(1554,1553,1,'C�s�','�M',NULL,NULL,'f9632b95',NULL,1,0,0),(1555,1554,1,'�V��','���;',NULL,NULL,'3d807491',NULL,1,0,0),(1556,1555,1,'/4Lw','r�p�',NULL,NULL,'f184637d',NULL,1,0,0),(1557,1556,8,'jl��','c�w\'',NULL,NULL,'3c8c3168',NULL,1,0,0),(1558,1396,1,'�y`|','9m?b',NULL,NULL,'4153a205',NULL,1,0,0),(1559,1558,1,'��tF','�ê',NULL,NULL,'5d787ca6',NULL,1,0,0),(1560,1559,1,'����','l��?',NULL,NULL,'1f57305b',NULL,1,0,0),(1561,1560,1,'_�|�','=D�',NULL,NULL,'fb0b4071',NULL,1,0,0),(1562,1561,8,'+W�4','��',NULL,NULL,'700c5a35',NULL,1,0,0),(1563,NULL,1,',���','|�q',NULL,1561411777,'86a2ad80',70,1,0,0),(1564,1563,1,'dJ��','���-',NULL,NULL,'2877b4d1',NULL,1,0,0),(1565,1564,1,'�I�-','ǭ�',NULL,NULL,'46f0dc42',NULL,1,0,0),(1566,1565,1,'�@+�','R�Q�',NULL,NULL,'6ac7850a',NULL,1,0,0),(1567,1566,8,'�\0{t','+F(�',NULL,NULL,'fc788cb2',NULL,1,0,0),(1568,1565,1,'b�R�','x�v�',NULL,NULL,'14494390',NULL,1,0,0),(1569,1568,8,'���z','G�\Z',NULL,NULL,'adf04935',NULL,1,0,0),(1570,1565,1,'�ߨ','Qr,E',NULL,NULL,'3fac457d',NULL,1,0,0),(1571,1570,8,';�۝','-Z<�',NULL,NULL,'8dfb3b6b',NULL,1,0,0),(1572,NULL,1,'����','$L�`',NULL,1561411777,'b0d6059d',71,1,0,0),(1573,1572,1,'��ʩ','�\\CY',NULL,NULL,'4f7ccb48',NULL,1,0,0),(1574,1573,1,'���','ݭ�j',NULL,NULL,'dcd74243',NULL,1,0,0),(1575,1574,1,'aR5k',']u2\0',NULL,NULL,'8f89d78f',NULL,1,0,0),(1576,1575,8,'X/�','q��',NULL,NULL,'274fb245',NULL,1,0,0),(1577,1574,1,'	#�t','|1��',NULL,NULL,'78146a34',NULL,1,0,0),(1578,1577,8,'E��','Ϙ��',NULL,NULL,'85302c0c',NULL,1,0,0),(1579,1572,1,'��','@)z',NULL,NULL,'f850b64f',NULL,1,0,0),(1580,1579,1,'�y�','�e�',NULL,NULL,'06b789a0',NULL,1,0,0),(1581,1580,8,'��A','K�\r',NULL,NULL,'7dd570b4',NULL,1,0,0),(1582,NULL,1,'�-0�','�%d',NULL,1561411777,'3bcd603d',72,1,0,0),(1583,1582,1,'�5OH','����',NULL,NULL,'118262a4',NULL,1,0,0),(1584,1583,1,'MF��','&���',NULL,NULL,'0c629d97',NULL,1,0,0),(1585,1584,1,'e�#�','�m�U',NULL,NULL,'75b9582c',NULL,1,0,0),(1586,1585,8,'r�;','�?�G',NULL,NULL,'3540b8ca',NULL,1,0,0),(1587,1584,1,' ?��','҃jV',NULL,NULL,'24c1a620',NULL,1,0,0),(1588,1587,8,'6V�','�-�}',NULL,NULL,'fd64956d',NULL,1,0,0),(1589,NULL,1,'ֈH�','��}�',NULL,1561411778,'09559045',73,1,0,0),(1590,1589,1,'�D��','э�',NULL,NULL,'375f6831',NULL,1,0,0),(1591,1590,1,';�','�F��',NULL,NULL,'865945b5',NULL,1,0,0),(1592,1591,1,'Bc�','R���',NULL,NULL,'2bd7cb68',NULL,1,0,0),(1593,1592,8,'w\Z�','a��',NULL,NULL,'1fd85057',NULL,1,0,0),(1594,1591,8,'�)�','QJ�',NULL,NULL,'78a2d581',NULL,1,0,0),(1595,1590,1,'d+��',' ��@',NULL,NULL,'63fb6d45',NULL,1,0,0),(1596,1595,8,'6�s�','�2ɖ',NULL,NULL,'a1713056',NULL,1,0,0),(1597,NULL,1,'gBө','�<�',NULL,1561411778,'d455b7f8',74,1,0,0),(1598,1597,1,'؜�,','c��',NULL,NULL,'f9a837f4',NULL,1,0,0),(1599,1598,1,'�֗:',' �',NULL,NULL,'1178014a',NULL,1,0,0),(1600,1599,1,'QGqV','���b',NULL,NULL,'f90177a7',NULL,1,0,0),(1601,1600,8,'����','�0�U',NULL,NULL,'b5467c02',NULL,1,0,0),(1602,1599,8,'�b�q','�q�?',NULL,NULL,'793894ba',NULL,1,0,0),(1603,1597,1,'�@�m','\n��',NULL,NULL,'c56679f7',NULL,1,0,0),(1604,1603,1,'eQ}�','�;��',NULL,NULL,'47f03522',NULL,1,0,0),(1605,1604,1,'�6hC','I	\r',NULL,NULL,'3ac935d3',NULL,1,0,0),(1606,1605,8,'�k��','ɗ�',NULL,NULL,'bc31f3f7',NULL,1,0,0),(1607,1604,8,'��','sd�v',NULL,NULL,'f5c52241',NULL,1,0,0),(1608,NULL,1,'Pz�','�F��',NULL,1561411778,'19852f1b',75,1,0,0),(1609,1608,1,'��B�','=��',NULL,NULL,'c1d40575',NULL,1,0,0),(1610,1609,1,'��Ň','z�(',NULL,NULL,'6dadff2a',NULL,1,0,0),(1611,1610,1,'�;�','AE',NULL,NULL,'b7b8983f',NULL,1,0,0),(1612,1611,8,'���+','0oEP',NULL,NULL,'61485885',NULL,1,0,0),(1613,1610,8,'��','&�u',NULL,NULL,'ffd27502',NULL,1,0,0),(1614,NULL,1,'4:�','�J��',NULL,1561411779,'c8bf0040',76,1,0,0),(1615,1614,1,'m�p�','\Z5(',NULL,NULL,'5812797a',NULL,1,0,0),(1616,1615,1,'��	','6L��',NULL,NULL,'39740116',NULL,1,0,0),(1617,1616,1,'7�l','i�be',NULL,NULL,'8c631590',NULL,1,0,0),(1618,1617,8,'\Z�b6','�K/',NULL,NULL,'f4a259f8',NULL,1,0,0),(1619,1615,1,'ŏ-@','l���',NULL,NULL,'1f15cc99',NULL,1,0,0),(1620,1619,1,'(p)i',' v',NULL,NULL,'c07f7048',NULL,1,0,0),(1621,1620,8,'��','3k�',NULL,NULL,'846119da',NULL,1,0,0),(1622,1615,1,'�3�','��M',NULL,NULL,'633a063b',NULL,1,0,0),(1623,1622,1,'8[c�','����',NULL,NULL,'835b370d',NULL,1,0,0),(1624,1623,8,'4�֜','_`�t',NULL,NULL,'24576af6',NULL,1,0,0),(1625,1615,1,'�)��','�Z��',NULL,NULL,'12d1c70c',NULL,1,0,0),(1626,1625,1,'��','�<$',NULL,NULL,'3b62412d',NULL,1,0,0),(1627,1626,8,'�Q\rp','<���',NULL,NULL,'506b21bf',NULL,1,0,0),(1628,1615,1,'���','�T=',NULL,NULL,'0c2a2286',NULL,1,0,0),(1629,1628,1,'5�','v�\Z�',NULL,NULL,'59b34690',NULL,1,0,0),(1630,1629,8,'��3�','JX',NULL,NULL,'6b3223a9',NULL,1,0,0),(1631,1615,1,'�TJO','y�W',NULL,NULL,'30635640',NULL,1,0,0),(1632,1631,1,'��5T',']gu',NULL,NULL,'77872096',NULL,1,0,0),(1633,1632,8,'����','#,h�',NULL,NULL,'4da9c35b',NULL,1,0,0),(1634,1615,1,'�2�','����',NULL,NULL,'059c44f3',NULL,1,0,0),(1635,1634,1,'rJ�','A}4�',NULL,NULL,'75267474',NULL,1,0,0),(1636,1635,8,'��','��Z1',NULL,NULL,'c224763d',NULL,1,0,0),(1637,NULL,1,'z�','�(\0�',NULL,1561411780,'14f6a62c',77,1,0,0),(1638,1637,1,'hG=','��bb',NULL,NULL,'65aa7994',NULL,1,0,0),(1639,1638,1,'d-��','��=�',NULL,NULL,'b1cb1b79',NULL,1,0,0),(1640,1639,1,'I�KF','�(�Z',NULL,NULL,'2a857ab7',NULL,1,0,0),(1641,1640,8,'��v','�,��',NULL,NULL,'85c1a227',NULL,1,0,0),(1642,1638,1,'Hl�9','�DC�',NULL,NULL,'39d98c61',NULL,1,0,0),(1643,1642,1,'�IqO','2\Z�E',NULL,NULL,'61ba8ad7',NULL,1,0,0),(1644,1643,8,'��(#','�q)�',NULL,NULL,'8409c805',NULL,1,0,0),(1645,1638,1,'i�g8','�Ԇ',NULL,NULL,'87ba0c8b',NULL,1,0,0),(1646,1645,1,'\Z���','��Q',NULL,NULL,'2cb53c83',NULL,1,0,0),(1647,1646,8,'X`��','|���',NULL,NULL,'293b7d68',NULL,1,0,0),(1648,1638,1,'���','6!(z',NULL,NULL,'b8f3599c',NULL,1,0,0),(1649,1648,1,'�Ƃ�','YV\"�',NULL,NULL,'60b50c67',NULL,1,0,0),(1650,1649,8,';=�b','v��d',NULL,NULL,'70b6d998',NULL,1,0,0),(1651,1638,1,'5��','�(o',NULL,NULL,'bc97736d',NULL,1,0,0),(1652,1651,1,'�w�M','�pf',NULL,NULL,'c121572b',NULL,1,0,0),(1653,1652,8,'k/?','!H�s',NULL,NULL,'bcf9d97c',NULL,1,0,0),(1654,1638,1,'k{Y�','�}x�',NULL,NULL,'d78a6733',NULL,1,0,0),(1655,1654,1,'�Pl!','/�:',NULL,NULL,'afdb15fb',NULL,1,0,0),(1656,1655,8,'���','�7z',NULL,NULL,'87594321',NULL,1,0,0),(1657,1638,1,'�o�_','�g�b',NULL,NULL,'a7dbf960',NULL,1,0,0),(1658,1657,1,'�@��','��4�',NULL,NULL,'ac92187a',NULL,1,0,0),(1659,1658,8,'?#��','��jo',NULL,NULL,'b57fff6d',NULL,1,0,0),(1660,1638,1,'��;','U�y�',NULL,NULL,'4f1acaa9',NULL,1,0,0),(1661,1660,1,'j�H�','j&�d',NULL,NULL,'f2c57020',NULL,1,0,0),(1662,1661,8,'�ȍ�','�!vH',NULL,NULL,'1557f525',NULL,1,0,0),(1663,1638,1,'�o\"�','�y�',NULL,NULL,'31bad811',NULL,1,0,0),(1664,1663,1,'lvC�','*��T',NULL,NULL,'6655a6f7',NULL,1,0,0),(1665,1664,8,'m$m','\"p�}',NULL,NULL,'bcd98d62',NULL,1,0,0),(1666,NULL,1,'�=?','1fH�',NULL,1561411781,'2c9457c1',78,1,0,0),(1667,1666,1,'���{','�_�',NULL,NULL,'3921cd1a',NULL,1,0,0),(1668,1667,1,'��� ','���)',NULL,NULL,'478b38d1',NULL,1,0,0),(1669,1668,1,'0��<','8_ka',NULL,NULL,'c74b15cd',NULL,1,0,0),(1670,1669,8,'��\Z','�\Z�Y',NULL,NULL,'aa710b2b',NULL,1,0,0),(1671,1667,1,'���','�ǆ',NULL,NULL,'26da0f9d',NULL,1,0,0),(1672,1671,1,'ׂ��','H�(',NULL,NULL,'b952d38f',NULL,1,0,0),(1673,1672,8,'9��','xo��',NULL,NULL,'95150571',NULL,1,0,0),(1674,1667,1,'�-o','r�f�',NULL,NULL,'39918d71',NULL,1,0,0),(1675,1674,1,'d<�%','dp�',NULL,NULL,'bd1baf13',NULL,1,0,0),(1676,1675,8,'L��',')E\\�',NULL,NULL,'c971944f',NULL,1,0,0),(1677,1667,1,',��','Ez�2',NULL,NULL,'d81894a0',NULL,1,0,0),(1678,1677,1,'\'�<�','�!k',NULL,NULL,'b9c41309',NULL,1,0,0),(1679,1678,8,'�H�3','[7U',NULL,NULL,'23057fdd',NULL,1,0,0),(1680,1667,1,'�`','��m',NULL,NULL,'ab12a70b',NULL,1,0,0),(1681,1680,1,'ȓ(i','��',NULL,NULL,'f6988bbd',NULL,1,0,0),(1682,1681,8,'��;','?�b�',NULL,NULL,'9975b0b3',NULL,1,0,0),(1683,1667,1,'\"}','�(6E',NULL,NULL,'022b1cb4',NULL,1,0,0),(1684,1683,1,'�e�','y�\"�',NULL,NULL,'032a8dbd',NULL,1,0,0),(1685,1684,8,'3Ot�',',DR�',NULL,NULL,'4c722515',NULL,1,0,0),(1686,NULL,1,'`V��','�ǌ�',NULL,1561411782,'10569fb1',79,1,0,0),(1687,1686,1,'6�y�','���',NULL,NULL,'0f8f7b28',NULL,1,0,0),(1688,1687,1,'\"DYT','����',NULL,NULL,'db130a80',NULL,1,0,0),(1689,1688,1,'_��','�*�',NULL,NULL,'b33d2482',NULL,1,0,0),(1690,1689,8,'Z_Zz','Bt�Q',NULL,NULL,'d8306512',NULL,1,0,0),(1691,1687,1,'���','A�H�',NULL,NULL,'9081fd41',NULL,1,0,0),(1692,1691,1,'ݛ˜','o*[�',NULL,NULL,'83969c67',NULL,1,0,0),(1693,1692,8,'#�c','/�\\Q',NULL,NULL,'12d075b5',NULL,1,0,0),(1694,1687,1,'�O�p','�\"h�',NULL,NULL,'4c57c63d',NULL,1,0,0),(1695,1694,1,'��<�','T�f',NULL,NULL,'013492bd',NULL,1,0,0),(1696,1695,8,'�f�','s9t',NULL,NULL,'0682b95f',NULL,1,0,0),(1697,1687,1,'E���','O�9',NULL,NULL,'daa6f621',NULL,1,0,0),(1698,1697,1,'���6','��:�',NULL,NULL,'584c765f',NULL,1,0,0),(1699,1698,8,'f���',' 	��',NULL,NULL,'c1c3fa2d',NULL,1,0,0),(1700,NULL,1,'�,g�',']�:�',NULL,1561411782,'a6123c29',80,1,0,0),(1701,1700,1,'�0<0','�#�A',NULL,NULL,'aadd2a6f',NULL,1,0,0),(1702,1701,1,'��T�','\Z!�',NULL,NULL,'25cc9053',NULL,1,0,0),(1703,1702,1,'�D�P','A�]',NULL,NULL,'6028825d',NULL,1,0,0),(1704,1703,8,'�f','6�h	',NULL,NULL,'8cba5517',NULL,1,0,0),(1705,1701,1,'Y|k�','�}��',NULL,NULL,'49c57520',NULL,1,0,0),(1706,1705,1,'�&�\"','v�/',NULL,NULL,'728f036d',NULL,1,0,0),(1707,1706,8,'аŻ','�\"j',NULL,NULL,'3ad5b860',NULL,1,0,0),(1708,1701,1,'8�','(}E\n',NULL,NULL,'0a624a6b',NULL,1,0,0),(1709,1708,1,'����','����',NULL,NULL,'4b874986',NULL,1,0,0),(1710,1709,8,'X\ZU','��s',NULL,NULL,'313b2d78',NULL,1,0,0),(1711,NULL,1,'P�ȴ','�U��',NULL,1561411782,'7709d571',81,1,0,0),(1712,1711,1,'�[�','`���',NULL,NULL,'f9aad01d',NULL,1,0,0),(1713,1712,1,'t@w','����',NULL,NULL,'8db7c8f0',NULL,1,0,0),(1714,1713,1,'g�','\'�(',NULL,NULL,'83835077',NULL,1,0,0),(1715,1714,8,'\"�ؐ','�\r�',NULL,NULL,'348b44b2',NULL,1,0,0),(1716,1712,1,'ֳ��','l##�',NULL,NULL,'8c797aa3',NULL,1,0,0),(1717,1716,1,'3�<','DB',NULL,NULL,'6b38036b',NULL,1,0,0),(1718,1717,8,'��D','{!��',NULL,NULL,'4a950172',NULL,1,0,0),(1719,NULL,1,'�c�','X���',NULL,1561411783,'07a0f48b',82,1,0,0),(1720,1719,1,'�\0�i','�H',NULL,NULL,'64d47d5c',NULL,1,0,0),(1721,1720,1,'R��','�\'q�',NULL,NULL,'8aa6d2c0',NULL,1,0,0),(1722,1721,1,'c̤9','K��',NULL,NULL,'d6435102',NULL,1,0,0),(1723,1722,8,';҇\'','E�T�',NULL,NULL,'a39c5274',NULL,1,0,0),(1724,1720,1,'�\n�','4��W',NULL,NULL,'0f69b5b8',NULL,1,0,0),(1725,1724,8,'�b�','�|�',NULL,NULL,'cf15a402',NULL,1,0,0),(1726,1720,1,'��K�','{_�',NULL,NULL,'33a6c6d2',NULL,1,0,0),(1727,1726,8,'Y�Ȍ','��y�',NULL,NULL,'b758d51a',NULL,1,0,0),(1728,NULL,1,' ?�','�#x',NULL,1561411783,'274a25fc',83,1,0,0),(1729,1728,1,']�	','k���',NULL,NULL,'3bc15086',NULL,1,0,0),(1730,1729,1,'E���','��ԃ',NULL,NULL,'44d5a521',NULL,1,0,0),(1731,1730,1,'u�','mE�h',NULL,NULL,'ab8c0136',NULL,1,0,0),(1732,1731,8,'�r','��H�',NULL,NULL,'37f62966',NULL,1,0,0),(1733,1729,1,'��6','�j}�',NULL,NULL,'03c59f9d',NULL,1,0,0),(1734,1733,8,'����','��',NULL,NULL,'03ab781d',NULL,1,0,0),(1735,NULL,1,'C���','4���',NULL,1561411784,'7a278900',84,1,0,0),(1736,1735,1,'U��\Z','W���',NULL,NULL,'26b256a3',NULL,1,0,0),(1737,1736,1,'\rlX','�7��',NULL,NULL,'63bb571b',NULL,1,0,0),(1738,1737,1,'*�;�','3h�',NULL,NULL,'a7df95d8',NULL,1,0,0),(1739,1738,8,'u:\\%','�&|',NULL,NULL,'0da26f07',NULL,1,0,0),(1740,1735,1,';�a�','Vo\n',NULL,NULL,'1105af77',NULL,1,0,0),(1741,1740,1,'tT/z','G��\Z',NULL,NULL,'92378d0a',NULL,1,0,0),(1742,1741,1,'�,۩','��ռ',NULL,NULL,'3127bc7b',NULL,1,0,0),(1743,1742,1,'�E6�',';�',NULL,NULL,'40b1cd06',NULL,1,0,0),(1744,1743,1,'G��J','�\0',NULL,NULL,'96f78aa0',NULL,1,0,0),(1745,1744,1,'���!','���\"',NULL,NULL,'0cc74c9a',NULL,1,0,0),(1746,1745,8,'��0r','����',NULL,NULL,'a39872ac',NULL,1,0,0),(1747,1742,1,'�a0&','�E�',NULL,NULL,'9db789f3',NULL,1,0,0),(1748,1747,1,'6� �','�$��',NULL,NULL,'61c702a9',NULL,1,0,0),(1749,1748,1,'#c,�','�%�',NULL,NULL,'4a908897',NULL,1,0,0),(1750,1749,1,'���f','d�J�',NULL,NULL,'9b63d532',NULL,1,0,0),(1751,1750,1,'�D�6','��AB',NULL,NULL,'489dfa56',NULL,1,0,0),(1752,1751,8,'�Y�','zI}�',NULL,NULL,'1dc45bff',NULL,1,0,0),(1753,1747,1,'z1\n�','�Ɂ',NULL,NULL,'abc253a7',NULL,1,0,0),(1754,1753,1,'Ķ�W','�',NULL,NULL,'5041b2fa',NULL,1,0,0),(1755,1754,1,'���','�Ʊ�',NULL,NULL,'5ff8fcd3',NULL,1,0,0),(1756,1755,1,'i;�','p7`',NULL,NULL,'7c652f63',NULL,1,0,0),(1757,1756,8,'̹F�','G�Q',NULL,NULL,'05c7bcfd',NULL,1,0,0),(1758,1747,1,'E[�','�,�',NULL,NULL,'acc0899c',NULL,1,0,0),(1759,1758,1,'��9K','W\':�',NULL,NULL,'cc5a119c',NULL,1,0,0),(1760,1759,1,'*��','<e��',NULL,NULL,'81cfbd03',NULL,1,0,0),(1761,1760,1,'�\\9�','\Z<�',NULL,NULL,'5dfac506',NULL,1,0,0),(1762,1761,8,'k@�s','�I[�',NULL,NULL,'d7bc69b0',NULL,1,0,0),(1763,1747,1,'�I\n','��Ъ',NULL,NULL,'b06fbb97',NULL,1,0,0),(1764,1763,1,'�s�','-��W',NULL,NULL,'b3cf892d',NULL,1,0,0),(1765,1764,1,'1�','����',NULL,NULL,'5950fb19',NULL,1,0,0),(1766,1765,1,'���','����',NULL,NULL,'df82262d',NULL,1,0,0),(1767,1766,8,'��','O�',NULL,NULL,'9050bba6',NULL,1,0,0),(1768,1741,1,'+�o_','�XP�',NULL,NULL,'3ada8f48',NULL,1,0,0),(1769,1768,1,'ڏ\rO','�u1<',NULL,NULL,'839d9f20',NULL,1,0,0),(1770,1769,1,'�G\n�','�0�',NULL,NULL,'58f899d7',NULL,1,0,0),(1771,1770,1,'���','q(��',NULL,NULL,'ab615408',NULL,1,0,0),(1772,1771,8,'}�l&','E��',NULL,NULL,'acb9132a',NULL,1,0,0),(1773,1768,1,'�*�]','��#A',NULL,NULL,'bfd7dbd5',NULL,1,0,0),(1774,1773,1,'C���','�H,�',NULL,NULL,'040b5743',NULL,1,0,0),(1775,1774,1,'��B','�V�m',NULL,NULL,'60ad43cf',NULL,1,0,0),(1776,1775,1,'i���','S�Ћ',NULL,NULL,'b88da325',NULL,1,0,0),(1777,1776,1,'��e','бX�',NULL,NULL,'c255f966',NULL,1,0,0),(1778,1777,8,'��k','/��',NULL,NULL,'9c12447a',NULL,1,0,0),(1779,1773,1,'נ�','��\r',NULL,NULL,'028385b7',NULL,1,0,0),(1780,1779,1,'+�ۤ','�ph�',NULL,NULL,'0173886a',NULL,1,0,0),(1781,1780,1,'��H�','�(W8',NULL,NULL,'1d058c63',NULL,1,0,0),(1782,1781,1,'	�','P�t�',NULL,NULL,'8282733c',NULL,1,0,0),(1783,1782,8,'��U','���',NULL,NULL,'9c32556b',NULL,1,0,0),(1784,1773,1,'y�H�','(-�z',NULL,NULL,'379cf7b4',NULL,1,0,0),(1785,1784,1,'�Ǥ�','34c',NULL,NULL,'50df6d71',NULL,1,0,0),(1786,1785,1,'�0+y','*Fh',NULL,NULL,'37cf6838',NULL,1,0,0),(1787,1786,1,'_��6','+e��',NULL,NULL,'97810c2a',NULL,1,0,0),(1788,1787,8,'����','-[=�',NULL,NULL,'fa26d2cb',NULL,1,0,0),(1789,1741,1,'?Z�','gu��',NULL,NULL,'44c0c7f4',NULL,1,0,0),(1790,1789,1,'�V9�','��y',NULL,NULL,'d63163ad',NULL,1,0,0),(1791,1790,1,'x��','�g��',NULL,NULL,'51121828',NULL,1,0,0),(1792,1791,1,'�\'�P','���',NULL,NULL,'d5f582d1',NULL,1,0,0),(1793,1792,8,'����','Q�?',NULL,NULL,'46dc13dd',NULL,1,0,0),(1794,1789,1,'WU��','R\Z-�',NULL,NULL,'7c31d948',NULL,1,0,0),(1795,1794,1,'��C�','!D5�',NULL,NULL,'4615fca5',NULL,1,0,0),(1796,1795,1,'�\n�','`�O',NULL,NULL,'0f21084b',NULL,1,0,0),(1797,1796,1,'#\Z\0�','��ƅ',NULL,NULL,'2022939b',NULL,1,0,0),(1798,1797,1,'�\nR�','�',NULL,NULL,'15b6fb73',NULL,1,0,0),(1799,1798,8,'tE��','{<',NULL,NULL,'20f60dc1',NULL,1,0,0),(1800,1794,1,'����','�CE',NULL,NULL,'02fc4d00',NULL,1,0,0),(1801,1800,1,'�q�','h_��',NULL,NULL,'daffdd6a',NULL,1,0,0),(1802,1801,1,'�w�e','m���',NULL,NULL,'7f02a7a4',NULL,1,0,0),(1803,1802,1,'�39\Z','vBU',NULL,NULL,'a1947702',NULL,1,0,0),(1804,1803,8,'o�','�J֘',NULL,NULL,'0246f6df',NULL,1,0,0),(1805,1794,1,'y�Xe','w�L&',NULL,NULL,'a8d7a04b',NULL,1,0,0),(1806,1805,1,']e','c*�',NULL,NULL,'25630614',NULL,1,0,0),(1807,1806,1,'D��','�u��',NULL,NULL,'cfca19d2',NULL,1,0,0),(1808,1807,1,'w*%','���',NULL,NULL,'c04c5619',NULL,1,0,0),(1809,1808,8,'4���','$9�',NULL,NULL,'a71a2d44',NULL,1,0,0),(1810,1794,1,'�s�','�Kt',NULL,NULL,'4f9660dc',NULL,1,0,0),(1811,1810,1,'�?�;','31@�',NULL,NULL,'18a4a375',NULL,1,0,0),(1812,1811,1,'�ǐ�','��',NULL,NULL,'186f81df',NULL,1,0,0),(1813,1812,1,'�a�0','�x�&',NULL,NULL,'4abafd42',NULL,1,0,0),(1814,1813,8,'f w#','O��',NULL,NULL,'3cc01032',NULL,1,0,0),(1815,1794,1,'ת��','��R|',NULL,NULL,'2d85b038',NULL,1,0,0),(1816,1815,1,'��5J','	 �',NULL,NULL,'a7ad37af',NULL,1,0,0),(1817,1816,1,'`�o�','�K�',NULL,NULL,'c7527a0f',NULL,1,0,0),(1818,1817,1,'�k�',')���',NULL,NULL,'3b97298f',NULL,1,0,0),(1819,1818,8,',R�','p�O%',NULL,NULL,'29fcf580',NULL,1,0,0),(1820,1794,1,'\0�k8','���',NULL,NULL,'28263292',NULL,1,0,0),(1821,1820,1,'R�h','�P�\n',NULL,NULL,'bc7f5641',NULL,1,0,0),(1822,1821,1,',�u&','Q���',NULL,NULL,'9c648a54',NULL,1,0,0),(1823,1822,1,'����','/��5',NULL,NULL,'cb059f98',NULL,1,0,0),(1824,1823,8,'��o','߷��',NULL,NULL,'0292a0b9',NULL,1,0,0),(1825,1741,1,'M@ך','�P��',NULL,NULL,'baf82bcf',NULL,1,0,0),(1826,1825,1,'c�G�','ˀ��',NULL,NULL,'fc9ad287',NULL,1,0,0),(1827,1826,1,'��ա','���',NULL,NULL,'1d17ba8c',NULL,1,0,0),(1828,1827,1,'T��\r','Ǽ�d',NULL,NULL,'8fd835fd',NULL,1,0,0),(1829,1828,8,' �$p','\r{\Z',NULL,NULL,'8dbb04bb',NULL,1,0,0),(1830,1825,1,'�j�','CQ�T',NULL,NULL,'2f05992c',NULL,1,0,0),(1831,1830,1,'�\0@4','�\"�U',NULL,NULL,'94655a3c',NULL,1,0,0),(1832,1831,1,'�ɡ�','�;�\r',NULL,NULL,'9b61dddc',NULL,1,0,0),(1833,1832,1,'Q;�-','I��',NULL,NULL,'6f285558',NULL,1,0,0),(1834,1833,1,'�*�B','W�UK',NULL,NULL,'d2a8d0d5',NULL,1,0,0),(1835,1834,8,'m��','�Â]',NULL,NULL,'6905d17a',NULL,1,0,0),(1836,1830,1,'JM�','��V�',NULL,NULL,'746d760f',NULL,1,0,0),(1837,1836,1,'����','����',NULL,NULL,'53cfc38a',NULL,1,0,0),(1838,1837,1,'v}�','��U',NULL,NULL,'2bcfb8ff',NULL,1,0,0),(1839,1838,1,'��)�',':ŰL',NULL,NULL,'a7f99df7',NULL,1,0,0),(1840,1839,8,'\n�','cȌ�',NULL,NULL,'a851b119',NULL,1,0,0),(1841,1830,1,'c�Lb','��A�',NULL,NULL,'c3b8b761',NULL,1,0,0),(1842,1841,1,'��̠','��E�',NULL,NULL,'332f87d6',NULL,1,0,0),(1843,1842,1,'G{�','c���',NULL,NULL,'5bc1d6c8',NULL,1,0,0),(1844,1843,1,'��','ym�',NULL,NULL,'8ad413ff',NULL,1,0,0),(1845,1844,8,'r','�y9�',NULL,NULL,'4a4d7dda',NULL,1,0,0),(1846,1830,1,'���','�<�G',NULL,NULL,'b1518c90',NULL,1,0,0),(1847,1846,1,'W}�','f���',NULL,NULL,'47656d47',NULL,1,0,0),(1848,1847,1,'H+߳','ߠ',NULL,NULL,'922a6bda',NULL,1,0,0),(1849,1848,1,'���P','��f�',NULL,NULL,'057c17c8',NULL,1,0,0),(1850,1849,8,'� �','��2',NULL,NULL,'ba1c302a',NULL,1,0,0),(1851,1741,1,'���4','���',NULL,NULL,'b2ad374c',NULL,1,0,0),(1852,1851,1,'1���','[�',NULL,NULL,'0f08a382',NULL,1,0,0),(1853,1852,1,'a�','k�i',NULL,NULL,'dc2c92a8',NULL,1,0,0),(1854,1853,1,'���4','I��',NULL,NULL,'0d482552',NULL,1,0,0),(1855,1854,8,'Z�i?','��y�',NULL,NULL,'287d4f25',NULL,1,0,0),(1856,NULL,1,'h\Zl�',';[�',NULL,1561411788,'bb9884c1',85,1,0,0),(1857,1856,1,'��VM','=�|P',NULL,NULL,'59666795',NULL,1,0,0),(1858,1857,1,'�7֊','!��F',NULL,NULL,'8329a018',NULL,1,0,0),(1859,1858,1,'���x','��',NULL,NULL,'6ac63870',NULL,1,0,0),(1860,1859,8,'��zT','�R�',NULL,NULL,'4f55a90f',NULL,1,0,0),(1861,1856,1,'	Yys','��<a',NULL,NULL,'a277a2f3',NULL,1,0,0),(1862,1861,1,'c�','U��',NULL,NULL,'1fcba579',NULL,1,0,0),(1863,1862,1,'��6�','�O�',NULL,NULL,'f935b3ba',NULL,1,0,0),(1864,1863,1,'�Ͱ','�7V)',NULL,NULL,'f5f974ca',NULL,1,0,0),(1865,1864,1,'�%�','��#�',NULL,NULL,'51accda8',NULL,1,0,0),(1866,1865,8,'4\0','z���',NULL,NULL,'43db2c46',NULL,1,0,0),(1867,1861,1,'TM�-','5\rW',NULL,NULL,'a6230611',NULL,1,0,0),(1868,1867,1,'����','�<A�',NULL,NULL,'f69b19a3',NULL,1,0,0),(1869,1868,1,'#��\"','yU�',NULL,NULL,'bc4869ab',NULL,1,0,0),(1870,1869,1,'t�z','�YoV',NULL,NULL,'f00f42fd',NULL,1,0,0),(1871,1870,8,'}��1','p�ڽ',NULL,NULL,'ac05cb5f',NULL,1,0,0),(1872,1861,1,'��ư','�٧S',NULL,NULL,'3bf3681c',NULL,1,0,0),(1873,1872,1,'�V}','���',NULL,NULL,'1479d568',NULL,1,0,0),(1874,1873,1,'z�yP','�&�H',NULL,NULL,'8440b908',NULL,1,0,0),(1875,1874,1,'�fQ�','[u',NULL,NULL,'a41375b3',NULL,1,0,0),(1876,1875,8,'��7�','%3�',NULL,NULL,'a2db6674',NULL,1,0,0),(1877,1861,1,'� =�','U ��',NULL,NULL,'2724354f',NULL,1,0,0),(1878,1877,1,'g�C','��U�',NULL,NULL,'8632bcf4',NULL,1,0,0),(1879,1878,1,'����','��Y\Z',NULL,NULL,'09c468d6',NULL,1,0,0),(1880,1879,1,'*��','���',NULL,NULL,'7f015066',NULL,1,0,0),(1881,1880,8,'|F0','%A\\�',NULL,NULL,'940b0b47',NULL,1,0,0),(1882,NULL,1,'s�','t\0�t',NULL,1561411789,'7a299f27',86,1,0,0),(1883,1882,1,'�<�]','x�\\�',NULL,NULL,'6b3a9494',NULL,1,0,0),(1884,1883,1,'_-��','ϔW�',NULL,NULL,'81842f93',NULL,1,0,0),(1885,1884,1,'W,��','�,�9',NULL,NULL,'d53bb43a',NULL,1,0,0),(1886,1885,8,'?�\n�','�U�[',NULL,NULL,'a94df1a5',NULL,1,0,0),(1887,1882,1,'eO�r','IF[',NULL,NULL,'ab723d66',NULL,1,0,0),(1888,1887,1,'��','?F+�',NULL,NULL,'6d1c06a4',NULL,1,0,0),(1889,1888,1,'��%v','��W',NULL,NULL,'ca2b9f43',NULL,1,0,0),(1890,1889,1,'�EcW','�tܝ',NULL,NULL,'9c097931',NULL,1,0,0),(1891,1890,1,'�0','�I�#',NULL,NULL,'9d78fb88',NULL,1,0,0),(1892,1891,8,'�\n','Ƭ�q',NULL,NULL,'686bb1cb',NULL,1,0,0),(1893,1887,1,'A#x','r��',NULL,NULL,'57523afa',NULL,1,0,0),(1894,1893,1,'��UC','�ɢ',NULL,NULL,'8676d430',NULL,1,0,0),(1895,1894,1,'G�','��c',NULL,NULL,'ddcd62c4',NULL,1,0,0),(1896,1895,1,'t&�,','T\'M',NULL,NULL,'0f789204',NULL,1,0,0),(1897,1896,8,'�c�','U#�',NULL,NULL,'0b9f758f',NULL,1,0,0),(1898,1887,1,'7<�','',NULL,NULL,'61b7ffcc',NULL,1,0,0),(1899,1898,1,';b�q',';X�r',NULL,NULL,'a45ca2db',NULL,1,0,0),(1900,1899,1,'b�k','���',NULL,NULL,'17a7557b',NULL,1,0,0),(1901,1900,1,'t��','v�ɬ',NULL,NULL,'34943628',NULL,1,0,0),(1902,1901,8,'��$�','��',NULL,NULL,'bc34cc95',NULL,1,0,0),(1903,NULL,1,'�A;�','E�t�',NULL,1561411790,'c8888055',87,1,0,0),(1904,1903,1,'V+I','��q�',NULL,NULL,'80850582',NULL,1,0,0),(1905,1904,1,'�$��','�\'ƀ',NULL,NULL,'c1b77c4a',NULL,1,0,0),(1906,1905,1,'�7	U','�%S',NULL,NULL,'b4db957f',NULL,1,0,0),(1907,1906,8,'��0','g���',NULL,NULL,'dc9392b4',NULL,1,0,0),(1908,1903,1,'�ؤ','��',NULL,NULL,'d5d73ca0',NULL,1,0,0),(1909,1908,1,'���','GL7',NULL,NULL,'affa2cb0',NULL,1,0,0),(1910,1909,1,']x��','t\"@',NULL,NULL,'4044d840',NULL,1,0,0),(1911,1910,1,'���1','��8�',NULL,NULL,'f5a2052b',NULL,1,0,0),(1912,1911,1,'��{','�{�',NULL,NULL,'6d1c6f0a',NULL,1,0,0),(1913,1912,8,'�X��','e|PC',NULL,NULL,'2b45d0d3',NULL,1,0,0),(1914,1908,1,'3�-V','�s�',NULL,NULL,'373481c6',NULL,1,0,0),(1915,1914,1,'ڨ3�','��0',NULL,NULL,'b09d31bb',NULL,1,0,0),(1916,1915,1,'L�D�','IAf<',NULL,NULL,'5851bb75',NULL,1,0,0),(1917,1916,1,'j?e�','����',NULL,NULL,'4500ffd7',NULL,1,0,0),(1918,1917,8,'/��','�O�',NULL,NULL,'9f81c1f2',NULL,1,0,0),(1919,1908,1,'loG+','=�!�',NULL,NULL,'883b2117',NULL,1,0,0),(1920,1919,1,'�߲','�v\\�',NULL,NULL,'1d5551cb',NULL,1,0,0),(1921,1920,1,'�\Z-\\','%�\r�',NULL,NULL,'69fc030a',NULL,1,0,0),(1922,1921,1,'\nzW','��S�',NULL,NULL,'4ad4a59d',NULL,1,0,0),(1923,1922,8,'\na��','m�&�',NULL,NULL,'4a4366f4',NULL,1,0,0),(1924,1908,1,'��Ҁ','�OE',NULL,NULL,'21814bb5',NULL,1,0,0),(1925,1924,1,';[','�Y�|',NULL,NULL,'36c98062',NULL,1,0,0),(1926,1925,1,'|�$','��K/',NULL,NULL,'52a13930',NULL,1,0,0),(1927,1926,1,'����','r�C�',NULL,NULL,'05431f88',NULL,1,0,0),(1928,1927,8,'c��]','@j�',NULL,NULL,'8fa71141',NULL,1,0,0),(1929,1908,1,'Խ,�','fB�T',NULL,NULL,'1fcf3c02',NULL,1,0,0),(1930,1929,1,'��b�','���',NULL,NULL,'c11c8037',NULL,1,0,0),(1931,1930,1,'��=','+���',NULL,NULL,'51d93b67',NULL,1,0,0),(1932,1931,1,'|\ZRY','j��e',NULL,NULL,'b2559800',NULL,1,0,0),(1933,1932,8,'����','���',NULL,NULL,'f272639f',NULL,1,0,0),(1934,1908,1,'Q�϶','\0x',NULL,NULL,'8b518466',NULL,1,0,0),(1935,1934,1,'�=��','��<',NULL,NULL,'357ddbbc',NULL,1,0,0),(1936,1935,1,'\"2��','%��',NULL,NULL,'cd2d8f57',NULL,1,0,0),(1937,1936,1,'&�#','�,!',NULL,NULL,'d97b97d0',NULL,1,0,0),(1938,1937,8,'� �\'','	!Ѽ',NULL,NULL,'a8d80ad2',NULL,1,0,0),(1939,NULL,1,'�݋','_��1',NULL,1561411791,'76d44889',88,1,0,0),(1940,1939,1,'��/','\'��@',NULL,NULL,'5a4a8a4a',NULL,1,0,0),(1941,1940,1,'�lk\0','P�y',NULL,NULL,'ad3878c1',NULL,1,0,0),(1942,1941,1,'y�w','�[T$',NULL,NULL,'486510ff',NULL,1,0,0),(1943,1942,8,'6��','�=W',NULL,NULL,'47420790',NULL,1,0,0),(1944,1939,1,'�i','�ݙ',NULL,NULL,'7747dc47',NULL,1,0,0),(1945,1944,1,'b�̥','�R�z',NULL,NULL,'7fc502f4',NULL,1,0,0),(1946,1945,1,'AlE','z�w',NULL,NULL,'7361b1c1',NULL,1,0,0),(1947,1946,1,'�[�8','����',NULL,NULL,'b1104247',NULL,1,0,0),(1948,1947,1,'�3�','Ȑ�',NULL,NULL,'ff8af27d',NULL,1,0,0),(1949,1948,8,'Tj(�','���',NULL,NULL,'b04c0db7',NULL,1,0,0),(1950,1939,1,'����','\nϹ',NULL,NULL,'896a7fc4',NULL,1,0,0),(1951,1950,1,'&�\\','9�',NULL,NULL,'9d6fb8f1',NULL,1,0,0),(1952,1951,1,'z)&&','�s!Q',NULL,NULL,'c08cbad0',NULL,1,0,0),(1953,1952,8,'Lf�','ac�M',NULL,NULL,'81180985',NULL,1,0,0),(1954,NULL,1,'\Zv�b','{\n�',NULL,1561411791,'25759738',89,1,0,0),(1955,1954,1,'���5','DO�#',NULL,NULL,'a757c64a',NULL,1,0,0),(1956,1955,1,'ZF�Z','��q�',NULL,NULL,'6a43a4b7',NULL,1,0,0),(1957,1956,1,'�0�','���',NULL,NULL,'4a915fb3',NULL,1,0,0),(1958,1957,8,'g�Y�','�ɊP',NULL,NULL,'34668554',NULL,1,0,0),(1959,1954,1,'\n+��','[�w�',NULL,NULL,'c79fca73',NULL,1,0,0),(1960,1959,1,'/\Z��','��bF',NULL,NULL,'73c77065',NULL,1,0,0),(1961,1960,1,'=�2�','\'UOB',NULL,NULL,'dbb10b70',NULL,1,0,0),(1962,1961,1,'��;�','Lۅ�',NULL,NULL,'c8421f4b',NULL,1,0,0),(1963,1962,8,'3\rZF','�G�u',NULL,NULL,'695cdf24',NULL,1,0,0),(1964,1960,1,'L�ʖ','�v��',NULL,NULL,'dab0a4c5',NULL,1,0,0),(1965,1964,1,')�43','Wh�',NULL,NULL,'03469544',NULL,1,0,0),(1966,1965,8,'񴥩','\0�x�',NULL,NULL,'9f66458b',NULL,1,0,0),(1967,1960,1,'�\rD�','�w�',NULL,NULL,'ab5d5285',NULL,1,0,0),(1968,1967,1,'V<kL','B�4�',NULL,NULL,'565aa630',NULL,1,0,0),(1969,1968,8,'8W�\"','�\n��',NULL,NULL,'81d6c37c',NULL,1,0,0),(1970,1960,1,'�','�t�%',NULL,NULL,'0965063b',NULL,1,0,0),(1971,1970,1,'���k','��',NULL,NULL,'6b8029c3',NULL,1,0,0),(1972,1971,8,'����','6�',NULL,NULL,'04bd65b6',NULL,1,0,0),(1973,1960,1,'��',',m<�',NULL,NULL,'20f21652',NULL,1,0,0),(1974,1973,1,'�','�z�',NULL,NULL,'76762fcc',NULL,1,0,0),(1975,1974,8,'�m�x','a�}�',NULL,NULL,'8907394a',NULL,1,0,0),(1976,1960,1,'�:�','�7�a',NULL,NULL,'32391f6a',NULL,1,0,0),(1977,1976,1,'f`�','F)\r�',NULL,NULL,'f232d340',NULL,1,0,0),(1978,1977,8,'�}[�','y�&',NULL,NULL,'1a395266',NULL,1,0,0),(1979,NULL,1,'c/�X','W{�<',NULL,1561411792,'f538da94',90,1,0,0),(1980,1979,1,'�-�S','*إu',NULL,NULL,'00534196',NULL,1,0,0),(1981,1980,1,'���','d}Jm',NULL,NULL,'65f99370',NULL,1,0,0),(1982,1981,1,'DM;�','�\n�',NULL,NULL,'038719a8',NULL,1,0,0),(1983,1982,8,'�Ȋ5','f�H�',NULL,NULL,'55c33a39',NULL,1,0,0),(1984,1979,1,'p7��','C?',NULL,NULL,'0a7172bd',NULL,1,0,0),(1985,1984,1,'m8z!','4�fg',NULL,NULL,'06c1d18f',NULL,1,0,0),(1986,1985,1,'6��7','�b6�',NULL,NULL,'aa937c68',NULL,1,0,0),(1987,1986,8,'�U','�FL�',NULL,NULL,'69367c07',NULL,1,0,0),(1988,1979,1,'��©','����',NULL,NULL,'371a7968',NULL,1,0,0),(1989,1988,1,'�&X','��G',NULL,NULL,'68604ca0',NULL,1,0,0),(1990,1989,1,'؋a�','Q�Bj',NULL,NULL,'a6789471',NULL,1,0,0),(1991,1990,8,'�\nU�','�)�;',NULL,NULL,'b9b96617',NULL,1,0,0),(1992,1979,1,'7!�<','����',NULL,NULL,'764a2842',NULL,1,0,0),(1993,1992,1,'1FT�',')���',NULL,NULL,'087d0c15',NULL,1,0,0),(1994,1993,1,'M!-','���',NULL,NULL,'15d3d0bb',NULL,1,0,0),(1995,1994,8,'KY1�','=b�H',NULL,NULL,'7b78b882',NULL,1,0,0),(1996,1979,1,'sD�','8�_',NULL,NULL,'704b1858',NULL,1,0,0),(1997,1996,1,'¡J�','�\0�',NULL,NULL,'4d4798c5',NULL,1,0,0),(1998,1997,1,'4;P�','����',NULL,NULL,'201f1c42',NULL,1,0,0),(1999,1998,8,'���A','�f?�',NULL,NULL,'a3d4c4cd',NULL,1,0,0),(2000,1979,1,'�\\�k','j��',NULL,NULL,'7b746852',NULL,1,0,0),(2001,2000,1,'\r�1R','��	�',NULL,NULL,'23d72553',NULL,1,0,0),(2002,2001,1,'���','�3m�',NULL,NULL,'88ff26a3',NULL,1,0,0),(2003,2002,8,'LM��','Bl��',NULL,NULL,'44c0c705',NULL,1,0,0),(2004,1979,1,'W=��','�/a',NULL,NULL,'f1045921',NULL,1,0,0),(2005,2004,1,'�C�','�9<',NULL,NULL,'01318508',NULL,1,0,0),(2006,2005,1,'�X�','*�',NULL,NULL,'bd7f42b7',NULL,1,0,0),(2007,2006,8,'�X�','[%\rr',NULL,NULL,'0a0800b6',NULL,1,0,0),(2008,NULL,1,'QHɸ','|M=',NULL,1561411793,'db218ddc',91,1,0,0),(2009,2008,1,'*�I�','񅭊',NULL,NULL,'9d50ac4d',NULL,1,0,0),(2010,2009,1,'2���','�k',NULL,NULL,'a9688004',NULL,1,0,0),(2011,2010,1,'Y�','�E',NULL,NULL,'1234c1fa',NULL,1,0,0),(2012,2011,8,'w�M]','�t��',NULL,NULL,'07f5c0af',NULL,1,0,0),(2013,2008,1,'�jP�','k��$',NULL,NULL,'cb00375b',NULL,1,0,0),(2014,2013,1,'���0','�',NULL,NULL,'8da5ad40',NULL,1,0,0),(2015,2014,1,'Q8�@','����',NULL,NULL,'198b77a8',NULL,1,0,0),(2016,2015,8,'����','��?',NULL,NULL,'7930d2cf',NULL,1,0,0),(2017,2008,1,'yYE�','X��',NULL,NULL,'d1375f94',NULL,1,0,0),(2018,2017,1,'[<��','e&۸',NULL,NULL,'65b41076',NULL,1,0,0),(2019,2018,1,'W�� ','`�BD',NULL,NULL,'b3434594',NULL,1,0,0),(2020,2019,8,'�L�$','XB��',NULL,NULL,'629faa48',NULL,1,0,0),(2021,2008,1,'��Vw','��/\n',NULL,NULL,'6226d327',NULL,1,0,0),(2022,2021,1,'Lx�','Uk�p',NULL,NULL,'a03c0dd8',NULL,1,0,0),(2023,2022,1,'��','9V�',NULL,NULL,'0fa9732f',NULL,1,0,0),(2024,2023,8,'��|','@�',NULL,NULL,'29fc70dd',NULL,1,0,0),(2025,2008,1,'I��5','�8�',NULL,NULL,'b4f8c476',NULL,1,0,0),(2026,2025,1,'��0�','X\\eO',NULL,NULL,'9a8f46b2',NULL,1,0,0),(2027,2026,1,'�-�E','W4��',NULL,NULL,'d4f16bd7',NULL,1,0,0),(2028,2027,8,' 6�','o�j',NULL,NULL,'d70d16f0',NULL,1,0,0),(2029,NULL,1,'4�j�','Ǫ�L',NULL,1561411794,'99dda3f2',92,1,0,0),(2030,2029,1,'��aJ','vw�a',NULL,NULL,'7d936189',NULL,1,0,0),(2031,2030,1,'шk�','�	��',NULL,NULL,'b65aa8ba',NULL,1,0,0),(2032,2031,1,'�,�B','ҕ��',NULL,NULL,'b7519365',NULL,1,0,0),(2033,2032,8,'�R','G�d',NULL,NULL,'6b5acb56',NULL,1,0,0),(2034,2029,1,'YPb','��$4',NULL,NULL,'424707f4',NULL,1,0,0),(2035,2034,1,'\r7�f','b_p',NULL,NULL,'755fc8d6',NULL,1,0,0),(2036,2035,1,'Vf܂','1Oy�',NULL,NULL,'19c11b9d',NULL,1,0,0),(2037,2036,8,'\'Tu/','�Q�z',NULL,NULL,'c6b58783',NULL,1,0,0),(2038,2029,1,'2���','u��',NULL,NULL,'662abbb7',NULL,1,0,0),(2039,2038,1,'p5��','/̷�',NULL,NULL,'c9057f6c',NULL,1,0,0),(2040,2039,1,'�]e�','K���',NULL,NULL,'3cc011c2',NULL,1,0,0),(2041,2040,8,'/��d','�ņQ',NULL,NULL,'f9027849',NULL,1,0,0),(2042,2029,1,']M(','DhHW',NULL,NULL,'9350696b',NULL,1,0,0),(2043,2042,1,'��','bzY',NULL,NULL,'929f63d3',NULL,1,0,0),(2044,2043,1,'3�:','�u<�',NULL,NULL,'6c22a138',NULL,1,0,0),(2045,2044,8,'��','?̴�',NULL,NULL,'9fafa09b',NULL,1,0,0),(2046,2029,1,'�C\'�','֨�\\',NULL,NULL,'11a78245',NULL,1,0,0),(2047,2046,1,'���','ְU\0',NULL,NULL,'440b7504',NULL,1,0,0),(2048,2047,1,'Ȇ�','��;',NULL,NULL,'30d21348',NULL,1,0,0),(2049,2048,8,'��)q',';f�',NULL,NULL,'7c90fbf8',NULL,1,0,0),(2050,2029,1,'��K�','�8c',NULL,NULL,'000d8605',NULL,1,0,0),(2051,2050,1,'����','���J',NULL,NULL,'72d321af',NULL,1,0,0),(2052,2051,1,';���','h�)�',NULL,NULL,'a988c812',NULL,1,0,0),(2053,2052,8,'M�M','�oY\Z',NULL,NULL,'3d6f2b04',NULL,1,0,0),(2054,NULL,1,'\Z�u','ٳj�',NULL,1561411795,'9a6a4fcd',93,1,0,0),(2055,2054,1,'��<','�5s!',NULL,NULL,'a426d9a4',NULL,1,0,0),(2056,2055,1,'l%�','�Zr�',NULL,NULL,'16cb3690',NULL,1,0,0),(2057,2056,1,'�,�','�Җ',NULL,NULL,'bb39b383',NULL,1,0,0),(2058,2057,8,'����','���',NULL,NULL,'942ff899',NULL,1,0,0),(2059,2054,1,'����','��E',NULL,NULL,'c212f1d4',NULL,1,0,0),(2060,2059,1,'�I��','�;�',NULL,NULL,'421cb5a9',NULL,1,0,0),(2061,2060,1,'�q�u','[J�',NULL,NULL,'081253ca',NULL,1,0,0),(2062,2061,8,'�D*','�Yz',NULL,NULL,'d6afb16b',NULL,1,0,0),(2063,2054,1,'D�!I','�U�',NULL,NULL,'a9a8c205',NULL,1,0,0),(2064,2063,1,'U���',']�p$',NULL,NULL,'268f28d1',NULL,1,0,0),(2065,2064,1,'�ʡ','f=',NULL,NULL,'1598c247',NULL,1,0,0),(2066,2065,8,'hs�','�נ8',NULL,NULL,'11dd58c2',NULL,1,0,0),(2067,2054,1,'�6I�','|�\\',NULL,NULL,'2508745f',NULL,1,0,0),(2068,2067,1,'ww�','�W�',NULL,NULL,'b6ccd758',NULL,1,0,0),(2069,2068,1,'U	L�','r��',NULL,NULL,'69769ad9',NULL,1,0,0),(2070,2069,8,'�wH',',��g',NULL,NULL,'b562b4dc',NULL,1,0,0),(2071,NULL,1,'���','<	d',NULL,1561411796,'d232f062',94,1,0,0),(2072,2071,1,'=%�','�q�?',NULL,NULL,'4bb5c460',NULL,1,0,0),(2073,2072,1,'\\b,�','��c�',NULL,NULL,'b23878bb',NULL,1,0,0),(2074,2073,1,'����','�ȴ)',NULL,NULL,'b4894a20',NULL,1,0,0),(2075,2074,8,'L�u','��q,',NULL,NULL,'1c417436',NULL,1,0,0),(2076,2071,1,'/���','�3�',NULL,NULL,'dbd09f4c',NULL,1,0,0),(2077,2076,1,'��3J','	?h7',NULL,NULL,'4bb1174f',NULL,1,0,0),(2078,2077,1,'ev','U�T�',NULL,NULL,'c01b3f9a',NULL,1,0,0),(2079,2078,8,'+\n�q','�pW�',NULL,NULL,'4c3ca14d',NULL,1,0,0),(2080,2071,1,'ha_�','�fZ',NULL,NULL,'2ad36598',NULL,1,0,0),(2081,2080,1,'�Ҽ�','Y`�g',NULL,NULL,'5bdb3958',NULL,1,0,0),(2082,2081,1,'7\0�3','xM� ',NULL,NULL,'0b9cf6f6',NULL,1,0,0),(2083,2082,8,'�3�','�/-w',NULL,NULL,'d21ddc4d',NULL,1,0,0),(2084,NULL,1,'�Gl','Ml�',NULL,1561411796,'25df1bd9',95,1,0,0),(2085,2084,1,'*�(','�U�',NULL,NULL,'4318c8bb',NULL,1,0,0),(2086,2085,1,'��#','��j',NULL,NULL,'54052ada',NULL,1,0,0),(2087,2086,1,'{�8','F�J�',NULL,NULL,'5c8bf474',NULL,1,0,0),(2088,2087,8,'TJ�E','�IG',NULL,NULL,'f22f3028',NULL,1,0,0),(2089,2084,1,'�M��','#v?�',NULL,NULL,'79ddc285',NULL,1,0,0),(2090,2089,1,'�\"CQ','21��',NULL,NULL,'00f2d68f',NULL,1,0,0),(2091,2090,1,'c��','_���',NULL,NULL,'27a18322',NULL,1,0,0),(2092,2091,8,'P!�',' ���',NULL,NULL,'107823a9',NULL,1,0,0),(2093,NULL,1,'��u{','��_�',NULL,1561411797,'d4a891db',96,1,0,0),(2094,2093,1,'�Ȑ�','O��!',NULL,NULL,'12d4ff9a',NULL,1,0,0),(2095,2094,1,'�ȭ1','t��',NULL,NULL,'a8d48459',NULL,1,0,0),(2096,2095,1,'�H��','�P',NULL,NULL,'7d606266',NULL,1,0,0),(2097,2096,8,'c���','�1�k',NULL,NULL,'c4f05069',NULL,1,0,0),(2098,2093,1,'�&�\0','k\Z+',NULL,NULL,'da6a810c',NULL,1,0,0),(2099,2098,8,'�D�7','D��',NULL,NULL,'9bc886d1',NULL,1,0,0),(2100,NULL,1,'�`��','|]|�',NULL,1561411797,'b7580bb0',97,1,0,0),(2101,2100,1,'}���','�[��',NULL,NULL,'95308573',NULL,1,0,0),(2102,2101,1,'uP�b','F|o',NULL,NULL,'6d778cd6',NULL,1,0,0),(2103,2102,1,'�P�\\','�Ă�',NULL,NULL,'27f0c414',NULL,1,0,0),(2104,2103,8,'�','�pJ9',NULL,NULL,'239048f2',NULL,1,0,0),(2105,NULL,1,'*vQ','9o��',NULL,1561411797,'3981a207',98,1,0,0),(2106,2105,1,'�U','���|',NULL,NULL,'da7d8818',NULL,1,0,0),(2107,2106,1,'|ŊF','�]%�',NULL,NULL,'0f9dbdf1',NULL,1,0,0),(2108,2107,8,'דyo','KW�6',NULL,NULL,'7cd69027',NULL,1,0,0),(2109,2107,1,'*���','\r��',NULL,NULL,'1a54b496',NULL,1,0,0),(2110,2109,8,'�E;','ʃ',NULL,NULL,'8f62c782',NULL,1,0,0),(2111,2107,8,'M���','I0�F',NULL,NULL,'2801b269',NULL,1,0,0),(2112,2107,1,'TFR','c�6\"',NULL,NULL,'b1d6aa11',NULL,1,0,0),(2113,2112,8,'c��O','�X�',NULL,NULL,'b6345b8c',NULL,1,0,0),(2114,2107,8,'��','����',NULL,NULL,'fcb4198d',NULL,1,0,0),(2115,NULL,1,'���','/�Ҫ',NULL,1561411798,'f78886a6',99,1,0,0),(2116,2115,1,'<�0','���',NULL,NULL,'614c0f8f',NULL,1,0,0),(2117,2116,1,'�D��','�]w',NULL,NULL,'5aa4a1b2',NULL,1,0,0),(2118,2117,8,'���','�z�L',NULL,NULL,'7279343f',NULL,1,0,0),(2119,2117,1,'(H,�','[���',NULL,NULL,'cc7d032d',NULL,1,0,0),(2120,2119,8,'um','a ��',NULL,NULL,'57a38b30',NULL,1,0,0),(2121,2116,1,'��\"�','f��O',NULL,NULL,'ca322228',NULL,1,0,0),(2122,2121,8,'u�a�','�̢',NULL,NULL,'19456008',NULL,1,0,0),(2123,2121,1,'\reR','B�%f',NULL,NULL,'87c69518',NULL,1,0,0),(2124,2123,8,'�)�','Hj?E',NULL,NULL,'b806a3bd',NULL,1,0,0),(2125,2116,1,'h�)2','�sR�',NULL,NULL,'0bfa1f2a',NULL,1,0,0),(2126,2125,8,'��t�','����',NULL,NULL,'7b333bb9',NULL,1,0,0),(2127,2125,1,'o*}&','8��',NULL,NULL,'c7047a69',NULL,1,0,0),(2128,2127,8,'�M4','�o��',NULL,NULL,'6361ff67',NULL,1,0,0),(2129,2116,1,'E�','��&	',NULL,NULL,'622645a3',NULL,1,0,0),(2130,2129,8,'-��|','O<Q',NULL,NULL,'85bb3975',NULL,1,0,0),(2131,2129,1,'�EH+','Ƚ�l',NULL,NULL,'33d725cb',NULL,1,0,0),(2132,2131,8,'��K','K�0`',NULL,NULL,'afc2c4d3',NULL,1,0,0),(2133,2116,1,'��','F��D',NULL,NULL,'5f841d39',NULL,1,0,0),(2134,2133,8,'��X','�B�',NULL,NULL,'91ab0dc3',NULL,1,0,0),(2135,2133,1,'��a�','��5',NULL,NULL,'724359a5',NULL,1,0,0),(2136,2135,8,'	�Z','e[0',NULL,NULL,'1997cd6a',NULL,1,0,0),(2137,NULL,1,'u�-','0��A',NULL,1561411799,'1c0fdbb0',100,1,0,0),(2138,2137,1,'�p�','���',NULL,NULL,'afcf9fb8',NULL,1,0,0),(2139,2138,1,'̗\"p',' �',NULL,NULL,'b1525165',NULL,1,0,0),(2140,2139,8,'!3�','R�&',NULL,NULL,'545a836c',NULL,1,0,0),(2141,2139,1,'���a','e��W',NULL,NULL,'601bf3b3',NULL,1,0,0),(2142,2141,8,'u6s�','��L+',NULL,NULL,'c134aa09',NULL,1,0,0),(2143,NULL,1,'��bz','�9�%',NULL,1561411799,'c2dc3790',101,1,0,0),(2144,2143,1,'z�5','��?',NULL,NULL,'fa45ad40',NULL,1,0,0),(2145,2144,1,'��\n','\"�G�',NULL,NULL,'95d33c85',NULL,1,0,0),(2146,2145,8,'�)Q','��?',NULL,NULL,'58a81c0c',NULL,1,0,0),(2147,2145,8,'V�,',':])',NULL,NULL,'021a9dd4',NULL,1,0,0),(2148,NULL,1,'eW�','\"�<G',NULL,1561411799,'15a9b838',102,1,0,0),(2149,2148,1,'��','�1��',NULL,NULL,'7fb1c874',NULL,1,0,0),(2150,2149,1,'�;',']��',NULL,NULL,'1f65a362',NULL,1,0,0),(2151,2150,8,'�	l�','���p',NULL,NULL,'75bca886',NULL,1,0,0),(2152,2149,1,'i�','0�\Z',NULL,NULL,'7a87da57',NULL,1,0,0),(2153,2152,1,'$','%�ļ',NULL,NULL,'adbcafdb',NULL,1,0,0),(2154,2153,1,'���\0','?���',NULL,NULL,'9413c465',NULL,1,0,0),(2155,2154,8,'I(&�','�$��',NULL,NULL,'35c44492',NULL,1,0,0),(2156,NULL,1,':�','1�',NULL,1561411799,'4442b55c',103,1,0,0),(2157,2156,1,'�j$','�i;�',NULL,NULL,'bf2a71c1',NULL,1,0,0),(2158,2157,1,'I`8�','\0��',NULL,NULL,'117700b8',NULL,1,0,0),(2159,2158,8,'t�4p','�e0�',NULL,NULL,'588f5391',NULL,1,0,0),(2160,2157,1,'�3�','#\'?�',NULL,NULL,'03c223d4',NULL,1,0,0),(2161,2160,1,'�U�','\Z��',NULL,NULL,'8b2f5208',NULL,1,0,0),(2162,2161,8,'���','�-�m',NULL,NULL,'b9bb55b7',NULL,1,0,0),(2163,2160,1,'��<','|0r',NULL,NULL,'0275c12b',NULL,1,0,0),(2164,2163,8,'��U','vgW',NULL,NULL,'628f615a',NULL,1,0,0),(2165,2160,1,')\'��','��J�',NULL,NULL,'1259d26a',NULL,1,0,0),(2166,2165,8,'�A��',',���',NULL,NULL,'8f5184f3',NULL,1,0,0),(2167,2160,1,'w<�','�&ϸ',NULL,NULL,'d033a79c',NULL,1,0,0),(2168,2167,8,'16�T','��C',NULL,NULL,'bcb52fcc',NULL,1,0,0),(2169,NULL,1,'1EX','��0',NULL,1561411800,'85d851c6',104,1,0,0),(2170,2169,1,'��(r','P{D�',NULL,NULL,'7586746d',NULL,1,0,0),(2171,2170,1,';�\n�','/���',NULL,NULL,'da40a030',NULL,1,0,0),(2172,2171,8,'�\"O','�5�\\',NULL,NULL,'36aab1f0',NULL,1,0,0),(2173,2170,1,'�]�p','���t',NULL,NULL,'7934b435',NULL,1,0,0),(2174,2173,1,'D��','su�',NULL,NULL,'136f05c4',NULL,1,0,0),(2175,2174,8,'x_��','�d',NULL,NULL,'9f7d7711',NULL,1,0,0),(2176,2173,8,'���','`��',NULL,NULL,'c91b22c4',NULL,1,0,0),(2177,2173,1,'\r�x/','O�Wi',NULL,NULL,'6bd6c7fb',NULL,1,0,0),(2178,2177,8,'D4�O','�K\Z',NULL,NULL,'12ac68a4',NULL,1,0,0),(2179,2173,8,'�mm',' \'+8',NULL,NULL,'081af7cc',NULL,1,0,0),(2180,2173,1,'�	,','w]R',NULL,NULL,'d4cb0cc3',NULL,1,0,0),(2181,2180,8,'_R	U','7��',NULL,NULL,'aa401ca2',NULL,1,0,0),(2182,NULL,1,'G�','tt',NULL,1561411800,'f9c6f625',105,1,0,0),(2183,2182,1,'1VK','�2��',NULL,NULL,'13d3ab0c',NULL,1,0,0),(2184,2183,1,'�\"�m','����',NULL,NULL,'7858d489',NULL,1,0,0),(2185,2184,8,'�=�\\','@�9q',NULL,NULL,'5397d168',NULL,1,0,0),(2186,2183,1,'�YY','װI�',NULL,NULL,'ab11559c',NULL,1,0,0),(2187,2186,1,'O�T�','/��Z',NULL,NULL,'faad1c10',NULL,1,0,0),(2188,2187,8,'3�mg','<\\)1',NULL,NULL,'609c7a5c',NULL,1,0,0),(2189,2186,8,'����','jlG',NULL,NULL,'9a1cb9fa',NULL,1,0,0),(2190,2186,1,'�QS','��HQ',NULL,NULL,'99fa7c4d',NULL,1,0,0),(2191,2190,8,'DE-','8�b\'',NULL,NULL,'a633ab7c',NULL,1,0,0),(2192,NULL,1,'��','��(�',NULL,1561411801,'c0731b25',106,1,0,0),(2193,2192,1,'�y#','dP�r',NULL,NULL,'9cb33422',NULL,1,0,0),(2194,2193,1,'�uO','���+',NULL,NULL,'48f6a8ca',NULL,1,0,0),(2195,2194,8,'L�\Z','��*�',NULL,NULL,'6f477d61',NULL,1,0,0),(2196,2193,1,'D}��','7�g�',NULL,NULL,'807340bf',NULL,1,0,0),(2197,2196,1,'���\r','9�@',NULL,NULL,'66d804ba',NULL,1,0,0),(2198,2197,8,'�� �',']�',NULL,NULL,'19c3d352',NULL,1,0,0),(2199,NULL,1,'h���','�(��',NULL,1561411801,'b1b6c136',107,1,0,0),(2200,2199,1,'�+B#','f��',NULL,NULL,'3bc086fd',NULL,1,0,0),(2201,2200,1,'̵�Y','k4�_',NULL,NULL,'8661fdf7',NULL,1,0,0),(2202,2201,8,'�e�','\'���',NULL,NULL,'85c05052',NULL,1,0,0),(2203,2200,1,'� %\n','{\'��',NULL,NULL,'21dfa38d',NULL,1,0,0),(2204,2203,8,'ě�','J��d',NULL,NULL,'f3d851c7',NULL,1,0,0),(2205,2200,1,'�kf','aDga',NULL,NULL,'df2a4635',NULL,1,0,0),(2206,2205,8,'�;L�','((� ',NULL,NULL,'06598278',NULL,1,0,0),(2207,2200,1,'�I','h���',NULL,NULL,'ba1ad9dd',NULL,1,0,0),(2208,2207,8,'��','�i��',NULL,NULL,'4b34f06b',NULL,1,0,0),(2209,2200,1,'l��','�a�!',NULL,NULL,'21d76169',NULL,1,0,0),(2210,2209,8,'�m�','�G`y',NULL,NULL,'0d1f1307',NULL,1,0,0),(2211,2200,1,'\r<','g(',NULL,NULL,'21ddb7b3',NULL,1,0,0),(2212,2211,8,'�a��','e�p2',NULL,NULL,'a8517d9b',NULL,1,0,0),(2213,NULL,1,'3clx','�e-�',NULL,1561411802,'a4d2b665',108,1,0,0),(2214,2213,1,'�]\\','*��',NULL,NULL,'a9f46897',NULL,1,0,0),(2215,2214,1,'W\Z!$','_��2',NULL,NULL,'2bbf94b6',NULL,1,0,0),(2216,2215,8,'U�$V','���\r',NULL,NULL,'37b5f6d2',NULL,1,0,0),(2217,2214,1,'�U�','?bq1',NULL,NULL,'b8a5d4d5',NULL,1,0,0),(2218,2217,8,'_	#','[�',NULL,NULL,'644fc205',NULL,1,0,0),(2219,2214,1,'��:','�;9�',NULL,NULL,'a095d4c3',NULL,1,0,0),(2220,2219,8,'�%�','ϕ��',NULL,NULL,'58630c65',NULL,1,0,0),(2221,2214,1,':7�','4R<<',NULL,NULL,'6fd2fb6f',NULL,1,0,0),(2222,2221,8,':b,','�\Z(&',NULL,NULL,'786ac328',NULL,1,0,0),(2223,NULL,1,'�Em','}�f�',NULL,1561411802,'d01af678',109,1,0,0),(2224,2223,1,'F�V','�',NULL,NULL,'9696bdcb',NULL,1,0,0),(2225,2224,1,'�H$','Ǳu�',NULL,NULL,'9bb42993',NULL,1,0,0),(2226,2225,8,'���\Z','Mp-',NULL,NULL,'8b8b64f5',NULL,1,0,0),(2227,2224,1,'��G','i!�	',NULL,NULL,'5690bfa5',NULL,1,0,0),(2228,2227,8,'��ǫ','���',NULL,NULL,'ddc36d75',NULL,1,0,0),(2229,2224,1,'����','�f�C',NULL,NULL,'7843d6a6',NULL,1,0,0),(2230,2229,8,'��x','bR��',NULL,NULL,'2d1a8527',NULL,1,0,0),(2231,NULL,1,'@k�0','j��{',NULL,1561411803,'11725a2d',110,1,0,0),(2232,2231,1,'a���','Iep�',NULL,NULL,'63ba6440',NULL,1,0,0),(2233,2232,1,'��� ','�F\"�',NULL,NULL,'3989052c',NULL,1,0,0),(2234,2233,8,'����',' p`�',NULL,NULL,'ddf9386f',NULL,1,0,0),(2235,2232,1,'ȉ�6','����',NULL,NULL,'100a6f06',NULL,1,0,0),(2236,2235,8,'�#t�','�H��',NULL,NULL,'49571b27',NULL,1,0,0),(2237,NULL,1,'kX;t','��o�',NULL,1561411803,'cf3111dd',111,1,0,0),(2238,2237,1,'�q��','�R �',NULL,NULL,'73512cd2',NULL,1,0,0),(2239,2238,1,'hY�a','\\�s',NULL,NULL,'d1d57156',NULL,1,0,0),(2240,2239,8,'q\"R�','�՘�',NULL,NULL,'abb1d4f9',NULL,1,0,0),(2241,2238,8,'5\'o','�Y�',NULL,NULL,'c97b9906',NULL,1,0,0),(2242,2237,1,'(��v','��b1',NULL,NULL,'389b4972',NULL,1,0,0),(2243,2242,1,'}��','O0x�',NULL,NULL,'49fc373f',NULL,1,0,0),(2244,2243,8,'���0','��u�',NULL,NULL,'1a5a3b1f',NULL,1,0,0),(2245,2242,8,'�W]','M(&�',NULL,NULL,'19a64244',NULL,1,0,0),(2246,NULL,1,')U��','�C�U',NULL,1561411803,'7a7f4324',112,1,0,0),(2247,2246,1,'([;\"','�̊',NULL,NULL,'503a851d',NULL,1,0,0),(2248,2247,1,'߿�\0','����',NULL,NULL,'d8f35940',NULL,1,0,0),(2249,2248,8,';�o','��#�',NULL,NULL,'19ad245c',NULL,1,0,0),(2250,2247,8,'�xpY','\0X',NULL,NULL,'7f3801c7',NULL,1,0,0),(2251,2246,1,'�q','�#X',NULL,NULL,'981bf5d6',NULL,1,0,0),(2252,2251,8,'�rf0',':�',NULL,NULL,'91145118',NULL,1,0,0),(2253,NULL,1,'a�\"','�D�{',NULL,1561411804,'7a5367fd',113,1,0,0),(2254,2253,1,'�Ċ�','q� ',NULL,NULL,'9300dd03',NULL,1,0,0),(2255,2254,1,'��	','���',NULL,NULL,'a951a188',NULL,1,0,0),(2256,2255,8,'�8�]','�5!�',NULL,NULL,'38238925',NULL,1,0,0),(2257,2254,8,'::G8',':?Ux',NULL,NULL,'af1323a5',NULL,1,0,0),(2258,NULL,1,'GP`','\r�oZ',NULL,1561411804,'2c388979',114,1,0,0),(2259,2258,1,'���','p���',NULL,NULL,'902095f9',NULL,1,0,0),(2260,2259,1,'�P��','b]u�',NULL,NULL,'7669c76b',NULL,1,0,0),(2261,2260,8,'$e��',';���',NULL,NULL,'3f1ab2b5',NULL,1,0,0),(2262,2258,1,'v�(�','�ԫM',NULL,NULL,'dc3f4958',NULL,1,0,0),(2263,2262,1,'1:','ݍ�',NULL,NULL,'736aa1c1',NULL,1,0,0),(2264,2263,1,'�H','�1�9',NULL,NULL,'61153252',NULL,1,0,0),(2265,2264,1,'���','ں�U',NULL,NULL,'7b927673',NULL,1,0,0),(2266,2265,1,'u��','��\\�',NULL,NULL,'0f9dba9b',NULL,1,0,0),(2267,2266,8,'گ3/','�A\Z5',NULL,NULL,'fa9059cc',NULL,1,0,0),(2268,2263,1,'��7�','��Y\Z',NULL,NULL,'20585b76',NULL,1,0,0),(2269,2268,1,':�\\�','s`��',NULL,NULL,'2a047a86',NULL,1,0,0),(2270,2269,1,'aև',',���',NULL,NULL,'d6a3cdd3',NULL,1,0,0),(2271,2270,8,'_�G','�ŕS',NULL,NULL,'60006c67',NULL,1,0,0),(2272,NULL,1,'�-J','w �q',NULL,1561411804,'0cac408d',115,1,0,0),(2273,2272,1,'YV�\'','R',NULL,NULL,'56a993a7',NULL,1,0,0),(2274,2273,1,'vf`�','{܅�',NULL,NULL,'927459d3',NULL,1,0,0),(2275,2274,8,'k&�','���',NULL,NULL,'7f414b26',NULL,1,0,0),(2276,2272,1,'�B?D','?��',NULL,NULL,'581ca2bc',NULL,1,0,0),(2277,2276,1,'���\0','���z',NULL,NULL,'25a42882',NULL,1,0,0),(2278,2277,1,'&��','#�Ę',NULL,NULL,'7dad7039',NULL,1,0,0),(2279,2278,1,'Z���','�;&X',NULL,NULL,'aff8502c',NULL,1,0,0),(2280,2279,8,'ZHs�','i��0',NULL,NULL,'baa192ac',NULL,1,0,0),(2281,2276,1,'��]p','?�]4',NULL,NULL,'7a4f74da',NULL,1,0,0),(2282,2281,1,'���','8T`',NULL,NULL,'fba284b6',NULL,1,0,0),(2283,2282,1,'�I]','���',NULL,NULL,'1cdb4c97',NULL,1,0,0),(2284,2283,8,'֋','��9�',NULL,NULL,'902c815f',NULL,1,0,0),(2285,2276,1,'7t%','��',NULL,NULL,'18f2f876',NULL,1,0,0),(2286,2285,1,'(%�','�c$s',NULL,NULL,'8b922184',NULL,1,0,0),(2287,2286,1,'ai�','�H3�',NULL,NULL,'9ac23352',NULL,1,0,0),(2288,2287,8,'25','<q�<',NULL,NULL,'aa5b1bc0',NULL,1,0,0),(2289,NULL,1,'�b��','¢7�',NULL,1561411805,'f68d7122',116,1,0,0),(2290,2289,1,'+��','bp��',NULL,NULL,'36410d73',NULL,1,0,0),(2291,2290,1,'���!','g�[�',NULL,NULL,'569d6fd5',NULL,1,0,0),(2292,2291,8,'Uw�','ʴo�',NULL,NULL,'c0dbdba3',NULL,1,0,0),(2293,2289,1,'%aP','PI�}',NULL,NULL,'03a4b506',NULL,1,0,0),(2294,2293,1,'��а','����',NULL,NULL,'acbc043f',NULL,1,0,0),(2295,2294,1,'�E�','�|�C',NULL,NULL,'a1a893a7',NULL,1,0,0),(2296,2295,1,'ԃ�','4�V',NULL,NULL,'9f909676',NULL,1,0,0),(2297,2296,8,'j{r�','�\'s�',NULL,NULL,'11b41118',NULL,1,0,0),(2298,2293,1,'�:W�','�',NULL,NULL,'fa16691a',NULL,1,0,0),(2299,2298,1,')/','-ɐ',NULL,NULL,'a323919c',NULL,1,0,0),(2300,2299,1,'[�5�','�\\��',NULL,NULL,'acb22c1b',NULL,1,0,0),(2301,2300,8,'ŰF','���',NULL,NULL,'254aa730',NULL,1,0,0),(2302,NULL,1,'Ì�','VG<',NULL,1561411806,'b9afd825',117,1,0,0),(2303,2302,1,'�}�','�z�',NULL,NULL,'20dcab94',NULL,1,0,0),(2304,2303,1,'�4lC','�Y(�',NULL,NULL,'a98b016b',NULL,1,0,0),(2305,2304,8,'�i?','r��{',NULL,NULL,'89630a49',NULL,1,0,0),(2306,2302,1,'&�ӻ','o!=�',NULL,NULL,'b8791aa5',NULL,1,0,0),(2307,2306,1,'��','k� �',NULL,NULL,'30a20737',NULL,1,0,0),(2308,2307,8,'*��w','��	�',NULL,NULL,'f18fd5df',NULL,1,0,0),(2309,2302,1,'�?�','',NULL,NULL,'46377f7d',NULL,1,0,0),(2310,2309,1,'w���','x�OA',NULL,NULL,'62086837',NULL,1,0,0),(2311,2310,8,'��@�','�fi�',NULL,NULL,'bf4138b2',NULL,1,0,0),(2312,2302,1,'��q#','%a�',NULL,NULL,'d8731831',NULL,1,0,0),(2313,2312,1,'�$�','�	�',NULL,NULL,'7d736556',NULL,1,0,0),(2314,2313,8,'R&��','O��',NULL,NULL,'12f94c3b',NULL,1,0,0),(2315,2302,1,'X��','�\'ɖ',NULL,NULL,'17788260',NULL,1,0,0),(2316,2315,1,'l#��','��\nf',NULL,NULL,'9f4f4c41',NULL,1,0,0),(2317,2316,8,'[0�','�f�',NULL,NULL,'f0d97c2d',NULL,1,0,0),(2318,2302,1,'�((w','�6g',NULL,NULL,'fd061f20',NULL,1,0,0),(2319,2318,1,'ZK�','�\ZT�',NULL,NULL,'84c8c048',NULL,1,0,0),(2320,2319,8,'\0r�;',';�ؑ',NULL,NULL,'c7506d10',NULL,1,0,0),(2321,NULL,1,'��:','b���',NULL,1561411807,'2f786c60',118,1,0,0),(2322,2321,1,'4\'ϱ','8;4',NULL,NULL,'3342f551',NULL,1,0,0),(2323,2322,1,'+�','�-\0�',NULL,NULL,'7d6faaf7',NULL,1,0,0),(2324,2323,8,'��I�','Kw��',NULL,NULL,'bb4dad7d',NULL,1,0,0),(2325,2321,1,'�R��','��',NULL,NULL,'c05cc52a',NULL,1,0,0),(2326,2325,1,'UZo�','�B�E',NULL,NULL,'80236863',NULL,1,0,0),(2327,2326,8,'BG �','��?o',NULL,NULL,'f5847568',NULL,1,0,0),(2328,2321,1,'b��E','��F#',NULL,NULL,'6a54d990',NULL,1,0,0),(2329,2328,1,'�?�T','s��#',NULL,NULL,'2c383c26',NULL,1,0,0),(2330,2329,8,'ë́�','bYb�',NULL,NULL,'cafd859d',NULL,1,0,0),(2331,2321,1,'�bP�','� \"',NULL,NULL,'bd3f3348',NULL,1,0,0),(2332,2331,1,'�5�`','�5��',NULL,NULL,'958163c3',NULL,1,0,0),(2333,2332,8,';�&','���',NULL,NULL,'f0a7b00c',NULL,1,0,0),(2334,2321,1,'�&�6','jA�}',NULL,NULL,'31b25cc2',NULL,1,0,0),(2335,2334,1,'�/�J','�Ԉ�',NULL,NULL,'0b7bfa61',NULL,1,0,0),(2336,2335,8,'ذ��','c��',NULL,NULL,'05d50afb',NULL,1,0,0),(2337,2321,1,'F\Z','��',NULL,NULL,'86b73185',NULL,1,0,0),(2338,2337,1,'��4','����',NULL,NULL,'521923b0',NULL,1,0,0),(2339,2338,8,'D�h�','�5',NULL,NULL,'f031c689',NULL,1,0,0),(2340,2321,1,'[�','4_Q6',NULL,NULL,'c81c268b',NULL,1,0,0),(2341,2340,1,'|�+','���',NULL,NULL,'018a5ba4',NULL,1,0,0),(2342,2341,8,'�� �','�:D',NULL,NULL,'22205781',NULL,1,0,0),(2343,NULL,1,'7�','�3)',NULL,1561411808,'a759742f',119,1,0,0),(2344,2343,1,'F ʄ','�',NULL,NULL,'964f8220',NULL,1,0,0),(2345,2344,1,' e�V','�j[',NULL,NULL,'0fccd0ac',NULL,1,0,0),(2346,2345,8,'��G�','���',NULL,NULL,'8cc640b7',NULL,1,0,0),(2347,2343,1,'v�SH','׬p',NULL,NULL,'37b3c172',NULL,1,0,0),(2348,2347,1,'V�Ư','��ۣ',NULL,NULL,'3a184033',NULL,1,0,0),(2349,2348,8,'0/��','9K��',NULL,NULL,'cfb9a2a2',NULL,1,0,0),(2350,2343,1,'aHƕ','vC�S',NULL,NULL,'20181737',NULL,1,0,0),(2351,2350,1,'6�','���v',NULL,NULL,'42454733',NULL,1,0,0),(2352,2351,8,'��<	','BC|Q',NULL,NULL,'36f40cd2',NULL,1,0,0),(2353,2343,1,'@\Z­','��\r',NULL,NULL,'867dac84',NULL,1,0,0),(2354,2353,1,'��{u','1<1h',NULL,NULL,'454a42d6',NULL,1,0,0),(2355,2354,8,'1�7','f�d3',NULL,NULL,'49a0fdb4',NULL,1,0,0),(2356,2343,1,'��O�','et�',NULL,NULL,'ad6b0a8c',NULL,1,0,0),(2357,2356,1,'�L��','v��',NULL,NULL,'05f92619',NULL,1,0,0),(2358,2357,8,'�\\3f','��m}',NULL,NULL,'b3351385',NULL,1,0,0),(2359,2343,1,'���$','қ�',NULL,NULL,'b80750f2',NULL,1,0,0),(2360,2359,1,'����','��q�',NULL,NULL,'757813cf',NULL,1,0,0),(2361,2360,8,'�ٛ�','z{��',NULL,NULL,'315afac6',NULL,1,0,0),(2362,2343,1,'Ѽ�','��Ė',NULL,NULL,'846b87f6',NULL,1,0,0),(2363,2362,1,'�oKv','�Y=�',NULL,NULL,'6f703ad4',NULL,1,0,0),(2364,2363,8,'otb�','�[r',NULL,NULL,'52713b85',NULL,1,0,0),(2365,2343,1,'�R,X','̭	\\',NULL,NULL,'f348ab6d',NULL,1,0,0),(2366,2365,1,'�H�','��c',NULL,NULL,'05f8c076',NULL,1,0,0),(2367,2366,8,'B�0�','J�F�',NULL,NULL,'83d89247',NULL,1,0,0),(2368,2343,1,'\rD�','�uߋ',NULL,NULL,'d27c03db',NULL,1,0,0),(2369,2368,1,'���','m��L',NULL,NULL,'1a390624',NULL,1,0,0),(2370,2369,8,'XU','<|��',NULL,NULL,'41011854',NULL,1,0,0),(2371,NULL,1,'T\'z','o�L�',NULL,1561411808,'6c41601c',120,1,0,0),(2372,2371,1,'5�A','*�\\�',NULL,NULL,'126d6415',NULL,1,0,0),(2373,2372,1,'v�e�','ʋOv',NULL,NULL,'9b6fb825',NULL,1,0,0),(2374,2373,8,'�iO�','�J-9',NULL,NULL,'6a8d5765',NULL,1,0,0),(2375,2371,1,'\"0��','=;xf',NULL,NULL,'236c7951',NULL,1,0,0),(2376,2375,1,'ei�w','F�M5',NULL,NULL,'d84d40d9',NULL,1,0,0),(2377,2376,8,'��P','av�',NULL,NULL,'109b0dac',NULL,1,0,0),(2378,2371,1,'X�','9�K�',NULL,NULL,'20aa6b14',NULL,1,0,0),(2379,2378,1,'b\\2','���',NULL,NULL,'468a396c',NULL,1,0,0),(2380,2379,8,'p؍�','����',NULL,NULL,'2dc8f96b',NULL,1,0,0),(2381,2371,1,'���s','Ջ\'',NULL,NULL,'287fa8cb',NULL,1,0,0),(2382,2381,1,'[�3�','Dp��',NULL,NULL,'380a7955',NULL,1,0,0),(2383,2382,8,'_@PT','��L',NULL,NULL,'96966392',NULL,1,0,0),(2384,2371,1,'U7ps','ہ��',NULL,NULL,'d9b177a4',NULL,1,0,0),(2385,2384,1,'p<:','f�c(',NULL,NULL,'82fbfd91',NULL,1,0,0),(2386,2385,8,'j�/',':�',NULL,NULL,'9b08a568',NULL,1,0,0),(2387,2371,1,'i5�','[gd',NULL,NULL,'76bdc388',NULL,1,0,0),(2388,2387,1,'80dQ','\':��',NULL,NULL,'d3c10624',NULL,1,0,0),(2389,2388,8,'v�','��9',NULL,NULL,'c2afd84d',NULL,1,0,0),(2390,2371,1,'+6�|','!oQ',NULL,NULL,'73a86b64',NULL,1,0,0),(2391,2390,1,'�fSI','�kZ',NULL,NULL,'c2f8acfa',NULL,1,0,0),(2392,2391,8,'�S��',' ��',NULL,NULL,'ff2bf770',NULL,1,0,0),(2393,2371,1,'C�=','A�\Z�',NULL,NULL,'5446575d',NULL,1,0,0),(2394,2393,1,'X��','j�',NULL,NULL,'bc47173f',NULL,1,0,0),(2395,2394,8,'a�y�','`�W',NULL,NULL,'780d1374',NULL,1,0,0),(2396,NULL,1,'��k','�R�',NULL,1561411809,'10ad3419',121,1,0,0),(2397,2396,1,'�d�','bʺ�',NULL,NULL,'0390a7d2',NULL,1,0,0),(2398,2397,1,'ə۬','0ߒ',NULL,NULL,'6b3d2950',NULL,1,0,0),(2399,2398,8,'2R�m','���',NULL,NULL,'2fb41165',NULL,1,0,0),(2400,2396,1,'d�Ӝ','z�\r�',NULL,NULL,'f3f25d39',NULL,1,0,0),(2401,2400,1,'B�,l','�\n{',NULL,NULL,'54014da5',NULL,1,0,0),(2402,2401,8,'�#�g','�z��',NULL,NULL,'bb2cb125',NULL,1,0,0),(2403,2396,1,'�U��','a[e',NULL,NULL,'432d73cf',NULL,1,0,0),(2404,2403,1,'+|w','�ͧS',NULL,NULL,'d9194788',NULL,1,0,0),(2405,2404,8,'/��P','�aE�',NULL,NULL,'baf4f08c',NULL,1,0,0),(2406,2396,1,',�','��Y�',NULL,NULL,'f9fcb9bc',NULL,1,0,0),(2407,2406,1,'\n���','9!�T',NULL,NULL,'5ab5c5bd',NULL,1,0,0),(2408,2407,8,'�$��','{H�=',NULL,NULL,'349ad3a1',NULL,1,0,0),(2409,2396,1,'3\rX','����',NULL,NULL,'fc2d06d7',NULL,1,0,0),(2410,2409,1,'o���','Tdm,',NULL,NULL,'4c2b2725',NULL,1,0,0),(2411,2410,8,'X�&',',Ϛ',NULL,NULL,'99d84f08',NULL,1,0,0),(2412,NULL,1,'=��','%�',NULL,1561411810,'cba57854',122,1,0,0),(2413,2412,1,'?og�','W�\Z�',NULL,NULL,'05717d53',NULL,1,0,0),(2414,2413,1,'�\n]\Z','J��',NULL,NULL,'115d9b52',NULL,1,0,0),(2415,2414,8,'����','�@iY',NULL,NULL,'4a2b6119',NULL,1,0,0),(2416,2412,1,'�v','up��',NULL,NULL,'ca52d081',NULL,1,0,0),(2417,2416,1,'#���','�q�',NULL,NULL,'6c2cc5b4',NULL,1,0,0),(2418,2417,8,'M','�5',NULL,NULL,'28656559',NULL,1,0,0),(2419,2412,1,'�`*','\Z��',NULL,NULL,'177b2537',NULL,1,0,0),(2420,2419,1,'�,��','��Z�',NULL,NULL,'996fab16',NULL,1,0,0),(2421,2420,8,'�Ŵ�','0}	',NULL,NULL,'4f6685f2',NULL,1,0,0),(2422,2412,1,'��','��\Z�',NULL,NULL,'a80178ff',NULL,1,0,0),(2423,2422,1,'ŀ�z','�[YR',NULL,NULL,'930b0346',NULL,1,0,0),(2424,2423,8,'p-��','�8�M',NULL,NULL,'bf27178b',NULL,1,0,0),(2425,NULL,1,'�Q��','ܓ#*',NULL,1561411811,'f98cf65b',123,1,0,0),(2426,2425,1,'��&�','��Zj',NULL,NULL,'bd3694f4',NULL,1,0,0),(2427,2426,1,'�','��L',NULL,NULL,'3575dd5f',NULL,1,0,0),(2428,2427,8,'��T','QDo',NULL,NULL,'f9868ff0',NULL,1,0,0),(2429,2425,1,'�Y�','o\'c�',NULL,NULL,'2672f37c',NULL,1,0,0),(2430,2429,1,'�)gP','�iږ',NULL,NULL,'61131480',NULL,1,0,0),(2431,2430,8,'\'-��','3�g',NULL,NULL,'61486611',NULL,1,0,0),(2432,2425,1,'1B�T','3c�',NULL,NULL,'f4b3b16c',NULL,1,0,0),(2433,2432,1,'<p�y','O!O',NULL,NULL,'3bc34539',NULL,1,0,0),(2434,2433,8,'Jt��','Z �O',NULL,NULL,'0a140aa2',NULL,1,0,0),(2435,NULL,1,'8#��',';��$',NULL,1561411811,'c87a27fa',124,1,0,0),(2436,2435,1,'*��','3 ��',NULL,NULL,'49dbb21a',NULL,1,0,0),(2437,2436,1,'�b�','�z:',NULL,NULL,'85d45fff',NULL,1,0,0),(2438,2437,8,'�g','�tt',NULL,NULL,'3820b63d',NULL,1,0,0),(2439,2435,1,'m$','����',NULL,NULL,'fdd3776d',NULL,1,0,0),(2440,2439,1,'Yp','�\0',NULL,NULL,'ab6d3871',NULL,1,0,0),(2441,2440,8,'(Ҭ�','�t�H',NULL,NULL,'4b10cff7',NULL,1,0,0),(2442,NULL,1,'LY','I?��',NULL,1561411811,'c8960968',125,1,0,0),(2443,2442,1,'m!��','l\n��',NULL,NULL,'31158f81',NULL,1,0,0),(2444,2443,1,'pq��','*�G�',NULL,NULL,'62dd8acd',NULL,1,0,0),(2445,2444,8,'��,s','�S?',NULL,NULL,'36f366f8',NULL,1,0,0),(2446,2442,1,'dՖ�','�1-l',NULL,NULL,'54736c6f',NULL,1,0,0),(2447,2446,8,'v���','��v�',NULL,NULL,'5bf102f8',NULL,1,0,0),(2448,2442,1,'ʰ3�','7�i',NULL,NULL,'352dd594',NULL,1,0,0),(2449,2448,8,'g\rx`','\'@؏',NULL,NULL,'a1c2df14',NULL,1,0,0),(2450,2442,1,'��q�','ܼw0',NULL,NULL,'c6b11f08',NULL,1,0,0),(2451,2450,8,'�{Y-','��fH',NULL,NULL,'7c643c6a',NULL,1,0,0),(2452,2442,1,'Q�V�','<���',NULL,NULL,'f4a9477d',NULL,1,0,0),(2453,2452,8,'���','i��,',NULL,NULL,'3af0c148',NULL,1,0,0),(2454,2442,1,'���W','I��9',NULL,NULL,'7abf1769',NULL,1,0,0),(2455,2454,8,'S�S/','So�I',NULL,NULL,'a1a3a1af',NULL,1,0,0),(2456,2442,1,'�F��','`g�',NULL,NULL,'fbb0caaf',NULL,1,0,0),(2457,2456,8,'���','ZQ�',NULL,NULL,'b44d3ff6',NULL,1,0,0),(2458,NULL,1,'�\Z','��',NULL,1561411812,'1fca921d',126,1,0,0),(2459,2458,1,'\Zv�c','z#�',NULL,NULL,'734c306a',NULL,1,0,0),(2460,2459,1,',Cy�','d��T',NULL,NULL,'9f58bb9d',NULL,1,0,0),(2461,2460,8,'�H��','#t��',NULL,NULL,'ffdd6b52',NULL,1,0,0),(2462,2458,1,'y��','�0Y&',NULL,NULL,'5585aa56',NULL,1,0,0),(2463,2462,8,'1h�O','+ �m',NULL,NULL,'96a27002',NULL,1,0,0),(2464,2458,1,'�,�','\"��',NULL,NULL,'8f5453da',NULL,1,0,0),(2465,2464,8,'��','q�@�',NULL,NULL,'0140d0bf',NULL,1,0,0),(2466,NULL,1,'�su','�B��',NULL,1561411812,'4d4622a3',127,1,0,0),(2467,2466,1,'���,','����',NULL,NULL,'b2bc19dc',NULL,1,0,0),(2468,2467,1,'�b{8','/��(',NULL,NULL,'3f154613',NULL,1,0,0),(2469,2468,8,'_<;','�*�q',NULL,NULL,'1acb11b2',NULL,1,0,0),(2470,2466,1,'|�i','v�',NULL,NULL,'dda02faa',NULL,1,0,0),(2471,2470,8,'6uE�','�9%\n',NULL,NULL,'2217fb90',NULL,1,0,0),(2472,NULL,1,'\n��','Ь�',NULL,1561411813,'3f823201',128,1,0,0),(2473,2472,1,'��m','�5�',NULL,NULL,'7566d2b3',NULL,1,0,0),(2474,2473,1,'����','_���',NULL,NULL,'00d0f178',NULL,1,0,0),(2475,2474,8,'7�A','���',NULL,NULL,'cdf3d517',NULL,1,0,0),(2476,NULL,1,'��w�','�&�g',NULL,1561411813,'d1c824f0',129,1,0,0),(2477,2476,1,'��gW','�3\r$',NULL,NULL,'b8412735',NULL,1,0,0),(2478,2477,8,'�f׸','�g�y',NULL,NULL,'02689ffb',NULL,1,0,0),(2479,2477,1,'{\'��','�!c',NULL,NULL,'fac46503',NULL,1,0,0),(2480,2479,1,'K�];','�&',NULL,NULL,'93373372',NULL,1,0,0),(2481,2480,8,'�ًp','�|��',NULL,NULL,'6779919c',NULL,1,0,0),(2482,2477,8,'[o�','�RH�',NULL,NULL,'6600309b',NULL,1,0,0),(2483,NULL,1,'Q �z','ֻ��',NULL,1561411813,'9ff8b604',130,1,0,0),(2484,2483,1,'�','�Ti<',NULL,NULL,'427793af',NULL,1,0,0),(2485,2484,8,'�sVv','r��',NULL,NULL,'28b69367',NULL,1,0,0),(2486,2484,1,'(#h0','�z��',NULL,NULL,'fdc3ac8d',NULL,1,0,0),(2487,2486,1,'��X�','A�-�',NULL,NULL,'7b28a209',NULL,1,0,0),(2488,2487,8,'1�\Z9','7�ǲ',NULL,NULL,'94f5879b',NULL,1,0,0),(2489,NULL,1,';��p','8ek�',NULL,1561411813,'a053af59',131,1,0,0),(2490,2489,1,'���u','#�D�',NULL,NULL,'2d2a1bb4',NULL,1,0,0),(2491,2490,8,'UF�m','qv��',NULL,NULL,'f67c80cc',NULL,1,0,0),(2492,2490,1,'��iO','|\n�y',NULL,NULL,'91da7c78',NULL,1,0,0),(2493,2492,8,'�xj`','d���',NULL,NULL,'a484399a',NULL,1,0,0),(2494,2490,8,'���','E�b',NULL,NULL,'8a2a2813',NULL,1,0,0),(2495,2490,1,'�\Z�z','���',NULL,NULL,'01fd5f0f',NULL,1,0,0),(2496,2495,8,'�&2�','1��',NULL,NULL,'cd10ffa2',NULL,1,0,0),(2497,2490,8,'H�v','�JG8',NULL,NULL,'33c28311',NULL,1,0,0),(2498,NULL,1,'gp�','��w�',NULL,1561411814,'9864f874',132,1,0,0),(2499,2498,1,'�3K:','\\s#',NULL,NULL,'9574d1aa',NULL,1,0,0),(2500,2499,8,'��M','�\r��',NULL,NULL,'0db948cb',NULL,1,0,0),(2501,2499,1,'��Xm','\n�',NULL,NULL,'4c3a96b4',NULL,1,0,0),(2502,2501,8,'q��3','<y@0',NULL,NULL,'f55d19b3',NULL,1,0,0),(2503,2499,8,'_���','���',NULL,NULL,'210c16ad',NULL,1,0,0),(2504,NULL,1,'�h?','U��<',NULL,1561411814,'737f9493',133,1,0,0),(2505,2504,1,'E5��','Zu]T',NULL,NULL,'a1950dba',NULL,1,0,0),(2506,2505,8,'|�3�',':��',NULL,NULL,'f8370661',NULL,1,0,0),(2507,2505,1,'Oͱ?','E!�6',NULL,NULL,'84641763',NULL,1,0,0),(2508,2507,8,';p�q','��k�',NULL,NULL,'710bbaca',NULL,1,0,0),(2509,2504,1,'_�\Z�','+��',NULL,NULL,'10690030',NULL,1,0,0),(2510,2509,1,'��\\�','4=�g',NULL,NULL,'a7b03793',NULL,1,0,0),(2511,2510,8,'׍�}','�pWV',NULL,NULL,'5a9db17f',NULL,1,0,0),(2512,NULL,1,'�','�F�',NULL,1561411815,'8fcbc812',134,1,0,0),(2513,2512,1,'[{�','��WE',NULL,NULL,'0a032f03',NULL,1,0,0),(2514,2513,8,'�]-d','����',NULL,NULL,'604d5b36',NULL,1,0,0),(2515,2513,1,'fh�k','�LM2',NULL,NULL,'7da7ba45',NULL,1,0,0),(2516,2515,8,'mULc','��',NULL,NULL,'5520a997',NULL,1,0,0),(2517,2512,1,'RY#\\','}CÒ',NULL,NULL,'b5b59cfa',NULL,1,0,0),(2518,2517,8,'�9�]','��',NULL,NULL,'72da6811',NULL,1,0,0),(2519,2517,1,'\\,�','&r�\\',NULL,NULL,'c6c5cbb9',NULL,1,0,0),(2520,2519,8,'i*�','�Ņ�',NULL,NULL,'16870b6a',NULL,1,0,0),(2521,2512,1,'dK<V','����',NULL,NULL,'100960af',NULL,1,0,0),(2522,2521,8,'�6�','�C�',NULL,NULL,'d597970c',NULL,1,0,0),(2523,2521,1,'R��','�W��',NULL,NULL,'15bcdbb1',NULL,1,0,0),(2524,2523,8,'�fX�','h*�',NULL,NULL,'ad7ffaff',NULL,1,0,0),(2525,2512,1,'J�i�','G���',NULL,NULL,'183d6a15',NULL,1,0,0),(2526,2525,8,'��:�','�\0\0{',NULL,NULL,'394bdffa',NULL,1,0,0),(2527,2525,1,'����','��ZC',NULL,NULL,'b9c133a2',NULL,1,0,0),(2528,2527,8,'TU�','��E',NULL,NULL,'8197c53b',NULL,1,0,0),(2529,2512,1,'_�','Jb�b',NULL,NULL,'5705bb57',NULL,1,0,0),(2530,2529,8,'����',';\\	',NULL,NULL,'d068449d',NULL,1,0,0),(2531,2529,1,'�Tt','V`��',NULL,NULL,'0c256439',NULL,1,0,0),(2532,2531,8,'i�z�','�o�',NULL,NULL,'2c9d3605',NULL,1,0,0),(2533,NULL,1,'�[�','����',NULL,1561411815,'bb40095c',135,1,0,0),(2534,2533,1,'<���','���\n',NULL,NULL,'caf4c84b',NULL,1,0,0),(2535,2534,8,'�7�','��E',NULL,NULL,'fb8fa199',NULL,1,0,0),(2536,2534,1,'���','�,�',NULL,NULL,'baba38ba',NULL,1,0,0),(2537,2536,8,' O%�','0�i�',NULL,NULL,'cb9c2c60',NULL,1,0,0),(2538,NULL,1,'QZ�','Y7��',NULL,1561411816,'fa3c5630',136,1,0,0),(2539,2538,1,'�cS]','f��w',NULL,NULL,'295c9cad',NULL,1,0,0),(2540,2539,8,'t5p�','])��',NULL,NULL,'12208b5a',NULL,1,0,0),(2541,2539,8,'c�`','`\0�',NULL,NULL,'d6274c9d',NULL,1,0,0),(2542,2539,1,'��','\Zf�',NULL,NULL,'3d1d12c7',NULL,1,0,0),(2543,2542,8,'$�6�','�/V4',NULL,NULL,'ff7ccd87',NULL,1,0,0),(2544,2538,1,'S�)S','�_xy',NULL,NULL,'32495fb8',NULL,1,0,0),(2545,2544,8,'�	6�','F�%',NULL,NULL,'278fa59d',NULL,1,0,0),(2546,NULL,1,'gF�','\"e\Z(',NULL,1561411816,'d702c5d9',137,1,0,0),(2547,2546,1,'���q','�Y�+',NULL,NULL,'01736faf',NULL,1,0,0),(2548,2547,8,'K,','թ{�',NULL,NULL,'5d9d3597',NULL,1,0,0),(2549,2547,8,'4�z','���m',NULL,NULL,'f687c45b',NULL,1,0,0),(2550,NULL,1,'1��|','6wz',NULL,1561411816,'6ab08caf',138,1,0,0),(2551,2550,1,'�J7�','=�M',NULL,NULL,'a1fc44d0',NULL,1,0,0),(2552,2551,8,'���S','����',NULL,NULL,'046b49aa',NULL,1,0,0),(2553,2550,1,'Ȳ�\0','AgE�',NULL,NULL,'86079278',NULL,1,0,0),(2554,2553,1,'đ�X','�7\"D',NULL,NULL,'d62588cb',NULL,1,0,0),(2555,2554,1,'����','G�b�',NULL,NULL,'5abb239c',NULL,1,0,0),(2556,2555,1,'�\ZE','���',NULL,NULL,'c2a12756',NULL,1,0,0),(2557,2556,8,'��/D','�WXP',NULL,NULL,'52355d40',NULL,1,0,0),(2558,NULL,1,'lE-8','y��%',NULL,1561411817,'260d0235',139,1,0,0),(2559,2558,1,'��5A','9%@�',NULL,NULL,'bc7f0004',NULL,1,0,0),(2560,2559,8,'Zg�D','Ç�X',NULL,NULL,'cb652514',NULL,1,0,0),(2561,2558,1,'�\0�','��v-',NULL,NULL,'bc12361b',NULL,1,0,0),(2562,2561,1,'��5�','Ϣ��',NULL,NULL,'a93555f7',NULL,1,0,0),(2563,2562,1,'U�t�','�rY�',NULL,NULL,'a0998685',NULL,1,0,0),(2564,2563,8,'��Ǔ','�G�C',NULL,NULL,'31913f9f',NULL,1,0,0),(2565,2561,1,'���}','dc�',NULL,NULL,'7064dff5',NULL,1,0,0),(2566,2565,1,'Q�W8','�q�o',NULL,NULL,'4059b4d9',NULL,1,0,0),(2567,2566,8,'y�gE','��`T',NULL,NULL,'72f0aa6b',NULL,1,0,0),(2568,2561,1,'\"76�','�<2�',NULL,NULL,'14b88acd',NULL,1,0,0),(2569,2568,1,'�p�','I1�',NULL,NULL,'6ac5f0aa',NULL,1,0,0),(2570,2569,8,'Ϛ�/','M�ږ',NULL,NULL,'6362f44d',NULL,1,0,0),(2571,2561,1,'zJZ8','�WUB',NULL,NULL,'d6cfda2f',NULL,1,0,0),(2572,2571,1,'�8��','4�\\�',NULL,NULL,'479871a8',NULL,1,0,0),(2573,2572,8,'!4�','9�XA',NULL,NULL,'8308c116',NULL,1,0,0),(2574,2561,1,'i3h�','�p	�',NULL,NULL,'6cc30a98',NULL,1,0,0),(2575,2574,1,'%\\','R�4�',NULL,NULL,'780d2db1',NULL,1,0,0),(2576,2575,8,'U�o','/\\',NULL,NULL,'da9f4b05',NULL,1,0,0),(2577,NULL,1,'%!�','ӣD',NULL,1561411817,'ad89b71f',140,1,0,0),(2578,2577,1,'1A3','�X�C',NULL,NULL,'1a916cd0',NULL,1,0,0),(2579,2578,8,'w�\r','5\";i',NULL,NULL,'f48d261b',NULL,1,0,0),(2580,2577,1,'ӱk�','���',NULL,NULL,'d7292761',NULL,1,0,0),(2581,2580,1,'�ߧ3','5���',NULL,NULL,'854639a7',NULL,1,0,0),(2582,2581,1,'d��','�Z�',NULL,NULL,'0592f0d9',NULL,1,0,0),(2583,2582,8,'5��{','�%9�',NULL,NULL,'5ada1208',NULL,1,0,0),(2584,2580,1,'�8O','���S',NULL,NULL,'472a459f',NULL,1,0,0),(2585,2584,1,'5�d','Y(��',NULL,NULL,'75f0a880',NULL,1,0,0),(2586,2585,8,'TO��','�(vy',NULL,NULL,'ca0b7c67',NULL,1,0,0),(2587,2580,1,'��/�','�)�',NULL,NULL,'223a53c1',NULL,1,0,0),(2588,2587,1,'\Z]��','|�D�',NULL,NULL,'3aff0b37',NULL,1,0,0),(2589,2588,8,'�R�','��\"�',NULL,NULL,'a0f326a9',NULL,1,0,0),(2590,2580,1,'�?�','F�o',NULL,NULL,'ba55208f',NULL,1,0,0),(2591,2590,1,'Ds','�DAm',NULL,NULL,'064a46f9',NULL,1,0,0),(2592,2591,8,'Yt8','}o�',NULL,NULL,'d185b837',NULL,1,0,0),(2593,2580,1,'@�g�','F',NULL,NULL,'202adda7',NULL,1,0,0),(2594,2593,1,'��','	�۽',NULL,NULL,'f909dc95',NULL,1,0,0),(2595,2594,8,')�L','7E�',NULL,NULL,'daa6b88f',NULL,1,0,0),(2596,2580,1,'�B�','r�w�',NULL,NULL,'7f816c5d',NULL,1,0,0),(2597,2596,1,'r���','+$@',NULL,NULL,'3573a57b',NULL,1,0,0),(2598,2597,8,'t�!h','0W3y',NULL,NULL,'cc9acaf6',NULL,1,0,0),(2599,NULL,1,'@���','�&f',NULL,1561411818,'664acdcd',141,1,0,0),(2600,2599,1,'d���','h��',NULL,NULL,'74341c6d',NULL,1,0,0),(2601,2600,8,'�:��','�I��',NULL,NULL,'8131a521',NULL,1,0,0),(2602,2599,1,'�a-e','��u<',NULL,NULL,'0f1ba3ca',NULL,1,0,0),(2603,2602,1,'zc�L','��V0',NULL,NULL,'9b64ffcd',NULL,1,0,0),(2604,2603,1,'���','0K��',NULL,NULL,'fd481574',NULL,1,0,0),(2605,2604,8,'�3','��sg',NULL,NULL,'c9b8bdb1',NULL,1,0,0),(2606,2602,1,'��գ',')ch�',NULL,NULL,'987c7647',NULL,1,0,0),(2607,2606,1,'F9z','0��',NULL,NULL,'02af9c21',NULL,1,0,0),(2608,2607,8,'=�\"7','Q���',NULL,NULL,'0066f2fc',NULL,1,0,0),(2609,2602,1,'���','�;a�',NULL,NULL,'179037c4',NULL,1,0,0),(2610,2609,1,'���','��',NULL,NULL,'63618f12',NULL,1,0,0),(2611,2610,8,'�*�','�4g',NULL,NULL,'313064f7',NULL,1,0,0),(2612,NULL,1,'��-�','4%!',NULL,1561411819,'8cab8874',142,1,0,0),(2613,2612,1,'�p','�Eb\\',NULL,NULL,'86951481',NULL,1,0,0),(2614,2613,8,'�&�','_:�',NULL,NULL,'5b254809',NULL,1,0,0),(2615,2612,1,'s��','��2�',NULL,NULL,'52a19b97',NULL,1,0,0),(2616,2615,1,'!�[','HzM�',NULL,NULL,'911b3bab',NULL,1,0,0),(2617,2616,1,'�f�p','+l�',NULL,NULL,'26cb0f0d',NULL,1,0,0),(2618,2617,8,'/�Y�','��R',NULL,NULL,'86b16982',NULL,1,0,0),(2619,2615,1,'����','���0',NULL,NULL,'bb4221df',NULL,1,0,0),(2620,2619,1,'a�v�','ġ��',NULL,NULL,'d1c2d02a',NULL,1,0,0),(2621,2620,8,'�?�?','rpF�',NULL,NULL,'72341079',NULL,1,0,0),(2622,NULL,1,'�R�\'',']9|',NULL,1561411819,'4069ca71',143,1,0,0),(2623,2622,1,'�B�','qPU',NULL,NULL,'f550cf0a',NULL,1,0,0),(2624,2623,8,'���','�S�',NULL,NULL,'166aa769',NULL,1,0,0),(2625,2622,1,'X?�','�U�',NULL,NULL,'a851a455',NULL,1,0,0),(2626,2625,1,'��Y','Ő��',NULL,NULL,'8472429c',NULL,1,0,0),(2627,2626,1,'��	<','�]Fo',NULL,NULL,'a9c50d85',NULL,1,0,0),(2628,2627,8,'��?','�h� ',NULL,NULL,'841cd699',NULL,1,0,0),(2629,NULL,1,'xa˪','�he9',NULL,1561411819,'6c4f07ca',144,1,0,0),(2630,2629,1,'3��+','I���',NULL,NULL,'4c50428a',NULL,1,0,0),(2631,2630,8,'���','�6e',NULL,NULL,'4001dbb6',NULL,1,0,0),(2632,2629,1,'��b�','|=��',NULL,NULL,'d9fa0fb4',NULL,1,0,0),(2633,2632,1,'�3O�','ė�',NULL,NULL,'d42f9086',NULL,1,0,0),(2634,2633,8,'C�h','���',NULL,NULL,'5d5a27c9',NULL,1,0,0),(2635,2632,1,'\'��','k̕',NULL,NULL,'9f180a5f',NULL,1,0,0),(2636,2635,8,'e?','��]�',NULL,NULL,'3a934597',NULL,1,0,0),(2637,2632,1,'���\"','$�<9',NULL,NULL,'17975a84',NULL,1,0,0),(2638,2637,8,'�ȟ/','9�P�',NULL,NULL,'508b89bc',NULL,1,0,0),(2639,2632,1,'���','=��',NULL,NULL,'bf9287cb',NULL,1,0,0),(2640,2639,8,'�9:H','��',NULL,NULL,'5a6d1172',NULL,1,0,0),(2641,2632,1,'qԅ�','���|',NULL,NULL,'b92c6654',NULL,1,0,0),(2642,2641,8,'*�R�','#iw�',NULL,NULL,'4f527ad2',NULL,1,0,0),(2643,2632,1,'��,','�X�',NULL,NULL,'a6acfb8f',NULL,1,0,0),(2644,2643,8,'�kg�','�',NULL,NULL,'4c9bc155',NULL,1,0,0),(2645,2632,1,'C7|�','k�Gk',NULL,NULL,'119dcb03',NULL,1,0,0),(2646,2645,8,'f��','6t�',NULL,NULL,'f8c3289f',NULL,1,0,0),(2647,2632,1,'��J','�Y`�',NULL,NULL,'c3f27471',NULL,1,0,0),(2648,2647,8,'I}fb','m��r',NULL,NULL,'bab26445',NULL,1,0,0),(2649,2632,1,'Dq��','ȇ��',NULL,NULL,'f5b0f334',NULL,1,0,0),(2650,2649,8,';�ˢ','?�Lf',NULL,NULL,'d168d420',NULL,1,0,0),(2651,NULL,1,'|Y�','6XW�',NULL,1561411820,'575aa405',145,1,0,0),(2652,2651,1,'�Eo�','��\\2',NULL,NULL,'351168b4',NULL,1,0,0),(2653,2652,8,'5Dl&','��\r�',NULL,NULL,'98783f3c',NULL,1,0,0),(2654,2651,1,'[+��','Ħ�\r',NULL,NULL,'f01b195f',NULL,1,0,0),(2655,2654,1,'�U�',',G',NULL,NULL,'ccadd357',NULL,1,0,0),(2656,2655,8,'Cq�k','���',NULL,NULL,'645b84c6',NULL,1,0,0),(2657,2654,1,'o��','b�A',NULL,NULL,'ddc38c28',NULL,1,0,0),(2658,2657,8,'s�Y�','��_',NULL,NULL,'6776a657',NULL,1,0,0),(2659,2654,1,'T/h-','k��%',NULL,NULL,'5d7a1f42',NULL,1,0,0),(2660,2659,8,'P�P','�Є�',NULL,NULL,'a4d70a96',NULL,1,0,0),(2661,2654,1,'Qyj\"','�0�',NULL,NULL,'b8cfc8a3',NULL,1,0,0),(2662,2661,8,'rZ\r�','�:�',NULL,NULL,'25c09190',NULL,1,0,0),(2663,NULL,1,']hI�','� ��',NULL,1561411821,'6965b176',146,1,0,0),(2664,2663,1,'�{B','��BF',NULL,NULL,'841745b6',NULL,1,0,0),(2665,2664,8,'&\0ŏ','E�:�',NULL,NULL,'d257c7ba',NULL,1,0,0),(2666,2663,1,'ʸ\'',':�Hd',NULL,NULL,'43084781',NULL,1,0,0),(2667,2666,1,']�a�','q�F�',NULL,NULL,'aadf3214',NULL,1,0,0),(2668,2667,8,'K��','��la',NULL,NULL,'f824b9c5',NULL,1,0,0),(2669,2666,1,'d�@�','�I�',NULL,NULL,'63adf99a',NULL,1,0,0),(2670,2669,8,'�UӶ','���8',NULL,NULL,'1b785b67',NULL,1,0,0),(2671,NULL,1,'$�2U','�ya4',NULL,1561411821,'69400fc3',147,1,0,0),(2672,2671,1,'�i','�v�\"',NULL,NULL,'7865d7d2',NULL,1,0,0),(2673,2672,8,'�\"','��]\n',NULL,NULL,'c66af080',NULL,1,0,0),(2674,2671,1,'��q','���S',NULL,NULL,'8d767060',NULL,1,0,0),(2675,2674,1,'�)d','Ĝ;�',NULL,NULL,'2531117a',NULL,1,0,0),(2676,2675,8,'yj=','�b',NULL,NULL,'bf49a8d1',NULL,1,0,0),(2677,2674,8,'\\�,','`A��',NULL,NULL,'a27555f1',NULL,1,0,0),(2678,2674,1,'*0i�','Ը�',NULL,NULL,'3a184fbd',NULL,1,0,0),(2679,2678,8,'%�w=','�5��',NULL,NULL,'ba1b500a',NULL,1,0,0),(2680,2674,8,'VFq','r|=�',NULL,NULL,'f1152992',NULL,1,0,0),(2681,2674,1,'��a�','�8%Q',NULL,NULL,'ab4c12a5',NULL,1,0,0),(2682,2681,8,'��g7','����',NULL,NULL,'b6d03f13',NULL,1,0,0),(2683,NULL,1,'L�hf','vyۊ',NULL,1561411822,'a21ba522',148,1,0,0),(2684,2683,1,'G[��','|OL',NULL,NULL,'6a706b69',NULL,1,0,0),(2685,2684,8,'?P�','�b]�',NULL,NULL,'6ff4fd58',NULL,1,0,0),(2686,2683,1,'�; ','/(�',NULL,NULL,'10cbc0c1',NULL,1,0,0),(2687,2686,1,'���x','g�ݓ',NULL,NULL,'b7f376a9',NULL,1,0,0),(2688,2687,8,'�Pu�','��t�',NULL,NULL,'5b9cd9f5',NULL,1,0,0),(2689,2686,8,'��h','w��[',NULL,NULL,'0dc510cb',NULL,1,0,0),(2690,2686,1,'OV��','����',NULL,NULL,'0a1157fc',NULL,1,0,0),(2691,2690,8,'`�O�','}��t',NULL,NULL,'10115332',NULL,1,0,0),(2692,NULL,1,'���M','$Gê',NULL,1561411822,'fa17f88b',149,1,0,0),(2693,2692,1,'���','jzԷ',NULL,NULL,'679c7044',NULL,1,0,0),(2694,2693,8,'�zS�','���',NULL,NULL,'604ba36a',NULL,1,0,0),(2695,2692,1,'-U�','�%\\',NULL,NULL,'fca51865',NULL,1,0,0),(2696,2695,1,'�Y@','h�߱',NULL,NULL,'7d290c98',NULL,1,0,0),(2697,2696,8,'0ۓ�','��',NULL,NULL,'d176b09c',NULL,1,0,0),(2698,2692,1,'�\'��','џ��',NULL,NULL,'c5d1883f',NULL,1,0,0),(2699,2698,8,'phX','t��_',NULL,NULL,'7950c1f7',NULL,1,0,0),(2700,NULL,1,'t?��','�S�',NULL,1561411822,'c991c78b',150,1,0,0),(2701,2700,1,'��jg','<�ʟ',NULL,NULL,'c87c727d',NULL,1,0,0),(2702,2701,8,'�x��','\Zo��',NULL,NULL,'c65f3ba7',NULL,1,0,0),(2703,2700,1,'�܉)','���',NULL,NULL,'4402c4d1',NULL,1,0,0),(2704,2703,1,'c�','g�',NULL,NULL,'a9985f33',NULL,1,0,0),(2705,2704,8,'�p��','��A',NULL,NULL,'6070fdfa',NULL,1,0,0),(2706,NULL,1,'�Z��',' f�',NULL,1561411823,'281f0875',151,1,0,0),(2707,2706,1,'�\'@','�4�$',NULL,NULL,'4534a24c',NULL,1,0,0),(2708,2707,8,'ɻ�\Z','�|U',NULL,NULL,'d6f78c98',NULL,1,0,0),(2709,2706,1,'\r��{','��C�',NULL,NULL,'788f593c',NULL,1,0,0),(2710,2709,8,'���E','�Ľ�',NULL,NULL,'0af8b02b',NULL,1,0,0),(2711,2709,8,'(\Z6','�<h',NULL,NULL,'5065c629',NULL,1,0,0),(2712,2709,8,'��ӊ','�D��',NULL,NULL,'5a42a782',NULL,1,0,0),(2713,NULL,1,'w��','��� ',NULL,1561411823,'bc626a1c',152,1,0,0),(2714,2713,1,'�\"�','L�E',NULL,NULL,'7ddb55f0',NULL,1,0,0),(2715,2714,8,'@qk�','ϛ�A',NULL,NULL,'1f07bf0d',NULL,1,0,0),(2716,2713,1,'��A','/\"��',NULL,NULL,'a4454065',NULL,1,0,0),(2717,2716,8,'?�\\&','�@�',NULL,NULL,'01a0d643',NULL,1,0,0),(2718,2716,8,'o��\"','�[�',NULL,NULL,'491401c4',NULL,1,0,0),(2719,NULL,1,'�f�','\'pc',NULL,1561411823,'5759bfba',153,1,0,0),(2720,2719,1,'|','��!Q',NULL,NULL,'c79dac46',NULL,1,0,0),(2721,2720,8,'��Zz','�\Z��',NULL,NULL,'68d89d00',NULL,1,0,0),(2722,2719,1,'C�dJ','�??-',NULL,NULL,'c4fb1f6b',NULL,1,0,0),(2723,2722,8,'�B}','�k',NULL,NULL,'bb6bcbfd',NULL,1,0,0),(2724,2719,1,'Iq6�','wZ',NULL,NULL,'9227571d',NULL,1,0,0),(2725,2724,1,'1�3�','�v�',NULL,NULL,'0c54f15a',NULL,1,0,0),(2726,2725,1,'p��','ц|�',NULL,NULL,'68d38c63',NULL,1,0,0),(2727,2726,8,'z�ʍ','�!?L',NULL,NULL,'9a840495',NULL,1,0,0),(2728,NULL,1,'�I_','�UC�',NULL,1561411824,'59490f14',154,1,0,0),(2729,2728,1,'?L�U','���',NULL,NULL,'13269adf',NULL,1,0,0),(2730,2729,8,'����','j�<p',NULL,NULL,'31870dd8',NULL,1,0,0),(2731,2728,1,'�&�Y','�J�$',NULL,NULL,'93b2a1b3',NULL,1,0,0),(2732,2731,8,'swM','��2�',NULL,NULL,'349a2949',NULL,1,0,0),(2733,2728,1,'�uI�','#ԍc',NULL,NULL,'c5a2fc90',NULL,1,0,0),(2734,2733,8,'c\Z�','<j',NULL,NULL,'78a456da',NULL,1,0,0),(2735,2728,1,'�b�3','/fX}',NULL,NULL,'7f20845f',NULL,1,0,0),(2736,2735,8,'�h&','G�<|',NULL,NULL,'6a05d7ff',NULL,1,0,0),(2737,2728,1,'�X̌','�a�6',NULL,NULL,'ffc8c65a',NULL,1,0,0),(2738,2737,8,'\n5�','���',NULL,NULL,'5828db07',NULL,1,0,0),(2739,2728,1,'M\r��','Giv',NULL,NULL,'daf2c17a',NULL,1,0,0),(2740,2739,8,'�0AE','�1�',NULL,NULL,'050c0b32',NULL,1,0,0),(2741,2728,1,'����','�Z�',NULL,NULL,'3f221f7a',NULL,1,0,0),(2742,2741,8,'B?c','�!�l',NULL,NULL,'c9d60f6d',NULL,1,0,0),(2743,2728,1,'��\Z5','��h�',NULL,NULL,'742fa470',NULL,1,0,0),(2744,2743,8,'B�;','a+�%',NULL,NULL,'d4f35fb3',NULL,1,0,0),(2745,2728,1,'Ϗ��','��;',NULL,NULL,'f8b7d280',NULL,1,0,0),(2746,2745,8,'�t��','[1H`',NULL,NULL,'c20571d4',NULL,1,0,0),(2747,2728,1,'�+�\n','�pm',NULL,NULL,'d572716b',NULL,1,0,0),(2748,2747,8,'�o97','��b�',NULL,NULL,'b224b9f5',NULL,1,0,0),(2749,NULL,1,' T�','&�!',NULL,1561411824,'88891848',155,1,0,0),(2750,2749,1,'p�ʏ','�j[h',NULL,NULL,'1c5dcdac',NULL,1,0,0),(2751,2750,8,'Fo','�',NULL,NULL,'3489982f',NULL,1,0,0),(2752,2749,1,'��f�','`É',NULL,NULL,'6cda08a2',NULL,1,0,0),(2753,2752,8,',FL�','��p',NULL,NULL,'f8777b8f',NULL,1,0,0),(2754,2749,1,'}ˣ�','Yǘ�',NULL,NULL,'ac3fb825',NULL,1,0,0),(2755,2754,8,'p{��','�u��',NULL,NULL,'445a6c26',NULL,1,0,0),(2756,2749,1,'�B]','of�',NULL,NULL,'38464b85',NULL,1,0,0),(2757,2756,8,'�z�','��+�',NULL,NULL,'9fa69ff3',NULL,1,0,0),(2758,2749,1,'%=W%','��|\'',NULL,NULL,'9d205a0f',NULL,1,0,0),(2759,2758,8,'�=','Y�',NULL,NULL,'282db841',NULL,1,0,0),(2760,2749,1,'��ڶ','��',NULL,NULL,'060f12ca',NULL,1,0,0),(2761,2760,8,'����','�8ջ',NULL,NULL,'b4b13109',NULL,1,0,0),(2762,2749,1,'$���','\0Y',NULL,NULL,'80df3378',NULL,1,0,0),(2763,2762,8,'���','U�/�',NULL,NULL,'3870702d',NULL,1,0,0),(2764,2749,1,']#l','���',NULL,NULL,'dc82d5bc',NULL,1,0,0),(2765,2764,8,'El%&','�eJ',NULL,NULL,'93a39a40',NULL,1,0,0),(2766,2749,1,'z���','�WR ',NULL,NULL,'5671ba6c',NULL,1,0,0),(2767,2766,8,'Ӎ�','���',NULL,NULL,'46fad4f3',NULL,1,0,0),(2768,NULL,1,'SK5�',':���',NULL,1561411825,'9044a454',156,1,0,0),(2769,2768,1,'%+��','�ό�',NULL,NULL,'6cd2b4f5',NULL,1,0,0),(2770,2769,8,'��6\\','���',NULL,NULL,'596cc9ba',NULL,1,0,0),(2771,2768,1,'	�)','r��',NULL,NULL,'62fa3974',NULL,1,0,0),(2772,2771,8,'U','\"�,5',NULL,NULL,'a20bf45c',NULL,1,0,0),(2773,2768,1,'5�0�','�˷',NULL,NULL,'9acd67c1',NULL,1,0,0),(2774,2773,8,'�f�','���',NULL,NULL,'7bf3da39',NULL,1,0,0),(2775,2768,1,'��O�','H�|',NULL,NULL,'56b731dd',NULL,1,0,0),(2776,2775,8,'�*=','z�',NULL,NULL,'dc850fb6',NULL,1,0,0),(2777,2768,1,'u{)','�\rbO',NULL,NULL,'bcb6d916',NULL,1,0,0),(2778,2777,8,'6kf�','-,86',NULL,NULL,'290658f1',NULL,1,0,0),(2779,2768,1,'f\\�','�a`',NULL,NULL,'8b93c6f7',NULL,1,0,0),(2780,2779,8,'�C�','hDw',NULL,NULL,'68a00bc1',NULL,1,0,0),(2781,NULL,1,'g?�','\0Ih',NULL,1561411826,'0a9b3c20',157,1,0,0),(2782,2781,1,'K��','�Z�@',NULL,NULL,'c3a9080b',NULL,1,0,0),(2783,2782,8,']?�\'','-)�\'',NULL,NULL,'6a3cdda4',NULL,1,0,0),(2784,2781,1,'�q�','|ۡS',NULL,NULL,'82165a44',NULL,1,0,0),(2785,2784,8,'�O;','�k�T',NULL,NULL,'8bd80659',NULL,1,0,0),(2786,2781,1,'qQ��','o)�A',NULL,NULL,'34ca5cf7',NULL,1,0,0),(2787,2786,8,'S�D@','eM��',NULL,NULL,'a6732612',NULL,1,0,0),(2788,2781,1,'�G��','\r��',NULL,NULL,'7c3c2f15',NULL,1,0,0),(2789,2788,8,'+�@','M�c�',NULL,NULL,'625ff217',NULL,1,0,0),(2790,2781,1,'���','�44�',NULL,NULL,'80885c83',NULL,1,0,0),(2791,2790,8,'EU�0','����',NULL,NULL,'3df76878',NULL,1,0,0),(2792,NULL,1,'���','�B\Z',NULL,1561411826,'202d3c3f',158,1,0,0),(2793,2792,1,'��)','�ߗx',NULL,NULL,'35232a82',NULL,1,0,0),(2794,2793,8,'R�MH','-�q',NULL,NULL,'23d45692',NULL,1,0,0),(2795,2792,1,'�	[','�<r�',NULL,NULL,'42df065b',NULL,1,0,0),(2796,2795,8,'��','�C{',NULL,NULL,'196cf3a0',NULL,1,0,0),(2797,2792,1,'���l','�c�B',NULL,NULL,'c558fafd',NULL,1,0,0),(2798,2797,8,'�%�C','h�S�',NULL,NULL,'a1916b70',NULL,1,0,0),(2799,2792,1,'���','_�A',NULL,NULL,'b40849af',NULL,1,0,0),(2800,2799,8,'��a�','�3U',NULL,NULL,'0d87d5af',NULL,1,0,0),(2801,NULL,1,'�%t',' �Vf',NULL,1561411826,'52a3fbaf',159,1,0,0),(2802,2801,1,'����','�G',NULL,NULL,'4a52ca97',NULL,1,0,0),(2803,2802,8,'���[','�� ',NULL,NULL,'5cd55213',NULL,1,0,0),(2804,2801,1,';�IE','Ow�',NULL,NULL,'8912b7f3',NULL,1,0,0),(2805,2804,8,'+8w/','8\0W]',NULL,NULL,'828abdcc',NULL,1,0,0),(2806,2801,1,'�0�','{',NULL,NULL,'c9d4fb07',NULL,1,0,0),(2807,2806,8,'�I��','6S�',NULL,NULL,'2cd83147',NULL,1,0,0),(2808,NULL,1,'	b29','O�\'',NULL,1561411827,'b8a26720',160,1,0,0),(2809,2808,1,';�X�','M�(p',NULL,NULL,'7af8b2c7',NULL,1,0,0),(2810,2809,8,'�\n�F','p��1',NULL,NULL,'27ca6b9f',NULL,1,0,0),(2811,2808,1,'-ǖ�','�Ԅ�',NULL,NULL,'cf7cab5f',NULL,1,0,0),(2812,2811,8,'buÝ','�}O\"',NULL,NULL,'7f38c17a',NULL,1,0,0),(2813,NULL,1,'�Л�','�q�f',NULL,1561411827,'b2396acc',161,1,0,0),(2814,2813,1,'���','?ɏ�',NULL,NULL,'537839a6',NULL,1,0,0),(2815,2814,8,'�p��','\"�vo',NULL,NULL,'595cd7bf',NULL,1,0,0),(2816,2813,8,'̽��','�sM�',NULL,NULL,'051f9a3b',NULL,1,0,0),(2817,2813,1,'�V��','\"SZ�',NULL,NULL,'bd4a7877',NULL,1,0,0),(2818,2817,8,'��g�','���',NULL,NULL,'5a4719ff',NULL,1,0,0),(2819,2813,8,'��vz','S�#',NULL,NULL,'c5990d76',NULL,1,0,0),(2820,2813,1,'D=_1','f��',NULL,NULL,'d783a03a',NULL,1,0,0),(2821,2820,8,'Ҁ�','�hj',NULL,NULL,'4852d6d9',NULL,1,0,0),(2822,2813,8,'\"','�DG\'',NULL,NULL,'369bc82a',NULL,1,0,0),(2823,2813,1,'��','is[�',NULL,NULL,'1ad73878',NULL,1,0,0),(2824,2823,8,'��BG','����',NULL,NULL,'b8b9b7a1',NULL,1,0,0),(2825,NULL,1,'&<�','�K-�',NULL,1561411827,'97c34892',162,1,0,0),(2826,2825,1,'���','HL�',NULL,NULL,'ff48679a',NULL,1,0,0),(2827,2826,8,'\\�\r','�u��',NULL,NULL,'f0c60408',NULL,1,0,0),(2828,2825,8,'V��7','H���',NULL,NULL,'208fdb70',NULL,1,0,0),(2829,2825,1,',��','Jw\Z�',NULL,NULL,'36674b04',NULL,1,0,0),(2830,2829,8,'�H','ȟX�',NULL,NULL,'20f3a607',NULL,1,0,0),(2831,2825,8,'�,*6','�H�',NULL,NULL,'c273554b',NULL,1,0,0),(2832,2825,1,'E\"��','L�B�',NULL,NULL,'224074fd',NULL,1,0,0),(2833,2832,8,'��I','SGY',NULL,NULL,'1b7738a2',NULL,1,0,0),(2834,2825,8,'��m','M?:',NULL,NULL,'b674acc6',NULL,1,0,0),(2835,NULL,1,'\rK%B','`{�',NULL,1561411828,'1ad5f609',163,1,0,0),(2836,2835,1,'qAIw','����',NULL,NULL,'7601953d',NULL,1,0,0),(2837,2836,8,'�3�#','����',NULL,NULL,'04977b29',NULL,1,0,0),(2838,2835,8,'T8!�','\n�fF',NULL,NULL,'7b96578a',NULL,1,0,0),(2839,2835,1,'��R6','2�+�',NULL,NULL,'07fb7f1b',NULL,1,0,0),(2840,2839,8,'�fH','��v\Z',NULL,NULL,'f5b9ca51',NULL,1,0,0),(2841,2835,8,'�l�','\r/�{',NULL,NULL,'5bc53812',NULL,1,0,0),(2842,2835,1,'v�=','��\0�',NULL,NULL,'43161b60',NULL,1,0,0),(2843,2842,8,',L�)','v�\r&',NULL,NULL,'876f1739',NULL,1,0,0),(2844,NULL,1,'Ð�$','2Lx3',NULL,1561411828,'2c89138f',164,1,0,0),(2845,2844,1,'���','5pc',NULL,NULL,'c20c5327',NULL,1,0,0),(2846,2845,8,'�M�','��	',NULL,NULL,'a2a69679',NULL,1,0,0),(2847,2844,8,'��`J','Q\0�',NULL,NULL,'f6b32b51',NULL,1,0,0),(2848,2844,1,'\r$','���',NULL,NULL,'1da22185',NULL,1,0,0),(2849,2848,8,'s��#','V�s�',NULL,NULL,'2c42cb97',NULL,1,0,0),(2850,NULL,1,'34�=','CcDF',NULL,1561411828,'1c5490fa',165,1,0,0),(2851,2850,1,'j��','uRZ�',NULL,NULL,'69db2787',NULL,1,0,0),(2852,2851,8,',��','eG[',NULL,NULL,'a9944084',NULL,1,0,0),(2853,2850,8,'&\"Ɔ','��v�',NULL,NULL,'b6d3f11a',NULL,1,0,0),(2854,NULL,1,']b9<','\"�=a',NULL,1561411829,'89a73f58',166,1,0,0),(2855,2854,1,'�aJ','ai�\\',NULL,NULL,'2a3315ba',NULL,1,0,0),(2856,2855,8,'�c-�','��\":',NULL,NULL,'3aa6fcdb',NULL,1,0,0),(2857,NULL,1,'��A','t��z',NULL,1561411829,'6a030dab',167,1,0,0),(2858,2857,8,'!�','��H�',NULL,NULL,'5abdfc09',NULL,1,0,0),(2859,2857,1,'݀��','*�j',NULL,NULL,'a44df360',NULL,1,0,0),(2860,2859,1,'-��','�S�',NULL,NULL,'01334720',NULL,1,0,0),(2861,2860,8,'w\"�','@o�	',NULL,NULL,'2c38d5b9',NULL,1,0,0),(2862,2857,8,'��(�','�d�',NULL,NULL,'89989861',NULL,1,0,0),(2863,NULL,1,'8_�','<ۙC',NULL,1561411829,'ab4b0f6a',168,1,0,0),(2864,2863,8,'FwPx','0�l',NULL,NULL,'fcc54482',NULL,1,0,0),(2865,2863,1,'$u�','��mD',NULL,NULL,'4851fd26',NULL,1,0,0),(2866,2865,1,'��`,','Օ��',NULL,NULL,'a2326f27',NULL,1,0,0),(2867,2866,8,'I�jF','�9��',NULL,NULL,'8a9c3c43',NULL,1,0,0),(2868,NULL,1,'ƈA}','���',NULL,1561411829,'9b18549b',169,1,0,0),(2869,2868,8,'ˊ','U\r',NULL,NULL,'1fb9db14',NULL,1,0,0),(2870,2868,1,'��','CV��',NULL,NULL,'796334b9',NULL,1,0,0),(2871,2870,8,'���Y','�3p',NULL,NULL,'c4d55ad2',NULL,1,0,0),(2872,2868,8,'����','X\"�',NULL,NULL,'2560c594',NULL,1,0,0),(2873,2868,1,'�\\�','�l��',NULL,NULL,'7802438d',NULL,1,0,0),(2874,2873,8,'2s�8','!d��',NULL,NULL,'3f890fca',NULL,1,0,0),(2875,2868,8,'@;��','s�',NULL,NULL,'60f07390',NULL,1,0,0),(2876,2868,1,'p@Z�','\Z�1e',NULL,NULL,'32cf47d1',NULL,1,0,0),(2877,2876,8,'��B�','J�',NULL,NULL,'b7c52bac',NULL,1,0,0),(2878,2868,8,'��','�S ',NULL,NULL,'a94bbafb',NULL,1,0,0),(2879,2868,1,'�\Z:4','5@��',NULL,NULL,'5f269525',NULL,1,0,0),(2880,2879,8,'ߍV','��i+',NULL,NULL,'253acb46',NULL,1,0,0),(2881,2868,8,'r���','D�F',NULL,NULL,'3a252007',NULL,1,0,0),(2882,2868,1,'{k�','��B�',NULL,NULL,'5473f885',NULL,1,0,0),(2883,2882,8,'�	�9','g̉',NULL,NULL,'bb68a626',NULL,1,0,0),(2884,2868,8,'x�6','2UÃ',NULL,NULL,'d7072b6f',NULL,1,0,0),(2885,2868,1,'j��','�5��',NULL,NULL,'855b942a',NULL,1,0,0),(2886,2885,8,'�G!!','��[�',NULL,NULL,'4c5fd542',NULL,1,0,0),(2887,2868,8,'��j4','���O',NULL,NULL,'db6c220a',NULL,1,0,0),(2888,NULL,1,'�%6T','�;��',NULL,1561411830,'574bc420',170,1,0,0),(2889,2888,8,'Yш`','�m',NULL,NULL,'d141a180',NULL,1,0,0),(2890,2888,1,'��D','���',NULL,NULL,'7c5006b1',NULL,1,0,0),(2891,2890,8,'z�[','��X',NULL,NULL,'9003dc57',NULL,1,0,0),(2892,2888,8,'��B�',']�8�',NULL,NULL,'0a01c976',NULL,1,0,0),(2893,2888,1,'�(��','�U',NULL,NULL,'fadaaf5c',NULL,1,0,0),(2894,2893,8,'lzc�','\"�0',NULL,NULL,'f75354f1',NULL,1,0,0),(2895,2888,8,'�JZZ','����',NULL,NULL,'86257504',NULL,1,0,0),(2896,NULL,1,'�U[','�#q�',NULL,1561411830,'f0c0b3d8',171,1,0,0),(2897,2896,8,'Y5�','�� y',NULL,NULL,'276907fa',NULL,1,0,0),(2898,2896,1,'�_,','�B�',NULL,NULL,'b3b9f19a',NULL,1,0,0),(2899,2898,8,'�\0(','�',NULL,NULL,'8874f206',NULL,1,0,0),(2900,2896,8,'�$S�','�z�q',NULL,NULL,'af3ab07a',NULL,1,0,0),(2901,2896,1,'q �','�<�{',NULL,NULL,'48b7f63c',NULL,1,0,0),(2902,2901,8,'����','HT�<',NULL,NULL,'a8d44df5',NULL,1,0,0),(2903,NULL,1,'ԋ#','2ye',NULL,1561411831,'aba48130',172,1,0,0),(2904,2903,8,'�F(','�46',NULL,NULL,'a68ad2b1',NULL,1,0,0),(2905,2903,1,'���','��	',NULL,NULL,'583c5f59',NULL,1,0,0),(2906,2905,8,'�t��','R�',NULL,NULL,'c4354c22',NULL,1,0,0),(2907,2903,8,'���','߀�w',NULL,NULL,'1071b8c5',NULL,1,0,0),(2908,NULL,1,'8_j�','�q��',NULL,1561411831,'aca74db2',173,1,0,0),(2909,2908,8,'�!','��#',NULL,NULL,'b028dd8a',NULL,1,0,0),(2910,2908,1,'��#','	«�',NULL,NULL,'64032dbf',NULL,1,0,0),(2911,2910,8,'R�A','�Ѱ',NULL,NULL,'328d761a',NULL,1,0,0),(2912,NULL,1,'�X��','�JU]',NULL,1561411831,'5830ddf0',174,1,0,0),(2913,2912,8,'�6Qy','�<<',NULL,NULL,'690ba98c',NULL,1,0,0),(2914,2912,8,'U�;%','�h',NULL,NULL,'044d53d3',NULL,1,0,0),(2915,2912,1,'�۷�','��S`',NULL,NULL,'a2333475',NULL,1,0,0),(2916,2915,8,'v}T','c@�\'',NULL,NULL,'5d21c4cf',NULL,1,0,0),(2917,NULL,1,'�8A\Z','3���',NULL,1561411831,'35919f84',175,1,0,0),(2918,2917,8,'	4�','��k�',NULL,NULL,'cbd56bca',NULL,1,0,0),(2919,2917,8,'8�\n�','j��',NULL,NULL,'4a5b5bbb',NULL,1,0,0),(2920,2917,8,'+`hw','scݶ',NULL,NULL,'32f9b799',NULL,1,0,0),(2921,2917,8,'��xj','a*hh',NULL,NULL,'a56a1fb0',NULL,1,0,0),(2922,NULL,1,'��&v','S�cY',NULL,1561411832,'05bb630f',176,1,0,0),(2923,2922,8,'hX��','9A�(',NULL,NULL,'9136f8da',NULL,1,0,0),(2924,2922,8,'��b�','W*��',NULL,NULL,'bf255048',NULL,1,0,0),(2925,2922,8,'ƨ�','�Vk',NULL,NULL,'3ab1f819',NULL,1,0,0),(2926,NULL,1,'�I��','��fv',NULL,1561411832,'f900bbc9',177,1,0,0),(2927,2926,8,'�O��','���',NULL,NULL,'0f89c82d',NULL,1,0,0),(2928,2926,8,'�D��','vgS3',NULL,NULL,'84348b40',NULL,1,0,0),(2929,NULL,1,'jA��',':DxM',NULL,1561411832,'03287990',178,1,0,0),(2930,2929,8,'�8','�',NULL,NULL,'1606174a',NULL,1,0,0);
/*!40000 ALTER TABLE `html` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `html_classes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `html_classes`
--

LOCK TABLES `html_classes` WRITE;
/*!40000 ALTER TABLE `html_classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `html_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `html_params`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_params` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `html_id` int(10) unsigned NOT NULL,
  `value` varchar(250) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_html_params_html_id` (`html_id`),
  CONSTRAINT `fk_html_params_html_id` FOREIGN KEY (`html_id`) REFERENCES `html` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `html_params`
--

LOCK TABLES `html_params` WRITE;
/*!40000 ALTER TABLE `html_params` DISABLE KEYS */;
/*!40000 ALTER TABLE `html_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `html_params_datasets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_params_datasets` (
  `html_id` int(10) unsigned NOT NULL,
  `key` varchar(8) NOT NULL,
  `value` varchar(250) NOT NULL,
  KEY `fk_html_params_datasets_html_id` (`html_id`),
  CONSTRAINT `fk_html_params_datasets_html_id` FOREIGN KEY (`html_id`) REFERENCES `html` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `html_params_datasets`
--

LOCK TABLES `html_params_datasets` WRITE;
/*!40000 ALTER TABLE `html_params_datasets` DISABLE KEYS */;
/*!40000 ALTER TABLE `html_params_datasets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `html_params_text`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html_params_text` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `html_id` int(10) unsigned NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `fk_html_params_text_html_id` (`html_id`),
  CONSTRAINT `fk_html_params_text_html_id` FOREIGN KEY (`html_id`) REFERENCES `html` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `html_params_text`
--

LOCK TABLES `html_params_text` WRITE;
/*!40000 ALTER TABLE `html_params_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `html_params_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `html_style`
--

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `html_style` (
  `styles_id` tinyint NOT NULL,
  `kind` tinyint NOT NULL,
  `value` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `images`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `file_id` int(10) unsigned NOT NULL,
  `pixabay_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `deleted` (`deleted`,`file_id`,`user_id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_images_added_by` (`added_by`),
  KEY `fk_images_deleted_by` (`deleted_by`),
  KEY `fk_images_updated_by` (`updated_by`),
  KEY `fk_images_added_ip_id` (`added_ip_id`),
  KEY `fk_images_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_images_updated_ip_id` (`updated_ip_id`),
  KEY `fk_images_user_id` (`user_id`),
  KEY `fk_images_file_id` (`file_id`),
  KEY `fk_images_pixabay_id` (`pixabay_id`),
  CONSTRAINT `fk_images_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_file_id` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_pixabay_id` FOREIGN KEY (`pixabay_id`) REFERENCES `pixabay` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_images_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `images_view`
--

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `images_view` (
  `id` tinyint NOT NULL,
  `uuid` tinyint NOT NULL,
  `user_id` tinyint NOT NULL,
  `file_id` tinyint NOT NULL,
  `pixabay_id` tinyint NOT NULL,
  `deleted` tinyint NOT NULL,
  `tags_count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ip`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `date` int(10) unsigned NOT NULL,
  `session_id` int(10) unsigned DEFAULT NULL,
  `host_name` varchar(250) DEFAULT NULL COMMENT 'Nazwa hosta z którego łączy się użytkownik',
  `country_name` varchar(250) DEFAULT NULL,
  `region_name` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `zip_code` varchar(50) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip` (`ip`,`user_id`,`session_id`),
  KEY `fk_ip_user_id` (`user_id`),
  KEY `fk_ip_session_id` (`session_id`),
  CONSTRAINT `fk_ip_session_id` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_ip_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ip`
--

LOCK TABLES `ip` WRITE;
/*!40000 ALTER TABLE `ip` DISABLE KEYS */;
INSERT INTO `ip` VALUES (1,1,NULL,1558820801,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,1,1,1558824007,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,1,1,1558841517,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,1,NULL,1559354545,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,1,1,1559366688,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,1,1,1559468390,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,1,1,1559475923,8,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,1,1,1559480891,15,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,1,1,1559488760,17,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,1,NULL,1559488780,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,1,1,1559493847,18,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,1,NULL,1559500766,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,1,1,1559512163,21,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,1,NULL,1560113911,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,1,1,1560142360,23,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(16,1,NULL,1560143002,24,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(17,1,NULL,1560171388,NULL,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(18,1,NULL,1560171398,NULL,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(19,1,NULL,1560180167,28,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(20,1,NULL,1560202058,NULL,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(21,1,NULL,1560230888,30,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(22,1,NULL,1560263625,NULL,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(23,1,NULL,1560263648,31,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(24,1,NULL,1560285658,NULL,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(25,1,NULL,1560316940,32,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(26,1,NULL,1560374083,33,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(27,1,NULL,1560378689,34,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(28,1,NULL,1560378641,34,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(29,1,NULL,1560389829,35,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(30,1,NULL,1560400818,36,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL),(31,1,NULL,1561412055,38,'ip6-localhost',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `url` varchar(90) DEFAULT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `pagination_url_param` int(11) DEFAULT NULL COMMENT 'parametr x jest do paginacji',
  `pagination_limit` int(11) DEFAULT NULL COMMENT 'Limit elementów na stronę dla paginacji',
  `user_id` int(10) unsigned NOT NULL,
  `content_id` int(10) unsigned DEFAULT NULL,
  `pagination_url` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `project_id` (`project_id`,`url`,`deleted`,`user_id`),
  KEY `projects_id` (`project_id`),
  KEY `fk_pages_added_by` (`added_by`),
  KEY `fk_pages_deleted_by` (`deleted_by`),
  KEY `fk_pages_updated_by` (`updated_by`),
  KEY `fk_pages_added_ip_id` (`added_ip_id`),
  KEY `fk_pages_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_pages_updated_ip_id` (`updated_ip_id`),
  KEY `fk_pages_user_id` (`user_id`),
  KEY `fk_pages_content_id` (`content_id`),
  CONSTRAINT `fk_pages_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_content_id` FOREIGN KEY (`content_id`) REFERENCES `html` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_datasets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_datasets` (
  `page_id` int(10) unsigned NOT NULL,
  `key` binary(4) NOT NULL,
  `value` text,
  `element_position_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`page_id`,`key`,`element_position_id`) USING BTREE,
  KEY `fk_pages_datasets_element_position_id` (`element_position_id`),
  CONSTRAINT `fk_pages_datasets_page_id` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_datasets`
--

LOCK TABLES `pages_datasets` WRITE;
/*!40000 ALTER TABLE `pages_datasets` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_datasets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `pages_datasets_view`
--

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pages_datasets_view` (
  `page_id` tinyint NOT NULL,
  `key` tinyint NOT NULL,
  `value` tinyint NOT NULL,
  `element_position_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `pages_day_count_view`
--

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pages_day_count_view` (
  `count` tinyint NOT NULL,
  `date` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `pages_day_project_count_view`
--

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pages_day_project_count_view` (
  `project_id` tinyint NOT NULL,
  `count` tinyint NOT NULL,
  `date` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pages_elements`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `uuid` binary(8) NOT NULL,
  `selector` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_pages_elements_element_id` (`element_id`),
  KEY `fk_pages_elements_page_id` (`page_id`),
  CONSTRAINT `fk_pages_elements_element_id` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_pages_elements_page_id` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_elements`
--

LOCK TABLES `pages_elements` WRITE;
/*!40000 ALTER TABLE `pages_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_elements_fields`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_elements_fields` (
  `field` binary(4) NOT NULL,
  `value` varchar(50) NOT NULL,
  `page_element_id` int(10) unsigned NOT NULL,
  `alt_id` binary(4) DEFAULT NULL,
  `id` binary(4) DEFAULT NULL,
  PRIMARY KEY (`field`,`page_element_id`),
  KEY `fk_pages_elements_fields_page_element_id` (`page_element_id`),
  CONSTRAINT `fk_pages_elements_fields_page_element_id` FOREIGN KEY (`page_element_id`) REFERENCES `pages_elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_elements_fields`
--

LOCK TABLES `pages_elements_fields` WRITE;
/*!40000 ALTER TABLE `pages_elements_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_elements_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `pages_month_count_view`
--

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pages_month_count_view` (
  `count` tinyint NOT NULL,
  `date` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `pages_month_project_count_view`
--

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pages_month_project_count_view` (
  `project_id` tinyint NOT NULL,
  `count` tinyint NOT NULL,
  `date` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pixabay`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pixabay` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pixa_id` int(10) unsigned NOT NULL,
  `image` varchar(250) DEFAULT NULL COMMENT 'zdjęcie w rozdzielczości w1280',
  `thumb` varchar(250) NOT NULL COMMENT 'zdjęcie w rozdzielczości w640',
  `link` varchar(250) NOT NULL COMMENT 'link do strony ze zdjęciem w Pixabay',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pixa_id` (`pixa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pixabay`
--

LOCK TABLES `pixabay` WRITE;
/*!40000 ALTER TABLE `pixabay` DISABLE KEYS */;
INSERT INTO `pixabay` VALUES (1,4190813,'https://pixabay.com/get/52e1dc434253af14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.jpg','https://pixabay.com/get/52e1dc434253af14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_640.jpg','https://pixabay.com/photos/oldtimer-auto-retro-automotive-4190813/'),(2,434918,'https://pixabay.com/get/52e3d14a4b5ab108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_1280.jpg','https://pixabay.com/get/52e3d14a4b5ab108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_640.jpg','https://pixabay.com/photos/legs-window-car-dirt-road-relax-434918/'),(3,887272,'https://pixabay.com/get/5ee8d2414d50b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_1280.jpg','https://pixabay.com/get/5ee8d2414d50b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_640.jpg','https://pixabay.com/photos/vintage-1950s-pretty-woman-887272/'),(4,362150,'https://pixabay.com/get/55e6d7424f52b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_1280.jpg','https://pixabay.com/get/55e6d7424f52b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_640.jpg','https://pixabay.com/photos/car-repair-car-workshop-repair-shop-362150/'),(5,49278,'https://pixabay.com/get/52e9d744424fad0bffd8992cc52835781336dbf85254744076277ed0914b_1280.jpg','https://pixabay.com/get/52e9d744424fad0bffd8992cc52835781336dbf85254744076277ed0914b_640.jpg','https://pixabay.com/illustrations/car-sports-car-racing-car-speed-49278/'),(6,1197800,'https://pixabay.com/get/57e1dc444252ac14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.jpg','https://pixabay.com/get/57e1dc444252ac14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_640.jpg','https://pixabay.com/photos/cuba-oldtimer-old-car-forest-red-1197800/'),(7,381233,'https://pixabay.com/get/55e8d4414951b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_1280.jpg','https://pixabay.com/get/55e8d4414951b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_640.jpg','https://pixabay.com/photos/taxi-cab-traffic-cab-new-york-381233/'),(8,158463,'https://pixabay.com/get/57e5dd474c51b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_1280.png','https://pixabay.com/get/57e5dd474c51b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_640.png','https://pixabay.com/vectors/volkswagen-car-bus-mobile-home-158463/'),(9,788747,'https://pixabay.com/get/51e8dd444e55b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_1280.jpg','https://pixabay.com/get/51e8dd444e55b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_640.jpg','https://pixabay.com/photos/auto-car-cadillac-oldtimer-788747/'),(10,1853936,'https://pixabay.com/get/57e8d0404351aa14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.jpg','https://pixabay.com/get/57e8d0404351aa14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_640.jpg','https://pixabay.com/photos/automobile-automotive-beach-beetle-1853936/'),(11,1209321,'https://pixabay.com/get/57e2d54a4950ad14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.jpg','https://pixabay.com/get/57e2d54a4950ad14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_640.jpg','https://pixabay.com/photos/girls-lying-classic-car-young-1209321/'),(12,3046424,'https://pixabay.com/get/55e0d1454e50a814f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.jpg','https://pixabay.com/get/55e0d1454e50a814f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_640.jpg','https://pixabay.com/photos/car-vehicle-motor-transport-3046424/'),(13,887286,'https://pixabay.com/get/5ee8d2414254b108f5d08460962a377c1339d7e04e50744c7c2c73d6974ac3_1280.jpg','https://pixabay.com/get/5ee8d2414254b108f5d08460962a377c1339d7e04e50744c7c2c73d6974ac3_640.jpg','https://pixabay.com/photos/woman-s-legs-high-heels-vintage-car-887286/'),(14,63930,'https://pixabay.com/get/50e3dc404a4fad0bffd8992cc52835781336dbf85254744076277ed0914b_1280.jpg','https://pixabay.com/get/50e3dc404a4fad0bffd8992cc52835781336dbf85254744076277ed0914b_640.jpg','https://pixabay.com/photos/ford-mustang-auto-vehicle-muscle-63930/'),(15,2705402,'https://pixabay.com/get/54e7d5464e52ae14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.jpg','https://pixabay.com/get/54e7d5464e52ae14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_640.jpg','https://pixabay.com/photos/ford-mustang-v8-67-ford-mustang-2705402/'),(16,1300629,'https://pixabay.com/get/57e3d5434c50a514f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.png','https://pixabay.com/get/57e3d5434c50a514f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_640.png','https://pixabay.com/vectors/car-sports-car-luxury-model-auto-1300629/'),(17,918408,'https://pixabay.com/get/5fe1dd474a5ab108f5d08460962a377c1339d7e04e50744c7c2c73d6974ac3_1280.jpg','https://pixabay.com/get/5fe1dd474a5ab108f5d08460962a377c1339d7e04e50744c7c2c73d6974ac3_640.jpg','https://pixabay.com/photos/bmw-car-front-sports-car-tuned-918408/'),(18,438467,'https://pixabay.com/get/52e3dd474c55b108f5d08460962a377c1339d7e04e50744c7c2c73d6974ac3_1280.jpg','https://pixabay.com/get/52e3dd474c55b108f5d08460962a377c1339d7e04e50744c7c2c73d6974ac3_640.jpg','https://pixabay.com/photos/car-race-ferrari-racing-car-pirelli-438467/'),(19,1249610,'https://pixabay.com/get/57e2d14a4c53ac14f6da8c7dda79357e1739d8ec564c704c702678dc954dc25e_1280.jpg','https://pixabay.com/get/57e2d14a4c53ac14f6da8c7dda79357e1739d8ec564c704c702678dc954dc25e_640.jpg','https://pixabay.com/photos/speed-car-vehicle-drive-automobile-1249610/'),(20,1853939,'https://pixabay.com/get/57e8d0404351a514f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.jpg','https://pixabay.com/get/57e8d0404351a514f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_640.jpg','https://pixabay.com/photos/beach-beautiful-beetle-classic-car-1853939/'),(21,2373727,'https://pixabay.com/get/54e3d2404d50ab14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/54e3d2404d50ab14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/airport-transport-woman-girl-2373727/'),(22,841441,'https://pixabay.com/get/5ee4d4474e53b108f5d08460962a377c1339d7e04e50744c7c2c73d1944dc7_1280.jpg','https://pixabay.com/get/5ee4d4474e53b108f5d08460962a377c1339d7e04e50744c7c2c73d1944dc7_640.jpg','https://pixabay.com/photos/plane-trip-journey-explore-841441/'),(23,170272,'https://pixabay.com/get/57e7d5414d50b108f5d08460962a377c1339d7e04e50744c7c2c73d1944dc7_1280.jpg','https://pixabay.com/get/57e7d5414d50b108f5d08460962a377c1339d7e04e50744c7c2c73d1944dc7_640.jpg','https://pixabay.com/photos/plane-aircraft-jet-airbase-airport-170272/'),(24,3184798,'https://pixabay.com/get/55e1dd474d5ba414f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/55e1dd474d5ba414f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/skyscraper-singapore-sky-blue-3184798/'),(25,1712825,'https://pixabay.com/get/57e7d4414250a914f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/57e7d4414250a914f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/girl-boxer-fighter-nude-1712825/'),(26,3526558,'https://pixabay.com/get/55e5d7454f57a414f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/55e5d7454f57a414f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/clouds-sky-dramatic-air-atmosphere-3526558/'),(27,582888,'https://pixabay.com/get/53e8d74b425ab108f5d08460962a377c1339d7e04e50744c7c2c73d1944dc7_1280.jpg','https://pixabay.com/get/53e8d74b425ab108f5d08460962a377c1339d7e04e50744c7c2c73d1944dc7_640.jpg','https://pixabay.com/photos/military-raptor-jet-f-22-airplane-582888/'),(28,63028,'https://pixabay.com/get/50e3d541424fad0bffd8992cc52835781336dbf85254744076277ed7974f_1280.jpg','https://pixabay.com/get/50e3d541424fad0bffd8992cc52835781336dbf85254744076277ed7974f_640.jpg','https://pixabay.com/photos/fighter-jet-fighter-aircraft-63028/'),(29,1807486,'https://pixabay.com/get/57e8d5444e5aaa14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/57e8d5444e5aaa14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/airline-architecture-buildings-city-1807486/'),(30,371412,'https://pixabay.com/get/55e7d4474b50b108f5d08460962a377c1339d7e04e50744c7c2c73d19745cd_1280.jpg','https://pixabay.com/get/55e7d4474b50b108f5d08460962a377c1339d7e04e50744c7c2c73d19745cd_640.jpg','https://pixabay.com/photos/jet-engine-jet-airplane-engine-371412/'),(31,2795557,'https://pixabay.com/get/54e7dc464f57ab14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/54e7dc464f57ab14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/aircraft-double-decker-oldtimer-2795557/'),(32,1984344,'https://pixabay.com/get/57e9dd474956a814f6da8c7dda79357e1739d8ec564c704c702678dc924dcd50_1280.jpg','https://pixabay.com/get/57e9dd474956a814f6da8c7dda79357e1739d8ec564c704c702678dc924dcd50_640.jpg','https://pixabay.com/photos/boxer-power-energy-sport-fighter-1984344/'),(33,63211,'https://pixabay.com/get/50e3d7424b4fad0bffd8992cc52835781336dbf85254744076277ed7974f_1280.jpg','https://pixabay.com/get/50e3d7424b4fad0bffd8992cc52835781336dbf85254744076277ed7974f_640.jpg','https://pixabay.com/photos/supersonic-fighter-aircraft-jet-63211/'),(34,1499171,'https://pixabay.com/get/57e4dc4a4b55ad14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/57e4dc4a4b55ad14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/aircraft-propeller-wing-flying-1499171/'),(35,1846668,'https://pixabay.com/get/57e8d1454c54a414f6da8c7dda79357e1739d8ec564c704c702678dc924dcd50_1280.jpg','https://pixabay.com/get/57e8d1454c54a414f6da8c7dda79357e1739d8ec564c704c702678dc924dcd50_640.jpg','https://pixabay.com/photos/body-boxer-boxing-fighter-strong-1846668/'),(36,659687,'https://pixabay.com/get/50e5dc454255b108f5d08460962a377c1339d7e04e50744c7c2c73d19745cd_1280.jpg','https://pixabay.com/get/50e5dc454255b108f5d08460962a377c1339d7e04e50744c7c2c73d19745cd_640.jpg','https://pixabay.com/photos/air-plane-fighter-night-sky-moon-659687/'),(37,63032,'https://pixabay.com/get/50e3d540484fad0bffd8992cc52835781336dbf85254744076277ed7974f_1280.jpg','https://pixabay.com/get/50e3d540484fad0bffd8992cc52835781336dbf85254744076277ed7974f_640.jpg','https://pixabay.com/photos/fighter-jet-fighter-aircraft-63032/'),(38,1813731,'https://pixabay.com/get/57e8d4404d51ad14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/57e8d4404d51ad14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/aircraft-double-decker-1813731/'),(39,731126,'https://pixabay.com/get/51e3d4424854b108f5d08460962a377c1339d7e04e50744c7c2c73d19745cd_1280.jpg','https://pixabay.com/get/51e3d4424854b108f5d08460962a377c1339d7e04e50744c7c2c73d19745cd_640.jpg','https://pixabay.com/photos/airplane-wrecked-plane-aircraft-731126/'),(40,1822133,'https://pixabay.com/get/57e8d7414b51af14f6da8c7dda79357e1739d8ec564c704c702678dc924dcd50_1280.jpg','https://pixabay.com/get/57e8d7414b51af14f6da8c7dda79357e1739d8ec564c704c702678dc924dcd50_640.jpg','https://pixabay.com/photos/airport-man-travel-traveler-1822133/'),(41,1756149,'https://pixabay.com/get/57e7d0454b56a514f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/57e7d0454b56a514f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/travel-flying-aircraft-sky-sunset-1756149/'),(42,1362586,'https://pixabay.com/get/57e3d3414f5aaa14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/57e3d3414f5aaa14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/aircraft-vacations-sun-tourism-1362586/'),(43,513641,'https://pixabay.com/get/53e1d6454e53b108f5d08460962a377c1339d7e04e50744c7c2c73d1944dc7_1280.jpg','https://pixabay.com/get/53e1d6454e53b108f5d08460962a377c1339d7e04e50744c7c2c73d1944dc7_640.jpg','https://pixabay.com/photos/aircraft-landing-sky-silhouette-513641/'),(44,547105,'https://pixabay.com/get/53e4d2424a57b108f5d08460962a377c1339d7e04e50744c7c2c73d1944dc7_1280.jpg','https://pixabay.com/get/53e4d2424a57b108f5d08460962a377c1339d7e04e50744c7c2c73d1944dc7_640.jpg','https://pixabay.com/photos/aircraft-double-decker-airport-547105/'),(45,1109093,'https://pixabay.com/get/57e1d54a4a5baf14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/57e1d54a4a5baf14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/military-jets-airplanes-flying-1109093/'),(46,2438799,'https://pixabay.com/get/54e4d64b4d5ba514f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg','https://pixabay.com/get/54e4d64b4d5ba514f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_640.jpg','https://pixabay.com/photos/background-technology-turbine-2438799/'),(47,1149997,'https://pixabay.com/get/57e1d14a435bab14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.jpg','https://pixabay.com/get/57e1d14a435bab14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_640.jpg','https://pixabay.com/photos/car-traffic-man-hurry-1149997/'),(48,2203329,'https://pixabay.com/get/54e2d5404950a514f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.jpg','https://pixabay.com/get/54e2d5404950a514f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_640.jpg','https://pixabay.com/photos/trolley-tram-tramway-street-car-2203329/'),(49,332857,'https://pixabay.com/get/55e3d74b4f55b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_1280.jpg','https://pixabay.com/get/55e3d74b4f55b108f5d08460962a377c1339d7e04e50744c7c2c73d1934bc3_640.jpg','https://pixabay.com/photos/traffic-highway-lights-night-road-332857/'),(50,1851246,'https://pixabay.com/get/57e8d0424856aa14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.jpg','https://pixabay.com/get/57e8d0424856aa14f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_640.jpg','https://pixabay.com/photos/buildings-car-city-porsche-rainy-1851246/'),(51,693375,'https://pixabay.com/get/50e9d6404d57b108f5d08460962a377c1339d7e04e50744c7c2c73d19249cd_1280.jpg','https://pixabay.com/get/50e9d6404d57b108f5d08460962a377c1339d7e04e50744c7c2c73d19249cd_640.jpg','https://pixabay.com/photos/corvette-chevrolet-corvette-c3-old-693375/'),(52,896783,'https://pixabay.com/get/5ee9d3444251b108f5d08460962a377c1339d7e04e50744c7c2c73d19249cd_1280.jpg','https://pixabay.com/get/5ee9d3444251b108f5d08460962a377c1339d7e04e50744c7c2c73d19249cd_640.jpg','https://pixabay.com/photos/chevrolet-corvettes-borgo-italian-896783/'),(53,171422,'https://pixabay.com/get/57e7d4474850b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.jpg','https://pixabay.com/get/57e7d4474850b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.jpg','https://pixabay.com/photos/corvette-racing-car-roadster-171422/'),(54,158202,'https://pixabay.com/get/57e5dd414a50b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.png','https://pixabay.com/get/57e5dd414a50b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.png','https://pixabay.com/vectors/car-automobile-chevy-corvette-auto-158202/'),(55,158548,'https://pixabay.com/get/57e5dd464e5ab108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.png','https://pixabay.com/get/57e5dd464e5ab108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.png','https://pixabay.com/vectors/car-red-cabriolet-sports-car-chevy-158548/'),(56,146873,'https://pixabay.com/get/57e4d34b4d51b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.png','https://pixabay.com/get/57e4d34b4d51b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.png','https://pixabay.com/vectors/sports-car-car-roadster-racing-car-146873/'),(57,4203302,'https://pixabay.com/get/52e2d5404952ae14f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_1280.jpg','https://pixabay.com/get/52e2d5404952ae14f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_640.jpg','https://pixabay.com/photos/car-engine-racing-chevrolet-4203302/'),(58,3253172,'https://pixabay.com/get/55e2d0404b55ae14f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_1280.png','https://pixabay.com/get/55e2d0404b55ae14f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_640.png','https://pixabay.com/photos/chevrolet-corvette-6zyl-in-series-3253172/'),(59,3880434,'https://pixabay.com/get/55e8dd434e51a814f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_1280.png','https://pixabay.com/get/55e8dd434e51a814f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_640.png','https://pixabay.com/photos/auto-chevrolet-corvette-usa-show-3880434/'),(60,398687,'https://pixabay.com/get/55e9dd454255b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.jpg','https://pixabay.com/get/55e9dd454255b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.jpg','https://pixabay.com/photos/corvette-car-auto-automobile-398687/'),(61,151875,'https://pixabay.com/get/57e5d44b4d57b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.png','https://pixabay.com/get/57e5d44b4d57b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.png','https://pixabay.com/vectors/corvette-sports-car-racing-car-151875/'),(62,1885678,'https://pixabay.com/get/57e8dd464c55a414f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_1280.jpg','https://pixabay.com/get/57e8dd464c55a414f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_640.jpg','https://pixabay.com/photos/chevrolet-corvette-cockpit-front-1885678/'),(63,553221,'https://pixabay.com/get/53e5d6414853b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.jpg','https://pixabay.com/get/53e5d6414853b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.jpg','https://pixabay.com/photos/corvette-convertible-vintage-553221/'),(64,212239,'https://pixabay.com/get/54e1d741495bb108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.jpg','https://pixabay.com/get/54e1d741495bb108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.jpg','https://pixabay.com/photos/cars-hotrods-muscle-roadsters-drag-212239/'),(65,633291,'https://pixabay.com/get/50e3d6414353b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.jpg','https://pixabay.com/get/50e3d6414353b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.jpg','https://pixabay.com/photos/sports-car-corvette-car-z06-633291/'),(66,152102,'https://pixabay.com/get/57e5d7424a50b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.png','https://pixabay.com/get/57e5d7424a50b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.png','https://pixabay.com/vectors/corvette-racing-car-flag-winner-152102/'),(67,212242,'https://pixabay.com/get/54e1d7414e50b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.jpg','https://pixabay.com/get/54e1d7414e50b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.jpg','https://pixabay.com/photos/cars-hotrods-muscle-roadsters-drag-212242/'),(68,3106694,'https://pixabay.com/get/55e1d5454c5ba814f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_1280.jpg','https://pixabay.com/get/55e1d5454c5ba814f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_640.jpg','https://pixabay.com/photos/car-wheel-transportation-system-3106694/'),(69,829795,'https://pixabay.com/get/5ee2dc444357b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.jpg','https://pixabay.com/get/5ee2dc444357b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.jpg','https://pixabay.com/photos/corvette-chevrolet-sport-car-garage-829795/'),(70,3864797,'https://pixabay.com/get/55e8d3474d5bab14f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_1280.jpg','https://pixabay.com/get/55e8d3474d5bab14f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_640.jpg','https://pixabay.com/photos/corvette-us-car-classic-car-3864797/'),(71,813482,'https://pixabay.com/get/5ee1d6474250b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_1280.jpg','https://pixabay.com/get/5ee1d6474250b108f5d08460962a377c1339d7e04e50744c7c2c73d1914dc0_640.jpg','https://pixabay.com/photos/car-auto-vehicle-automobile-813482/'),(72,2087749,'https://pixabay.com/get/54e0dd444d56a514f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_1280.jpg','https://pixabay.com/get/54e0dd444d56a514f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_640.jpg','https://pixabay.com/photos/automobile-corvette-grand-sport-car-2087749/'),(73,4036203,'https://pixabay.com/get/52e0d6454852af14f6da8c7dda79357e1739d8ec564c704c702678dc914cc65e_1280.jpg','https://pixabay.com/get/52e0d6454852af14f6da8c7dda79357e1739d8ec564c704c702678dc914cc65e_640.jpg','https://pixabay.com/photos/vehicle-automobile-sports-porsche-4036203/'),(74,1836962,'https://pixabay.com/get/57e8d6454354ae14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/57e8d6454354ae14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/bike-honda-motorbike-motorcycle-1836962/'),(75,1453863,'https://pixabay.com/get/57e4d0404254af14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/57e4d0404254af14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/motorcycle-bike-honda-motorbike-1453863/'),(76,4092631,'https://pixabay.com/get/52e0dc414c51ad14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/52e0dc414c51ad14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/traffic-road-vehicle-bike-4092631/'),(77,143174,'https://pixabay.com/get/57e4d6424d56b108f5d08460962a377c1339d7e04e50744c7c2c73d29549cd_1280.jpg','https://pixabay.com/get/57e4d6424d56b108f5d08460962a377c1339d7e04e50744c7c2c73d29549cd_640.jpg','https://pixabay.com/photos/motorcycle-bike-honda-show-vehicle-143174/'),(78,4167394,'https://pixabay.com/get/52e1d344495ba814f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/52e1d344495ba814f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/motorbike-bike-motorcycle-fast-4167394/'),(79,1030936,'https://pixabay.com/get/57e0d6434351aa14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/57e0d6434351aa14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/honda-motorbike-motorcycle-bike-1030936/'),(80,297090,'https://pixabay.com/get/54e9d2434352b108f5d08460962a377c1339d7e04e50744c7c2c73d29549cd_1280.png','https://pixabay.com/get/54e9d2434352b108f5d08460962a377c1339d7e04e50744c7c2c73d29549cd_640.png','https://pixabay.com/vectors/motorbike-motorcycle-bike-race-297090/'),(81,1731774,'https://pixabay.com/get/57e7d6424d55a814f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/57e7d6424d55a814f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/motorcycle-chopper-chrome-1731774/'),(82,1596081,'https://pixabay.com/get/57e5dc454a5aad14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.png','https://pixabay.com/get/57e5dc454a5aad14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.png','https://pixabay.com/illustrations/honda-logo-car-honda-honda-honda-1596081/'),(83,143399,'https://pixabay.com/get/57e4d640435bb108f5d08460962a377c1339d7e04e50744c7c2c73d29549cd_1280.jpg','https://pixabay.com/get/57e4d640435bb108f5d08460962a377c1339d7e04e50744c7c2c73d29549cd_640.jpg','https://pixabay.com/photos/honda-cb650-c-motorcycle-cycle-143399/'),(84,3242291,'https://pixabay.com/get/55e2d141485bad14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/55e2d141485bad14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/motocross-ride-landscape-panoramic-3242291/'),(85,2651951,'https://pixabay.com/get/54e6d0424357ad14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/54e6d0424357ad14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/motorcycle-motor-screw-view-details-2651951/'),(86,1110180,'https://pixabay.com/get/57e1d4434b5aac14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/57e1d4434b5aac14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/sport-motocross-motorcycle-sport-1110180/'),(87,2845525,'https://pixabay.com/get/54e8d1464f50a914f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/54e8d1464f50a914f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/motorcycle-honda-racetrack-bike-2845525/'),(88,3920628,'https://pixabay.com/get/55e9d7434c50a414f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/55e9d7434c50a414f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/engine-honda-vfr1200-3920628/'),(89,3768912,'https://pixabay.com/get/55e7d34b4353ae14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/55e7d34b4353ae14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/engine-honda-honda-shadow-3768912/'),(90,1581945,'https://pixabay.com/get/57e5dd424356a914f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/57e5dd424356a914f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/illustrations/motorcycle-engine-raytracing-render-1581945/'),(91,1548330,'https://pixabay.com/get/57e5d14b4951ac14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/57e5d14b4951ac14f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/bahia-honda-state-park-park-florida-1548330/'),(92,3768895,'https://pixabay.com/get/55e7d34b425ba914f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/55e7d34b425ba914f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/engine-honda-honda-shadow-3768895/'),(93,3768909,'https://pixabay.com/get/55e7d34b4352a514f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg','https://pixabay.com/get/55e7d34b4352a514f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_640.jpg','https://pixabay.com/photos/engine-honda-honda-shadow-3768909/'),(94,62829,'https://pixabay.com/get/50e2dd41434fad0bffd8992cc52835781336dbf852547749742c7ed49048_1280.jpg','https://pixabay.com/get/50e2dd41434fad0bffd8992cc52835781336dbf852547749742c7ed49048_640.jpg','https://pixabay.com/photos/supersonic-fighter-fighter-jet-62829/'),(95,1816071,'https://pixabay.com/get/57e8d4454a55ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.jpg','https://pixabay.com/get/57e8d4454a55ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.jpg','https://pixabay.com/illustrations/f-16-military-aircraft-aviation-1816071/'),(96,4199105,'https://pixabay.com/get/52e1dc4a4b52a914f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.jpg','https://pixabay.com/get/52e1dc4a4b52a914f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.jpg','https://pixabay.com/photos/aircraft-propeller-bomber-retro-4199105/'),(97,4166926,'https://pixabay.com/get/52e1d3454350aa14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.jpg','https://pixabay.com/get/52e1d3454350aa14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.jpg','https://pixabay.com/photos/candy-bomber-aircraft-4166926/'),(98,62833,'https://pixabay.com/get/50e2dd40494fad0bffd8992cc52835781336dbf852547749742c7ed49048_1280.jpg','https://pixabay.com/get/50e2dd40494fad0bffd8992cc52835781336dbf852547749742c7ed49048_640.jpg','https://pixabay.com/photos/delta-wings-aircraft-stealth-bomber-62833/'),(99,384868,'https://pixabay.com/get/55e8d14b4c5ab108f5d08460962a377c1339d7e04e50744f752e78d1974ac0_1280.jpg','https://pixabay.com/get/55e8d14b4c5ab108f5d08460962a377c1339d7e04e50744f752e78d1974ac0_640.jpg','https://pixabay.com/photos/model-aircraft-model-airplane-plane-384868/'),(100,1023,'https://pixabay.com/get/57e0d7405753ae01f7c5d57fc72a31781c3ac3e456577148762a7bd392_1280.jpg','https://pixabay.com/get/57e0d7405753ae01f7c5d57fc72a31781c3ac3e456577148762a7bd392_640.jpg','https://pixabay.com/photos/aircraft-delta-wing-stealth-bomber-1023/'),(101,2507504,'https://pixabay.com/get/54e5d5444f52a814f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.jpg','https://pixabay.com/get/54e5d5444f52a814f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.jpg','https://pixabay.com/photos/airplane-world-war-ii-sepia-2507504/'),(102,607225,'https://pixabay.com/get/50e0d2414857b108f5d08460962a377c1339d7e04e50744f752e78d1974ac0_1280.jpg','https://pixabay.com/get/50e0d2414857b108f5d08460962a377c1339d7e04e50744f752e78d1974ac0_640.jpg','https://pixabay.com/photos/airplane-military-aircraft-b-17-607225/'),(103,1040097,'https://pixabay.com/get/57e0d1434a5bab14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.png','https://pixabay.com/get/57e0d1434a5bab14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.png','https://pixabay.com/photos/raptor-f-22-jet-fighter-military-1040097/'),(104,1005,'https://pixabay.com/get/57e0d5465753ae01f7c5d57fc72a31781c3ac3e456577148762a7bd392_1280.jpg','https://pixabay.com/get/57e0d5465753ae01f7c5d57fc72a31781c3ac3e456577148762a7bd392_640.jpg','https://pixabay.com/photos/aircraft-bomber-fighter-jets-jets-1005/'),(105,2027551,'https://pixabay.com/get/54e0d7444f57ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.png','https://pixabay.com/get/54e0d7444f57ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.png','https://pixabay.com/vectors/aeroplane-air-force-airplane-bomber-2027551/'),(106,3867031,'https://pixabay.com/get/55e8d3444a51ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.jpg','https://pixabay.com/get/55e8d3444a51ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.jpg','https://pixabay.com/photos/airplane-plane-aviation-flight-sky-3867031/'),(107,3858901,'https://pixabay.com/get/55e8d04b4352ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.jpg','https://pixabay.com/get/55e8d04b4352ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.jpg','https://pixabay.com/photos/airplane-plane-aircraft-aviation-3858901/'),(108,2024011,'https://pixabay.com/get/54e0d7474a53ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.png','https://pixabay.com/get/54e0d7474a53ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.png','https://pixabay.com/vectors/airplane-bomber-england-fighter-2024011/'),(109,429993,'https://pixabay.com/get/52e2dc4a4351b108f5d08460962a377c1339d7e04e50744f752e78d1974ac0_1280.jpg','https://pixabay.com/get/52e2dc4a4351b108f5d08460962a377c1339d7e04e50744f752e78d1974ac0_640.jpg','https://pixabay.com/photos/bomber-vulcan-aircraft-aeroplane-429993/'),(110,2025895,'https://pixabay.com/get/54e0d746425ba914f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.png','https://pixabay.com/get/54e0d746425ba914f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.png','https://pixabay.com/vectors/aircraft-bomber-germany-historical-2025895/'),(111,1299535,'https://pixabay.com/get/57e2dc4a4f51a914f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.png','https://pixabay.com/get/57e2dc4a4f51a914f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.png','https://pixabay.com/vectors/aircraft-airplane-bomber-ww-2-1299535/'),(112,429985,'https://pixabay.com/get/52e2dc4a4257b108f5d08460962a377c1339d7e04e50744f752e78d1974ac0_1280.jpg','https://pixabay.com/get/52e2dc4a4257b108f5d08460962a377c1339d7e04e50744f752e78d1974ac0_640.jpg','https://pixabay.com/photos/bomber-war-aircraft-airplane-world-429985/'),(113,3698982,'https://pixabay.com/get/55e6dc4b435aae14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.jpg','https://pixabay.com/get/55e6dc4b435aae14f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_640.jpg','https://pixabay.com/photos/military-bomber-aviation-aircraft-3698982/'),(114,1914310,'https://pixabay.com/get/57e9d4474953ac14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_1280.jpg','https://pixabay.com/get/57e9d4474953ac14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_640.jpg','https://pixabay.com/photos/gas-pump-petrol-stations-petrol-gas-1914310/'),(115,2273069,'https://pixabay.com/get/54e2d2404a54a514f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_1280.jpg','https://pixabay.com/get/54e2d2404a54a514f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_640.jpg','https://pixabay.com/photos/apocalypse-disaster-end-time-2273069/'),(116,67557,'https://pixabay.com/get/50e7d0464d4fad0bffd8992cc52835781336dbf852547749742c79d5954a_1280.jpg','https://pixabay.com/get/50e7d0464d4fad0bffd8992cc52835781336dbf852547749742c79d5954a_640.jpg','https://pixabay.com/photos/nuclear-weapons-test-nuclear-weapon-67557/'),(117,147909,'https://pixabay.com/get/57e4d24a4a5bb108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_1280.png','https://pixabay.com/get/57e4d24a4a5bb108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_640.png','https://pixabay.com/vectors/explosion-detonation-blast-burst-147909/'),(118,183543,'https://pixabay.com/get/57e8d6464e51b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_1280.jpg','https://pixabay.com/get/57e8d6464e51b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_640.jpg','https://pixabay.com/photos/chocolate-dark-coffee-confiserie-183543/'),(119,3366430,'https://pixabay.com/get/55e3d3454e51ac14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_1280.jpg','https://pixabay.com/get/55e3d3454e51ac14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_640.jpg','https://pixabay.com/photos/eclair-strawberry-cake-strawberries-3366430/'),(120,693645,'https://pixabay.com/get/50e9d6454e57b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_1280.jpg','https://pixabay.com/get/50e9d6454e57b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_640.jpg','https://pixabay.com/photos/cake-pops-pastries-cake-sweet-693645/'),(121,3629119,'https://pixabay.com/get/55e6d74a4b53a514f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_1280.jpg','https://pixabay.com/get/55e6d74a4b53a514f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_640.jpg','https://pixabay.com/photos/oil-oil-rig-industry-oil-industry-3629119/'),(122,1737503,'https://pixabay.com/get/57e7d6444f52af14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_1280.jpg','https://pixabay.com/get/57e7d6444f52af14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_640.jpg','https://pixabay.com/photos/chocolates-chocolate-confiserie-1737503/'),(123,768666,'https://pixabay.com/get/51e6dd454c54b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_1280.jpg','https://pixabay.com/get/51e6dd454c54b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_640.jpg','https://pixabay.com/photos/old-bottles-glass-vintage-empty-768666/'),(124,1030751,'https://pixabay.com/get/57e0d6434d57ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_1280.jpg','https://pixabay.com/get/57e0d6434d57ad14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_640.jpg','https://pixabay.com/photos/fire-explosion-danger-hot-flame-1030751/'),(125,1284496,'https://pixabay.com/get/57e2dd474e5baa14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_1280.jpg','https://pixabay.com/get/57e2dd474e5baa14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_640.jpg','https://pixabay.com/photos/fashion-high-heels-shoes-pumps-1284496/'),(126,985500,'https://pixabay.com/get/5fe8d0464a52b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_1280.jpg','https://pixabay.com/get/5fe8d0464a52b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_640.jpg','https://pixabay.com/illustrations/animals-black-and-white-bomb-boom-985500/'),(127,153710,'https://pixabay.com/get/57e5d6444b52b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_1280.png','https://pixabay.com/get/57e5d6444b52b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_640.png','https://pixabay.com/vectors/explosion-pow-detonation-bomb-boom-153710/'),(128,2753762,'https://pixabay.com/get/54e7d0404d54ae14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_1280.png','https://pixabay.com/get/54e7d0404d54ae14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_640.png','https://pixabay.com/vectors/ekg-electrocardiogram-anatomy-aorta-2753762/'),(129,2136244,'https://pixabay.com/get/54e1d6454856a814f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_1280.jpg','https://pixabay.com/get/54e1d6454856a814f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_640.jpg','https://pixabay.com/photos/nuclear-atom-bomb-atomic-science-2136244/'),(130,154456,'https://pixabay.com/get/57e5d1474f54b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_1280.png','https://pixabay.com/get/57e5d1474f54b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_640.png','https://pixabay.com/vectors/bomb-explosive-detonation-fuze-154456/'),(131,567454,'https://pixabay.com/get/53e6d2474f56b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_1280.jpg','https://pixabay.com/get/53e6d2474f56b108f5d08460962a377c1339d7e04e50744f752e78d6964fc2_640.jpg','https://pixabay.com/photos/eat-sugar-calories-food-sweet-567454/'),(132,2753763,'https://pixabay.com/get/54e7d0404d54af14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_1280.png','https://pixabay.com/get/54e7d0404d54af14f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_640.png','https://pixabay.com/vectors/ekg-electrocardiogram-anatomy-aorta-2753763/'),(133,1660545,'https://pixabay.com/get/57e6d3434f56a914f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_1280.png','https://pixabay.com/get/57e6d3434f56a914f6da8c7dda79357e1739d8ec564c704c732f7ad7954cc75f_640.png','https://pixabay.com/illustrations/explosive-fire-bomb-fire-fire-1660545/'),(134,1655830,'https://pixabay.com/get/57e6d0464251ac14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/57e6d0464251ac14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/florence-italy-church-tuscany-1655830/'),(135,3024768,'https://pixabay.com/get/55e0d7474d54a414f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/55e0d7474d54a414f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/church-dom-chapel-altar-religion-3024768/'),(136,3599450,'https://pixabay.com/get/55e5dc4a4e57ac14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/55e5dc4a4e57ac14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/cathedral-church-dom-religion-3599450/'),(137,1846338,'https://pixabay.com/get/57e8d1454951a414f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/57e8d1454951a414f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/cologne-cathedral-dom-city-germany-1846338/'),(138,3592874,'https://pixabay.com/get/55e5dc414255a814f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/55e5dc414255a814f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/berlin-cathedral-building-3592874/'),(139,1078671,'https://pixabay.com/get/57e0d24b4c55ad14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/57e0d24b4c55ad14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/cologne-rhine-night-bridge-dom-1078671/'),(140,3023439,'https://pixabay.com/get/55e0d7404e51a514f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/55e0d7404e51a514f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/regensburg-stone-bridge-dom-3023439/'),(141,1707634,'https://pixabay.com/get/57e7d5444c51a814f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/57e7d5444c51a814f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/dom-church-cathedral-window-1707634/'),(142,3284639,'https://pixabay.com/get/55e2dd474c51a514f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/55e2dd474c51a514f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/church-architecture-dom-chapel-old-3284639/'),(143,1707664,'https://pixabay.com/get/57e7d5444c54a814f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/57e7d5444c54a814f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/dom-church-cathedral-1707664/'),(144,3594407,'https://pixabay.com/get/55e5dc474e52ab14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/55e5dc474e52ab14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/berlin-cathedral-building-3594407/'),(145,3599448,'https://pixabay.com/get/55e5dc4a4e56a414f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/55e5dc4a4e56a414f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/church-window-colorful-3599448/'),(146,3710237,'https://pixabay.com/get/55e7d4434851ab14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/55e7d4434851ab14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/church-dome-gilded-architecture-3710237/'),(147,3260163,'https://pixabay.com/get/55e2d3434b54af14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/55e2d3434b54af14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/grave-crypt-burial-chamber-tomb-3260163/'),(148,3394376,'https://pixabay.com/get/55e3dc474955aa14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/55e3dc474955aa14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/freiberg-church-steeple-city-3394376/'),(149,3457933,'https://pixabay.com/get/55e4d0444351af14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/55e4d0444351af14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/cathedral-church-dom-religion-3457933/'),(150,1853687,'https://pixabay.com/get/57e8d0404c5aab14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/57e8d0404c5aab14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/architecture-city-houses-bridge-1853687/'),(151,4196408,'https://pixabay.com/get/52e1dc454e52a414f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/52e1dc454e52a414f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/dom-church-copper-beech-crown-4196408/'),(152,4180223,'https://pixabay.com/get/52e1dd434850af14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/52e1dd434850af14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/dom-church-copper-beech-crown-4180223/'),(153,4196511,'https://pixabay.com/get/52e1dc454f53ad14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg','https://pixabay.com/get/52e1dc454f53ad14f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_640.jpg','https://pixabay.com/photos/church-window-church-window-4196511/'),(154,1620440,'https://pixabay.com/get/57e6d7434e56ac14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/57e6d7434e56ac14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/mill-black-forest-bach-water-1620440/'),(155,3080644,'https://pixabay.com/get/55e0dd434c56a814f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/55e0dd434c56a814f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/fashion-woman-portrait-model-girl-3080644/'),(156,1072366,'https://pixabay.com/get/57e0d2414954aa14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/57e0d2414954aa14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/black-board-chalk-traces-school-1072366/'),(157,3076685,'https://pixabay.com/get/55e0d2454c5aa914f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/55e0d2454c5aa914f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/architecture-fachwerkh%C3%A4user-night-3076685/'),(158,1620452,'https://pixabay.com/get/57e6d7434e57ae14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/57e6d7434e57ae14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/boat-rowing-boat-lake-water-1620452/'),(159,69124,'https://pixabay.com/get/50e9d4414e4fad0bffd8992cc52835781336dbf852547749772f78d1924b_1280.jpg','https://pixabay.com/get/50e9d4414e4fad0bffd8992cc52835781336dbf852547749772f78d1924b_640.jpg','https://pixabay.com/photos/smoke-background-artwork-swirl-69124/'),(160,1789903,'https://pixabay.com/get/57e7dd4a4352af14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/57e7dd4a4352af14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/morning-fog-sunbeam-forest-1789903/'),(161,2150881,'https://pixabay.com/get/54e1d043425aad14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/54e1d043425aad14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/beautiful-woman-face-young-woman-2150881/'),(162,792180,'https://pixabay.com/get/51e9d7424252b108f5d08460962a377c1339d7e04e50744f752d7bd79248c3_1280.jpg','https://pixabay.com/get/51e9d7424252b108f5d08460962a377c1339d7e04e50744f752d7bd79248c3_640.jpg','https://pixabay.com/photos/technology-tablet-digital-tablet-792180/'),(163,694730,'https://pixabay.com/get/50e9d1444952b108f5d08460962a377c1339d7e04e50744f752d7bd79248c3_1280.jpg','https://pixabay.com/get/50e9d1444952b108f5d08460962a377c1339d7e04e50744f752d7bd79248c3_640.jpg','https://pixabay.com/photos/cat-silhouette-cats-silhouette-694730/'),(164,415501,'https://pixabay.com/get/52e1d0464a53b108f5d08460962a377c1339d7e04e50744f752d7bd79248c3_1280.jpg','https://pixabay.com/get/52e1d0464a53b108f5d08460962a377c1339d7e04e50744f752d7bd79248c3_640.jpg','https://pixabay.com/photos/full-moon-moon-night-dark-black-415501/'),(165,1047518,'https://pixabay.com/get/57e0d1444f53a414f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/57e0d1444f53a414f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/bulldog-puppy-dog-pet-sweet-black-1047518/'),(166,407256,'https://pixabay.com/get/52e0d2414f54b108f5d08460962a377c1339d7e04e50744f752d7bd79248c3_1280.jpg','https://pixabay.com/get/52e0d2414f54b108f5d08460962a377c1339d7e04e50744f752d7bd79248c3_640.jpg','https://pixabay.com/photos/tree-silhouette-mysterious-407256/'),(167,1218884,'https://pixabay.com/get/57e2d44b425aa814f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/57e2d44b425aa814f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/butterfly-insect-wings-nature-1218884/'),(168,2591874,'https://pixabay.com/get/54e5dc424255a814f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/54e5dc424255a814f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/people-woman-travel-adventure-trek-2591874/'),(169,1149911,'https://pixabay.com/get/57e1d14a4353ad14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/57e1d14a4353ad14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/woman-girl-blonde-hair-blonde-woman-1149911/'),(170,3104635,'https://pixabay.com/get/55e1d5474c51a914f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/55e1d5474c51a914f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/people-three-portrait-black-3104635/'),(171,1365995,'https://pixabay.com/get/57e3d346435ba914f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/57e3d346435ba914f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/earth-globe-moon-world-planet-1365995/'),(172,97088,'https://pixabay.com/get/5fe7d54b424fad0bffd8992cc52835781336dbf852547749772f78d1924b_1280.jpg','https://pixabay.com/get/5fe7d54b424fad0bffd8992cc52835781336dbf852547749772f78d1924b_640.jpg','https://pixabay.com/photos/girl-model-pretty-portrait-lady-97088/'),(173,1081873,'https://pixabay.com/get/57e0dd424255af14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg','https://pixabay.com/get/57e0dd424255af14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_640.jpg','https://pixabay.com/photos/woman-gothic-dark-girl-female-1081873/');
/*!40000 ALTER TABLE `pixabay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `domain` varchar(250) DEFAULT NULL COMMENT 'Pole z domeną pod którą będzie się znajdował projekt',
  `config` text,
  `show_left_sidebar` tinyint(1) DEFAULT '0',
  `show_right_sidebar` tinyint(1) DEFAULT '0',
  `forked_id` int(10) unsigned DEFAULT NULL COMMENT 'Wskazuje na id z tabeli projects jest to projekt od którego został utworzony ten rekord',
  `style_id` int(10) unsigned DEFAULT NULL,
  `custom_style_id` int(10) unsigned DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_projects_added_by` (`added_by`),
  KEY `fk_projects_deleted_by` (`deleted_by`),
  KEY `fk_projects_updated_by` (`updated_by`),
  KEY `fk_projects_added_ip_id` (`added_ip_id`),
  KEY `fk_projects_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_projects_updated_ip_id` (`updated_ip_id`),
  KEY `fk_projects_user_id` (`user_id`),
  KEY `fk_projects_forked_id` (`forked_id`),
  KEY `fk_projects_style_id` (`style_id`),
  KEY `fk_projects_custom_style_id` (`custom_style_id`),
  CONSTRAINT `fk_projects_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_custom_style_id` FOREIGN KEY (`custom_style_id`) REFERENCES `styles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_forked_id` FOREIGN KEY (`forked_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_style_id` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_datasets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_datasets` (
  `project_id` int(10) unsigned NOT NULL,
  `key` binary(4) NOT NULL,
  `value` text,
  `element_position_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`project_id`,`key`,`element_position_id`) USING BTREE,
  KEY `fk_projects_datasets_element_position_id` (`element_position_id`),
  CONSTRAINT `fk_projects_datasets_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_datasets`
--

LOCK TABLES `projects_datasets` WRITE;
/*!40000 ALTER TABLE `projects_datasets` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_datasets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `projects_datasets_view`
--

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `projects_datasets_view` (
  `project_id` tinyint NOT NULL,
  `key` tinyint NOT NULL,
  `value` tinyint NOT NULL,
  `element_position_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `projects_elements`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(10) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `uuid` binary(8) NOT NULL,
  `selector` varchar(50) DEFAULT NULL,
  `kind` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_projects_elements_element_id` (`element_id`),
  KEY `fk_projects_elements_project_id` (`project_id`),
  CONSTRAINT `fk_projects_elements_element_id` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_elements_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_elements`
--

LOCK TABLES `projects_elements` WRITE;
/*!40000 ALTER TABLE `projects_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_elements_fields`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_elements_fields` (
  `field` binary(4) NOT NULL,
  `value` varchar(50) NOT NULL,
  `project_element_id` int(10) unsigned NOT NULL,
  `alt_id` binary(4) DEFAULT NULL,
  `id` binary(4) DEFAULT NULL,
  PRIMARY KEY (`field`,`project_element_id`),
  KEY `fk_pages_elements_fields_project_element_id` (`project_element_id`),
  CONSTRAINT `fk_pages_elements_fields_page_project_element_id` FOREIGN KEY (`project_element_id`) REFERENCES `projects_elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_elements_fields`
--

LOCK TABLES `projects_elements_fields` WRITE;
/*!40000 ALTER TABLE `projects_elements_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_elements_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `sessid` binary(16) NOT NULL,
  `access` int(10) unsigned DEFAULT NULL,
  `data` text,
  `user_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sessid` (`sessid`),
  KEY `fk_session_user_id` (`user_id`),
  CONSTRAINT `fk_session_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES ('�t(��[dY������',1558823954,'ip_id|s:1:\"2\";user_id|i:1;',1,1558824007,1),('�fǅr+�Y�����iI',1558841517,'ip_id|s:1:\"3\";user_id|i:1;',1,0,2),('�tZ�OR�/U]<',1559366685,'ip_id|s:1:\"5\";user_id|i:1;',1,1559366688,3),('\n\nt\ns�,|?��s�c|',1559468391,'ip_id|s:1:\"6\";user_id|i:1;',1,0,4),('$�	S3<B�SB��',1559475846,'',NULL,0,5),('�J�e�@jK\Z�=:',1559475846,'',NULL,0,6),('��Q}:�l�S5�=T±',1559475846,'',NULL,0,7),('���GM��/Ч���s�',1559475924,'ip_id|s:1:\"7\";user_id|i:1;',1,0,8),('U+IR��%��Tk�@�',1559475846,'',NULL,0,9),('/B�(uqј�7�L',1559475846,'',NULL,0,10),('�y2y��RL�x���{',1559480258,'',NULL,0,11),('�(O��֡�L�W��o2E',1559480258,'',NULL,0,12),('��@��z�ڥ\"]˱��',1559480258,'',NULL,0,13),('�f\n�G�g�O�,*�W�',1559480258,'',NULL,0,14),('Y-6u����|;�',1559480891,'ip_id|s:1:\"8\";user_id|i:1;',1,0,15),('�h����*\"�s�	��',1559480258,'',NULL,0,16),('TU�ځ�HҨ�f�6���',1559488760,'\"ip_id|s:1:\\\"9\\\";user_id|i:1;\"',1,1559488760,17),('\"K����*��%*��IP]',1559493848,'ip_id|s:2:\"11\";user_id|i:1;',1,0,18),('\r\"�YL\niW�:`9�',1559488780,'ip_id|s:2:\"10\";',NULL,0,19),('�H`/	��H�CL��p��',1559500766,'ip_id|s:2:\"12\";',NULL,0,20),(';U�Ȃ���m�lX��',1559512163,'ip_id|s:2:\"13\";user_id|i:1;',1,0,21),('��ХTM����ĵ*W�',1560113911,'',NULL,0,22),('�/���ud�����fA',1560142344,'ip_id|s:2:\"15\";user_id|i:1;',1,1560142360,23),('{D�YW�󦬯�̦�&',1560143002,'ip_id|s:2:\"16\";user_id|i:1;',1,0,24),('���7��@����R',1560171387,'',NULL,0,25),('8�h��׳��kI�',1560171396,'',NULL,0,26),('w�P�+2h�Qj���',1560171398,'ip_id|s:2:\"17\";',NULL,0,27),('(FAX�d��YZ���',1560180167,'ip_id|s:2:\"19\";user_id|i:1;',1,0,28),('�ɘ\ZW�_ŝrͫ]}�',1560202057,'',NULL,0,29),('{���Z���ܜ\\*ܴH',1560230888,'ip_id|s:2:\"21\";user_id|i:1;',1,0,30),('M\"R/�/Z6؉x�zV:',1560263649,'ip_id|s:2:\"23\";',NULL,0,31),('vT�ܵ�;\rS���T��',1560316943,'ip_id|s:2:\"25\";user_id|i:1;',1,0,32),('J�EǪM)xQ�O�q',1560374083,'ip_id|s:2:\"26\";user_id|i:1;',1,0,33),('�6�6Eg�E;����K',1560378646,'ip_id|s:2:\"27\";user_id|i:1;',1,0,34),('RS`�_w�(�sBb��',1560389829,'ip_id|s:2:\"29\";user_id|i:1;',1,0,35),('@qZ1�e-�ѵd��a&',1560400818,'ip_id|s:2:\"30\";user_id|i:1;',1,0,36),('VG:ºBϸ�M��`��',1560437926,'',NULL,0,37),('�s��\nl�<d�U�SR�',1561412055,'ip_id|s:2:\"31\";user_id|i:1;',1,0,38);
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(8) DEFAULT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned NOT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) NOT NULL COMMENT 'Nazwa wyświetlana w liście styli',
  `user_id` int(10) unsigned NOT NULL,
  `image_id` int(10) unsigned DEFAULT NULL,
  `public` tinyint(1) DEFAULT '0',
  `system` tinyint(1) DEFAULT '1' COMMENT 'Jeżeli jest true to jest to styl systemowy np. z elementów',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `fk_styles_added_by` (`added_by`),
  KEY `fk_styles_deleted_by` (`deleted_by`),
  KEY `fk_styles_updated_by` (`updated_by`),
  KEY `fk_styles_added_ip_id` (`added_ip_id`),
  KEY `fk_styles_deleted_ip_id` (`deleted_ip_id`),
  KEY `fk_styles_updated_ip_id` (`updated_ip_id`),
  KEY `fk_styles_user_id` (`user_id`),
  KEY `fk_styles_image_id` (`image_id`),
  KEY `public` (`public`),
  KEY `system` (`system`),
  CONSTRAINT `fk_styles_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_image_id` FOREIGN KEY (`image_id`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles`
--

LOCK TABLES `styles` WRITE;
/*!40000 ALTER TABLE `styles` DISABLE KEYS */;
INSERT INTO `styles` VALUES (6,'y��\rW�D�',1558824516,1,3,1559491599,1,11,0,NULL,NULL,'Default',1,NULL,1,0);
/*!40000 ALTER TABLE `styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `styles_css_view`
--

SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `styles_css_view` (
  `id` tinyint NOT NULL,
  `styles_id` tinyint NOT NULL,
  `param` tinyint NOT NULL,
  `value` tinyint NOT NULL,
  `tag` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `styles_fonts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles_fonts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `styles_id` int(10) unsigned NOT NULL,
  `name` varchar(250) NOT NULL,
  `category` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_styles_fonts_styles_id` (`styles_id`),
  CONSTRAINT `fk_styles_fonts_styles_id` FOREIGN KEY (`styles_id`) REFERENCES `styles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles_fonts`
--

LOCK TABLES `styles_fonts` WRITE;
/*!40000 ALTER TABLE `styles_fonts` DISABLE KEYS */;
INSERT INTO `styles_fonts` VALUES (40,30,'Roboto','sans-serif','https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,i,500,500i,700,700i,900,900i'),(41,29,'Roboto','sans-serif','https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,i,500,500i,700,700i,900,900i'),(42,6,'Roboto','sans-serif','https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,i,500,500i,700,700i,900,900i');
/*!40000 ALTER TABLE `styles_fonts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles_params`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles_params` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles_params`
--

LOCK TABLES `styles_params` WRITE;
/*!40000 ALTER TABLE `styles_params` DISABLE KEYS */;
INSERT INTO `styles_params` VALUES (1,'font-family'),(2,'color'),(3,'text-decoration'),(4,'cursor'),(5,'font-weight'),(6,'border'),(7,'background'),(8,'padding'),(9,'font-size'),(10,'position'),(11,'top'),(12,'width'),(13,'display'),(14,'float'),(15,'margin-bottom'),(16,'box-sizing'),(17,'text-align'),(18,'margin'),(19,'border-bottom'),(20,'border-top'),(21,'border-left'),(22,'border-right'),(23,'margin-top'),(24,'outline'),(25,'max-width'),(26,'margin-right'),(27,'margin-left'),(28,'padding-top'),(29,'z-index'),(30,'border-radius'),(31,'background-size'),(32,'background-position'),(33,'background-image');
/*!40000 ALTER TABLE `styles_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles_style`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles_style` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `styles_id` int(10) unsigned NOT NULL,
  `param_id` int(10) unsigned NOT NULL,
  `value_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_styles_style_styles_id` (`styles_id`),
  KEY `fk_styles_style_param_id` (`param_id`),
  KEY `fk_styles_style_value_id` (`value_id`),
  KEY `fk_styles_style_tag_id` (`tag_id`),
  CONSTRAINT `fk_styles_style_param_id` FOREIGN KEY (`param_id`) REFERENCES `styles_params` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_style_styles_id` FOREIGN KEY (`styles_id`) REFERENCES `styles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_style_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `styles_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_styles_style_value_id` FOREIGN KEY (`value_id`) REFERENCES `styles_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18280 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles_style`
--

LOCK TABLES `styles_style` WRITE;
/*!40000 ALTER TABLE `styles_style` DISABLE KEYS */;
INSERT INTO `styles_style` VALUES (18112,6,4,48,72),(18113,6,6,3,72),(18114,6,7,7,71),(18115,6,7,47,70),(18116,6,8,36,70),(18117,6,8,46,69),(18118,6,7,37,68),(18119,6,2,2,68),(18120,6,8,45,67),(18121,6,6,6,67),(18122,6,2,2,67),(18123,6,8,39,64),(18124,6,5,22,64),(18125,6,9,29,64),(18126,6,13,40,64),(18127,6,12,31,64),(18128,6,29,41,64),(18129,6,10,35,64),(18130,6,7,7,64),(18131,6,27,20,64),(18132,6,13,15,50),(18133,6,10,35,49),(18134,6,11,33,49),(18135,6,13,15,48),(18136,6,10,10,48),(18137,6,11,33,47),(18138,6,10,35,47),(18139,6,13,15,46),(18140,6,27,34,45),(18141,6,26,34,44),(18142,6,17,28,41),(18143,6,14,16,40),(18144,6,12,14,40),(18145,6,9,29,40),(18146,6,17,28,40),(18147,6,7,37,39),(18148,6,23,17,39),(18149,6,22,6,39),(18150,6,21,6,39),(18151,6,20,6,39),(18152,6,4,4,38),(18153,6,19,6,38),(18154,6,8,8,38),(18155,6,14,16,38),(18156,6,18,25,37),(18157,6,2,12,36),(18158,6,9,21,36),(18159,6,17,44,36),(18160,6,8,8,36),(18161,6,20,6,35),(18162,6,8,8,35),(18163,6,9,9,35),(18164,6,19,6,34),(18165,6,8,8,34),(18166,6,2,23,34),(18167,6,9,26,34),(18168,6,8,27,33),(18169,6,12,14,32),(18170,6,9,9,31),(18171,6,20,6,31),(18172,6,8,8,31),(18173,6,12,14,31),(18174,6,16,19,31),(18175,6,13,15,31),(18176,6,2,12,30),(18177,6,17,44,30),(18178,6,9,21,30),(18179,6,8,8,30),(18180,6,16,19,30),(18181,6,12,14,30),(18182,6,13,15,30),(18183,6,2,23,29),(18184,6,9,26,29),(18185,6,8,8,29),(18186,6,19,6,29),(18187,6,12,14,29),(18188,6,16,19,29),(18189,6,13,15,29),(18190,6,6,6,28),(18191,6,12,14,28),(18192,6,13,15,28),(18193,6,17,24,27),(18194,6,12,14,26),(18195,6,14,16,26),(18196,6,2,23,25),(18197,6,5,22,25),(18198,6,9,21,25),(18199,6,8,18,25),(18200,6,16,19,25),(18201,6,12,14,25),(18202,6,14,16,25),(18203,6,8,20,24),(18204,6,14,16,24),(18205,6,7,37,66),(18206,6,20,6,23),(18207,6,16,19,23),(18208,6,8,18,23),(18209,6,14,16,23),(18210,6,12,14,23),(18211,6,12,14,22),(18212,6,12,14,21),(18213,6,5,13,20),(18214,6,13,15,19),(18215,6,12,14,19),(18216,6,5,13,18),(18217,6,5,13,17),(18218,6,5,13,16),(18219,6,5,13,15),(18220,6,5,13,14),(18221,6,5,13,13),(18222,6,2,12,12),(18223,6,9,9,12),(18224,6,11,11,11),(18225,6,10,10,11),(18226,6,9,29,10),(18227,6,23,20,10),(18228,6,8,33,10),(18229,6,24,3,9),(18230,6,8,42,9),(18231,6,7,7,9),(18232,6,2,2,9),(18233,6,6,6,9),(18234,6,10,10,9),(18235,6,23,45,9),(18236,6,24,3,8),(18237,6,6,6,8),(18238,6,2,2,8),(18239,6,7,7,8),(18240,6,8,42,8),(18241,6,10,10,8),(18242,6,23,45,8),(18243,6,24,3,7),(18244,6,7,7,7),(18245,6,8,42,7),(18246,6,2,2,7),(18247,6,6,6,7),(18248,6,10,10,7),(18249,6,23,45,7),(18250,6,7,37,65),(18251,6,4,4,6),(18252,6,24,3,6),(18253,6,2,2,6),(18254,6,8,8,6),(18255,6,7,7,6),(18256,6,6,6,6),(18257,6,3,3,5),(18258,6,2,2,5),(18259,6,3,3,4),(18260,6,2,2,4),(18261,6,2,43,3),(18262,6,3,3,3),(18263,6,24,3,2),(18264,6,4,4,2),(18265,6,2,43,2),(18266,6,3,3,2),(18267,6,18,33,1),(18268,6,5,5,1),(18269,6,2,2,1),(18270,6,1,1,1),(18271,6,9,38,1),(18272,6,13,15,42),(18273,6,25,30,42),(18274,6,12,14,42),(18275,6,18,31,42),(18276,6,30,49,73),(18277,6,30,50,74),(18278,6,27,51,75),(18279,6,26,51,75);
/*!40000 ALTER TABLE `styles_style` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles_tags`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles_tags`
--

LOCK TABLES `styles_tags` WRITE;
/*!40000 ALTER TABLE `styles_tags` DISABLE KEYS */;
INSERT INTO `styles_tags` VALUES (1,'body'),(2,'a'),(3,'a:hover'),(4,'a:focus'),(5,'a:active'),(6,'button'),(7,'textarea'),(8,'select'),(9,'input'),(10,'label.checkbox'),(11,'input[type=checkbox]'),(12,'p'),(13,'h1'),(14,'h2'),(15,'h3'),(16,'h4'),(17,'h5'),(18,'h6'),(19,'.menu.vertical a'),(20,'.menu.vertical .title'),(21,'.card .header img'),(22,'.table .body .row img'),(23,'.table .body .row'),(24,'.table .col'),(25,'.table .header'),(26,'.table .body'),(27,'.right'),(28,'.card'),(29,'.card .header'),(30,'.card .body'),(31,'.card .footer'),(32,'img.responsive'),(33,'.card .header.image'),(34,'.panel .header'),(35,'.panel .footer'),(36,'.panel .body'),(37,'.panel'),(38,'.tabs .tab'),(39,'.tabs .tab.active'),(40,'footer'),(41,'pagination'),(42,'.container'),(44,'view-body.right-sidebar'),(45,'view-body.left-sidebar'),(46,'view-body'),(47,'right-sidebar'),(48,'wrapper'),(49,'left-sidebar'),(50,'view-header'),(64,'label'),(65,'button:hover'),(66,'.table .body .row:hover'),(67,'pagination .link'),(68,'pagination .link:hover'),(69,'pagination .link.not-important'),(70,'pagination .link.active'),(71,'pagination .link.unactive:hover'),(72,'pagination .link.unactive'),(73,'.group button:first-child'),(74,'.group button:last-child'),(75,'.group button');
/*!40000 ALTER TABLE `styles_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles_values`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles_values`
--

LOCK TABLES `styles_values` WRITE;
/*!40000 ALTER TABLE `styles_values` DISABLE KEYS */;
INSERT INTO `styles_values` VALUES (1,'\'Roboto\', sans-serif'),(2,'#333'),(3,'none'),(4,'pointer'),(5,'300'),(6,'1px solid #ccc'),(7,'#fff'),(8,'4px 8px'),(9,'0.9em'),(10,'relative'),(11,'1px'),(12,'#666'),(13,'400'),(14,'100%'),(15,'block'),(16,'left'),(17,'-1px'),(18,'0px 4px'),(19,'border-box'),(20,'4px'),(21,'0.85em'),(22,'600'),(23,'#000'),(24,'right'),(25,'5px'),(26,'1.1em'),(27,'0px'),(28,'center'),(29,'0.8em'),(30,'850px'),(31,'auto'),(32,'100'),(33,'0'),(34,'200px'),(35,'absolute'),(36,'10px'),(37,'#eee'),(38,'14px'),(39,'0 4px'),(40,'inline-block'),(41,'1'),(42,'10px 8px 4px 8px'),(43,'#14748C'),(44,'justify'),(45,'6px'),(46,'3px'),(47,'#ddd'),(48,'default'),(49,'5px 0px 0px 5px'),(50,'0px 5px 5px 0px'),(51,'-3px'),(52,'cover'),(53,'url(/Files/Images/57e3d5434c50a514f6da8c7dda79357e1739d8ec564c704c702678dc954dc25e_1280.png)'),(54,'url(/Files/Images/50e3d540484fad0bffd8992cc52835781336dbf852547440762778d0934a_1280.jpg)'),(55,'url(/Files/Images/55e7d34b425ba914f6da8c7dda79357e1739d8ec564c704c702678dc914fc150_1280.jpg)'),(56,'#FFFFFF'),(57,'url(/Files/Images/50e3d541424fad0bffd8992cc52835781336dbf85254744076277ed49f45_1280.jpg)'),(58,'url(/Files/Images/52e0d6454852af14f6da8c7dda79357e1739d8ec564c704c702678dc914cc65e_1280.jpg)'),(59,'url(/Files/Images/54e0dd444d56a514f6da8c7dda79357e1739d8ec564c704c702678dc924bc55d_1280.jpg)'),(60,'#FF0000'),(61,'url(/Files/Images/57e1d54a4a5baf14f6da8c7dda79357e1739d8ec564c704c702678dc924ec55a_1280.jpg)'),(62,'url(/Files/Images/57e3d5434c50a514f6da8c7dda79357e1739d8ec564c704c702678dc9249c35e_1280.png)'),(63,'#EC1414'),(64,'#E72B2B'),(65,'#E02525'),(66,'#5D1BEB'),(67,'15px 5px'),(68,'15px'),(69,'15px 0px'),(70,'1.15em'),(71,'url(/pliki/343b/dee3/76b0/1_54e5d5444f52a814f6da8c7dda79357e1739d8ec564c704c70267fd19045c359_1280.jpg)'),(72,'url(/pliki/3749/89b4/1915/54e5d5444f52a814f6da8c7dda79357e1739d8ec564c704c70267fd2974bc558_1280.jpg)'),(73,'url(/pliki/45f4/eaae/5e67/50e7d0464d4fad0bffd8992cc52835781336dbf85254744071297bd09745_1280.jpg)'),(74,'#EE1515'),(75,'url(/3749/89b4/1915/54e5d5444f52a814f6da8c7dda79357e1739d8ec564c704c70267fd2974bc558_1280.jpg)'),(76,'#fff !important'),(77,'url(/pliki/d4cd/9c24/1038/54e1d6454856a814f6da8c7dda79357e1739d8ec564c704c70267fd29749c550_1280.jpg)'),(78,'#E71919'),(79,'url(/pliki/b9fb/f625/164f/1_52e1dc454e52a414f6da8c7dda79357e1739d8ec564c704c70267fd19f4acc59_1280.jpg)'),(80,'75px 0px'),(81,'hsl(0, 0%, 0%)'),(82,'url(/pliki/7415bd6f5a956a13/50e7d0464d4fad0bffd8992cc52835781336dbf852547749742c79d5954a_1280.jpg)'),(83,'url(/pliki/dda8bb670773b191/54e5d5444f52a814f6da8c7dda79357e1739d8ec564c704c732f7ad7924dc25d_1280.jpg)'),(84,'url(/pliki/6b5d7da02b0c382b/57e0d2414954aa14f6da8c7dda79357e1739d8ec564c704c732f79d49448c05e_1280.jpg)');
/*!40000 ALTER TABLE `styles_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `caption` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_elements`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `element_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_id` (`tag_id`,`element_id`),
  KEY `fk_tags_elements_tag_id` (`tag_id`),
  KEY `fk_tags_elements_element_id` (`element_id`),
  CONSTRAINT `fk_tags_elements_element_id` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_tags_elements_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_elements`
--

LOCK TABLES `tags_elements` WRITE;
/*!40000 ALTER TABLE `tags_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_images`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `image_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_id` (`tag_id`,`image_id`),
  KEY `fk_tags_images_tag_id` (`tag_id`),
  KEY `fk_tags_images_image_id` (`image_id`),
  CONSTRAINT `fk_tags_images_image_id` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_tags_images_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_images`
--

LOCK TABLES `tags_images` WRITE;
/*!40000 ALTER TABLE `tags_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mail` varchar(90) DEFAULT NULL,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned DEFAULT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `reset` varchar(16) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT '0',
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `editor` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Jeżeli jest ustawione na 1 to znaczy że ten użytkownik edytuje projekty na potrzeby serwisu',
  `active_project_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `email_2` (`mail`,`deleted`),
  KEY `added_by` (`added_by`),
  KEY `added_ip_id` (`added_ip_id`),
  KEY `deleted_by` (`deleted_by`),
  KEY `deleted_ip_id` (`deleted_ip_id`),
  KEY `updated_by` (`updated_by`),
  KEY `updated_ip_id` (`updated_ip_id`),
  KEY `fk_users_active_project_id` (`active_project_id`),
  CONSTRAINT `fk_users_active_project_id` FOREIGN KEY (`active_project_id`) REFERENCES `projects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'worzala86@gmail.com','��\\O�e�',1558820875,NULL,2,0,NULL,NULL,1559512055,1,13,'3ee04957500696f42ec2be6fe35cb1ce9d8ff5c3dc22879950e0f855c78836f8a098619959908d25e60a390282fb3a38f6a756594c124b89dd63cef4b2eafbb1',NULL,1,'Paweł','Worzała',1,16);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_registrations`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_registrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` binary(8) NOT NULL,
  `added` int(10) unsigned NOT NULL,
  `added_by` int(10) unsigned DEFAULT NULL,
  `added_ip_id` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `updated_ip_id` int(10) unsigned DEFAULT NULL,
  `deleted` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `deleted_ip_id` int(10) unsigned DEFAULT NULL,
  `mail` varchar(250) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `newsletter` tinyint(1) DEFAULT NULL,
  `code` varchar(16) DEFAULT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `added_by` (`added_by`),
  KEY `added_ip_id` (`added_ip_id`),
  KEY `updated_by` (`updated_by`),
  KEY `updated_ip_id` (`updated_ip_id`),
  KEY `deleted_by` (`deleted_by`),
  KEY `deleted_ip_id` (`deleted_ip_id`),
  CONSTRAINT `fk_users_registrations_added_by` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_added_ip_id` FOREIGN KEY (`added_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_deleted_by` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_deleted_ip_id` FOREIGN KEY (`deleted_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_users_registrations_updated_ip_id` FOREIGN KEY (`updated_ip_id`) REFERENCES `ip` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_registrations`
--

LOCK TABLES `users_registrations` WRITE;
/*!40000 ALTER TABLE `users_registrations` DISABLE KEYS */;
INSERT INTO `users_registrations` VALUES (1,'c1405ba9',1558820831,NULL,2,NULL,NULL,NULL,0,NULL,NULL,'worzala86@gmail.com','3ee04957500696f42ec2be6fe35cb1ce9d8ff5c3dc22879950e0f855c78836f8a098619959908d25e60a390282fb3a38f6a756594c124b89dd63cef4b2eafbb1',0,'2385ab6552da219c','Paweł','Worzała');
/*!40000 ALTER TABLE `users_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `version`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `version` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(10) unsigned NOT NULL,
  `date` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `version`
--

LOCK TABLES `version` WRITE;
/*!40000 ALTER TABLE `version` DISABLE KEYS */;
INSERT INTO `version` VALUES (1,0,1558821682),(4,1,1558823534),(5,2,1558824229),(6,3,1559367535),(7,4,1559373319),(8,5,1559376739),(9,6,1559387569),(10,7,1559389044),(11,8,1559397435),(12,9,1559397435),(13,10,1559408062),(14,11,1559408363),(15,12,1559411912),(16,13,1559413743),(17,14,1559421166),(18,15,1559422403),(19,16,1559426085),(20,17,1559426473),(21,18,1559426720),(22,19,1559433791),(23,20,1559435656),(24,21,1559435704),(25,22,1559436591),(26,23,1559444556),(27,24,1559445535),(28,25,1559447298),(29,26,1559449463),(30,27,1559449559),(31,28,1559450033),(32,29,1559453161),(33,30,1560130325),(34,31,1560134440),(35,32,1560134880),(36,33,1560137159),(37,34,1560140727),(38,35,1560142341),(39,36,1560205399),(40,37,1560222288),(41,38,1560299001),(42,39,1560302029),(43,40,1560303476),(44,41,1560312747),(45,42,1560356534),(46,43,1561389055),(47,44,1561399179),(48,45,1561412007),(49,46,1561412052);
/*!40000 ALTER TABLE `version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `components_view`
--

/*!50001 DROP TABLE IF EXISTS `components_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `components_view` AS select `components`.`id` AS `id`,`components`.`uuid` AS `uuid`,`components`.`key` AS `key`,`components`.`description` AS `description`,`components`.`name` AS `name`,`components`.`file` AS `file`,`components`.`deleted` AS `deleted`,(select count(0) from `html` where (`html`.`component_id` = `components`.`id`)) AS `count`,`components`.`kind` AS `kind`,`components`.`simple_view` AS `simple_view` from `components` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `elements_datasets_view`
--

/*!50001 DROP TABLE IF EXISTS `elements_datasets_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `elements_datasets_view` AS select `elements_datasets`.`element_id` AS `element_id`,`elements_datasets`.`key` AS `key`,`elements_datasets`.`value` AS `value`,`elements`.`uuid` AS `element_position_id` from (`elements_datasets` left join `elements` on((`elements`.`id` = `elements_datasets`.`element_position_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `html_style`
--

/*!50001 DROP TABLE IF EXISTS `html_style`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `html_style` AS select `styles_style`.`styles_id` AS `styles_id`,`styles_params`.`name` AS `kind`,`styles_values`.`value` AS `value` from ((`styles_style` left join `styles_params` on((`styles_style`.`param_id` = `styles_params`.`id`))) left join `styles_values` on((`styles_style`.`value_id` = `styles_values`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `images_view`
--

/*!50001 DROP TABLE IF EXISTS `images_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `images_view` AS select `images`.`id` AS `id`,`images`.`uuid` AS `uuid`,`images`.`user_id` AS `user_id`,`images`.`file_id` AS `file_id`,`images`.`pixabay_id` AS `pixabay_id`,`images`.`deleted` AS `deleted`,(select count(0) from `tags_images` where (`tags_images`.`image_id` = `images`.`id`)) AS `tags_count` from `images` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pages_datasets_view`
--

/*!50001 DROP TABLE IF EXISTS `pages_datasets_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pages_datasets_view` AS select `pages_datasets`.`page_id` AS `page_id`,`pages_datasets`.`key` AS `key`,`pages_datasets`.`value` AS `value`,`pages_elements`.`uuid` AS `element_position_id` from (`pages_datasets` left join `pages_elements` on((`pages_elements`.`id` = `pages_datasets`.`element_position_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pages_day_count_view`
--

/*!50001 DROP TABLE IF EXISTS `pages_day_count_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pages_day_count_view` AS select sum(`p`.`count`) AS `count`,`p`.`date` AS `date` from `pages_day_project_count_view` `p` group by `p`.`date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pages_day_project_count_view`
--

/*!50001 DROP TABLE IF EXISTS `pages_day_project_count_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pages_day_project_count_view` AS select `pages`.`project_id` AS `project_id`,count(0) AS `count`,date_format(from_unixtime(`pages`.`added`),'%Y-%m-%d') AS `date` from `pages` group by `pages`.`project_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pages_month_count_view`
--

/*!50001 DROP TABLE IF EXISTS `pages_month_count_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pages_month_count_view` AS select sum(`p`.`count`) AS `count`,`p`.`date` AS `date` from `pages_month_project_count_view` `p` group by `p`.`date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pages_month_project_count_view`
--

/*!50001 DROP TABLE IF EXISTS `pages_month_project_count_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pages_month_project_count_view` AS select `pages`.`project_id` AS `project_id`,count(0) AS `count`,date_format(from_unixtime(`pages`.`added`),'%Y-%m') AS `date` from `pages` group by `pages`.`project_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `projects_datasets_view`
--

/*!50001 DROP TABLE IF EXISTS `projects_datasets_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `projects_datasets_view` AS select `projects_datasets`.`project_id` AS `project_id`,`projects_datasets`.`key` AS `key`,`projects_datasets`.`value` AS `value`,`projects_elements`.`uuid` AS `element_position_id` from (`projects_datasets` left join `projects_elements` on((`projects_elements`.`id` = `projects_datasets`.`element_position_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `styles_css_view`
--

/*!50001 DROP TABLE IF EXISTS `styles_css_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `styles_css_view` AS select `styles_style`.`id` AS `id`,`styles_style`.`styles_id` AS `styles_id`,`styles_params`.`name` AS `param`,`styles_values`.`value` AS `value`,`styles_tags`.`name` AS `tag` from (((`styles_style` left join `styles_tags` on((`styles_tags`.`id` = `styles_style`.`tag_id`))) left join `styles_params` on((`styles_params`.`id` = `styles_style`.`param_id`))) left join `styles_values` on((`styles_values`.`id` = `styles_style`.`value_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-24 23:34:31
